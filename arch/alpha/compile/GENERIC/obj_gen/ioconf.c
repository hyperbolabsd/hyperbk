/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/alpha/conf/GENERIC"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver video_cd;
extern struct cfdriver audio_cd;
extern struct cfdriver midi_cd;
extern struct cfdriver vga_cd;
extern struct cfdriver wdc_cd;
extern struct cfdriver ahc_cd;
extern struct cfdriver adw_cd;
extern struct cfdriver gdt_cd;
extern struct cfdriver twe_cd;
extern struct cfdriver ciss_cd;
extern struct cfdriver qlw_cd;
extern struct cfdriver qla_cd;
extern struct cfdriver mpi_cd;
extern struct cfdriver sili_cd;
extern struct cfdriver siop_cd;
extern struct cfdriver ep_cd;
extern struct cfdriver lc_cd;
extern struct cfdriver wi_cd;
extern struct cfdriver an_cd;
extern struct cfdriver le_cd;
extern struct cfdriver xl_cd;
extern struct cfdriver fxp_cd;
extern struct cfdriver mtd_cd;
extern struct cfdriver rl_cd;
extern struct cfdriver re_cd;
extern struct cfdriver dc_cd;
extern struct cfdriver epic_cd;
extern struct cfdriver ne_cd;
extern struct cfdriver ti_cd;
extern struct cfdriver com_cd;
extern struct cfdriver pckbc_cd;
extern struct cfdriver cy_cd;
extern struct cfdriver lpt_cd;
extern struct cfdriver lm_cd;
extern struct cfdriver ath_cd;
extern struct cfdriver atw_cd;
extern struct cfdriver ral_cd;
extern struct cfdriver acx_cd;
extern struct cfdriver pgt_cd;
extern struct cfdriver sf_cd;
extern struct cfdriver malo_cd;
extern struct cfdriver uhci_cd;
extern struct cfdriver ohci_cd;
extern struct cfdriver ehci_cd;
extern struct cfdriver radio_cd;
extern struct cfdriver vscsi_cd;
extern struct cfdriver softraid_cd;
extern struct cfdriver spdmem_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver tlphy_cd;
extern struct cfdriver nsphy_cd;
extern struct cfdriver nsphyter_cd;
extern struct cfdriver qsphy_cd;
extern struct cfdriver inphy_cd;
extern struct cfdriver iophy_cd;
extern struct cfdriver eephy_cd;
extern struct cfdriver exphy_cd;
extern struct cfdriver rlphy_cd;
extern struct cfdriver lxtphy_cd;
extern struct cfdriver luphy_cd;
extern struct cfdriver mtdphy_cd;
extern struct cfdriver icsphy_cd;
extern struct cfdriver sqphy_cd;
extern struct cfdriver tqphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver dcphy_cd;
extern struct cfdriver bmtphy_cd;
extern struct cfdriver brgphy_cd;
extern struct cfdriver xmphy_cd;
extern struct cfdriver amphy_cd;
extern struct cfdriver acphy_cd;
extern struct cfdriver urlphy_cd;
extern struct cfdriver rgephy_cd;
extern struct cfdriver ciphy_cd;
extern struct cfdriver ipgphy_cd;
extern struct cfdriver mlphy_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver cd_cd;
extern struct cfdriver ch_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver st_cd;
extern struct cfdriver uk_cd;
extern struct cfdriver safte_cd;
extern struct cfdriver ses_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver wsmouse_cd;
extern struct cfdriver tc_cd;
extern struct cfdriver ioasic_cd;
extern struct cfdriver bba_cd;
extern struct cfdriver tcds_cd;
extern struct cfdriver asc_cd;
extern struct cfdriver zs_cd;
extern struct cfdriver zstty_cd;
extern struct cfdriver mcbus_cd;
extern struct cfdriver mcmem_cd;
extern struct cfdriver tcasic_cd;
extern struct cfdriver atapiscsi_cd;
extern struct cfdriver wd_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver tga_cd;
extern struct cfdriver eap_cd;
extern struct cfdriver eso_cd;
extern struct cfdriver emu_cd;
extern struct cfdriver fms_cd;
extern struct cfdriver auvia_cd;
extern struct cfdriver qle_cd;
extern struct cfdriver de_cd;
extern struct cfdriver pcn_cd;
extern struct cfdriver pciide_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver vr_cd;
extern struct cfdriver tl_cd;
extern struct cfdriver txp_cd;
extern struct cfdriver sv_cd;
extern struct cfdriver bktr_cd;
extern struct cfdriver em_cd;
extern struct cfdriver xge_cd;
extern struct cfdriver hifn_cd;
extern struct cfdriver ubsec_cd;
extern struct cfdriver safe_cd;
extern struct cfdriver wb_cd;
extern struct cfdriver sis_cd;
extern struct cfdriver ste_cd;
extern struct cfdriver skc_cd;
extern struct cfdriver sk_cd;
extern struct cfdriver mskc_cd;
extern struct cfdriver msk_cd;
extern struct cfdriver puc_cd;
extern struct cfdriver cmpci_cd;
extern struct cfdriver pcscp_cd;
extern struct cfdriver lge_cd;
extern struct cfdriver bge_cd;
extern struct cfdriver vge_cd;
extern struct cfdriver stge_cd;
extern struct cfdriver alipm_cd;
extern struct cfdriver isa_cd;
extern struct cfdriver isadma_cd;
extern struct cfdriver fdc_cd;
extern struct cfdriver fd_cd;
extern struct cfdriver we_cd;
extern struct cfdriver ec_cd;
extern struct cfdriver pcppi_cd;
extern struct cfdriver spkr_cd;
extern struct cfdriver pcic_cd;
extern struct cfdriver pcmcia_cd;
extern struct cfdriver pckbd_cd;
extern struct cfdriver pms_cd;
extern struct cfdriver eisa_cd;
extern struct cfdriver apecs_cd;
extern struct cfdriver lca_cd;
extern struct cfdriver cia_cd;
extern struct cfdriver irongate_cd;
extern struct cfdriver tsc_cd;
extern struct cfdriver tsciic_cd;
extern struct cfdriver tsp_cd;
extern struct cfdriver mcpcia_cd;
extern struct cfdriver sio_cd;
extern struct cfdriver pceb_cd;
extern struct cfdriver hme_cd;
extern struct cfdriver isapnp_cd;
extern struct cfdriver mcclock_cd;
extern struct cfdriver iic_cd;
extern struct cfdriver lmtemp_cd;
extern struct cfdriver lmenv_cd;
extern struct cfdriver maxtmp_cd;
extern struct cfdriver adc_cd;
extern struct cfdriver admtemp_cd;
extern struct cfdriver admlc_cd;
extern struct cfdriver admtm_cd;
extern struct cfdriver admtmp_cd;
extern struct cfdriver admtt_cd;
extern struct cfdriver maxds_cd;
extern struct cfdriver adt_cd;
extern struct cfdriver sdtemp_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver uaudio_cd;
extern struct cfdriver uvideo_cd;
extern struct cfdriver utvfu_cd;
extern struct cfdriver umidi_cd;
extern struct cfdriver ucom_cd;
extern struct cfdriver ugen_cd;
extern struct cfdriver uhidev_cd;
extern struct cfdriver uhid_cd;
extern struct cfdriver fido_cd;
extern struct cfdriver ujoy_cd;
extern struct cfdriver ukbd_cd;
extern struct cfdriver ums_cd;
extern struct cfdriver ucycom_cd;
extern struct cfdriver uslhcom_cd;
extern struct cfdriver ulpt_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver uthum_cd;
extern struct cfdriver ugold_cd;
extern struct cfdriver uonerng_cd;
extern struct cfdriver urng_cd;
extern struct cfdriver uvisor_cd;
extern struct cfdriver udsbr_cd;
extern struct cfdriver utwitch_cd;
extern struct cfdriver aue_cd;
extern struct cfdriver axe_cd;
extern struct cfdriver axen_cd;
extern struct cfdriver smsc_cd;
extern struct cfdriver cue_cd;
extern struct cfdriver kue_cd;
extern struct cfdriver cdce_cd;
extern struct cfdriver urndis_cd;
extern struct cfdriver udav_cd;
extern struct cfdriver upl_cd;
extern struct cfdriver ugl_cd;
extern struct cfdriver url_cd;
extern struct cfdriver ure_cd;
extern struct cfdriver umodem_cd;
extern struct cfdriver uftdi_cd;
extern struct cfdriver uplcom_cd;
extern struct cfdriver umct_cd;
extern struct cfdriver uvscom_cd;
extern struct cfdriver ubsa_cd;
extern struct cfdriver uslcom_cd;
extern struct cfdriver uark_cd;
extern struct cfdriver uscom_cd;
extern struct cfdriver ucrcom_cd;
extern struct cfdriver uipaq_cd;
extern struct cfdriver uchcom_cd;
extern struct cfdriver ural_cd;
extern struct cfdriver rum_cd;
extern struct cfdriver run_cd;
extern struct cfdriver uath_cd;
extern struct cfdriver uow_cd;
extern struct cfdriver upd_cd;
extern struct cfdriver uhidpp_cd;
extern struct cfdriver ucc_cd;
extern struct cfdriver onewire_cd;
extern struct cfdriver owid_cd;
extern struct cfdriver owsbm_cd;
extern struct cfdriver owtemp_cd;
extern struct cfdriver owctr_cd;

extern struct cfattach video_ca;
extern struct cfattach audio_ca;
extern struct cfattach midi_ca;
extern struct cfattach radio_ca;
extern struct cfattach vscsi_ca;
extern struct cfattach softraid_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach cpu_ca;
extern struct cfattach tlphy_ca;
extern struct cfattach nsphy_ca;
extern struct cfattach nsphyter_ca;
extern struct cfattach qsphy_ca;
extern struct cfattach inphy_ca;
extern struct cfattach iophy_ca;
extern struct cfattach eephy_ca;
extern struct cfattach exphy_ca;
extern struct cfattach rlphy_ca;
extern struct cfattach lxtphy_ca;
extern struct cfattach luphy_ca;
extern struct cfattach mtdphy_ca;
extern struct cfattach icsphy_ca;
extern struct cfattach sqphy_ca;
extern struct cfattach tqphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach dcphy_ca;
extern struct cfattach bmtphy_ca;
extern struct cfattach brgphy_ca;
extern struct cfattach xmphy_ca;
extern struct cfattach amphy_ca;
extern struct cfattach acphy_ca;
extern struct cfattach urlphy_ca;
extern struct cfattach rgephy_ca;
extern struct cfattach ciphy_ca;
extern struct cfattach ipgphy_ca;
extern struct cfattach mlphy_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach cd_ca;
extern struct cfattach ch_ca;
extern struct cfattach sd_ca;
extern struct cfattach st_ca;
extern struct cfattach uk_ca;
extern struct cfattach safte_ca;
extern struct cfattach ses_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach wsmouse_ca;
extern struct cfattach ioasic_ca;
extern struct cfattach le_ioasic_ca;
extern struct cfattach le_tc_ca;
extern struct cfattach bba_ca;
extern struct cfattach tcds_ca;
extern struct cfattach asc_tc_ca;
extern struct cfattach asc_tcds_ca;
extern struct cfattach zs_ioasic_ca;
extern struct cfattach zstty_ca;
extern struct cfattach mcbus_ca;
extern struct cfattach mcmem_ca;
extern struct cfattach tc_ca;
extern struct cfattach tcasic_ca;
extern struct cfattach atapiscsi_ca;
extern struct cfattach wd_ca;
extern struct cfattach pci_ca;
extern struct cfattach vga_pci_ca;
extern struct cfattach tga_ca;
extern struct cfattach ahc_pci_ca;
extern struct cfattach adw_pci_ca;
extern struct cfattach twe_pci_ca;
extern struct cfattach eap_ca;
extern struct cfattach eso_ca;
extern struct cfattach emu_ca;
extern struct cfattach fms_ca;
extern struct cfattach auvia_ca;
extern struct cfattach gdt_pci_ca;
extern struct cfattach ciss_pci_ca;
extern struct cfattach qlw_pci_ca;
extern struct cfattach qla_pci_ca;
extern struct cfattach qle_ca;
extern struct cfattach mpi_pci_ca;
extern struct cfattach sili_pci_ca;
extern struct cfattach de_ca;
extern struct cfattach ep_pci_ca;
extern struct cfattach pcn_ca;
extern struct cfattach siop_pci_ca;
extern struct cfattach pciide_pci_ca;
extern struct cfattach ppb_ca;
extern struct cfattach cy_pci_ca;
extern struct cfattach mtd_pci_ca;
extern struct cfattach rl_pci_ca;
extern struct cfattach re_pci_ca;
extern struct cfattach vr_ca;
extern struct cfattach tl_ca;
extern struct cfattach txp_ca;
extern struct cfattach sv_ca;
extern struct cfattach bktr_ca;
extern struct cfattach xl_pci_ca;
extern struct cfattach fxp_pci_ca;
extern struct cfattach em_ca;
extern struct cfattach xge_ca;
extern struct cfattach dc_pci_ca;
extern struct cfattach epic_pci_ca;
extern struct cfattach ti_pci_ca;
extern struct cfattach ne_pci_ca;
extern struct cfattach hifn_ca;
extern struct cfattach ubsec_ca;
extern struct cfattach safe_ca;
extern struct cfattach wb_ca;
extern struct cfattach sf_pci_ca;
extern struct cfattach sis_ca;
extern struct cfattach ste_ca;
extern struct cfattach uhci_pci_ca;
extern struct cfattach ohci_pci_ca;
extern struct cfattach ehci_pci_ca;
extern struct cfattach skc_ca;
extern struct cfattach sk_ca;
extern struct cfattach mskc_ca;
extern struct cfattach msk_ca;
extern struct cfattach com_puc_ca;
extern struct cfattach puc_pci_ca;
extern struct cfattach wi_pci_ca;
extern struct cfattach an_pci_ca;
extern struct cfattach cmpci_ca;
extern struct cfattach pcscp_ca;
extern struct cfattach lge_ca;
extern struct cfattach bge_ca;
extern struct cfattach vge_ca;
extern struct cfattach stge_ca;
extern struct cfattach ath_pci_ca;
extern struct cfattach atw_pci_ca;
extern struct cfattach ral_pci_ca;
extern struct cfattach acx_pci_ca;
extern struct cfattach pgt_pci_ca;
extern struct cfattach malo_pci_ca;
extern struct cfattach alipm_ca;
extern struct cfattach isa_ca;
extern struct cfattach isadma_ca;
extern struct cfattach fdc_ca;
extern struct cfattach fd_ca;
extern struct cfattach com_isa_ca;
extern struct cfattach cy_isa_ca;
extern struct cfattach pckbc_isa_ca;
extern struct cfattach vga_isa_ca;
extern struct cfattach wdc_isa_ca;
extern struct cfattach lc_isa_ca;
extern struct cfattach ne_isa_ca;
extern struct cfattach we_isa_ca;
extern struct cfattach ec_ca;
extern struct cfattach ep_isa_ca;
extern struct cfattach pcppi_ca;
extern struct cfattach spkr_ca;
extern struct cfattach lpt_isa_ca;
extern struct cfattach pcic_isa_ca;
extern struct cfattach pcmcia_ca;
extern struct cfattach ep_pcmcia_ca;
extern struct cfattach pckbd_ca;
extern struct cfattach pms_ca;
extern struct cfattach eisa_ca;
extern struct cfattach ahc_eisa_ca;
extern struct cfattach apecs_ca;
extern struct cfattach lca_ca;
extern struct cfattach cia_ca;
extern struct cfattach irongate_ca;
extern struct cfattach tsc_ca;
extern struct cfattach tsciic_ca;
extern struct cfattach tsp_ca;
extern struct cfattach mcpcia_ca;
extern struct cfattach sio_ca;
extern struct cfattach pceb_ca;
extern struct cfattach hme_pci_ca;
extern struct cfattach isapnp_ca;
extern struct cfattach ep_isapnp_ca;
extern struct cfattach mcclock_ioasic_ca;
extern struct cfattach mcclock_isa_ca;
extern struct cfattach iic_ca;
extern struct cfattach lmtemp_ca;
extern struct cfattach lmenv_ca;
extern struct cfattach maxtmp_ca;
extern struct cfattach adc_ca;
extern struct cfattach admtemp_ca;
extern struct cfattach admlc_ca;
extern struct cfattach admtm_ca;
extern struct cfattach admtmp_ca;
extern struct cfattach admtt_ca;
extern struct cfattach maxds_ca;
extern struct cfattach adt_ca;
extern struct cfattach lm_i2c_ca;
extern struct cfattach spdmem_iic_ca;
extern struct cfattach sdtemp_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach uaudio_ca;
extern struct cfattach uvideo_ca;
extern struct cfattach utvfu_ca;
extern struct cfattach umidi_ca;
extern struct cfattach ucom_ca;
extern struct cfattach ugen_ca;
extern struct cfattach uhidev_ca;
extern struct cfattach uhid_ca;
extern struct cfattach fido_ca;
extern struct cfattach ujoy_ca;
extern struct cfattach ukbd_ca;
extern struct cfattach ums_ca;
extern struct cfattach ucycom_ca;
extern struct cfattach uslhcom_ca;
extern struct cfattach ulpt_ca;
extern struct cfattach umass_ca;
extern struct cfattach uthum_ca;
extern struct cfattach ugold_ca;
extern struct cfattach uonerng_ca;
extern struct cfattach urng_ca;
extern struct cfattach uvisor_ca;
extern struct cfattach udsbr_ca;
extern struct cfattach utwitch_ca;
extern struct cfattach aue_ca;
extern struct cfattach axe_ca;
extern struct cfattach axen_ca;
extern struct cfattach smsc_ca;
extern struct cfattach cue_ca;
extern struct cfattach kue_ca;
extern struct cfattach cdce_ca;
extern struct cfattach urndis_ca;
extern struct cfattach udav_ca;
extern struct cfattach upl_ca;
extern struct cfattach ugl_ca;
extern struct cfattach url_ca;
extern struct cfattach ure_ca;
extern struct cfattach umodem_ca;
extern struct cfattach uftdi_ca;
extern struct cfattach uplcom_ca;
extern struct cfattach umct_ca;
extern struct cfattach uvscom_ca;
extern struct cfattach ubsa_ca;
extern struct cfattach uslcom_ca;
extern struct cfattach uark_ca;
extern struct cfattach uscom_ca;
extern struct cfattach ucrcom_ca;
extern struct cfattach uipaq_ca;
extern struct cfattach uchcom_ca;
extern struct cfattach wi_usb_ca;
extern struct cfattach ural_ca;
extern struct cfattach rum_ca;
extern struct cfattach run_ca;
extern struct cfattach uath_ca;
extern struct cfattach uow_ca;
extern struct cfattach upd_ca;
extern struct cfattach uhidpp_ca;
extern struct cfattach ucc_ca;
extern struct cfattach onewire_ca;
extern struct cfattach owid_ca;
extern struct cfattach owsbm_ca;
extern struct cfattach owtemp_ca;
extern struct cfattach owctr_ca;


/* locators */
static long loc[149] = {
	0x3f0, 0, -1, 0, 6, 2, -1, 0x3bc,
	0, -1, 0, 7, -1, -1, -1, 0,
	-1, 0, -1, -1, -1, 0x250, 0, 0xd8000,
	0, 9, -1, -1, 0x70, 0, -1, 0,
	-1, -1, -1, 0x3e2, 0, 0xcc000, 0x10000, -1,
	-1, -1, 0x300, 0, 0xcc000, 0, 0xa, -1,
	-1, 0x280, 0, 0xd0000, 0, 9, -1, -1,
	0x280, 0, -1, 0, 9, -1, -1, 0x300,
	0, -1, 0, 0xa, -1, -1, 0x240, 0,
	-1, 0, 9, -1, -1, 0x280, 0, -1,
	0, -1, -1, -1, 0x200, 0, -1, 0,
	-1, -1, -1, 0x170, 0, -1, 0, 0xf,
	-1, -1, 0x1f0, 0, -1, 0, 0xe, -1,
	-1, -1, 0, 0xd4000, 0, 0xc, -1, -1,
	0x2f8, 0, -1, 0, 3, -1, -1, 0x3f8,
	0, -1, 0, 4, -1, -1, 0x3e0, 0,
	0xd0000, 0x10000, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, 0, -1, 0, -1,
	-1, -1, -1, 1, 0,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"phy",
	"target",
	"lun",
	"console",
	"primary",
	"mux",
	"slot",
	"offset",
	"chip",
	"channel",
	"mid",
	"bus",
	"dev",
	"function",
	"port",
	"size",
	"iomem",
	"iosiz",
	"irq",
	"drq",
	"drq2",
	"drive",
	"controller",
	"socket",
	"addr",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"portno",
	"reportid",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 1, 2, -1, 3, 4,
	5, -1, 3, 4, 5, -1, 3, 4,
	5, -1, 3, 5, -1, 3, 5, -1,
	3, 5, -1, 5, -1, 5, -1, 6,
	7, -1, 7, -1, 8, -1, 9, -1,
	10, -1, 9, -1, 9, -1, 9, -1,
	11, -1, 11, -1, 11, -1, 11, -1,
	11, -1, 11, -1, 11, -1, 12, 13,
	-1, 14, 15, 16, 17, 18, 19, 20,
	-1, 6, -1, 14, 15, 16, 17, 18,
	19, -1, 13, 18, -1, 14, -1, 21,
	-1, 22, 23, -1, 22, 23, -1, 6,
	-1, 24, 15, -1, 14, 25, 26, 27,
	28, 29, -1, 14, 25, 26, 27, 28,
	29, -1, 30, -1, 30, -1, 30, -1,
	30, -1, 30, -1, 30, -1, 30, -1,
	30, -1, 30, -1, 30, -1, 30, -1,
	30, -1, 30, -1, 30, -1, 30, -1,
	31, -1,
};

/* size of parent vectors */
int pv_size = 185;

/* parent vectors */
short pv[185] = {
	232, 231, 228, 223, 222, 221, 220, 175, 130, 129, 128, 127, 120, 118, 113, 112,
	110, 95, 94, 86, 111, 105, 106, 150, 151, 152, 104, 103, 93, 92, 91, 100,
	99, 82, 83, 84, 85, -1, 5, 4, 212, 126, 78, 59, 52, 51, 87, 80,
	79, 77, 76, 75, 68, 74, 67, 65, 66, -1, 233, 217, 237, 238, 234, 235,
	236, 239, 241, 242, 240, 243, 244, 209, 210, -1, 197, 199, 49, 69, 71, 70,
	97, 125, 72, 73, -1, 165, 167, 168, 166, 89, 171, 172, -1, 146, 147, 88,
	-1, 116, 114, 115, -1, 62, 63, 64, -1, 207, 253, 162, -1, 195, 196, -1,
	137, 170, -1, 174, 173, -1, 198, 199, -1, 200, 69, -1, 218, 98, -1, 159,
	160, -1, 208, 163, -1, 61, -1, 35, -1, 57, -1, 179, -1, 46, -1, 50,
	-1, 53, -1, 55, -1, 164, -1, 122, -1, 140, -1, 145, -1, 203, -1, 169,
	-1, 6, -1, 174, -1, 156, -1, 119, -1, 117, -1, 58, -1, 138, -1, 176,
	-1, 161, -1, 254, -1, 194, -1, 250, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: video* at uvideo*|utvfu* */
    {&video_ca,		&video_cd,	 0, STAR,     loc,    0, pv+118, 0,    0},
/*  1: audio* at uaudio*|utvfu*|bba0|eap*|emu*|eso*|sv*|cmpci*|fms*|auvia* */
    {&audio_ca,		&audio_cd,	 0, STAR,     loc,    0, pv+74, 0,    0},
/*  2: midi* at umidi*|eap* */
    {&midi_ca,		&midi_cd,	 0, STAR,     loc,    0, pv+121, 0,    0},
/*  3: radio* at udsbr*|bktr0 */
    {&radio_ca,		&radio_cd,	 0, STAR,     loc,    0, pv+124, 0,    0},
/*  4: vscsi0 at root */
    {&vscsi_ca,		&vscsi_cd,	 0, NORM,     loc,    0, pv+37, 0,    0},
/*  5: softraid0 at root */
    {&softraid_ca,	&softraid_cd,	 0, NORM,     loc,    0, pv+37, 0,    0},
/*  6: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+37, 0,    0},
/*  7: cpu0 at mainbus0 */
    {&cpu_ca,		&cpu_cd,	 0, NORM,     loc,    0, pv+161, 111,    0},
/*  8: tlphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&tlphy_ca,		&tlphy_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/*  9: nsphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&nsphy_ca,		&nsphy_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/* 10: nsphyter* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&nsphyter_ca,	&nsphyter_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/* 11: qsphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&qsphy_ca,		&qsphy_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/* 12: inphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&inphy_ca,		&inphy_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/* 13: iophy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&iophy_ca,		&iophy_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/* 14: eephy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&eephy_ca,		&eephy_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/* 15: exphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&exphy_ca,		&exphy_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/* 16: rlphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&rlphy_ca,		&rlphy_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/* 17: lxtphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&lxtphy_ca,	&lxtphy_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/* 18: luphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&luphy_ca,		&luphy_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/* 19: mtdphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&mtdphy_ca,	&mtdphy_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/* 20: icsphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&icsphy_ca,	&icsphy_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/* 21: sqphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&sqphy_ca,		&sqphy_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/* 22: tqphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&tqphy_ca,		&tqphy_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/* 23: ukphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/* 24: dcphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&dcphy_ca,		&dcphy_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/* 25: bmtphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&bmtphy_ca,	&bmtphy_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/* 26: brgphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&brgphy_ca,	&brgphy_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/* 27: xmphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&xmphy_ca,		&xmphy_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/* 28: amphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&amphy_ca,		&amphy_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/* 29: acphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&acphy_ca,		&acphy_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/* 30: urlphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&urlphy_ca,	&urlphy_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/* 31: rgephy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&rgephy_ca,	&rgephy_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/* 32: ciphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&ciphy_ca,		&ciphy_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/* 33: ipgphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&ipgphy_ca,	&ipgphy_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/* 34: mlphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|ne0|ne1|ne2|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep*|ep* phy -1 */
    {&mlphy_ca,		&mlphy_cd,	 0, STAR, loc+144,    0, pv+ 0, 1,    0},
/* 35: scsibus* at softraid0|vscsi0|umass*|pcscp*|qle*|atapiscsi*|asc*|asc*|siop*|sili*|mpi*|qla*|qlw*|ciss*|twe*|gdt*|adw*|ahc*|ahc* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+38, 74,    0},
/* 36: cd* at scsibus* target -1 lun -1 */
    {&cd_ca,		&cd_cd,		 0, STAR, loc+143,    0, pv+135, 75,    0},
/* 37: ch* at scsibus* target -1 lun -1 */
    {&ch_ca,		&ch_cd,		 0, STAR, loc+143,    0, pv+135, 75,    0},
/* 38: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+143,    0, pv+135, 75,    0},
/* 39: st* at scsibus* target -1 lun -1 */
    {&st_ca,		&st_cd,		 0, STAR, loc+143,    0, pv+135, 75,    0},
/* 40: uk* at scsibus* target -1 lun -1 */
    {&uk_ca,		&uk_cd,		 0, STAR, loc+143,    0, pv+135, 75,    0},
/* 41: safte* at scsibus* target -1 lun -1 */
    {&safte_ca,		&safte_cd,	 0, STAR, loc+143,    0, pv+135, 75,    0},
/* 42: ses* at scsibus* target -1 lun -1 */
    {&ses_ca,		&ses_cd,	 0, STAR, loc+143,    0, pv+135, 75,    0},
/* 43: wsdisplay* at vga*|vga*|tga* console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+145,    0, pv+101, 78,    0},
/* 44: wskbd* at ukbd*|ucc*|pckbd* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+146,    0, pv+105, 90,    0},
/* 45: wsmouse* at ums*|pms* mux 0 */
    {&wsmouse_ca,	&wsmouse_cd,	 0, STAR, loc+148,    0, pv+130, 99,    0},
/* 46: ioasic* at tc* slot -1 offset -1 */
    {&ioasic_ca,	&ioasic_cd,	 0, STAR, loc+143,    0, pv+137, 103,    0},
/* 47: le* at ioasic* offset -1 */
    {&le_ioasic_ca,	&le_cd,		 0, STAR, loc+144,    0, pv+141, 106,    0},
/* 48: le* at tc* slot -1 offset -1 */
    {&le_tc_ca,		&le_cd,		 0, STAR, loc+143,    0, pv+137, 103,    0},
/* 49: bba0 at ioasic* offset -1 */
    {&bba_ca,		&bba_cd,	 0, NORM, loc+144,    0, pv+141, 106,    0},
/* 50: tcds* at tc* slot -1 offset -1 */
    {&tcds_ca,		&tcds_cd,	 0, STAR, loc+143,    0, pv+137, 103,    0},
/* 51: asc* at tc* slot -1 offset -1 */
    {&asc_tc_ca,	&asc_cd,	 0, STAR, loc+143,    0, pv+137, 103,    0},
/* 52: asc* at tcds* chip -1 */
    {&asc_tcds_ca,	&asc_cd,	 0, STAR, loc+144,    0, pv+143, 108,    0},
/* 53: zs* at ioasic* offset -1 */
    {&zs_ioasic_ca,	&zs_cd,		 0, STAR, loc+144,    0, pv+141, 106,    0},
/* 54: zstty* at zs* channel 1 */
    {&zstty_ca,		&zstty_cd,	 0, STAR, loc+147,    0, pv+145, 110,    0},
/* 55: mcbus* at mainbus0 */
    {&mcbus_ca,		&mcbus_cd,	 0, STAR,     loc,    0, pv+161, 111,    0},
/* 56: mcmem* at mcbus* mid -1 */
    {&mcmem_ca,		&mcmem_cd,	 0, STAR, loc+144,    0, pv+147, 112,    0},
/* 57: tc* at tcasic* */
    {&tc_ca,		&tc_cd,		 0, STAR,     loc,    0, pv+171, 113,    0},
/* 58: tcasic* at mainbus0 */
    {&tcasic_ca,	&tcasic_cd,	 0, STAR,     loc,    0, pv+161, 111,    0},
/* 59: atapiscsi* at wdc0|wdc1|pciide* channel -1 */
    {&atapiscsi_ca,	&atapiscsi_cd,	 0, STAR, loc+144,    0, pv+93, 114,    0},
/* 60: wd* at wdc0|wdc1|pciide* channel -1 drive -1 */
    {&wd_ca,		&wd_cd,		 0, STAR, loc+143,    0, pv+93, 114,    0},
/* 61: pci* at apecs*|cia*|irongate*|lca*|ppb*|tsp*|mcpcia* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+144,    0, pv+85, 120,    0},
/* 62: vga* at pci* dev -1 function -1 */
    {&vga_pci_ca,	&vga_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 63: vga* at isa* port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&vga_isa_ca,	&vga_cd,	 0, STAR, loc+ 14,    0, pv+173, 137,    0},
/* 64: tga* at pci* dev -1 function -1 */
    {&tga_ca,		&tga_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 65: ahc* at pci* dev -1 function -1 */
    {&ahc_pci_ca,	&ahc_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 66: ahc* at eisa* slot -1 */
    {&ahc_eisa_ca,	&ahc_cd,	 0, STAR, loc+144,    0, pv+149, 145,    0},
/* 67: adw* at pci* dev -1 function -1 */
    {&adw_pci_ca,	&adw_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 68: twe* at pci* dev -1 function -1 */
    {&twe_pci_ca,	&twe_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 69: eap* at pci* dev -1 function -1 */
    {&eap_ca,		&eap_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 70: eso* at pci* dev -1 function -1 */
    {&eso_ca,		&eso_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 71: emu* at pci* dev -1 function -1 */
    {&emu_ca,		&emu_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 72: fms* at pci* dev -1 function -1 */
    {&fms_ca,		&fms_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 73: auvia* at pci* dev -1 function -1 */
    {&auvia_ca,		&auvia_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 74: gdt* at pci* dev -1 function -1 */
    {&gdt_pci_ca,	&gdt_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 75: ciss* at pci* dev -1 function -1 */
    {&ciss_pci_ca,	&ciss_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 76: qlw* at pci* dev -1 function -1 */
    {&qlw_pci_ca,	&qlw_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 77: qla* at pci* dev -1 function -1 */
    {&qla_pci_ca,	&qla_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 78: qle* at pci* dev -1 function -1 */
    {&qle_ca,		&qle_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 79: mpi* at pci* dev -1 function -1 */
    {&mpi_pci_ca,	&mpi_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 80: sili* at pci* dev -1 function -1 */
    {&sili_pci_ca,	&sili_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 81: de* at pci* dev -1 function -1 */
    {&de_ca,		&de_cd,		 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 82: ep* at pci* dev -1 function -1 */
    {&ep_pci_ca,	&ep_cd,		 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 83: ep* at isa* port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&ep_isa_ca,	&ep_cd,		 0, STAR, loc+ 14,    0, pv+173, 137,    0},
/* 84: ep* at isapnp0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 */
    {&ep_isapnp_ca,	&ep_cd,		 0, STAR, loc+139,    0, pv+175, 147,    0},
/* 85: ep* at pcmcia* function -1 irq -1 */
    {&ep_pcmcia_ca,	&ep_cd,		 0, STAR, loc+143,    0, pv+177, 154,    0},
/* 86: pcn* at pci* dev -1 function -1 */
    {&pcn_ca,		&pcn_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 87: siop* at pci* dev -1 function -1 */
    {&siop_pci_ca,	&siop_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 88: pciide* at pci* dev -1 function -1 */
    {&pciide_pci_ca,	&pciide_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 89: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 90: cy* at pci* dev -1 function -1 */
    {&cy_pci_ca,	&cy_cd,		 1, STAR, loc+143,    0, pv+133, 134,    1},
/* 91: mtd* at pci* dev -1 function -1 */
    {&mtd_pci_ca,	&mtd_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 92: rl* at pci* dev -1 function -1 */
    {&rl_pci_ca,	&rl_cd,		 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 93: re* at pci* dev -1 function -1 */
    {&re_pci_ca,	&re_cd,		 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 94: vr* at pci* dev -1 function -1 */
    {&vr_ca,		&vr_cd,		 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 95: tl* at pci* dev -1 function -1 */
    {&tl_ca,		&tl_cd,		 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 96: txp* at pci* dev -1 function -1 */
    {&txp_ca,		&txp_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 97: sv* at pci* dev -1 function -1 */
    {&sv_ca,		&sv_cd,		 0, STAR, loc+143,    0, pv+133, 134,    0},
/* 98: bktr0 at pci* dev -1 function -1 */
    {&bktr_ca,		&bktr_cd,	 0, NORM, loc+143,    0, pv+133, 134,    0},
/* 99: xl* at pci* dev -1 function -1 */
    {&xl_pci_ca,	&xl_cd,		 0, STAR, loc+143,    0, pv+133, 134,    0},
/*100: fxp* at pci* dev -1 function -1 */
    {&fxp_pci_ca,	&fxp_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*101: em* at pci* dev -1 function -1 */
    {&em_ca,		&em_cd,		 0, STAR, loc+143,    0, pv+133, 134,    0},
/*102: xge* at pci* dev -1 function -1 */
    {&xge_ca,		&xge_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*103: dc* at pci* dev -1 function -1 */
    {&dc_pci_ca,	&dc_cd,		 0, STAR, loc+143,    0, pv+133, 134,    0},
/*104: epic* at pci* dev -1 function -1 */
    {&epic_pci_ca,	&epic_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*105: ti* at pci* dev -1 function -1 */
    {&ti_pci_ca,	&ti_cd,		 0, STAR, loc+143,    0, pv+133, 134,    0},
/*106: ne* at pci* dev -1 function -1 */
    {&ne_pci_ca,	&ne_cd,		 3, STAR, loc+143,    0, pv+133, 134,    3},
/*107: hifn* at pci* dev -1 function -1 */
    {&hifn_ca,		&hifn_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*108: ubsec* at pci* dev -1 function -1 */
    {&ubsec_ca,		&ubsec_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*109: safe* at pci* dev -1 function -1 */
    {&safe_ca,		&safe_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*110: wb* at pci* dev -1 function -1 */
    {&wb_ca,		&wb_cd,		 0, STAR, loc+143,    0, pv+133, 134,    0},
/*111: sf* at pci* dev -1 function -1 */
    {&sf_pci_ca,	&sf_cd,		 0, STAR, loc+143,    0, pv+133, 134,    0},
/*112: sis* at pci* dev -1 function -1 */
    {&sis_ca,		&sis_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*113: ste* at pci* dev -1 function -1 */
    {&ste_ca,		&ste_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*114: uhci* at pci* dev -1 function -1 */
    {&uhci_pci_ca,	&uhci_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*115: ohci* at pci* dev -1 function -1 */
    {&ohci_pci_ca,	&ohci_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*116: ehci* at pci* dev -1 function -1 */
    {&ehci_pci_ca,	&ehci_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*117: skc* at pci* dev -1 function -1 */
    {&skc_ca,		&skc_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*118: sk* at skc* */
    {&sk_ca,		&sk_cd,		 0, STAR,     loc,    0, pv+169, 156,    0},
/*119: mskc* at pci* dev -1 function -1 */
    {&mskc_ca,		&mskc_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*120: msk* at mskc* */
    {&msk_ca,		&msk_cd,	 0, STAR,     loc,    0, pv+167, 156,    0},
/*121: com* at puc* port -1 */
    {&com_puc_ca,	&com_cd,	 0, STAR, loc+144,    0, pv+151, 157,    0},
/*122: puc* at pci* dev -1 function -1 */
    {&puc_pci_ca,	&puc_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*123: wi* at pci* dev -1 function -1 */
    {&wi_pci_ca,	&wi_cd,		 0, STAR, loc+143,    0, pv+133, 134,    0},
/*124: an* at pci* dev -1 function -1 */
    {&an_pci_ca,	&an_cd,		 0, STAR, loc+143,    0, pv+133, 134,    0},
/*125: cmpci* at pci* dev -1 function -1 */
    {&cmpci_ca,		&cmpci_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*126: pcscp* at pci* dev -1 function -1 */
    {&pcscp_ca,		&pcscp_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*127: lge* at pci* dev -1 function -1 */
    {&lge_ca,		&lge_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*128: bge* at pci* dev -1 function -1 */
    {&bge_ca,		&bge_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*129: vge* at pci* dev -1 function -1 */
    {&vge_ca,		&vge_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*130: stge* at pci* dev -1 function -1 */
    {&stge_ca,		&stge_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*131: ath* at pci* dev -1 function -1 */
    {&ath_pci_ca,	&ath_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*132: atw* at pci* dev -1 function -1 */
    {&atw_pci_ca,	&atw_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*133: ral* at pci* dev -1 function -1 */
    {&ral_pci_ca,	&ral_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*134: acx* at pci* dev -1 function -1 */
    {&acx_pci_ca,	&acx_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*135: pgt* at pci* dev -1 function -1 */
    {&pgt_pci_ca,	&pgt_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*136: malo* at pci* dev -1 function -1 */
    {&malo_pci_ca,	&malo_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*137: alipm* at pci* dev -1 function -1 */
    {&alipm_ca,		&alipm_cd,	 0, DSTR, loc+143,    0, pv+133, 134,    0},
/*138: isa* at pceb*|sio* */
    {&isa_ca,		&isa_cd,	 0, STAR,     loc,    0, pv+115, 158,    0},
/*139: isadma0 at isa* port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&isadma_ca,	&isadma_cd,	 0, NORM, loc+ 14,    0, pv+173, 137,    0},
/*140: fdc0 at isa* port 0x3f0 size 0 iomem -1 iosiz 0 irq 6 drq 2 drq2 -1 */
    {&fdc_ca,		&fdc_cd,	 0, NORM, loc+  0,    0, pv+173, 137,    0},
/*141: fd* at fdc0 drive -1 */
    {&fd_ca,		&fd_cd,		 0, STAR, loc+144,    0, pv+153, 159,    0},
/*142: com* at isa* port 0x3f8 size 0 iomem -1 iosiz 0 irq 4 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 0, STAR, loc+119,    0, pv+173, 137,    0},
/*143: com* at isa* port 0x2f8 size 0 iomem -1 iosiz 0 irq 3 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 0, STAR, loc+112,    0, pv+173, 137,    0},
/*144: cy0 at isa* port -1 size 0 iomem 0xd4000 iosiz 0 irq 0xc drq -1 drq2 -1 */
    {&cy_isa_ca,	&cy_cd,		 0, NORM, loc+105,    0, pv+173, 137,    0},
/*145: pckbc* at isa* port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&pckbc_isa_ca,	&pckbc_cd,	 0, STAR, loc+ 14,    0, pv+173, 137,    0},
/*146: wdc0 at isa* port 0x1f0 size 0 iomem -1 iosiz 0 irq 0xe drq -1 drq2 -1 */
    {&wdc_isa_ca,	&wdc_cd,	 0, NORM, loc+ 98,    0, pv+173, 137,    0},
/*147: wdc1 at isa* port 0x170 size 0 iomem -1 iosiz 0 irq 0xf drq -1 drq2 -1 */
    {&wdc_isa_ca,	&wdc_cd,	 1, NORM, loc+ 91,    0, pv+173, 137,    1},
/*148: lc0 at isa* port 0x200 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&lc_isa_ca,	&lc_cd,		 0, NORM, loc+ 84,    0, pv+173, 137,    0},
/*149: lc1 at isa* port 0x280 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&lc_isa_ca,	&lc_cd,		 1, NORM, loc+ 77,    0, pv+173, 137,    1},
/*150: ne0 at isa* port 0x240 size 0 iomem -1 iosiz 0 irq 9 drq -1 drq2 -1 */
    {&ne_isa_ca,	&ne_cd,		 0, NORM, loc+ 70,    0, pv+173, 137,    0},
/*151: ne1 at isa* port 0x300 size 0 iomem -1 iosiz 0 irq 0xa drq -1 drq2 -1 */
    {&ne_isa_ca,	&ne_cd,		 1, NORM, loc+ 63,    0, pv+173, 137,    1},
/*152: ne2 at isa* port 0x280 size 0 iomem -1 iosiz 0 irq 9 drq -1 drq2 -1 */
    {&ne_isa_ca,	&ne_cd,		 2, NORM, loc+ 56,    0, pv+173, 137,    2},
/*153: we0 at isa* port 0x280 size 0 iomem 0xd0000 iosiz 0 irq 9 drq -1 drq2 -1 */
    {&we_isa_ca,	&we_cd,		 0, NORM, loc+ 49,    0, pv+173, 137,    0},
/*154: we1 at isa* port 0x300 size 0 iomem 0xcc000 iosiz 0 irq 0xa drq -1 drq2 -1 */
    {&we_isa_ca,	&we_cd,		 1, NORM, loc+ 42,    0, pv+173, 137,    1},
/*155: ec0 at isa* port 0x250 size 0 iomem 0xd8000 iosiz 0 irq 9 drq -1 drq2 -1 */
    {&ec_ca,		&ec_cd,		 0, NORM, loc+ 21,    0, pv+173, 137,    0},
/*156: pcppi* at isa* port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&pcppi_ca,		&pcppi_cd,	 0, STAR, loc+ 14,    0, pv+173, 137,    0},
/*157: spkr0 at pcppi* */
    {&spkr_ca,		&spkr_cd,	 0, NORM,     loc,    0, pv+165, 160,    0},
/*158: lpt* at isa* port 0x3bc size 0 iomem -1 iosiz 0 irq 7 drq -1 drq2 -1 */
    {&lpt_isa_ca,	&lpt_cd,	 0, STAR, loc+  7,    0, pv+173, 137,    0},
/*159: pcic0 at isa* port 0x3e0 size 0 iomem 0xd0000 iosiz 0x10000 irq -1 drq -1 drq2 -1 */
    {&pcic_isa_ca,	&pcic_cd,	 0, NORM, loc+126,    0, pv+173, 137,    0},
/*160: pcic1 at isa* port 0x3e2 size 0 iomem 0xcc000 iosiz 0x10000 irq -1 drq -1 drq2 -1 */
    {&pcic_isa_ca,	&pcic_cd,	 1, NORM, loc+ 35,    0, pv+173, 137,    1},
/*161: pcmcia* at pcic0|pcic1 controller -1 socket -1 */
    {&pcmcia_ca,	&pcmcia_cd,	 0, STAR, loc+143,    0, pv+127, 161,    0},
/*162: pckbd* at pckbc* slot -1 */
    {&pckbd_ca,		&pckbd_cd,	 0, STAR, loc+144,    0, pv+155, 167,    0},
/*163: pms* at pckbc* slot -1 */
    {&pms_ca,		&pms_cd,	 0, STAR, loc+144,    0, pv+155, 167,    0},
/*164: eisa* at pceb* */
    {&eisa_ca,		&eisa_cd,	 0, STAR,     loc,    0, pv+163, 158,    0},
/*165: apecs* at mainbus0 */
    {&apecs_ca,		&apecs_cd,	 0, STAR,     loc,    0, pv+161, 111,    0},
/*166: lca* at mainbus0 */
    {&lca_ca,		&lca_cd,	 0, STAR,     loc,    0, pv+161, 111,    0},
/*167: cia* at mainbus0 */
    {&cia_ca,		&cia_cd,	 0, STAR,     loc,    0, pv+161, 111,    0},
/*168: irongate* at mainbus0 */
    {&irongate_ca,	&irongate_cd,	 0, STAR,     loc,    0, pv+161, 111,    0},
/*169: tsc* at mainbus0 */
    {&tsc_ca,		&tsc_cd,	 0, STAR,     loc,    0, pv+161, 111,    0},
/*170: tsciic* at tsc* */
    {&tsciic_ca,	&tsciic_cd,	 0, STAR,     loc,    0, pv+159, 168,    0},
/*171: tsp* at tsc* */
    {&tsp_ca,		&tsp_cd,	 0, STAR,     loc,    0, pv+159, 168,    0},
/*172: mcpcia* at mcbus* mid -1 */
    {&mcpcia_ca,	&mcpcia_cd,	 0, STAR, loc+144,    0, pv+147, 112,    0},
/*173: sio* at pci* dev -1 function -1 */
    {&sio_ca,		&sio_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*174: pceb* at pci* dev -1 function -1 */
    {&pceb_ca,		&pceb_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*175: hme* at pci* dev -1 function -1 */
    {&hme_pci_ca,	&hme_cd,	 0, STAR, loc+143,    0, pv+133, 134,    0},
/*176: isapnp0 at isa* port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&isapnp_ca,	&isapnp_cd,	 0, NORM, loc+ 14,    0, pv+173, 137,    0},
/*177: mcclock* at ioasic* offset -1 */
    {&mcclock_ioasic_ca,	&mcclock_cd,	 0, STAR, loc+144,    0, pv+141, 106,    0},
/*178: mcclock* at isa* port 0x70 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&mcclock_isa_ca,	&mcclock_cd,	 0, STAR, loc+ 28,    0, pv+173, 137,    0},
/*179: iic* at alipm*|tsciic* */
    {&iic_ca,		&iic_cd,	 0, STAR,     loc,    0, pv+112, 168,    0},
/*180: lmtemp* at iic* addr -1 size -1 */
    {&lmtemp_ca,	&lmtemp_cd,	 0, STAR, loc+143,    0, pv+139, 169,    0},
/*181: lmenv* at iic* addr -1 size -1 */
    {&lmenv_ca,		&lmenv_cd,	 0, STAR, loc+143,    0, pv+139, 169,    0},
/*182: maxtmp* at iic* addr -1 size -1 */
    {&maxtmp_ca,	&maxtmp_cd,	 0, STAR, loc+143,    0, pv+139, 169,    0},
/*183: adc* at iic* addr -1 size -1 */
    {&adc_ca,		&adc_cd,	 0, STAR, loc+143,    0, pv+139, 169,    0},
/*184: admtemp* at iic* addr -1 size -1 */
    {&admtemp_ca,	&admtemp_cd,	 0, STAR, loc+143,    0, pv+139, 169,    0},
/*185: admlc* at iic* addr -1 size -1 */
    {&admlc_ca,		&admlc_cd,	 0, STAR, loc+143,    0, pv+139, 169,    0},
/*186: admtm* at iic* addr -1 size -1 */
    {&admtm_ca,		&admtm_cd,	 0, STAR, loc+143,    0, pv+139, 169,    0},
/*187: admtmp* at iic* addr -1 size -1 */
    {&admtmp_ca,	&admtmp_cd,	 0, STAR, loc+143,    0, pv+139, 169,    0},
/*188: admtt* at iic* addr -1 size -1 */
    {&admtt_ca,		&admtt_cd,	 0, STAR, loc+143,    0, pv+139, 169,    0},
/*189: maxds* at iic* addr -1 size -1 */
    {&maxds_ca,		&maxds_cd,	 0, STAR, loc+143,    0, pv+139, 169,    0},
/*190: adt* at iic* addr -1 size -1 */
    {&adt_ca,		&adt_cd,	 0, STAR, loc+143,    0, pv+139, 169,    0},
/*191: lm* at iic* addr -1 size -1 */
    {&lm_i2c_ca,	&lm_cd,		 0, STAR, loc+143,    0, pv+139, 169,    0},
/*192: spdmem* at iic* addr -1 size -1 */
    {&spdmem_iic_ca,	&spdmem_cd,	 0, STAR, loc+143,    0, pv+139, 169,    0},
/*193: sdtemp* at iic* addr -1 size -1 */
    {&sdtemp_ca,	&sdtemp_cd,	 0, STAR, loc+143,    0, pv+139, 169,    0},
/*194: usb* at ehci*|uhci*|ohci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+97, 171,    0},
/*195: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+181, 171,    0},
/*196: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*197: uaudio* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uaudio_ca,	&uaudio_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*198: uvideo* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvideo_ca,	&uvideo_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*199: utvfu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&utvfu_ca,		&utvfu_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*200: umidi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umidi_ca,		&umidi_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*201: ucom* at umodem*|uvisor*|uvscom*|ubsa*|uftdi*|uplcom*|umct*|uslcom*|uscom*|ucrcom*|uark*|uipaq*|uchcom*|ucycom*|uslhcom* portno -1 */
    {&ucom_ca,		&ucom_cd,	 0, STAR, loc+144,    0, pv+58, 186,    0},
/*202: ugen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugen_ca,		&ugen_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*203: uhidev* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhidev_ca,	&uhidev_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*204: uhid* at uhidev* reportid -1 */
    {&uhid_ca,		&uhid_cd,	 0, STAR, loc+144,    0, pv+157, 216,    0},
/*205: fido* at uhidev* reportid -1 */
    {&fido_ca,		&fido_cd,	 0, STAR, loc+144,    0, pv+157, 216,    0},
/*206: ujoy* at uhidev* reportid -1 */
    {&ujoy_ca,		&ujoy_cd,	 0, STAR, loc+144,    0, pv+157, 216,    0},
/*207: ukbd* at uhidev* reportid -1 */
    {&ukbd_ca,		&ukbd_cd,	 0, STAR, loc+144,    0, pv+157, 216,    0},
/*208: ums* at uhidev* reportid -1 */
    {&ums_ca,		&ums_cd,	 0, STAR, loc+144,    0, pv+157, 216,    0},
/*209: ucycom* at uhidev* reportid -1 */
    {&ucycom_ca,	&ucycom_cd,	 0, STAR, loc+144,    0, pv+157, 216,    0},
/*210: uslhcom* at uhidev* reportid -1 */
    {&uslhcom_ca,	&uslhcom_cd,	 0, STAR, loc+144,    0, pv+157, 216,    0},
/*211: ulpt* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ulpt_ca,		&ulpt_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*212: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*213: uthum* at uhidev* reportid -1 */
    {&uthum_ca,		&uthum_cd,	 0, STAR, loc+144,    0, pv+157, 216,    0},
/*214: ugold* at uhidev* reportid -1 */
    {&ugold_ca,		&ugold_cd,	 0, STAR, loc+144,    0, pv+157, 216,    0},
/*215: uonerng* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uonerng_ca,	&uonerng_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*216: urng* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urng_ca,		&urng_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*217: uvisor* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvisor_ca,	&uvisor_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*218: udsbr* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udsbr_ca,		&udsbr_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*219: utwitch* at uhidev* reportid -1 */
    {&utwitch_ca,	&utwitch_cd,	 0, STAR, loc+144,    0, pv+157, 216,    0},
/*220: aue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&aue_ca,		&aue_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*221: axe* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axe_ca,		&axe_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*222: axen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axen_ca,		&axen_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*223: smsc* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&smsc_ca,		&smsc_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*224: cue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cue_ca,		&cue_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*225: kue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&kue_ca,		&kue_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*226: cdce* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cdce_ca,		&cdce_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*227: urndis* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urndis_ca,	&urndis_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*228: udav* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udav_ca,		&udav_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*229: upl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upl_ca,		&upl_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*230: ugl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugl_ca,		&ugl_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*231: url* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&url_ca,		&url_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*232: ure* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ure_ca,		&ure_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*233: umodem* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umodem_ca,	&umodem_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*234: uftdi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uftdi_ca,		&uftdi_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*235: uplcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uplcom_ca,	&uplcom_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*236: umct* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umct_ca,		&umct_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*237: uvscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvscom_ca,	&uvscom_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*238: ubsa* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ubsa_ca,		&ubsa_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*239: uslcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uslcom_ca,	&uslcom_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*240: uark* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uark_ca,		&uark_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*241: uscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uscom_ca,		&uscom_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*242: ucrcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ucrcom_ca,	&ucrcom_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*243: uipaq* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uipaq_ca,		&uipaq_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*244: uchcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uchcom_ca,	&uchcom_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*245: wi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&wi_usb_ca,	&wi_cd,		 0, STAR, loc+133,    0, pv+109, 172,    0},
/*246: ural* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ural_ca,		&ural_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*247: rum* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rum_ca,		&rum_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*248: run* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&run_ca,		&run_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*249: uath* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uath_ca,		&uath_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*250: uow* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uow_ca,		&uow_cd,	 0, STAR, loc+133,    0, pv+109, 172,    0},
/*251: upd* at uhidev* reportid -1 */
    {&upd_ca,		&upd_cd,	 0, STAR, loc+144,    0, pv+157, 216,    0},
/*252: uhidpp* at uhidev* reportid -1 */
    {&uhidpp_ca,	&uhidpp_cd,	 0, STAR, loc+144,    0, pv+157, 216,    0},
/*253: ucc* at uhidev* reportid -1 */
    {&ucc_ca,		&ucc_cd,	 0, STAR, loc+144,    0, pv+157, 216,    0},
/*254: onewire* at uow* */
    {&onewire_ca,	&onewire_cd,	 0, STAR,     loc,    0, pv+183, 217,    0},
/*255: owid* at onewire* */
    {&owid_ca,		&owid_cd,	 0, STAR,     loc,    0, pv+179, 217,    0},
/*256: owsbm* at onewire* */
    {&owsbm_ca,		&owsbm_cd,	 0, STAR,     loc,    0, pv+179, 217,    0},
/*257: owtemp* at onewire* */
    {&owtemp_ca,	&owtemp_cd,	 0, STAR,     loc,    0, pv+179, 217,    0},
/*258: owctr* at onewire* */
    {&owctr_ca,		&owctr_cd,	 0, STAR,     loc,    0, pv+179, 217,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 4 /* vscsi0 */,
	 5 /* softraid0 */,
	 6 /* mainbus0 */,
	-1
};

int cfroots_size = 4;

/* pseudo-devices */
extern void pfattach(int);
extern void pflogattach(int);
extern void pfsyncattach(int);
extern void pflowattach(int);
extern void encattach(int);
extern void ptyattach(int);
extern void nmeaattach(int);
extern void mstsattach(int);
extern void endrunattach(int);
extern void vndattach(int);
extern void ksymsattach(int);
extern void bpfilterattach(int);
extern void bridgeattach(int);
extern void vebattach(int);
extern void carpattach(int);
extern void etheripattach(int);
extern void gifattach(int);
extern void greattach(int);
extern void loopattach(int);
extern void mpeattach(int);
extern void mpwattach(int);
extern void mpipattach(int);
extern void bpeattach(int);
extern void pairattach(int);
extern void pppattach(int);
extern void pppoeattach(int);
extern void pppxattach(int);
extern void spppattach(int);
extern void trunkattach(int);
extern void aggrattach(int);
extern void tpmrattach(int);
extern void tunattach(int);
extern void vetherattach(int);
extern void vxlanattach(int);
extern void vlanattach(int);
extern void switchattach(int);
extern void wgattach(int);
extern void bioattach(int);
extern void fuseattach(int);
extern void hotplugattach(int);
extern void wsmuxattach(int);

char *pdevnames[] = {
	"pf",
	"pflog",
	"pfsync",
	"pflow",
	"enc",
	"pty",
	"nmea",
	"msts",
	"endrun",
	"vnd",
	"ksyms",
	"bpfilter",
	"bridge",
	"veb",
	"carp",
	"etherip",
	"gif",
	"gre",
	"loop",
	"mpe",
	"mpw",
	"mpip",
	"bpe",
	"pair",
	"ppp",
	"pppoe",
	"pppx",
	"sppp",
	"trunk",
	"aggr",
	"tpmr",
	"tun",
	"vether",
	"vxlan",
	"vlan",
	"switch",
	"wg",
	"bio",
	"fuse",
	"hotplug",
	"wsmux",
};

int pdevnames_size = 41;

struct pdevinit pdevinit[] = {
	{ pfattach, 1 },
	{ pflogattach, 1 },
	{ pfsyncattach, 1 },
	{ pflowattach, 1 },
	{ encattach, 1 },
	{ ptyattach, 16 },
	{ nmeaattach, 1 },
	{ mstsattach, 1 },
	{ endrunattach, 1 },
	{ vndattach, 4 },
	{ ksymsattach, 1 },
	{ bpfilterattach, 1 },
	{ bridgeattach, 1 },
	{ vebattach, 1 },
	{ carpattach, 1 },
	{ etheripattach, 1 },
	{ gifattach, 1 },
	{ greattach, 1 },
	{ loopattach, 1 },
	{ mpeattach, 1 },
	{ mpwattach, 1 },
	{ mpipattach, 1 },
	{ bpeattach, 1 },
	{ pairattach, 1 },
	{ pppattach, 1 },
	{ pppoeattach, 1 },
	{ pppxattach, 1 },
	{ spppattach, 1 },
	{ trunkattach, 1 },
	{ aggrattach, 1 },
	{ tpmrattach, 1 },
	{ tunattach, 1 },
	{ vetherattach, 1 },
	{ vxlanattach, 1 },
	{ vlanattach, 1 },
	{ switchattach, 1 },
	{ wgattach, 1 },
	{ bioattach, 1 },
	{ fuseattach, 1 },
	{ hotplugattach, 1 },
	{ wsmuxattach, 2 },
	{ NULL, 0 }
};
