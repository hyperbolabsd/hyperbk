/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/alpha/conf/RAMDISK"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver vga_cd;
extern struct cfdriver qlw_cd;
extern struct cfdriver qla_cd;
extern struct cfdriver siop_cd;
extern struct cfdriver fxp_cd;
extern struct cfdriver dc_cd;
extern struct cfdriver com_cd;
extern struct cfdriver pckbc_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver nsphy_cd;
extern struct cfdriver nsphyter_cd;
extern struct cfdriver lxtphy_cd;
extern struct cfdriver mtdphy_cd;
extern struct cfdriver sqphy_cd;
extern struct cfdriver tqphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver dcphy_cd;
extern struct cfdriver bmtphy_cd;
extern struct cfdriver amphy_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver mcbus_cd;
extern struct cfdriver mcmem_cd;
extern struct cfdriver wd_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver tga_cd;
extern struct cfdriver de_cd;
extern struct cfdriver pciide_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver isa_cd;
extern struct cfdriver pckbd_cd;
extern struct cfdriver apecs_cd;
extern struct cfdriver lca_cd;
extern struct cfdriver cia_cd;
extern struct cfdriver mcpcia_cd;
extern struct cfdriver sio_cd;
extern struct cfdriver pceb_cd;
extern struct cfdriver mcclock_cd;

extern struct cfattach mainbus_ca;
extern struct cfattach cpu_ca;
extern struct cfattach nsphy_ca;
extern struct cfattach nsphyter_ca;
extern struct cfattach lxtphy_ca;
extern struct cfattach mtdphy_ca;
extern struct cfattach sqphy_ca;
extern struct cfattach tqphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach dcphy_ca;
extern struct cfattach bmtphy_ca;
extern struct cfattach amphy_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach sd_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach mcbus_ca;
extern struct cfattach mcmem_ca;
extern struct cfattach wd_ca;
extern struct cfattach pci_ca;
extern struct cfattach vga_pci_ca;
extern struct cfattach tga_ca;
extern struct cfattach qlw_pci_ca;
extern struct cfattach qla_pci_ca;
extern struct cfattach de_ca;
extern struct cfattach siop_pci_ca;
extern struct cfattach pciide_pci_ca;
extern struct cfattach ppb_ca;
extern struct cfattach fxp_pci_ca;
extern struct cfattach dc_pci_ca;
extern struct cfattach isa_ca;
extern struct cfattach com_isa_ca;
extern struct cfattach pckbc_isa_ca;
extern struct cfattach vga_isa_ca;
extern struct cfattach pckbd_ca;
extern struct cfattach apecs_ca;
extern struct cfattach lca_ca;
extern struct cfattach cia_ca;
extern struct cfattach mcpcia_ca;
extern struct cfattach sio_ca;
extern struct cfattach pceb_ca;
extern struct cfattach mcclock_isa_ca;


/* locators */
static long loc[31] = {
	0x70, 0, -1, 0, -1, -1, -1, -1,
	0, -1, 0, -1, -1, -1, 0x2f8, 0,
	-1, 0, 3, -1, -1, 0x3f8, 0, -1,
	0, 4, -1, -1, -1, -1, 1,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"phy",
	"target",
	"lun",
	"console",
	"primary",
	"mux",
	"mid",
	"channel",
	"drive",
	"bus",
	"dev",
	"function",
	"port",
	"size",
	"iomem",
	"iosiz",
	"irq",
	"drq",
	"drq2",
	"slot",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, -1, 0, -1, 1, 2, -1,
	3, 4, 5, -1, 3, 4, 5, -1,
	3, 4, 5, -1, 3, 5, -1, 6,
	-1, 7, 8, -1, 9, -1, 9, -1,
	9, -1, 9, -1, 9, -1, 10, 11,
	-1, 12, 13, 14, 15, 16, 17, 18,
	-1, 19, -1,
};

/* size of parent vectors */
int pv_size = 36;

/* parent vectors */
short pv[36] = {
	36, 38, 37, 28, 39, -1, 20, 21, 22, -1, 26, 24, 23, -1, 30, 29,
	-1, 41, 40, -1, 19, -1, 34, -1, 16, -1, 0, -1, 31, -1, 12, -1,
	35, -1, 27, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+ 5, 0,    0},
/*  1: cpu* at mainbus0 */
    {&cpu_ca,		&cpu_cd,	 0, STAR,     loc,    0, pv+26, 22,    0},
/*  2: nsphy* at dc*|fxp* phy -1 */
    {&nsphy_ca,		&nsphy_cd,	 0, STAR, loc+ 27,    0, pv+14, 1,    0},
/*  3: nsphyter* at dc*|fxp* phy -1 */
    {&nsphyter_ca,	&nsphyter_cd,	 0, STAR, loc+ 27,    0, pv+14, 1,    0},
/*  4: lxtphy* at dc*|fxp* phy -1 */
    {&lxtphy_ca,	&lxtphy_cd,	 0, STAR, loc+ 27,    0, pv+14, 1,    0},
/*  5: mtdphy* at dc*|fxp* phy -1 */
    {&mtdphy_ca,	&mtdphy_cd,	 0, STAR, loc+ 27,    0, pv+14, 1,    0},
/*  6: sqphy* at dc*|fxp* phy -1 */
    {&sqphy_ca,		&sqphy_cd,	 0, STAR, loc+ 27,    0, pv+14, 1,    0},
/*  7: tqphy* at dc*|fxp* phy -1 */
    {&tqphy_ca,		&tqphy_cd,	 0, STAR, loc+ 27,    0, pv+14, 1,    0},
/*  8: ukphy* at dc*|fxp* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+ 27,    0, pv+14, 1,    0},
/*  9: dcphy* at dc*|fxp* phy -1 */
    {&dcphy_ca,		&dcphy_cd,	 0, STAR, loc+ 27,    0, pv+14, 1,    0},
/* 10: bmtphy* at dc*|fxp* phy -1 */
    {&bmtphy_ca,	&bmtphy_cd,	 0, STAR, loc+ 27,    0, pv+14, 1,    0},
/* 11: amphy* at dc*|fxp* phy -1 */
    {&amphy_ca,		&amphy_cd,	 0, STAR, loc+ 27,    0, pv+14, 1,    0},
/* 12: scsibus* at siop*|qla*|qlw* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+10, 4,    0},
/* 13: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+ 26,    0, pv+30, 5,    0},
/* 14: wsdisplay* at vga*|vga*|tga* console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+ 28,    0, pv+ 6, 8,    0},
/* 15: wskbd* at pckbd* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+ 29,    0, pv+32, 20,    0},
/* 16: mcbus* at mainbus0 */
    {&mcbus_ca,		&mcbus_cd,	 0, STAR,     loc,    0, pv+26, 22,    0},
/* 17: mcmem* at mcbus* mid -1 */
    {&mcmem_ca,		&mcmem_cd,	 0, STAR, loc+ 27,    0, pv+24, 23,    0},
/* 18: wd* at pciide* channel -1 drive -1 */
    {&wd_ca,		&wd_cd,		 0, STAR, loc+ 26,    0, pv+34, 25,    0},
/* 19: pci* at apecs*|cia*|lca*|ppb*|mcpcia* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+ 27,    0, pv+ 0, 28,    0},
/* 20: vga* at pci* dev -1 function -1 */
    {&vga_pci_ca,	&vga_cd,	 0, STAR, loc+ 26,    0, pv+20, 38,    0},
/* 21: vga* at isa* port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&vga_isa_ca,	&vga_cd,	 0, STAR, loc+  7,    0, pv+28, 41,    0},
/* 22: tga* at pci* dev -1 function -1 */
    {&tga_ca,		&tga_cd,	 0, STAR, loc+ 26,    0, pv+20, 38,    0},
/* 23: qlw* at pci* dev -1 function -1 */
    {&qlw_pci_ca,	&qlw_cd,	 0, STAR, loc+ 26,    0, pv+20, 38,    0},
/* 24: qla* at pci* dev -1 function -1 */
    {&qla_pci_ca,	&qla_cd,	 0, STAR, loc+ 26,    0, pv+20, 38,    0},
/* 25: de* at pci* dev -1 function -1 */
    {&de_ca,		&de_cd,		 0, STAR, loc+ 26,    0, pv+20, 38,    0},
/* 26: siop* at pci* dev -1 function -1 */
    {&siop_pci_ca,	&siop_cd,	 0, STAR, loc+ 26,    0, pv+20, 38,    0},
/* 27: pciide* at pci* dev -1 function -1 */
    {&pciide_pci_ca,	&pciide_cd,	 0, STAR, loc+ 26,    0, pv+20, 38,    0},
/* 28: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+ 26,    0, pv+20, 38,    0},
/* 29: fxp* at pci* dev -1 function -1 */
    {&fxp_pci_ca,	&fxp_cd,	 0, STAR, loc+ 26,    0, pv+20, 38,    0},
/* 30: dc* at pci* dev -1 function -1 */
    {&dc_pci_ca,	&dc_cd,		 0, STAR, loc+ 26,    0, pv+20, 38,    0},
/* 31: isa* at pceb*|sio* */
    {&isa_ca,		&isa_cd,	 0, STAR,     loc,    0, pv+17, 48,    0},
/* 32: com* at isa* port 0x3f8 size 0 iomem -1 iosiz 0 irq 4 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 0, STAR, loc+ 21,    0, pv+28, 41,    0},
/* 33: com* at isa* port 0x2f8 size 0 iomem -1 iosiz 0 irq 3 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 0, STAR, loc+ 14,    0, pv+28, 41,    0},
/* 34: pckbc* at isa* port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&pckbc_isa_ca,	&pckbc_cd,	 0, STAR, loc+  7,    0, pv+28, 41,    0},
/* 35: pckbd* at pckbc* slot -1 */
    {&pckbd_ca,		&pckbd_cd,	 0, STAR, loc+ 27,    0, pv+22, 49,    0},
/* 36: apecs* at mainbus0 */
    {&apecs_ca,		&apecs_cd,	 0, STAR,     loc,    0, pv+26, 22,    0},
/* 37: lca* at mainbus0 */
    {&lca_ca,		&lca_cd,	 0, STAR,     loc,    0, pv+26, 22,    0},
/* 38: cia* at mainbus0 */
    {&cia_ca,		&cia_cd,	 0, STAR,     loc,    0, pv+26, 22,    0},
/* 39: mcpcia* at mcbus* mid -1 */
    {&mcpcia_ca,	&mcpcia_cd,	 0, STAR, loc+ 27,    0, pv+24, 23,    0},
/* 40: sio* at pci* dev -1 function -1 */
    {&sio_ca,		&sio_cd,	 0, STAR, loc+ 26,    0, pv+20, 38,    0},
/* 41: pceb* at pci* dev -1 function -1 */
    {&pceb_ca,		&pceb_cd,	 0, STAR, loc+ 26,    0, pv+20, 38,    0},
/* 42: mcclock* at isa* port 0x70 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&mcclock_isa_ca,	&mcclock_cd,	 0, STAR, loc+  0,    0, pv+28, 41,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 0 /* mainbus0 */,
	-1
};

int cfroots_size = 2;

/* pseudo-devices */
extern void bpfilterattach(int);
extern void loopattach(int);
extern void rdattach(int);

char *pdevnames[] = {
	"bpfilter",
	"loop",
	"rd",
};

int pdevnames_size = 3;

struct pdevinit pdevinit[] = {
	{ bpfilterattach, 1 },
	{ loopattach, 1 },
	{ rdattach, 1 },
	{ NULL, 0 }
};
