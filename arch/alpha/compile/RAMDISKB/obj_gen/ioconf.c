/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/alpha/conf/RAMDISKB"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver vga_cd;
extern struct cfdriver ahc_cd;
extern struct cfdriver qlw_cd;
extern struct cfdriver qla_cd;
extern struct cfdriver siop_cd;
extern struct cfdriver dc_cd;
extern struct cfdriver com_cd;
extern struct cfdriver pckbc_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver lxtphy_cd;
extern struct cfdriver mtdphy_cd;
extern struct cfdriver sqphy_cd;
extern struct cfdriver tqphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver dcphy_cd;
extern struct cfdriver bmtphy_cd;
extern struct cfdriver amphy_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver wd_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver tga_cd;
extern struct cfdriver de_cd;
extern struct cfdriver pciide_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver isa_cd;
extern struct cfdriver pckbd_cd;
extern struct cfdriver irongate_cd;
extern struct cfdriver tsc_cd;
extern struct cfdriver tsp_cd;
extern struct cfdriver sio_cd;
extern struct cfdriver pceb_cd;
extern struct cfdriver mcclock_cd;

extern struct cfattach mainbus_ca;
extern struct cfattach cpu_ca;
extern struct cfattach lxtphy_ca;
extern struct cfattach mtdphy_ca;
extern struct cfattach sqphy_ca;
extern struct cfattach tqphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach dcphy_ca;
extern struct cfattach bmtphy_ca;
extern struct cfattach amphy_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach sd_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach wd_ca;
extern struct cfattach pci_ca;
extern struct cfattach vga_pci_ca;
extern struct cfattach tga_ca;
extern struct cfattach ahc_pci_ca;
extern struct cfattach qlw_pci_ca;
extern struct cfattach qla_pci_ca;
extern struct cfattach de_ca;
extern struct cfattach siop_pci_ca;
extern struct cfattach pciide_pci_ca;
extern struct cfattach ppb_ca;
extern struct cfattach dc_pci_ca;
extern struct cfattach isa_ca;
extern struct cfattach com_isa_ca;
extern struct cfattach pckbc_isa_ca;
extern struct cfattach pckbd_ca;
extern struct cfattach irongate_ca;
extern struct cfattach tsc_ca;
extern struct cfattach tsp_ca;
extern struct cfattach sio_ca;
extern struct cfattach pceb_ca;
extern struct cfattach mcclock_isa_ca;


/* locators */
static long loc[31] = {
	0x70, 0, -1, 0, -1, -1, -1, -1,
	0, -1, 0, -1, -1, -1, 0x2f8, 0,
	-1, 0, 3, -1, -1, 0x3f8, 0, -1,
	0, 4, -1, -1, -1, -1, 1,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"phy",
	"target",
	"lun",
	"console",
	"primary",
	"mux",
	"channel",
	"drive",
	"bus",
	"dev",
	"function",
	"port",
	"size",
	"iomem",
	"iosiz",
	"irq",
	"drq",
	"drq2",
	"slot",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, -1, 1, 2, -1, 3, 4,
	5, -1, 3, 4, 5, -1, 3, 5,
	-1, 6, 7, -1, 8, -1, 8, -1,
	8, -1, 9, 10, -1, 11, 12, 13,
	14, 15, 16, 17, -1, 18, -1,
};

/* size of parent vectors */
int pv_size = 33;

/* parent vectors */
short pv[33] = {
	22, 20, 19, 18, -1, 31, 33, 24, -1, 35, 34, -1, 16, 17, -1, 15,
	-1, 10, -1, 30, -1, 23, -1, 25, -1, 29, -1, 0, -1, 32, -1, 26,
	-1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+ 4, 0,    0},
/*  1: cpu* at mainbus0 */
    {&cpu_ca,		&cpu_cd,	 0, STAR,     loc,    0, pv+27, 38,    0},
/*  2: lxtphy* at dc* phy -1 */
    {&lxtphy_ca,	&lxtphy_cd,	 0, STAR, loc+ 27,    0, pv+23, 1,    0},
/*  3: mtdphy* at dc* phy -1 */
    {&mtdphy_ca,	&mtdphy_cd,	 0, STAR, loc+ 27,    0, pv+23, 1,    0},
/*  4: sqphy* at dc* phy -1 */
    {&sqphy_ca,		&sqphy_cd,	 0, STAR, loc+ 27,    0, pv+23, 1,    0},
/*  5: tqphy* at dc* phy -1 */
    {&tqphy_ca,		&tqphy_cd,	 0, STAR, loc+ 27,    0, pv+23, 1,    0},
/*  6: ukphy* at dc* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+ 27,    0, pv+23, 1,    0},
/*  7: dcphy* at dc* phy -1 */
    {&dcphy_ca,		&dcphy_cd,	 0, STAR, loc+ 27,    0, pv+23, 1,    0},
/*  8: bmtphy* at dc* phy -1 */
    {&bmtphy_ca,	&bmtphy_cd,	 0, STAR, loc+ 27,    0, pv+23, 1,    0},
/*  9: amphy* at dc* phy -1 */
    {&amphy_ca,		&amphy_cd,	 0, STAR, loc+ 27,    0, pv+23, 1,    0},
/* 10: scsibus* at siop*|qla*|qlw*|ahc* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+ 0, 2,    0},
/* 11: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+ 26,    0, pv+17, 3,    0},
/* 12: wsdisplay* at vga*|tga* console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+ 28,    0, pv+12, 6,    0},
/* 13: wskbd* at pckbd* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+ 29,    0, pv+19, 14,    0},
/* 14: wd* at pciide* channel -1 drive -1 */
    {&wd_ca,		&wd_cd,		 0, STAR, loc+ 26,    0, pv+21, 17,    0},
/* 15: pci* at irongate*|tsp*|ppb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+ 27,    0, pv+ 5, 20,    0},
/* 16: vga* at pci* dev -1 function -1 */
    {&vga_pci_ca,	&vga_cd,	 0, STAR, loc+ 26,    0, pv+15, 26,    0},
/* 17: tga* at pci* dev -1 function -1 */
    {&tga_ca,		&tga_cd,	 0, STAR, loc+ 26,    0, pv+15, 26,    0},
/* 18: ahc* at pci* dev -1 function -1 */
    {&ahc_pci_ca,	&ahc_cd,	 0, STAR, loc+ 26,    0, pv+15, 26,    0},
/* 19: qlw* at pci* dev -1 function -1 */
    {&qlw_pci_ca,	&qlw_cd,	 0, STAR, loc+ 26,    0, pv+15, 26,    0},
/* 20: qla* at pci* dev -1 function -1 */
    {&qla_pci_ca,	&qla_cd,	 0, STAR, loc+ 26,    0, pv+15, 26,    0},
/* 21: de* at pci* dev -1 function -1 */
    {&de_ca,		&de_cd,		 0, STAR, loc+ 26,    0, pv+15, 26,    0},
/* 22: siop* at pci* dev -1 function -1 */
    {&siop_pci_ca,	&siop_cd,	 0, STAR, loc+ 26,    0, pv+15, 26,    0},
/* 23: pciide* at pci* dev -1 function -1 */
    {&pciide_pci_ca,	&pciide_cd,	 0, STAR, loc+ 26,    0, pv+15, 26,    0},
/* 24: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+ 26,    0, pv+15, 26,    0},
/* 25: dc* at pci* dev -1 function -1 */
    {&dc_pci_ca,	&dc_cd,		 0, STAR, loc+ 26,    0, pv+15, 26,    0},
/* 26: isa* at pceb*|sio* */
    {&isa_ca,		&isa_cd,	 0, STAR,     loc,    0, pv+ 9, 28,    0},
/* 27: com* at isa* port 0x3f8 size 0 iomem -1 iosiz 0 irq 4 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 0, STAR, loc+ 21,    0, pv+31, 29,    0},
/* 28: com* at isa* port 0x2f8 size 0 iomem -1 iosiz 0 irq 3 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 0, STAR, loc+ 14,    0, pv+31, 29,    0},
/* 29: pckbc* at isa* port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&pckbc_isa_ca,	&pckbc_cd,	 0, STAR, loc+  7,    0, pv+31, 29,    0},
/* 30: pckbd* at pckbc* slot -1 */
    {&pckbd_ca,		&pckbd_cd,	 0, STAR, loc+ 27,    0, pv+25, 37,    0},
/* 31: irongate* at mainbus0 */
    {&irongate_ca,	&irongate_cd,	 0, STAR,     loc,    0, pv+27, 38,    0},
/* 32: tsc* at mainbus0 */
    {&tsc_ca,		&tsc_cd,	 0, STAR,     loc,    0, pv+27, 38,    0},
/* 33: tsp* at tsc* */
    {&tsp_ca,		&tsp_cd,	 0, STAR,     loc,    0, pv+29, 38,    0},
/* 34: sio* at pci* dev -1 function -1 */
    {&sio_ca,		&sio_cd,	 0, STAR, loc+ 26,    0, pv+15, 26,    0},
/* 35: pceb* at pci* dev -1 function -1 */
    {&pceb_ca,		&pceb_cd,	 0, STAR, loc+ 26,    0, pv+15, 26,    0},
/* 36: mcclock* at isa* port 0x70 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&mcclock_isa_ca,	&mcclock_cd,	 0, STAR, loc+  0,    0, pv+31, 29,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 0 /* mainbus0 */,
	-1
};

int cfroots_size = 2;

/* pseudo-devices */
extern void loopattach(int);
extern void rdattach(int);

char *pdevnames[] = {
	"loop",
	"rd",
};

int pdevnames_size = 2;

struct pdevinit pdevinit[] = {
	{ loopattach, 1 },
	{ rdattach, 1 },
	{ NULL, 0 }
};
