/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/alpha/conf/RAMDISKBIG"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver vga_cd;
extern struct cfdriver ahc_cd;
extern struct cfdriver adw_cd;
extern struct cfdriver gdt_cd;
extern struct cfdriver twe_cd;
extern struct cfdriver ciss_cd;
extern struct cfdriver qlw_cd;
extern struct cfdriver qla_cd;
extern struct cfdriver siop_cd;
extern struct cfdriver ep_cd;
extern struct cfdriver lc_cd;
extern struct cfdriver wi_cd;
extern struct cfdriver an_cd;
extern struct cfdriver le_cd;
extern struct cfdriver xl_cd;
extern struct cfdriver fxp_cd;
extern struct cfdriver mtd_cd;
extern struct cfdriver rl_cd;
extern struct cfdriver re_cd;
extern struct cfdriver dc_cd;
extern struct cfdriver epic_cd;
extern struct cfdriver ne_cd;
extern struct cfdriver ti_cd;
extern struct cfdriver com_cd;
extern struct cfdriver pckbc_cd;
extern struct cfdriver ral_cd;
extern struct cfdriver sf_cd;
extern struct cfdriver uhci_cd;
extern struct cfdriver ohci_cd;
extern struct cfdriver ehci_cd;
extern struct cfdriver softraid_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver tlphy_cd;
extern struct cfdriver nsphy_cd;
extern struct cfdriver nsphyter_cd;
extern struct cfdriver qsphy_cd;
extern struct cfdriver inphy_cd;
extern struct cfdriver iophy_cd;
extern struct cfdriver eephy_cd;
extern struct cfdriver exphy_cd;
extern struct cfdriver rlphy_cd;
extern struct cfdriver lxtphy_cd;
extern struct cfdriver luphy_cd;
extern struct cfdriver mtdphy_cd;
extern struct cfdriver icsphy_cd;
extern struct cfdriver sqphy_cd;
extern struct cfdriver tqphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver dcphy_cd;
extern struct cfdriver bmtphy_cd;
extern struct cfdriver brgphy_cd;
extern struct cfdriver xmphy_cd;
extern struct cfdriver amphy_cd;
extern struct cfdriver acphy_cd;
extern struct cfdriver urlphy_cd;
extern struct cfdriver rgephy_cd;
extern struct cfdriver ciphy_cd;
extern struct cfdriver ipgphy_cd;
extern struct cfdriver mlphy_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver cd_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver st_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver tc_cd;
extern struct cfdriver ioasic_cd;
extern struct cfdriver tcds_cd;
extern struct cfdriver asc_cd;
extern struct cfdriver zs_cd;
extern struct cfdriver zstty_cd;
extern struct cfdriver mcbus_cd;
extern struct cfdriver mcmem_cd;
extern struct cfdriver tcasic_cd;
extern struct cfdriver atapiscsi_cd;
extern struct cfdriver wd_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver tga_cd;
extern struct cfdriver qle_cd;
extern struct cfdriver de_cd;
extern struct cfdriver pcn_cd;
extern struct cfdriver pciide_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver vr_cd;
extern struct cfdriver tl_cd;
extern struct cfdriver txp_cd;
extern struct cfdriver em_cd;
extern struct cfdriver xge_cd;
extern struct cfdriver wb_cd;
extern struct cfdriver sis_cd;
extern struct cfdriver ste_cd;
extern struct cfdriver skc_cd;
extern struct cfdriver sk_cd;
extern struct cfdriver mskc_cd;
extern struct cfdriver msk_cd;
extern struct cfdriver pcscp_cd;
extern struct cfdriver lge_cd;
extern struct cfdriver bge_cd;
extern struct cfdriver vge_cd;
extern struct cfdriver stge_cd;
extern struct cfdriver isa_cd;
extern struct cfdriver isadma_cd;
extern struct cfdriver pckbd_cd;
extern struct cfdriver eisa_cd;
extern struct cfdriver apecs_cd;
extern struct cfdriver lca_cd;
extern struct cfdriver cia_cd;
extern struct cfdriver irongate_cd;
extern struct cfdriver tsc_cd;
extern struct cfdriver tsp_cd;
extern struct cfdriver mcpcia_cd;
extern struct cfdriver sio_cd;
extern struct cfdriver pceb_cd;
extern struct cfdriver hme_cd;
extern struct cfdriver isapnp_cd;
extern struct cfdriver mcclock_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver uhidev_cd;
extern struct cfdriver uhid_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver aue_cd;
extern struct cfdriver axe_cd;
extern struct cfdriver axen_cd;
extern struct cfdriver smsc_cd;
extern struct cfdriver cue_cd;
extern struct cfdriver kue_cd;
extern struct cfdriver cdce_cd;
extern struct cfdriver udav_cd;
extern struct cfdriver upl_cd;
extern struct cfdriver ugl_cd;
extern struct cfdriver url_cd;
extern struct cfdriver ure_cd;

extern struct cfattach softraid_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach cpu_ca;
extern struct cfattach tlphy_ca;
extern struct cfattach nsphy_ca;
extern struct cfattach nsphyter_ca;
extern struct cfattach qsphy_ca;
extern struct cfattach inphy_ca;
extern struct cfattach iophy_ca;
extern struct cfattach eephy_ca;
extern struct cfattach exphy_ca;
extern struct cfattach rlphy_ca;
extern struct cfattach lxtphy_ca;
extern struct cfattach luphy_ca;
extern struct cfattach mtdphy_ca;
extern struct cfattach icsphy_ca;
extern struct cfattach sqphy_ca;
extern struct cfattach tqphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach dcphy_ca;
extern struct cfattach bmtphy_ca;
extern struct cfattach brgphy_ca;
extern struct cfattach xmphy_ca;
extern struct cfattach amphy_ca;
extern struct cfattach acphy_ca;
extern struct cfattach urlphy_ca;
extern struct cfattach rgephy_ca;
extern struct cfattach ciphy_ca;
extern struct cfattach ipgphy_ca;
extern struct cfattach mlphy_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach cd_ca;
extern struct cfattach sd_ca;
extern struct cfattach st_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach ioasic_ca;
extern struct cfattach le_ioasic_ca;
extern struct cfattach le_tc_ca;
extern struct cfattach tcds_ca;
extern struct cfattach asc_tc_ca;
extern struct cfattach asc_tcds_ca;
extern struct cfattach zs_ioasic_ca;
extern struct cfattach zstty_ca;
extern struct cfattach mcbus_ca;
extern struct cfattach mcmem_ca;
extern struct cfattach tc_ca;
extern struct cfattach tcasic_ca;
extern struct cfattach atapiscsi_ca;
extern struct cfattach wd_ca;
extern struct cfattach pci_ca;
extern struct cfattach vga_pci_ca;
extern struct cfattach tga_ca;
extern struct cfattach ahc_pci_ca;
extern struct cfattach adw_pci_ca;
extern struct cfattach twe_pci_ca;
extern struct cfattach gdt_pci_ca;
extern struct cfattach ciss_pci_ca;
extern struct cfattach qlw_pci_ca;
extern struct cfattach qla_pci_ca;
extern struct cfattach qle_ca;
extern struct cfattach de_ca;
extern struct cfattach ep_pci_ca;
extern struct cfattach pcn_ca;
extern struct cfattach siop_pci_ca;
extern struct cfattach pciide_pci_ca;
extern struct cfattach ppb_ca;
extern struct cfattach mtd_pci_ca;
extern struct cfattach rl_pci_ca;
extern struct cfattach re_pci_ca;
extern struct cfattach vr_ca;
extern struct cfattach tl_ca;
extern struct cfattach txp_ca;
extern struct cfattach xl_pci_ca;
extern struct cfattach fxp_pci_ca;
extern struct cfattach em_ca;
extern struct cfattach xge_ca;
extern struct cfattach dc_pci_ca;
extern struct cfattach epic_pci_ca;
extern struct cfattach ti_pci_ca;
extern struct cfattach ne_pci_ca;
extern struct cfattach wb_ca;
extern struct cfattach sf_pci_ca;
extern struct cfattach sis_ca;
extern struct cfattach ste_ca;
extern struct cfattach uhci_pci_ca;
extern struct cfattach ohci_pci_ca;
extern struct cfattach ehci_pci_ca;
extern struct cfattach skc_ca;
extern struct cfattach sk_ca;
extern struct cfattach mskc_ca;
extern struct cfattach msk_ca;
extern struct cfattach an_pci_ca;
extern struct cfattach pcscp_ca;
extern struct cfattach lge_ca;
extern struct cfattach bge_ca;
extern struct cfattach vge_ca;
extern struct cfattach stge_ca;
extern struct cfattach ral_pci_ca;
extern struct cfattach isa_ca;
extern struct cfattach isadma_ca;
extern struct cfattach com_isa_ca;
extern struct cfattach pckbc_isa_ca;
extern struct cfattach vga_isa_ca;
extern struct cfattach lc_isa_ca;
extern struct cfattach ep_isa_ca;
extern struct cfattach pckbd_ca;
extern struct cfattach eisa_ca;
extern struct cfattach ahc_eisa_ca;
extern struct cfattach apecs_ca;
extern struct cfattach lca_ca;
extern struct cfattach cia_ca;
extern struct cfattach irongate_ca;
extern struct cfattach tsc_ca;
extern struct cfattach tsp_ca;
extern struct cfattach mcpcia_ca;
extern struct cfattach sio_ca;
extern struct cfattach pceb_ca;
extern struct cfattach hme_pci_ca;
extern struct cfattach isapnp_ca;
extern struct cfattach ep_isapnp_ca;
extern struct cfattach mcclock_ioasic_ca;
extern struct cfattach mcclock_isa_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach uhidev_ca;
extern struct cfattach uhid_ca;
extern struct cfattach umass_ca;
extern struct cfattach aue_ca;
extern struct cfattach axe_ca;
extern struct cfattach axen_ca;
extern struct cfattach smsc_ca;
extern struct cfattach cue_ca;
extern struct cfattach kue_ca;
extern struct cfattach cdce_ca;
extern struct cfattach udav_ca;
extern struct cfattach upl_ca;
extern struct cfattach ugl_ca;
extern struct cfattach url_ca;
extern struct cfattach ure_ca;
extern struct cfattach wi_usb_ca;


/* locators */
static long loc[57] = {
	-1, 0, -1, 0, -1, -1, -1, 0x280,
	0, -1, 0, -1, -1, -1, 0x200, 0,
	-1, 0, -1, -1, -1, 0x2f8, 0, -1,
	0, 3, -1, -1, 0x3f8, 0, -1, 0,
	4, -1, -1, 0x70, 0, -1, 0, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, 0, -1, 0, -1, -1, -1, -1,
	1,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"phy",
	"target",
	"lun",
	"console",
	"primary",
	"mux",
	"slot",
	"offset",
	"chip",
	"channel",
	"mid",
	"bus",
	"dev",
	"function",
	"port",
	"size",
	"iomem",
	"iosiz",
	"irq",
	"drq",
	"drq2",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"reportid",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 1, 2, -1, 3, 4,
	5, -1, 3, 4, 5, -1, 3, 4,
	5, -1, 3, 5, -1, 6, 7, -1,
	7, -1, 8, -1, 9, -1, 10, -1,
	9, -1, 11, -1, 11, -1, 11, -1,
	11, -1, 11, -1, 11, -1, 11, -1,
	12, 13, -1, 14, 15, 16, 17, 18,
	19, 20, -1, 6, -1, 14, 15, 16,
	17, 18, 19, -1, 6, -1, 14, 21,
	22, 23, 24, 25, -1, 14, 21, 22,
	23, 24, 25, -1, 26, -1,
};

/* size of parent vectors */
int pv_size = 115;

/* parent vectors */
short pv[115] = {
	142, 141, 138, 134, 133, 132, 131, 121, 101, 100, 99, 98, 95, 93, 88, 87,
	85, 75, 74, 67, 86, 83, 84, 82, 81, 73, 72, 71, 78, 77, 64, 65,
	66, -1, 130, 97, 62, 48, 41, 40, 0, 68, 61, 60, 59, 57, 58, 56,
	54, 55, -1, 112, 114, 115, 113, 70, 117, 118, -1, 91, 89, 90, -1, 51,
	52, 53, -1, 126, 127, -1, 120, 119, -1, 50, -1, 103, -1, 111, -1, 107,
	-1, 44, -1, 36, -1, 128, -1, 39, -1, 42, -1, 69, -1, 1, -1, 94,
	-1, 116, -1, 125, -1, 92, -1, 120, -1, 122, -1, 30, -1, 110, -1, 46,
	-1, 47, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: softraid0 at root */
    {&softraid_ca,	&softraid_cd,	 0, NORM,     loc,    0, pv+33, 0,    0},
/*  1: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+33, 0,    0},
/*  2: cpu* at mainbus0 */
    {&cpu_ca,		&cpu_cd,	 0, STAR,     loc,    0, pv+93, 93,    0},
/*  3: tlphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&tlphy_ca,		&tlphy_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/*  4: nsphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&nsphy_ca,		&nsphy_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/*  5: nsphyter* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&nsphyter_ca,	&nsphyter_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/*  6: qsphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&qsphy_ca,		&qsphy_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/*  7: inphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&inphy_ca,		&inphy_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/*  8: iophy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&iophy_ca,		&iophy_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/*  9: eephy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&eephy_ca,		&eephy_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/* 10: exphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&exphy_ca,		&exphy_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/* 11: rlphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&rlphy_ca,		&rlphy_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/* 12: lxtphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&lxtphy_ca,	&lxtphy_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/* 13: luphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&luphy_ca,		&luphy_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/* 14: mtdphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&mtdphy_ca,	&mtdphy_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/* 15: icsphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&icsphy_ca,	&icsphy_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/* 16: sqphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&sqphy_ca,		&sqphy_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/* 17: tqphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&tqphy_ca,		&tqphy_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/* 18: ukphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/* 19: dcphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&dcphy_ca,		&dcphy_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/* 20: bmtphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&bmtphy_ca,	&bmtphy_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/* 21: brgphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&brgphy_ca,	&brgphy_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/* 22: xmphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&xmphy_ca,		&xmphy_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/* 23: amphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&amphy_ca,		&amphy_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/* 24: acphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&acphy_ca,		&acphy_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/* 25: urlphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&urlphy_ca,	&urlphy_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/* 26: rgephy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&rgephy_ca,	&rgephy_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/* 27: ciphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&ciphy_ca,		&ciphy_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/* 28: ipgphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&ipgphy_ca,	&ipgphy_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/* 29: mlphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|vge*|bge*|lge*|msk*|sk*|ste*|sis*|wb*|tl*|vr*|pcn*|sf*|ti*|ne*|epic*|dc*|re*|rl*|mtd*|fxp*|xl*|ep*|ep*|ep* phy -1 */
    {&mlphy_ca,		&mlphy_cd,	 0, STAR, loc+ 53,    0, pv+ 0, 1,    0},
/* 30: scsibus* at umass*|pcscp*|qle*|atapiscsi*|asc*|asc*|softraid0|siop*|qla*|qlw*|ciss*|twe*|gdt*|adw*|ahc*|ahc* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+34, 66,    0},
/* 31: cd* at scsibus* target -1 lun -1 */
    {&cd_ca,		&cd_cd,		 0, STAR, loc+ 52,    0, pv+107, 67,    0},
/* 32: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+ 52,    0, pv+107, 67,    0},
/* 33: st* at scsibus* target -1 lun -1 */
    {&st_ca,		&st_cd,		 0, STAR, loc+ 52,    0, pv+107, 67,    0},
/* 34: wsdisplay* at vga*|vga*|tga* console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+ 54,    0, pv+63, 70,    0},
/* 35: wskbd* at pckbd* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+ 55,    0, pv+109, 82,    0},
/* 36: ioasic* at tc* slot -1 offset -1 */
    {&ioasic_ca,	&ioasic_cd,	 0, STAR, loc+ 52,    0, pv+111, 85,    0},
/* 37: le* at ioasic* offset -1 */
    {&le_ioasic_ca,	&le_cd,		 0, STAR, loc+ 53,    0, pv+83, 88,    0},
/* 38: le* at tc* slot -1 offset -1 */
    {&le_tc_ca,		&le_cd,		 0, STAR, loc+ 52,    0, pv+111, 85,    0},
/* 39: tcds* at tc* slot -1 offset -1 */
    {&tcds_ca,		&tcds_cd,	 0, STAR, loc+ 52,    0, pv+111, 85,    0},
/* 40: asc* at tc* slot -1 offset -1 */
    {&asc_tc_ca,	&asc_cd,	 0, STAR, loc+ 52,    0, pv+111, 85,    0},
/* 41: asc* at tcds* chip -1 */
    {&asc_tcds_ca,	&asc_cd,	 0, STAR, loc+ 53,    0, pv+87, 90,    0},
/* 42: zs* at ioasic* offset -1 */
    {&zs_ioasic_ca,	&zs_cd,		 0, STAR, loc+ 53,    0, pv+83, 88,    0},
/* 43: zstty* at zs* channel 1 */
    {&zstty_ca,		&zstty_cd,	 0, STAR, loc+ 56,    0, pv+89, 92,    0},
/* 44: mcbus* at mainbus0 */
    {&mcbus_ca,		&mcbus_cd,	 0, STAR,     loc,    0, pv+93, 93,    0},
/* 45: mcmem* at mcbus* mid -1 */
    {&mcmem_ca,		&mcmem_cd,	 0, STAR, loc+ 53,    0, pv+81, 94,    0},
/* 46: tc* at tcasic* */
    {&tc_ca,		&tc_cd,		 0, STAR,     loc,    0, pv+113, 95,    0},
/* 47: tcasic* at mainbus0 */
    {&tcasic_ca,	&tcasic_cd,	 0, STAR,     loc,    0, pv+93, 93,    0},
/* 48: atapiscsi* at pciide* channel -1 */
    {&atapiscsi_ca,	&atapiscsi_cd,	 0, STAR, loc+ 53,    0, pv+91, 96,    0},
/* 49: wd* at pciide* channel -1 drive -1 */
    {&wd_ca,		&wd_cd,		 0, STAR, loc+ 52,    0, pv+91, 96,    0},
/* 50: pci* at apecs*|cia*|irongate*|lca*|ppb*|tsp*|mcpcia* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+ 53,    0, pv+51, 98,    0},
/* 51: vga* at pci* dev -1 function -1 */
    {&vga_pci_ca,	&vga_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 52: vga* at isa* port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&vga_isa_ca,	&vga_cd,	 0, STAR, loc+  0,    0, pv+75, 115,    0},
/* 53: tga* at pci* dev -1 function -1 */
    {&tga_ca,		&tga_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 54: ahc* at pci* dev -1 function -1 */
    {&ahc_pci_ca,	&ahc_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 55: ahc* at eisa* slot -1 */
    {&ahc_eisa_ca,	&ahc_cd,	 0, STAR, loc+ 53,    0, pv+77, 123,    0},
/* 56: adw* at pci* dev -1 function -1 */
    {&adw_pci_ca,	&adw_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 57: twe* at pci* dev -1 function -1 */
    {&twe_pci_ca,	&twe_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 58: gdt* at pci* dev -1 function -1 */
    {&gdt_pci_ca,	&gdt_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 59: ciss* at pci* dev -1 function -1 */
    {&ciss_pci_ca,	&ciss_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 60: qlw* at pci* dev -1 function -1 */
    {&qlw_pci_ca,	&qlw_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 61: qla* at pci* dev -1 function -1 */
    {&qla_pci_ca,	&qla_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 62: qle* at pci* dev -1 function -1 */
    {&qle_ca,		&qle_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 63: de* at pci* dev -1 function -1 */
    {&de_ca,		&de_cd,		 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 64: ep* at pci* dev -1 function -1 */
    {&ep_pci_ca,	&ep_cd,		 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 65: ep* at isa* port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&ep_isa_ca,	&ep_cd,		 0, STAR, loc+  0,    0, pv+75, 115,    0},
/* 66: ep* at isapnp0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 */
    {&ep_isapnp_ca,	&ep_cd,		 0, STAR, loc+ 48,    0, pv+105, 125,    0},
/* 67: pcn* at pci* dev -1 function -1 */
    {&pcn_ca,		&pcn_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 68: siop* at pci* dev -1 function -1 */
    {&siop_pci_ca,	&siop_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 69: pciide* at pci* dev -1 function -1 */
    {&pciide_pci_ca,	&pciide_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 70: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 71: mtd* at pci* dev -1 function -1 */
    {&mtd_pci_ca,	&mtd_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 72: rl* at pci* dev -1 function -1 */
    {&rl_pci_ca,	&rl_cd,		 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 73: re* at pci* dev -1 function -1 */
    {&re_pci_ca,	&re_cd,		 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 74: vr* at pci* dev -1 function -1 */
    {&vr_ca,		&vr_cd,		 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 75: tl* at pci* dev -1 function -1 */
    {&tl_ca,		&tl_cd,		 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 76: txp* at pci* dev -1 function -1 */
    {&txp_ca,		&txp_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 77: xl* at pci* dev -1 function -1 */
    {&xl_pci_ca,	&xl_cd,		 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 78: fxp* at pci* dev -1 function -1 */
    {&fxp_pci_ca,	&fxp_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 79: em* at pci* dev -1 function -1 */
    {&em_ca,		&em_cd,		 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 80: xge* at pci* dev -1 function -1 */
    {&xge_ca,		&xge_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 81: dc* at pci* dev -1 function -1 */
    {&dc_pci_ca,	&dc_cd,		 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 82: epic* at pci* dev -1 function -1 */
    {&epic_pci_ca,	&epic_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 83: ti* at pci* dev -1 function -1 */
    {&ti_pci_ca,	&ti_cd,		 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 84: ne* at pci* dev -1 function -1 */
    {&ne_pci_ca,	&ne_cd,		 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 85: wb* at pci* dev -1 function -1 */
    {&wb_ca,		&wb_cd,		 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 86: sf* at pci* dev -1 function -1 */
    {&sf_pci_ca,	&sf_cd,		 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 87: sis* at pci* dev -1 function -1 */
    {&sis_ca,		&sis_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 88: ste* at pci* dev -1 function -1 */
    {&ste_ca,		&ste_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 89: uhci* at pci* dev -1 function -1 */
    {&uhci_pci_ca,	&uhci_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 90: ohci* at pci* dev -1 function -1 */
    {&ohci_pci_ca,	&ohci_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 91: ehci* at pci* dev -1 function -1 */
    {&ehci_pci_ca,	&ehci_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 92: skc* at pci* dev -1 function -1 */
    {&skc_ca,		&skc_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 93: sk* at skc* */
    {&sk_ca,		&sk_cd,		 0, STAR,     loc,    0, pv+101, 131,    0},
/* 94: mskc* at pci* dev -1 function -1 */
    {&mskc_ca,		&mskc_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 95: msk* at mskc* */
    {&msk_ca,		&msk_cd,	 0, STAR,     loc,    0, pv+95, 131,    0},
/* 96: an* at pci* dev -1 function -1 */
    {&an_pci_ca,	&an_cd,		 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 97: pcscp* at pci* dev -1 function -1 */
    {&pcscp_ca,		&pcscp_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 98: lge* at pci* dev -1 function -1 */
    {&lge_ca,		&lge_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/* 99: bge* at pci* dev -1 function -1 */
    {&bge_ca,		&bge_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/*100: vge* at pci* dev -1 function -1 */
    {&vge_ca,		&vge_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/*101: stge* at pci* dev -1 function -1 */
    {&stge_ca,		&stge_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/*102: ral* at pci* dev -1 function -1 */
    {&ral_pci_ca,	&ral_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/*103: isa* at pceb*|sio* */
    {&isa_ca,		&isa_cd,	 0, STAR,     loc,    0, pv+70, 131,    0},
/*104: isadma0 at isa* port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&isadma_ca,	&isadma_cd,	 0, NORM, loc+  0,    0, pv+75, 115,    0},
/*105: com* at isa* port 0x3f8 size 0 iomem -1 iosiz 0 irq 4 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 0, STAR, loc+ 28,    0, pv+75, 115,    0},
/*106: com* at isa* port 0x2f8 size 0 iomem -1 iosiz 0 irq 3 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 0, STAR, loc+ 21,    0, pv+75, 115,    0},
/*107: pckbc* at isa* port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&pckbc_isa_ca,	&pckbc_cd,	 0, STAR, loc+  0,    0, pv+75, 115,    0},
/*108: lc0 at isa* port 0x200 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&lc_isa_ca,	&lc_cd,		 0, NORM, loc+ 14,    0, pv+75, 115,    0},
/*109: lc1 at isa* port 0x280 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&lc_isa_ca,	&lc_cd,		 1, NORM, loc+  7,    0, pv+75, 115,    1},
/*110: pckbd* at pckbc* slot -1 */
    {&pckbd_ca,		&pckbd_cd,	 0, STAR, loc+ 53,    0, pv+79, 132,    0},
/*111: eisa* at pceb* */
    {&eisa_ca,		&eisa_cd,	 0, STAR,     loc,    0, pv+103, 131,    0},
/*112: apecs* at mainbus0 */
    {&apecs_ca,		&apecs_cd,	 0, STAR,     loc,    0, pv+93, 93,    0},
/*113: lca* at mainbus0 */
    {&lca_ca,		&lca_cd,	 0, STAR,     loc,    0, pv+93, 93,    0},
/*114: cia* at mainbus0 */
    {&cia_ca,		&cia_cd,	 0, STAR,     loc,    0, pv+93, 93,    0},
/*115: irongate* at mainbus0 */
    {&irongate_ca,	&irongate_cd,	 0, STAR,     loc,    0, pv+93, 93,    0},
/*116: tsc* at mainbus0 */
    {&tsc_ca,		&tsc_cd,	 0, STAR,     loc,    0, pv+93, 93,    0},
/*117: tsp* at tsc* */
    {&tsp_ca,		&tsp_cd,	 0, STAR,     loc,    0, pv+97, 133,    0},
/*118: mcpcia* at mcbus* mid -1 */
    {&mcpcia_ca,	&mcpcia_cd,	 0, STAR, loc+ 53,    0, pv+81, 94,    0},
/*119: sio* at pci* dev -1 function -1 */
    {&sio_ca,		&sio_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/*120: pceb* at pci* dev -1 function -1 */
    {&pceb_ca,		&pceb_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/*121: hme* at pci* dev -1 function -1 */
    {&hme_pci_ca,	&hme_cd,	 0, STAR, loc+ 52,    0, pv+73, 112,    0},
/*122: isapnp0 at isa* port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&isapnp_ca,	&isapnp_cd,	 0, NORM, loc+  0,    0, pv+75, 115,    0},
/*123: mcclock* at ioasic* offset -1 */
    {&mcclock_ioasic_ca,	&mcclock_cd,	 0, STAR, loc+ 53,    0, pv+83, 88,    0},
/*124: mcclock* at isa* port 0x70 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&mcclock_isa_ca,	&mcclock_cd,	 0, STAR, loc+ 35,    0, pv+75, 115,    0},
/*125: usb* at ehci*|uhci*|ohci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+59, 133,    0},
/*126: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+99, 133,    0},
/*127: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+ 42,    0, pv+67, 134,    0},
/*128: uhidev* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhidev_ca,	&uhidev_cd,	 0, STAR, loc+ 42,    0, pv+67, 134,    0},
/*129: uhid* at uhidev* reportid -1 */
    {&uhid_ca,		&uhid_cd,	 0, STAR, loc+ 53,    0, pv+85, 148,    0},
/*130: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+ 42,    0, pv+67, 134,    0},
/*131: aue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&aue_ca,		&aue_cd,	 0, STAR, loc+ 42,    0, pv+67, 134,    0},
/*132: axe* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axe_ca,		&axe_cd,	 0, STAR, loc+ 42,    0, pv+67, 134,    0},
/*133: axen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axen_ca,		&axen_cd,	 0, STAR, loc+ 42,    0, pv+67, 134,    0},
/*134: smsc* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&smsc_ca,		&smsc_cd,	 0, STAR, loc+ 42,    0, pv+67, 134,    0},
/*135: cue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cue_ca,		&cue_cd,	 0, STAR, loc+ 42,    0, pv+67, 134,    0},
/*136: kue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&kue_ca,		&kue_cd,	 0, STAR, loc+ 42,    0, pv+67, 134,    0},
/*137: cdce* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cdce_ca,		&cdce_cd,	 0, STAR, loc+ 42,    0, pv+67, 134,    0},
/*138: udav* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udav_ca,		&udav_cd,	 0, STAR, loc+ 42,    0, pv+67, 134,    0},
/*139: upl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upl_ca,		&upl_cd,	 0, STAR, loc+ 42,    0, pv+67, 134,    0},
/*140: ugl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugl_ca,		&ugl_cd,	 0, STAR, loc+ 42,    0, pv+67, 134,    0},
/*141: url* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&url_ca,		&url_cd,	 0, STAR, loc+ 42,    0, pv+67, 134,    0},
/*142: ure* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ure_ca,		&ure_cd,	 0, STAR, loc+ 42,    0, pv+67, 134,    0},
/*143: wi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&wi_usb_ca,	&wi_cd,		 0, STAR, loc+ 42,    0, pv+67, 134,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 0 /* softraid0 */,
	 1 /* mainbus0 */,
	-1
};

int cfroots_size = 3;

/* pseudo-devices */
extern void bpfilterattach(int);
extern void vlanattach(int);
extern void loopattach(int);
extern void rdattach(int);
extern void bioattach(int);

char *pdevnames[] = {
	"bpfilter",
	"vlan",
	"loop",
	"rd",
	"bio",
};

int pdevnames_size = 5;

struct pdevinit pdevinit[] = {
	{ bpfilterattach, 1 },
	{ vlanattach, 1 },
	{ loopattach, 1 },
	{ rdattach, 1 },
	{ bioattach, 1 },
	{ NULL, 0 }
};
