/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/alpha/conf/RAMDISKC"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver vga_cd;
extern struct cfdriver qla_cd;
extern struct cfdriver siop_cd;
extern struct cfdriver ep_cd;
extern struct cfdriver com_cd;
extern struct cfdriver pckbc_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver tqphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver wd_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver tga_cd;
extern struct cfdriver pciide_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver isa_cd;
extern struct cfdriver pcic_cd;
extern struct cfdriver pcmcia_cd;
extern struct cfdriver pckbd_cd;
extern struct cfdriver lca_cd;
extern struct cfdriver sio_cd;
extern struct cfdriver pceb_cd;
extern struct cfdriver mcclock_cd;

extern struct cfattach mainbus_ca;
extern struct cfattach cpu_ca;
extern struct cfattach tqphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach sd_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach wd_ca;
extern struct cfattach pci_ca;
extern struct cfattach vga_pci_ca;
extern struct cfattach tga_ca;
extern struct cfattach qla_pci_ca;
extern struct cfattach siop_pci_ca;
extern struct cfattach pciide_pci_ca;
extern struct cfattach ppb_ca;
extern struct cfattach isa_ca;
extern struct cfattach com_isa_ca;
extern struct cfattach pckbc_isa_ca;
extern struct cfattach vga_isa_ca;
extern struct cfattach pcic_isa_ca;
extern struct cfattach pcmcia_ca;
extern struct cfattach ep_pcmcia_ca;
extern struct cfattach pckbd_ca;
extern struct cfattach lca_ca;
extern struct cfattach sio_ca;
extern struct cfattach pceb_ca;
extern struct cfattach mcclock_isa_ca;


/* locators */
static long loc[45] = {
	0x2f8, 0, -1, 0, 3, -1, -1, -1,
	0, -1, 0, -1, -1, -1, 0x70, 0,
	-1, 0, -1, -1, -1, 0x3e0, 0, 0xd0000,
	0x10000, -1, -1, -1, 0x3e2, 0, 0xcc000, 0x10000,
	-1, -1, -1, 0x3f8, 0, -1, 0, 4,
	-1, -1, -1, -1, 1,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"phy",
	"target",
	"lun",
	"console",
	"primary",
	"mux",
	"channel",
	"drive",
	"bus",
	"dev",
	"function",
	"port",
	"size",
	"iomem",
	"iosiz",
	"irq",
	"drq",
	"drq2",
	"controller",
	"socket",
	"slot",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, -1, 1, 2, -1, 3, 4,
	5, -1, 3, 4, 5, -1, 3, 4,
	5, -1, 3, 5, -1, 6, 7, -1,
	8, -1, 8, -1, 9, 10, -1, 11,
	12, 13, 14, 15, 16, 17, -1, 18,
	19, -1, 18, 19, -1, 10, 15, -1,
	20, -1,
};

/* size of parent vectors */
int pv_size = 34;

/* parent vectors */
short pv[34] = {
	10, 11, 12, -1, 21, 22, -1, 26, 16, -1, 14, 13, -1, 28, 27, -1,
	17, -1, 9, -1, 4, -1, 25, -1, 15, -1, 23, -1, 24, -1, 20, -1,
	0, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+ 3, 0,    0},
/*  1: cpu* at mainbus0 */
    {&cpu_ca,		&cpu_cd,	 0, STAR,     loc,    0, pv+32, 49,    0},
/*  2: tqphy* at ep* phy -1 */
    {&tqphy_ca,		&tqphy_cd,	 0, STAR, loc+ 41,    0, pv+28, 1,    0},
/*  3: ukphy* at ep* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+ 41,    0, pv+28, 1,    0},
/*  4: scsibus* at siop*|qla* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+10, 2,    0},
/*  5: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+ 40,    0, pv+20, 3,    0},
/*  6: wsdisplay* at vga*|vga*|tga* console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+ 42,    0, pv+ 0, 6,    0},
/*  7: wskbd* at pckbd* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+ 43,    0, pv+22, 18,    0},
/*  8: wd* at pciide* channel -1 drive -1 */
    {&wd_ca,		&wd_cd,		 0, STAR, loc+ 40,    0, pv+24, 21,    0},
/*  9: pci* at lca*|ppb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+ 41,    0, pv+ 7, 24,    0},
/* 10: vga* at pci* dev -1 function -1 */
    {&vga_pci_ca,	&vga_cd,	 0, STAR, loc+ 40,    0, pv+18, 28,    0},
/* 11: vga* at isa* port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&vga_isa_ca,	&vga_cd,	 0, STAR, loc+  7,    0, pv+16, 31,    0},
/* 12: tga* at pci* dev -1 function -1 */
    {&tga_ca,		&tga_cd,	 0, STAR, loc+ 40,    0, pv+18, 28,    0},
/* 13: qla* at pci* dev -1 function -1 */
    {&qla_pci_ca,	&qla_cd,	 0, STAR, loc+ 40,    0, pv+18, 28,    0},
/* 14: siop* at pci* dev -1 function -1 */
    {&siop_pci_ca,	&siop_cd,	 0, STAR, loc+ 40,    0, pv+18, 28,    0},
/* 15: pciide* at pci* dev -1 function -1 */
    {&pciide_pci_ca,	&pciide_cd,	 0, STAR, loc+ 40,    0, pv+18, 28,    0},
/* 16: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+ 40,    0, pv+18, 28,    0},
/* 17: isa* at pceb*|sio* */
    {&isa_ca,		&isa_cd,	 0, STAR,     loc,    0, pv+13, 38,    0},
/* 18: com* at isa* port 0x3f8 size 0 iomem -1 iosiz 0 irq 4 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 0, STAR, loc+ 35,    0, pv+16, 31,    0},
/* 19: com* at isa* port 0x2f8 size 0 iomem -1 iosiz 0 irq 3 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 0, STAR, loc+  0,    0, pv+16, 31,    0},
/* 20: pckbc* at isa* port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&pckbc_isa_ca,	&pckbc_cd,	 0, STAR, loc+  7,    0, pv+16, 31,    0},
/* 21: pcic0 at isa* port 0x3e0 size 0 iomem 0xd0000 iosiz 0x10000 irq -1 drq -1 drq2 -1 */
    {&pcic_isa_ca,	&pcic_cd,	 0, NORM, loc+ 21,    0, pv+16, 31,    0},
/* 22: pcic1 at isa* port 0x3e2 size 0 iomem 0xcc000 iosiz 0x10000 irq -1 drq -1 drq2 -1 */
    {&pcic_isa_ca,	&pcic_cd,	 1, NORM, loc+ 28,    0, pv+16, 31,    1},
/* 23: pcmcia* at pcic0|pcic1 controller -1 socket -1 */
    {&pcmcia_ca,	&pcmcia_cd,	 0, STAR, loc+ 40,    0, pv+ 4, 39,    0},
/* 24: ep* at pcmcia* function -1 irq -1 */
    {&ep_pcmcia_ca,	&ep_cd,		 0, STAR, loc+ 40,    0, pv+26, 45,    0},
/* 25: pckbd* at pckbc* slot -1 */
    {&pckbd_ca,		&pckbd_cd,	 0, STAR, loc+ 41,    0, pv+30, 48,    0},
/* 26: lca* at mainbus0 */
    {&lca_ca,		&lca_cd,	 0, STAR,     loc,    0, pv+32, 49,    0},
/* 27: sio* at pci* dev -1 function -1 */
    {&sio_ca,		&sio_cd,	 0, STAR, loc+ 40,    0, pv+18, 28,    0},
/* 28: pceb* at pci* dev -1 function -1 */
    {&pceb_ca,		&pceb_cd,	 0, STAR, loc+ 40,    0, pv+18, 28,    0},
/* 29: mcclock* at isa* port 0x70 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&mcclock_isa_ca,	&mcclock_cd,	 0, STAR, loc+ 14,    0, pv+16, 31,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 0 /* mainbus0 */,
	-1
};

int cfroots_size = 2;

/* pseudo-devices */
extern void loopattach(int);
extern void rdattach(int);

char *pdevnames[] = {
	"loop",
	"rd",
};

int pdevnames_size = 2;

struct pdevinit pdevinit[] = {
	{ loopattach, 1 },
	{ rdattach, 1 },
	{ NULL, 0 }
};
