/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/amd64/conf/RAMDISK"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver vga_cd;
extern struct cfdriver mpi_cd;
extern struct cfdriver re_cd;
extern struct cfdriver com_cd;
extern struct cfdriver pckbc_cd;
extern struct cfdriver uhci_cd;
extern struct cfdriver ehci_cd;
extern struct cfdriver xhci_cd;
extern struct cfdriver nsphy_cd;
extern struct cfdriver nsphyter_cd;
extern struct cfdriver inphy_cd;
extern struct cfdriver iophy_cd;
extern struct cfdriver rlphy_cd;
extern struct cfdriver lxtphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver brgphy_cd;
extern struct cfdriver rgephy_cd;
extern struct cfdriver ciphy_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver wd_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver bios_cd;
extern struct cfdriver mpbios_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver ioapic_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver pciide_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver em_cd;
extern struct cfdriver bge_cd;
extern struct cfdriver pchb_cd;
extern struct cfdriver aapic_cd;
extern struct cfdriver isa_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver pckbd_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver uhidev_cd;
extern struct cfdriver ukbd_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver acpi_cd;
extern struct cfdriver acpicmos_cd;
extern struct cfdriver acpihpet_cd;
extern struct cfdriver acpiec_cd;
extern struct cfdriver acpimadt_cd;
extern struct cfdriver acpiprt_cd;
extern struct cfdriver acpipci_cd;

extern struct cfattach nsphy_ca;
extern struct cfattach nsphyter_ca;
extern struct cfattach inphy_ca;
extern struct cfattach iophy_ca;
extern struct cfattach rlphy_ca;
extern struct cfattach lxtphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach brgphy_ca;
extern struct cfattach rgephy_ca;
extern struct cfattach ciphy_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach sd_ca;
extern struct cfattach wd_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach bios_ca;
extern struct cfattach mpbios_ca;
extern struct cfattach cpu_ca;
extern struct cfattach ioapic_ca;
extern struct cfattach pci_ca;
extern struct cfattach vga_pci_ca;
extern struct cfattach mpi_pci_ca;
extern struct cfattach pciide_pci_ca;
extern struct cfattach ppb_ca;
extern struct cfattach re_pci_ca;
extern struct cfattach em_ca;
extern struct cfattach uhci_pci_ca;
extern struct cfattach ehci_pci_ca;
extern struct cfattach xhci_pci_ca;
extern struct cfattach bge_ca;
extern struct cfattach pchb_ca;
extern struct cfattach aapic_ca;
extern struct cfattach isa_ca;
extern struct cfattach com_isa_ca;
extern struct cfattach pckbc_isa_ca;
extern struct cfattach vga_isa_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach pckbd_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach uhidev_ca;
extern struct cfattach ukbd_ca;
extern struct cfattach umass_ca;
extern struct cfattach acpicmos_ca;
extern struct cfattach acpihpet_ca;
extern struct cfattach acpiec_ca;
extern struct cfattach acpimadt_ca;
extern struct cfattach acpiprt_ca;
extern struct cfattach acpi_ca;
extern struct cfattach acpipci_ca;


/* locators */
static long loc[30] = {
	-1, 0, -1, 0, -1, -1, -1, 0x2f8,
	0, -1, 0, 3, -1, -1, 0x3f8, 0,
	-1, 0, 4, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, 1,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"phy",
	"target",
	"lun",
	"channel",
	"drive",
	"apid",
	"bus",
	"dev",
	"function",
	"port",
	"size",
	"iomem",
	"iosiz",
	"irq",
	"drq",
	"drq2",
	"console",
	"primary",
	"mux",
	"slot",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"reportid",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, -1, 0, -1, 1, 2, -1,
	3, 4, -1, 5, -1, 6, -1, 6,
	-1, 7, 8, -1, 9, 10, 11, 12,
	13, 14, 15, -1, 16, 17, 18, -1,
	16, 17, 18, -1, 16, 18, -1, 16,
	18, -1, 19, -1, 9, 20, 21, 22,
	23, 24, -1, 9, 20, 21, 22, 23,
	24, -1, 25, -1,
};

/* size of parent vectors */
int pv_size = 43;

/* parent vectors */
short pv[43] = {
	13, 22, 29, -1, 27, 26, 25, -1, 40, 41, -1, 35, 19, -1, 28, 23,
	-1, 44, 20, -1, 43, 38, -1, 13, -1, 34, -1, 42, -1, 14, -1, 39,
	-1, 50, -1, 31, -1, 10, -1, 21, -1, 18, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: nsphy* at bge*|re* phy -1 */
    {&nsphy_ca,		&nsphy_cd,	 0, STAR, loc+ 26,    0, pv+14, 1,    0},
/*  1: nsphyter* at bge*|re* phy -1 */
    {&nsphyter_ca,	&nsphyter_cd,	 0, STAR, loc+ 26,    0, pv+14, 1,    0},
/*  2: inphy* at bge*|re* phy -1 */
    {&inphy_ca,		&inphy_cd,	 0, STAR, loc+ 26,    0, pv+14, 1,    0},
/*  3: iophy* at bge*|re* phy -1 */
    {&iophy_ca,		&iophy_cd,	 0, STAR, loc+ 26,    0, pv+14, 1,    0},
/*  4: rlphy* at bge*|re* phy -1 */
    {&rlphy_ca,		&rlphy_cd,	 0, STAR, loc+ 26,    0, pv+14, 1,    0},
/*  5: lxtphy* at bge*|re* phy -1 */
    {&lxtphy_ca,	&lxtphy_cd,	 0, STAR, loc+ 26,    0, pv+14, 1,    0},
/*  6: ukphy* at bge*|re* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+ 26,    0, pv+14, 1,    0},
/*  7: brgphy* at bge*|re* phy -1 */
    {&brgphy_ca,	&brgphy_cd,	 0, STAR, loc+ 26,    0, pv+14, 1,    0},
/*  8: rgephy* at bge*|re* phy -1 */
    {&rgephy_ca,	&rgephy_cd,	 0, STAR, loc+ 26,    0, pv+14, 1,    0},
/*  9: ciphy* at bge*|re* phy -1 */
    {&ciphy_ca,		&ciphy_cd,	 0, STAR, loc+ 26,    0, pv+14, 1,    0},
/* 10: scsibus* at umass*|mpi* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+17, 4,    0},
/* 11: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+ 25,    0, pv+37, 5,    0},
/* 12: wd* at pciide* channel -1 drive -1 */
    {&wd_ca,		&wd_cd,		 0, STAR, loc+ 25,    0, pv+39, 8,    0},
/* 13: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+ 3, 0,    0},
/* 14: bios0 at mainbus0 apid -1 */
    {&bios_ca,		&bios_cd,	 0, NORM, loc+ 26,    0, pv+23, 11,    0},
/* 15: mpbios0 at bios0 */
    {&mpbios_ca,	&mpbios_cd,	 0, NORM,     loc,    0, pv+29, 12,    0},
/* 16: cpu0 at mainbus0 apid -1 */
    {&cpu_ca,		&cpu_cd,	 0, NORM, loc+ 26,    0, pv+23, 11,    0},
/* 17: ioapic* at mainbus0 apid -1 */
    {&ioapic_ca,	&ioapic_cd,	 0, STAR, loc+ 26,    0, pv+23, 11,    0},
/* 18: pci* at mainbus0|ppb*|pchb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+ 26,    0, pv+ 0, 11,    0},
/* 19: vga* at pci* dev -1 function -1 */
    {&vga_pci_ca,	&vga_cd,	 1, STAR, loc+ 25,    0, pv+41, 17,    1},
/* 20: mpi* at pci* dev -1 function -1 */
    {&mpi_pci_ca,	&mpi_cd,	 0, STAR, loc+ 25,    0, pv+41, 17,    0},
/* 21: pciide* at pci* dev -1 function -1 */
    {&pciide_pci_ca,	&pciide_cd,	 0, STAR, loc+ 25,    0, pv+41, 17,    0},
/* 22: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+ 25,    0, pv+41, 17,    0},
/* 23: re* at pci* dev -1 function -1 */
    {&re_pci_ca,	&re_cd,		 0, STAR, loc+ 25,    0, pv+41, 17,    0},
/* 24: em* at pci* dev -1 function -1 */
    {&em_ca,		&em_cd,		 0, STAR, loc+ 25,    0, pv+41, 17,    0},
/* 25: uhci* at pci* dev -1 function -1 */
    {&uhci_pci_ca,	&uhci_cd,	 0, STAR, loc+ 25,    0, pv+41, 17,    0},
/* 26: ehci* at pci* dev -1 function -1 */
    {&ehci_pci_ca,	&ehci_cd,	 0, STAR, loc+ 25,    0, pv+41, 17,    0},
/* 27: xhci* at pci* dev -1 function -1 */
    {&xhci_pci_ca,	&xhci_cd,	 0, STAR, loc+ 25,    0, pv+41, 17,    0},
/* 28: bge* at pci* dev -1 function -1 */
    {&bge_ca,		&bge_cd,	 0, STAR, loc+ 25,    0, pv+41, 17,    0},
/* 29: pchb* at pci* dev -1 function -1 */
    {&pchb_ca,		&pchb_cd,	 0, STAR, loc+ 25,    0, pv+41, 17,    0},
/* 30: aapic* at pci* dev -1 function -1 */
    {&aapic_ca,		&aapic_cd,	 0, STAR, loc+ 25,    0, pv+41, 17,    0},
/* 31: isa0 at mainbus0 */
    {&isa_ca,		&isa_cd,	 0, NORM,     loc,    0, pv+23, 11,    0},
/* 32: com0 at isa0 port 0x3f8 size 0 iomem -1 iosiz 0 irq 4 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 0, NORM, loc+ 14,    0, pv+35, 20,    0},
/* 33: com1 at isa0 port 0x2f8 size 0 iomem -1 iosiz 0 irq 3 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 1, NORM, loc+  7,    0, pv+35, 20,    1},
/* 34: pckbc0 at isa0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&pckbc_isa_ca,	&pckbc_cd,	 0, NORM, loc+  0,    0, pv+35, 20,    0},
/* 35: vga0 at isa0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&vga_isa_ca,	&vga_cd,	 0, NORM, loc+  0,    0, pv+35, 20,    0},
/* 36: wsdisplay* at vga0|vga* console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+ 27,    0, pv+11, 28,    0},
/* 37: wskbd* at ukbd*|pckbd* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+ 28,    0, pv+20, 36,    0},
/* 38: pckbd* at pckbc0 slot -1 */
    {&pckbd_ca,		&pckbd_cd,	 0, STAR, loc+ 26,    0, pv+25, 42,    0},
/* 39: usb* at xhci*|ehci*|uhci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+ 4, 43,    0},
/* 40: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+31, 43,    0},
/* 41: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+ 21,    0, pv+ 8, 44,    0},
/* 42: uhidev* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhidev_ca,	&uhidev_cd,	 0, STAR, loc+ 21,    0, pv+ 8, 44,    0},
/* 43: ukbd* at uhidev* reportid -1 */
    {&ukbd_ca,		&ukbd_cd,	 0, STAR, loc+ 26,    0, pv+27, 58,    0},
/* 44: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+ 21,    0, pv+ 8, 44,    0},
/* 45: acpicmos* at acpi0 */
    {&acpicmos_ca,	&acpicmos_cd,	 0, STAR,     loc,    0, pv+33, 59,    0},
/* 46: acpihpet* at acpi0 */
    {&acpihpet_ca,	&acpihpet_cd,	 0, STAR,     loc,    0, pv+33, 59,    0},
/* 47: acpiec* at acpi0 */
    {&acpiec_ca,	&acpiec_cd,	 0, STAR,     loc,    0, pv+33, 59,    0},
/* 48: acpimadt0 at acpi0 */
    {&acpimadt_ca,	&acpimadt_cd,	 0, NORM,     loc,    0, pv+33, 59,    0},
/* 49: acpiprt* at acpi0 */
    {&acpiprt_ca,	&acpiprt_cd,	 0, STAR,     loc,    0, pv+33, 59,    0},
/* 50: acpi0 at bios0 */
    {&acpi_ca,		&acpi_cd,	 0, NORM,     loc,    0, pv+29, 12,    0},
/* 51: acpipci* at acpi0 */
    {&acpipci_ca,	&acpipci_cd,	 0, STAR,     loc,    0, pv+33, 59,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	13 /* mainbus0 */,
	-1
};

int cfroots_size = 2;

/* pseudo-devices */
extern void loopattach(int);
extern void bpfilterattach(int);
extern void rdattach(int);
extern void wsmuxattach(int);

char *pdevnames[] = {
	"loop",
	"bpfilter",
	"rd",
	"wsmux",
};

int pdevnames_size = 4;

struct pdevinit pdevinit[] = {
	{ loopattach, 1 },
	{ bpfilterattach, 1 },
	{ rdattach, 1 },
	{ wsmuxattach, 2 },
	{ NULL, 0 }
};
