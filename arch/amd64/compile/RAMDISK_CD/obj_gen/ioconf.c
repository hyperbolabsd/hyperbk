/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/amd64/conf/RAMDISK_CD"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver vga_cd;
extern struct cfdriver wdc_cd;
extern struct cfdriver ahc_cd;
extern struct cfdriver ahd_cd;
extern struct cfdriver adw_cd;
extern struct cfdriver gdt_cd;
extern struct cfdriver twe_cd;
extern struct cfdriver ciss_cd;
extern struct cfdriver ami_cd;
extern struct cfdriver mfi_cd;
extern struct cfdriver qlw_cd;
extern struct cfdriver qla_cd;
extern struct cfdriver ahci_cd;
extern struct cfdriver nvme_cd;
extern struct cfdriver mpi_cd;
extern struct cfdriver sili_cd;
extern struct cfdriver siop_cd;
extern struct cfdriver ep_cd;
extern struct cfdriver wi_cd;
extern struct cfdriver an_cd;
extern struct cfdriver xl_cd;
extern struct cfdriver fxp_cd;
extern struct cfdriver rl_cd;
extern struct cfdriver re_cd;
extern struct cfdriver dc_cd;
extern struct cfdriver sm_cd;
extern struct cfdriver epic_cd;
extern struct cfdriver ne_cd;
extern struct cfdriver com_cd;
extern struct cfdriver pckbc_cd;
extern struct cfdriver iha_cd;
extern struct cfdriver ath_cd;
extern struct cfdriver athn_cd;
extern struct cfdriver bwfm_cd;
extern struct cfdriver atw_cd;
extern struct cfdriver rtw_cd;
extern struct cfdriver rtwn_cd;
extern struct cfdriver ral_cd;
extern struct cfdriver acx_cd;
extern struct cfdriver pgt_cd;
extern struct cfdriver sf_cd;
extern struct cfdriver malo_cd;
extern struct cfdriver bwi_cd;
extern struct cfdriver virtio_cd;
extern struct cfdriver uhci_cd;
extern struct cfdriver ohci_cd;
extern struct cfdriver ehci_cd;
extern struct cfdriver xhci_cd;
extern struct cfdriver ccp_cd;
extern struct cfdriver sdhc_cd;
extern struct cfdriver rtsx_cd;
extern struct cfdriver softraid_cd;
extern struct cfdriver dwiic_cd;
extern struct cfdriver nsphy_cd;
extern struct cfdriver nsphyter_cd;
extern struct cfdriver qsphy_cd;
extern struct cfdriver inphy_cd;
extern struct cfdriver iophy_cd;
extern struct cfdriver eephy_cd;
extern struct cfdriver exphy_cd;
extern struct cfdriver rlphy_cd;
extern struct cfdriver lxtphy_cd;
extern struct cfdriver luphy_cd;
extern struct cfdriver mtdphy_cd;
extern struct cfdriver icsphy_cd;
extern struct cfdriver sqphy_cd;
extern struct cfdriver tqphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver dcphy_cd;
extern struct cfdriver bmtphy_cd;
extern struct cfdriver brgphy_cd;
extern struct cfdriver xmphy_cd;
extern struct cfdriver amphy_cd;
extern struct cfdriver acphy_cd;
extern struct cfdriver urlphy_cd;
extern struct cfdriver rgephy_cd;
extern struct cfdriver ciphy_cd;
extern struct cfdriver ipgphy_cd;
extern struct cfdriver etphy_cd;
extern struct cfdriver jmphy_cd;
extern struct cfdriver atphy_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver cd_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver st_cd;
extern struct cfdriver atapiscsi_cd;
extern struct cfdriver wd_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver bios_cd;
extern struct cfdriver mpbios_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver ioapic_cd;
extern struct cfdriver efifb_cd;
extern struct cfdriver pvbus_cd;
extern struct cfdriver xen_cd;
extern struct cfdriver xnf_cd;
extern struct cfdriver xbf_cd;
extern struct cfdriver hyperv_cd;
extern struct cfdriver hvn_cd;
extern struct cfdriver hvs_cd;
extern struct cfdriver vio_cd;
extern struct cfdriver vioblk_cd;
extern struct cfdriver viornd_cd;
extern struct cfdriver vioscsi_cd;
extern struct cfdriver vmmci_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver arc_cd;
extern struct cfdriver jmb_cd;
extern struct cfdriver mfii_cd;
extern struct cfdriver ips_cd;
extern struct cfdriver qle_cd;
extern struct cfdriver mpii_cd;
extern struct cfdriver aq_cd;
extern struct cfdriver de_cd;
extern struct cfdriver pcn_cd;
extern struct cfdriver pciide_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver vr_cd;
extern struct cfdriver em_cd;
extern struct cfdriver ixgb_cd;
extern struct cfdriver ix_cd;
extern struct cfdriver ixl_cd;
extern struct cfdriver xge_cd;
extern struct cfdriver myx_cd;
extern struct cfdriver oce_cd;
extern struct cfdriver sis_cd;
extern struct cfdriver se_cd;
extern struct cfdriver cbb_cd;
extern struct cfdriver skc_cd;
extern struct cfdriver sk_cd;
extern struct cfdriver mskc_cd;
extern struct cfdriver msk_cd;
extern struct cfdriver iwi_cd;
extern struct cfdriver wpi_cd;
extern struct cfdriver iwn_cd;
extern struct cfdriver iwm_cd;
extern struct cfdriver iwx_cd;
extern struct cfdriver pcscp_cd;
extern struct cfdriver bge_cd;
extern struct cfdriver bnx_cd;
extern struct cfdriver vge_cd;
extern struct cfdriver stge_cd;
extern struct cfdriver nfe_cd;
extern struct cfdriver et_cd;
extern struct cfdriver jme_cd;
extern struct cfdriver age_cd;
extern struct cfdriver alc_cd;
extern struct cfdriver ale_cd;
extern struct cfdriver bce_cd;
extern struct cfdriver vic_cd;
extern struct cfdriver vmx_cd;
extern struct cfdriver vmwpvs_cd;
extern struct cfdriver lii_cd;
extern struct cfdriver xspd_cd;
extern struct cfdriver bnxt_cd;
extern struct cfdriver mcx_cd;
extern struct cfdriver iavf_cd;
extern struct cfdriver rge_cd;
extern struct cfdriver pchb_cd;
extern struct cfdriver cardslot_cd;
extern struct cfdriver cardbus_cd;
extern struct cfdriver pcmcia_cd;
extern struct cfdriver xe_cd;
extern struct cfdriver aapic_cd;
extern struct cfdriver hme_cd;
extern struct cfdriver isa_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver pckbd_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver uhidev_cd;
extern struct cfdriver ukbd_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver aue_cd;
extern struct cfdriver axe_cd;
extern struct cfdriver axen_cd;
extern struct cfdriver smsc_cd;
extern struct cfdriver cue_cd;
extern struct cfdriver kue_cd;
extern struct cfdriver cdce_cd;
extern struct cfdriver mos_cd;
extern struct cfdriver mue_cd;
extern struct cfdriver udav_cd;
extern struct cfdriver upl_cd;
extern struct cfdriver ugl_cd;
extern struct cfdriver url_cd;
extern struct cfdriver ure_cd;
extern struct cfdriver uaq_cd;
extern struct cfdriver atu_cd;
extern struct cfdriver ural_cd;
extern struct cfdriver rum_cd;
extern struct cfdriver run_cd;
extern struct cfdriver zyd_cd;
extern struct cfdriver upgt_cd;
extern struct cfdriver urtw_cd;
extern struct cfdriver urtwn_cd;
extern struct cfdriver rsu_cd;
extern struct cfdriver otus_cd;
extern struct cfdriver uath_cd;
extern struct cfdriver iic_cd;
extern struct cfdriver ihidev_cd;
extern struct cfdriver ikbd_cd;
extern struct cfdriver acpi_cd;
extern struct cfdriver acpicmos_cd;
extern struct cfdriver acpihpet_cd;
extern struct cfdriver acpiec_cd;
extern struct cfdriver acpimadt_cd;
extern struct cfdriver acpiprt_cd;
extern struct cfdriver aplgpio_cd;
extern struct cfdriver bytgpio_cd;
extern struct cfdriver chvgpio_cd;
extern struct cfdriver glkgpio_cd;
extern struct cfdriver acpihve_cd;
extern struct cfdriver amdgpio_cd;
extern struct cfdriver acpipci_cd;
extern struct cfdriver sdmmc_cd;

extern struct cfattach softraid_ca;
extern struct cfattach nsphy_ca;
extern struct cfattach nsphyter_ca;
extern struct cfattach qsphy_ca;
extern struct cfattach inphy_ca;
extern struct cfattach iophy_ca;
extern struct cfattach eephy_ca;
extern struct cfattach exphy_ca;
extern struct cfattach rlphy_ca;
extern struct cfattach lxtphy_ca;
extern struct cfattach luphy_ca;
extern struct cfattach mtdphy_ca;
extern struct cfattach icsphy_ca;
extern struct cfattach sqphy_ca;
extern struct cfattach tqphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach dcphy_ca;
extern struct cfattach bmtphy_ca;
extern struct cfattach brgphy_ca;
extern struct cfattach xmphy_ca;
extern struct cfattach amphy_ca;
extern struct cfattach acphy_ca;
extern struct cfattach urlphy_ca;
extern struct cfattach rgephy_ca;
extern struct cfattach ciphy_ca;
extern struct cfattach ipgphy_ca;
extern struct cfattach etphy_ca;
extern struct cfattach jmphy_ca;
extern struct cfattach atphy_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach cd_ca;
extern struct cfattach sd_ca;
extern struct cfattach st_ca;
extern struct cfattach atapiscsi_ca;
extern struct cfattach wd_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach bios_ca;
extern struct cfattach mpbios_ca;
extern struct cfattach cpu_ca;
extern struct cfattach ioapic_ca;
extern struct cfattach efifb_ca;
extern struct cfattach pvbus_ca;
extern struct cfattach xen_ca;
extern struct cfattach xnf_ca;
extern struct cfattach xbf_ca;
extern struct cfattach hyperv_ca;
extern struct cfattach hvn_ca;
extern struct cfattach hvs_ca;
extern struct cfattach vio_ca;
extern struct cfattach vioblk_ca;
extern struct cfattach viornd_ca;
extern struct cfattach vioscsi_ca;
extern struct cfattach vmmci_ca;
extern struct cfattach pci_ca;
extern struct cfattach vga_pci_ca;
extern struct cfattach ahc_pci_ca;
extern struct cfattach ahd_pci_ca;
extern struct cfattach adw_pci_ca;
extern struct cfattach twe_pci_ca;
extern struct cfattach arc_ca;
extern struct cfattach jmb_ca;
extern struct cfattach ahci_pci_ca;
extern struct cfattach ahci_jmb_ca;
extern struct cfattach nvme_pci_ca;
extern struct cfattach ami_pci_ca;
extern struct cfattach mfi_pci_ca;
extern struct cfattach mfii_ca;
extern struct cfattach ips_ca;
extern struct cfattach gdt_pci_ca;
extern struct cfattach ciss_pci_ca;
extern struct cfattach qlw_pci_ca;
extern struct cfattach qla_pci_ca;
extern struct cfattach qle_ca;
extern struct cfattach mpi_pci_ca;
extern struct cfattach mpii_ca;
extern struct cfattach sili_pci_ca;
extern struct cfattach aq_ca;
extern struct cfattach de_ca;
extern struct cfattach ep_pci_ca;
extern struct cfattach pcn_ca;
extern struct cfattach siop_pci_ca;
extern struct cfattach pciide_pci_ca;
extern struct cfattach pciide_jmb_ca;
extern struct cfattach ppb_ca;
extern struct cfattach rl_pci_ca;
extern struct cfattach re_pci_ca;
extern struct cfattach vr_ca;
extern struct cfattach xl_pci_ca;
extern struct cfattach fxp_pci_ca;
extern struct cfattach em_ca;
extern struct cfattach ixgb_ca;
extern struct cfattach ix_ca;
extern struct cfattach ixl_ca;
extern struct cfattach xge_ca;
extern struct cfattach myx_ca;
extern struct cfattach oce_ca;
extern struct cfattach dc_pci_ca;
extern struct cfattach epic_pci_ca;
extern struct cfattach ne_pci_ca;
extern struct cfattach sf_pci_ca;
extern struct cfattach sis_ca;
extern struct cfattach se_ca;
extern struct cfattach uhci_pci_ca;
extern struct cfattach ohci_pci_ca;
extern struct cfattach ehci_pci_ca;
extern struct cfattach xhci_pci_ca;
extern struct cfattach cbb_pci_ca;
extern struct cfattach skc_ca;
extern struct cfattach sk_ca;
extern struct cfattach mskc_ca;
extern struct cfattach msk_ca;
extern struct cfattach wi_pci_ca;
extern struct cfattach iwi_ca;
extern struct cfattach wpi_ca;
extern struct cfattach iwn_ca;
extern struct cfattach iwm_ca;
extern struct cfattach iwx_ca;
extern struct cfattach iha_pci_ca;
extern struct cfattach pcscp_ca;
extern struct cfattach bge_ca;
extern struct cfattach bnx_ca;
extern struct cfattach vge_ca;
extern struct cfattach stge_ca;
extern struct cfattach nfe_ca;
extern struct cfattach et_ca;
extern struct cfattach jme_ca;
extern struct cfattach age_ca;
extern struct cfattach alc_ca;
extern struct cfattach ale_ca;
extern struct cfattach bce_ca;
extern struct cfattach ath_pci_ca;
extern struct cfattach athn_pci_ca;
extern struct cfattach atw_pci_ca;
extern struct cfattach rtw_pci_ca;
extern struct cfattach rtwn_pci_ca;
extern struct cfattach ral_pci_ca;
extern struct cfattach acx_pci_ca;
extern struct cfattach pgt_pci_ca;
extern struct cfattach malo_pci_ca;
extern struct cfattach bwi_pci_ca;
extern struct cfattach vic_ca;
extern struct cfattach vmx_ca;
extern struct cfattach vmwpvs_ca;
extern struct cfattach lii_ca;
extern struct cfattach sdhc_pci_ca;
extern struct cfattach rtsx_pci_ca;
extern struct cfattach xspd_ca;
extern struct cfattach virtio_pci_ca;
extern struct cfattach dwiic_pci_ca;
extern struct cfattach bwfm_pci_ca;
extern struct cfattach ccp_pci_ca;
extern struct cfattach bnxt_ca;
extern struct cfattach mcx_ca;
extern struct cfattach iavf_ca;
extern struct cfattach rge_ca;
extern struct cfattach pchb_ca;
extern struct cfattach cardslot_ca;
extern struct cfattach cardbus_ca;
extern struct cfattach xl_cardbus_ca;
extern struct cfattach dc_cardbus_ca;
extern struct cfattach fxp_cardbus_ca;
extern struct cfattach rl_cardbus_ca;
extern struct cfattach re_cardbus_ca;
extern struct cfattach ath_cardbus_ca;
extern struct cfattach athn_cardbus_ca;
extern struct cfattach atw_cardbus_ca;
extern struct cfattach rtw_cardbus_ca;
extern struct cfattach ral_cardbus_ca;
extern struct cfattach acx_cardbus_ca;
extern struct cfattach pgt_cardbus_ca;
extern struct cfattach ehci_cardbus_ca;
extern struct cfattach ohci_cardbus_ca;
extern struct cfattach malo_cardbus_ca;
extern struct cfattach bwi_cardbus_ca;
extern struct cfattach pcmcia_ca;
extern struct cfattach ep_pcmcia_ca;
extern struct cfattach ne_pcmcia_ca;
extern struct cfattach wdc_pcmcia_ca;
extern struct cfattach sm_pcmcia_ca;
extern struct cfattach xe_pcmcia_ca;
extern struct cfattach wi_pcmcia_ca;
extern struct cfattach malo_pcmcia_ca;
extern struct cfattach an_pcmcia_ca;
extern struct cfattach aapic_ca;
extern struct cfattach hme_pci_ca;
extern struct cfattach isa_ca;
extern struct cfattach com_isa_ca;
extern struct cfattach pckbc_isa_ca;
extern struct cfattach vga_isa_ca;
extern struct cfattach wdc_isa_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach pckbd_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach uhidev_ca;
extern struct cfattach ukbd_ca;
extern struct cfattach umass_ca;
extern struct cfattach aue_ca;
extern struct cfattach axe_ca;
extern struct cfattach axen_ca;
extern struct cfattach smsc_ca;
extern struct cfattach cue_ca;
extern struct cfattach kue_ca;
extern struct cfattach cdce_ca;
extern struct cfattach mos_ca;
extern struct cfattach mue_ca;
extern struct cfattach udav_ca;
extern struct cfattach upl_ca;
extern struct cfattach ugl_ca;
extern struct cfattach url_ca;
extern struct cfattach ure_ca;
extern struct cfattach uaq_ca;
extern struct cfattach wi_usb_ca;
extern struct cfattach atu_ca;
extern struct cfattach ural_ca;
extern struct cfattach rum_ca;
extern struct cfattach run_ca;
extern struct cfattach zyd_ca;
extern struct cfattach upgt_ca;
extern struct cfattach urtw_ca;
extern struct cfattach urtwn_ca;
extern struct cfattach rsu_ca;
extern struct cfattach otus_ca;
extern struct cfattach uath_ca;
extern struct cfattach athn_usb_ca;
extern struct cfattach bwfm_usb_ca;
extern struct cfattach iic_ca;
extern struct cfattach ihidev_ca;
extern struct cfattach ikbd_ca;
extern struct cfattach acpicmos_ca;
extern struct cfattach acpihpet_ca;
extern struct cfattach acpiec_ca;
extern struct cfattach acpimadt_ca;
extern struct cfattach acpiprt_ca;
extern struct cfattach aplgpio_ca;
extern struct cfattach bytgpio_ca;
extern struct cfattach chvgpio_ca;
extern struct cfattach glkgpio_ca;
extern struct cfattach sdhc_acpi_ca;
extern struct cfattach dwiic_acpi_ca;
extern struct cfattach acpihve_ca;
extern struct cfattach amdgpio_ca;
extern struct cfattach acpi_ca;
extern struct cfattach acpipci_ca;
extern struct cfattach sdmmc_ca;
extern struct cfattach bwfm_sdio_ca;


/* locators */
static long loc[54] = {
	-1, 0, -1, 0, -1, -1, -1, 0x3e8,
	0, -1, 0, 5, -1, -1, 0x3f8, 0,
	-1, 0, 4, -1, -1, 0x2f8, 0, -1,
	0, 3, -1, -1, 0x1f0, 0, -1, 0,
	0xe, -1, -1, 0x170, 0, -1, 0, 0xf,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, 1, 1, -1, 1,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"phy",
	"target",
	"lun",
	"channel",
	"apid",
	"bus",
	"dev",
	"function",
	"irq",
	"slot",
	"port",
	"size",
	"iomem",
	"iosiz",
	"drq",
	"drq2",
	"console",
	"primary",
	"mux",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"reportid",
	"addr",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 1,
	2, -1, 3, -1, 3, -1, 3, -1,
	3, -1, 3, -1, 4, -1, 5, -1,
	5, -1, 6, 7, -1, 7, 8, -1,
	6, 7, -1, 9, -1, 9, -1, 10,
	11, 12, 13, 8, 14, 15, -1, 16,
	17, 18, -1, 16, 17, 18, -1, 16,
	17, 18, -1, 16, 18, -1, 16, 18,
	-1, 16, 18, -1, 9, -1, 10, 19,
	20, 21, 22, 23, -1, 10, 19, 20,
	21, 22, 23, -1, 24, -1, 25, 11,
	-1, 24, -1,
};

/* size of parent vectors */
int pv_size = 163;

/* parent vectors */
short pv[163] = {
	216, 215, 212, 211, 210, 206, 205, 204, 203, 184, 181, 163, 139, 138, 137, 136,
	135, 134, 133, 132, 131, 130, 129, 119, 117, 108, 107, 89, 80, 76, 106, 104,
	105, 103, 180, 101, 102, 87, 88, 85, 86, 92, 93, 90, 91, 78, 79, -1,
	251, 202, 162, 128, 74, 72, 67, 66, 59, 51, 49, 47, 44, 33, 0, 127,
	81, 75, 73, 63, 62, 61, 71, 70, 65, 64, 69, 58, 68, 57, 56, 55,
	-1, 114, 112, 113, 109, 110, 111, -1, 191, 192, 179, 83, 82, -1, 245, 164,
	165, -1, 35, 84, 175, -1, 235, 201, 196, -1, 198, 199, -1, 190, 54, -1,
	246, 168, -1, 53, -1, 176, -1, 178, -1, 233, -1, 177, -1, 189, -1, 35,
	-1, 200, -1, 234, -1, 115, -1, 249, -1, 45, -1, 167, -1, 36, -1, 197,
	-1, 60, -1, 41, -1, 116, -1, 42, -1, 251, -1, 185, -1, 40, -1, 29,
	-1, 118, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: softraid0 at root */
    {&softraid_ca,	&softraid_cd,	 0, NORM,     loc,    0, pv+47, 0,    0},
/*  1: nsphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&nsphy_ca,		&nsphy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/*  2: nsphyter* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&nsphyter_ca,	&nsphyter_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/*  3: qsphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&qsphy_ca,		&qsphy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/*  4: inphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&inphy_ca,		&inphy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/*  5: iophy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&iophy_ca,		&iophy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/*  6: eephy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&eephy_ca,		&eephy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/*  7: exphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&exphy_ca,		&exphy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/*  8: rlphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&rlphy_ca,		&rlphy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/*  9: lxtphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&lxtphy_ca,	&lxtphy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/* 10: luphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&luphy_ca,		&luphy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/* 11: mtdphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&mtdphy_ca,	&mtdphy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/* 12: icsphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&icsphy_ca,	&icsphy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/* 13: sqphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&sqphy_ca,		&sqphy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/* 14: tqphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&tqphy_ca,		&tqphy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/* 15: ukphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/* 16: dcphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&dcphy_ca,		&dcphy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/* 17: bmtphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&bmtphy_ca,	&bmtphy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/* 18: brgphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&brgphy_ca,	&brgphy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/* 19: xmphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&xmphy_ca,		&xmphy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/* 20: amphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&amphy_ca,		&amphy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/* 21: acphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&acphy_ca,		&acphy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/* 22: urlphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&urlphy_ca,	&urlphy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/* 23: rgephy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&rgephy_ca,	&rgephy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/* 24: ciphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&ciphy_ca,		&ciphy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/* 25: ipgphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&ipgphy_ca,	&ipgphy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/* 26: etphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&etphy_ca,		&etphy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/* 27: jmphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&jmphy_ca,		&jmphy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/* 28: atphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|xe*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|msk*|sk*|se*|sis*|vr*|pcn*|aq*|sf*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep*|ep* phy -1 */
    {&atphy_ca,		&atphy_cd,	 0, STAR, loc+ 47,    0, pv+ 0, 1,    0},
/* 29: scsibus* at sdmmc*|umass*|vmwpvs*|pcscp*|mpii*|qle*|ips*|mfii*|arc*|vioscsi*|vioblk*|hvs*|xbf*|atapiscsi*|softraid0|iha*|siop*|sili*|mpi*|nvme*|ahci*|ahci*|qla*|qlw*|mfi*|ami*|ciss*|twe*|gdt*|adw*|ahd*|ahc* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+48, 94,    0},
/* 30: cd* at scsibus* target -1 lun -1 */
    {&cd_ca,		&cd_cd,		 0, STAR, loc+ 46,    0, pv+159, 95,    0},
/* 31: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+ 46,    0, pv+159, 95,    0},
/* 32: st* at scsibus* target -1 lun -1 */
    {&st_ca,		&st_cd,		 0, STAR, loc+ 46,    0, pv+159, 95,    0},
/* 33: atapiscsi* at wdc0|wdc1|wdc*|pciide*|pciide* channel -1 */
    {&atapiscsi_ca,	&atapiscsi_cd,	 0, STAR, loc+ 47,    0, pv+88, 98,    0},
/* 34: wd* at wdc0|wdc1|wdc*|pciide*|pciide* channel -1 drive -1 */
    {&wd_ca,		&wd_cd,		 0, STAR, loc+ 46,    0, pv+88, 98,    0},
/* 35: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+47, 0,    0},
/* 36: bios0 at mainbus0 apid -1 */
    {&bios_ca,		&bios_cd,	 0, NORM, loc+ 47,    0, pv+127, 108,    0},
/* 37: mpbios0 at bios0 */
    {&mpbios_ca,	&mpbios_cd,	 0, NORM,     loc,    0, pv+141, 109,    0},
/* 38: cpu0 at mainbus0 apid -1 */
    {&cpu_ca,		&cpu_cd,	 0, NORM, loc+ 47,    0, pv+127, 108,    0},
/* 39: ioapic* at mainbus0 apid -1 */
    {&ioapic_ca,	&ioapic_cd,	 0, STAR, loc+ 47,    0, pv+127, 108,    0},
/* 40: efifb0 at mainbus0 apid -1 */
    {&efifb_ca,		&efifb_cd,	 0, NORM, loc+ 47,    0, pv+127, 108,    0},
/* 41: pvbus0 at mainbus0 apid -1 */
    {&pvbus_ca,		&pvbus_cd,	 0, NORM, loc+ 47,    0, pv+127, 108,    0},
/* 42: xen0 at pvbus0 */
    {&xen_ca,		&xen_cd,	 0, NORM,     loc,    0, pv+147, 109,    0},
/* 43: xnf* at xen0 */
    {&xnf_ca,		&xnf_cd,	 0, STAR,     loc,    0, pv+151, 109,    0},
/* 44: xbf* at xen0 */
    {&xbf_ca,		&xbf_cd,	 0, STAR,     loc,    0, pv+151, 109,    0},
/* 45: hyperv0 at pvbus0 */
    {&hyperv_ca,	&hyperv_cd,	 0, NORM,     loc,    0, pv+147, 109,    0},
/* 46: hvn* at hyperv0 */
    {&hvn_ca,		&hvn_cd,	 0, STAR,     loc,    0, pv+137, 109,    0},
/* 47: hvs* at hyperv0 */
    {&hvs_ca,		&hvs_cd,	 0, STAR,     loc,    0, pv+137, 109,    0},
/* 48: vio* at virtio* */
    {&vio_ca,		&vio_cd,	 0, STAR,     loc,    0, pv+139, 109,    0},
/* 49: vioblk* at virtio* */
    {&vioblk_ca,	&vioblk_cd,	 0, STAR,     loc,    0, pv+139, 109,    0},
/* 50: viornd* at virtio* */
    {&viornd_ca,	&viornd_cd,	 0, STAR,     loc,    0, pv+139, 109,    0},
/* 51: vioscsi* at virtio* */
    {&vioscsi_ca,	&vioscsi_cd,	 0, STAR,     loc,    0, pv+139, 109,    0},
/* 52: vmmci* at virtio* */
    {&vmmci_ca,		&vmmci_cd,	 0, STAR,     loc,    0, pv+139, 109,    0},
/* 53: pci* at mainbus0|ppb*|pchb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+ 47,    0, pv+98, 108,    0},
/* 54: vga* at pci* dev -1 function -1 */
    {&vga_pci_ca,	&vga_cd,	 1, STAR, loc+ 46,    0, pv+115, 114,    1},
/* 55: ahc* at pci* dev -1 function -1 */
    {&ahc_pci_ca,	&ahc_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 56: ahd* at pci* dev -1 function -1 */
    {&ahd_pci_ca,	&ahd_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 57: adw* at pci* dev -1 function -1 */
    {&adw_pci_ca,	&adw_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 58: twe* at pci* dev -1 function -1 */
    {&twe_pci_ca,	&twe_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 59: arc* at pci* dev -1 function -1 */
    {&arc_ca,		&arc_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 60: jmb* at pci* dev -1 function -1 */
    {&jmb_ca,		&jmb_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 61: ahci* at pci* dev -1 function -1 */
    {&ahci_pci_ca,	&ahci_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 62: ahci* at jmb* */
    {&ahci_jmb_ca,	&ahci_cd,	 0, STAR,     loc,    0, pv+145, 116,    0},
/* 63: nvme* at pci* dev -1 function -1 */
    {&nvme_pci_ca,	&nvme_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 64: ami* at pci* dev -1 function -1 */
    {&ami_pci_ca,	&ami_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 65: mfi* at pci* dev -1 function -1 */
    {&mfi_pci_ca,	&mfi_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 66: mfii* at pci* dev -1 function -1 */
    {&mfii_ca,		&mfii_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 67: ips* at pci* dev -1 function -1 */
    {&ips_ca,		&ips_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 68: gdt* at pci* dev -1 function -1 */
    {&gdt_pci_ca,	&gdt_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 69: ciss* at pci* dev -1 function -1 */
    {&ciss_pci_ca,	&ciss_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 70: qlw* at pci* dev -1 function -1 */
    {&qlw_pci_ca,	&qlw_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 71: qla* at pci* dev -1 function -1 */
    {&qla_pci_ca,	&qla_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 72: qle* at pci* dev -1 function -1 */
    {&qle_ca,		&qle_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 73: mpi* at pci* dev -1 function -1 */
    {&mpi_pci_ca,	&mpi_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 74: mpii* at pci* dev -1 function -1 */
    {&mpii_ca,		&mpii_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 75: sili* at pci* dev -1 function -1 */
    {&sili_pci_ca,	&sili_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 76: aq* at pci* dev -1 function -1 */
    {&aq_ca,		&aq_cd,		 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 77: de* at pci* dev -1 function -1 */
    {&de_ca,		&de_cd,		 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 78: ep* at pci* dev -1 function -1 */
    {&ep_pci_ca,	&ep_cd,		 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 79: ep* at pcmcia* function -1 irq -1 */
    {&ep_pcmcia_ca,	&ep_cd,		 0, STAR, loc+ 46,    0, pv+119, 117,    0},
/* 80: pcn* at pci* dev -1 function -1 */
    {&pcn_ca,		&pcn_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 81: siop* at pci* dev -1 function -1 */
    {&siop_pci_ca,	&siop_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 82: pciide* at pci* dev -1 function -1 */
    {&pciide_pci_ca,	&pciide_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 83: pciide* at jmb* */
    {&pciide_jmb_ca,	&pciide_cd,	 0, STAR,     loc,    0, pv+145, 116,    0},
/* 84: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 85: rl* at pci* dev -1 function -1 */
    {&rl_pci_ca,	&rl_cd,		 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 86: rl* at cardbus* dev -1 function -1 */
    {&rl_cardbus_ca,	&rl_cd,		 0, STAR, loc+ 46,    0, pv+123, 120,    0},
/* 87: re* at pci* dev -1 function -1 */
    {&re_pci_ca,	&re_cd,		 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 88: re* at cardbus* dev -1 function -1 */
    {&re_cardbus_ca,	&re_cd,		 0, STAR, loc+ 46,    0, pv+123, 120,    0},
/* 89: vr* at pci* dev -1 function -1 */
    {&vr_ca,		&vr_cd,		 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 90: xl* at pci* dev -1 function -1 */
    {&xl_pci_ca,	&xl_cd,		 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 91: xl* at cardbus* dev -1 function -1 */
    {&xl_cardbus_ca,	&xl_cd,		 0, STAR, loc+ 46,    0, pv+123, 120,    0},
/* 92: fxp* at pci* dev -1 function -1 */
    {&fxp_pci_ca,	&fxp_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 93: fxp* at cardbus* dev -1 function -1 */
    {&fxp_cardbus_ca,	&fxp_cd,	 0, STAR, loc+ 46,    0, pv+123, 120,    0},
/* 94: em* at pci* dev -1 function -1 */
    {&em_ca,		&em_cd,		 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 95: ixgb* at pci* dev -1 function -1 */
    {&ixgb_ca,		&ixgb_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 96: ix* at pci* dev -1 function -1 */
    {&ix_ca,		&ix_cd,		 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 97: ixl* at pci* dev -1 function -1 */
    {&ixl_ca,		&ixl_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 98: xge* at pci* dev -1 function -1 */
    {&xge_ca,		&xge_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/* 99: myx* at pci* dev -1 function -1 */
    {&myx_ca,		&myx_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*100: oce* at pci* dev -1 function -1 */
    {&oce_ca,		&oce_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*101: dc* at pci* dev -1 function -1 */
    {&dc_pci_ca,	&dc_cd,		 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*102: dc* at cardbus* dev -1 function -1 */
    {&dc_cardbus_ca,	&dc_cd,		 0, STAR, loc+ 46,    0, pv+123, 120,    0},
/*103: epic* at pci* dev -1 function -1 */
    {&epic_pci_ca,	&epic_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*104: ne* at pci* dev -1 function -1 */
    {&ne_pci_ca,	&ne_cd,		 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*105: ne* at pcmcia* function -1 irq -1 */
    {&ne_pcmcia_ca,	&ne_cd,		 0, STAR, loc+ 46,    0, pv+119, 117,    0},
/*106: sf* at pci* dev -1 function -1 */
    {&sf_pci_ca,	&sf_cd,		 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*107: sis* at pci* dev -1 function -1 */
    {&sis_ca,		&sis_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*108: se* at pci* dev -1 function -1 */
    {&se_ca,		&se_cd,		 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*109: uhci* at pci* dev -1 function -1 */
    {&uhci_pci_ca,	&uhci_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*110: ohci* at pci* dev -1 function -1 */
    {&ohci_pci_ca,	&ohci_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*111: ohci* at cardbus* dev -1 function -1 */
    {&ohci_cardbus_ca,	&ohci_cd,	 0, STAR, loc+ 46,    0, pv+123, 120,    0},
/*112: ehci* at pci* dev -1 function -1 */
    {&ehci_pci_ca,	&ehci_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*113: ehci* at cardbus* dev -1 function -1 */
    {&ehci_cardbus_ca,	&ehci_cd,	 0, STAR, loc+ 46,    0, pv+123, 120,    0},
/*114: xhci* at pci* dev -1 function -1 */
    {&xhci_pci_ca,	&xhci_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*115: cbb* at pci* dev -1 function -1 */
    {&cbb_pci_ca,	&cbb_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*116: skc* at pci* dev -1 function -1 */
    {&skc_ca,		&skc_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*117: sk* at skc* */
    {&sk_ca,		&sk_cd,		 0, STAR,     loc,    0, pv+149, 122,    0},
/*118: mskc* at pci* dev -1 function -1 */
    {&mskc_ca,		&mskc_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*119: msk* at mskc* */
    {&msk_ca,		&msk_cd,	 0, STAR,     loc,    0, pv+161, 122,    0},
/*120: wi* at pci* dev -1 function -1 */
    {&wi_pci_ca,	&wi_cd,		 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*121: wi* at pcmcia* function -1 irq -1 */
    {&wi_pcmcia_ca,	&wi_cd,		 0, STAR, loc+ 46,    0, pv+119, 117,    0},
/*122: iwi* at pci* dev -1 function -1 */
    {&iwi_ca,		&iwi_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*123: wpi* at pci* dev -1 function -1 */
    {&wpi_ca,		&wpi_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*124: iwn* at pci* dev -1 function -1 */
    {&iwn_ca,		&iwn_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*125: iwm* at pci* dev -1 function -1 */
    {&iwm_ca,		&iwm_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*126: iwx* at pci* dev -1 function -1 */
    {&iwx_ca,		&iwx_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*127: iha* at pci* dev -1 function -1 */
    {&iha_pci_ca,	&iha_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*128: pcscp* at pci* dev -1 function -1 */
    {&pcscp_ca,		&pcscp_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*129: bge* at pci* dev -1 function -1 */
    {&bge_ca,		&bge_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*130: bnx* at pci* dev -1 function -1 */
    {&bnx_ca,		&bnx_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*131: vge* at pci* dev -1 function -1 */
    {&vge_ca,		&vge_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*132: stge* at pci* dev -1 function -1 */
    {&stge_ca,		&stge_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*133: nfe* at pci* dev -1 function -1 */
    {&nfe_ca,		&nfe_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*134: et* at pci* dev -1 function -1 */
    {&et_ca,		&et_cd,		 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*135: jme* at pci* dev -1 function -1 */
    {&jme_ca,		&jme_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*136: age* at pci* dev -1 function -1 */
    {&age_ca,		&age_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*137: alc* at pci* dev -1 function -1 */
    {&alc_ca,		&alc_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*138: ale* at pci* dev -1 function -1 */
    {&ale_ca,		&ale_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*139: bce* at pci* dev -1 function -1 */
    {&bce_ca,		&bce_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*140: ath* at pci* dev -1 function -1 */
    {&ath_pci_ca,	&ath_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*141: ath* at cardbus* dev -1 function -1 */
    {&ath_cardbus_ca,	&ath_cd,	 0, STAR, loc+ 46,    0, pv+123, 120,    0},
/*142: athn* at pci* dev -1 function -1 */
    {&athn_pci_ca,	&athn_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*143: athn* at cardbus* dev -1 function -1 */
    {&athn_cardbus_ca,	&athn_cd,	 0, STAR, loc+ 46,    0, pv+123, 120,    0},
/*144: atw* at pci* dev -1 function -1 */
    {&atw_pci_ca,	&atw_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*145: atw* at cardbus* dev -1 function -1 */
    {&atw_cardbus_ca,	&atw_cd,	 0, STAR, loc+ 46,    0, pv+123, 120,    0},
/*146: rtw* at pci* dev -1 function -1 */
    {&rtw_pci_ca,	&rtw_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*147: rtw* at cardbus* dev -1 function -1 */
    {&rtw_cardbus_ca,	&rtw_cd,	 0, STAR, loc+ 46,    0, pv+123, 120,    0},
/*148: rtwn* at pci* dev -1 function -1 */
    {&rtwn_pci_ca,	&rtwn_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*149: ral* at pci* dev -1 function -1 */
    {&ral_pci_ca,	&ral_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*150: ral* at cardbus* dev -1 function -1 */
    {&ral_cardbus_ca,	&ral_cd,	 0, STAR, loc+ 46,    0, pv+123, 120,    0},
/*151: acx* at pci* dev -1 function -1 */
    {&acx_pci_ca,	&acx_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*152: acx* at cardbus* dev -1 function -1 */
    {&acx_cardbus_ca,	&acx_cd,	 0, STAR, loc+ 46,    0, pv+123, 120,    0},
/*153: pgt* at pci* dev -1 function -1 */
    {&pgt_pci_ca,	&pgt_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*154: pgt* at cardbus* dev -1 function -1 */
    {&pgt_cardbus_ca,	&pgt_cd,	 0, STAR, loc+ 46,    0, pv+123, 120,    0},
/*155: malo* at pci* dev -1 function -1 */
    {&malo_pci_ca,	&malo_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*156: malo* at cardbus* dev -1 function -1 */
    {&malo_cardbus_ca,	&malo_cd,	 0, STAR, loc+ 46,    0, pv+123, 120,    0},
/*157: malo* at pcmcia* function -1 irq -1 */
    {&malo_pcmcia_ca,	&malo_cd,	 0, STAR, loc+ 46,    0, pv+119, 117,    0},
/*158: bwi* at pci* dev -1 function -1 */
    {&bwi_pci_ca,	&bwi_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*159: bwi* at cardbus* dev -1 function -1 */
    {&bwi_cardbus_ca,	&bwi_cd,	 0, STAR, loc+ 46,    0, pv+123, 120,    0},
/*160: vic* at pci* dev -1 function -1 */
    {&vic_ca,		&vic_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*161: vmx* at pci* dev -1 function -1 */
    {&vmx_ca,		&vmx_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*162: vmwpvs* at pci* dev -1 function -1 */
    {&vmwpvs_ca,	&vmwpvs_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*163: lii* at pci* dev -1 function -1 */
    {&lii_ca,		&lii_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*164: sdhc* at pci* dev -1 function -1 */
    {&sdhc_pci_ca,	&sdhc_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*165: rtsx* at pci* dev -1 function -1 */
    {&rtsx_pci_ca,	&rtsx_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*166: xspd0 at pci* dev -1 function -1 */
    {&xspd_ca,		&xspd_cd,	 0, NORM, loc+ 46,    0, pv+115, 114,    0},
/*167: virtio* at pci* dev -1 function -1 */
    {&virtio_pci_ca,	&virtio_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*168: dwiic* at pci* dev -1 function -1 */
    {&dwiic_pci_ca,	&dwiic_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*169: bwfm* at pci* dev -1 function -1 */
    {&bwfm_pci_ca,	&bwfm_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*170: ccp* at pci* dev -1 function -1 */
    {&ccp_pci_ca,	&ccp_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*171: bnxt* at pci* dev -1 function -1 */
    {&bnxt_ca,		&bnxt_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*172: mcx* at pci* dev -1 function -1 */
    {&mcx_ca,		&mcx_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*173: iavf* at pci* dev -1 function -1 */
    {&iavf_ca,		&iavf_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*174: rge* at pci* dev -1 function -1 */
    {&rge_ca,		&rge_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*175: pchb* at pci* dev -1 function -1 */
    {&pchb_ca,		&pchb_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*176: cardslot* at cbb* slot -1 */
    {&cardslot_ca,	&cardslot_cd,	 0, STAR, loc+ 47,    0, pv+133, 123,    0},
/*177: cardbus* at cardslot* slot -1 */
    {&cardbus_ca,	&cardbus_cd,	 0, STAR, loc+ 47,    0, pv+117, 125,    0},
/*178: pcmcia* at cardslot* controller -1 socket -1 */
    {&pcmcia_ca,	&pcmcia_cd,	 0, STAR, loc+ 46,    0, pv+117, 125,    0},
/*179: wdc* at pcmcia* function -1 irq -1 */
    {&wdc_pcmcia_ca,	&wdc_cd,	 2, STAR, loc+ 46,    0, pv+119, 117,    2},
/*180: sm* at pcmcia* function -1 irq -1 */
    {&sm_pcmcia_ca,	&sm_cd,		 0, STAR, loc+ 46,    0, pv+119, 117,    0},
/*181: xe* at pcmcia* function -1 irq -1 */
    {&xe_pcmcia_ca,	&xe_cd,		 0, STAR, loc+ 46,    0, pv+119, 117,    0},
/*182: an* at pcmcia* function -1 irq -1 */
    {&an_pcmcia_ca,	&an_cd,		 0, STAR, loc+ 46,    0, pv+119, 117,    0},
/*183: aapic* at pci* dev -1 function -1 */
    {&aapic_ca,		&aapic_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*184: hme* at pci* dev -1 function -1 */
    {&hme_pci_ca,	&hme_cd,	 0, STAR, loc+ 46,    0, pv+115, 114,    0},
/*185: isa0 at mainbus0 */
    {&isa_ca,		&isa_cd,	 0, NORM,     loc,    0, pv+127, 108,    0},
/*186: com0 at isa0 port 0x3f8 size 0 iomem -1 iosiz 0 irq 4 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 0, NORM, loc+ 14,    0, pv+155, 127,    0},
/*187: com1 at isa0 port 0x2f8 size 0 iomem -1 iosiz 0 irq 3 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 1, NORM, loc+ 21,    0, pv+155, 127,    1},
/*188: com2 at isa0 port 0x3e8 size 0 iomem -1 iosiz 0 irq 5 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 2, NORM, loc+  7,    0, pv+155, 127,    2},
/*189: pckbc0 at isa0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&pckbc_isa_ca,	&pckbc_cd,	 0, NORM, loc+  0,    0, pv+155, 127,    0},
/*190: vga0 at isa0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&vga_isa_ca,	&vga_cd,	 0, NORM, loc+  0,    0, pv+155, 127,    0},
/*191: wdc0 at isa0 port 0x1f0 size 0 iomem -1 iosiz 0 irq 0xe drq -1 drq2 -1 */
    {&wdc_isa_ca,	&wdc_cd,	 0, DNRM, loc+ 28,    0, pv+155, 127,    0},
/*192: wdc1 at isa0 port 0x170 size 0 iomem -1 iosiz 0 irq 0xf drq -1 drq2 -1 */
    {&wdc_isa_ca,	&wdc_cd,	 1, DNRM, loc+ 35,    0, pv+155, 127,    1},
/*193: wsdisplay* at vga0|vga* console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 1, STAR, loc+ 48,    0, pv+109, 135,    1},
/*194: wsdisplay0 at efifb0 console 1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, NORM, loc+ 51,    0, pv+157, 143,    0},
/*195: wskbd* at ikbd*|ukbd*|pckbd* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+ 52,    0, pv+102, 147,    0},
/*196: pckbd* at pckbc0 slot -1 */
    {&pckbd_ca,		&pckbd_cd,	 0, STAR, loc+ 47,    0, pv+125, 156,    0},
/*197: usb* at xhci*|ehci*|ehci*|uhci*|ohci*|ohci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+81, 157,    0},
/*198: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+143, 157,    0},
/*199: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*200: uhidev* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhidev_ca,	&uhidev_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*201: ukbd* at uhidev* reportid -1 */
    {&ukbd_ca,		&ukbd_cd,	 0, STAR, loc+ 47,    0, pv+129, 172,    0},
/*202: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*203: aue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&aue_ca,		&aue_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*204: axe* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axe_ca,		&axe_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*205: axen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axen_ca,		&axen_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*206: smsc* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&smsc_ca,		&smsc_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*207: cue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cue_ca,		&cue_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*208: kue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&kue_ca,		&kue_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*209: cdce* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cdce_ca,		&cdce_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*210: mos* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mos_ca,		&mos_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*211: mue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mue_ca,		&mue_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*212: udav* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udav_ca,		&udav_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*213: upl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upl_ca,		&upl_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*214: ugl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugl_ca,		&ugl_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*215: url* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&url_ca,		&url_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*216: ure* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ure_ca,		&ure_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*217: uaq* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uaq_ca,		&uaq_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*218: wi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&wi_usb_ca,	&wi_cd,		 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*219: atu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&atu_ca,		&atu_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*220: ural* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ural_ca,		&ural_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*221: rum* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rum_ca,		&rum_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*222: run* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&run_ca,		&run_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*223: zyd* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&zyd_ca,		&zyd_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*224: upgt* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upgt_ca,		&upgt_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*225: urtw* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtw_ca,		&urtw_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*226: urtwn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtwn_ca,		&urtwn_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*227: rsu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rsu_ca,		&rsu_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*228: otus* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&otus_ca,		&otus_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*229: uath* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uath_ca,		&uath_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*230: athn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&athn_usb_ca,	&athn_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*231: bwfm* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&bwfm_usb_ca,	&bwfm_cd,	 0, STAR, loc+ 42,    0, pv+106, 158,    0},
/*232: bwfm* at sdmmc* */
    {&bwfm_sdio_ca,	&bwfm_cd,	 0, STAR,     loc,    0, pv+153, 94,    0},
/*233: iic* at dwiic*|dwiic* */
    {&iic_ca,		&iic_cd,	 0, STAR,     loc,    0, pv+112, 173,    0},
/*234: ihidev* at iic* addr -1 size -1 */
    {&ihidev_ca,	&ihidev_cd,	 0, STAR, loc+ 46,    0, pv+121, 174,    0},
/*235: ikbd* at ihidev* reportid -1 */
    {&ikbd_ca,		&ikbd_cd,	 0, STAR, loc+ 47,    0, pv+131, 177,    0},
/*236: acpicmos* at acpi0 */
    {&acpicmos_ca,	&acpicmos_cd,	 0, STAR,     loc,    0, pv+135, 178,    0},
/*237: acpihpet* at acpi0 */
    {&acpihpet_ca,	&acpihpet_cd,	 0, STAR,     loc,    0, pv+135, 178,    0},
/*238: acpiec* at acpi0 */
    {&acpiec_ca,	&acpiec_cd,	 0, STAR,     loc,    0, pv+135, 178,    0},
/*239: acpimadt0 at acpi0 */
    {&acpimadt_ca,	&acpimadt_cd,	 0, NORM,     loc,    0, pv+135, 178,    0},
/*240: acpiprt* at acpi0 */
    {&acpiprt_ca,	&acpiprt_cd,	 0, STAR,     loc,    0, pv+135, 178,    0},
/*241: aplgpio* at acpi0 */
    {&aplgpio_ca,	&aplgpio_cd,	 0, STAR,     loc,    0, pv+135, 178,    0},
/*242: bytgpio* at acpi0 */
    {&bytgpio_ca,	&bytgpio_cd,	 0, STAR,     loc,    0, pv+135, 178,    0},
/*243: chvgpio* at acpi0 */
    {&chvgpio_ca,	&chvgpio_cd,	 0, STAR,     loc,    0, pv+135, 178,    0},
/*244: glkgpio* at acpi0 */
    {&glkgpio_ca,	&glkgpio_cd,	 0, STAR,     loc,    0, pv+135, 178,    0},
/*245: sdhc* at acpi0 */
    {&sdhc_acpi_ca,	&sdhc_cd,	 0, STAR,     loc,    0, pv+135, 178,    0},
/*246: dwiic* at acpi0 */
    {&dwiic_acpi_ca,	&dwiic_cd,	 0, STAR,     loc,    0, pv+135, 178,    0},
/*247: acpihve* at acpi0 */
    {&acpihve_ca,	&acpihve_cd,	 0, STAR,     loc,    0, pv+135, 178,    0},
/*248: amdgpio* at acpi0 */
    {&amdgpio_ca,	&amdgpio_cd,	 0, STAR,     loc,    0, pv+135, 178,    0},
/*249: acpi0 at bios0 */
    {&acpi_ca,		&acpi_cd,	 0, NORM,     loc,    0, pv+141, 109,    0},
/*250: acpipci* at acpi0 */
    {&acpipci_ca,	&acpipci_cd,	 0, STAR,     loc,    0, pv+135, 178,    0},
/*251: sdmmc* at sdhc*|sdhc*|rtsx* */
    {&sdmmc_ca,		&sdmmc_cd,	 0, STAR,     loc,    0, pv+94, 178,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 0 /* softraid0 */,
	35 /* mainbus0 */,
	-1
};

int cfroots_size = 3;

/* pseudo-devices */
extern void loopattach(int);
extern void vlanattach(int);
extern void trunkattach(int);
extern void bpfilterattach(int);
extern void rdattach(int);
extern void wsmuxattach(int);
extern void bioattach(int);

char *pdevnames[] = {
	"loop",
	"vlan",
	"trunk",
	"bpfilter",
	"rd",
	"wsmux",
	"bio",
};

int pdevnames_size = 7;

struct pdevinit pdevinit[] = {
	{ loopattach, 1 },
	{ vlanattach, 1 },
	{ trunkattach, 1 },
	{ bpfilterattach, 1 },
	{ rdattach, 1 },
	{ wsmuxattach, 2 },
	{ bioattach, 1 },
	{ NULL, 0 }
};
