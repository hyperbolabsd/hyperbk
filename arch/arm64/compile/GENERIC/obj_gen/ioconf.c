/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/arm64/conf/GENERIC"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver video_cd;
extern struct cfdriver audio_cd;
extern struct cfdriver midi_cd;
extern struct cfdriver drm_cd;
extern struct cfdriver mfi_cd;
extern struct cfdriver ahci_cd;
extern struct cfdriver nvme_cd;
extern struct cfdriver mpi_cd;
extern struct cfdriver wi_cd;
extern struct cfdriver re_cd;
extern struct cfdriver bse_cd;
extern struct cfdriver com_cd;
extern struct cfdriver pluart_cd;
extern struct cfdriver athn_cd;
extern struct cfdriver bwfm_cd;
extern struct cfdriver virtio_cd;
extern struct cfdriver uhci_cd;
extern struct cfdriver ohci_cd;
extern struct cfdriver ehci_cd;
extern struct cfdriver xhci_cd;
extern struct cfdriver ccp_cd;
extern struct cfdriver sdhc_cd;
extern struct cfdriver radio_cd;
extern struct cfdriver ipmi_cd;
extern struct cfdriver vscsi_cd;
extern struct cfdriver softraid_cd;
extern struct cfdriver dwiic_cd;
extern struct cfdriver imxiic_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver cd_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver simplebus_cd;
extern struct cfdriver efi_cd;
extern struct cfdriver smbios_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver ch_cd;
extern struct cfdriver st_cd;
extern struct cfdriver uk_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver wsmouse_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver iic_cd;
extern struct cfdriver pcamux_cd;
extern struct cfdriver maxrtc_cd;
extern struct cfdriver dsxrtc_cd;
extern struct cfdriver pcfrtc_cd;
extern struct cfdriver pcxrtc_cd;
extern struct cfdriver islrtc_cd;
extern struct cfdriver abcrtc_cd;
extern struct cfdriver mcprtc_cd;
extern struct cfdriver mfokrtc_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver uaudio_cd;
extern struct cfdriver uvideo_cd;
extern struct cfdriver utvfu_cd;
extern struct cfdriver udl_cd;
extern struct cfdriver umidi_cd;
extern struct cfdriver ucom_cd;
extern struct cfdriver ugen_cd;
extern struct cfdriver uhidev_cd;
extern struct cfdriver uhid_cd;
extern struct cfdriver fido_cd;
extern struct cfdriver ujoy_cd;
extern struct cfdriver ukbd_cd;
extern struct cfdriver ums_cd;
extern struct cfdriver umt_cd;
extern struct cfdriver uts_cd;
extern struct cfdriver ucycom_cd;
extern struct cfdriver uslhcom_cd;
extern struct cfdriver ulpt_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver uthum_cd;
extern struct cfdriver ugold_cd;
extern struct cfdriver utrh_cd;
extern struct cfdriver uoakrh_cd;
extern struct cfdriver uoaklux_cd;
extern struct cfdriver uoakv_cd;
extern struct cfdriver uonerng_cd;
extern struct cfdriver urng_cd;
extern struct cfdriver udcf_cd;
extern struct cfdriver uvisor_cd;
extern struct cfdriver udsbr_cd;
extern struct cfdriver utwitch_cd;
extern struct cfdriver aue_cd;
extern struct cfdriver axe_cd;
extern struct cfdriver axen_cd;
extern struct cfdriver smsc_cd;
extern struct cfdriver cue_cd;
extern struct cfdriver kue_cd;
extern struct cfdriver cdce_cd;
extern struct cfdriver urndis_cd;
extern struct cfdriver mos_cd;
extern struct cfdriver mue_cd;
extern struct cfdriver udav_cd;
extern struct cfdriver upl_cd;
extern struct cfdriver ugl_cd;
extern struct cfdriver url_cd;
extern struct cfdriver ure_cd;
extern struct cfdriver umodem_cd;
extern struct cfdriver uftdi_cd;
extern struct cfdriver uplcom_cd;
extern struct cfdriver umct_cd;
extern struct cfdriver uvscom_cd;
extern struct cfdriver ubsa_cd;
extern struct cfdriver uslcom_cd;
extern struct cfdriver uark_cd;
extern struct cfdriver moscom_cd;
extern struct cfdriver umcs_cd;
extern struct cfdriver uscom_cd;
extern struct cfdriver ucrcom_cd;
extern struct cfdriver uxrcom_cd;
extern struct cfdriver uipaq_cd;
extern struct cfdriver umsm_cd;
extern struct cfdriver uchcom_cd;
extern struct cfdriver uticom_cd;
extern struct cfdriver atu_cd;
extern struct cfdriver ural_cd;
extern struct cfdriver rum_cd;
extern struct cfdriver run_cd;
extern struct cfdriver zyd_cd;
extern struct cfdriver upgt_cd;
extern struct cfdriver urtw_cd;
extern struct cfdriver urtwn_cd;
extern struct cfdriver rsu_cd;
extern struct cfdriver otus_cd;
extern struct cfdriver umb_cd;
extern struct cfdriver uath_cd;
extern struct cfdriver uow_cd;
extern struct cfdriver uberry_cd;
extern struct cfdriver upd_cd;
extern struct cfdriver uwacom_cd;
extern struct cfdriver uhidpp_cd;
extern struct cfdriver ucc_cd;
extern struct cfdriver gpio_cd;
extern struct cfdriver gpioiic_cd;
extern struct cfdriver gpioow_cd;
extern struct cfdriver onewire_cd;
extern struct cfdriver owid_cd;
extern struct cfdriver owsbm_cd;
extern struct cfdriver owtemp_cd;
extern struct cfdriver owctr_cd;
extern struct cfdriver vio_cd;
extern struct cfdriver vioblk_cd;
extern struct cfdriver viomb_cd;
extern struct cfdriver viornd_cd;
extern struct cfdriver vioscsi_cd;
extern struct cfdriver eephy_cd;
extern struct cfdriver rlphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver brgphy_cd;
extern struct cfdriver amphy_cd;
extern struct cfdriver urlphy_cd;
extern struct cfdriver rgephy_cd;
extern struct cfdriver atphy_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver mfii_cd;
extern struct cfdriver mpii_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver em_cd;
extern struct cfdriver ix_cd;
extern struct cfdriver ixl_cd;
extern struct cfdriver mskc_cd;
extern struct cfdriver msk_cd;
extern struct cfdriver iwx_cd;
extern struct cfdriver bge_cd;
extern struct cfdriver bnxt_cd;
extern struct cfdriver mcx_cd;
extern struct cfdriver rge_cd;
extern struct cfdriver radeondrm_cd;
extern struct cfdriver amdgpu_cd;
extern struct cfdriver sdmmc_cd;
extern struct cfdriver pinctrl_cd;
extern struct cfdriver graphaudio_cd;
extern struct cfdriver simpleamp_cd;
extern struct cfdriver simpleaudio_cd;
extern struct cfdriver simplefb_cd;
extern struct cfdriver simplepanel_cd;
extern struct cfdriver sxiccmu_cd;
extern struct cfdriver sxidog_cd;
extern struct cfdriver sxipio_cd;
extern struct cfdriver sxirsb_cd;
extern struct cfdriver sxipwm_cd;
extern struct cfdriver sxirtc_cd;
extern struct cfdriver sximmc_cd;
extern struct cfdriver sxisid_cd;
extern struct cfdriver sxisyscon_cd;
extern struct cfdriver sxitemp_cd;
extern struct cfdriver sxitwi_cd;
extern struct cfdriver axppmic_cd;
extern struct cfdriver fanpwr_cd;
extern struct cfdriver sypwr_cd;
extern struct cfdriver bcmaux_cd;
extern struct cfdriver bcmbsc_cd;
extern struct cfdriver bcmclock_cd;
extern struct cfdriver bcmdmac_cd;
extern struct cfdriver bcmdog_cd;
extern struct cfdriver bcmgpio_cd;
extern struct cfdriver bcmirng_cd;
extern struct cfdriver bcmmbox_cd;
extern struct cfdriver bcmpcie_cd;
extern struct cfdriver bcmrng_cd;
extern struct cfdriver bcmsdhost_cd;
extern struct cfdriver bcmtemp_cd;
extern struct cfdriver bcmtmon_cd;
extern struct cfdriver dwctwo_cd;
extern struct cfdriver exuart_cd;
extern struct cfdriver plgpio_cd;
extern struct cfdriver plrtc_cd;
extern struct cfdriver psci_cd;
extern struct cfdriver dwge_cd;
extern struct cfdriver syscon_cd;
extern struct cfdriver pwmbl_cd;
extern struct cfdriver pwmfan_cd;
extern struct cfdriver pwmreg_cd;
extern struct cfdriver amlclock_cd;
extern struct cfdriver amldwusb_cd;
extern struct cfdriver amliic_cd;
extern struct cfdriver amlmmc_cd;
extern struct cfdriver amlpciephy_cd;
extern struct cfdriver amlpinctrl_cd;
extern struct cfdriver amlpwm_cd;
extern struct cfdriver amlpwrc_cd;
extern struct cfdriver amlreset_cd;
extern struct cfdriver amlrng_cd;
extern struct cfdriver amlsm_cd;
extern struct cfdriver amltemp_cd;
extern struct cfdriver amluart_cd;
extern struct cfdriver amlusbphy_cd;
extern struct cfdriver hiclock_cd;
extern struct cfdriver hidwusb_cd;
extern struct cfdriver hireset_cd;
extern struct cfdriver hitemp_cd;
extern struct cfdriver rkanxdp_cd;
extern struct cfdriver rkclock_cd;
extern struct cfdriver rkdrm_cd;
extern struct cfdriver rkdwhdmi_cd;
extern struct cfdriver rkdwusb_cd;
extern struct cfdriver rkemmcphy_cd;
extern struct cfdriver rkgrf_cd;
extern struct cfdriver rkgpio_cd;
extern struct cfdriver rkiic_cd;
extern struct cfdriver rkiis_cd;
extern struct cfdriver rkpmic_cd;
extern struct cfdriver rkpinctrl_cd;
extern struct cfdriver rkpcie_cd;
extern struct cfdriver rkpwm_cd;
extern struct cfdriver rkrng_cd;
extern struct cfdriver rktcphy_cd;
extern struct cfdriver rktemp_cd;
extern struct cfdriver rkvop_cd;
extern struct cfdriver dwmmc_cd;
extern struct cfdriver dwpcie_cd;
extern struct cfdriver moxtet_cd;
extern struct cfdriver mvclock_cd;
extern struct cfdriver mvdog_cd;
extern struct cfdriver mvgicp_cd;
extern struct cfdriver mvgpio_cd;
extern struct cfdriver mvicu_cd;
extern struct cfdriver mviic_cd;
extern struct cfdriver mvkpcie_cd;
extern struct cfdriver mvpinctrl_cd;
extern struct cfdriver mvmdio_cd;
extern struct cfdriver mvneta_cd;
extern struct cfdriver mvppc_cd;
extern struct cfdriver mvpp_cd;
extern struct cfdriver mvrng_cd;
extern struct cfdriver mvrtc_cd;
extern struct cfdriver mvspi_cd;
extern struct cfdriver mvsw_cd;
extern struct cfdriver mvtemp_cd;
extern struct cfdriver mvuart_cd;
extern struct cfdriver dwxe_cd;
extern struct cfdriver imxanatop_cd;
extern struct cfdriver imxccm_cd;
extern struct cfdriver imxdog_cd;
extern struct cfdriver imxdwusb_cd;
extern struct cfdriver imxehci_cd;
extern struct cfdriver imxesdhc_cd;
extern struct cfdriver imxgpc_cd;
extern struct cfdriver imxgpio_cd;
extern struct cfdriver imxiomuxc_cd;
extern struct cfdriver imxpciephy_cd;
extern struct cfdriver imxpwm_cd;
extern struct cfdriver imxsrc_cd;
extern struct cfdriver imxtmu_cd;
extern struct cfdriver imxuart_cd;
extern struct cfdriver fec_cd;
extern struct cfdriver imxspi_cd;
extern struct cfdriver ssdfb_cd;
extern struct cfdriver fusbtc_cd;
extern struct cfdriver sfp_cd;
extern struct cfdriver bdpmic_cd;
extern struct cfdriver tcpci_cd;
extern struct cfdriver escodec_cd;
extern struct cfdriver cwfg_cd;
extern struct cfdriver ampintc_cd;
extern struct cfdriver ampintcmsi_cd;
extern struct cfdriver agintc_cd;
extern struct cfdriver agintcmsi_cd;
extern struct cfdriver agtimer_cd;
extern struct cfdriver apldart_cd;
extern struct cfdriver apldog_cd;
extern struct cfdriver apldwusb_cd;
extern struct cfdriver aplintc_cd;
extern struct cfdriver aplpcie_cd;
extern struct cfdriver aplpinctrl_cd;
extern struct cfdriver aplns_cd;
extern struct cfdriver aplpmu_cd;
extern struct cfdriver aplspmi_cd;
extern struct cfdriver bcmintc_cd;
extern struct cfdriver pciecam_cd;
extern struct cfdriver smmu_cd;
extern struct cfdriver acpi_cd;
extern struct cfdriver acpibtn_cd;
extern struct cfdriver acpiec_cd;
extern struct cfdriver acpige_cd;
extern struct cfdriver acpitz_cd;
extern struct cfdriver acpimcfg_cd;
extern struct cfdriver acpipwrres_cd;
extern struct cfdriver dwgpio_cd;
extern struct cfdriver acpiiort_cd;
extern struct cfdriver acpipci_cd;
extern struct cfdriver apm_cd;

extern struct cfattach video_ca;
extern struct cfattach audio_ca;
extern struct cfattach midi_ca;
extern struct cfattach drm_ca;
extern struct cfattach radio_ca;
extern struct cfattach vscsi_ca;
extern struct cfattach softraid_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach simplebus_ca;
extern struct cfattach efi_ca;
extern struct cfattach smbios_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach cd_ca;
extern struct cfattach ch_ca;
extern struct cfattach sd_ca;
extern struct cfattach st_ca;
extern struct cfattach uk_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach wsmouse_ca;
extern struct cfattach cpu_ca;
extern struct cfattach iic_ca;
extern struct cfattach pcamux_ca;
extern struct cfattach maxrtc_ca;
extern struct cfattach dsxrtc_ca;
extern struct cfattach pcfrtc_ca;
extern struct cfattach pcxrtc_ca;
extern struct cfattach islrtc_ca;
extern struct cfattach abcrtc_ca;
extern struct cfattach ipmi_i2c_ca;
extern struct cfattach mcprtc_ca;
extern struct cfattach mfokrtc_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach uaudio_ca;
extern struct cfattach uvideo_ca;
extern struct cfattach utvfu_ca;
extern struct cfattach udl_ca;
extern struct cfattach umidi_ca;
extern struct cfattach ucom_ca;
extern struct cfattach ugen_ca;
extern struct cfattach uhidev_ca;
extern struct cfattach uhid_ca;
extern struct cfattach fido_ca;
extern struct cfattach ujoy_ca;
extern struct cfattach ukbd_ca;
extern struct cfattach ums_ca;
extern struct cfattach umt_ca;
extern struct cfattach uts_ca;
extern struct cfattach ucycom_ca;
extern struct cfattach uslhcom_ca;
extern struct cfattach ulpt_ca;
extern struct cfattach umass_ca;
extern struct cfattach uthum_ca;
extern struct cfattach ugold_ca;
extern struct cfattach utrh_ca;
extern struct cfattach uoakrh_ca;
extern struct cfattach uoaklux_ca;
extern struct cfattach uoakv_ca;
extern struct cfattach uonerng_ca;
extern struct cfattach urng_ca;
extern struct cfattach udcf_ca;
extern struct cfattach uvisor_ca;
extern struct cfattach udsbr_ca;
extern struct cfattach utwitch_ca;
extern struct cfattach aue_ca;
extern struct cfattach axe_ca;
extern struct cfattach axen_ca;
extern struct cfattach smsc_ca;
extern struct cfattach cue_ca;
extern struct cfattach kue_ca;
extern struct cfattach cdce_ca;
extern struct cfattach urndis_ca;
extern struct cfattach mos_ca;
extern struct cfattach mue_ca;
extern struct cfattach udav_ca;
extern struct cfattach upl_ca;
extern struct cfattach ugl_ca;
extern struct cfattach url_ca;
extern struct cfattach ure_ca;
extern struct cfattach umodem_ca;
extern struct cfattach uftdi_ca;
extern struct cfattach uplcom_ca;
extern struct cfattach umct_ca;
extern struct cfattach uvscom_ca;
extern struct cfattach ubsa_ca;
extern struct cfattach uslcom_ca;
extern struct cfattach uark_ca;
extern struct cfattach moscom_ca;
extern struct cfattach umcs_ca;
extern struct cfattach uscom_ca;
extern struct cfattach ucrcom_ca;
extern struct cfattach uxrcom_ca;
extern struct cfattach uipaq_ca;
extern struct cfattach umsm_ca;
extern struct cfattach uchcom_ca;
extern struct cfattach uticom_ca;
extern struct cfattach wi_usb_ca;
extern struct cfattach atu_ca;
extern struct cfattach ural_ca;
extern struct cfattach rum_ca;
extern struct cfattach run_ca;
extern struct cfattach zyd_ca;
extern struct cfattach upgt_ca;
extern struct cfattach urtw_ca;
extern struct cfattach urtwn_ca;
extern struct cfattach rsu_ca;
extern struct cfattach otus_ca;
extern struct cfattach umb_ca;
extern struct cfattach uath_ca;
extern struct cfattach athn_usb_ca;
extern struct cfattach uow_ca;
extern struct cfattach uberry_ca;
extern struct cfattach upd_ca;
extern struct cfattach uwacom_ca;
extern struct cfattach bwfm_usb_ca;
extern struct cfattach uhidpp_ca;
extern struct cfattach ucc_ca;
extern struct cfattach gpio_ca;
extern struct cfattach gpioiic_ca;
extern struct cfattach gpioow_ca;
extern struct cfattach onewire_ca;
extern struct cfattach owid_ca;
extern struct cfattach owsbm_ca;
extern struct cfattach owtemp_ca;
extern struct cfattach owctr_ca;
extern struct cfattach vio_ca;
extern struct cfattach vioblk_ca;
extern struct cfattach viomb_ca;
extern struct cfattach viornd_ca;
extern struct cfattach vioscsi_ca;
extern struct cfattach eephy_ca;
extern struct cfattach rlphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach brgphy_ca;
extern struct cfattach amphy_ca;
extern struct cfattach urlphy_ca;
extern struct cfattach rgephy_ca;
extern struct cfattach atphy_ca;
extern struct cfattach pci_ca;
extern struct cfattach ahci_pci_ca;
extern struct cfattach nvme_pci_ca;
extern struct cfattach mfi_pci_ca;
extern struct cfattach mfii_ca;
extern struct cfattach mpi_pci_ca;
extern struct cfattach mpii_ca;
extern struct cfattach ppb_ca;
extern struct cfattach re_pci_ca;
extern struct cfattach em_ca;
extern struct cfattach ix_ca;
extern struct cfattach ixl_ca;
extern struct cfattach uhci_pci_ca;
extern struct cfattach ohci_pci_ca;
extern struct cfattach ehci_pci_ca;
extern struct cfattach xhci_pci_ca;
extern struct cfattach mskc_ca;
extern struct cfattach msk_ca;
extern struct cfattach iwx_ca;
extern struct cfattach bge_ca;
extern struct cfattach athn_pci_ca;
extern struct cfattach virtio_pci_ca;
extern struct cfattach bwfm_pci_ca;
extern struct cfattach bnxt_ca;
extern struct cfattach mcx_ca;
extern struct cfattach rge_ca;
extern struct cfattach radeondrm_ca;
extern struct cfattach amdgpu_ca;
extern struct cfattach sdmmc_ca;
extern struct cfattach bwfm_sdio_ca;
extern struct cfattach pinctrl_ca;
extern struct cfattach graphaudio_ca;
extern struct cfattach simpleamp_ca;
extern struct cfattach simpleaudio_ca;
extern struct cfattach simplefb_ca;
extern struct cfattach simplepanel_ca;
extern struct cfattach sxiccmu_ca;
extern struct cfattach sxidog_ca;
extern struct cfattach sxipio_ca;
extern struct cfattach sxirsb_ca;
extern struct cfattach sxipwm_ca;
extern struct cfattach sxirtc_ca;
extern struct cfattach sximmc_ca;
extern struct cfattach sxisid_ca;
extern struct cfattach sxisyscon_ca;
extern struct cfattach sxitemp_ca;
extern struct cfattach sxitwi_ca;
extern struct cfattach axppmic_rsb_ca;
extern struct cfattach fanpwr_ca;
extern struct cfattach sypwr_ca;
extern struct cfattach bcmaux_ca;
extern struct cfattach bcmbsc_ca;
extern struct cfattach bcmclock_ca;
extern struct cfattach bcmdmac_ca;
extern struct cfattach bcmdog_ca;
extern struct cfattach bcmgpio_ca;
extern struct cfattach bcmirng_ca;
extern struct cfattach bcmmbox_ca;
extern struct cfattach bcmpcie_ca;
extern struct cfattach bcmrng_ca;
extern struct cfattach bcmsdhost_ca;
extern struct cfattach bcmtemp_ca;
extern struct cfattach bcmtmon_ca;
extern struct cfattach bse_fdt_ca;
extern struct cfattach bcmdwctwo_ca;
extern struct cfattach exuart_ca;
extern struct cfattach plgpio_ca;
extern struct cfattach plrtc_ca;
extern struct cfattach pluart_fdt_ca;
extern struct cfattach psci_ca;
extern struct cfattach virtio_mmio_ca;
extern struct cfattach ahci_fdt_ca;
extern struct cfattach dwge_ca;
extern struct cfattach ehci_fdt_ca;
extern struct cfattach ohci_fdt_ca;
extern struct cfattach sdhc_fdt_ca;
extern struct cfattach xhci_fdt_ca;
extern struct cfattach syscon_ca;
extern struct cfattach pwmbl_ca;
extern struct cfattach pwmfan_ca;
extern struct cfattach pwmreg_ca;
extern struct cfattach amlclock_ca;
extern struct cfattach amldwusb_ca;
extern struct cfattach amliic_ca;
extern struct cfattach amlmmc_ca;
extern struct cfattach amlpciephy_ca;
extern struct cfattach amlpinctrl_ca;
extern struct cfattach amlpwm_ca;
extern struct cfattach amlpwrc_ca;
extern struct cfattach amlreset_ca;
extern struct cfattach amlrng_ca;
extern struct cfattach amlsm_ca;
extern struct cfattach amltemp_ca;
extern struct cfattach amluart_ca;
extern struct cfattach amlusbphy_ca;
extern struct cfattach hiclock_ca;
extern struct cfattach hidwusb_ca;
extern struct cfattach hireset_ca;
extern struct cfattach hitemp_ca;
extern struct cfattach rkanxdp_ca;
extern struct cfattach rkclock_ca;
extern struct cfattach rkdrm_ca;
extern struct cfattach rkdwhdmi_ca;
extern struct cfattach rkdwusb_ca;
extern struct cfattach rkemmcphy_ca;
extern struct cfattach rkgrf_ca;
extern struct cfattach rkgpio_ca;
extern struct cfattach rkiic_ca;
extern struct cfattach rkiis_ca;
extern struct cfattach rkpmic_ca;
extern struct cfattach rkpinctrl_ca;
extern struct cfattach rkpcie_ca;
extern struct cfattach rkpwm_ca;
extern struct cfattach rkrng_ca;
extern struct cfattach rktcphy_ca;
extern struct cfattach rktemp_ca;
extern struct cfattach rkvop_ca;
extern struct cfattach dwmmc_ca;
extern struct cfattach dwpcie_ca;
extern struct cfattach moxtet_ca;
extern struct cfattach mvclock_ca;
extern struct cfattach mvdog_ca;
extern struct cfattach mvgicp_ca;
extern struct cfattach mvgpio_ca;
extern struct cfattach mvicu_ca;
extern struct cfattach mviic_ca;
extern struct cfattach mvkpcie_ca;
extern struct cfattach mvpinctrl_ca;
extern struct cfattach mvmdio_ca;
extern struct cfattach mvneta_ca;
extern struct cfattach mvppc_ca;
extern struct cfattach mvpp_ca;
extern struct cfattach mvrng_ca;
extern struct cfattach mvrtc_ca;
extern struct cfattach mvspi_ca;
extern struct cfattach mvsw_ca;
extern struct cfattach mvtemp_ca;
extern struct cfattach mvuart_ca;
extern struct cfattach dwxe_ca;
extern struct cfattach imxanatop_ca;
extern struct cfattach imxccm_ca;
extern struct cfattach imxdog_ca;
extern struct cfattach imxdwusb_ca;
extern struct cfattach imxehci_ca;
extern struct cfattach imxesdhc_ca;
extern struct cfattach imxgpc_ca;
extern struct cfattach imxgpio_ca;
extern struct cfattach imxiic_fdt_ca;
extern struct cfattach imxiomuxc_ca;
extern struct cfattach imxpciephy_ca;
extern struct cfattach imxpwm_ca;
extern struct cfattach imxsrc_ca;
extern struct cfattach imxtmu_ca;
extern struct cfattach imxuart_ca;
extern struct cfattach fec_ca;
extern struct cfattach imxspi_ca;
extern struct cfattach ccp_fdt_ca;
extern struct cfattach com_fdt_ca;
extern struct cfattach ipmi_fdt_ca;
extern struct cfattach ssdfb_spi_ca;
extern struct cfattach ssdfb_i2c_ca;
extern struct cfattach fusbtc_ca;
extern struct cfattach sfp_ca;
extern struct cfattach bdpmic_ca;
extern struct cfattach tcpci_ca;
extern struct cfattach escodec_ca;
extern struct cfattach cwfg_ca;
extern struct cfattach ampintc_ca;
extern struct cfattach ampintcmsi_ca;
extern struct cfattach agintc_ca;
extern struct cfattach agintcmsi_ca;
extern struct cfattach agtimer_ca;
extern struct cfattach apldart_ca;
extern struct cfattach apldog_ca;
extern struct cfattach apldwusb_ca;
extern struct cfattach aplintc_ca;
extern struct cfattach aplpcie_ca;
extern struct cfattach aplpinctrl_ca;
extern struct cfattach aplns_ca;
extern struct cfattach nvme_ans_ca;
extern struct cfattach aplpmu_ca;
extern struct cfattach aplspmi_ca;
extern struct cfattach bcmintc_ca;
extern struct cfattach pciecam_ca;
extern struct cfattach smmu_fdt_ca;
extern struct cfattach acpibtn_ca;
extern struct cfattach acpiec_ca;
extern struct cfattach acpige_ca;
extern struct cfattach acpitz_ca;
extern struct cfattach acpimcfg_ca;
extern struct cfattach acpipwrres_ca;
extern struct cfattach ahci_acpi_ca;
extern struct cfattach com_acpi_ca;
extern struct cfattach pluart_acpi_ca;
extern struct cfattach sdhc_acpi_ca;
extern struct cfattach xhci_acpi_ca;
extern struct cfattach dwgpio_ca;
extern struct cfattach dwiic_acpi_ca;
extern struct cfattach ipmi_acpi_ca;
extern struct cfattach ccp_acpi_ca;
extern struct cfattach bse_acpi_ca;
extern struct cfattach imxiic_acpi_ca;
extern struct cfattach acpi_fdt_ca;
extern struct cfattach acpiiort_ca;
extern struct cfattach acpipci_ca;
extern struct cfattach smmu_acpi_ca;
extern struct cfattach apm_ca;


/* locators */
static long loc[12] = {
	-1, -1, -1, -1, -1, -1, -1, 0,
	0, -1, -1, 1,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"primary",
	"early",
	"target",
	"lun",
	"console",
	"mux",
	"addr",
	"size",
	"port",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"portno",
	"reportid",
	"offset",
	"mask",
	"flag",
	"phy",
	"bus",
	"dev",
	"function",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, -1, 0, -1, 0, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 2,
	3, -1, 4, 0, 5, -1, 4, 0,
	5, -1, 4, 0, 5, -1, 4, 0,
	5, -1, 4, 5, -1, 4, 5, -1,
	5, -1, 5, -1, 5, -1, 5, -1,
	6, 7, -1, 8, 9, 10, 11, 12,
	13, -1, 8, 9, 10, 11, 12, 13,
	-1, 14, -1, 14, -1, 14, -1, 14,
	-1, 14, -1, 14, -1, 14, -1, 14,
	-1, 14, -1, 14, -1, 14, -1, 14,
	-1, 14, -1, 14, -1, 14, -1, 14,
	-1, 14, -1, 14, -1, 14, -1, 14,
	-1, 15, -1, 16, 17, 18, -1, 19,
	-1, 19, -1, 19, -1, 19, -1, 19,
	-1, 19, -1, 19, -1, 19, -1, 19,
	-1, 19, -1, 19, -1, 19, -1, 19,
	-1, 19, -1, 19, -1, 19, -1, 19,
	-1, 19, -1, 19, -1, 20, -1, 20,
	-1, 20, -1, 20, -1, 20, -1, 20,
	-1, 20, -1, 20, -1, 21, 22, -1,
};

/* size of parent vectors */
int pv_size = 192;

/* parent vectors */
short pv[192] = {
	81, 63, 85, 86, 82, 83, 84, 87, 91, 92, 88, 89, 90, 94, 95, 96,
	97, 93, 50, 51, -1, 295, 279, 272, 270, 213, 159, 157, 80, 79, 76, 75,
	74, 69, 68, 67, 66, 203, 204, 148, -1, 315, 310, 308, 292, 283, 269, 255,
	251, 246, 244, 237, 223, 218, 184, 8, 7, -1, 6, 5, 168, 146, 144, 131,
	128, 53, 145, 320, 142, 332, 212, 141, 143, -1, 284, 205, 215, 153, 152, 214,
	154, 336, 217, 155, -1, 338, 341, 288, 191, 224, 266, 248, 186, 22, 120, -1,
	344, 259, 324, 317, 198, 267, 252, 147, -1, 174, 166, 167, 301, 300, 242, 38,
	-1, 335, 216, 285, 200, 225, 258, 182, -1, 171, 173, 35, 37, -1, 47, 48,
	49, 115, -1, 166, 167, 242, -1, 211, 161, -1, 33, 34, -1, 46, 118, -1,
	112, 121, -1, 195, 178, -1, 36, 37, -1, 296, 275, -1, 42, -1, 21, -1,
	140, -1, 11, -1, 342, -1, 319, -1, 322, -1, 156, -1, 122, -1, 343, -1,
	32, -1, 168, -1, 271, -1, 64, -1, 39, -1, 119, -1, 9, -1, 179, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: video* at uvideo*|utvfu* */
    {&video_ca,		&video_cd,	 0, STAR,     loc,    0, pv+150, 0,    0},
/*  1: audio* at graphaudio*|simpleaudio*|uaudio*|utvfu* */
    {&audio_ca,		&audio_cd,	 0, STAR,     loc,    0, pv+121, 0,    0},
/*  2: midi* at umidi* */
    {&midi_ca,		&midi_cd,	 0, STAR,     loc,    0, pv+184, 0,    0},
/*  3: drm* at radeondrm*|amdgpu*|rkdrm* primary -1 */
    {&drm_ca,		&drm_cd,	 0, STAR, loc+  5,    0, pv+131, 1,    0},
/*  4: radio* at udsbr* */
    {&radio_ca,		&radio_cd,	 0, STAR,     loc,    0, pv+182, 6,    0},
/*  5: vscsi0 at root */
    {&vscsi_ca,		&vscsi_cd,	 0, NORM,     loc,    0, pv+20, 0,    0},
/*  6: softraid0 at root */
    {&softraid_ca,	&softraid_cd,	 0, NORM,     loc,    0, pv+20, 0,    0},
/*  7: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+20, 0,    0},
/*  8: simplebus* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&simplebus_ca,	&simplebus_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*  9: efi0 at mainbus0 early 0 */
    {&efi_ca,		&efi_cd,	 0, NORM, loc+  8,    0, pv+56, 37,    0},
/* 10: smbios0 at efi0 */
    {&smbios_ca,	&smbios_cd,	 0, NORM,     loc,    0, pv+188, 38,    0},
/* 11: scsibus* at softraid0|vscsi0|sdmmc*|mpii*|mfii*|vioscsi*|vioblk*|umass*|mpi*|nvme*|nvme*|ahci*|ahci*|ahci*|mfi* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+58, 38,    0},
/* 12: cd* at scsibus* target -1 lun -1 */
    {&cd_ca,		&cd_cd,		 0, STAR, loc+  4,    0, pv+162, 39,    0},
/* 13: ch* at scsibus* target -1 lun -1 */
    {&ch_ca,		&ch_cd,		 0, STAR, loc+  4,    0, pv+162, 39,    0},
/* 14: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+  4,    0, pv+162, 39,    0},
/* 15: st* at scsibus* target -1 lun -1 */
    {&st_ca,		&st_cd,		 0, STAR, loc+  4,    0, pv+162, 39,    0},
/* 16: uk* at scsibus* target -1 lun -1 */
    {&uk_ca,		&uk_cd,		 0, STAR, loc+  4,    0, pv+162, 39,    0},
/* 17: wsdisplay* at simplefb*|radeondrm*|amdgpu*|ssdfb*|ssdfb*|rkdrm*|udl* console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+  9,    0, pv+105, 42,    0},
/* 18: wskbd* at ukbd*|ucc* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+ 10,    0, pv+141, 58,    0},
/* 19: wsmouse* at ums*|umt*|uts*|uwacom* mux 0 */
    {&wsmouse_ca,	&wsmouse_cd,	 0, STAR, loc+  8,    0, pv+126, 64,    0},
/* 20: cpu0 at mainbus0 */
    {&cpu_ca,		&cpu_cd,	 0, NORM,     loc,    0, pv+56, 37,    0},
/* 21: iic* at dwiic*|imxiic*|imxiic*|bcmbsc*|amliic*|mviic*|rkiic*|sxitwi*|pcamux*|gpioiic* */
    {&iic_ca,		&iic_cd,	 0, STAR,     loc,    0, pv+85, 71,    0},
/* 22: pcamux* at iic* addr -1 size -1 */
    {&pcamux_ca,	&pcamux_cd,	 0, STAR, loc+  4,    0, pv+158, 72,    0},
/* 23: maxrtc* at iic* addr -1 size -1 */
    {&maxrtc_ca,	&maxrtc_cd,	 0, STAR, loc+  4,    0, pv+158, 72,    0},
/* 24: dsxrtc* at iic* addr -1 size -1 */
    {&dsxrtc_ca,	&dsxrtc_cd,	 0, STAR, loc+  4,    0, pv+158, 72,    0},
/* 25: pcfrtc* at iic* addr -1 size -1 */
    {&pcfrtc_ca,	&pcfrtc_cd,	 0, STAR, loc+  4,    0, pv+158, 72,    0},
/* 26: pcxrtc* at iic* addr -1 size -1 */
    {&pcxrtc_ca,	&pcxrtc_cd,	 0, STAR, loc+  4,    0, pv+158, 72,    0},
/* 27: islrtc* at iic* addr -1 size -1 */
    {&islrtc_ca,	&islrtc_cd,	 0, STAR, loc+  4,    0, pv+158, 72,    0},
/* 28: abcrtc* at iic* addr -1 size -1 */
    {&abcrtc_ca,	&abcrtc_cd,	 0, STAR, loc+  4,    0, pv+158, 72,    0},
/* 29: ipmi* at iic* addr -1 size -1 */
    {&ipmi_i2c_ca,	&ipmi_cd,	 0, STAR, loc+  4,    0, pv+158, 72,    0},
/* 30: mcprtc* at iic* addr -1 size -1 */
    {&mcprtc_ca,	&mcprtc_cd,	 0, STAR, loc+  4,    0, pv+158, 72,    0},
/* 31: mfokrtc* at iic* addr -1 size -1 */
    {&mfokrtc_ca,	&mfokrtc_cd,	 0, STAR, loc+  4,    0, pv+158, 72,    0},
/* 32: usb* at imxehci*|dwctwo*|ohci*|ohci*|uhci*|ehci*|ehci*|xhci*|xhci*|xhci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+74, 74,    0},
/* 33: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+176, 74,    0},
/* 34: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 35: uaudio* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uaudio_ca,	&uaudio_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 36: uvideo* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvideo_ca,	&uvideo_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 37: utvfu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&utvfu_ca,		&utvfu_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 38: udl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udl_ca,		&udl_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 39: umidi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umidi_ca,		&umidi_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 40: ucom* at umodem*|uvisor*|uvscom*|ubsa*|uftdi*|uplcom*|umct*|uslcom*|uscom*|ucrcom*|uark*|moscom*|umcs*|uipaq*|umsm*|uchcom*|uticom*|uxrcom*|ucycom*|uslhcom* portno -1 */
    {&ucom_ca,		&ucom_cd,	 0, STAR, loc+  5,    0, pv+ 0, 89,    0},
/* 41: ugen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugen_ca,		&ugen_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 42: uhidev* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhidev_ca,	&uhidev_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 43: uhid* at uhidev* reportid -1 */
    {&uhid_ca,		&uhid_cd,	 0, STAR, loc+  5,    0, pv+156, 129,    0},
/* 44: fido* at uhidev* reportid -1 */
    {&fido_ca,		&fido_cd,	 0, STAR, loc+  5,    0, pv+156, 129,    0},
/* 45: ujoy* at uhidev* reportid -1 */
    {&ujoy_ca,		&ujoy_cd,	 0, STAR, loc+  5,    0, pv+156, 129,    0},
/* 46: ukbd* at uhidev* reportid -1 */
    {&ukbd_ca,		&ukbd_cd,	 0, STAR, loc+  5,    0, pv+156, 129,    0},
/* 47: ums* at uhidev* reportid -1 */
    {&ums_ca,		&ums_cd,	 0, STAR, loc+  5,    0, pv+156, 129,    0},
/* 48: umt* at uhidev* reportid -1 */
    {&umt_ca,		&umt_cd,	 0, STAR, loc+  5,    0, pv+156, 129,    0},
/* 49: uts* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uts_ca,		&uts_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 50: ucycom* at uhidev* reportid -1 */
    {&ucycom_ca,	&ucycom_cd,	 0, STAR, loc+  5,    0, pv+156, 129,    0},
/* 51: uslhcom* at uhidev* reportid -1 */
    {&uslhcom_ca,	&uslhcom_cd,	 0, STAR, loc+  5,    0, pv+156, 129,    0},
/* 52: ulpt* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ulpt_ca,		&ulpt_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 53: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 54: uthum* at uhidev* reportid -1 */
    {&uthum_ca,		&uthum_cd,	 0, STAR, loc+  5,    0, pv+156, 129,    0},
/* 55: ugold* at uhidev* reportid -1 */
    {&ugold_ca,		&ugold_cd,	 0, STAR, loc+  5,    0, pv+156, 129,    0},
/* 56: utrh* at uhidev* reportid -1 */
    {&utrh_ca,		&utrh_cd,	 0, STAR, loc+  5,    0, pv+156, 129,    0},
/* 57: uoakrh* at uhidev* reportid -1 */
    {&uoakrh_ca,	&uoakrh_cd,	 0, STAR, loc+  5,    0, pv+156, 129,    0},
/* 58: uoaklux* at uhidev* reportid -1 */
    {&uoaklux_ca,	&uoaklux_cd,	 0, STAR, loc+  5,    0, pv+156, 129,    0},
/* 59: uoakv* at uhidev* reportid -1 */
    {&uoakv_ca,		&uoakv_cd,	 0, STAR, loc+  5,    0, pv+156, 129,    0},
/* 60: uonerng* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uonerng_ca,	&uonerng_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 61: urng* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urng_ca,		&urng_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 62: udcf* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udcf_ca,		&udcf_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 63: uvisor* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvisor_ca,	&uvisor_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 64: udsbr* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udsbr_ca,		&udsbr_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 65: utwitch* at uhidev* reportid -1 */
    {&utwitch_ca,	&utwitch_cd,	 0, STAR, loc+  5,    0, pv+156, 129,    0},
/* 66: aue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&aue_ca,		&aue_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 67: axe* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axe_ca,		&axe_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 68: axen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axen_ca,		&axen_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 69: smsc* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&smsc_ca,		&smsc_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 70: cue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cue_ca,		&cue_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 71: kue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&kue_ca,		&kue_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 72: cdce* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cdce_ca,		&cdce_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 73: urndis* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urndis_ca,	&urndis_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 74: mos* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mos_ca,		&mos_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 75: mue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mue_ca,		&mue_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 76: udav* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udav_ca,		&udav_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 77: upl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upl_ca,		&upl_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 78: ugl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugl_ca,		&ugl_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 79: url* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&url_ca,		&url_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 80: ure* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ure_ca,		&ure_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 81: umodem* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umodem_ca,	&umodem_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 82: uftdi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uftdi_ca,		&uftdi_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 83: uplcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uplcom_ca,	&uplcom_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 84: umct* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umct_ca,		&umct_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 85: uvscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvscom_ca,	&uvscom_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 86: ubsa* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ubsa_ca,		&ubsa_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 87: uslcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uslcom_ca,	&uslcom_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 88: uark* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uark_ca,		&uark_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 89: moscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&moscom_ca,	&moscom_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 90: umcs* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umcs_ca,		&umcs_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 91: uscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uscom_ca,		&uscom_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 92: ucrcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ucrcom_ca,	&ucrcom_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 93: uxrcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uxrcom_ca,	&uxrcom_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 94: uipaq* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uipaq_ca,		&uipaq_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 95: umsm* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umsm_ca,		&umsm_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 96: uchcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uchcom_ca,	&uchcom_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 97: uticom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uticom_ca,	&uticom_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 98: wi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&wi_usb_ca,	&wi_cd,		 0, STAR, loc+  0,    0, pv+138, 75,    0},
/* 99: atu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&atu_ca,		&atu_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/*100: ural* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ural_ca,		&ural_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/*101: rum* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rum_ca,		&rum_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/*102: run* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&run_ca,		&run_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/*103: zyd* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&zyd_ca,		&zyd_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/*104: upgt* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upgt_ca,		&upgt_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/*105: urtw* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtw_ca,		&urtw_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/*106: urtwn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtwn_ca,		&urtwn_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/*107: rsu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rsu_ca,		&rsu_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/*108: otus* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&otus_ca,		&otus_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/*109: umb* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umb_ca,		&umb_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/*110: uath* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uath_ca,		&uath_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/*111: athn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&athn_usb_ca,	&athn_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/*112: uow* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uow_ca,		&uow_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/*113: uberry* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uberry_ca,	&uberry_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/*114: upd* at uhidev* reportid -1 */
    {&upd_ca,		&upd_cd,	 0, STAR, loc+  5,    0, pv+156, 129,    0},
/*115: uwacom* at uhidev* reportid -1 */
    {&uwacom_ca,	&uwacom_cd,	 0, STAR, loc+  5,    0, pv+156, 129,    0},
/*116: bwfm* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&bwfm_usb_ca,	&bwfm_cd,	 0, STAR, loc+  0,    0, pv+138, 75,    0},
/*117: uhidpp* at uhidev* reportid -1 */
    {&uhidpp_ca,	&uhidpp_cd,	 0, STAR, loc+  5,    0, pv+156, 129,    0},
/*118: ucc* at uhidev* reportid -1 */
    {&ucc_ca,		&ucc_cd,	 0, STAR, loc+  5,    0, pv+156, 129,    0},
/*119: gpio* at bcmgpio*|sxipio* */
    {&gpio_ca,		&gpio_cd,	 0, STAR,     loc,    0, pv+147, 130,    0},
/*120: gpioiic* at gpio* offset -1 mask 0 flag 0 */
    {&gpioiic_ca,	&gpioiic_cd,	 0, STAR, loc+  6,    0, pv+186, 131,    0},
/*121: gpioow* at gpio* offset -1 mask 0 flag 0 */
    {&gpioow_ca,	&gpioow_cd,	 0, STAR, loc+  6,    0, pv+186, 131,    0},
/*122: onewire* at uow*|gpioow* */
    {&onewire_ca,	&onewire_cd,	 0, STAR,     loc,    0, pv+144, 134,    0},
/*123: owid* at onewire* */
    {&owid_ca,		&owid_cd,	 0, STAR,     loc,    0, pv+172, 134,    0},
/*124: owsbm* at onewire* */
    {&owsbm_ca,		&owsbm_cd,	 0, STAR,     loc,    0, pv+172, 134,    0},
/*125: owtemp* at onewire* */
    {&owtemp_ca,	&owtemp_cd,	 0, STAR,     loc,    0, pv+172, 134,    0},
/*126: owctr* at onewire* */
    {&owctr_ca,		&owctr_cd,	 0, STAR,     loc,    0, pv+172, 134,    0},
/*127: vio* at virtio*|virtio* */
    {&vio_ca,		&vio_cd,	 0, STAR,     loc,    0, pv+135, 134,    0},
/*128: vioblk* at virtio*|virtio* */
    {&vioblk_ca,	&vioblk_cd,	 0, STAR,     loc,    0, pv+135, 134,    0},
/*129: viomb* at virtio*|virtio* */
    {&viomb_ca,		&viomb_cd,	 0, STAR,     loc,    0, pv+135, 134,    0},
/*130: viornd* at virtio*|virtio* */
    {&viornd_ca,	&viornd_cd,	 0, STAR,     loc,    0, pv+135, 134,    0},
/*131: vioscsi* at virtio*|virtio* */
    {&vioscsi_ca,	&vioscsi_cd,	 0, STAR,     loc,    0, pv+135, 134,    0},
/*132: eephy* at fec*|dwxe*|mvpp*|mvneta*|dwge*|bge*|msk*|ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|bse*|bse*|re* phy -1 */
    {&eephy_ca,		&eephy_cd,	 0, STAR, loc+  5,    0, pv+21, 135,    0},
/*133: rlphy* at fec*|dwxe*|mvpp*|mvneta*|dwge*|bge*|msk*|ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|bse*|bse*|re* phy -1 */
    {&rlphy_ca,		&rlphy_cd,	 0, STAR, loc+  5,    0, pv+21, 135,    0},
/*134: ukphy* at fec*|dwxe*|mvpp*|mvneta*|dwge*|bge*|msk*|ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|bse*|bse*|re* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+  5,    0, pv+21, 135,    0},
/*135: brgphy* at fec*|dwxe*|mvpp*|mvneta*|dwge*|bge*|msk*|ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|bse*|bse*|re* phy -1 */
    {&brgphy_ca,	&brgphy_cd,	 0, STAR, loc+  5,    0, pv+21, 135,    0},
/*136: amphy* at fec*|dwxe*|mvpp*|mvneta*|dwge*|bge*|msk*|ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|bse*|bse*|re* phy -1 */
    {&amphy_ca,		&amphy_cd,	 0, STAR, loc+  5,    0, pv+21, 135,    0},
/*137: urlphy* at fec*|dwxe*|mvpp*|mvneta*|dwge*|bge*|msk*|ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|bse*|bse*|re* phy -1 */
    {&urlphy_ca,	&urlphy_cd,	 0, STAR, loc+  5,    0, pv+21, 135,    0},
/*138: rgephy* at fec*|dwxe*|mvpp*|mvneta*|dwge*|bge*|msk*|ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|bse*|bse*|re* phy -1 */
    {&rgephy_ca,	&rgephy_cd,	 0, STAR, loc+  5,    0, pv+21, 135,    0},
/*139: atphy* at fec*|dwxe*|mvpp*|mvneta*|dwge*|bge*|msk*|ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|bse*|bse*|re* phy -1 */
    {&atphy_ca,		&atphy_cd,	 0, STAR, loc+  5,    0, pv+21, 135,    0},
/*140: pci* at acpipci*|dwpcie*|pciecam*|aplpcie*|bcmpcie*|mvkpcie*|rkpcie*|ppb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+  5,    0, pv+96, 173,    0},
/*141: ahci* at pci* dev -1 function -1 */
    {&ahci_pci_ca,	&ahci_cd,	 0, STAR, loc+  4,    0, pv+160, 189,    0},
/*142: nvme* at pci* dev -1 function -1 */
    {&nvme_pci_ca,	&nvme_cd,	 0, STAR, loc+  4,    0, pv+160, 189,    0},
/*143: mfi* at pci* dev -1 function -1 */
    {&mfi_pci_ca,	&mfi_cd,	 0, STAR, loc+  4,    0, pv+160, 189,    0},
/*144: mfii* at pci* dev -1 function -1 */
    {&mfii_ca,		&mfii_cd,	 0, STAR, loc+  4,    0, pv+160, 189,    0},
/*145: mpi* at pci* dev -1 function -1 */
    {&mpi_pci_ca,	&mpi_cd,	 0, STAR, loc+  4,    0, pv+160, 189,    0},
/*146: mpii* at pci* dev -1 function -1 */
    {&mpii_ca,		&mpii_cd,	 0, STAR, loc+  4,    0, pv+160, 189,    0},
/*147: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+  4,    0, pv+160, 189,    0},
/*148: re* at pci* dev -1 function -1 */
    {&re_pci_ca,	&re_cd,		 0, STAR, loc+  4,    0, pv+160, 189,    0},
/*149: em* at pci* dev -1 function -1 */
    {&em_ca,		&em_cd,		 0, STAR, loc+  4,    0, pv+160, 189,    0},
/*150: ix* at pci* dev -1 function -1 */
    {&ix_ca,		&ix_cd,		 0, STAR, loc+  4,    0, pv+160, 189,    0},
/*151: ixl* at pci* dev -1 function -1 */
    {&ixl_ca,		&ixl_cd,	 0, STAR, loc+  4,    0, pv+160, 189,    0},
/*152: uhci* at pci* dev -1 function -1 */
    {&uhci_pci_ca,	&uhci_cd,	 0, STAR, loc+  4,    0, pv+160, 189,    0},
/*153: ohci* at pci* dev -1 function -1 */
    {&ohci_pci_ca,	&ohci_cd,	 0, STAR, loc+  4,    0, pv+160, 189,    0},
/*154: ehci* at pci* dev -1 function -1 */
    {&ehci_pci_ca,	&ehci_cd,	 0, STAR, loc+  4,    0, pv+160, 189,    0},
/*155: xhci* at pci* dev -1 function -1 */
    {&xhci_pci_ca,	&xhci_cd,	 0, STAR, loc+  4,    0, pv+160, 189,    0},
/*156: mskc* at pci* dev -1 function -1 */
    {&mskc_ca,		&mskc_cd,	 0, STAR, loc+  4,    0, pv+160, 189,    0},
/*157: msk* at mskc* */
    {&msk_ca,		&msk_cd,	 0, STAR,     loc,    0, pv+170, 191,    0},
/*158: iwx* at pci* dev -1 function -1 */
    {&iwx_ca,		&iwx_cd,	 0, STAR, loc+  4,    0, pv+160, 189,    0},
/*159: bge* at pci* dev -1 function -1 */
    {&bge_ca,		&bge_cd,	 0, STAR, loc+  4,    0, pv+160, 189,    0},
/*160: athn* at pci* dev -1 function -1 */
    {&athn_pci_ca,	&athn_cd,	 0, STAR, loc+  4,    0, pv+160, 189,    0},
/*161: virtio* at pci* dev -1 function -1 */
    {&virtio_pci_ca,	&virtio_cd,	 0, STAR, loc+  4,    0, pv+160, 189,    0},
/*162: bwfm* at pci* dev -1 function -1 */
    {&bwfm_pci_ca,	&bwfm_cd,	 0, STAR, loc+  4,    0, pv+160, 189,    0},
/*163: bnxt* at pci* dev -1 function -1 */
    {&bnxt_ca,		&bnxt_cd,	 0, STAR, loc+  4,    0, pv+160, 189,    0},
/*164: mcx* at pci* dev -1 function -1 */
    {&mcx_ca,		&mcx_cd,	 0, STAR, loc+  4,    0, pv+160, 189,    0},
/*165: rge* at pci* dev -1 function -1 */
    {&rge_ca,		&rge_cd,	 0, STAR, loc+  4,    0, pv+160, 189,    0},
/*166: radeondrm* at pci* dev -1 function -1 */
    {&radeondrm_ca,	&radeondrm_cd,	 0, STAR, loc+  4,    0, pv+160, 189,    0},
/*167: amdgpu* at pci* dev -1 function -1 */
    {&amdgpu_ca,	&amdgpu_cd,	 0, STAR, loc+  4,    0, pv+160, 189,    0},
/*168: sdmmc* at sdhc*|sdhc*|imxesdhc*|bcmsdhost*|amlmmc*|dwmmc*|sximmc* */
    {&sdmmc_ca,		&sdmmc_cd,	 0, STAR,     loc,    0, pv+113, 191,    0},
/*169: bwfm* at sdmmc* */
    {&bwfm_sdio_ca,	&bwfm_cd,	 0, STAR,     loc,    0, pv+178, 38,    0},
/*170: pinctrl* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&pinctrl_ca,	&pinctrl_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*171: graphaudio* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&graphaudio_ca,	&graphaudio_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*172: simpleamp* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&simpleamp_ca,	&simpleamp_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*173: simpleaudio* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&simpleaudio_ca,	&simpleaudio_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*174: simplefb* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&simplefb_ca,	&simplefb_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*175: simplepanel* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&simplepanel_ca,	&simplepanel_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*176: sxiccmu* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&sxiccmu_ca,	&sxiccmu_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*177: sxidog* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&sxidog_ca,	&sxidog_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*178: sxipio* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&sxipio_ca,	&sxipio_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*179: sxirsb* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&sxirsb_ca,	&sxirsb_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*180: sxipwm* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&sxipwm_ca,	&sxipwm_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*181: sxirtc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&sxirtc_ca,	&sxirtc_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*182: sximmc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&sximmc_ca,	&sximmc_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*183: sxisid* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&sxisid_ca,	&sxisid_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*184: sxisyscon* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&sxisyscon_ca,	&sxisyscon_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*185: sxitemp* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&sxitemp_ca,	&sxitemp_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*186: sxitwi* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&sxitwi_ca,	&sxitwi_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*187: axppmic* at sxirsb* */
    {&axppmic_rsb_ca,	&axppmic_cd,	 0, STAR,     loc,    0, pv+190, 191,    0},
/*188: fanpwr* at iic* addr -1 size -1 */
    {&fanpwr_ca,	&fanpwr_cd,	 0, STAR, loc+  4,    0, pv+158, 72,    0},
/*189: sypwr* at iic* addr -1 size -1 */
    {&sypwr_ca,		&sypwr_cd,	 0, STAR, loc+  4,    0, pv+158, 72,    0},
/*190: bcmaux* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&bcmaux_ca,	&bcmaux_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*191: bcmbsc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&bcmbsc_ca,	&bcmbsc_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*192: bcmclock* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&bcmclock_ca,	&bcmclock_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*193: bcmdmac* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&bcmdmac_ca,	&bcmdmac_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*194: bcmdog* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&bcmdog_ca,	&bcmdog_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*195: bcmgpio* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&bcmgpio_ca,	&bcmgpio_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*196: bcmirng* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&bcmirng_ca,	&bcmirng_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*197: bcmmbox* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&bcmmbox_ca,	&bcmmbox_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*198: bcmpcie* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&bcmpcie_ca,	&bcmpcie_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*199: bcmrng* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&bcmrng_ca,	&bcmrng_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*200: bcmsdhost* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&bcmsdhost_ca,	&bcmsdhost_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*201: bcmtemp* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&bcmtemp_ca,	&bcmtemp_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*202: bcmtmon* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&bcmtmon_ca,	&bcmtmon_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*203: bse* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&bse_fdt_ca,	&bse_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*204: bse* at acpi0 */
    {&bse_acpi_ca,	&bse_cd,	 0, STAR,     loc,    0, pv+164, 191,    0},
/*205: dwctwo* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&bcmdwctwo_ca,	&dwctwo_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*206: exuart* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&exuart_ca,	&exuart_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*207: plgpio* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&plgpio_ca,	&plgpio_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*208: plrtc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&plrtc_ca,		&plrtc_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*209: pluart* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&pluart_fdt_ca,	&pluart_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*210: psci* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&psci_ca,		&psci_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*211: virtio* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&virtio_mmio_ca,	&virtio_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*212: ahci* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&ahci_fdt_ca,	&ahci_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*213: dwge* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&dwge_ca,		&dwge_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*214: ehci* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&ehci_fdt_ca,	&ehci_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*215: ohci* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&ohci_fdt_ca,	&ohci_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*216: sdhc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&sdhc_fdt_ca,	&sdhc_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*217: xhci* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&xhci_fdt_ca,	&xhci_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*218: syscon* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&syscon_ca,	&syscon_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*219: pwmbl* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&pwmbl_ca,		&pwmbl_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*220: pwmfan* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&pwmfan_ca,	&pwmfan_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*221: pwmreg* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&pwmreg_ca,	&pwmreg_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*222: amlclock* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&amlclock_ca,	&amlclock_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*223: amldwusb* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&amldwusb_ca,	&amldwusb_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*224: amliic* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&amliic_ca,	&amliic_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*225: amlmmc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&amlmmc_ca,	&amlmmc_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*226: amlpciephy* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&amlpciephy_ca,	&amlpciephy_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*227: amlpinctrl* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&amlpinctrl_ca,	&amlpinctrl_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*228: amlpwm* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&amlpwm_ca,	&amlpwm_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*229: amlpwrc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&amlpwrc_ca,	&amlpwrc_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*230: amlreset* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&amlreset_ca,	&amlreset_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*231: amlrng* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&amlrng_ca,	&amlrng_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*232: amlsm* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&amlsm_ca,		&amlsm_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*233: amltemp* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&amltemp_ca,	&amltemp_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*234: amluart* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&amluart_ca,	&amluart_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*235: amlusbphy* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&amlusbphy_ca,	&amlusbphy_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*236: hiclock* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&hiclock_ca,	&hiclock_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*237: hidwusb* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&hidwusb_ca,	&hidwusb_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*238: hireset* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&hireset_ca,	&hireset_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*239: hitemp* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&hitemp_ca,	&hitemp_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*240: rkanxdp* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&rkanxdp_ca,	&rkanxdp_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*241: rkclock* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&rkclock_ca,	&rkclock_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*242: rkdrm* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&rkdrm_ca,		&rkdrm_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*243: rkdwhdmi* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&rkdwhdmi_ca,	&rkdwhdmi_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*244: rkdwusb* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&rkdwusb_ca,	&rkdwusb_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*245: rkemmcphy* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&rkemmcphy_ca,	&rkemmcphy_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*246: rkgrf* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&rkgrf_ca,		&rkgrf_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*247: rkgpio* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&rkgpio_ca,	&rkgpio_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*248: rkiic* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&rkiic_ca,		&rkiic_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*249: rkiis* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&rkiis_ca,		&rkiis_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*250: rkpmic* at iic* addr -1 size -1 */
    {&rkpmic_ca,	&rkpmic_cd,	 0, STAR, loc+  4,    0, pv+158, 72,    0},
/*251: rkpinctrl* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&rkpinctrl_ca,	&rkpinctrl_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*252: rkpcie* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&rkpcie_ca,	&rkpcie_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*253: rkpwm* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&rkpwm_ca,		&rkpwm_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*254: rkrng* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&rkrng_ca,		&rkrng_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*255: rktcphy* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&rktcphy_ca,	&rktcphy_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*256: rktemp* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&rktemp_ca,	&rktemp_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*257: rkvop* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&rkvop_ca,		&rkvop_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*258: dwmmc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&dwmmc_ca,		&dwmmc_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*259: dwpcie* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&dwpcie_ca,	&dwpcie_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*260: moxtet* at imxspi*|mvspi* */
    {&moxtet_ca,	&moxtet_cd,	 0, STAR,     loc,    0, pv+153, 191,    0},
/*261: mvclock* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&mvclock_ca,	&mvclock_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*262: mvdog* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&mvdog_ca,		&mvdog_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*263: mvgicp* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&mvgicp_ca,	&mvgicp_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*264: mvgpio* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&mvgpio_ca,	&mvgpio_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*265: mvicu* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&mvicu_ca,		&mvicu_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*266: mviic* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&mviic_ca,		&mviic_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*267: mvkpcie* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&mvkpcie_ca,	&mvkpcie_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*268: mvpinctrl* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&mvpinctrl_ca,	&mvpinctrl_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*269: mvmdio* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&mvmdio_ca,	&mvmdio_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*270: mvneta* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&mvneta_ca,	&mvneta_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*271: mvppc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&mvppc_ca,		&mvppc_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*272: mvpp* at mvppc* */
    {&mvpp_ca,		&mvpp_cd,	 0, STAR,     loc,    0, pv+180, 191,    0},
/*273: mvrng* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&mvrng_ca,		&mvrng_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*274: mvrtc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&mvrtc_ca,		&mvrtc_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*275: mvspi* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&mvspi_ca,		&mvspi_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*276: mvsw* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&mvsw_ca,		&mvsw_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*277: mvtemp* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&mvtemp_ca,	&mvtemp_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*278: mvuart* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&mvuart_ca,	&mvuart_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*279: dwxe* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&dwxe_ca,		&dwxe_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*280: imxanatop* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&imxanatop_ca,	&imxanatop_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*281: imxccm* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&imxccm_ca,	&imxccm_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*282: imxdog* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&imxdog_ca,	&imxdog_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*283: imxdwusb* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&imxdwusb_ca,	&imxdwusb_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*284: imxehci* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&imxehci_ca,	&imxehci_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*285: imxesdhc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&imxesdhc_ca,	&imxesdhc_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*286: imxgpc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&imxgpc_ca,	&imxgpc_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*287: imxgpio* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&imxgpio_ca,	&imxgpio_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*288: imxiic* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&imxiic_fdt_ca,	&imxiic_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*289: imxiomuxc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&imxiomuxc_ca,	&imxiomuxc_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*290: imxpciephy* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&imxpciephy_ca,	&imxpciephy_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*291: imxpwm* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&imxpwm_ca,	&imxpwm_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*292: imxsrc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&imxsrc_ca,	&imxsrc_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*293: imxtmu* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&imxtmu_ca,	&imxtmu_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*294: imxuart* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&imxuart_ca,	&imxuart_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*295: fec* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&fec_ca,		&fec_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*296: imxspi* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&imxspi_ca,	&imxspi_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*297: ccp* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&ccp_fdt_ca,	&ccp_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*298: com* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&com_fdt_ca,	&com_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*299: ipmi* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&ipmi_fdt_ca,	&ipmi_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*300: ssdfb* at imxspi*|mvspi* */
    {&ssdfb_spi_ca,	&ssdfb_cd,	 0, STAR,     loc,    0, pv+153, 191,    0},
/*301: ssdfb* at iic* addr -1 size -1 */
    {&ssdfb_i2c_ca,	&ssdfb_cd,	 0, STAR, loc+  4,    0, pv+158, 72,    0},
/*302: fusbtc* at iic* addr -1 size -1 */
    {&fusbtc_ca,	&fusbtc_cd,	 0, STAR, loc+  4,    0, pv+158, 72,    0},
/*303: sfp* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&sfp_ca,		&sfp_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*304: bdpmic* at iic* addr -1 size -1 */
    {&bdpmic_ca,	&bdpmic_cd,	 0, STAR, loc+  4,    0, pv+158, 72,    0},
/*305: tcpci* at iic* addr -1 size -1 */
    {&tcpci_ca,		&tcpci_cd,	 0, STAR, loc+  4,    0, pv+158, 72,    0},
/*306: escodec* at iic* addr -1 size -1 */
    {&escodec_ca,	&escodec_cd,	 0, STAR, loc+  4,    0, pv+158, 72,    0},
/*307: cwfg* at iic* addr -1 size -1 */
    {&cwfg_ca,		&cwfg_cd,	 0, STAR, loc+  4,    0, pv+158, 72,    0},
/*308: ampintc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&ampintc_ca,	&ampintc_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*309: ampintcmsi* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&ampintcmsi_ca,	&ampintcmsi_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*310: agintc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&agintc_ca,	&agintc_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*311: agintcmsi* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&agintcmsi_ca,	&agintcmsi_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*312: agtimer* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&agtimer_ca,	&agtimer_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*313: apldart* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&apldart_ca,	&apldart_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*314: apldog* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&apldog_ca,	&apldog_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*315: apldwusb* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&apldwusb_ca,	&apldwusb_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*316: aplintc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&aplintc_ca,	&aplintc_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*317: aplpcie* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&aplpcie_ca,	&aplpcie_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*318: aplpinctrl* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&aplpinctrl_ca,	&aplpinctrl_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*319: aplns* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&aplns_ca,		&aplns_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*320: nvme* at aplns* */
    {&nvme_ans_ca,	&nvme_cd,	 0, STAR,     loc,    0, pv+166, 191,    0},
/*321: aplpmu* at aplspmi* */
    {&aplpmu_ca,	&aplpmu_cd,	 0, STAR,     loc,    0, pv+168, 191,    0},
/*322: aplspmi* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&aplspmi_ca,	&aplspmi_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*323: bcmintc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&bcmintc_ca,	&bcmintc_cd,	 0, STAR, loc+ 11,    0, pv+41, 7,    0},
/*324: pciecam* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&pciecam_ca,	&pciecam_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*325: smmu* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&smmu_fdt_ca,	&smmu_cd,	 0, STAR, loc+  8,    0, pv+41, 7,    0},
/*326: acpibtn* at acpi0 */
    {&acpibtn_ca,	&acpibtn_cd,	 0, STAR,     loc,    0, pv+164, 191,    0},
/*327: acpiec* at acpi0 */
    {&acpiec_ca,	&acpiec_cd,	 0, STAR,     loc,    0, pv+164, 191,    0},
/*328: acpige* at acpi0 */
    {&acpige_ca,	&acpige_cd,	 0, STAR,     loc,    0, pv+164, 191,    0},
/*329: acpitz* at acpi0 */
    {&acpitz_ca,	&acpitz_cd,	 0, STAR,     loc,    0, pv+164, 191,    0},
/*330: acpimcfg* at acpi0 */
    {&acpimcfg_ca,	&acpimcfg_cd,	 0, STAR,     loc,    0, pv+164, 191,    0},
/*331: acpipwrres* at acpi0 */
    {&acpipwrres_ca,	&acpipwrres_cd,	 0, STAR,     loc,    0, pv+164, 191,    0},
/*332: ahci* at acpi0 */
    {&ahci_acpi_ca,	&ahci_cd,	 0, STAR,     loc,    0, pv+164, 191,    0},
/*333: com* at acpi0 */
    {&com_acpi_ca,	&com_cd,	 0, STAR,     loc,    0, pv+164, 191,    0},
/*334: pluart* at acpi0 */
    {&pluart_acpi_ca,	&pluart_cd,	 0, STAR,     loc,    0, pv+164, 191,    0},
/*335: sdhc* at acpi0 */
    {&sdhc_acpi_ca,	&sdhc_cd,	 0, STAR,     loc,    0, pv+164, 191,    0},
/*336: xhci* at acpi0 */
    {&xhci_acpi_ca,	&xhci_cd,	 0, STAR,     loc,    0, pv+164, 191,    0},
/*337: dwgpio* at acpi0 */
    {&dwgpio_ca,	&dwgpio_cd,	 0, STAR,     loc,    0, pv+164, 191,    0},
/*338: dwiic* at acpi0 */
    {&dwiic_acpi_ca,	&dwiic_cd,	 0, STAR,     loc,    0, pv+164, 191,    0},
/*339: ipmi* at acpi0 */
    {&ipmi_acpi_ca,	&ipmi_cd,	 0, STAR,     loc,    0, pv+164, 191,    0},
/*340: ccp* at acpi0 */
    {&ccp_acpi_ca,	&ccp_cd,	 0, STAR,     loc,    0, pv+164, 191,    0},
/*341: imxiic* at acpi0 */
    {&imxiic_acpi_ca,	&imxiic_cd,	 0, STAR,     loc,    0, pv+164, 191,    0},
/*342: acpi0 at mainbus0 early 0 */
    {&acpi_fdt_ca,	&acpi_cd,	 0, NORM, loc+  8,    0, pv+56, 37,    0},
/*343: acpiiort* at acpi0 */
    {&acpiiort_ca,	&acpiiort_cd,	 0, STAR,     loc,    0, pv+164, 191,    0},
/*344: acpipci* at acpi0 */
    {&acpipci_ca,	&acpipci_cd,	 0, STAR,     loc,    0, pv+164, 191,    0},
/*345: smmu* at acpiiort* */
    {&smmu_acpi_ca,	&smmu_cd,	 0, STAR,     loc,    0, pv+174, 191,    0},
/*346: apm0 at mainbus0 */
    {&apm_ca,		&apm_cd,	 0, NORM,     loc,    0, pv+56, 37,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 5 /* vscsi0 */,
	 6 /* softraid0 */,
	 7 /* mainbus0 */,
	-1
};

int cfroots_size = 4;

/* pseudo-devices */
extern void pfattach(int);
extern void pflogattach(int);
extern void pfsyncattach(int);
extern void pflowattach(int);
extern void encattach(int);
extern void ptyattach(int);
extern void nmeaattach(int);
extern void mstsattach(int);
extern void endrunattach(int);
extern void vndattach(int);
extern void ksymsattach(int);
extern void bpfilterattach(int);
extern void bridgeattach(int);
extern void vebattach(int);
extern void carpattach(int);
extern void etheripattach(int);
extern void gifattach(int);
extern void greattach(int);
extern void loopattach(int);
extern void mpeattach(int);
extern void mpwattach(int);
extern void mpipattach(int);
extern void bpeattach(int);
extern void pairattach(int);
extern void pppattach(int);
extern void pppoeattach(int);
extern void pppxattach(int);
extern void spppattach(int);
extern void trunkattach(int);
extern void aggrattach(int);
extern void tpmrattach(int);
extern void tunattach(int);
extern void vetherattach(int);
extern void vxlanattach(int);
extern void vlanattach(int);
extern void switchattach(int);
extern void wgattach(int);
extern void bioattach(int);
extern void fuseattach(int);
extern void openpromattach(int);
extern void hotplugattach(int);
extern void dtattach(int);
extern void wsmuxattach(int);

char *pdevnames[] = {
	"pf",
	"pflog",
	"pfsync",
	"pflow",
	"enc",
	"pty",
	"nmea",
	"msts",
	"endrun",
	"vnd",
	"ksyms",
	"bpfilter",
	"bridge",
	"veb",
	"carp",
	"etherip",
	"gif",
	"gre",
	"loop",
	"mpe",
	"mpw",
	"mpip",
	"bpe",
	"pair",
	"ppp",
	"pppoe",
	"pppx",
	"sppp",
	"trunk",
	"aggr",
	"tpmr",
	"tun",
	"vether",
	"vxlan",
	"vlan",
	"switch",
	"wg",
	"bio",
	"fuse",
	"openprom",
	"hotplug",
	"dt",
	"wsmux",
};

int pdevnames_size = 43;

struct pdevinit pdevinit[] = {
	{ pfattach, 1 },
	{ pflogattach, 1 },
	{ pfsyncattach, 1 },
	{ pflowattach, 1 },
	{ encattach, 1 },
	{ ptyattach, 16 },
	{ nmeaattach, 1 },
	{ mstsattach, 1 },
	{ endrunattach, 1 },
	{ vndattach, 4 },
	{ ksymsattach, 1 },
	{ bpfilterattach, 1 },
	{ bridgeattach, 1 },
	{ vebattach, 1 },
	{ carpattach, 1 },
	{ etheripattach, 1 },
	{ gifattach, 1 },
	{ greattach, 1 },
	{ loopattach, 1 },
	{ mpeattach, 1 },
	{ mpwattach, 1 },
	{ mpipattach, 1 },
	{ bpeattach, 1 },
	{ pairattach, 1 },
	{ pppattach, 1 },
	{ pppoeattach, 1 },
	{ pppxattach, 1 },
	{ spppattach, 1 },
	{ trunkattach, 1 },
	{ aggrattach, 1 },
	{ tpmrattach, 1 },
	{ tunattach, 1 },
	{ vetherattach, 1 },
	{ vxlanattach, 1 },
	{ vlanattach, 1 },
	{ switchattach, 1 },
	{ wgattach, 1 },
	{ bioattach, 1 },
	{ fuseattach, 1 },
	{ openpromattach, 1 },
	{ hotplugattach, 1 },
	{ dtattach, 1 },
	{ wsmuxattach, 2 },
	{ NULL, 0 }
};
