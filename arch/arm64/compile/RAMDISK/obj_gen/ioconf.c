/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/arm64/conf/RAMDISK"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver ahci_cd;
extern struct cfdriver nvme_cd;
extern struct cfdriver wi_cd;
extern struct cfdriver re_cd;
extern struct cfdriver bse_cd;
extern struct cfdriver com_cd;
extern struct cfdriver pluart_cd;
extern struct cfdriver athn_cd;
extern struct cfdriver bwfm_cd;
extern struct cfdriver virtio_cd;
extern struct cfdriver uhci_cd;
extern struct cfdriver ohci_cd;
extern struct cfdriver ehci_cd;
extern struct cfdriver xhci_cd;
extern struct cfdriver ccp_cd;
extern struct cfdriver sdhc_cd;
extern struct cfdriver softraid_cd;
extern struct cfdriver imxiic_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver cd_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver simplebus_cd;
extern struct cfdriver efi_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver ch_cd;
extern struct cfdriver st_cd;
extern struct cfdriver uk_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver iic_cd;
extern struct cfdriver maxrtc_cd;
extern struct cfdriver dsxrtc_cd;
extern struct cfdriver pcfrtc_cd;
extern struct cfdriver pcxrtc_cd;
extern struct cfdriver islrtc_cd;
extern struct cfdriver abcrtc_cd;
extern struct cfdriver mcprtc_cd;
extern struct cfdriver mfokrtc_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver uhidev_cd;
extern struct cfdriver ukbd_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver aue_cd;
extern struct cfdriver axe_cd;
extern struct cfdriver axen_cd;
extern struct cfdriver smsc_cd;
extern struct cfdriver cue_cd;
extern struct cfdriver kue_cd;
extern struct cfdriver cdce_cd;
extern struct cfdriver urndis_cd;
extern struct cfdriver mos_cd;
extern struct cfdriver mue_cd;
extern struct cfdriver udav_cd;
extern struct cfdriver upl_cd;
extern struct cfdriver ugl_cd;
extern struct cfdriver url_cd;
extern struct cfdriver ure_cd;
extern struct cfdriver atu_cd;
extern struct cfdriver ural_cd;
extern struct cfdriver rum_cd;
extern struct cfdriver run_cd;
extern struct cfdriver zyd_cd;
extern struct cfdriver upgt_cd;
extern struct cfdriver urtw_cd;
extern struct cfdriver urtwn_cd;
extern struct cfdriver rsu_cd;
extern struct cfdriver otus_cd;
extern struct cfdriver uath_cd;
extern struct cfdriver gpio_cd;
extern struct cfdriver vio_cd;
extern struct cfdriver vioblk_cd;
extern struct cfdriver viomb_cd;
extern struct cfdriver viornd_cd;
extern struct cfdriver vioscsi_cd;
extern struct cfdriver eephy_cd;
extern struct cfdriver rlphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver brgphy_cd;
extern struct cfdriver amphy_cd;
extern struct cfdriver urlphy_cd;
extern struct cfdriver rgephy_cd;
extern struct cfdriver atphy_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver em_cd;
extern struct cfdriver ix_cd;
extern struct cfdriver ixl_cd;
extern struct cfdriver mskc_cd;
extern struct cfdriver msk_cd;
extern struct cfdriver iwx_cd;
extern struct cfdriver bge_cd;
extern struct cfdriver bnxt_cd;
extern struct cfdriver mcx_cd;
extern struct cfdriver rge_cd;
extern struct cfdriver sdmmc_cd;
extern struct cfdriver pinctrl_cd;
extern struct cfdriver simplefb_cd;
extern struct cfdriver sxiccmu_cd;
extern struct cfdriver sxidog_cd;
extern struct cfdriver sxipio_cd;
extern struct cfdriver sxirsb_cd;
extern struct cfdriver sxirtc_cd;
extern struct cfdriver sximmc_cd;
extern struct cfdriver sxisid_cd;
extern struct cfdriver sxisyscon_cd;
extern struct cfdriver sxitwi_cd;
extern struct cfdriver bcmaux_cd;
extern struct cfdriver bcmbsc_cd;
extern struct cfdriver bcmclock_cd;
extern struct cfdriver bcmdmac_cd;
extern struct cfdriver bcmdog_cd;
extern struct cfdriver bcmgpio_cd;
extern struct cfdriver bcmirng_cd;
extern struct cfdriver bcmmbox_cd;
extern struct cfdriver bcmpcie_cd;
extern struct cfdriver bcmrng_cd;
extern struct cfdriver bcmsdhost_cd;
extern struct cfdriver dwctwo_cd;
extern struct cfdriver exuart_cd;
extern struct cfdriver plgpio_cd;
extern struct cfdriver plrtc_cd;
extern struct cfdriver psci_cd;
extern struct cfdriver dwge_cd;
extern struct cfdriver syscon_cd;
extern struct cfdriver pwmfan_cd;
extern struct cfdriver amlclock_cd;
extern struct cfdriver amldwusb_cd;
extern struct cfdriver amliic_cd;
extern struct cfdriver amlmmc_cd;
extern struct cfdriver amlpciephy_cd;
extern struct cfdriver amlpinctrl_cd;
extern struct cfdriver amlpwrc_cd;
extern struct cfdriver amlreset_cd;
extern struct cfdriver amlrng_cd;
extern struct cfdriver amluart_cd;
extern struct cfdriver amlusbphy_cd;
extern struct cfdriver hiclock_cd;
extern struct cfdriver hidwusb_cd;
extern struct cfdriver hireset_cd;
extern struct cfdriver rkclock_cd;
extern struct cfdriver rkdwusb_cd;
extern struct cfdriver rkemmcphy_cd;
extern struct cfdriver rkgrf_cd;
extern struct cfdriver rkgpio_cd;
extern struct cfdriver rkiic_cd;
extern struct cfdriver rkpmic_cd;
extern struct cfdriver rkpinctrl_cd;
extern struct cfdriver rkpcie_cd;
extern struct cfdriver rkpwm_cd;
extern struct cfdriver rkrng_cd;
extern struct cfdriver rktcphy_cd;
extern struct cfdriver dwmmc_cd;
extern struct cfdriver dwpcie_cd;
extern struct cfdriver mvclock_cd;
extern struct cfdriver mvdog_cd;
extern struct cfdriver mvgicp_cd;
extern struct cfdriver mvgpio_cd;
extern struct cfdriver mvicu_cd;
extern struct cfdriver mviic_cd;
extern struct cfdriver mvkpcie_cd;
extern struct cfdriver mvpinctrl_cd;
extern struct cfdriver mvmdio_cd;
extern struct cfdriver mvneta_cd;
extern struct cfdriver mvppc_cd;
extern struct cfdriver mvpp_cd;
extern struct cfdriver mvrng_cd;
extern struct cfdriver mvrtc_cd;
extern struct cfdriver mvsw_cd;
extern struct cfdriver mvuart_cd;
extern struct cfdriver dwxe_cd;
extern struct cfdriver imxanatop_cd;
extern struct cfdriver imxccm_cd;
extern struct cfdriver imxdog_cd;
extern struct cfdriver imxdwusb_cd;
extern struct cfdriver imxehci_cd;
extern struct cfdriver imxesdhc_cd;
extern struct cfdriver imxgpc_cd;
extern struct cfdriver imxgpio_cd;
extern struct cfdriver imxiomuxc_cd;
extern struct cfdriver imxpciephy_cd;
extern struct cfdriver imxsrc_cd;
extern struct cfdriver imxuart_cd;
extern struct cfdriver fec_cd;
extern struct cfdriver fusbtc_cd;
extern struct cfdriver sfp_cd;
extern struct cfdriver bdpmic_cd;
extern struct cfdriver tcpci_cd;
extern struct cfdriver ampintc_cd;
extern struct cfdriver ampintcmsi_cd;
extern struct cfdriver agintc_cd;
extern struct cfdriver agintcmsi_cd;
extern struct cfdriver agtimer_cd;
extern struct cfdriver apldart_cd;
extern struct cfdriver apldog_cd;
extern struct cfdriver apldwusb_cd;
extern struct cfdriver aplintc_cd;
extern struct cfdriver aplpcie_cd;
extern struct cfdriver aplpinctrl_cd;
extern struct cfdriver aplns_cd;
extern struct cfdriver aplpmu_cd;
extern struct cfdriver aplspmi_cd;
extern struct cfdriver bcmintc_cd;
extern struct cfdriver pciecam_cd;
extern struct cfdriver smmu_cd;
extern struct cfdriver acpi_cd;
extern struct cfdriver acpiec_cd;
extern struct cfdriver acpimcfg_cd;
extern struct cfdriver acpiiort_cd;
extern struct cfdriver acpipci_cd;

extern struct cfattach softraid_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach simplebus_ca;
extern struct cfattach efi_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach cd_ca;
extern struct cfattach ch_ca;
extern struct cfattach sd_ca;
extern struct cfattach st_ca;
extern struct cfattach uk_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach cpu_ca;
extern struct cfattach iic_ca;
extern struct cfattach maxrtc_ca;
extern struct cfattach dsxrtc_ca;
extern struct cfattach pcfrtc_ca;
extern struct cfattach pcxrtc_ca;
extern struct cfattach islrtc_ca;
extern struct cfattach abcrtc_ca;
extern struct cfattach mcprtc_ca;
extern struct cfattach mfokrtc_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach uhidev_ca;
extern struct cfattach ukbd_ca;
extern struct cfattach umass_ca;
extern struct cfattach aue_ca;
extern struct cfattach axe_ca;
extern struct cfattach axen_ca;
extern struct cfattach smsc_ca;
extern struct cfattach cue_ca;
extern struct cfattach kue_ca;
extern struct cfattach cdce_ca;
extern struct cfattach urndis_ca;
extern struct cfattach mos_ca;
extern struct cfattach mue_ca;
extern struct cfattach udav_ca;
extern struct cfattach upl_ca;
extern struct cfattach ugl_ca;
extern struct cfattach url_ca;
extern struct cfattach ure_ca;
extern struct cfattach wi_usb_ca;
extern struct cfattach atu_ca;
extern struct cfattach ural_ca;
extern struct cfattach rum_ca;
extern struct cfattach run_ca;
extern struct cfattach zyd_ca;
extern struct cfattach upgt_ca;
extern struct cfattach urtw_ca;
extern struct cfattach urtwn_ca;
extern struct cfattach rsu_ca;
extern struct cfattach otus_ca;
extern struct cfattach uath_ca;
extern struct cfattach athn_usb_ca;
extern struct cfattach bwfm_usb_ca;
extern struct cfattach gpio_ca;
extern struct cfattach vio_ca;
extern struct cfattach vioblk_ca;
extern struct cfattach viomb_ca;
extern struct cfattach viornd_ca;
extern struct cfattach vioscsi_ca;
extern struct cfattach eephy_ca;
extern struct cfattach rlphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach brgphy_ca;
extern struct cfattach amphy_ca;
extern struct cfattach urlphy_ca;
extern struct cfattach rgephy_ca;
extern struct cfattach atphy_ca;
extern struct cfattach pci_ca;
extern struct cfattach ahci_pci_ca;
extern struct cfattach nvme_pci_ca;
extern struct cfattach ppb_ca;
extern struct cfattach re_pci_ca;
extern struct cfattach em_ca;
extern struct cfattach ix_ca;
extern struct cfattach ixl_ca;
extern struct cfattach uhci_pci_ca;
extern struct cfattach ohci_pci_ca;
extern struct cfattach ehci_pci_ca;
extern struct cfattach xhci_pci_ca;
extern struct cfattach mskc_ca;
extern struct cfattach msk_ca;
extern struct cfattach iwx_ca;
extern struct cfattach bge_ca;
extern struct cfattach athn_pci_ca;
extern struct cfattach virtio_pci_ca;
extern struct cfattach bwfm_pci_ca;
extern struct cfattach bnxt_ca;
extern struct cfattach mcx_ca;
extern struct cfattach rge_ca;
extern struct cfattach sdmmc_ca;
extern struct cfattach bwfm_sdio_ca;
extern struct cfattach pinctrl_ca;
extern struct cfattach simplefb_ca;
extern struct cfattach sxiccmu_ca;
extern struct cfattach sxidog_ca;
extern struct cfattach sxipio_ca;
extern struct cfattach sxirsb_ca;
extern struct cfattach sxirtc_ca;
extern struct cfattach sximmc_ca;
extern struct cfattach sxisid_ca;
extern struct cfattach sxisyscon_ca;
extern struct cfattach sxitwi_ca;
extern struct cfattach bcmaux_ca;
extern struct cfattach bcmbsc_ca;
extern struct cfattach bcmclock_ca;
extern struct cfattach bcmdmac_ca;
extern struct cfattach bcmdog_ca;
extern struct cfattach bcmgpio_ca;
extern struct cfattach bcmirng_ca;
extern struct cfattach bcmmbox_ca;
extern struct cfattach bcmpcie_ca;
extern struct cfattach bcmrng_ca;
extern struct cfattach bcmsdhost_ca;
extern struct cfattach bse_fdt_ca;
extern struct cfattach bcmdwctwo_ca;
extern struct cfattach exuart_ca;
extern struct cfattach plgpio_ca;
extern struct cfattach plrtc_ca;
extern struct cfattach pluart_fdt_ca;
extern struct cfattach psci_ca;
extern struct cfattach virtio_mmio_ca;
extern struct cfattach ahci_fdt_ca;
extern struct cfattach dwge_ca;
extern struct cfattach ehci_fdt_ca;
extern struct cfattach ohci_fdt_ca;
extern struct cfattach sdhc_fdt_ca;
extern struct cfattach xhci_fdt_ca;
extern struct cfattach syscon_ca;
extern struct cfattach pwmfan_ca;
extern struct cfattach amlclock_ca;
extern struct cfattach amldwusb_ca;
extern struct cfattach amliic_ca;
extern struct cfattach amlmmc_ca;
extern struct cfattach amlpciephy_ca;
extern struct cfattach amlpinctrl_ca;
extern struct cfattach amlpwrc_ca;
extern struct cfattach amlreset_ca;
extern struct cfattach amlrng_ca;
extern struct cfattach amluart_ca;
extern struct cfattach amlusbphy_ca;
extern struct cfattach hiclock_ca;
extern struct cfattach hidwusb_ca;
extern struct cfattach hireset_ca;
extern struct cfattach rkclock_ca;
extern struct cfattach rkdwusb_ca;
extern struct cfattach rkemmcphy_ca;
extern struct cfattach rkgrf_ca;
extern struct cfattach rkgpio_ca;
extern struct cfattach rkiic_ca;
extern struct cfattach rkpmic_ca;
extern struct cfattach rkpinctrl_ca;
extern struct cfattach rkpcie_ca;
extern struct cfattach rkpwm_ca;
extern struct cfattach rkrng_ca;
extern struct cfattach rktcphy_ca;
extern struct cfattach dwmmc_ca;
extern struct cfattach dwpcie_ca;
extern struct cfattach mvclock_ca;
extern struct cfattach mvdog_ca;
extern struct cfattach mvgicp_ca;
extern struct cfattach mvgpio_ca;
extern struct cfattach mvicu_ca;
extern struct cfattach mviic_ca;
extern struct cfattach mvkpcie_ca;
extern struct cfattach mvpinctrl_ca;
extern struct cfattach mvmdio_ca;
extern struct cfattach mvneta_ca;
extern struct cfattach mvppc_ca;
extern struct cfattach mvpp_ca;
extern struct cfattach mvrng_ca;
extern struct cfattach mvrtc_ca;
extern struct cfattach mvsw_ca;
extern struct cfattach mvuart_ca;
extern struct cfattach dwxe_ca;
extern struct cfattach imxanatop_ca;
extern struct cfattach imxccm_ca;
extern struct cfattach imxdog_ca;
extern struct cfattach imxdwusb_ca;
extern struct cfattach imxehci_ca;
extern struct cfattach imxesdhc_ca;
extern struct cfattach imxgpc_ca;
extern struct cfattach imxgpio_ca;
extern struct cfattach imxiic_fdt_ca;
extern struct cfattach imxiomuxc_ca;
extern struct cfattach imxpciephy_ca;
extern struct cfattach imxsrc_ca;
extern struct cfattach imxuart_ca;
extern struct cfattach fec_ca;
extern struct cfattach ccp_fdt_ca;
extern struct cfattach com_fdt_ca;
extern struct cfattach fusbtc_ca;
extern struct cfattach sfp_ca;
extern struct cfattach bdpmic_ca;
extern struct cfattach tcpci_ca;
extern struct cfattach ampintc_ca;
extern struct cfattach ampintcmsi_ca;
extern struct cfattach agintc_ca;
extern struct cfattach agintcmsi_ca;
extern struct cfattach agtimer_ca;
extern struct cfattach apldart_ca;
extern struct cfattach apldog_ca;
extern struct cfattach apldwusb_ca;
extern struct cfattach aplintc_ca;
extern struct cfattach aplpcie_ca;
extern struct cfattach aplpinctrl_ca;
extern struct cfattach aplns_ca;
extern struct cfattach nvme_ans_ca;
extern struct cfattach aplpmu_ca;
extern struct cfattach aplspmi_ca;
extern struct cfattach bcmintc_ca;
extern struct cfattach pciecam_ca;
extern struct cfattach smmu_fdt_ca;
extern struct cfattach acpiec_ca;
extern struct cfattach acpimcfg_ca;
extern struct cfattach ahci_acpi_ca;
extern struct cfattach com_acpi_ca;
extern struct cfattach pluart_acpi_ca;
extern struct cfattach sdhc_acpi_ca;
extern struct cfattach xhci_acpi_ca;
extern struct cfattach ccp_acpi_ca;
extern struct cfattach bse_acpi_ca;
extern struct cfattach imxiic_acpi_ca;
extern struct cfattach acpi_fdt_ca;
extern struct cfattach acpiiort_ca;
extern struct cfattach acpipci_ca;
extern struct cfattach smmu_acpi_ca;


/* locators */
static long loc[10] = {
	-1, -1, -1, -1, -1, -1, -1, -1,
	1, 0,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"early",
	"target",
	"lun",
	"console",
	"primary",
	"mux",
	"addr",
	"size",
	"port",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"reportid",
	"phy",
	"bus",
	"dev",
	"function",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 1, 2, -1, 3, 4, 5, -1,
	3, 5, -1, 6, 7, -1, 8, 9,
	10, 11, 12, 13, -1, 8, 9, 10,
	11, 12, 13, -1, 14, -1, 15, -1,
	15, -1, 15, -1, 15, -1, 15, -1,
	15, -1, 15, -1, 15, -1, 15, -1,
	15, -1, 15, -1, 15, -1, 15, -1,
	15, -1, 15, -1, 15, -1, 15, -1,
	15, -1, 15, -1, 16, -1, 16, -1,
	16, -1, 16, -1, 16, -1, 16, -1,
	16, -1, 16, -1, 17, 18, -1,
};

/* size of parent vectors */
int pv_size = 120;

/* parent vectors */
short pv[120] = {
	192, 178, 173, 171, 127, 86, 84, 42, 41, 38, 37, 36, 31, 30, 29, 28,
	117, 118, 75, -1, 206, 201, 199, 190, 182, 170, 159, 155, 151, 149, 146, 135,
	132, 104, 2, 1, -1, 183, 119, 129, 80, 79, 128, 81, 223, 131, 82, -1,
	93, 62, 59, 27, 0, 211, 73, 219, 126, 72, -1, 228, 161, 215, 208, 114,
	168, 156, 74, -1, 222, 130, 184, 116, 137, 160, 102, -1, 225, 187, 107, 136,
	167, 153, 105, -1, 125, 88, -1, 23, 24, -1, 13, -1, 71, -1, 26, -1,
	4, -1, 226, -1, 96, -1, 172, -1, 83, -1, 99, -1, 22, -1, 93, -1,
	210, -1, 213, -1, 227, -1, 25, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: softraid0 at root */
    {&softraid_ca,	&softraid_cd,	 0, NORM,     loc,    0, pv+19, 0,    0},
/*  1: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+19, 0,    0},
/*  2: simplebus* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&simplebus_ca,	&simplebus_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*  3: efi0 at mainbus0 early 0 */
    {&efi_ca,		&efi_cd,	 0, NORM, loc+  9,    0, pv+35, 31,    0},
/*  4: scsibus* at sdmmc*|vioscsi*|vioblk*|umass*|softraid0|nvme*|nvme*|ahci*|ahci*|ahci* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+48, 32,    0},
/*  5: cd* at scsibus* target -1 lun -1 */
    {&cd_ca,		&cd_cd,		 0, STAR, loc+  4,    0, pv+96, 33,    0},
/*  6: ch* at scsibus* target -1 lun -1 */
    {&ch_ca,		&ch_cd,		 0, STAR, loc+  4,    0, pv+96, 33,    0},
/*  7: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+  4,    0, pv+96, 33,    0},
/*  8: st* at scsibus* target -1 lun -1 */
    {&st_ca,		&st_cd,		 0, STAR, loc+  4,    0, pv+96, 33,    0},
/*  9: uk* at scsibus* target -1 lun -1 */
    {&uk_ca,		&uk_cd,		 0, STAR, loc+  4,    0, pv+96, 33,    0},
/* 10: wsdisplay* at simplefb* console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+  6,    0, pv+100, 36,    0},
/* 11: wskbd* at ukbd* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+  7,    0, pv+94, 40,    0},
/* 12: cpu0 at mainbus0 */
    {&cpu_ca,		&cpu_cd,	 0, NORM,     loc,    0, pv+35, 31,    0},
/* 13: iic* at imxiic*|imxiic*|bcmbsc*|amliic*|mviic*|rkiic*|sxitwi* */
    {&iic_ca,		&iic_cd,	 0, STAR,     loc,    0, pv+76, 42,    0},
/* 14: maxrtc* at iic* addr -1 size -1 */
    {&maxrtc_ca,	&maxrtc_cd,	 0, STAR, loc+  4,    0, pv+90, 43,    0},
/* 15: dsxrtc* at iic* addr -1 size -1 */
    {&dsxrtc_ca,	&dsxrtc_cd,	 0, STAR, loc+  4,    0, pv+90, 43,    0},
/* 16: pcfrtc* at iic* addr -1 size -1 */
    {&pcfrtc_ca,	&pcfrtc_cd,	 0, STAR, loc+  4,    0, pv+90, 43,    0},
/* 17: pcxrtc* at iic* addr -1 size -1 */
    {&pcxrtc_ca,	&pcxrtc_cd,	 0, STAR, loc+  4,    0, pv+90, 43,    0},
/* 18: islrtc* at iic* addr -1 size -1 */
    {&islrtc_ca,	&islrtc_cd,	 0, STAR, loc+  4,    0, pv+90, 43,    0},
/* 19: abcrtc* at iic* addr -1 size -1 */
    {&abcrtc_ca,	&abcrtc_cd,	 0, STAR, loc+  4,    0, pv+90, 43,    0},
/* 20: mcprtc* at iic* addr -1 size -1 */
    {&mcprtc_ca,	&mcprtc_cd,	 0, STAR, loc+  4,    0, pv+90, 43,    0},
/* 21: mfokrtc* at iic* addr -1 size -1 */
    {&mfokrtc_ca,	&mfokrtc_cd,	 0, STAR, loc+  4,    0, pv+90, 43,    0},
/* 22: usb* at imxehci*|dwctwo*|ohci*|ohci*|uhci*|ehci*|ehci*|xhci*|xhci*|xhci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+37, 45,    0},
/* 23: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+108, 45,    0},
/* 24: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 25: uhidev* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhidev_ca,	&uhidev_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 26: ukbd* at uhidev* reportid -1 */
    {&ukbd_ca,		&ukbd_cd,	 0, STAR, loc+  5,    0, pv+118, 60,    0},
/* 27: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 28: aue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&aue_ca,		&aue_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 29: axe* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axe_ca,		&axe_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 30: axen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axen_ca,		&axen_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 31: smsc* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&smsc_ca,		&smsc_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 32: cue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cue_ca,		&cue_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 33: kue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&kue_ca,		&kue_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 34: cdce* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cdce_ca,		&cdce_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 35: urndis* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urndis_ca,	&urndis_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 36: mos* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mos_ca,		&mos_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 37: mue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mue_ca,		&mue_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 38: udav* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udav_ca,		&udav_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 39: upl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upl_ca,		&upl_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 40: ugl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugl_ca,		&ugl_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 41: url* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&url_ca,		&url_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 42: ure* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ure_ca,		&ure_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 43: wi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&wi_usb_ca,	&wi_cd,		 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 44: atu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&atu_ca,		&atu_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 45: ural* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ural_ca,		&ural_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 46: rum* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rum_ca,		&rum_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 47: run* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&run_ca,		&run_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 48: zyd* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&zyd_ca,		&zyd_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 49: upgt* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upgt_ca,		&upgt_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 50: urtw* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtw_ca,		&urtw_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 51: urtwn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtwn_ca,		&urtwn_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 52: rsu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rsu_ca,		&rsu_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 53: otus* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&otus_ca,		&otus_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 54: uath* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uath_ca,		&uath_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 55: athn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&athn_usb_ca,	&athn_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 56: bwfm* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&bwfm_usb_ca,	&bwfm_cd,	 0, STAR, loc+  0,    0, pv+87, 46,    0},
/* 57: gpio* at sxipio* */
    {&gpio_ca,		&gpio_cd,	 0, STAR,     loc,    0, pv+106, 61,    0},
/* 58: vio* at virtio*|virtio* */
    {&vio_ca,		&vio_cd,	 0, STAR,     loc,    0, pv+84, 61,    0},
/* 59: vioblk* at virtio*|virtio* */
    {&vioblk_ca,	&vioblk_cd,	 0, STAR,     loc,    0, pv+84, 61,    0},
/* 60: viomb* at virtio*|virtio* */
    {&viomb_ca,		&viomb_cd,	 0, STAR,     loc,    0, pv+84, 61,    0},
/* 61: viornd* at virtio*|virtio* */
    {&viornd_ca,	&viornd_cd,	 0, STAR,     loc,    0, pv+84, 61,    0},
/* 62: vioscsi* at virtio*|virtio* */
    {&vioscsi_ca,	&vioscsi_cd,	 0, STAR,     loc,    0, pv+84, 61,    0},
/* 63: eephy* at fec*|dwxe*|mvpp*|mvneta*|dwge*|bge*|msk*|ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|bse*|bse*|re* phy -1 */
    {&eephy_ca,		&eephy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 62,    0},
/* 64: rlphy* at fec*|dwxe*|mvpp*|mvneta*|dwge*|bge*|msk*|ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|bse*|bse*|re* phy -1 */
    {&rlphy_ca,		&rlphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 62,    0},
/* 65: ukphy* at fec*|dwxe*|mvpp*|mvneta*|dwge*|bge*|msk*|ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|bse*|bse*|re* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 62,    0},
/* 66: brgphy* at fec*|dwxe*|mvpp*|mvneta*|dwge*|bge*|msk*|ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|bse*|bse*|re* phy -1 */
    {&brgphy_ca,	&brgphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 62,    0},
/* 67: amphy* at fec*|dwxe*|mvpp*|mvneta*|dwge*|bge*|msk*|ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|bse*|bse*|re* phy -1 */
    {&amphy_ca,		&amphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 62,    0},
/* 68: urlphy* at fec*|dwxe*|mvpp*|mvneta*|dwge*|bge*|msk*|ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|bse*|bse*|re* phy -1 */
    {&urlphy_ca,	&urlphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 62,    0},
/* 69: rgephy* at fec*|dwxe*|mvpp*|mvneta*|dwge*|bge*|msk*|ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|bse*|bse*|re* phy -1 */
    {&rgephy_ca,	&rgephy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 62,    0},
/* 70: atphy* at fec*|dwxe*|mvpp*|mvneta*|dwge*|bge*|msk*|ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|bse*|bse*|re* phy -1 */
    {&atphy_ca,		&atphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 62,    0},
/* 71: pci* at acpipci*|dwpcie*|pciecam*|aplpcie*|bcmpcie*|mvkpcie*|rkpcie*|ppb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+  5,    0, pv+59, 100,    0},
/* 72: ahci* at pci* dev -1 function -1 */
    {&ahci_pci_ca,	&ahci_cd,	 0, STAR, loc+  4,    0, pv+92, 116,    0},
/* 73: nvme* at pci* dev -1 function -1 */
    {&nvme_pci_ca,	&nvme_cd,	 0, STAR, loc+  4,    0, pv+92, 116,    0},
/* 74: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+  4,    0, pv+92, 116,    0},
/* 75: re* at pci* dev -1 function -1 */
    {&re_pci_ca,	&re_cd,		 0, STAR, loc+  4,    0, pv+92, 116,    0},
/* 76: em* at pci* dev -1 function -1 */
    {&em_ca,		&em_cd,		 0, STAR, loc+  4,    0, pv+92, 116,    0},
/* 77: ix* at pci* dev -1 function -1 */
    {&ix_ca,		&ix_cd,		 0, STAR, loc+  4,    0, pv+92, 116,    0},
/* 78: ixl* at pci* dev -1 function -1 */
    {&ixl_ca,		&ixl_cd,	 0, STAR, loc+  4,    0, pv+92, 116,    0},
/* 79: uhci* at pci* dev -1 function -1 */
    {&uhci_pci_ca,	&uhci_cd,	 0, STAR, loc+  4,    0, pv+92, 116,    0},
/* 80: ohci* at pci* dev -1 function -1 */
    {&ohci_pci_ca,	&ohci_cd,	 0, STAR, loc+  4,    0, pv+92, 116,    0},
/* 81: ehci* at pci* dev -1 function -1 */
    {&ehci_pci_ca,	&ehci_cd,	 0, STAR, loc+  4,    0, pv+92, 116,    0},
/* 82: xhci* at pci* dev -1 function -1 */
    {&xhci_pci_ca,	&xhci_cd,	 0, STAR, loc+  4,    0, pv+92, 116,    0},
/* 83: mskc* at pci* dev -1 function -1 */
    {&mskc_ca,		&mskc_cd,	 0, STAR, loc+  4,    0, pv+92, 116,    0},
/* 84: msk* at mskc* */
    {&msk_ca,		&msk_cd,	 0, STAR,     loc,    0, pv+104, 118,    0},
/* 85: iwx* at pci* dev -1 function -1 */
    {&iwx_ca,		&iwx_cd,	 0, STAR, loc+  4,    0, pv+92, 116,    0},
/* 86: bge* at pci* dev -1 function -1 */
    {&bge_ca,		&bge_cd,	 0, STAR, loc+  4,    0, pv+92, 116,    0},
/* 87: athn* at pci* dev -1 function -1 */
    {&athn_pci_ca,	&athn_cd,	 0, STAR, loc+  4,    0, pv+92, 116,    0},
/* 88: virtio* at pci* dev -1 function -1 */
    {&virtio_pci_ca,	&virtio_cd,	 0, STAR, loc+  4,    0, pv+92, 116,    0},
/* 89: bwfm* at pci* dev -1 function -1 */
    {&bwfm_pci_ca,	&bwfm_cd,	 0, STAR, loc+  4,    0, pv+92, 116,    0},
/* 90: bnxt* at pci* dev -1 function -1 */
    {&bnxt_ca,		&bnxt_cd,	 0, STAR, loc+  4,    0, pv+92, 116,    0},
/* 91: mcx* at pci* dev -1 function -1 */
    {&mcx_ca,		&mcx_cd,	 0, STAR, loc+  4,    0, pv+92, 116,    0},
/* 92: rge* at pci* dev -1 function -1 */
    {&rge_ca,		&rge_cd,	 0, STAR, loc+  4,    0, pv+92, 116,    0},
/* 93: sdmmc* at sdhc*|sdhc*|imxesdhc*|bcmsdhost*|amlmmc*|dwmmc*|sximmc* */
    {&sdmmc_ca,		&sdmmc_cd,	 0, STAR,     loc,    0, pv+68, 118,    0},
/* 94: bwfm* at sdmmc* */
    {&bwfm_sdio_ca,	&bwfm_cd,	 0, STAR,     loc,    0, pv+110, 32,    0},
/* 95: pinctrl* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&pinctrl_ca,	&pinctrl_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/* 96: simplefb* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&simplefb_ca,	&simplefb_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/* 97: sxiccmu* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&sxiccmu_ca,	&sxiccmu_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/* 98: sxidog* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&sxidog_ca,	&sxidog_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/* 99: sxipio* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&sxipio_ca,	&sxipio_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*100: sxirsb* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&sxirsb_ca,	&sxirsb_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*101: sxirtc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&sxirtc_ca,	&sxirtc_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*102: sximmc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&sximmc_ca,	&sximmc_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*103: sxisid* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&sxisid_ca,	&sxisid_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*104: sxisyscon* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&sxisyscon_ca,	&sxisyscon_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*105: sxitwi* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&sxitwi_ca,	&sxitwi_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*106: bcmaux* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&bcmaux_ca,	&bcmaux_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*107: bcmbsc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&bcmbsc_ca,	&bcmbsc_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*108: bcmclock* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&bcmclock_ca,	&bcmclock_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*109: bcmdmac* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&bcmdmac_ca,	&bcmdmac_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*110: bcmdog* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&bcmdog_ca,	&bcmdog_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*111: bcmgpio* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&bcmgpio_ca,	&bcmgpio_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*112: bcmirng* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&bcmirng_ca,	&bcmirng_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*113: bcmmbox* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&bcmmbox_ca,	&bcmmbox_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*114: bcmpcie* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&bcmpcie_ca,	&bcmpcie_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*115: bcmrng* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&bcmrng_ca,	&bcmrng_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*116: bcmsdhost* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&bcmsdhost_ca,	&bcmsdhost_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*117: bse* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&bse_fdt_ca,	&bse_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*118: bse* at acpi0 */
    {&bse_acpi_ca,	&bse_cd,	 0, STAR,     loc,    0, pv+98, 118,    0},
/*119: dwctwo* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&bcmdwctwo_ca,	&dwctwo_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*120: exuart* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&exuart_ca,	&exuart_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*121: plgpio* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&plgpio_ca,	&plgpio_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*122: plrtc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&plrtc_ca,		&plrtc_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*123: pluart* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&pluart_fdt_ca,	&pluart_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*124: psci* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&psci_ca,		&psci_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*125: virtio* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&virtio_mmio_ca,	&virtio_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*126: ahci* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&ahci_fdt_ca,	&ahci_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*127: dwge* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&dwge_ca,		&dwge_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*128: ehci* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&ehci_fdt_ca,	&ehci_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*129: ohci* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&ohci_fdt_ca,	&ohci_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*130: sdhc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&sdhc_fdt_ca,	&sdhc_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*131: xhci* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&xhci_fdt_ca,	&xhci_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*132: syscon* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&syscon_ca,	&syscon_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*133: pwmfan* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&pwmfan_ca,	&pwmfan_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*134: amlclock* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&amlclock_ca,	&amlclock_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*135: amldwusb* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&amldwusb_ca,	&amldwusb_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*136: amliic* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&amliic_ca,	&amliic_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*137: amlmmc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&amlmmc_ca,	&amlmmc_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*138: amlpciephy* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&amlpciephy_ca,	&amlpciephy_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*139: amlpinctrl* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&amlpinctrl_ca,	&amlpinctrl_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*140: amlpwrc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&amlpwrc_ca,	&amlpwrc_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*141: amlreset* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&amlreset_ca,	&amlreset_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*142: amlrng* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&amlrng_ca,	&amlrng_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*143: amluart* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&amluart_ca,	&amluart_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*144: amlusbphy* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&amlusbphy_ca,	&amlusbphy_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*145: hiclock* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&hiclock_ca,	&hiclock_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*146: hidwusb* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&hidwusb_ca,	&hidwusb_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*147: hireset* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&hireset_ca,	&hireset_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*148: rkclock* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&rkclock_ca,	&rkclock_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*149: rkdwusb* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&rkdwusb_ca,	&rkdwusb_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*150: rkemmcphy* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&rkemmcphy_ca,	&rkemmcphy_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*151: rkgrf* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&rkgrf_ca,		&rkgrf_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*152: rkgpio* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&rkgpio_ca,	&rkgpio_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*153: rkiic* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&rkiic_ca,		&rkiic_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*154: rkpmic* at iic* addr -1 size -1 */
    {&rkpmic_ca,	&rkpmic_cd,	 0, STAR, loc+  4,    0, pv+90, 43,    0},
/*155: rkpinctrl* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&rkpinctrl_ca,	&rkpinctrl_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*156: rkpcie* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&rkpcie_ca,	&rkpcie_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*157: rkpwm* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&rkpwm_ca,		&rkpwm_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*158: rkrng* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&rkrng_ca,		&rkrng_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*159: rktcphy* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&rktcphy_ca,	&rktcphy_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*160: dwmmc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&dwmmc_ca,		&dwmmc_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*161: dwpcie* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&dwpcie_ca,	&dwpcie_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*162: mvclock* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&mvclock_ca,	&mvclock_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*163: mvdog* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&mvdog_ca,		&mvdog_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*164: mvgicp* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&mvgicp_ca,	&mvgicp_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*165: mvgpio* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&mvgpio_ca,	&mvgpio_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*166: mvicu* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&mvicu_ca,		&mvicu_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*167: mviic* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&mviic_ca,		&mviic_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*168: mvkpcie* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&mvkpcie_ca,	&mvkpcie_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*169: mvpinctrl* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&mvpinctrl_ca,	&mvpinctrl_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*170: mvmdio* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&mvmdio_ca,	&mvmdio_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*171: mvneta* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&mvneta_ca,	&mvneta_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*172: mvppc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&mvppc_ca,		&mvppc_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*173: mvpp* at mvppc* */
    {&mvpp_ca,		&mvpp_cd,	 0, STAR,     loc,    0, pv+102, 118,    0},
/*174: mvrng* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&mvrng_ca,		&mvrng_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*175: mvrtc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&mvrtc_ca,		&mvrtc_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*176: mvsw* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&mvsw_ca,		&mvsw_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*177: mvuart* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&mvuart_ca,	&mvuart_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*178: dwxe* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&dwxe_ca,		&dwxe_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*179: imxanatop* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&imxanatop_ca,	&imxanatop_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*180: imxccm* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&imxccm_ca,	&imxccm_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*181: imxdog* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&imxdog_ca,	&imxdog_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*182: imxdwusb* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&imxdwusb_ca,	&imxdwusb_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*183: imxehci* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&imxehci_ca,	&imxehci_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*184: imxesdhc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&imxesdhc_ca,	&imxesdhc_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*185: imxgpc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&imxgpc_ca,	&imxgpc_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*186: imxgpio* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&imxgpio_ca,	&imxgpio_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*187: imxiic* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&imxiic_fdt_ca,	&imxiic_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*188: imxiomuxc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&imxiomuxc_ca,	&imxiomuxc_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*189: imxpciephy* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&imxpciephy_ca,	&imxpciephy_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*190: imxsrc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&imxsrc_ca,	&imxsrc_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*191: imxuart* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&imxuart_ca,	&imxuart_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*192: fec* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&fec_ca,		&fec_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*193: ccp* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&ccp_fdt_ca,	&ccp_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*194: com* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&com_fdt_ca,	&com_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*195: fusbtc* at iic* addr -1 size -1 */
    {&fusbtc_ca,	&fusbtc_cd,	 0, STAR, loc+  4,    0, pv+90, 43,    0},
/*196: sfp* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&sfp_ca,		&sfp_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*197: bdpmic* at iic* addr -1 size -1 */
    {&bdpmic_ca,	&bdpmic_cd,	 0, STAR, loc+  4,    0, pv+90, 43,    0},
/*198: tcpci* at iic* addr -1 size -1 */
    {&tcpci_ca,		&tcpci_cd,	 0, STAR, loc+  4,    0, pv+90, 43,    0},
/*199: ampintc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&ampintc_ca,	&ampintc_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*200: ampintcmsi* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&ampintcmsi_ca,	&ampintcmsi_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*201: agintc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&agintc_ca,	&agintc_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*202: agintcmsi* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&agintcmsi_ca,	&agintcmsi_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*203: agtimer* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&agtimer_ca,	&agtimer_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*204: apldart* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&apldart_ca,	&apldart_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*205: apldog* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&apldog_ca,	&apldog_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*206: apldwusb* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&apldwusb_ca,	&apldwusb_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*207: aplintc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&aplintc_ca,	&aplintc_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*208: aplpcie* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&aplpcie_ca,	&aplpcie_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*209: aplpinctrl* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&aplpinctrl_ca,	&aplpinctrl_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*210: aplns* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&aplns_ca,		&aplns_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*211: nvme* at aplns* */
    {&nvme_ans_ca,	&nvme_cd,	 0, STAR,     loc,    0, pv+112, 118,    0},
/*212: aplpmu* at aplspmi* */
    {&aplpmu_ca,	&aplpmu_cd,	 0, STAR,     loc,    0, pv+114, 118,    0},
/*213: aplspmi* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&aplspmi_ca,	&aplspmi_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*214: bcmintc* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 1 */
    {&bcmintc_ca,	&bcmintc_cd,	 0, STAR, loc+  8,    0, pv+20, 1,    0},
/*215: pciecam* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&pciecam_ca,	&pciecam_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*216: smmu* at apldwusb*|agintc*|ampintc*|imxsrc*|imxdwusb*|mvmdio*|rktcphy*|rkpinctrl*|rkgrf*|rkdwusb*|hidwusb*|amldwusb*|syscon*|sxisyscon*|simplebus*|mainbus0 early 0 */
    {&smmu_fdt_ca,	&smmu_cd,	 0, STAR, loc+  9,    0, pv+20, 1,    0},
/*217: acpiec* at acpi0 */
    {&acpiec_ca,	&acpiec_cd,	 0, STAR,     loc,    0, pv+98, 118,    0},
/*218: acpimcfg* at acpi0 */
    {&acpimcfg_ca,	&acpimcfg_cd,	 0, STAR,     loc,    0, pv+98, 118,    0},
/*219: ahci* at acpi0 */
    {&ahci_acpi_ca,	&ahci_cd,	 0, STAR,     loc,    0, pv+98, 118,    0},
/*220: com* at acpi0 */
    {&com_acpi_ca,	&com_cd,	 0, STAR,     loc,    0, pv+98, 118,    0},
/*221: pluart* at acpi0 */
    {&pluart_acpi_ca,	&pluart_cd,	 0, STAR,     loc,    0, pv+98, 118,    0},
/*222: sdhc* at acpi0 */
    {&sdhc_acpi_ca,	&sdhc_cd,	 0, STAR,     loc,    0, pv+98, 118,    0},
/*223: xhci* at acpi0 */
    {&xhci_acpi_ca,	&xhci_cd,	 0, STAR,     loc,    0, pv+98, 118,    0},
/*224: ccp* at acpi0 */
    {&ccp_acpi_ca,	&ccp_cd,	 0, STAR,     loc,    0, pv+98, 118,    0},
/*225: imxiic* at acpi0 */
    {&imxiic_acpi_ca,	&imxiic_cd,	 0, STAR,     loc,    0, pv+98, 118,    0},
/*226: acpi0 at mainbus0 early 0 */
    {&acpi_fdt_ca,	&acpi_cd,	 0, NORM, loc+  9,    0, pv+35, 31,    0},
/*227: acpiiort* at acpi0 */
    {&acpiiort_ca,	&acpiiort_cd,	 0, STAR,     loc,    0, pv+98, 118,    0},
/*228: acpipci* at acpi0 */
    {&acpipci_ca,	&acpipci_cd,	 0, STAR,     loc,    0, pv+98, 118,    0},
/*229: smmu* at acpiiort* */
    {&smmu_acpi_ca,	&smmu_cd,	 0, STAR,     loc,    0, pv+116, 118,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 0 /* softraid0 */,
	 1 /* mainbus0 */,
	-1
};

int cfroots_size = 3;

/* pseudo-devices */
extern void loopattach(int);
extern void vlanattach(int);
extern void trunkattach(int);
extern void bpfilterattach(int);
extern void rdattach(int);
extern void bioattach(int);

char *pdevnames[] = {
	"loop",
	"vlan",
	"trunk",
	"bpfilter",
	"rd",
	"bio",
};

int pdevnames_size = 6;

struct pdevinit pdevinit[] = {
	{ loopattach, 1 },
	{ vlanattach, 1 },
	{ trunkattach, 1 },
	{ bpfilterattach, 1 },
	{ rdattach, 1 },
	{ bioattach, 1 },
	{ NULL, 0 }
};
