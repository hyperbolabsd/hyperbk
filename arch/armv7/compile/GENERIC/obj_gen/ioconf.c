/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/armv7/conf/GENERIC"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver video_cd;
extern struct cfdriver audio_cd;
extern struct cfdriver midi_cd;
extern struct cfdriver ahci_cd;
extern struct cfdriver nvme_cd;
extern struct cfdriver wi_cd;
extern struct cfdriver re_cd;
extern struct cfdriver com_cd;
extern struct cfdriver pluart_cd;
extern struct cfdriver athn_cd;
extern struct cfdriver bwfm_cd;
extern struct cfdriver virtio_cd;
extern struct cfdriver uhci_cd;
extern struct cfdriver ohci_cd;
extern struct cfdriver ehci_cd;
extern struct cfdriver xhci_cd;
extern struct cfdriver sdhc_cd;
extern struct cfdriver radio_cd;
extern struct cfdriver vscsi_cd;
extern struct cfdriver softraid_cd;
extern struct cfdriver imxiic_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver simplebus_cd;
extern struct cfdriver cortex_cd;
extern struct cfdriver ampintc_cd;
extern struct cfdriver ampintcmsi_cd;
extern struct cfdriver amptimer_cd;
extern struct cfdriver agtimer_cd;
extern struct cfdriver armliicc_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver cd_cd;
extern struct cfdriver sdmmc_cd;
extern struct cfdriver rlphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver rgephy_cd;
extern struct cfdriver atphy_cd;
extern struct cfdriver iic_cd;
extern struct cfdriver pcagpio_cd;
extern struct cfdriver maxrtc_cd;
extern struct cfdriver dsxrtc_cd;
extern struct cfdriver pcfrtc_cd;
extern struct cfdriver pcxrtc_cd;
extern struct cfdriver islrtc_cd;
extern struct cfdriver abcrtc_cd;
extern struct cfdriver mcprtc_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver ch_cd;
extern struct cfdriver st_cd;
extern struct cfdriver uk_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver wsmouse_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver uaudio_cd;
extern struct cfdriver uvideo_cd;
extern struct cfdriver utvfu_cd;
extern struct cfdriver udl_cd;
extern struct cfdriver umidi_cd;
extern struct cfdriver ucom_cd;
extern struct cfdriver ugen_cd;
extern struct cfdriver uhidev_cd;
extern struct cfdriver uhid_cd;
extern struct cfdriver fido_cd;
extern struct cfdriver ujoy_cd;
extern struct cfdriver ukbd_cd;
extern struct cfdriver ums_cd;
extern struct cfdriver uts_cd;
extern struct cfdriver ucycom_cd;
extern struct cfdriver uslhcom_cd;
extern struct cfdriver ulpt_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver uthum_cd;
extern struct cfdriver ugold_cd;
extern struct cfdriver utrh_cd;
extern struct cfdriver uoakrh_cd;
extern struct cfdriver uoaklux_cd;
extern struct cfdriver uoakv_cd;
extern struct cfdriver uonerng_cd;
extern struct cfdriver urng_cd;
extern struct cfdriver udcf_cd;
extern struct cfdriver uvisor_cd;
extern struct cfdriver udsbr_cd;
extern struct cfdriver utwitch_cd;
extern struct cfdriver aue_cd;
extern struct cfdriver axe_cd;
extern struct cfdriver axen_cd;
extern struct cfdriver smsc_cd;
extern struct cfdriver cue_cd;
extern struct cfdriver kue_cd;
extern struct cfdriver cdce_cd;
extern struct cfdriver urndis_cd;
extern struct cfdriver mos_cd;
extern struct cfdriver mue_cd;
extern struct cfdriver udav_cd;
extern struct cfdriver upl_cd;
extern struct cfdriver ugl_cd;
extern struct cfdriver url_cd;
extern struct cfdriver ure_cd;
extern struct cfdriver umodem_cd;
extern struct cfdriver uftdi_cd;
extern struct cfdriver uplcom_cd;
extern struct cfdriver umct_cd;
extern struct cfdriver uvscom_cd;
extern struct cfdriver ubsa_cd;
extern struct cfdriver uslcom_cd;
extern struct cfdriver uark_cd;
extern struct cfdriver moscom_cd;
extern struct cfdriver umcs_cd;
extern struct cfdriver uscom_cd;
extern struct cfdriver ucrcom_cd;
extern struct cfdriver uipaq_cd;
extern struct cfdriver umsm_cd;
extern struct cfdriver uchcom_cd;
extern struct cfdriver uticom_cd;
extern struct cfdriver atu_cd;
extern struct cfdriver ural_cd;
extern struct cfdriver rum_cd;
extern struct cfdriver run_cd;
extern struct cfdriver zyd_cd;
extern struct cfdriver upgt_cd;
extern struct cfdriver urtw_cd;
extern struct cfdriver urtwn_cd;
extern struct cfdriver rsu_cd;
extern struct cfdriver otus_cd;
extern struct cfdriver uath_cd;
extern struct cfdriver uow_cd;
extern struct cfdriver uberry_cd;
extern struct cfdriver upd_cd;
extern struct cfdriver uwacom_cd;
extern struct cfdriver uhidpp_cd;
extern struct cfdriver ucc_cd;
extern struct cfdriver vio_cd;
extern struct cfdriver vioblk_cd;
extern struct cfdriver viomb_cd;
extern struct cfdriver viornd_cd;
extern struct cfdriver vioscsi_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver em_cd;
extern struct cfdriver ix_cd;
extern struct cfdriver mskc_cd;
extern struct cfdriver msk_cd;
extern struct cfdriver gpio_cd;
extern struct cfdriver gpioiic_cd;
extern struct cfdriver gpioow_cd;
extern struct cfdriver onewire_cd;
extern struct cfdriver owid_cd;
extern struct cfdriver owsbm_cd;
extern struct cfdriver owtemp_cd;
extern struct cfdriver owctr_cd;
extern struct cfdriver pinctrl_cd;
extern struct cfdriver simplefb_cd;
extern struct cfdriver sxiccmu_cd;
extern struct cfdriver sxidog_cd;
extern struct cfdriver sxipio_cd;
extern struct cfdriver sxirsb_cd;
extern struct cfdriver sxirtc_cd;
extern struct cfdriver sximmc_cd;
extern struct cfdriver sxisid_cd;
extern struct cfdriver sxisyscon_cd;
extern struct cfdriver sxitemp_cd;
extern struct cfdriver sxits_cd;
extern struct cfdriver sxitwi_cd;
extern struct cfdriver axppmic_cd;
extern struct cfdriver bcmaux_cd;
extern struct cfdriver bcmbsc_cd;
extern struct cfdriver bcmclock_cd;
extern struct cfdriver bcmdmac_cd;
extern struct cfdriver bcmdog_cd;
extern struct cfdriver bcmgpio_cd;
extern struct cfdriver bcmmbox_cd;
extern struct cfdriver bcmrng_cd;
extern struct cfdriver bcmsdhost_cd;
extern struct cfdriver bcmtemp_cd;
extern struct cfdriver dwctwo_cd;
extern struct cfdriver exrtc_cd;
extern struct cfdriver exuart_cd;
extern struct cfdriver plrtc_cd;
extern struct cfdriver psci_cd;
extern struct cfdriver dwge_cd;
extern struct cfdriver syscon_cd;
extern struct cfdriver cad_cd;
extern struct cfdriver cduart_cd;
extern struct cfdriver rkclock_cd;
extern struct cfdriver rkgrf_cd;
extern struct cfdriver rkgpio_cd;
extern struct cfdriver rkiic_cd;
extern struct cfdriver rkpmic_cd;
extern struct cfdriver rkpinctrl_cd;
extern struct cfdriver rktemp_cd;
extern struct cfdriver dwmmc_cd;
extern struct cfdriver dwdog_cd;
extern struct cfdriver mvpinctrl_cd;
extern struct cfdriver mvmdio_cd;
extern struct cfdriver mvneta_cd;
extern struct cfdriver dwxe_cd;
extern struct cfdriver acrtc_cd;
extern struct cfdriver imxanatop_cd;
extern struct cfdriver imxccm_cd;
extern struct cfdriver imxdog_cd;
extern struct cfdriver imxehci_cd;
extern struct cfdriver imxesdhc_cd;
extern struct cfdriver imxgpc_cd;
extern struct cfdriver imxgpio_cd;
extern struct cfdriver imxiomuxc_cd;
extern struct cfdriver imxrtc_cd;
extern struct cfdriver imxsrc_cd;
extern struct cfdriver imxuart_cd;
extern struct cfdriver fec_cd;
extern struct cfdriver imxspi_cd;
extern struct cfdriver ssdfb_cd;
extern struct cfdriver imxahci_cd;
extern struct cfdriver imxtemp_cd;
extern struct cfdriver omap_cd;
extern struct cfdriver omcm_cd;
extern struct cfdriver omclock_cd;
extern struct cfdriver ommmc_cd;
extern struct cfdriver cpsw_cd;
extern struct cfdriver prcm_cd;
extern struct cfdriver omgpio_cd;
extern struct cfdriver tiiic_cd;
extern struct cfdriver intc_cd;
extern struct cfdriver omwugen_cd;
extern struct cfdriver gptimer_cd;
extern struct cfdriver dmtimer_cd;
extern struct cfdriver omapid_cd;
extern struct cfdriver omdog_cd;
extern struct cfdriver omrng_cd;
extern struct cfdriver omehci_cd;
extern struct cfdriver omsysc_cd;
extern struct cfdriver omusbtll_cd;
extern struct cfdriver amdisplay_cd;
extern struct cfdriver nxphdmi_cd;
extern struct cfdriver sxiintc_cd;
extern struct cfdriver sxitimer_cd;
extern struct cfdriver sxiahci_cd;
extern struct cfdriver sxie_cd;
extern struct cfdriver exclock_cd;
extern struct cfdriver expower_cd;
extern struct cfdriver exmct_cd;
extern struct cfdriver exdog_cd;
extern struct cfdriver exgpio_cd;
extern struct cfdriver exiic_cd;
extern struct cfdriver exehci_cd;
extern struct cfdriver exdwusb_cd;
extern struct cfdriver crosec_cd;
extern struct cfdriver tpspmic_cd;
extern struct cfdriver sysreg_cd;
extern struct cfdriver pciecam_cd;
extern struct cfdriver bcmintc_cd;
extern struct cfdriver mvacc_cd;
extern struct cfdriver mvagc_cd;
extern struct cfdriver mvsysctrl_cd;
extern struct cfdriver mvmbus_cd;
extern struct cfdriver mvxhci_cd;
extern struct cfdriver mvahci_cd;
extern struct cfdriver mvmpic_cd;
extern struct cfdriver mvpcie_cd;
extern struct cfdriver mvpxa_cd;
extern struct cfdriver zqclock_cd;
extern struct cfdriver zqreset_cd;

extern struct cfattach video_ca;
extern struct cfattach audio_ca;
extern struct cfattach midi_ca;
extern struct cfattach radio_ca;
extern struct cfattach vscsi_ca;
extern struct cfattach softraid_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach simplebus_ca;
extern struct cfattach cortex_ca;
extern struct cfattach ampintc_ca;
extern struct cfattach ampintcmsi_ca;
extern struct cfattach amptimer_ca;
extern struct cfattach agtimer_ca;
extern struct cfattach armliicc_ca;
extern struct cfattach cpu_ca;
extern struct cfattach sdmmc_ca;
extern struct cfattach bwfm_sdio_ca;
extern struct cfattach rlphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach rgephy_ca;
extern struct cfattach atphy_ca;
extern struct cfattach iic_ca;
extern struct cfattach pcagpio_ca;
extern struct cfattach maxrtc_ca;
extern struct cfattach dsxrtc_ca;
extern struct cfattach pcfrtc_ca;
extern struct cfattach pcxrtc_ca;
extern struct cfattach islrtc_ca;
extern struct cfattach abcrtc_ca;
extern struct cfattach mcprtc_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach cd_ca;
extern struct cfattach ch_ca;
extern struct cfattach sd_ca;
extern struct cfattach st_ca;
extern struct cfattach uk_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach wsmouse_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach uaudio_ca;
extern struct cfattach uvideo_ca;
extern struct cfattach utvfu_ca;
extern struct cfattach udl_ca;
extern struct cfattach umidi_ca;
extern struct cfattach ucom_ca;
extern struct cfattach ugen_ca;
extern struct cfattach uhidev_ca;
extern struct cfattach uhid_ca;
extern struct cfattach fido_ca;
extern struct cfattach ujoy_ca;
extern struct cfattach ukbd_ca;
extern struct cfattach ums_ca;
extern struct cfattach uts_ca;
extern struct cfattach ucycom_ca;
extern struct cfattach uslhcom_ca;
extern struct cfattach ulpt_ca;
extern struct cfattach umass_ca;
extern struct cfattach uthum_ca;
extern struct cfattach ugold_ca;
extern struct cfattach utrh_ca;
extern struct cfattach uoakrh_ca;
extern struct cfattach uoaklux_ca;
extern struct cfattach uoakv_ca;
extern struct cfattach uonerng_ca;
extern struct cfattach urng_ca;
extern struct cfattach udcf_ca;
extern struct cfattach uvisor_ca;
extern struct cfattach udsbr_ca;
extern struct cfattach utwitch_ca;
extern struct cfattach aue_ca;
extern struct cfattach axe_ca;
extern struct cfattach axen_ca;
extern struct cfattach smsc_ca;
extern struct cfattach cue_ca;
extern struct cfattach kue_ca;
extern struct cfattach cdce_ca;
extern struct cfattach urndis_ca;
extern struct cfattach mos_ca;
extern struct cfattach mue_ca;
extern struct cfattach udav_ca;
extern struct cfattach upl_ca;
extern struct cfattach ugl_ca;
extern struct cfattach url_ca;
extern struct cfattach ure_ca;
extern struct cfattach umodem_ca;
extern struct cfattach uftdi_ca;
extern struct cfattach uplcom_ca;
extern struct cfattach umct_ca;
extern struct cfattach uvscom_ca;
extern struct cfattach ubsa_ca;
extern struct cfattach uslcom_ca;
extern struct cfattach uark_ca;
extern struct cfattach moscom_ca;
extern struct cfattach umcs_ca;
extern struct cfattach uscom_ca;
extern struct cfattach ucrcom_ca;
extern struct cfattach uipaq_ca;
extern struct cfattach umsm_ca;
extern struct cfattach uchcom_ca;
extern struct cfattach uticom_ca;
extern struct cfattach wi_usb_ca;
extern struct cfattach atu_ca;
extern struct cfattach ural_ca;
extern struct cfattach rum_ca;
extern struct cfattach run_ca;
extern struct cfattach zyd_ca;
extern struct cfattach upgt_ca;
extern struct cfattach urtw_ca;
extern struct cfattach urtwn_ca;
extern struct cfattach rsu_ca;
extern struct cfattach otus_ca;
extern struct cfattach uath_ca;
extern struct cfattach athn_usb_ca;
extern struct cfattach uow_ca;
extern struct cfattach uberry_ca;
extern struct cfattach upd_ca;
extern struct cfattach uwacom_ca;
extern struct cfattach bwfm_usb_ca;
extern struct cfattach uhidpp_ca;
extern struct cfattach ucc_ca;
extern struct cfattach vio_ca;
extern struct cfattach vioblk_ca;
extern struct cfattach viomb_ca;
extern struct cfattach viornd_ca;
extern struct cfattach vioscsi_ca;
extern struct cfattach pci_ca;
extern struct cfattach ahci_pci_ca;
extern struct cfattach nvme_pci_ca;
extern struct cfattach ppb_ca;
extern struct cfattach re_pci_ca;
extern struct cfattach em_ca;
extern struct cfattach ix_ca;
extern struct cfattach uhci_pci_ca;
extern struct cfattach ohci_pci_ca;
extern struct cfattach ehci_pci_ca;
extern struct cfattach xhci_pci_ca;
extern struct cfattach mskc_ca;
extern struct cfattach msk_ca;
extern struct cfattach virtio_pci_ca;
extern struct cfattach gpio_ca;
extern struct cfattach gpioiic_ca;
extern struct cfattach gpioow_ca;
extern struct cfattach onewire_ca;
extern struct cfattach owid_ca;
extern struct cfattach owsbm_ca;
extern struct cfattach owtemp_ca;
extern struct cfattach owctr_ca;
extern struct cfattach pinctrl_ca;
extern struct cfattach simplefb_ca;
extern struct cfattach sxiccmu_ca;
extern struct cfattach sxidog_ca;
extern struct cfattach sxipio_ca;
extern struct cfattach sxirsb_ca;
extern struct cfattach sxirtc_ca;
extern struct cfattach sximmc_ca;
extern struct cfattach sxisid_ca;
extern struct cfattach sxisyscon_ca;
extern struct cfattach sxitemp_ca;
extern struct cfattach sxits_ca;
extern struct cfattach sxitwi_ca;
extern struct cfattach axppmic_ca;
extern struct cfattach axppmic_rsb_ca;
extern struct cfattach bcmaux_ca;
extern struct cfattach bcmbsc_ca;
extern struct cfattach bcmclock_ca;
extern struct cfattach bcmdmac_ca;
extern struct cfattach bcmdog_ca;
extern struct cfattach bcmgpio_ca;
extern struct cfattach bcmmbox_ca;
extern struct cfattach bcmrng_ca;
extern struct cfattach bcmsdhost_ca;
extern struct cfattach bcmtemp_ca;
extern struct cfattach bcmdwctwo_ca;
extern struct cfattach exrtc_ca;
extern struct cfattach exuart_ca;
extern struct cfattach plrtc_ca;
extern struct cfattach pluart_fdt_ca;
extern struct cfattach psci_ca;
extern struct cfattach virtio_mmio_ca;
extern struct cfattach dwge_ca;
extern struct cfattach ehci_fdt_ca;
extern struct cfattach ohci_fdt_ca;
extern struct cfattach sdhc_fdt_ca;
extern struct cfattach xhci_fdt_ca;
extern struct cfattach syscon_ca;
extern struct cfattach cad_ca;
extern struct cfattach cduart_ca;
extern struct cfattach rkclock_ca;
extern struct cfattach rkgrf_ca;
extern struct cfattach rkgpio_ca;
extern struct cfattach rkiic_ca;
extern struct cfattach rkpmic_ca;
extern struct cfattach rkpinctrl_ca;
extern struct cfattach rktemp_ca;
extern struct cfattach dwmmc_ca;
extern struct cfattach dwdog_ca;
extern struct cfattach mvpinctrl_ca;
extern struct cfattach mvmdio_ca;
extern struct cfattach mvneta_ca;
extern struct cfattach dwxe_ca;
extern struct cfattach acrtc_ca;
extern struct cfattach imxanatop_ca;
extern struct cfattach imxccm_ca;
extern struct cfattach imxdog_ca;
extern struct cfattach imxehci_ca;
extern struct cfattach imxesdhc_ca;
extern struct cfattach imxgpc_ca;
extern struct cfattach imxgpio_ca;
extern struct cfattach imxiic_fdt_ca;
extern struct cfattach imxiomuxc_ca;
extern struct cfattach imxrtc_ca;
extern struct cfattach imxsrc_ca;
extern struct cfattach imxuart_ca;
extern struct cfattach fec_ca;
extern struct cfattach imxspi_ca;
extern struct cfattach com_fdt_ca;
extern struct cfattach ssdfb_spi_ca;
extern struct cfattach ssdfb_i2c_ca;
extern struct cfattach imxahci_ca;
extern struct cfattach imxtemp_ca;
extern struct cfattach omap_ca;
extern struct cfattach omcm_ca;
extern struct cfattach omclock_ca;
extern struct cfattach ommmc_ca;
extern struct cfattach cpsw_ca;
extern struct cfattach prcm_ca;
extern struct cfattach omgpio_ca;
extern struct cfattach tiiic_ca;
extern struct cfattach intc_ca;
extern struct cfattach omwugen_ca;
extern struct cfattach gptimer_ca;
extern struct cfattach dmtimer_ca;
extern struct cfattach omapid_ca;
extern struct cfattach omdog_ca;
extern struct cfattach omrng_ca;
extern struct cfattach omehci_ca;
extern struct cfattach omsysc_ca;
extern struct cfattach omusbtll_ca;
extern struct cfattach amdisplay_ca;
extern struct cfattach nxphdmi_ca;
extern struct cfattach sxiintc_ca;
extern struct cfattach sxitimer_ca;
extern struct cfattach sxiahci_ca;
extern struct cfattach sxie_ca;
extern struct cfattach exclock_ca;
extern struct cfattach expower_ca;
extern struct cfattach exmct_ca;
extern struct cfattach exdog_ca;
extern struct cfattach exgpio_ca;
extern struct cfattach exiic_ca;
extern struct cfattach exehci_ca;
extern struct cfattach exdwusb_ca;
extern struct cfattach crosec_ca;
extern struct cfattach tpspmic_ca;
extern struct cfattach sysreg_ca;
extern struct cfattach pciecam_ca;
extern struct cfattach bcmintc_ca;
extern struct cfattach mvacc_ca;
extern struct cfattach mvagc_ca;
extern struct cfattach mvsysctrl_ca;
extern struct cfattach mvmbus_ca;
extern struct cfattach mvxhci_ca;
extern struct cfattach mvahci_ca;
extern struct cfattach mvmpic_ca;
extern struct cfattach mvpcie_ca;
extern struct cfattach mvpxa_ca;
extern struct cfattach zqclock_ca;
extern struct cfattach zqreset_ca;


/* locators */
static long loc[12] = {
	-1, -1, -1, -1, -1, -1, -1, 0,
	0, -1, -1, 1,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"early",
	"port",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"phy",
	"addr",
	"size",
	"target",
	"lun",
	"console",
	"primary",
	"mux",
	"portno",
	"reportid",
	"bus",
	"dev",
	"function",
	"offset",
	"mask",
	"flag",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 1,
	2, 3, 4, 5, 6, -1, 1, 2,
	3, 4, 5, 6, -1, 7, -1, 7,
	-1, 7, -1, 7, -1, 7, -1, 7,
	-1, 7, -1, 7, -1, 7, -1, 7,
	-1, 7, -1, 7, -1, 7, -1, 7,
	-1, 7, -1, 7, -1, 7, -1, 7,
	-1, 8, 9, -1, 10, 11, -1, 12,
	13, 14, -1, 12, 13, 14, -1, 12,
	13, 14, -1, 12, 13, 14, -1, 12,
	13, 14, -1, 12, 14, -1, 12, 14,
	-1, 12, 14, -1, 14, -1, 14, -1,
	14, -1, 15, -1, 15, -1, 15, -1,
	15, -1, 15, -1, 15, -1, 15, -1,
	15, -1, 15, -1, 15, -1, 15, -1,
	15, -1, 15, -1, 15, -1, 15, -1,
	15, -1, 15, -1, 15, -1, 15, -1,
	16, -1, 17, -1, 17, -1, 17, -1,
	18, 19, -1, 20, 21, 22, -1,
};

/* size of parent vectors */
int pv_size = 167;

/* parent vectors */
short pv[167] = {
	89, 71, 93, 94, 90, 91, 92, 95, 99, 100, 96, 97, 98, 101, 102, 103,
	104, 58, 59, -1, 248, 228, 217, 203, 202, 189, 183, 141, 88, 87, 84, 83,
	82, 77, 76, 75, 74, 133, -1, 265, 256, 250, 240, 241, 225, 215, 201, 196,
	192, 188, 160, 9, 7, 6, -1, 208, 239, 184, 138, 185, 137, 255, 187, 139,
	176, 266, 136, -1, 5, 4, 267, 247, 222, 128, 125, 61, 15, 131, 130, -1,
	212, 231, 163, 254, 167, 194, 144, -1, 209, 227, 158, 186, 198, 174, 270, -1,
	221, 220, 243, 152, 47, -1, 260, 269, 132, -1, 56, 57, 121, -1, 230, 155,
	171, -1, 257, 55, 123, -1, 42, 43, -1, 40, 41, -1, 184, 138, -1, 45,
	46, -1, 44, 46, -1, 118, 145, -1, 182, 142, -1, 140, -1, 51, -1, 48,
	-1, 22, -1, 129, -1, 31, -1, 143, -1, 218, -1, 8, -1, 224, -1, 72,
	-1, 156, -1, 146, -1, 15, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: video* at uvideo*|utvfu* */
    {&video_ca,		&video_cd,	 0, STAR,     loc,    0, pv+127, 0,    0},
/*  1: audio* at uaudio*|utvfu* */
    {&audio_ca,		&audio_cd,	 0, STAR,     loc,    0, pv+130, 0,    0},
/*  2: midi* at umidi* */
    {&midi_ca,		&midi_cd,	 0, STAR,     loc,    0, pv+143, 0,    0},
/*  3: radio* at udsbr* */
    {&radio_ca,		&radio_cd,	 0, STAR,     loc,    0, pv+159, 0,    0},
/*  4: vscsi0 at root */
    {&vscsi_ca,		&vscsi_cd,	 0, NORM,     loc,    0, pv+19, 0,    0},
/*  5: softraid0 at root */
    {&softraid_ca,	&softraid_cd,	 0, NORM,     loc,    0, pv+19, 0,    0},
/*  6: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+19, 0,    0},
/*  7: simplebus* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&simplebus_ca,	&simplebus_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*  8: cortex0 at mainbus0 early 0 */
    {&cortex_ca,	&cortex_cd,	 0, NORM, loc+  8,    0, pv+53, 29,    0},
/*  9: ampintc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&ampintc_ca,	&ampintc_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/* 10: ampintcmsi* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&ampintcmsi_ca,	&ampintcmsi_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/* 11: amptimer* at cortex0 */
    {&amptimer_ca,	&amptimer_cd,	 0, STAR,     loc,    0, pv+155, 30,    0},
/* 12: agtimer* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&agtimer_ca,	&agtimer_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/* 13: armliicc* at cortex0 */
    {&armliicc_ca,	&armliicc_cd,	 0, STAR,     loc,    0, pv+155, 30,    0},
/* 14: cpu0 at mainbus0 */
    {&cpu_ca,		&cpu_cd,	 0, NORM,     loc,    0, pv+53, 29,    0},
/* 15: sdmmc* at imxesdhc*|ommmc*|sximmc*|sdhc*|dwmmc*|bcmsdhost*|mvpxa* */
    {&sdmmc_ca,		&sdmmc_cd,	 0, STAR,     loc,    0, pv+88, 30,    0},
/* 16: bwfm* at sdmmc* */
    {&bwfm_sdio_ca,	&bwfm_cd,	 0, STAR,     loc,    0, pv+165, 30,    0},
/* 17: bwfm* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&bwfm_usb_ca,	&bwfm_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 18: rlphy* at sxie*|cpsw*|fec*|dwxe*|mvneta*|cad*|dwge*|msk*|ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|re* phy -1 */
    {&rlphy_ca,		&rlphy_cd,	 0, STAR, loc+  5,    0, pv+20, 45,    0},
/* 19: ukphy* at sxie*|cpsw*|fec*|dwxe*|mvneta*|cad*|dwge*|msk*|ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|re* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+  5,    0, pv+20, 45,    0},
/* 20: rgephy* at sxie*|cpsw*|fec*|dwxe*|mvneta*|cad*|dwge*|msk*|ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|re* phy -1 */
    {&rgephy_ca,	&rgephy_cd,	 0, STAR, loc+  5,    0, pv+20, 45,    0},
/* 21: atphy* at sxie*|cpsw*|fec*|dwxe*|mvneta*|cad*|dwge*|msk*|ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|re* phy -1 */
    {&atphy_ca,		&atphy_cd,	 0, STAR, loc+  5,    0, pv+20, 45,    0},
/* 22: iic* at imxiic*|tiiic*|sxitwi*|exiic*|bcmbsc*|rkiic*|gpioiic* */
    {&iic_ca,		&iic_cd,	 0, STAR,     loc,    0, pv+80, 80,    0},
/* 23: pcagpio* at iic* addr -1 size -1 */
    {&pcagpio_ca,	&pcagpio_cd,	 0, STAR, loc+  4,    0, pv+145, 81,    0},
/* 24: maxrtc* at iic* addr -1 size -1 */
    {&maxrtc_ca,	&maxrtc_cd,	 0, STAR, loc+  4,    0, pv+145, 81,    0},
/* 25: dsxrtc* at iic* addr -1 size -1 */
    {&dsxrtc_ca,	&dsxrtc_cd,	 0, STAR, loc+  4,    0, pv+145, 81,    0},
/* 26: pcfrtc* at iic* addr -1 size -1 */
    {&pcfrtc_ca,	&pcfrtc_cd,	 0, STAR, loc+  4,    0, pv+145, 81,    0},
/* 27: pcxrtc* at iic* addr -1 size -1 */
    {&pcxrtc_ca,	&pcxrtc_cd,	 0, STAR, loc+  4,    0, pv+145, 81,    0},
/* 28: islrtc* at iic* addr -1 size -1 */
    {&islrtc_ca,	&islrtc_cd,	 0, STAR, loc+  4,    0, pv+145, 81,    0},
/* 29: abcrtc* at iic* addr -1 size -1 */
    {&abcrtc_ca,	&abcrtc_cd,	 0, STAR, loc+  4,    0, pv+145, 81,    0},
/* 30: mcprtc* at iic* addr -1 size -1 */
    {&mcprtc_ca,	&mcprtc_cd,	 0, STAR, loc+  4,    0, pv+145, 81,    0},
/* 31: scsibus* at softraid0|vscsi0|mvahci*|sxiahci*|imxahci*|vioscsi*|vioblk*|umass*|sdmmc*|nvme*|ahci* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+68, 83,    0},
/* 32: cd* at scsibus* target -1 lun -1 */
    {&cd_ca,		&cd_cd,		 0, STAR, loc+  4,    0, pv+149, 84,    0},
/* 33: ch* at scsibus* target -1 lun -1 */
    {&ch_ca,		&ch_cd,		 0, STAR, loc+  4,    0, pv+149, 84,    0},
/* 34: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+  4,    0, pv+149, 84,    0},
/* 35: st* at scsibus* target -1 lun -1 */
    {&st_ca,		&st_cd,		 0, STAR, loc+  4,    0, pv+149, 84,    0},
/* 36: uk* at scsibus* target -1 lun -1 */
    {&uk_ca,		&uk_cd,		 0, STAR, loc+  4,    0, pv+149, 84,    0},
/* 37: wsdisplay* at ssdfb*|ssdfb*|amdisplay*|simplefb*|udl* console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+  9,    0, pv+96, 87,    0},
/* 38: wskbd* at crosec*|ukbd*|ucc* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+ 10,    0, pv+114, 107,    0},
/* 39: wsmouse* at ums*|uts*|uwacom* mux 0 */
    {&wsmouse_ca,	&wsmouse_cd,	 0, STAR, loc+  8,    0, pv+106, 116,    0},
/* 40: usb* at imxehci*|omehci*|ehci*|ehci*|ohci*|ohci*|exehci*|xhci*|xhci*|dwctwo*|mvxhci*|uhci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+55, 121,    0},
/* 41: usb* at ehci*|ehci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,  0x1, pv+124, 121,    0},
/* 42: uhub* at usb*|usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+121, 121,    0},
/* 43: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 44: uaudio* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uaudio_ca,	&uaudio_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 45: uvideo* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvideo_ca,	&uvideo_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 46: utvfu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&utvfu_ca,		&utvfu_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 47: udl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udl_ca,		&udl_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 48: umidi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umidi_ca,		&umidi_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 49: ucom* at umodem*|uvisor*|uvscom*|ubsa*|uftdi*|uplcom*|umct*|uslcom*|uscom*|ucrcom*|uark*|moscom*|umcs*|uipaq*|umsm*|uchcom*|uticom*|ucycom*|uslhcom* portno -1 */
    {&ucom_ca,		&ucom_cd,	 0, STAR, loc+  5,    0, pv+ 0, 122,    0},
/* 50: ugen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugen_ca,		&ugen_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 51: uhidev* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhidev_ca,	&uhidev_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 52: uhid* at uhidev* reportid -1 */
    {&uhid_ca,		&uhid_cd,	 0, STAR, loc+  5,    0, pv+141, 160,    0},
/* 53: fido* at uhidev* reportid -1 */
    {&fido_ca,		&fido_cd,	 0, STAR, loc+  5,    0, pv+141, 160,    0},
/* 54: ujoy* at uhidev* reportid -1 */
    {&ujoy_ca,		&ujoy_cd,	 0, STAR, loc+  5,    0, pv+141, 160,    0},
/* 55: ukbd* at uhidev* reportid -1 */
    {&ukbd_ca,		&ukbd_cd,	 0, STAR, loc+  5,    0, pv+141, 160,    0},
/* 56: ums* at uhidev* reportid -1 */
    {&ums_ca,		&ums_cd,	 0, STAR, loc+  5,    0, pv+141, 160,    0},
/* 57: uts* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uts_ca,		&uts_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 58: ucycom* at uhidev* reportid -1 */
    {&ucycom_ca,	&ucycom_cd,	 0, STAR, loc+  5,    0, pv+141, 160,    0},
/* 59: uslhcom* at uhidev* reportid -1 */
    {&uslhcom_ca,	&uslhcom_cd,	 0, STAR, loc+  5,    0, pv+141, 160,    0},
/* 60: ulpt* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ulpt_ca,		&ulpt_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 61: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 62: uthum* at uhidev* reportid -1 */
    {&uthum_ca,		&uthum_cd,	 0, STAR, loc+  5,    0, pv+141, 160,    0},
/* 63: ugold* at uhidev* reportid -1 */
    {&ugold_ca,		&ugold_cd,	 0, STAR, loc+  5,    0, pv+141, 160,    0},
/* 64: utrh* at uhidev* reportid -1 */
    {&utrh_ca,		&utrh_cd,	 0, STAR, loc+  5,    0, pv+141, 160,    0},
/* 65: uoakrh* at uhidev* reportid -1 */
    {&uoakrh_ca,	&uoakrh_cd,	 0, STAR, loc+  5,    0, pv+141, 160,    0},
/* 66: uoaklux* at uhidev* reportid -1 */
    {&uoaklux_ca,	&uoaklux_cd,	 0, STAR, loc+  5,    0, pv+141, 160,    0},
/* 67: uoakv* at uhidev* reportid -1 */
    {&uoakv_ca,		&uoakv_cd,	 0, STAR, loc+  5,    0, pv+141, 160,    0},
/* 68: uonerng* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uonerng_ca,	&uonerng_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 69: urng* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urng_ca,		&urng_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 70: udcf* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udcf_ca,		&udcf_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 71: uvisor* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvisor_ca,	&uvisor_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 72: udsbr* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udsbr_ca,		&udsbr_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 73: utwitch* at uhidev* reportid -1 */
    {&utwitch_ca,	&utwitch_cd,	 0, STAR, loc+  5,    0, pv+141, 160,    0},
/* 74: aue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&aue_ca,		&aue_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 75: axe* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axe_ca,		&axe_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 76: axen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axen_ca,		&axen_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 77: smsc* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&smsc_ca,		&smsc_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 78: cue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cue_ca,		&cue_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 79: kue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&kue_ca,		&kue_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 80: cdce* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cdce_ca,		&cdce_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 81: urndis* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urndis_ca,	&urndis_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 82: mos* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mos_ca,		&mos_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 83: mue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mue_ca,		&mue_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 84: udav* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udav_ca,		&udav_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 85: upl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upl_ca,		&upl_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 86: ugl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugl_ca,		&ugl_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 87: url* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&url_ca,		&url_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 88: ure* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ure_ca,		&ure_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 89: umodem* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umodem_ca,	&umodem_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 90: uftdi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uftdi_ca,		&uftdi_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 91: uplcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uplcom_ca,	&uplcom_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 92: umct* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umct_ca,		&umct_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 93: uvscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvscom_ca,	&uvscom_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 94: ubsa* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ubsa_ca,		&ubsa_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 95: uslcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uslcom_ca,	&uslcom_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 96: uark* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uark_ca,		&uark_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 97: moscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&moscom_ca,	&moscom_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 98: umcs* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umcs_ca,		&umcs_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/* 99: uscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uscom_ca,		&uscom_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/*100: ucrcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ucrcom_ca,	&ucrcom_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/*101: uipaq* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uipaq_ca,		&uipaq_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/*102: umsm* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umsm_ca,		&umsm_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/*103: uchcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uchcom_ca,	&uchcom_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/*104: uticom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uticom_ca,	&uticom_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/*105: wi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&wi_usb_ca,	&wi_cd,		 0, STAR, loc+  0,    0, pv+118, 31,    0},
/*106: atu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&atu_ca,		&atu_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/*107: ural* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ural_ca,		&ural_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/*108: rum* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rum_ca,		&rum_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/*109: run* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&run_ca,		&run_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/*110: zyd* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&zyd_ca,		&zyd_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/*111: upgt* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upgt_ca,		&upgt_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/*112: urtw* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtw_ca,		&urtw_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/*113: urtwn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtwn_ca,		&urtwn_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/*114: rsu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rsu_ca,		&rsu_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/*115: otus* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&otus_ca,		&otus_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/*116: uath* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uath_ca,		&uath_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/*117: athn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&athn_usb_ca,	&athn_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/*118: uow* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uow_ca,		&uow_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/*119: uberry* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uberry_ca,	&uberry_cd,	 0, STAR, loc+  0,    0, pv+118, 31,    0},
/*120: upd* at uhidev* reportid -1 */
    {&upd_ca,		&upd_cd,	 0, STAR, loc+  5,    0, pv+141, 160,    0},
/*121: uwacom* at uhidev* reportid -1 */
    {&uwacom_ca,	&uwacom_cd,	 0, STAR, loc+  5,    0, pv+141, 160,    0},
/*122: uhidpp* at uhidev* reportid -1 */
    {&uhidpp_ca,	&uhidpp_cd,	 0, STAR, loc+  5,    0, pv+141, 160,    0},
/*123: ucc* at uhidev* reportid -1 */
    {&ucc_ca,		&ucc_cd,	 0, STAR, loc+  5,    0, pv+141, 160,    0},
/*124: vio* at virtio*|virtio* */
    {&vio_ca,		&vio_cd,	 0, STAR,     loc,    0, pv+136, 161,    0},
/*125: vioblk* at virtio*|virtio* */
    {&vioblk_ca,	&vioblk_cd,	 0, STAR,     loc,    0, pv+136, 161,    0},
/*126: viomb* at virtio*|virtio* */
    {&viomb_ca,		&viomb_cd,	 0, STAR,     loc,    0, pv+136, 161,    0},
/*127: viornd* at virtio*|virtio* */
    {&viornd_ca,	&viornd_cd,	 0, STAR,     loc,    0, pv+136, 161,    0},
/*128: vioscsi* at virtio*|virtio* */
    {&vioscsi_ca,	&vioscsi_cd,	 0, STAR,     loc,    0, pv+136, 161,    0},
/*129: pci* at pciecam*|mvpcie*|ppb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+  5,    0, pv+102, 162,    0},
/*130: ahci* at pci* dev -1 function -1 */
    {&ahci_pci_ca,	&ahci_cd,	 0, STAR, loc+  4,    0, pv+147, 168,    0},
/*131: nvme* at pci* dev -1 function -1 */
    {&nvme_pci_ca,	&nvme_cd,	 0, STAR, loc+  4,    0, pv+147, 168,    0},
/*132: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+  4,    0, pv+147, 168,    0},
/*133: re* at pci* dev -1 function -1 */
    {&re_pci_ca,	&re_cd,		 0, STAR, loc+  4,    0, pv+147, 168,    0},
/*134: em* at pci* dev -1 function -1 */
    {&em_ca,		&em_cd,		 0, STAR, loc+  4,    0, pv+147, 168,    0},
/*135: ix* at pci* dev -1 function -1 */
    {&ix_ca,		&ix_cd,		 0, STAR, loc+  4,    0, pv+147, 168,    0},
/*136: uhci* at pci* dev -1 function -1 */
    {&uhci_pci_ca,	&uhci_cd,	 0, STAR, loc+  4,    0, pv+147, 168,    0},
/*137: ohci* at pci* dev -1 function -1 */
    {&ohci_pci_ca,	&ohci_cd,	 0, STAR, loc+  4,    0, pv+147, 168,    0},
/*138: ehci* at pci* dev -1 function -1 */
    {&ehci_pci_ca,	&ehci_cd,	 0, STAR, loc+  4,    0, pv+147, 168,    0},
/*139: xhci* at pci* dev -1 function -1 */
    {&xhci_pci_ca,	&xhci_cd,	 0, STAR, loc+  4,    0, pv+147, 168,    0},
/*140: mskc* at pci* dev -1 function -1 */
    {&mskc_ca,		&mskc_cd,	 0, STAR, loc+  4,    0, pv+147, 168,    0},
/*141: msk* at mskc* */
    {&msk_ca,		&msk_cd,	 0, STAR,     loc,    0, pv+139, 170,    0},
/*142: virtio* at pci* dev -1 function -1 */
    {&virtio_pci_ca,	&virtio_cd,	 0, STAR, loc+  4,    0, pv+147, 168,    0},
/*143: gpio* at omgpio*|sxipio*|bcmgpio* */
    {&gpio_ca,		&gpio_cd,	 0, STAR,     loc,    0, pv+110, 170,    0},
/*144: gpioiic* at gpio* offset -1 mask 0 flag 0 */
    {&gpioiic_ca,	&gpioiic_cd,	 0, STAR, loc+  6,    0, pv+151, 171,    0},
/*145: gpioow* at gpio* offset -1 mask 0 flag 0 */
    {&gpioow_ca,	&gpioow_cd,	 0, STAR, loc+  6,    0, pv+151, 171,    0},
/*146: onewire* at uow*|gpioow* */
    {&onewire_ca,	&onewire_cd,	 0, STAR,     loc,    0, pv+133, 174,    0},
/*147: owid* at onewire* */
    {&owid_ca,		&owid_cd,	 0, STAR,     loc,    0, pv+163, 174,    0},
/*148: owsbm* at onewire* */
    {&owsbm_ca,		&owsbm_cd,	 0, STAR,     loc,    0, pv+163, 174,    0},
/*149: owtemp* at onewire* */
    {&owtemp_ca,	&owtemp_cd,	 0, STAR,     loc,    0, pv+163, 174,    0},
/*150: owctr* at onewire* */
    {&owctr_ca,		&owctr_cd,	 0, STAR,     loc,    0, pv+163, 174,    0},
/*151: pinctrl* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&pinctrl_ca,	&pinctrl_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*152: simplefb* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&simplefb_ca,	&simplefb_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*153: sxiccmu* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&sxiccmu_ca,	&sxiccmu_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*154: sxidog* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&sxidog_ca,	&sxidog_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*155: sxipio* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&sxipio_ca,	&sxipio_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*156: sxirsb* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&sxirsb_ca,	&sxirsb_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*157: sxirtc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&sxirtc_ca,	&sxirtc_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*158: sximmc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&sximmc_ca,	&sximmc_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*159: sxisid* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&sxisid_ca,	&sxisid_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*160: sxisyscon* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&sxisyscon_ca,	&sxisyscon_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*161: sxitemp* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&sxitemp_ca,	&sxitemp_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*162: sxits* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&sxits_ca,		&sxits_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*163: sxitwi* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&sxitwi_ca,	&sxitwi_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*164: axppmic* at iic* addr -1 size -1 */
    {&axppmic_ca,	&axppmic_cd,	 0, STAR, loc+  4,    0, pv+145, 81,    0},
/*165: axppmic* at sxirsb* */
    {&axppmic_rsb_ca,	&axppmic_cd,	 0, STAR,     loc,    0, pv+161, 174,    0},
/*166: bcmaux* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&bcmaux_ca,	&bcmaux_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*167: bcmbsc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&bcmbsc_ca,	&bcmbsc_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*168: bcmclock* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&bcmclock_ca,	&bcmclock_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*169: bcmdmac* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&bcmdmac_ca,	&bcmdmac_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*170: bcmdog* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&bcmdog_ca,	&bcmdog_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*171: bcmgpio* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&bcmgpio_ca,	&bcmgpio_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*172: bcmmbox* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&bcmmbox_ca,	&bcmmbox_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*173: bcmrng* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&bcmrng_ca,	&bcmrng_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*174: bcmsdhost* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&bcmsdhost_ca,	&bcmsdhost_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*175: bcmtemp* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&bcmtemp_ca,	&bcmtemp_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*176: dwctwo* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&bcmdwctwo_ca,	&dwctwo_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*177: exrtc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&exrtc_ca,		&exrtc_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*178: exuart* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&exuart_ca,	&exuart_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*179: plrtc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&plrtc_ca,		&plrtc_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*180: pluart* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&pluart_fdt_ca,	&pluart_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*181: psci* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&psci_ca,		&psci_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*182: virtio* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&virtio_mmio_ca,	&virtio_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*183: dwge* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&dwge_ca,		&dwge_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*184: ehci* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&ehci_fdt_ca,	&ehci_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*185: ohci* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&ohci_fdt_ca,	&ohci_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*186: sdhc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&sdhc_fdt_ca,	&sdhc_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*187: xhci* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&xhci_fdt_ca,	&xhci_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*188: syscon* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&syscon_ca,	&syscon_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*189: cad* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&cad_ca,		&cad_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*190: cduart* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&cduart_ca,	&cduart_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*191: rkclock* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&rkclock_ca,	&rkclock_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*192: rkgrf* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&rkgrf_ca,		&rkgrf_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*193: rkgpio* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&rkgpio_ca,	&rkgpio_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*194: rkiic* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&rkiic_ca,		&rkiic_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*195: rkpmic* at iic* addr -1 size -1 */
    {&rkpmic_ca,	&rkpmic_cd,	 0, STAR, loc+  4,    0, pv+145, 81,    0},
/*196: rkpinctrl* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&rkpinctrl_ca,	&rkpinctrl_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*197: rktemp* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&rktemp_ca,	&rktemp_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*198: dwmmc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&dwmmc_ca,		&dwmmc_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*199: dwdog* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&dwdog_ca,		&dwdog_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*200: mvpinctrl* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&mvpinctrl_ca,	&mvpinctrl_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*201: mvmdio* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&mvmdio_ca,	&mvmdio_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*202: mvneta* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&mvneta_ca,	&mvneta_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*203: dwxe* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&dwxe_ca,		&dwxe_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*204: acrtc* at sxirsb* */
    {&acrtc_ca,		&acrtc_cd,	 0, STAR,     loc,    0, pv+161, 174,    0},
/*205: imxanatop* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&imxanatop_ca,	&imxanatop_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*206: imxccm* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&imxccm_ca,	&imxccm_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*207: imxdog* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&imxdog_ca,	&imxdog_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*208: imxehci* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&imxehci_ca,	&imxehci_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*209: imxesdhc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&imxesdhc_ca,	&imxesdhc_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*210: imxgpc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&imxgpc_ca,	&imxgpc_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*211: imxgpio* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&imxgpio_ca,	&imxgpio_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*212: imxiic* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&imxiic_fdt_ca,	&imxiic_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*213: imxiomuxc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&imxiomuxc_ca,	&imxiomuxc_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*214: imxrtc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&imxrtc_ca,	&imxrtc_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*215: imxsrc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&imxsrc_ca,	&imxsrc_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*216: imxuart* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&imxuart_ca,	&imxuart_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*217: fec* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&fec_ca,		&fec_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*218: imxspi* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&imxspi_ca,	&imxspi_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*219: com* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&com_fdt_ca,	&com_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*220: ssdfb* at imxspi* */
    {&ssdfb_spi_ca,	&ssdfb_cd,	 0, STAR,     loc,    0, pv+153, 174,    0},
/*221: ssdfb* at iic* addr -1 size -1 */
    {&ssdfb_i2c_ca,	&ssdfb_cd,	 0, STAR, loc+  4,    0, pv+145, 81,    0},
/*222: imxahci* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&imxahci_ca,	&imxahci_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*223: imxtemp* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&imxtemp_ca,	&imxtemp_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*224: omap0 at mainbus0 early 0 */
    {&omap_ca,		&omap_cd,	 0, NORM, loc+  8,    0, pv+53, 29,    0},
/*225: omcm* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&omcm_ca,		&omcm_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*226: omclock* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&omclock_ca,	&omclock_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*227: ommmc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&ommmc_ca,		&ommmc_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*228: cpsw* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&cpsw_ca,		&cpsw_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*229: prcm* at omap0 */
    {&prcm_ca,		&prcm_cd,	 0, STAR,     loc,    0, pv+157, 174,    0},
/*230: omgpio* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&omgpio_ca,	&omgpio_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*231: tiiic* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&tiiic_ca,		&tiiic_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*232: intc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&intc_ca,		&intc_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*233: omwugen* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&omwugen_ca,	&omwugen_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*234: gptimer* at omap0 */
    {&gptimer_ca,	&gptimer_cd,	 0, STAR,     loc,    0, pv+157, 174,    0},
/*235: dmtimer* at omap0 */
    {&dmtimer_ca,	&dmtimer_cd,	 0, STAR,     loc,    0, pv+157, 174,    0},
/*236: omapid* at omap0 */
    {&omapid_ca,	&omapid_cd,	 0, STAR,     loc,    0, pv+157, 174,    0},
/*237: omdog* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&omdog_ca,		&omdog_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*238: omrng* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&omrng_ca,		&omrng_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*239: omehci* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&omehci_ca,	&omehci_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*240: omsysc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&omsysc_ca,	&omsysc_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*241: omsysc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&omsysc_ca,	&omsysc_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*242: omusbtll* at omap0 */
    {&omusbtll_ca,	&omusbtll_cd,	 0, STAR,     loc,    0, pv+157, 174,    0},
/*243: amdisplay* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&amdisplay_ca,	&amdisplay_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*244: nxphdmi* at iic* addr -1 size -1 */
    {&nxphdmi_ca,	&nxphdmi_cd,	 0, STAR, loc+  4,    0, pv+145, 81,    0},
/*245: sxiintc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&sxiintc_ca,	&sxiintc_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*246: sxitimer* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&sxitimer_ca,	&sxitimer_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*247: sxiahci* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&sxiahci_ca,	&sxiahci_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*248: sxie* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&sxie_ca,		&sxie_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*249: exclock* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&exclock_ca,	&exclock_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*250: expower* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&expower_ca,	&expower_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*251: exmct* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&exmct_ca,		&exmct_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*252: exdog* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&exdog_ca,		&exdog_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*253: exgpio* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&exgpio_ca,	&exgpio_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*254: exiic* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&exiic_ca,		&exiic_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*255: exehci* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&exehci_ca,	&exehci_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*256: exdwusb* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&exdwusb_ca,	&exdwusb_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*257: crosec* at iic* addr -1 size -1 */
    {&crosec_ca,	&crosec_cd,	 0, STAR, loc+  4,    0, pv+145, 81,    0},
/*258: tpspmic* at iic* addr -1 size -1 */
    {&tpspmic_ca,	&tpspmic_cd,	 0, STAR, loc+  4,    0, pv+145, 81,    0},
/*259: sysreg* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&sysreg_ca,	&sysreg_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*260: pciecam* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&pciecam_ca,	&pciecam_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*261: bcmintc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&bcmintc_ca,	&bcmintc_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*262: mvacc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&mvacc_ca,		&mvacc_cd,	 0, STAR, loc+ 11,    0, pv+39, 1,    0},
/*263: mvagc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&mvagc_ca,		&mvagc_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*264: mvsysctrl* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&mvsysctrl_ca,	&mvsysctrl_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*265: mvmbus* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&mvmbus_ca,	&mvmbus_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*266: mvxhci* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&mvxhci_ca,	&mvxhci_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*267: mvahci* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&mvahci_ca,	&mvahci_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*268: mvmpic* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&mvmpic_ca,	&mvmpic_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*269: mvpcie* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&mvpcie_ca,	&mvpcie_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*270: mvpxa* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&mvpxa_ca,		&mvpxa_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*271: zqclock* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&zqclock_ca,	&zqclock_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
/*272: zqreset* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|imxsrc*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&zqreset_ca,	&zqreset_cd,	 0, STAR, loc+  8,    0, pv+39, 1,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 4 /* vscsi0 */,
	 5 /* softraid0 */,
	 6 /* mainbus0 */,
	-1
};

int cfroots_size = 4;

/* pseudo-devices */
extern void pfattach(int);
extern void pflogattach(int);
extern void pfsyncattach(int);
extern void pflowattach(int);
extern void encattach(int);
extern void ptyattach(int);
extern void nmeaattach(int);
extern void mstsattach(int);
extern void endrunattach(int);
extern void vndattach(int);
extern void ksymsattach(int);
extern void bpfilterattach(int);
extern void bridgeattach(int);
extern void vebattach(int);
extern void carpattach(int);
extern void etheripattach(int);
extern void gifattach(int);
extern void greattach(int);
extern void loopattach(int);
extern void mpeattach(int);
extern void mpwattach(int);
extern void mpipattach(int);
extern void bpeattach(int);
extern void pairattach(int);
extern void pppattach(int);
extern void pppoeattach(int);
extern void pppxattach(int);
extern void spppattach(int);
extern void trunkattach(int);
extern void aggrattach(int);
extern void tpmrattach(int);
extern void tunattach(int);
extern void vetherattach(int);
extern void vxlanattach(int);
extern void vlanattach(int);
extern void switchattach(int);
extern void wgattach(int);
extern void bioattach(int);
extern void fuseattach(int);
extern void openpromattach(int);
extern void hotplugattach(int);
extern void wsmuxattach(int);

char *pdevnames[] = {
	"pf",
	"pflog",
	"pfsync",
	"pflow",
	"enc",
	"pty",
	"nmea",
	"msts",
	"endrun",
	"vnd",
	"ksyms",
	"bpfilter",
	"bridge",
	"veb",
	"carp",
	"etherip",
	"gif",
	"gre",
	"loop",
	"mpe",
	"mpw",
	"mpip",
	"bpe",
	"pair",
	"ppp",
	"pppoe",
	"pppx",
	"sppp",
	"trunk",
	"aggr",
	"tpmr",
	"tun",
	"vether",
	"vxlan",
	"vlan",
	"switch",
	"wg",
	"bio",
	"fuse",
	"openprom",
	"hotplug",
	"wsmux",
};

int pdevnames_size = 42;

struct pdevinit pdevinit[] = {
	{ pfattach, 1 },
	{ pflogattach, 1 },
	{ pfsyncattach, 1 },
	{ pflowattach, 1 },
	{ encattach, 1 },
	{ ptyattach, 16 },
	{ nmeaattach, 1 },
	{ mstsattach, 1 },
	{ endrunattach, 1 },
	{ vndattach, 4 },
	{ ksymsattach, 1 },
	{ bpfilterattach, 1 },
	{ bridgeattach, 1 },
	{ vebattach, 1 },
	{ carpattach, 1 },
	{ etheripattach, 1 },
	{ gifattach, 1 },
	{ greattach, 1 },
	{ loopattach, 1 },
	{ mpeattach, 1 },
	{ mpwattach, 1 },
	{ mpipattach, 1 },
	{ bpeattach, 1 },
	{ pairattach, 1 },
	{ pppattach, 1 },
	{ pppoeattach, 1 },
	{ pppxattach, 1 },
	{ spppattach, 1 },
	{ trunkattach, 1 },
	{ aggrattach, 1 },
	{ tpmrattach, 1 },
	{ tunattach, 1 },
	{ vetherattach, 1 },
	{ vxlanattach, 1 },
	{ vlanattach, 1 },
	{ switchattach, 1 },
	{ wgattach, 1 },
	{ bioattach, 1 },
	{ fuseattach, 1 },
	{ openpromattach, 1 },
	{ hotplugattach, 1 },
	{ wsmuxattach, 2 },
	{ NULL, 0 }
};
