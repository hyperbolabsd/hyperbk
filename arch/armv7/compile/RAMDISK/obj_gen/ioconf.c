/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/armv7/conf/RAMDISK"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver ahci_cd;
extern struct cfdriver nvme_cd;
extern struct cfdriver wi_cd;
extern struct cfdriver re_cd;
extern struct cfdriver com_cd;
extern struct cfdriver pluart_cd;
extern struct cfdriver athn_cd;
extern struct cfdriver bwfm_cd;
extern struct cfdriver virtio_cd;
extern struct cfdriver uhci_cd;
extern struct cfdriver ohci_cd;
extern struct cfdriver ehci_cd;
extern struct cfdriver xhci_cd;
extern struct cfdriver sdhc_cd;
extern struct cfdriver softraid_cd;
extern struct cfdriver imxiic_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver simplebus_cd;
extern struct cfdriver cortex_cd;
extern struct cfdriver ampintc_cd;
extern struct cfdriver ampintcmsi_cd;
extern struct cfdriver amptimer_cd;
extern struct cfdriver agtimer_cd;
extern struct cfdriver armliicc_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver cd_cd;
extern struct cfdriver sdmmc_cd;
extern struct cfdriver rlphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver rgephy_cd;
extern struct cfdriver atphy_cd;
extern struct cfdriver iic_cd;
extern struct cfdriver pcagpio_cd;
extern struct cfdriver pcfrtc_cd;
extern struct cfdriver pcxrtc_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver uhidev_cd;
extern struct cfdriver ukbd_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver aue_cd;
extern struct cfdriver axe_cd;
extern struct cfdriver axen_cd;
extern struct cfdriver smsc_cd;
extern struct cfdriver cue_cd;
extern struct cfdriver kue_cd;
extern struct cfdriver cdce_cd;
extern struct cfdriver mos_cd;
extern struct cfdriver mue_cd;
extern struct cfdriver udav_cd;
extern struct cfdriver upl_cd;
extern struct cfdriver ugl_cd;
extern struct cfdriver url_cd;
extern struct cfdriver ure_cd;
extern struct cfdriver atu_cd;
extern struct cfdriver ural_cd;
extern struct cfdriver rum_cd;
extern struct cfdriver run_cd;
extern struct cfdriver zyd_cd;
extern struct cfdriver upgt_cd;
extern struct cfdriver urtw_cd;
extern struct cfdriver urtwn_cd;
extern struct cfdriver rsu_cd;
extern struct cfdriver otus_cd;
extern struct cfdriver uath_cd;
extern struct cfdriver vio_cd;
extern struct cfdriver vioblk_cd;
extern struct cfdriver viomb_cd;
extern struct cfdriver viornd_cd;
extern struct cfdriver vioscsi_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver em_cd;
extern struct cfdriver ix_cd;
extern struct cfdriver mskc_cd;
extern struct cfdriver msk_cd;
extern struct cfdriver gpio_cd;
extern struct cfdriver pinctrl_cd;
extern struct cfdriver simplefb_cd;
extern struct cfdriver sxiccmu_cd;
extern struct cfdriver sxidog_cd;
extern struct cfdriver sxipio_cd;
extern struct cfdriver sxirsb_cd;
extern struct cfdriver sxirtc_cd;
extern struct cfdriver sximmc_cd;
extern struct cfdriver sxisid_cd;
extern struct cfdriver sxisyscon_cd;
extern struct cfdriver sxitwi_cd;
extern struct cfdriver axppmic_cd;
extern struct cfdriver bcmaux_cd;
extern struct cfdriver bcmbsc_cd;
extern struct cfdriver bcmclock_cd;
extern struct cfdriver bcmdmac_cd;
extern struct cfdriver bcmdog_cd;
extern struct cfdriver bcmgpio_cd;
extern struct cfdriver bcmmbox_cd;
extern struct cfdriver bcmrng_cd;
extern struct cfdriver bcmsdhost_cd;
extern struct cfdriver dwctwo_cd;
extern struct cfdriver exrtc_cd;
extern struct cfdriver exuart_cd;
extern struct cfdriver plrtc_cd;
extern struct cfdriver psci_cd;
extern struct cfdriver dwge_cd;
extern struct cfdriver syscon_cd;
extern struct cfdriver cad_cd;
extern struct cfdriver cduart_cd;
extern struct cfdriver rkclock_cd;
extern struct cfdriver rkgrf_cd;
extern struct cfdriver rkgpio_cd;
extern struct cfdriver rkiic_cd;
extern struct cfdriver rkpmic_cd;
extern struct cfdriver rkpinctrl_cd;
extern struct cfdriver dwmmc_cd;
extern struct cfdriver dwdog_cd;
extern struct cfdriver mvpinctrl_cd;
extern struct cfdriver mvmdio_cd;
extern struct cfdriver mvneta_cd;
extern struct cfdriver dwxe_cd;
extern struct cfdriver acrtc_cd;
extern struct cfdriver imxanatop_cd;
extern struct cfdriver imxccm_cd;
extern struct cfdriver imxdog_cd;
extern struct cfdriver imxehci_cd;
extern struct cfdriver imxesdhc_cd;
extern struct cfdriver imxgpc_cd;
extern struct cfdriver imxgpio_cd;
extern struct cfdriver imxiomuxc_cd;
extern struct cfdriver imxrtc_cd;
extern struct cfdriver imxuart_cd;
extern struct cfdriver fec_cd;
extern struct cfdriver imxahci_cd;
extern struct cfdriver omap_cd;
extern struct cfdriver omcm_cd;
extern struct cfdriver omclock_cd;
extern struct cfdriver ommmc_cd;
extern struct cfdriver cpsw_cd;
extern struct cfdriver prcm_cd;
extern struct cfdriver omgpio_cd;
extern struct cfdriver tiiic_cd;
extern struct cfdriver intc_cd;
extern struct cfdriver omwugen_cd;
extern struct cfdriver gptimer_cd;
extern struct cfdriver dmtimer_cd;
extern struct cfdriver omapid_cd;
extern struct cfdriver omdog_cd;
extern struct cfdriver omrng_cd;
extern struct cfdriver omehci_cd;
extern struct cfdriver omsysc_cd;
extern struct cfdriver omusbtll_cd;
extern struct cfdriver sxiintc_cd;
extern struct cfdriver sxitimer_cd;
extern struct cfdriver sxiahci_cd;
extern struct cfdriver sxie_cd;
extern struct cfdriver exclock_cd;
extern struct cfdriver expower_cd;
extern struct cfdriver exmct_cd;
extern struct cfdriver exdog_cd;
extern struct cfdriver exgpio_cd;
extern struct cfdriver exiic_cd;
extern struct cfdriver exehci_cd;
extern struct cfdriver exdwusb_cd;
extern struct cfdriver crosec_cd;
extern struct cfdriver tpspmic_cd;
extern struct cfdriver sysreg_cd;
extern struct cfdriver pciecam_cd;
extern struct cfdriver bcmintc_cd;
extern struct cfdriver mvacc_cd;
extern struct cfdriver mvagc_cd;
extern struct cfdriver mvsysctrl_cd;
extern struct cfdriver mvmbus_cd;
extern struct cfdriver mvxhci_cd;
extern struct cfdriver mvahci_cd;
extern struct cfdriver mvmpic_cd;
extern struct cfdriver mvpcie_cd;
extern struct cfdriver mvpxa_cd;
extern struct cfdriver zqclock_cd;
extern struct cfdriver zqreset_cd;

extern struct cfattach softraid_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach simplebus_ca;
extern struct cfattach cortex_ca;
extern struct cfattach ampintc_ca;
extern struct cfattach ampintcmsi_ca;
extern struct cfattach amptimer_ca;
extern struct cfattach agtimer_ca;
extern struct cfattach armliicc_ca;
extern struct cfattach cpu_ca;
extern struct cfattach sdmmc_ca;
extern struct cfattach bwfm_sdio_ca;
extern struct cfattach rlphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach rgephy_ca;
extern struct cfattach atphy_ca;
extern struct cfattach iic_ca;
extern struct cfattach pcagpio_ca;
extern struct cfattach pcfrtc_ca;
extern struct cfattach pcxrtc_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach cd_ca;
extern struct cfattach sd_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach uhidev_ca;
extern struct cfattach ukbd_ca;
extern struct cfattach umass_ca;
extern struct cfattach aue_ca;
extern struct cfattach axe_ca;
extern struct cfattach axen_ca;
extern struct cfattach smsc_ca;
extern struct cfattach cue_ca;
extern struct cfattach kue_ca;
extern struct cfattach cdce_ca;
extern struct cfattach mos_ca;
extern struct cfattach mue_ca;
extern struct cfattach udav_ca;
extern struct cfattach upl_ca;
extern struct cfattach ugl_ca;
extern struct cfattach url_ca;
extern struct cfattach ure_ca;
extern struct cfattach wi_usb_ca;
extern struct cfattach atu_ca;
extern struct cfattach ural_ca;
extern struct cfattach rum_ca;
extern struct cfattach run_ca;
extern struct cfattach zyd_ca;
extern struct cfattach upgt_ca;
extern struct cfattach urtw_ca;
extern struct cfattach urtwn_ca;
extern struct cfattach rsu_ca;
extern struct cfattach otus_ca;
extern struct cfattach uath_ca;
extern struct cfattach athn_usb_ca;
extern struct cfattach bwfm_usb_ca;
extern struct cfattach vio_ca;
extern struct cfattach vioblk_ca;
extern struct cfattach viomb_ca;
extern struct cfattach viornd_ca;
extern struct cfattach vioscsi_ca;
extern struct cfattach pci_ca;
extern struct cfattach ahci_pci_ca;
extern struct cfattach nvme_pci_ca;
extern struct cfattach ppb_ca;
extern struct cfattach re_pci_ca;
extern struct cfattach em_ca;
extern struct cfattach ix_ca;
extern struct cfattach uhci_pci_ca;
extern struct cfattach ohci_pci_ca;
extern struct cfattach ehci_pci_ca;
extern struct cfattach xhci_pci_ca;
extern struct cfattach mskc_ca;
extern struct cfattach msk_ca;
extern struct cfattach virtio_pci_ca;
extern struct cfattach gpio_ca;
extern struct cfattach pinctrl_ca;
extern struct cfattach simplefb_ca;
extern struct cfattach sxiccmu_ca;
extern struct cfattach sxidog_ca;
extern struct cfattach sxipio_ca;
extern struct cfattach sxirsb_ca;
extern struct cfattach sxirtc_ca;
extern struct cfattach sximmc_ca;
extern struct cfattach sxisid_ca;
extern struct cfattach sxisyscon_ca;
extern struct cfattach sxitwi_ca;
extern struct cfattach axppmic_ca;
extern struct cfattach axppmic_rsb_ca;
extern struct cfattach bcmaux_ca;
extern struct cfattach bcmbsc_ca;
extern struct cfattach bcmclock_ca;
extern struct cfattach bcmdmac_ca;
extern struct cfattach bcmdog_ca;
extern struct cfattach bcmgpio_ca;
extern struct cfattach bcmmbox_ca;
extern struct cfattach bcmrng_ca;
extern struct cfattach bcmsdhost_ca;
extern struct cfattach bcmdwctwo_ca;
extern struct cfattach exrtc_ca;
extern struct cfattach exuart_ca;
extern struct cfattach plrtc_ca;
extern struct cfattach pluart_fdt_ca;
extern struct cfattach psci_ca;
extern struct cfattach virtio_mmio_ca;
extern struct cfattach dwge_ca;
extern struct cfattach ehci_fdt_ca;
extern struct cfattach ohci_fdt_ca;
extern struct cfattach sdhc_fdt_ca;
extern struct cfattach xhci_fdt_ca;
extern struct cfattach syscon_ca;
extern struct cfattach cad_ca;
extern struct cfattach cduart_ca;
extern struct cfattach rkclock_ca;
extern struct cfattach rkgrf_ca;
extern struct cfattach rkgpio_ca;
extern struct cfattach rkiic_ca;
extern struct cfattach rkpmic_ca;
extern struct cfattach rkpinctrl_ca;
extern struct cfattach dwmmc_ca;
extern struct cfattach dwdog_ca;
extern struct cfattach mvpinctrl_ca;
extern struct cfattach mvmdio_ca;
extern struct cfattach mvneta_ca;
extern struct cfattach dwxe_ca;
extern struct cfattach acrtc_ca;
extern struct cfattach imxanatop_ca;
extern struct cfattach imxccm_ca;
extern struct cfattach imxdog_ca;
extern struct cfattach imxehci_ca;
extern struct cfattach imxesdhc_ca;
extern struct cfattach imxgpc_ca;
extern struct cfattach imxgpio_ca;
extern struct cfattach imxiic_fdt_ca;
extern struct cfattach imxiomuxc_ca;
extern struct cfattach imxrtc_ca;
extern struct cfattach imxuart_ca;
extern struct cfattach fec_ca;
extern struct cfattach com_fdt_ca;
extern struct cfattach imxahci_ca;
extern struct cfattach omap_ca;
extern struct cfattach omcm_ca;
extern struct cfattach omclock_ca;
extern struct cfattach ommmc_ca;
extern struct cfattach cpsw_ca;
extern struct cfattach prcm_ca;
extern struct cfattach omgpio_ca;
extern struct cfattach tiiic_ca;
extern struct cfattach intc_ca;
extern struct cfattach omwugen_ca;
extern struct cfattach gptimer_ca;
extern struct cfattach dmtimer_ca;
extern struct cfattach omapid_ca;
extern struct cfattach omdog_ca;
extern struct cfattach omrng_ca;
extern struct cfattach omehci_ca;
extern struct cfattach omsysc_ca;
extern struct cfattach omusbtll_ca;
extern struct cfattach sxiintc_ca;
extern struct cfattach sxitimer_ca;
extern struct cfattach sxiahci_ca;
extern struct cfattach sxie_ca;
extern struct cfattach exclock_ca;
extern struct cfattach expower_ca;
extern struct cfattach exmct_ca;
extern struct cfattach exdog_ca;
extern struct cfattach exgpio_ca;
extern struct cfattach exiic_ca;
extern struct cfattach exehci_ca;
extern struct cfattach exdwusb_ca;
extern struct cfattach crosec_ca;
extern struct cfattach tpspmic_ca;
extern struct cfattach sysreg_ca;
extern struct cfattach pciecam_ca;
extern struct cfattach bcmintc_ca;
extern struct cfattach mvacc_ca;
extern struct cfattach mvagc_ca;
extern struct cfattach mvsysctrl_ca;
extern struct cfattach mvmbus_ca;
extern struct cfattach mvxhci_ca;
extern struct cfattach mvahci_ca;
extern struct cfattach mvmpic_ca;
extern struct cfattach mvpcie_ca;
extern struct cfattach mvpxa_ca;
extern struct cfattach zqclock_ca;
extern struct cfattach zqreset_ca;


/* locators */
static long loc[10] = {
	-1, -1, -1, -1, -1, -1, -1, -1,
	1, 0,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"early",
	"port",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"phy",
	"addr",
	"size",
	"target",
	"lun",
	"console",
	"primary",
	"mux",
	"reportid",
	"bus",
	"dev",
	"function",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 1, 2, 3,
	4, 5, 6, -1, 1, 2, 3, 4,
	5, 6, -1, 7, -1, 7, -1, 7,
	-1, 7, -1, 7, -1, 7, -1, 7,
	-1, 7, -1, 7, -1, 7, -1, 7,
	-1, 7, -1, 7, -1, 7, -1, 7,
	-1, 7, -1, 7, -1, 7, -1, 8,
	9, -1, 10, 11, -1, 12, 13, 14,
	-1, 12, 14, -1, 12, 14, -1, 15,
	-1, 16, -1, 16, -1, 16, -1, 17,
	18, -1,
};

/* size of parent vectors */
int pv_size = 115;

/* parent vectors */
short pv[115] = {
	166, 148, 141, 128, 127, 115, 109, 77, 46, 45, 42, 41, 40, 36, 35, 34,
	33, 69, -1, 183, 174, 168, 160, 161, 145, 126, 122, 118, 114, 89, 4, 2,
	1, -1, 133, 159, 110, 74, 111, 73, 173, 113, 75, 102, 184, 72, -1, 185,
	165, 143, 64, 61, 32, 10, 0, 67, 66, -1, 134, 147, 87, 112, 123, 101,
	188, -1, 137, 151, 90, 172, 94, 120, -1, 178, 187, 68, -1, 108, 78, -1,
	28, 29, -1, 175, 31, -1, 26, 27, -1, 110, 74, -1, 150, 84, -1, 21,
	-1, 65, -1, 17, -1, 144, -1, 3, -1, 85, -1, 10, -1, 76, -1, 81,
	-1, 30, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: softraid0 at root */
    {&softraid_ca,	&softraid_cd,	 0, NORM,     loc,    0, pv+18, 0,    0},
/*  1: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+18, 0,    0},
/*  2: simplebus* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&simplebus_ca,	&simplebus_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*  3: cortex0 at mainbus0 early 0 */
    {&cortex_ca,	&cortex_cd,	 0, NORM, loc+  9,    0, pv+32, 27,    0},
/*  4: ampintc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&ampintc_ca,	&ampintc_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/*  5: ampintcmsi* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&ampintcmsi_ca,	&ampintcmsi_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/*  6: amptimer* at cortex0 */
    {&amptimer_ca,	&amptimer_cd,	 0, STAR,     loc,    0, pv+103, 28,    0},
/*  7: agtimer* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&agtimer_ca,	&agtimer_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*  8: armliicc* at cortex0 */
    {&armliicc_ca,	&armliicc_cd,	 0, STAR,     loc,    0, pv+103, 28,    0},
/*  9: cpu0 at mainbus0 */
    {&cpu_ca,		&cpu_cd,	 0, NORM,     loc,    0, pv+32, 27,    0},
/* 10: sdmmc* at imxesdhc*|ommmc*|sximmc*|sdhc*|dwmmc*|bcmsdhost*|mvpxa* */
    {&sdmmc_ca,		&sdmmc_cd,	 0, STAR,     loc,    0, pv+58, 28,    0},
/* 11: bwfm* at sdmmc* */
    {&bwfm_sdio_ca,	&bwfm_cd,	 0, STAR,     loc,    0, pv+107, 28,    0},
/* 12: bwfm* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&bwfm_usb_ca,	&bwfm_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 13: rlphy* at sxie*|cpsw*|fec*|dwxe*|mvneta*|cad*|dwge*|msk*|ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|re* phy -1 */
    {&rlphy_ca,		&rlphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 43,    0},
/* 14: ukphy* at sxie*|cpsw*|fec*|dwxe*|mvneta*|cad*|dwge*|msk*|ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|re* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 43,    0},
/* 15: rgephy* at sxie*|cpsw*|fec*|dwxe*|mvneta*|cad*|dwge*|msk*|ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|re* phy -1 */
    {&rgephy_ca,	&rgephy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 43,    0},
/* 16: atphy* at sxie*|cpsw*|fec*|dwxe*|mvneta*|cad*|dwge*|msk*|ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|re* phy -1 */
    {&atphy_ca,		&atphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 43,    0},
/* 17: iic* at imxiic*|tiiic*|sxitwi*|exiic*|bcmbsc*|rkiic* */
    {&iic_ca,		&iic_cd,	 0, STAR,     loc,    0, pv+66, 78,    0},
/* 18: pcagpio* at iic* addr -1 size -1 */
    {&pcagpio_ca,	&pcagpio_cd,	 0, STAR, loc+  4,    0, pv+99, 79,    0},
/* 19: pcfrtc* at iic* addr -1 size -1 */
    {&pcfrtc_ca,	&pcfrtc_cd,	 0, STAR, loc+  4,    0, pv+99, 79,    0},
/* 20: pcxrtc* at iic* addr -1 size -1 */
    {&pcxrtc_ca,	&pcxrtc_cd,	 0, STAR, loc+  4,    0, pv+99, 79,    0},
/* 21: scsibus* at mvahci*|sxiahci*|imxahci*|vioscsi*|vioblk*|umass*|sdmmc*|softraid0|nvme*|ahci* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+47, 81,    0},
/* 22: cd* at scsibus* target -1 lun -1 */
    {&cd_ca,		&cd_cd,		 0, STAR, loc+  4,    0, pv+95, 82,    0},
/* 23: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+  4,    0, pv+95, 82,    0},
/* 24: wsdisplay* at simplefb* console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+  6,    0, pv+111, 85,    0},
/* 25: wskbd* at crosec*|ukbd* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+  7,    0, pv+83, 89,    0},
/* 26: usb* at imxehci*|omehci*|ehci*|ehci*|ohci*|ohci*|exehci*|xhci*|xhci*|dwctwo*|mvxhci*|uhci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+34, 94,    0},
/* 27: usb* at ehci*|ehci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,  0x1, pv+89, 94,    0},
/* 28: uhub* at usb*|usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+86, 94,    0},
/* 29: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 30: uhidev* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhidev_ca,	&uhidev_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 31: ukbd* at uhidev* reportid -1 */
    {&ukbd_ca,		&ukbd_cd,	 0, STAR, loc+  5,    0, pv+113, 95,    0},
/* 32: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 33: aue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&aue_ca,		&aue_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 34: axe* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axe_ca,		&axe_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 35: axen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axen_ca,		&axen_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 36: smsc* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&smsc_ca,		&smsc_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 37: cue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cue_ca,		&cue_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 38: kue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&kue_ca,		&kue_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 39: cdce* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cdce_ca,		&cdce_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 40: mos* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mos_ca,		&mos_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 41: mue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mue_ca,		&mue_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 42: udav* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udav_ca,		&udav_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 43: upl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upl_ca,		&upl_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 44: ugl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugl_ca,		&ugl_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 45: url* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&url_ca,		&url_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 46: ure* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ure_ca,		&ure_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 47: wi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&wi_usb_ca,	&wi_cd,		 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 48: atu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&atu_ca,		&atu_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 49: ural* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ural_ca,		&ural_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 50: rum* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rum_ca,		&rum_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 51: run* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&run_ca,		&run_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 52: zyd* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&zyd_ca,		&zyd_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 53: upgt* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upgt_ca,		&upgt_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 54: urtw* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtw_ca,		&urtw_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 55: urtwn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtwn_ca,		&urtwn_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 56: rsu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rsu_ca,		&rsu_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 57: otus* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&otus_ca,		&otus_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 58: uath* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uath_ca,		&uath_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 59: athn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&athn_usb_ca,	&athn_cd,	 0, STAR, loc+  0,    0, pv+80, 29,    0},
/* 60: vio* at virtio*|virtio* */
    {&vio_ca,		&vio_cd,	 0, STAR,     loc,    0, pv+77, 96,    0},
/* 61: vioblk* at virtio*|virtio* */
    {&vioblk_ca,	&vioblk_cd,	 0, STAR,     loc,    0, pv+77, 96,    0},
/* 62: viomb* at virtio*|virtio* */
    {&viomb_ca,		&viomb_cd,	 0, STAR,     loc,    0, pv+77, 96,    0},
/* 63: viornd* at virtio*|virtio* */
    {&viornd_ca,	&viornd_cd,	 0, STAR,     loc,    0, pv+77, 96,    0},
/* 64: vioscsi* at virtio*|virtio* */
    {&vioscsi_ca,	&vioscsi_cd,	 0, STAR,     loc,    0, pv+77, 96,    0},
/* 65: pci* at pciecam*|mvpcie*|ppb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+  5,    0, pv+73, 97,    0},
/* 66: ahci* at pci* dev -1 function -1 */
    {&ahci_pci_ca,	&ahci_cd,	 0, STAR, loc+  4,    0, pv+97, 103,    0},
/* 67: nvme* at pci* dev -1 function -1 */
    {&nvme_pci_ca,	&nvme_cd,	 0, STAR, loc+  4,    0, pv+97, 103,    0},
/* 68: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+  4,    0, pv+97, 103,    0},
/* 69: re* at pci* dev -1 function -1 */
    {&re_pci_ca,	&re_cd,		 0, STAR, loc+  4,    0, pv+97, 103,    0},
/* 70: em* at pci* dev -1 function -1 */
    {&em_ca,		&em_cd,		 0, STAR, loc+  4,    0, pv+97, 103,    0},
/* 71: ix* at pci* dev -1 function -1 */
    {&ix_ca,		&ix_cd,		 0, STAR, loc+  4,    0, pv+97, 103,    0},
/* 72: uhci* at pci* dev -1 function -1 */
    {&uhci_pci_ca,	&uhci_cd,	 0, STAR, loc+  4,    0, pv+97, 103,    0},
/* 73: ohci* at pci* dev -1 function -1 */
    {&ohci_pci_ca,	&ohci_cd,	 0, STAR, loc+  4,    0, pv+97, 103,    0},
/* 74: ehci* at pci* dev -1 function -1 */
    {&ehci_pci_ca,	&ehci_cd,	 0, STAR, loc+  4,    0, pv+97, 103,    0},
/* 75: xhci* at pci* dev -1 function -1 */
    {&xhci_pci_ca,	&xhci_cd,	 0, STAR, loc+  4,    0, pv+97, 103,    0},
/* 76: mskc* at pci* dev -1 function -1 */
    {&mskc_ca,		&mskc_cd,	 0, STAR, loc+  4,    0, pv+97, 103,    0},
/* 77: msk* at mskc* */
    {&msk_ca,		&msk_cd,	 0, STAR,     loc,    0, pv+109, 105,    0},
/* 78: virtio* at pci* dev -1 function -1 */
    {&virtio_pci_ca,	&virtio_cd,	 0, STAR, loc+  4,    0, pv+97, 103,    0},
/* 79: gpio* at omgpio*|sxipio* */
    {&gpio_ca,		&gpio_cd,	 0, STAR,     loc,    0, pv+92, 105,    0},
/* 80: pinctrl* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&pinctrl_ca,	&pinctrl_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/* 81: simplefb* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&simplefb_ca,	&simplefb_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/* 82: sxiccmu* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&sxiccmu_ca,	&sxiccmu_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/* 83: sxidog* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&sxidog_ca,	&sxidog_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/* 84: sxipio* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&sxipio_ca,	&sxipio_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/* 85: sxirsb* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&sxirsb_ca,	&sxirsb_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/* 86: sxirtc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&sxirtc_ca,	&sxirtc_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/* 87: sximmc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&sximmc_ca,	&sximmc_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/* 88: sxisid* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&sxisid_ca,	&sxisid_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/* 89: sxisyscon* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&sxisyscon_ca,	&sxisyscon_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/* 90: sxitwi* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&sxitwi_ca,	&sxitwi_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/* 91: axppmic* at iic* addr -1 size -1 */
    {&axppmic_ca,	&axppmic_cd,	 0, STAR, loc+  4,    0, pv+99, 79,    0},
/* 92: axppmic* at sxirsb* */
    {&axppmic_rsb_ca,	&axppmic_cd,	 0, STAR,     loc,    0, pv+105, 105,    0},
/* 93: bcmaux* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&bcmaux_ca,	&bcmaux_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/* 94: bcmbsc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&bcmbsc_ca,	&bcmbsc_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/* 95: bcmclock* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&bcmclock_ca,	&bcmclock_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/* 96: bcmdmac* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&bcmdmac_ca,	&bcmdmac_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/* 97: bcmdog* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&bcmdog_ca,	&bcmdog_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/* 98: bcmgpio* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&bcmgpio_ca,	&bcmgpio_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/* 99: bcmmbox* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&bcmmbox_ca,	&bcmmbox_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/*100: bcmrng* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&bcmrng_ca,	&bcmrng_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*101: bcmsdhost* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&bcmsdhost_ca,	&bcmsdhost_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*102: dwctwo* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&bcmdwctwo_ca,	&dwctwo_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*103: exrtc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&exrtc_ca,		&exrtc_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*104: exuart* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&exuart_ca,	&exuart_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*105: plrtc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&plrtc_ca,		&plrtc_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*106: pluart* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&pluart_fdt_ca,	&pluart_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*107: psci* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&psci_ca,		&psci_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/*108: virtio* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&virtio_mmio_ca,	&virtio_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*109: dwge* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&dwge_ca,		&dwge_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*110: ehci* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&ehci_fdt_ca,	&ehci_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*111: ohci* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&ohci_fdt_ca,	&ohci_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*112: sdhc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&sdhc_fdt_ca,	&sdhc_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*113: xhci* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&xhci_fdt_ca,	&xhci_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*114: syscon* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&syscon_ca,	&syscon_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/*115: cad* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&cad_ca,		&cad_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*116: cduart* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&cduart_ca,	&cduart_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*117: rkclock* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&rkclock_ca,	&rkclock_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/*118: rkgrf* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&rkgrf_ca,		&rkgrf_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/*119: rkgpio* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&rkgpio_ca,	&rkgpio_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*120: rkiic* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&rkiic_ca,		&rkiic_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*121: rkpmic* at iic* addr -1 size -1 */
    {&rkpmic_ca,	&rkpmic_cd,	 0, STAR, loc+  4,    0, pv+99, 79,    0},
/*122: rkpinctrl* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&rkpinctrl_ca,	&rkpinctrl_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/*123: dwmmc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&dwmmc_ca,		&dwmmc_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*124: dwdog* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&dwdog_ca,		&dwdog_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*125: mvpinctrl* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&mvpinctrl_ca,	&mvpinctrl_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*126: mvmdio* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&mvmdio_ca,	&mvmdio_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*127: mvneta* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&mvneta_ca,	&mvneta_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*128: dwxe* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&dwxe_ca,		&dwxe_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*129: acrtc* at sxirsb* */
    {&acrtc_ca,		&acrtc_cd,	 0, STAR,     loc,    0, pv+105, 105,    0},
/*130: imxanatop* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&imxanatop_ca,	&imxanatop_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/*131: imxccm* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&imxccm_ca,	&imxccm_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/*132: imxdog* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&imxdog_ca,	&imxdog_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*133: imxehci* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&imxehci_ca,	&imxehci_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*134: imxesdhc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&imxesdhc_ca,	&imxesdhc_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*135: imxgpc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&imxgpc_ca,	&imxgpc_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*136: imxgpio* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&imxgpio_ca,	&imxgpio_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*137: imxiic* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&imxiic_fdt_ca,	&imxiic_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*138: imxiomuxc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&imxiomuxc_ca,	&imxiomuxc_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/*139: imxrtc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&imxrtc_ca,	&imxrtc_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*140: imxuart* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&imxuart_ca,	&imxuart_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*141: fec* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&fec_ca,		&fec_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*142: com* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&com_fdt_ca,	&com_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*143: imxahci* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&imxahci_ca,	&imxahci_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*144: omap0 at mainbus0 early 0 */
    {&omap_ca,		&omap_cd,	 0, NORM, loc+  9,    0, pv+32, 27,    0},
/*145: omcm* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&omcm_ca,		&omcm_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/*146: omclock* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&omclock_ca,	&omclock_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/*147: ommmc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&ommmc_ca,		&ommmc_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*148: cpsw* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&cpsw_ca,		&cpsw_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*149: prcm* at omap0 */
    {&prcm_ca,		&prcm_cd,	 0, STAR,     loc,    0, pv+101, 105,    0},
/*150: omgpio* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&omgpio_ca,	&omgpio_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*151: tiiic* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&tiiic_ca,		&tiiic_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*152: intc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&intc_ca,		&intc_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/*153: omwugen* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&omwugen_ca,	&omwugen_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*154: gptimer* at omap0 */
    {&gptimer_ca,	&gptimer_cd,	 0, STAR,     loc,    0, pv+101, 105,    0},
/*155: dmtimer* at omap0 */
    {&dmtimer_ca,	&dmtimer_cd,	 0, STAR,     loc,    0, pv+101, 105,    0},
/*156: omapid* at omap0 */
    {&omapid_ca,	&omapid_cd,	 0, STAR,     loc,    0, pv+101, 105,    0},
/*157: omdog* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&omdog_ca,		&omdog_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*158: omrng* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&omrng_ca,		&omrng_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*159: omehci* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&omehci_ca,	&omehci_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*160: omsysc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&omsysc_ca,	&omsysc_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/*161: omsysc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&omsysc_ca,	&omsysc_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*162: omusbtll* at omap0 */
    {&omusbtll_ca,	&omusbtll_cd,	 0, STAR,     loc,    0, pv+101, 105,    0},
/*163: sxiintc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&sxiintc_ca,	&sxiintc_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*164: sxitimer* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&sxitimer_ca,	&sxitimer_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/*165: sxiahci* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&sxiahci_ca,	&sxiahci_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*166: sxie* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&sxie_ca,		&sxie_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*167: exclock* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&exclock_ca,	&exclock_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/*168: expower* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&expower_ca,	&expower_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/*169: exmct* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&exmct_ca,		&exmct_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/*170: exdog* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&exdog_ca,		&exdog_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*171: exgpio* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&exgpio_ca,	&exgpio_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/*172: exiic* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&exiic_ca,		&exiic_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*173: exehci* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&exehci_ca,	&exehci_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*174: exdwusb* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&exdwusb_ca,	&exdwusb_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*175: crosec* at iic* addr -1 size -1 */
    {&crosec_ca,	&crosec_cd,	 0, STAR, loc+  4,    0, pv+99, 79,    0},
/*176: tpspmic* at iic* addr -1 size -1 */
    {&tpspmic_ca,	&tpspmic_cd,	 0, STAR, loc+  4,    0, pv+99, 79,    0},
/*177: sysreg* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&sysreg_ca,	&sysreg_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*178: pciecam* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&pciecam_ca,	&pciecam_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*179: bcmintc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&bcmintc_ca,	&bcmintc_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/*180: mvacc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 1 */
    {&mvacc_ca,		&mvacc_cd,	 0, STAR, loc+  8,    0, pv+19, 1,    0},
/*181: mvagc* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&mvagc_ca,		&mvagc_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*182: mvsysctrl* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&mvsysctrl_ca,	&mvsysctrl_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*183: mvmbus* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&mvmbus_ca,	&mvmbus_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*184: mvxhci* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&mvxhci_ca,	&mvxhci_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*185: mvahci* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&mvahci_ca,	&mvahci_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*186: mvmpic* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&mvmpic_ca,	&mvmpic_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*187: mvpcie* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&mvpcie_ca,	&mvpcie_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*188: mvpxa* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&mvpxa_ca,		&mvpxa_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*189: zqclock* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&zqclock_ca,	&zqclock_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
/*190: zqreset* at mvmbus*|exdwusb*|expower*|omsysc*|omsysc*|omcm*|mvmdio*|rkpinctrl*|rkgrf*|syscon*|sxisyscon*|ampintc*|simplebus*|mainbus0 early 0 */
    {&zqreset_ca,	&zqreset_cd,	 0, STAR, loc+  9,    0, pv+19, 1,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 0 /* softraid0 */,
	 1 /* mainbus0 */,
	-1
};

int cfroots_size = 3;

/* pseudo-devices */
extern void openpromattach(int);
extern void loopattach(int);
extern void bpfilterattach(int);
extern void rdattach(int);
extern void bioattach(int);

char *pdevnames[] = {
	"openprom",
	"loop",
	"bpfilter",
	"rd",
	"bio",
};

int pdevnames_size = 5;

struct pdevinit pdevinit[] = {
	{ openpromattach, 1 },
	{ loopattach, 1 },
	{ bpfilterattach, 1 },
	{ rdattach, 1 },
	{ bioattach, 1 },
	{ NULL, 0 }
};
