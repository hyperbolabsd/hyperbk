#include <sys/param.h>
#include <sys/conf.h>
#include <sys/systm.h>

dev_t	rootdev = makedev(18, 0);	/* rd0a */
dev_t	dumpdev = makedev(18, 1);	/* rd0b */

struct	swdevt swdevt[] = {
	{ makedev(18, 1),	0 },	/* rd0b */
	{ NODEV, 0 }
};

int (*mountroot)(void) = dk_mountroot;
