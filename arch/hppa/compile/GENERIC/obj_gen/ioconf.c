/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/hppa/conf/GENERIC"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver audio_cd;
extern struct cfdriver midi_cd;
extern struct cfdriver wdc_cd;
extern struct cfdriver ahc_cd;
extern struct cfdriver qlw_cd;
extern struct cfdriver qla_cd;
extern struct cfdriver mpi_cd;
extern struct cfdriver siop_cd;
extern struct cfdriver osiop_cd;
extern struct cfdriver oosiop_cd;
extern struct cfdriver wi_cd;
extern struct cfdriver an_cd;
extern struct cfdriver xl_cd;
extern struct cfdriver fxp_cd;
extern struct cfdriver rl_cd;
extern struct cfdriver dc_cd;
extern struct cfdriver ne_cd;
extern struct cfdriver ie_cd;
extern struct cfdriver ti_cd;
extern struct cfdriver com_cd;
extern struct cfdriver lpt_cd;
extern struct cfdriver sti_cd;
extern struct cfdriver uhci_cd;
extern struct cfdriver ohci_cd;
extern struct cfdriver ehci_cd;
extern struct cfdriver arcofi_cd;
extern struct cfdriver radio_cd;
extern struct cfdriver vscsi_cd;
extern struct cfdriver mpath_cd;
extern struct cfdriver softraid_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver wsmouse_cd;
extern struct cfdriver nsphy_cd;
extern struct cfdriver nsphyter_cd;
extern struct cfdriver gentbi_cd;
extern struct cfdriver qsphy_cd;
extern struct cfdriver inphy_cd;
extern struct cfdriver iophy_cd;
extern struct cfdriver eephy_cd;
extern struct cfdriver exphy_cd;
extern struct cfdriver rlphy_cd;
extern struct cfdriver lxtphy_cd;
extern struct cfdriver luphy_cd;
extern struct cfdriver mtdphy_cd;
extern struct cfdriver icsphy_cd;
extern struct cfdriver sqphy_cd;
extern struct cfdriver tqphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver dcphy_cd;
extern struct cfdriver bmtphy_cd;
extern struct cfdriver brgphy_cd;
extern struct cfdriver xmphy_cd;
extern struct cfdriver amphy_cd;
extern struct cfdriver acphy_cd;
extern struct cfdriver urlphy_cd;
extern struct cfdriver rgephy_cd;
extern struct cfdriver ipgphy_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver cd_cd;
extern struct cfdriver ch_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver st_cd;
extern struct cfdriver uk_cd;
extern struct cfdriver safte_cd;
extern struct cfdriver ses_cd;
extern struct cfdriver sym_cd;
extern struct cfdriver rdac_cd;
extern struct cfdriver emc_cd;
extern struct cfdriver hds_cd;
extern struct cfdriver atapiscsi_cd;
extern struct cfdriver wd_cd;
extern struct cfdriver eisa_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver qle_cd;
extern struct cfdriver pciide_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver em_cd;
extern struct cfdriver cbb_cd;
extern struct cfdriver skc_cd;
extern struct cfdriver sk_cd;
extern struct cfdriver mskc_cd;
extern struct cfdriver msk_cd;
extern struct cfdriver bge_cd;
extern struct cfdriver stge_cd;
extern struct cfdriver hme_cd;
extern struct cfdriver cardslot_cd;
extern struct cfdriver cardbus_cd;
extern struct cfdriver pcmcia_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver uaudio_cd;
extern struct cfdriver udl_cd;
extern struct cfdriver umidi_cd;
extern struct cfdriver ucom_cd;
extern struct cfdriver ugen_cd;
extern struct cfdriver uhidev_cd;
extern struct cfdriver uhid_cd;
extern struct cfdriver fido_cd;
extern struct cfdriver ujoy_cd;
extern struct cfdriver ukbd_cd;
extern struct cfdriver ums_cd;
extern struct cfdriver ucycom_cd;
extern struct cfdriver uslhcom_cd;
extern struct cfdriver ulpt_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver uthum_cd;
extern struct cfdriver ugold_cd;
extern struct cfdriver uonerng_cd;
extern struct cfdriver urng_cd;
extern struct cfdriver uvisor_cd;
extern struct cfdriver udsbr_cd;
extern struct cfdriver utwitch_cd;
extern struct cfdriver aue_cd;
extern struct cfdriver axe_cd;
extern struct cfdriver axen_cd;
extern struct cfdriver smsc_cd;
extern struct cfdriver cue_cd;
extern struct cfdriver kue_cd;
extern struct cfdriver cdce_cd;
extern struct cfdriver urndis_cd;
extern struct cfdriver udav_cd;
extern struct cfdriver upl_cd;
extern struct cfdriver ugl_cd;
extern struct cfdriver url_cd;
extern struct cfdriver ure_cd;
extern struct cfdriver umodem_cd;
extern struct cfdriver uftdi_cd;
extern struct cfdriver uplcom_cd;
extern struct cfdriver umct_cd;
extern struct cfdriver uvscom_cd;
extern struct cfdriver ubsa_cd;
extern struct cfdriver uslcom_cd;
extern struct cfdriver uark_cd;
extern struct cfdriver uscom_cd;
extern struct cfdriver ucrcom_cd;
extern struct cfdriver uipaq_cd;
extern struct cfdriver umsm_cd;
extern struct cfdriver uchcom_cd;
extern struct cfdriver atu_cd;
extern struct cfdriver ural_cd;
extern struct cfdriver rum_cd;
extern struct cfdriver run_cd;
extern struct cfdriver zyd_cd;
extern struct cfdriver rsu_cd;
extern struct cfdriver uow_cd;
extern struct cfdriver upd_cd;
extern struct cfdriver uhidpp_cd;
extern struct cfdriver ucc_cd;
extern struct cfdriver hil_cd;
extern struct cfdriver hilkbd_cd;
extern struct cfdriver hilms_cd;
extern struct cfdriver hilid_cd;
extern struct cfdriver onewire_cd;
extern struct cfdriver owid_cd;
extern struct cfdriver owsbm_cd;
extern struct cfdriver owtemp_cd;
extern struct cfdriver owctr_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver pdc_cd;
extern struct cfdriver power_cd;
extern struct cfdriver lcd_cd;
extern struct cfdriver mem_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver phantomas_cd;
extern struct cfdriver uturn_cd;
extern struct cfdriver astro_cd;
extern struct cfdriver lasi_cd;
extern struct cfdriver asp_cd;
extern struct cfdriver wax_cd;
extern struct cfdriver mongoose_cd;
extern struct cfdriver dino_cd;
extern struct cfdriver elroy_cd;
extern struct cfdriver ssio_cd;
extern struct cfdriver gsc_cd;
extern struct cfdriver harmony_cd;
extern struct cfdriver gsckbc_cd;
extern struct cfdriver pckbd_cd;
extern struct cfdriver pms_cd;

extern struct cfattach audio_ca;
extern struct cfattach midi_ca;
extern struct cfattach radio_ca;
extern struct cfattach vscsi_ca;
extern struct cfattach mpath_ca;
extern struct cfattach softraid_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach wsmouse_ca;
extern struct cfattach nsphy_ca;
extern struct cfattach nsphyter_ca;
extern struct cfattach gentbi_ca;
extern struct cfattach qsphy_ca;
extern struct cfattach inphy_ca;
extern struct cfattach iophy_ca;
extern struct cfattach eephy_ca;
extern struct cfattach exphy_ca;
extern struct cfattach rlphy_ca;
extern struct cfattach lxtphy_ca;
extern struct cfattach luphy_ca;
extern struct cfattach mtdphy_ca;
extern struct cfattach icsphy_ca;
extern struct cfattach sqphy_ca;
extern struct cfattach tqphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach dcphy_ca;
extern struct cfattach bmtphy_ca;
extern struct cfattach brgphy_ca;
extern struct cfattach xmphy_ca;
extern struct cfattach amphy_ca;
extern struct cfattach acphy_ca;
extern struct cfattach urlphy_ca;
extern struct cfattach rgephy_ca;
extern struct cfattach ipgphy_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach cd_ca;
extern struct cfattach ch_ca;
extern struct cfattach sd_ca;
extern struct cfattach st_ca;
extern struct cfattach uk_ca;
extern struct cfattach safte_ca;
extern struct cfattach ses_ca;
extern struct cfattach sym_ca;
extern struct cfattach rdac_ca;
extern struct cfattach emc_ca;
extern struct cfattach hds_ca;
extern struct cfattach atapiscsi_ca;
extern struct cfattach wd_ca;
extern struct cfattach eisa_ca;
extern struct cfattach pci_ca;
extern struct cfattach sti_pci_ca;
extern struct cfattach ahc_pci_ca;
extern struct cfattach qlw_pci_ca;
extern struct cfattach qla_pci_ca;
extern struct cfattach qle_ca;
extern struct cfattach mpi_pci_ca;
extern struct cfattach siop_pci_ca;
extern struct cfattach pciide_pci_ca;
extern struct cfattach ppb_ca;
extern struct cfattach rl_pci_ca;
extern struct cfattach xl_pci_ca;
extern struct cfattach fxp_pci_ca;
extern struct cfattach em_ca;
extern struct cfattach dc_pci_ca;
extern struct cfattach ti_pci_ca;
extern struct cfattach ne_pci_ca;
extern struct cfattach uhci_pci_ca;
extern struct cfattach ohci_pci_ca;
extern struct cfattach ehci_pci_ca;
extern struct cfattach cbb_pci_ca;
extern struct cfattach skc_ca;
extern struct cfattach sk_ca;
extern struct cfattach mskc_ca;
extern struct cfattach msk_ca;
extern struct cfattach bge_ca;
extern struct cfattach stge_ca;
extern struct cfattach hme_pci_ca;
extern struct cfattach cardslot_ca;
extern struct cfattach cardbus_ca;
extern struct cfattach ehci_cardbus_ca;
extern struct cfattach ohci_cardbus_ca;
extern struct cfattach pcmcia_ca;
extern struct cfattach ne_pcmcia_ca;
extern struct cfattach wdc_pcmcia_ca;
extern struct cfattach wi_pcmcia_ca;
extern struct cfattach an_pcmcia_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach uaudio_ca;
extern struct cfattach udl_ca;
extern struct cfattach umidi_ca;
extern struct cfattach ucom_ca;
extern struct cfattach ugen_ca;
extern struct cfattach uhidev_ca;
extern struct cfattach uhid_ca;
extern struct cfattach fido_ca;
extern struct cfattach ujoy_ca;
extern struct cfattach ukbd_ca;
extern struct cfattach ums_ca;
extern struct cfattach ucycom_ca;
extern struct cfattach uslhcom_ca;
extern struct cfattach ulpt_ca;
extern struct cfattach umass_ca;
extern struct cfattach uthum_ca;
extern struct cfattach ugold_ca;
extern struct cfattach uonerng_ca;
extern struct cfattach urng_ca;
extern struct cfattach uvisor_ca;
extern struct cfattach udsbr_ca;
extern struct cfattach utwitch_ca;
extern struct cfattach aue_ca;
extern struct cfattach axe_ca;
extern struct cfattach axen_ca;
extern struct cfattach smsc_ca;
extern struct cfattach cue_ca;
extern struct cfattach kue_ca;
extern struct cfattach cdce_ca;
extern struct cfattach urndis_ca;
extern struct cfattach udav_ca;
extern struct cfattach upl_ca;
extern struct cfattach ugl_ca;
extern struct cfattach url_ca;
extern struct cfattach ure_ca;
extern struct cfattach umodem_ca;
extern struct cfattach uftdi_ca;
extern struct cfattach uplcom_ca;
extern struct cfattach umct_ca;
extern struct cfattach uvscom_ca;
extern struct cfattach ubsa_ca;
extern struct cfattach uslcom_ca;
extern struct cfattach uark_ca;
extern struct cfattach uscom_ca;
extern struct cfattach ucrcom_ca;
extern struct cfattach uipaq_ca;
extern struct cfattach umsm_ca;
extern struct cfattach uchcom_ca;
extern struct cfattach wi_usb_ca;
extern struct cfattach atu_ca;
extern struct cfattach ural_ca;
extern struct cfattach rum_ca;
extern struct cfattach run_ca;
extern struct cfattach zyd_ca;
extern struct cfattach rsu_ca;
extern struct cfattach uow_ca;
extern struct cfattach upd_ca;
extern struct cfattach uhidpp_ca;
extern struct cfattach ucc_ca;
extern struct cfattach hilkbd_ca;
extern struct cfattach hilms_ca;
extern struct cfattach hilid_ca;
extern struct cfattach onewire_ca;
extern struct cfattach owid_ca;
extern struct cfattach owsbm_ca;
extern struct cfattach owtemp_ca;
extern struct cfattach owctr_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach pdc_ca;
extern struct cfattach power_ca;
extern struct cfattach lcd_ca;
extern struct cfattach mem_ca;
extern struct cfattach cpu_ca;
extern struct cfattach phantomas_ca;
extern struct cfattach uturn_ca;
extern struct cfattach astro_ca;
extern struct cfattach lasi_ca;
extern struct cfattach asp_ca;
extern struct cfattach wax_ca;
extern struct cfattach mg_gedoens_ca;
extern struct cfattach dino_ca;
extern struct cfattach com_dino_ca;
extern struct cfattach elroy_ca;
extern struct cfattach sti_gedoens_ca;
extern struct cfattach siop_gedoens_ca;
extern struct cfattach ssio_ca;
extern struct cfattach com_ssio_ca;
extern struct cfattach lpt_ssio_ca;
extern struct cfattach gsc_ca;
extern struct cfattach com_gsc_ca;
extern struct cfattach lpt_gsc_ca;
extern struct cfattach ie_gsc_ca;
extern struct cfattach osiop_gsc_ca;
extern struct cfattach oosiop_gsc_ca;
extern struct cfattach hil_gsc_ca;
extern struct cfattach arcofi_gsc_ca;
extern struct cfattach harmony_ca;
extern struct cfattach gsckbc_ca;
extern struct cfattach pckbd_ca;
extern struct cfattach pms_ca;


/* locators */
static long loc[49] = {
	-1, -1, -1, -1, -1, -1, -1, -1,
	1, 0x4060, 0xd, 0x4040, 0xd, -1, 0xd, 0,
	0xd, -1, 7, -1, 9, -1, 0x1a, 0x823000,
	5, 0x5000, 5, -1, 3, -1, 0x1f, 0x100000,
	-1, 0xfd00000, -1, 0x500000, -1, -1, 0x11, -1,
	0xb, -1, 8, 0x2000, 6, 0x822000, 6, 0,
	4,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"console",
	"primary",
	"mux",
	"phy",
	"target",
	"lun",
	"channel",
	"drive",
	"bus",
	"dev",
	"function",
	"irq",
	"slot",
	"port",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"portno",
	"reportid",
	"code",
	"offset",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, 1, 2, -1, 0, 1, 2,
	-1, 0, 1, 2, -1, 0, 2, -1,
	0, 2, -1, 0, 2, -1, 0, 2,
	-1, 2, -1, 2, -1, 2, -1, 3,
	-1, 3, -1, 3, -1, 3, -1, 3,
	-1, 3, -1, 3, -1, 3, -1, 3,
	-1, 3, -1, 3, -1, 3, -1, 3,
	-1, 3, -1, 3, -1, 3, -1, 3,
	-1, 3, -1, 3, -1, 4, 5, -1,
	6, -1, 6, 7, -1, 8, -1, 8,
	-1, 8, -1, 9, 10, -1, 10, 11,
	-1, 9, 10, -1, 12, -1, 12, -1,
	13, 14, 15, 16, 17, 18, -1, 13,
	14, 15, 16, 17, 18, -1, 19, -1,
	19, -1, 19, -1, 19, -1, 19, -1,
	19, -1, 19, -1, 19, -1, 19, -1,
	19, -1, 19, -1, 19, -1, 19, -1,
	19, -1, 19, -1, 19, -1, 20, -1,
	21, -1, 22, 11, -1, 22, 11, -1,
	22, 11, -1, 22, 11, -1, 11, -1,
	22, 11, -1, 22, 11, -1, 22, 11,
	-1, 22, 11, -1, 12, -1,
};

/* size of parent vectors */
int pv_size = 148;

/* parent vectors */
short pv[148] = {
	123, 122, 119, 114, 113, 112, 111, 79, 78, 77, 76, 74, 64, 65, 66, 63,
	59, 61, 60, -1, 125, 126, 131, 124, 108, 128, 129, 127, 130, 132, 133, 134,
	135, 100, 101, 136, -1, 5, 3, 103, 54, 46, 4, 195, 194, 180, 56, 55,
	53, 52, 51, -1, 70, 71, 67, 68, 69, -1, 156, 162, 163, 164, -1, 98,
	147, 200, 148, -1, 187, 188, 189, -1, 169, 166, 167, -1, 89, 197, 198, -1,
	90, 179, 50, -1, 99, 201, 149, -1, 172, 178, 58, -1, 87, 88, -1, 83,
	57, -1, 151, -1, 91, -1, 144, -1, 86, -1, 168, -1, 170, -1, 75, -1,
	73, -1, 171, -1, 49, -1, 82, -1, 81, -1, 187, -1, 80, -1, 34, -1,
	165, -1, 156, -1, 172, -1, 188, -1, 199, -1, 72, -1, 94, -1, 196, -1,
	181, -1, 109, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: audio* at uaudio*|arcofi*|harmony* */
    {&audio_ca,		&audio_cd,	 0, STAR,     loc,    0, pv+76, 0,    0},
/*  1: midi* at umidi* */
    {&midi_ca,		&midi_cd,	 0, STAR,     loc,    0, pv+100, 0,    0},
/*  2: radio* at udsbr* */
    {&radio_ca,		&radio_cd,	 0, STAR,     loc,    0, pv+146, 0,    0},
/*  3: vscsi0 at root */
    {&vscsi_ca,		&vscsi_cd,	 0, NORM,     loc,    0, pv+19, 0,    0},
/*  4: mpath0 at root */
    {&mpath_ca,		&mpath_cd,	 0, NORM,     loc,    0, pv+19, 0,    0},
/*  5: softraid0 at root */
    {&softraid_ca,	&softraid_cd,	 0, NORM,     loc,    0, pv+19, 0,    0},
/*  6: wsdisplay* at udl*|sti*|sti* console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+  6,    0, pv+80, 1,    0},
/*  7: wskbd* at ukbd*|ucc*|pckbd*|hilkbd* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+  7,    0, pv+63, 13,    0},
/*  8: wsmouse* at ums*|pms*|hilms* mux 0 */
    {&wsmouse_ca,	&wsmouse_cd,	 0, STAR, loc+ 47,    0, pv+84, 25,    0},
/*  9: nsphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ti*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&nsphy_ca,		&nsphy_cd,	 0, STAR, loc+ 36,    0, pv+ 0, 31,    0},
/* 10: nsphyter* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ti*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&nsphyter_ca,	&nsphyter_cd,	 0, STAR, loc+ 36,    0, pv+ 0, 31,    0},
/* 11: gentbi* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ti*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&gentbi_ca,	&gentbi_cd,	 0, STAR, loc+ 36,    0, pv+ 0, 31,    0},
/* 12: qsphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ti*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&qsphy_ca,		&qsphy_cd,	 0, STAR, loc+ 36,    0, pv+ 0, 31,    0},
/* 13: inphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ti*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&inphy_ca,		&inphy_cd,	 0, STAR, loc+ 36,    0, pv+ 0, 31,    0},
/* 14: iophy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ti*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&iophy_ca,		&iophy_cd,	 0, STAR, loc+ 36,    0, pv+ 0, 31,    0},
/* 15: eephy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ti*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&eephy_ca,		&eephy_cd,	 0, STAR, loc+ 36,    0, pv+ 0, 31,    0},
/* 16: exphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ti*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&exphy_ca,		&exphy_cd,	 0, STAR, loc+ 36,    0, pv+ 0, 31,    0},
/* 17: rlphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ti*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&rlphy_ca,		&rlphy_cd,	 0, STAR, loc+ 36,    0, pv+ 0, 31,    0},
/* 18: lxtphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ti*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&lxtphy_ca,	&lxtphy_cd,	 0, STAR, loc+ 36,    0, pv+ 0, 31,    0},
/* 19: luphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ti*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&luphy_ca,		&luphy_cd,	 0, STAR, loc+ 36,    0, pv+ 0, 31,    0},
/* 20: mtdphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ti*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&mtdphy_ca,	&mtdphy_cd,	 0, STAR, loc+ 36,    0, pv+ 0, 31,    0},
/* 21: icsphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ti*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&icsphy_ca,	&icsphy_cd,	 0, STAR, loc+ 36,    0, pv+ 0, 31,    0},
/* 22: sqphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ti*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&sqphy_ca,		&sqphy_cd,	 0, STAR, loc+ 36,    0, pv+ 0, 31,    0},
/* 23: tqphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ti*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&tqphy_ca,		&tqphy_cd,	 0, STAR, loc+ 36,    0, pv+ 0, 31,    0},
/* 24: ukphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ti*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+ 36,    0, pv+ 0, 31,    0},
/* 25: dcphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ti*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&dcphy_ca,		&dcphy_cd,	 0, STAR, loc+ 36,    0, pv+ 0, 31,    0},
/* 26: bmtphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ti*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&bmtphy_ca,	&bmtphy_cd,	 0, STAR, loc+ 36,    0, pv+ 0, 31,    0},
/* 27: brgphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ti*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&brgphy_ca,	&brgphy_cd,	 0, STAR, loc+ 36,    0, pv+ 0, 31,    0},
/* 28: xmphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ti*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&xmphy_ca,		&xmphy_cd,	 0, STAR, loc+ 36,    0, pv+ 0, 31,    0},
/* 29: amphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ti*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&amphy_ca,		&amphy_cd,	 0, STAR, loc+ 36,    0, pv+ 0, 31,    0},
/* 30: acphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ti*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&acphy_ca,		&acphy_cd,	 0, STAR, loc+ 36,    0, pv+ 0, 31,    0},
/* 31: urlphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ti*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&urlphy_ca,	&urlphy_cd,	 0, STAR, loc+ 36,    0, pv+ 0, 31,    0},
/* 32: rgephy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ti*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&rgephy_ca,	&rgephy_cd,	 0, STAR, loc+ 36,    0, pv+ 0, 31,    0},
/* 33: ipgphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ti*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&ipgphy_ca,	&ipgphy_cd,	 0, STAR, loc+ 36,    0, pv+ 0, 31,    0},
/* 34: scsibus* at softraid0|vscsi0|umass*|qle*|atapiscsi*|mpath0|oosiop*|osiop*|siop*|siop*|mpi*|qla*|qlw*|ahc* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+37, 68,    0},
/* 35: cd* at scsibus* target -1 lun -1 */
    {&cd_ca,		&cd_cd,		 0, STAR, loc+  4,    0, pv+126, 69,    0},
/* 36: ch* at scsibus* target -1 lun -1 */
    {&ch_ca,		&ch_cd,		 0, STAR, loc+  4,    0, pv+126, 69,    0},
/* 37: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+  4,    0, pv+126, 69,    0},
/* 38: st* at scsibus* target -1 lun -1 */
    {&st_ca,		&st_cd,		 0, STAR, loc+  4,    0, pv+126, 69,    0},
/* 39: uk* at scsibus* target -1 lun -1 */
    {&uk_ca,		&uk_cd,		 0, STAR, loc+  4,    0, pv+126, 69,    0},
/* 40: safte* at scsibus* target -1 lun -1 */
    {&safte_ca,		&safte_cd,	 0, STAR, loc+  4,    0, pv+126, 69,    0},
/* 41: ses* at scsibus* target -1 lun -1 */
    {&ses_ca,		&ses_cd,	 0, STAR, loc+  4,    0, pv+126, 69,    0},
/* 42: sym* at scsibus* target -1 lun -1 */
    {&sym_ca,		&sym_cd,	 0, STAR, loc+  4,    0, pv+126, 69,    0},
/* 43: rdac* at scsibus* target -1 lun -1 */
    {&rdac_ca,		&rdac_cd,	 0, STAR, loc+  4,    0, pv+126, 69,    0},
/* 44: emc* at scsibus* target -1 lun -1 */
    {&emc_ca,		&emc_cd,	 0, STAR, loc+  4,    0, pv+126, 69,    0},
/* 45: hds* at scsibus* target -1 lun -1 */
    {&hds_ca,		&hds_cd,	 0, STAR, loc+  4,    0, pv+126, 69,    0},
/* 46: atapiscsi* at pciide* channel -1 */
    {&atapiscsi_ca,	&atapiscsi_cd,	 0, STAR, loc+ 36,    0, pv+96, 72,    0},
/* 47: wd* at wdc*|pciide* channel -1 drive -1 */
    {&wd_ca,		&wd_cd,		 0, STAR, loc+  4,    0, pv+95, 74,    0},
/* 48: eisa* at mongoose* */
    {&eisa_ca,		&eisa_cd,	 0, STAR,     loc,    0, pv+114, 76,    0},
/* 49: pci* at dino*|elroy*|ppb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+ 36,    0, pv+88, 77,    0},
/* 50: sti* at pci* dev -1 function -1 */
    {&sti_pci_ca,	&sti_cd,	 0, STAR, loc+  4,    0, pv+116, 83,    0},
/* 51: ahc* at pci* dev -1 function -1 */
    {&ahc_pci_ca,	&ahc_cd,	 0, STAR, loc+  4,    0, pv+116, 83,    0},
/* 52: qlw* at pci* dev -1 function -1 */
    {&qlw_pci_ca,	&qlw_cd,	 0, STAR, loc+  4,    0, pv+116, 83,    0},
/* 53: qla* at pci* dev -1 function -1 */
    {&qla_pci_ca,	&qla_cd,	 0, STAR, loc+  4,    0, pv+116, 83,    0},
/* 54: qle* at pci* dev -1 function -1 */
    {&qle_ca,		&qle_cd,	 0, STAR, loc+  4,    0, pv+116, 83,    0},
/* 55: mpi* at pci* dev -1 function -1 */
    {&mpi_pci_ca,	&mpi_cd,	 0, STAR, loc+  4,    0, pv+116, 83,    0},
/* 56: siop* at pci* dev -1 function -1 */
    {&siop_pci_ca,	&siop_cd,	 0, STAR, loc+  4,    0, pv+116, 83,    0},
/* 57: pciide* at pci* dev -1 function -1 */
    {&pciide_pci_ca,	&pciide_cd,	 0, STAR, loc+  4,    0, pv+116, 83,    0},
/* 58: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+  4,    0, pv+116, 83,    0},
/* 59: rl* at pci* dev -1 function -1 */
    {&rl_pci_ca,	&rl_cd,		 0, STAR, loc+  4,    0, pv+116, 83,    0},
/* 60: xl* at pci* dev -1 function -1 */
    {&xl_pci_ca,	&xl_cd,		 0, STAR, loc+  4,    0, pv+116, 83,    0},
/* 61: fxp* at pci* dev -1 function -1 */
    {&fxp_pci_ca,	&fxp_cd,	 0, STAR, loc+  4,    0, pv+116, 83,    0},
/* 62: em* at pci* dev -1 function -1 */
    {&em_ca,		&em_cd,		 0, STAR, loc+  4,    0, pv+116, 83,    0},
/* 63: dc* at pci* dev -1 function -1 */
    {&dc_pci_ca,	&dc_cd,		 0, STAR, loc+  4,    0, pv+116, 83,    0},
/* 64: ti* at pci* dev -1 function -1 */
    {&ti_pci_ca,	&ti_cd,		 0, STAR, loc+  4,    0, pv+116, 83,    0},
/* 65: ne* at pci* dev -1 function -1 */
    {&ne_pci_ca,	&ne_cd,		 0, STAR, loc+  4,    0, pv+116, 83,    0},
/* 66: ne* at pcmcia* function -1 irq -1 */
    {&ne_pcmcia_ca,	&ne_cd,		 0, STAR, loc+  4,    0, pv+118, 86,    0},
/* 67: uhci* at pci* dev -1 function -1 */
    {&uhci_pci_ca,	&uhci_cd,	 0, STAR, loc+  4,    0, pv+116, 83,    0},
/* 68: ohci* at pci* dev -1 function -1 */
    {&ohci_pci_ca,	&ohci_cd,	 0, STAR, loc+  4,    0, pv+116, 83,    0},
/* 69: ohci* at cardbus* dev -1 function -1 */
    {&ohci_cardbus_ca,	&ohci_cd,	 0, STAR, loc+  4,    0, pv+120, 89,    0},
/* 70: ehci* at pci* dev -1 function -1 */
    {&ehci_pci_ca,	&ehci_cd,	 0, STAR, loc+  4,    0, pv+116, 83,    0},
/* 71: ehci* at cardbus* dev -1 function -1 */
    {&ehci_cardbus_ca,	&ehci_cd,	 0, STAR, loc+  4,    0, pv+120, 89,    0},
/* 72: cbb* at pci* dev -1 function -1 */
    {&cbb_pci_ca,	&cbb_cd,	 0, STAR, loc+  4,    0, pv+116, 83,    0},
/* 73: skc* at pci* dev -1 function -1 */
    {&skc_ca,		&skc_cd,	 0, STAR, loc+  4,    0, pv+116, 83,    0},
/* 74: sk* at skc* */
    {&sk_ca,		&sk_cd,		 0, STAR,     loc,    0, pv+112, 91,    0},
/* 75: mskc* at pci* dev -1 function -1 */
    {&mskc_ca,		&mskc_cd,	 0, STAR, loc+  4,    0, pv+116, 83,    0},
/* 76: msk* at mskc* */
    {&msk_ca,		&msk_cd,	 0, STAR,     loc,    0, pv+110, 91,    0},
/* 77: bge* at pci* dev -1 function -1 */
    {&bge_ca,		&bge_cd,	 0, STAR, loc+  4,    0, pv+116, 83,    0},
/* 78: stge* at pci* dev -1 function -1 */
    {&stge_ca,		&stge_cd,	 0, STAR, loc+  4,    0, pv+116, 83,    0},
/* 79: hme* at pci* dev -1 function -1 */
    {&hme_pci_ca,	&hme_cd,	 0, STAR, loc+  4,    0, pv+116, 83,    0},
/* 80: cardslot* at cbb* slot -1 */
    {&cardslot_ca,	&cardslot_cd,	 0, STAR, loc+ 36,    0, pv+138, 92,    0},
/* 81: cardbus* at cardslot* slot -1 */
    {&cardbus_ca,	&cardbus_cd,	 0, STAR, loc+ 36,    0, pv+124, 94,    0},
/* 82: pcmcia* at cardslot* controller -1 socket -1 */
    {&pcmcia_ca,	&pcmcia_cd,	 0, STAR, loc+  4,    0, pv+124, 94,    0},
/* 83: wdc* at pcmcia* function -1 irq -1 */
    {&wdc_pcmcia_ca,	&wdc_cd,	 0, STAR, loc+  4,    0, pv+118, 86,    0},
/* 84: wi* at pcmcia* function -1 irq -1 */
    {&wi_pcmcia_ca,	&wi_cd,		 0, STAR, loc+  4,    0, pv+118, 86,    0},
/* 85: an* at pcmcia* function -1 irq -1 */
    {&an_pcmcia_ca,	&an_cd,		 0, STAR, loc+  4,    0, pv+118, 86,    0},
/* 86: usb* at ehci*|ehci*|uhci*|ohci*|ohci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+52, 95,    0},
/* 87: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+104, 95,    0},
/* 88: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/* 89: uaudio* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uaudio_ca,	&uaudio_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/* 90: udl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udl_ca,		&udl_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/* 91: umidi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umidi_ca,		&umidi_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/* 92: ucom* at uftdi*|uplcom*|uark*|umodem*|uvisor*|uvscom*|ubsa*|umct*|uslcom*|uscom*|ucrcom*|uipaq*|umsm*|ucycom*|uslhcom*|uchcom* portno -1 */
    {&ucom_ca,		&ucom_cd,	 0, STAR, loc+ 36,    0, pv+20, 110,    0},
/* 93: ugen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugen_ca,		&ugen_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/* 94: uhidev* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhidev_ca,	&uhidev_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/* 95: uhid* at uhidev* reportid -1 */
    {&uhid_ca,		&uhid_cd,	 0, STAR, loc+ 36,    0, pv+140, 142,    0},
/* 96: fido* at uhidev* reportid -1 */
    {&fido_ca,		&fido_cd,	 0, STAR, loc+ 36,    0, pv+140, 142,    0},
/* 97: ujoy* at uhidev* reportid -1 */
    {&ujoy_ca,		&ujoy_cd,	 0, STAR, loc+ 36,    0, pv+140, 142,    0},
/* 98: ukbd* at uhidev* reportid -1 */
    {&ukbd_ca,		&ukbd_cd,	 0, STAR, loc+ 36,    0, pv+140, 142,    0},
/* 99: ums* at uhidev* reportid -1 */
    {&ums_ca,		&ums_cd,	 0, STAR, loc+ 36,    0, pv+140, 142,    0},
/*100: ucycom* at uhidev* reportid -1 */
    {&ucycom_ca,	&ucycom_cd,	 0, STAR, loc+ 36,    0, pv+140, 142,    0},
/*101: uslhcom* at uhidev* reportid -1 */
    {&uslhcom_ca,	&uslhcom_cd,	 0, STAR, loc+ 36,    0, pv+140, 142,    0},
/*102: ulpt* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ulpt_ca,		&ulpt_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*103: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*104: uthum* at uhidev* reportid -1 */
    {&uthum_ca,		&uthum_cd,	 0, STAR, loc+ 36,    0, pv+140, 142,    0},
/*105: ugold* at uhidev* reportid -1 */
    {&ugold_ca,		&ugold_cd,	 0, STAR, loc+ 36,    0, pv+140, 142,    0},
/*106: uonerng* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uonerng_ca,	&uonerng_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*107: urng* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urng_ca,		&urng_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*108: uvisor* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvisor_ca,	&uvisor_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*109: udsbr* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udsbr_ca,		&udsbr_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*110: utwitch* at uhidev* reportid -1 */
    {&utwitch_ca,	&utwitch_cd,	 0, STAR, loc+ 36,    0, pv+140, 142,    0},
/*111: aue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&aue_ca,		&aue_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*112: axe* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axe_ca,		&axe_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*113: axen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axen_ca,		&axen_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*114: smsc* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&smsc_ca,		&smsc_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*115: cue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cue_ca,		&cue_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*116: kue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&kue_ca,		&kue_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*117: cdce* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cdce_ca,		&cdce_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*118: urndis* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urndis_ca,	&urndis_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*119: udav* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udav_ca,		&udav_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*120: upl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upl_ca,		&upl_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*121: ugl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugl_ca,		&ugl_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*122: url* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&url_ca,		&url_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*123: ure* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ure_ca,		&ure_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*124: umodem* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umodem_ca,	&umodem_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*125: uftdi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uftdi_ca,		&uftdi_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*126: uplcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uplcom_ca,	&uplcom_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*127: umct* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umct_ca,		&umct_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*128: uvscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvscom_ca,	&uvscom_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*129: ubsa* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ubsa_ca,		&ubsa_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*130: uslcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uslcom_ca,	&uslcom_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*131: uark* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uark_ca,		&uark_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*132: uscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uscom_ca,		&uscom_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*133: ucrcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ucrcom_ca,	&ucrcom_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*134: uipaq* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uipaq_ca,		&uipaq_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*135: umsm* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umsm_ca,		&umsm_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*136: uchcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uchcom_ca,	&uchcom_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*137: wi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&wi_usb_ca,	&wi_cd,		 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*138: atu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&atu_ca,		&atu_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*139: ural* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ural_ca,		&ural_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*140: rum* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rum_ca,		&rum_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*141: run* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&run_ca,		&run_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*142: zyd* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&zyd_ca,		&zyd_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*143: rsu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rsu_ca,		&rsu_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*144: uow* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uow_ca,		&uow_cd,	 0, STAR, loc+  0,    0, pv+92, 96,    0},
/*145: upd* at uhidev* reportid -1 */
    {&upd_ca,		&upd_cd,	 0, STAR, loc+ 36,    0, pv+140, 142,    0},
/*146: uhidpp* at uhidev* reportid -1 */
    {&uhidpp_ca,	&uhidpp_cd,	 0, STAR, loc+ 36,    0, pv+140, 142,    0},
/*147: ucc* at uhidev* reportid -1 */
    {&ucc_ca,		&ucc_cd,	 0, STAR, loc+ 36,    0, pv+140, 142,    0},
/*148: hilkbd* at hil* code -1 */
    {&hilkbd_ca,	&hilkbd_cd,	 0, STAR, loc+ 36,    0, pv+142, 144,    0},
/*149: hilms* at hil* code -1 */
    {&hilms_ca,		&hilms_cd,	 0, STAR, loc+ 36,    0, pv+142, 144,    0},
/*150: hilid* at hil* code -1 */
    {&hilid_ca,		&hilid_cd,	 0, STAR, loc+ 36,    0, pv+142, 144,    0},
/*151: onewire* at uow* */
    {&onewire_ca,	&onewire_cd,	 0, STAR,     loc,    0, pv+102, 145,    0},
/*152: owid* at onewire* */
    {&owid_ca,		&owid_cd,	 0, STAR,     loc,    0, pv+98, 145,    0},
/*153: owsbm* at onewire* */
    {&owsbm_ca,		&owsbm_cd,	 0, STAR,     loc,    0, pv+98, 145,    0},
/*154: owtemp* at onewire* */
    {&owtemp_ca,	&owtemp_cd,	 0, STAR,     loc,    0, pv+98, 145,    0},
/*155: owctr* at onewire* */
    {&owctr_ca,		&owctr_cd,	 0, STAR,     loc,    0, pv+98, 145,    0},
/*156: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+19, 0,    0},
/*157: pdc0 at mainbus0 offset -1 irq -1 */
    {&pdc_ca,		&pdc_cd,	 0, NORM, loc+  4,    0, pv+130, 146,    0},
/*158: power0 at mainbus0 offset -1 irq -1 */
    {&power_ca,		&power_cd,	 0, NORM, loc+  4,    0, pv+130, 146,    0},
/*159: lcd0 at mainbus0 offset -1 irq -1 */
    {&lcd_ca,		&lcd_cd,	 0, NORM, loc+  4,    0, pv+130, 146,    0},
/*160: mem* at mainbus0 offset -1 irq -1 */
    {&mem_ca,		&mem_cd,	 0, STAR, loc+  4,    0, pv+130, 146,    0},
/*161: cpu0 at mainbus0 offset -1 irq 0x1f */
    {&cpu_ca,		&cpu_cd,	 0, NORM, loc+ 29,    0, pv+130, 146,    0},
/*162: phantomas0 at mainbus0 offset -1 irq -1 */
    {&phantomas_ca,	&phantomas_cd,	 0, NORM, loc+  4,    0, pv+130, 146,    0},
/*163: uturn0 at mainbus0 offset -1 irq -1 */
    {&uturn_ca,		&uturn_cd,	 0, NORM, loc+  4,    0, pv+130, 146,    0},
/*164: uturn1 at mainbus0 offset -1 irq -1 */
    {&uturn_ca,		&uturn_cd,	 1, NORM, loc+  4,    0, pv+130, 146,    1},
/*165: astro* at mainbus0 offset -1 irq -1 */
    {&astro_ca,		&astro_cd,	 0, STAR, loc+  4,    0, pv+130, 146,    0},
/*166: lasi0 at mainbus0 offset 0x100000 irq -1 */
    {&lasi_ca,		&lasi_cd,	 0, NORM, loc+ 31,    0, pv+130, 146,    0},
/*167: lasi0 at mainbus0|phantomas0|uturn0|uturn1 offset 0xfd00000 irq -1 */
    {&lasi_ca,		&lasi_cd,	 0, NORM, loc+ 33,    0, pv+58, 146,    0},
/*168: lasi1 at mainbus0 offset 0x500000 irq -1 */
    {&lasi_ca,		&lasi_cd,	 1, NORM, loc+ 35,    0, pv+130, 146,    1},
/*169: asp* at mainbus0 offset -1 irq -1 */
    {&asp_ca,		&asp_cd,	 0, STAR, loc+  4,    0, pv+130, 146,    0},
/*170: wax* at mainbus0|phantomas0|uturn0|uturn1 offset -1 irq -1 */
    {&wax_ca,		&wax_cd,	 0, STAR, loc+  4,    0, pv+58, 146,    0},
/*171: mongoose* at mainbus0 offset -1 irq 0x11 */
    {&mg_gedoens_ca,	&mongoose_cd,	 0, STAR, loc+ 37,    0, pv+130, 146,    0},
/*172: dino* at phantomas0|uturn0|uturn1 offset -1 irq -1 */
    {&dino_ca,		&dino_cd,	 0, STAR, loc+  4,    0, pv+59, 149,    0},
/*173: com1 at dino* offset -1 irq 0xb */
    {&com_dino_ca,	&com_cd,	 1, NORM, loc+ 39,    0, pv+132, 77,    1},
/*174: com1 at ssio* irq 3 */
    {&com_ssio_ca,	&com_cd,	 1, NORM, loc+ 28,    0, pv+144, 158,    1},
/*175: com1 at gsc1 offset 0x5000 irq 5 */
    {&com_gsc_ca,	&com_cd,	 1, NORM, loc+ 25,    0, pv+134, 160,    1},
/*176: com1 at gsc2 offset 0x2000 irq 6 */
    {&com_gsc_ca,	&com_cd,	 1, NORM, loc+ 43,    0, pv+70, 163,    1},
/*177: com1 at gsc0 offset 0x822000 irq 6 */
    {&com_gsc_ca,	&com_cd,	 1, NORM, loc+ 45,    0, pv+122, 166,    1},
/*178: elroy* at astro* offset -1 irq -1 */
    {&elroy_ca,		&elroy_cd,	 0, STAR, loc+  4,    0, pv+128, 169,    0},
/*179: sti* at mainbus0|phantomas0|uturn0|uturn1 offset -1 irq -1 */
    {&sti_gedoens_ca,	&sti_cd,	 0, STAR, loc+  4,    0, pv+58, 146,    0},
/*180: siop* at mainbus0|phantomas0|uturn0|uturn1 offset -1 irq 3 */
    {&siop_gedoens_ca,	&siop_cd,	 0, STAR, loc+ 27,    0, pv+58, 146,    0},
/*181: ssio* at pci* dev -1 function -1 */
    {&ssio_ca,		&ssio_cd,	 0, STAR, loc+  4,    0, pv+116, 83,    0},
/*182: com0 at ssio* irq 4 */
    {&com_ssio_ca,	&com_cd,	 0, NORM, loc+ 48,    0, pv+144, 158,    0},
/*183: com0 at gsc0 offset 0x5000 irq 5 */
    {&com_gsc_ca,	&com_cd,	 0, NORM, loc+ 25,    0, pv+122, 166,    0},
/*184: com0 at gsc0 offset 0x823000 irq 5 */
    {&com_gsc_ca,	&com_cd,	 0, NORM, loc+ 23,    0, pv+122, 166,    0},
/*185: lpt0 at ssio* irq -1 */
    {&lpt_ssio_ca,	&lpt_cd,	 0, NORM, loc+ 36,    0, pv+144, 158,    0},
/*186: lpt0 at gsc0|gsc1|gsc2 offset -1 irq 7 */
    {&lpt_gsc_ca,	&lpt_cd,	 0, NORM, loc+ 17,    0, pv+68, 166,    0},
/*187: gsc0 at asp*|lasi0|lasi0 */
    {&gsc_ca,		&gsc_cd,	 0, NORM,     loc,    0, pv+72, 171,    0},
/*188: gsc1 at lasi1 */
    {&gsc_ca,		&gsc_cd,	 1, NORM,     loc,    0, pv+106, 171,    1},
/*189: gsc2 at wax* */
    {&gsc_ca,		&gsc_cd,	 2, NORM,     loc,    0, pv+108, 171,    2},
/*190: com2 at gsc0 offset 0 irq 0xd */
    {&com_gsc_ca,	&com_cd,	 2, NORM, loc+ 15,    0, pv+122, 166,    2},
/*191: com2 at gsc0 offset 0x4040 irq 0xd */
    {&com_gsc_ca,	&com_cd,	 2, NORM, loc+ 11,    0, pv+122, 166,    2},
/*192: com3 at gsc0 offset 0x4060 irq 0xd */
    {&com_gsc_ca,	&com_cd,	 3, NORM, loc+  9,    0, pv+122, 166,    3},
/*193: ie0 at gsc0 offset -1 irq 8 */
    {&ie_gsc_ca,	&ie_cd,		 0, NORM, loc+ 41,    0, pv+122, 166,    0},
/*194: osiop* at gsc0|gsc1|gsc2 offset -1 irq 9 */
    {&osiop_gsc_ca,	&osiop_cd,	 0, STAR, loc+ 19,    0, pv+68, 166,    0},
/*195: oosiop* at gsc0|gsc1|gsc2 offset -1 irq 9 */
    {&oosiop_gsc_ca,	&oosiop_cd,	 0, STAR, loc+ 19,    0, pv+68, 166,    0},
/*196: hil* at gsc0|gsc1|gsc2 offset -1 irq 1 */
    {&hil_gsc_ca,	&hil_cd,	 0, STAR, loc+  7,    0, pv+68, 166,    0},
/*197: arcofi* at gsc0|gsc1|gsc2 offset -1 irq 0xd */
    {&arcofi_gsc_ca,	&arcofi_cd,	 0, STAR, loc+ 13,    0, pv+68, 166,    0},
/*198: harmony* at gsc0|gsc1|gsc2 offset -1 irq 0xd */
    {&harmony_ca,	&harmony_cd,	 0, STAR, loc+ 13,    0, pv+68, 166,    0},
/*199: gsckbc* at gsc0|gsc1|gsc2 offset -1 irq 0x1a */
    {&gsckbc_ca,	&gsckbc_cd,	 0, STAR, loc+ 21,    0, pv+68, 166,    0},
/*200: pckbd* at gsckbc* slot -1 */
    {&pckbd_ca,		&pckbd_cd,	 0, STAR, loc+ 36,    0, pv+136, 172,    0},
/*201: pms* at gsckbc* slot -1 */
    {&pms_ca,		&pms_cd,	 0, STAR, loc+ 36,    0, pv+136, 172,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 3 /* vscsi0 */,
	 4 /* mpath0 */,
	 5 /* softraid0 */,
	156 /* mainbus0 */,
	-1
};

int cfroots_size = 5;

/* pseudo-devices */
extern void pfattach(int);
extern void pflogattach(int);
extern void pfsyncattach(int);
extern void pflowattach(int);
extern void encattach(int);
extern void ptyattach(int);
extern void nmeaattach(int);
extern void mstsattach(int);
extern void endrunattach(int);
extern void vndattach(int);
extern void ksymsattach(int);
extern void bpfilterattach(int);
extern void bridgeattach(int);
extern void vebattach(int);
extern void carpattach(int);
extern void etheripattach(int);
extern void gifattach(int);
extern void greattach(int);
extern void loopattach(int);
extern void mpeattach(int);
extern void mpwattach(int);
extern void mpipattach(int);
extern void bpeattach(int);
extern void pairattach(int);
extern void pppattach(int);
extern void pppoeattach(int);
extern void pppxattach(int);
extern void spppattach(int);
extern void trunkattach(int);
extern void aggrattach(int);
extern void tpmrattach(int);
extern void tunattach(int);
extern void vetherattach(int);
extern void vxlanattach(int);
extern void vlanattach(int);
extern void switchattach(int);
extern void wgattach(int);
extern void bioattach(int);
extern void fuseattach(int);
extern void hotplugattach(int);
extern void wsmuxattach(int);

char *pdevnames[] = {
	"pf",
	"pflog",
	"pfsync",
	"pflow",
	"enc",
	"pty",
	"nmea",
	"msts",
	"endrun",
	"vnd",
	"ksyms",
	"bpfilter",
	"bridge",
	"veb",
	"carp",
	"etherip",
	"gif",
	"gre",
	"loop",
	"mpe",
	"mpw",
	"mpip",
	"bpe",
	"pair",
	"ppp",
	"pppoe",
	"pppx",
	"sppp",
	"trunk",
	"aggr",
	"tpmr",
	"tun",
	"vether",
	"vxlan",
	"vlan",
	"switch",
	"wg",
	"bio",
	"fuse",
	"hotplug",
	"wsmux",
};

int pdevnames_size = 41;

struct pdevinit pdevinit[] = {
	{ pfattach, 1 },
	{ pflogattach, 1 },
	{ pfsyncattach, 1 },
	{ pflowattach, 1 },
	{ encattach, 1 },
	{ ptyattach, 16 },
	{ nmeaattach, 1 },
	{ mstsattach, 1 },
	{ endrunattach, 1 },
	{ vndattach, 4 },
	{ ksymsattach, 1 },
	{ bpfilterattach, 1 },
	{ bridgeattach, 1 },
	{ vebattach, 1 },
	{ carpattach, 1 },
	{ etheripattach, 1 },
	{ gifattach, 1 },
	{ greattach, 1 },
	{ loopattach, 1 },
	{ mpeattach, 1 },
	{ mpwattach, 1 },
	{ mpipattach, 1 },
	{ bpeattach, 1 },
	{ pairattach, 1 },
	{ pppattach, 1 },
	{ pppoeattach, 1 },
	{ pppxattach, 1 },
	{ spppattach, 1 },
	{ trunkattach, 1 },
	{ aggrattach, 1 },
	{ tpmrattach, 1 },
	{ tunattach, 1 },
	{ vetherattach, 1 },
	{ vxlanattach, 1 },
	{ vlanattach, 1 },
	{ switchattach, 1 },
	{ wgattach, 1 },
	{ bioattach, 1 },
	{ fuseattach, 1 },
	{ hotplugattach, 1 },
	{ wsmuxattach, 2 },
	{ NULL, 0 }
};
