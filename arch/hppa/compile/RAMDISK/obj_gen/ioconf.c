/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/hppa/conf/RAMDISK"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver wdc_cd;
extern struct cfdriver ahc_cd;
extern struct cfdriver qlw_cd;
extern struct cfdriver qla_cd;
extern struct cfdriver mpi_cd;
extern struct cfdriver siop_cd;
extern struct cfdriver osiop_cd;
extern struct cfdriver oosiop_cd;
extern struct cfdriver wi_cd;
extern struct cfdriver an_cd;
extern struct cfdriver xl_cd;
extern struct cfdriver fxp_cd;
extern struct cfdriver rl_cd;
extern struct cfdriver dc_cd;
extern struct cfdriver ne_cd;
extern struct cfdriver ie_cd;
extern struct cfdriver com_cd;
extern struct cfdriver sti_cd;
extern struct cfdriver uhci_cd;
extern struct cfdriver ohci_cd;
extern struct cfdriver ehci_cd;
extern struct cfdriver softraid_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver nsphy_cd;
extern struct cfdriver nsphyter_cd;
extern struct cfdriver gentbi_cd;
extern struct cfdriver qsphy_cd;
extern struct cfdriver inphy_cd;
extern struct cfdriver iophy_cd;
extern struct cfdriver eephy_cd;
extern struct cfdriver exphy_cd;
extern struct cfdriver rlphy_cd;
extern struct cfdriver lxtphy_cd;
extern struct cfdriver luphy_cd;
extern struct cfdriver mtdphy_cd;
extern struct cfdriver icsphy_cd;
extern struct cfdriver sqphy_cd;
extern struct cfdriver tqphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver dcphy_cd;
extern struct cfdriver bmtphy_cd;
extern struct cfdriver brgphy_cd;
extern struct cfdriver xmphy_cd;
extern struct cfdriver amphy_cd;
extern struct cfdriver acphy_cd;
extern struct cfdriver urlphy_cd;
extern struct cfdriver rgephy_cd;
extern struct cfdriver ipgphy_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver cd_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver st_cd;
extern struct cfdriver atapiscsi_cd;
extern struct cfdriver wd_cd;
extern struct cfdriver eisa_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver qle_cd;
extern struct cfdriver pciide_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver em_cd;
extern struct cfdriver cbb_cd;
extern struct cfdriver skc_cd;
extern struct cfdriver sk_cd;
extern struct cfdriver mskc_cd;
extern struct cfdriver msk_cd;
extern struct cfdriver bge_cd;
extern struct cfdriver stge_cd;
extern struct cfdriver hme_cd;
extern struct cfdriver cardslot_cd;
extern struct cfdriver cardbus_cd;
extern struct cfdriver pcmcia_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver uhidev_cd;
extern struct cfdriver uhid_cd;
extern struct cfdriver ukbd_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver aue_cd;
extern struct cfdriver axe_cd;
extern struct cfdriver axen_cd;
extern struct cfdriver smsc_cd;
extern struct cfdriver cue_cd;
extern struct cfdriver kue_cd;
extern struct cfdriver cdce_cd;
extern struct cfdriver udav_cd;
extern struct cfdriver upl_cd;
extern struct cfdriver ugl_cd;
extern struct cfdriver url_cd;
extern struct cfdriver ure_cd;
extern struct cfdriver atu_cd;
extern struct cfdriver ural_cd;
extern struct cfdriver hil_cd;
extern struct cfdriver hilkbd_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver pdc_cd;
extern struct cfdriver power_cd;
extern struct cfdriver mem_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver phantomas_cd;
extern struct cfdriver uturn_cd;
extern struct cfdriver astro_cd;
extern struct cfdriver lasi_cd;
extern struct cfdriver asp_cd;
extern struct cfdriver wax_cd;
extern struct cfdriver mongoose_cd;
extern struct cfdriver dino_cd;
extern struct cfdriver elroy_cd;
extern struct cfdriver ssio_cd;
extern struct cfdriver gsc_cd;
extern struct cfdriver gsckbc_cd;
extern struct cfdriver pckbd_cd;

extern struct cfattach softraid_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach nsphy_ca;
extern struct cfattach nsphyter_ca;
extern struct cfattach gentbi_ca;
extern struct cfattach qsphy_ca;
extern struct cfattach inphy_ca;
extern struct cfattach iophy_ca;
extern struct cfattach eephy_ca;
extern struct cfattach exphy_ca;
extern struct cfattach rlphy_ca;
extern struct cfattach lxtphy_ca;
extern struct cfattach luphy_ca;
extern struct cfattach mtdphy_ca;
extern struct cfattach icsphy_ca;
extern struct cfattach sqphy_ca;
extern struct cfattach tqphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach dcphy_ca;
extern struct cfattach bmtphy_ca;
extern struct cfattach brgphy_ca;
extern struct cfattach xmphy_ca;
extern struct cfattach amphy_ca;
extern struct cfattach acphy_ca;
extern struct cfattach urlphy_ca;
extern struct cfattach rgephy_ca;
extern struct cfattach ipgphy_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach cd_ca;
extern struct cfattach sd_ca;
extern struct cfattach st_ca;
extern struct cfattach atapiscsi_ca;
extern struct cfattach wd_ca;
extern struct cfattach eisa_ca;
extern struct cfattach pci_ca;
extern struct cfattach sti_pci_ca;
extern struct cfattach ahc_pci_ca;
extern struct cfattach qlw_pci_ca;
extern struct cfattach qla_pci_ca;
extern struct cfattach qle_ca;
extern struct cfattach mpi_pci_ca;
extern struct cfattach siop_pci_ca;
extern struct cfattach pciide_pci_ca;
extern struct cfattach ppb_ca;
extern struct cfattach rl_pci_ca;
extern struct cfattach xl_pci_ca;
extern struct cfattach fxp_pci_ca;
extern struct cfattach em_ca;
extern struct cfattach dc_pci_ca;
extern struct cfattach ne_pci_ca;
extern struct cfattach uhci_pci_ca;
extern struct cfattach ohci_pci_ca;
extern struct cfattach ehci_pci_ca;
extern struct cfattach cbb_pci_ca;
extern struct cfattach skc_ca;
extern struct cfattach sk_ca;
extern struct cfattach mskc_ca;
extern struct cfattach msk_ca;
extern struct cfattach bge_ca;
extern struct cfattach stge_ca;
extern struct cfattach hme_pci_ca;
extern struct cfattach cardslot_ca;
extern struct cfattach cardbus_ca;
extern struct cfattach ehci_cardbus_ca;
extern struct cfattach ohci_cardbus_ca;
extern struct cfattach pcmcia_ca;
extern struct cfattach ne_pcmcia_ca;
extern struct cfattach wdc_pcmcia_ca;
extern struct cfattach wi_pcmcia_ca;
extern struct cfattach an_pcmcia_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach uhidev_ca;
extern struct cfattach uhid_ca;
extern struct cfattach ukbd_ca;
extern struct cfattach umass_ca;
extern struct cfattach aue_ca;
extern struct cfattach axe_ca;
extern struct cfattach axen_ca;
extern struct cfattach smsc_ca;
extern struct cfattach cue_ca;
extern struct cfattach kue_ca;
extern struct cfattach cdce_ca;
extern struct cfattach udav_ca;
extern struct cfattach upl_ca;
extern struct cfattach ugl_ca;
extern struct cfattach url_ca;
extern struct cfattach ure_ca;
extern struct cfattach wi_usb_ca;
extern struct cfattach atu_ca;
extern struct cfattach ural_ca;
extern struct cfattach hilkbd_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach pdc_ca;
extern struct cfattach power_ca;
extern struct cfattach mem_ca;
extern struct cfattach cpu_ca;
extern struct cfattach phantomas_ca;
extern struct cfattach uturn_ca;
extern struct cfattach astro_ca;
extern struct cfattach lasi_ca;
extern struct cfattach asp_ca;
extern struct cfattach wax_ca;
extern struct cfattach mg_gedoens_ca;
extern struct cfattach dino_ca;
extern struct cfattach com_dino_ca;
extern struct cfattach elroy_ca;
extern struct cfattach sti_gedoens_ca;
extern struct cfattach siop_gedoens_ca;
extern struct cfattach ssio_ca;
extern struct cfattach com_ssio_ca;
extern struct cfattach gsc_ca;
extern struct cfattach com_gsc_ca;
extern struct cfattach ie_gsc_ca;
extern struct cfattach osiop_gsc_ca;
extern struct cfattach oosiop_gsc_ca;
extern struct cfattach hil_gsc_ca;
extern struct cfattach gsckbc_ca;
extern struct cfattach pckbd_ca;


/* locators */
static long loc[44] = {
	-1, -1, -1, -1, -1, -1, -1, -1,
	1, -1, 0x1f, 0x100000, -1, 0xfd00000, -1, 0x500000,
	-1, -1, 0x11, -1, 0xb, 0x5000, 5, 0x2000,
	6, 0x822000, 6, -1, 3, 0x823000, 5, 0,
	0xd, 0x4040, 0xd, 0x4060, 0xd, -1, 8, -1,
	9, -1, 0x1a, 4,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"console",
	"primary",
	"mux",
	"phy",
	"target",
	"lun",
	"channel",
	"drive",
	"bus",
	"dev",
	"function",
	"irq",
	"slot",
	"port",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"reportid",
	"code",
	"offset",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, 1, 2, -1, 0, 1, 2,
	-1, 0, 2, -1, 0, 2, -1, 0,
	2, -1, 3, -1, 3, -1, 3, -1,
	3, -1, 3, -1, 3, -1, 3, -1,
	3, -1, 3, -1, 3, -1, 3, -1,
	3, -1, 3, -1, 3, -1, 3, -1,
	3, -1, 3, -1, 3, -1, 4, 5,
	-1, 6, -1, 6, 7, -1, 8, -1,
	8, -1, 8, -1, 9, 10, -1, 10,
	11, -1, 9, 10, -1, 12, -1, 12,
	-1, 13, 14, 15, 16, 17, 18, -1,
	13, 14, 15, 16, 17, 18, -1, 19,
	-1, 20, -1, 21, 11, -1, 21, 11,
	-1, 21, 11, -1, 21, 11, -1, 11,
	-1, 21, 11, -1, 21, 11, -1, 21,
	11, -1, 21, 11, -1, 12, -1,
};

/* size of parent vectors */
int pv_size = 110;

/* parent vectors */
short pv[110] = {
	89, 88, 85, 81, 80, 79, 78, 64, 63, 62, 61, 59, 50, 51, 49, 45,
	47, 46, -1, 77, 40, 32, 0, 130, 129, 117, 42, 41, 39, 38, 37, -1,
	55, 56, 52, 53, 54, -1, 94, 99, 100, 101, -1, 122, 123, 124, -1, 76,
	133, 93, -1, 106, 103, 104, -1, 109, 115, 44, -1, 72, 73, -1, 116, 36,
	-1, 68, 43, -1, 122, -1, 35, -1, 102, -1, 28, -1, 94, -1, 67, -1,
	123, -1, 109, -1, 131, -1, 118, -1, 132, -1, 57, -1, 65, -1, 74, -1,
	66, -1, 107, -1, 58, -1, 105, -1, 71, -1, 108, -1, 60, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: softraid0 at root */
    {&softraid_ca,	&softraid_cd,	 0, NORM,     loc,    0, pv+18, 0,    0},
/*  1: wsdisplay* at sti*|sti* console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+  6,    0, pv+62, 1,    0},
/*  2: wskbd* at ukbd*|pckbd*|hilkbd* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+  7,    0, pv+47, 9,    0},
/*  3: nsphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&nsphy_ca,		&nsphy_cd,	 0, STAR, loc+ 16,    0, pv+ 0, 18,    0},
/*  4: nsphyter* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&nsphyter_ca,	&nsphyter_cd,	 0, STAR, loc+ 16,    0, pv+ 0, 18,    0},
/*  5: gentbi* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&gentbi_ca,	&gentbi_cd,	 0, STAR, loc+ 16,    0, pv+ 0, 18,    0},
/*  6: qsphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&qsphy_ca,		&qsphy_cd,	 0, STAR, loc+ 16,    0, pv+ 0, 18,    0},
/*  7: inphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&inphy_ca,		&inphy_cd,	 0, STAR, loc+ 16,    0, pv+ 0, 18,    0},
/*  8: iophy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&iophy_ca,		&iophy_cd,	 0, STAR, loc+ 16,    0, pv+ 0, 18,    0},
/*  9: eephy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&eephy_ca,		&eephy_cd,	 0, STAR, loc+ 16,    0, pv+ 0, 18,    0},
/* 10: exphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&exphy_ca,		&exphy_cd,	 0, STAR, loc+ 16,    0, pv+ 0, 18,    0},
/* 11: rlphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&rlphy_ca,		&rlphy_cd,	 0, STAR, loc+ 16,    0, pv+ 0, 18,    0},
/* 12: lxtphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&lxtphy_ca,	&lxtphy_cd,	 0, STAR, loc+ 16,    0, pv+ 0, 18,    0},
/* 13: luphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&luphy_ca,		&luphy_cd,	 0, STAR, loc+ 16,    0, pv+ 0, 18,    0},
/* 14: mtdphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&mtdphy_ca,	&mtdphy_cd,	 0, STAR, loc+ 16,    0, pv+ 0, 18,    0},
/* 15: icsphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&icsphy_ca,	&icsphy_cd,	 0, STAR, loc+ 16,    0, pv+ 0, 18,    0},
/* 16: sqphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&sqphy_ca,		&sqphy_cd,	 0, STAR, loc+ 16,    0, pv+ 0, 18,    0},
/* 17: tqphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&tqphy_ca,		&tqphy_cd,	 0, STAR, loc+ 16,    0, pv+ 0, 18,    0},
/* 18: ukphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+ 16,    0, pv+ 0, 18,    0},
/* 19: dcphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&dcphy_ca,		&dcphy_cd,	 0, STAR, loc+ 16,    0, pv+ 0, 18,    0},
/* 20: bmtphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&bmtphy_ca,	&bmtphy_cd,	 0, STAR, loc+ 16,    0, pv+ 0, 18,    0},
/* 21: brgphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&brgphy_ca,	&brgphy_cd,	 0, STAR, loc+ 16,    0, pv+ 0, 18,    0},
/* 22: xmphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&xmphy_ca,		&xmphy_cd,	 0, STAR, loc+ 16,    0, pv+ 0, 18,    0},
/* 23: amphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&amphy_ca,		&amphy_cd,	 0, STAR, loc+ 16,    0, pv+ 0, 18,    0},
/* 24: acphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&acphy_ca,		&acphy_cd,	 0, STAR, loc+ 16,    0, pv+ 0, 18,    0},
/* 25: urlphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&urlphy_ca,	&urlphy_cd,	 0, STAR, loc+ 16,    0, pv+ 0, 18,    0},
/* 26: rgephy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&rgephy_ca,	&rgephy_cd,	 0, STAR, loc+ 16,    0, pv+ 0, 18,    0},
/* 27: ipgphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|hme*|stge*|bge*|msk*|sk*|ne*|ne*|dc*|rl*|fxp*|xl* phy -1 */
    {&ipgphy_ca,	&ipgphy_cd,	 0, STAR, loc+ 16,    0, pv+ 0, 18,    0},
/* 28: scsibus* at umass*|qle*|atapiscsi*|softraid0|oosiop*|osiop*|siop*|siop*|mpi*|qla*|qlw*|ahc* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+19, 53,    0},
/* 29: cd* at scsibus* target -1 lun -1 */
    {&cd_ca,		&cd_cd,		 0, STAR, loc+  4,    0, pv+74, 54,    0},
/* 30: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+  4,    0, pv+74, 54,    0},
/* 31: st* at scsibus* target -1 lun -1 */
    {&st_ca,		&st_cd,		 0, STAR, loc+  4,    0, pv+74, 54,    0},
/* 32: atapiscsi* at pciide* channel -1 */
    {&atapiscsi_ca,	&atapiscsi_cd,	 0, STAR, loc+ 16,    0, pv+66, 57,    0},
/* 33: wd* at wdc*|pciide* channel -1 drive -1 */
    {&wd_ca,		&wd_cd,		 0, STAR, loc+  4,    0, pv+65, 59,    0},
/* 34: eisa* at mongoose* */
    {&eisa_ca,		&eisa_cd,	 0, STAR,     loc,    0, pv+106, 61,    0},
/* 35: pci* at dino*|elroy*|ppb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+ 16,    0, pv+55, 62,    0},
/* 36: sti* at pci* dev -1 function -1 */
    {&sti_pci_ca,	&sti_cd,	 0, STAR, loc+  4,    0, pv+70, 68,    0},
/* 37: ahc* at pci* dev -1 function -1 */
    {&ahc_pci_ca,	&ahc_cd,	 0, STAR, loc+  4,    0, pv+70, 68,    0},
/* 38: qlw* at pci* dev -1 function -1 */
    {&qlw_pci_ca,	&qlw_cd,	 0, STAR, loc+  4,    0, pv+70, 68,    0},
/* 39: qla* at pci* dev -1 function -1 */
    {&qla_pci_ca,	&qla_cd,	 0, STAR, loc+  4,    0, pv+70, 68,    0},
/* 40: qle* at pci* dev -1 function -1 */
    {&qle_ca,		&qle_cd,	 0, STAR, loc+  4,    0, pv+70, 68,    0},
/* 41: mpi* at pci* dev -1 function -1 */
    {&mpi_pci_ca,	&mpi_cd,	 0, STAR, loc+  4,    0, pv+70, 68,    0},
/* 42: siop* at pci* dev -1 function -1 */
    {&siop_pci_ca,	&siop_cd,	 0, STAR, loc+  4,    0, pv+70, 68,    0},
/* 43: pciide* at pci* dev -1 function -1 */
    {&pciide_pci_ca,	&pciide_cd,	 0, STAR, loc+  4,    0, pv+70, 68,    0},
/* 44: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+  4,    0, pv+70, 68,    0},
/* 45: rl* at pci* dev -1 function -1 */
    {&rl_pci_ca,	&rl_cd,		 0, STAR, loc+  4,    0, pv+70, 68,    0},
/* 46: xl* at pci* dev -1 function -1 */
    {&xl_pci_ca,	&xl_cd,		 0, STAR, loc+  4,    0, pv+70, 68,    0},
/* 47: fxp* at pci* dev -1 function -1 */
    {&fxp_pci_ca,	&fxp_cd,	 0, STAR, loc+  4,    0, pv+70, 68,    0},
/* 48: em* at pci* dev -1 function -1 */
    {&em_ca,		&em_cd,		 0, STAR, loc+  4,    0, pv+70, 68,    0},
/* 49: dc* at pci* dev -1 function -1 */
    {&dc_pci_ca,	&dc_cd,		 0, STAR, loc+  4,    0, pv+70, 68,    0},
/* 50: ne* at pci* dev -1 function -1 */
    {&ne_pci_ca,	&ne_cd,		 0, STAR, loc+  4,    0, pv+70, 68,    0},
/* 51: ne* at pcmcia* function -1 irq -1 */
    {&ne_pcmcia_ca,	&ne_cd,		 0, STAR, loc+  4,    0, pv+78, 71,    0},
/* 52: uhci* at pci* dev -1 function -1 */
    {&uhci_pci_ca,	&uhci_cd,	 0, STAR, loc+  4,    0, pv+70, 68,    0},
/* 53: ohci* at pci* dev -1 function -1 */
    {&ohci_pci_ca,	&ohci_cd,	 0, STAR, loc+  4,    0, pv+70, 68,    0},
/* 54: ohci* at cardbus* dev -1 function -1 */
    {&ohci_cardbus_ca,	&ohci_cd,	 0, STAR, loc+  4,    0, pv+96, 74,    0},
/* 55: ehci* at pci* dev -1 function -1 */
    {&ehci_pci_ca,	&ehci_cd,	 0, STAR, loc+  4,    0, pv+70, 68,    0},
/* 56: ehci* at cardbus* dev -1 function -1 */
    {&ehci_cardbus_ca,	&ehci_cd,	 0, STAR, loc+  4,    0, pv+96, 74,    0},
/* 57: cbb* at pci* dev -1 function -1 */
    {&cbb_pci_ca,	&cbb_cd,	 0, STAR, loc+  4,    0, pv+70, 68,    0},
/* 58: skc* at pci* dev -1 function -1 */
    {&skc_ca,		&skc_cd,	 0, STAR, loc+  4,    0, pv+70, 68,    0},
/* 59: sk* at skc* */
    {&sk_ca,		&sk_cd,		 0, STAR,     loc,    0, pv+100, 76,    0},
/* 60: mskc* at pci* dev -1 function -1 */
    {&mskc_ca,		&mskc_cd,	 0, STAR, loc+  4,    0, pv+70, 68,    0},
/* 61: msk* at mskc* */
    {&msk_ca,		&msk_cd,	 0, STAR,     loc,    0, pv+108, 76,    0},
/* 62: bge* at pci* dev -1 function -1 */
    {&bge_ca,		&bge_cd,	 0, STAR, loc+  4,    0, pv+70, 68,    0},
/* 63: stge* at pci* dev -1 function -1 */
    {&stge_ca,		&stge_cd,	 0, STAR, loc+  4,    0, pv+70, 68,    0},
/* 64: hme* at pci* dev -1 function -1 */
    {&hme_pci_ca,	&hme_cd,	 0, STAR, loc+  4,    0, pv+70, 68,    0},
/* 65: cardslot* at cbb* slot -1 */
    {&cardslot_ca,	&cardslot_cd,	 0, STAR, loc+ 16,    0, pv+90, 77,    0},
/* 66: cardbus* at cardslot* slot -1 */
    {&cardbus_ca,	&cardbus_cd,	 0, STAR, loc+ 16,    0, pv+92, 79,    0},
/* 67: pcmcia* at cardslot* controller -1 socket -1 */
    {&pcmcia_ca,	&pcmcia_cd,	 0, STAR, loc+  4,    0, pv+92, 79,    0},
/* 68: wdc* at pcmcia* function -1 irq -1 */
    {&wdc_pcmcia_ca,	&wdc_cd,	 0, STAR, loc+  4,    0, pv+78, 71,    0},
/* 69: wi* at pcmcia* function -1 irq -1 */
    {&wi_pcmcia_ca,	&wi_cd,		 0, STAR, loc+  4,    0, pv+78, 71,    0},
/* 70: an* at pcmcia* function -1 irq -1 */
    {&an_pcmcia_ca,	&an_cd,		 0, STAR, loc+  4,    0, pv+78, 71,    0},
/* 71: usb* at ehci*|ehci*|uhci*|ohci*|ohci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+32, 80,    0},
/* 72: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+104, 80,    0},
/* 73: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+  0,    0, pv+59, 81,    0},
/* 74: uhidev* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhidev_ca,	&uhidev_cd,	 0, STAR, loc+  0,    0, pv+59, 81,    0},
/* 75: uhid* at uhidev* reportid -1 */
    {&uhid_ca,		&uhid_cd,	 0, STAR, loc+ 16,    0, pv+94, 95,    0},
/* 76: ukbd* at uhidev* reportid -1 */
    {&ukbd_ca,		&ukbd_cd,	 0, STAR, loc+ 16,    0, pv+94, 95,    0},
/* 77: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+  0,    0, pv+59, 81,    0},
/* 78: aue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&aue_ca,		&aue_cd,	 0, STAR, loc+  0,    0, pv+59, 81,    0},
/* 79: axe* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axe_ca,		&axe_cd,	 0, STAR, loc+  0,    0, pv+59, 81,    0},
/* 80: axen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axen_ca,		&axen_cd,	 0, STAR, loc+  0,    0, pv+59, 81,    0},
/* 81: smsc* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&smsc_ca,		&smsc_cd,	 0, STAR, loc+  0,    0, pv+59, 81,    0},
/* 82: cue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cue_ca,		&cue_cd,	 0, STAR, loc+  0,    0, pv+59, 81,    0},
/* 83: kue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&kue_ca,		&kue_cd,	 0, STAR, loc+  0,    0, pv+59, 81,    0},
/* 84: cdce* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cdce_ca,		&cdce_cd,	 0, STAR, loc+  0,    0, pv+59, 81,    0},
/* 85: udav* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udav_ca,		&udav_cd,	 0, STAR, loc+  0,    0, pv+59, 81,    0},
/* 86: upl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upl_ca,		&upl_cd,	 0, STAR, loc+  0,    0, pv+59, 81,    0},
/* 87: ugl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugl_ca,		&ugl_cd,	 0, STAR, loc+  0,    0, pv+59, 81,    0},
/* 88: url* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&url_ca,		&url_cd,	 0, STAR, loc+  0,    0, pv+59, 81,    0},
/* 89: ure* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ure_ca,		&ure_cd,	 0, STAR, loc+  0,    0, pv+59, 81,    0},
/* 90: wi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&wi_usb_ca,	&wi_cd,		 0, STAR, loc+  0,    0, pv+59, 81,    0},
/* 91: atu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&atu_ca,		&atu_cd,	 0, STAR, loc+  0,    0, pv+59, 81,    0},
/* 92: ural* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ural_ca,		&ural_cd,	 0, STAR, loc+  0,    0, pv+59, 81,    0},
/* 93: hilkbd* at hil* code -1 */
    {&hilkbd_ca,	&hilkbd_cd,	 0, STAR, loc+ 16,    0, pv+84, 97,    0},
/* 94: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+18, 0,    0},
/* 95: pdc0 at mainbus0 offset -1 irq -1 */
    {&pdc_ca,		&pdc_cd,	 0, NORM, loc+  4,    0, pv+76, 99,    0},
/* 96: power0 at mainbus0 offset -1 irq -1 */
    {&power_ca,		&power_cd,	 0, NORM, loc+  4,    0, pv+76, 99,    0},
/* 97: mem* at mainbus0 offset -1 irq -1 */
    {&mem_ca,		&mem_cd,	 0, STAR, loc+  4,    0, pv+76, 99,    0},
/* 98: cpu* at mainbus0 offset -1 irq 0x1f */
    {&cpu_ca,		&cpu_cd,	 0, STAR, loc+  9,    0, pv+76, 99,    0},
/* 99: phantomas0 at mainbus0 offset -1 irq -1 */
    {&phantomas_ca,	&phantomas_cd,	 0, NORM, loc+  4,    0, pv+76, 99,    0},
/*100: uturn0 at mainbus0 offset -1 irq -1 */
    {&uturn_ca,		&uturn_cd,	 0, NORM, loc+  4,    0, pv+76, 99,    0},
/*101: uturn1 at mainbus0 offset -1 irq -1 */
    {&uturn_ca,		&uturn_cd,	 1, NORM, loc+  4,    0, pv+76, 99,    1},
/*102: astro* at mainbus0 offset -1 irq -1 */
    {&astro_ca,		&astro_cd,	 0, STAR, loc+  4,    0, pv+76, 99,    0},
/*103: lasi0 at mainbus0 offset 0x100000 irq -1 */
    {&lasi_ca,		&lasi_cd,	 0, NORM, loc+ 11,    0, pv+76, 99,    0},
/*104: lasi0 at mainbus0|phantomas0|uturn0|uturn1 offset 0xfd00000 irq -1 */
    {&lasi_ca,		&lasi_cd,	 0, NORM, loc+ 13,    0, pv+38, 99,    0},
/*105: lasi1 at mainbus0 offset 0x500000 irq -1 */
    {&lasi_ca,		&lasi_cd,	 1, NORM, loc+ 15,    0, pv+76, 99,    1},
/*106: asp* at mainbus0 offset -1 irq -1 */
    {&asp_ca,		&asp_cd,	 0, STAR, loc+  4,    0, pv+76, 99,    0},
/*107: wax* at mainbus0|phantomas0|uturn0|uturn1 offset -1 irq -1 */
    {&wax_ca,		&wax_cd,	 0, STAR, loc+  4,    0, pv+38, 99,    0},
/*108: mongoose* at mainbus0 offset -1 irq 0x11 */
    {&mg_gedoens_ca,	&mongoose_cd,	 0, STAR, loc+ 17,    0, pv+76, 99,    0},
/*109: dino* at phantomas0|uturn0|uturn1 offset -1 irq -1 */
    {&dino_ca,		&dino_cd,	 0, STAR, loc+  4,    0, pv+39, 102,    0},
/*110: com1 at dino* offset -1 irq 0xb */
    {&com_dino_ca,	&com_cd,	 1, NORM, loc+ 19,    0, pv+82, 62,    1},
/*111: com1 at ssio* irq 3 */
    {&com_ssio_ca,	&com_cd,	 1, NORM, loc+ 28,    0, pv+86, 111,    1},
/*112: com1 at gsc1 offset 0x5000 irq 5 */
    {&com_gsc_ca,	&com_cd,	 1, NORM, loc+ 21,    0, pv+80, 113,    1},
/*113: com1 at gsc2 offset 0x2000 irq 6 */
    {&com_gsc_ca,	&com_cd,	 1, NORM, loc+ 23,    0, pv+45, 116,    1},
/*114: com1 at gsc0 offset 0x822000 irq 6 */
    {&com_gsc_ca,	&com_cd,	 1, NORM, loc+ 25,    0, pv+68, 119,    1},
/*115: elroy* at astro* offset -1 irq -1 */
    {&elroy_ca,		&elroy_cd,	 0, STAR, loc+  4,    0, pv+72, 122,    0},
/*116: sti* at mainbus0|phantomas0|uturn0|uturn1 offset -1 irq -1 */
    {&sti_gedoens_ca,	&sti_cd,	 0, STAR, loc+  4,    0, pv+38, 99,    0},
/*117: siop* at mainbus0|phantomas0|uturn0|uturn1 offset -1 irq 3 */
    {&siop_gedoens_ca,	&siop_cd,	 0, STAR, loc+ 27,    0, pv+38, 99,    0},
/*118: ssio* at pci* dev -1 function -1 */
    {&ssio_ca,		&ssio_cd,	 0, STAR, loc+  4,    0, pv+70, 68,    0},
/*119: com0 at ssio* irq 4 */
    {&com_ssio_ca,	&com_cd,	 0, NORM, loc+ 43,    0, pv+86, 111,    0},
/*120: com0 at gsc0 offset 0x5000 irq 5 */
    {&com_gsc_ca,	&com_cd,	 0, NORM, loc+ 21,    0, pv+68, 119,    0},
/*121: com0 at gsc0 offset 0x823000 irq 5 */
    {&com_gsc_ca,	&com_cd,	 0, NORM, loc+ 29,    0, pv+68, 119,    0},
/*122: gsc0 at asp*|lasi0|lasi0 */
    {&gsc_ca,		&gsc_cd,	 0, NORM,     loc,    0, pv+51, 124,    0},
/*123: gsc1 at lasi1 */
    {&gsc_ca,		&gsc_cd,	 1, NORM,     loc,    0, pv+102, 124,    1},
/*124: gsc2 at wax* */
    {&gsc_ca,		&gsc_cd,	 2, NORM,     loc,    0, pv+98, 124,    2},
/*125: com2 at gsc0 offset 0 irq 0xd */
    {&com_gsc_ca,	&com_cd,	 2, NORM, loc+ 31,    0, pv+68, 119,    2},
/*126: com2 at gsc0 offset 0x4040 irq 0xd */
    {&com_gsc_ca,	&com_cd,	 2, NORM, loc+ 33,    0, pv+68, 119,    2},
/*127: com3 at gsc0 offset 0x4060 irq 0xd */
    {&com_gsc_ca,	&com_cd,	 3, NORM, loc+ 35,    0, pv+68, 119,    3},
/*128: ie0 at gsc0 offset -1 irq 8 */
    {&ie_gsc_ca,	&ie_cd,		 0, NORM, loc+ 37,    0, pv+68, 119,    0},
/*129: osiop* at gsc0|gsc1|gsc2 offset -1 irq 9 */
    {&osiop_gsc_ca,	&osiop_cd,	 0, STAR, loc+ 39,    0, pv+43, 119,    0},
/*130: oosiop* at gsc0|gsc1|gsc2 offset -1 irq 9 */
    {&oosiop_gsc_ca,	&oosiop_cd,	 0, STAR, loc+ 39,    0, pv+43, 119,    0},
/*131: hil* at gsc0|gsc1|gsc2 offset -1 irq 1 */
    {&hil_gsc_ca,	&hil_cd,	 0, STAR, loc+  7,    0, pv+43, 119,    0},
/*132: gsckbc* at gsc0|gsc1|gsc2 offset -1 irq 0x1a */
    {&gsckbc_ca,	&gsckbc_cd,	 0, STAR, loc+ 41,    0, pv+43, 119,    0},
/*133: pckbd* at gsckbc* slot -1 */
    {&pckbd_ca,		&pckbd_cd,	 0, STAR, loc+ 16,    0, pv+88, 125,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 0 /* softraid0 */,
	94 /* mainbus0 */,
	-1
};

int cfroots_size = 3;

/* pseudo-devices */
extern void rdattach(int);
extern void loopattach(int);
extern void bpfilterattach(int);
extern void vlanattach(int);
extern void wsmuxattach(int);
extern void bioattach(int);

char *pdevnames[] = {
	"rd",
	"loop",
	"bpfilter",
	"vlan",
	"wsmux",
	"bio",
};

int pdevnames_size = 6;

struct pdevinit pdevinit[] = {
	{ rdattach, 1 },
	{ loopattach, 1 },
	{ bpfilterattach, 1 },
	{ vlanattach, 1 },
	{ wsmuxattach, 2 },
	{ bioattach, 1 },
	{ NULL, 0 }
};
