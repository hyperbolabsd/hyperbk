/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/i386/conf/GENERIC"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver video_cd;
extern struct cfdriver audio_cd;
extern struct cfdriver midi_cd;
extern struct cfdriver drm_cd;
extern struct cfdriver vga_cd;
extern struct cfdriver wdc_cd;
extern struct cfdriver ahc_cd;
extern struct cfdriver ahd_cd;
extern struct cfdriver aic_cd;
extern struct cfdriver adv_cd;
extern struct cfdriver adw_cd;
extern struct cfdriver gdt_cd;
extern struct cfdriver twe_cd;
extern struct cfdriver cac_cd;
extern struct cfdriver ciss_cd;
extern struct cfdriver ami_cd;
extern struct cfdriver mfi_cd;
extern struct cfdriver qlw_cd;
extern struct cfdriver qla_cd;
extern struct cfdriver ahci_cd;
extern struct cfdriver nvme_cd;
extern struct cfdriver mpi_cd;
extern struct cfdriver sili_cd;
extern struct cfdriver uha_cd;
extern struct cfdriver siop_cd;
extern struct cfdriver ep_cd;
extern struct cfdriver lc_cd;
extern struct cfdriver wi_cd;
extern struct cfdriver an_cd;
extern struct cfdriver le_cd;
extern struct cfdriver xl_cd;
extern struct cfdriver fxp_cd;
extern struct cfdriver mtd_cd;
extern struct cfdriver rl_cd;
extern struct cfdriver re_cd;
extern struct cfdriver dc_cd;
extern struct cfdriver sm_cd;
extern struct cfdriver epic_cd;
extern struct cfdriver ne_cd;
extern struct cfdriver ie_cd;
extern struct cfdriver gem_cd;
extern struct cfdriver ti_cd;
extern struct cfdriver com_cd;
extern struct cfdriver pckbc_cd;
extern struct cfdriver cy_cd;
extern struct cfdriver lpt_cd;
extern struct cfdriver iha_cd;
extern struct cfdriver trm_cd;
extern struct cfdriver lm_cd;
extern struct cfdriver ath_cd;
extern struct cfdriver athn_cd;
extern struct cfdriver bwfm_cd;
extern struct cfdriver atw_cd;
extern struct cfdriver rtw_cd;
extern struct cfdriver rtwn_cd;
extern struct cfdriver ral_cd;
extern struct cfdriver acx_cd;
extern struct cfdriver pgt_cd;
extern struct cfdriver sf_cd;
extern struct cfdriver malo_cd;
extern struct cfdriver bwi_cd;
extern struct cfdriver virtio_cd;
extern struct cfdriver uhci_cd;
extern struct cfdriver ohci_cd;
extern struct cfdriver ehci_cd;
extern struct cfdriver xhci_cd;
extern struct cfdriver sdhc_cd;
extern struct cfdriver rtsx_cd;
extern struct cfdriver radio_cd;
extern struct cfdriver ipmi_cd;
extern struct cfdriver vscsi_cd;
extern struct cfdriver mpath_cd;
extern struct cfdriver softraid_cd;
extern struct cfdriver spdmem_cd;
extern struct cfdriver tlphy_cd;
extern struct cfdriver nsphy_cd;
extern struct cfdriver nsphyter_cd;
extern struct cfdriver gentbi_cd;
extern struct cfdriver qsphy_cd;
extern struct cfdriver inphy_cd;
extern struct cfdriver iophy_cd;
extern struct cfdriver eephy_cd;
extern struct cfdriver exphy_cd;
extern struct cfdriver rlphy_cd;
extern struct cfdriver lxtphy_cd;
extern struct cfdriver luphy_cd;
extern struct cfdriver mtdphy_cd;
extern struct cfdriver icsphy_cd;
extern struct cfdriver sqphy_cd;
extern struct cfdriver tqphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver dcphy_cd;
extern struct cfdriver bmtphy_cd;
extern struct cfdriver brgphy_cd;
extern struct cfdriver xmphy_cd;
extern struct cfdriver amphy_cd;
extern struct cfdriver acphy_cd;
extern struct cfdriver nsgphy_cd;
extern struct cfdriver urlphy_cd;
extern struct cfdriver rgephy_cd;
extern struct cfdriver ciphy_cd;
extern struct cfdriver ipgphy_cd;
extern struct cfdriver etphy_cd;
extern struct cfdriver jmphy_cd;
extern struct cfdriver atphy_cd;
extern struct cfdriver rdcphy_cd;
extern struct cfdriver mlphy_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver cd_cd;
extern struct cfdriver ch_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver st_cd;
extern struct cfdriver uk_cd;
extern struct cfdriver safte_cd;
extern struct cfdriver ses_cd;
extern struct cfdriver sym_cd;
extern struct cfdriver rdac_cd;
extern struct cfdriver emc_cd;
extern struct cfdriver hds_cd;
extern struct cfdriver atapiscsi_cd;
extern struct cfdriver wd_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver pvbus_cd;
extern struct cfdriver pvclock_cd;
extern struct cfdriver vmt_cd;
extern struct cfdriver vio_cd;
extern struct cfdriver vioblk_cd;
extern struct cfdriver viomb_cd;
extern struct cfdriver viornd_cd;
extern struct cfdriver vioscsi_cd;
extern struct cfdriver vmmci_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver arc_cd;
extern struct cfdriver jmb_cd;
extern struct cfdriver mfii_cd;
extern struct cfdriver ips_cd;
extern struct cfdriver eap_cd;
extern struct cfdriver eso_cd;
extern struct cfdriver auacer_cd;
extern struct cfdriver auich_cd;
extern struct cfdriver azalia_cd;
extern struct cfdriver envy_cd;
extern struct cfdriver emu_cd;
extern struct cfdriver autri_cd;
extern struct cfdriver auixp_cd;
extern struct cfdriver clcs_cd;
extern struct cfdriver clct_cd;
extern struct cfdriver maestro_cd;
extern struct cfdriver esa_cd;
extern struct cfdriver yds_cd;
extern struct cfdriver fms_cd;
extern struct cfdriver auvia_cd;
extern struct cfdriver qle_cd;
extern struct cfdriver mpii_cd;
extern struct cfdriver de_cd;
extern struct cfdriver pcn_cd;
extern struct cfdriver neo_cd;
extern struct cfdriver pciide_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver vr_cd;
extern struct cfdriver vte_cd;
extern struct cfdriver tl_cd;
extern struct cfdriver txp_cd;
extern struct cfdriver sv_cd;
extern struct cfdriver bktr_cd;
extern struct cfdriver em_cd;
extern struct cfdriver ixgb_cd;
extern struct cfdriver ix_cd;
extern struct cfdriver xge_cd;
extern struct cfdriver thtc_cd;
extern struct cfdriver tht_cd;
extern struct cfdriver myx_cd;
extern struct cfdriver oce_cd;
extern struct cfdriver cas_cd;
extern struct cfdriver hifn_cd;
extern struct cfdriver ubsec_cd;
extern struct cfdriver safe_cd;
extern struct cfdriver wb_cd;
extern struct cfdriver sis_cd;
extern struct cfdriver se_cd;
extern struct cfdriver ste_cd;
extern struct cfdriver wdt_cd;
extern struct cfdriver berkwdt_cd;
extern struct cfdriver pwdog_cd;
extern struct cfdriver mbg_cd;
extern struct cfdriver cbb_cd;
extern struct cfdriver skc_cd;
extern struct cfdriver sk_cd;
extern struct cfdriver mskc_cd;
extern struct cfdriver msk_cd;
extern struct cfdriver puc_cd;
extern struct cfdriver ipw_cd;
extern struct cfdriver iwi_cd;
extern struct cfdriver wpi_cd;
extern struct cfdriver iwn_cd;
extern struct cfdriver iwm_cd;
extern struct cfdriver cmpci_cd;
extern struct cfdriver pcscp_cd;
extern struct cfdriver nge_cd;
extern struct cfdriver lge_cd;
extern struct cfdriver bge_cd;
extern struct cfdriver bnx_cd;
extern struct cfdriver vge_cd;
extern struct cfdriver stge_cd;
extern struct cfdriver nfe_cd;
extern struct cfdriver et_cd;
extern struct cfdriver jme_cd;
extern struct cfdriver age_cd;
extern struct cfdriver alc_cd;
extern struct cfdriver ale_cd;
extern struct cfdriver amdpm_cd;
extern struct cfdriver bce_cd;
extern struct cfdriver piixpm_cd;
extern struct cfdriver vic_cd;
extern struct cfdriver vmx_cd;
extern struct cfdriver vmwpvs_cd;
extern struct cfdriver lii_cd;
extern struct cfdriver ichiic_cd;
extern struct cfdriver alipm_cd;
extern struct cfdriver viapm_cd;
extern struct cfdriver amdiic_cd;
extern struct cfdriver nviic_cd;
extern struct cfdriver kate_cd;
extern struct cfdriver km_cd;
extern struct cfdriver gcu_cd;
extern struct cfdriver auglx_cd;
extern struct cfdriver itherm_cd;
extern struct cfdriver pchtemp_cd;
extern struct cfdriver glxpcib_cd;
extern struct cfdriver rge_cd;
extern struct cfdriver agp_cd;
extern struct cfdriver aliagp_cd;
extern struct cfdriver amdagp_cd;
extern struct cfdriver intelagp_cd;
extern struct cfdriver intagp_cd;
extern struct cfdriver sisagp_cd;
extern struct cfdriver viaagp_cd;
extern struct cfdriver inteldrm_cd;
extern struct cfdriver radeondrm_cd;
extern struct cfdriver pchb_cd;
extern struct cfdriver amas_cd;
extern struct cfdriver elansc_cd;
extern struct cfdriver geodesc_cd;
extern struct cfdriver glxsb_cd;
extern struct cfdriver amdmsr_cd;
extern struct cfdriver pcib_cd;
extern struct cfdriver ichpcib_cd;
extern struct cfdriver gscpcib_cd;
extern struct cfdriver amdpcib_cd;
extern struct cfdriver tcpcib_cd;
extern struct cfdriver hme_cd;
extern struct cfdriver fd_cd;
extern struct cfdriver isa_cd;
extern struct cfdriver isadma_cd;
extern struct cfdriver fdc_cd;
extern struct cfdriver ast_cd;
extern struct cfdriver pcdisplay_cd;
extern struct cfdriver wds_cd;
extern struct cfdriver we_cd;
extern struct cfdriver ec_cd;
extern struct cfdriver eg_cd;
extern struct cfdriver el_cd;
extern struct cfdriver ex_cd;
extern struct cfdriver mpu_cd;
extern struct cfdriver sb_cd;
extern struct cfdriver pas_cd;
extern struct cfdriver ess_cd;
extern struct cfdriver gus_cd;
extern struct cfdriver pcppi_cd;
extern struct cfdriver spkr_cd;
extern struct cfdriver wbsio_cd;
extern struct cfdriver schsio_cd;
extern struct cfdriver fins_cd;
extern struct cfdriver nsclpcsio_cd;
extern struct cfdriver gscsio_cd;
extern struct cfdriver it_cd;
extern struct cfdriver viasio_cd;
extern struct cfdriver uguru_cd;
extern struct cfdriver aps_cd;
extern struct cfdriver npx_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver wsmouse_cd;
extern struct cfdriver pckbd_cd;
extern struct cfdriver pms_cd;
extern struct cfdriver skgpio_cd;
extern struct cfdriver eisa_cd;
extern struct cfdriver isapnp_cd;
extern struct cfdriver ef_cd;
extern struct cfdriver joy_cd;
extern struct cfdriver bios_cd;
extern struct cfdriver apm_cd;
extern struct cfdriver mpbios_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver ioapic_cd;
extern struct cfdriver pcibios_cd;
extern struct cfdriver cardslot_cd;
extern struct cfdriver cardbus_cd;
extern struct cfdriver pcic_cd;
extern struct cfdriver tcic_cd;
extern struct cfdriver pcmcia_cd;
extern struct cfdriver xe_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver uaudio_cd;
extern struct cfdriver uvideo_cd;
extern struct cfdriver utvfu_cd;
extern struct cfdriver udl_cd;
extern struct cfdriver umidi_cd;
extern struct cfdriver ucom_cd;
extern struct cfdriver ugen_cd;
extern struct cfdriver uhidev_cd;
extern struct cfdriver uhid_cd;
extern struct cfdriver fido_cd;
extern struct cfdriver ujoy_cd;
extern struct cfdriver ukbd_cd;
extern struct cfdriver ums_cd;
extern struct cfdriver uts_cd;
extern struct cfdriver ubcmtp_cd;
extern struct cfdriver ucycom_cd;
extern struct cfdriver uslhcom_cd;
extern struct cfdriver ulpt_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver uthum_cd;
extern struct cfdriver ugold_cd;
extern struct cfdriver utrh_cd;
extern struct cfdriver uoakrh_cd;
extern struct cfdriver uoaklux_cd;
extern struct cfdriver uoakv_cd;
extern struct cfdriver uonerng_cd;
extern struct cfdriver urng_cd;
extern struct cfdriver udcf_cd;
extern struct cfdriver umbg_cd;
extern struct cfdriver uvisor_cd;
extern struct cfdriver udsbr_cd;
extern struct cfdriver utwitch_cd;
extern struct cfdriver aue_cd;
extern struct cfdriver axe_cd;
extern struct cfdriver axen_cd;
extern struct cfdriver smsc_cd;
extern struct cfdriver cue_cd;
extern struct cfdriver kue_cd;
extern struct cfdriver cdce_cd;
extern struct cfdriver urndis_cd;
extern struct cfdriver mos_cd;
extern struct cfdriver mue_cd;
extern struct cfdriver udav_cd;
extern struct cfdriver upl_cd;
extern struct cfdriver ugl_cd;
extern struct cfdriver url_cd;
extern struct cfdriver ure_cd;
extern struct cfdriver umodem_cd;
extern struct cfdriver uftdi_cd;
extern struct cfdriver uplcom_cd;
extern struct cfdriver umct_cd;
extern struct cfdriver uvscom_cd;
extern struct cfdriver ubsa_cd;
extern struct cfdriver uslcom_cd;
extern struct cfdriver uark_cd;
extern struct cfdriver moscom_cd;
extern struct cfdriver umcs_cd;
extern struct cfdriver uscom_cd;
extern struct cfdriver ucrcom_cd;
extern struct cfdriver uipaq_cd;
extern struct cfdriver umsm_cd;
extern struct cfdriver uchcom_cd;
extern struct cfdriver uticom_cd;
extern struct cfdriver atu_cd;
extern struct cfdriver ural_cd;
extern struct cfdriver rum_cd;
extern struct cfdriver run_cd;
extern struct cfdriver zyd_cd;
extern struct cfdriver upgt_cd;
extern struct cfdriver urtw_cd;
extern struct cfdriver urtwn_cd;
extern struct cfdriver rsu_cd;
extern struct cfdriver otus_cd;
extern struct cfdriver umb_cd;
extern struct cfdriver uath_cd;
extern struct cfdriver uow_cd;
extern struct cfdriver uberry_cd;
extern struct cfdriver upd_cd;
extern struct cfdriver uwacom_cd;
extern struct cfdriver uhidpp_cd;
extern struct cfdriver ucc_cd;
extern struct cfdriver iic_cd;
extern struct cfdriver lmtemp_cd;
extern struct cfdriver lmn_cd;
extern struct cfdriver lmenv_cd;
extern struct cfdriver maxtmp_cd;
extern struct cfdriver adc_cd;
extern struct cfdriver admtemp_cd;
extern struct cfdriver admlc_cd;
extern struct cfdriver admtm_cd;
extern struct cfdriver admtmp_cd;
extern struct cfdriver admtt_cd;
extern struct cfdriver maxds_cd;
extern struct cfdriver adt_cd;
extern struct cfdriver admcts_cd;
extern struct cfdriver asbtm_cd;
extern struct cfdriver wbenv_cd;
extern struct cfdriver wbng_cd;
extern struct cfdriver nvt_cd;
extern struct cfdriver glenv_cd;
extern struct cfdriver adl_cd;
extern struct cfdriver andl_cd;
extern struct cfdriver thmc_cd;
extern struct cfdriver sdtemp_cd;
extern struct cfdriver lisa_cd;
extern struct cfdriver gpio_cd;
extern struct cfdriver gpioiic_cd;
extern struct cfdriver gpioow_cd;
extern struct cfdriver onewire_cd;
extern struct cfdriver owid_cd;
extern struct cfdriver owsbm_cd;
extern struct cfdriver owtemp_cd;
extern struct cfdriver owctr_cd;
extern struct cfdriver sdmmc_cd;
extern struct cfdriver acpi_cd;
extern struct cfdriver acpitimer_cd;
extern struct cfdriver acpiac_cd;
extern struct cfdriver acpibat_cd;
extern struct cfdriver acpibtn_cd;
extern struct cfdriver acpicmos_cd;
extern struct cfdriver acpicpu_cd;
extern struct cfdriver acpihpet_cd;
extern struct cfdriver acpiec_cd;
extern struct cfdriver acpitz_cd;
extern struct cfdriver acpimadt_cd;
extern struct cfdriver acpimcfg_cd;
extern struct cfdriver acpiprt_cd;
extern struct cfdriver acpidock_cd;
extern struct cfdriver asmc_cd;
extern struct cfdriver acpiasus_cd;
extern struct cfdriver acpithinkpad_cd;
extern struct cfdriver acpitoshiba_cd;
extern struct cfdriver acpisony_cd;
extern struct cfdriver acpivideo_cd;
extern struct cfdriver acpivout_cd;
extern struct cfdriver acpipwrres_cd;
extern struct cfdriver aibs_cd;
extern struct cfdriver acpisbs_cd;
extern struct cfdriver esm_cd;

extern struct cfattach video_ca;
extern struct cfattach audio_ca;
extern struct cfattach midi_ca;
extern struct cfattach drm_ca;
extern struct cfattach radio_ca;
extern struct cfattach vscsi_ca;
extern struct cfattach mpath_ca;
extern struct cfattach softraid_ca;
extern struct cfattach tlphy_ca;
extern struct cfattach nsphy_ca;
extern struct cfattach nsphyter_ca;
extern struct cfattach gentbi_ca;
extern struct cfattach qsphy_ca;
extern struct cfattach inphy_ca;
extern struct cfattach iophy_ca;
extern struct cfattach eephy_ca;
extern struct cfattach exphy_ca;
extern struct cfattach rlphy_ca;
extern struct cfattach lxtphy_ca;
extern struct cfattach luphy_ca;
extern struct cfattach mtdphy_ca;
extern struct cfattach icsphy_ca;
extern struct cfattach sqphy_ca;
extern struct cfattach tqphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach dcphy_ca;
extern struct cfattach bmtphy_ca;
extern struct cfattach brgphy_ca;
extern struct cfattach xmphy_ca;
extern struct cfattach amphy_ca;
extern struct cfattach acphy_ca;
extern struct cfattach nsgphy_ca;
extern struct cfattach urlphy_ca;
extern struct cfattach rgephy_ca;
extern struct cfattach ciphy_ca;
extern struct cfattach ipgphy_ca;
extern struct cfattach etphy_ca;
extern struct cfattach jmphy_ca;
extern struct cfattach atphy_ca;
extern struct cfattach rdcphy_ca;
extern struct cfattach mlphy_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach cd_ca;
extern struct cfattach ch_ca;
extern struct cfattach sd_ca;
extern struct cfattach st_ca;
extern struct cfattach uk_ca;
extern struct cfattach safte_ca;
extern struct cfattach ses_ca;
extern struct cfattach sym_ca;
extern struct cfattach rdac_ca;
extern struct cfattach emc_ca;
extern struct cfattach hds_ca;
extern struct cfattach atapiscsi_ca;
extern struct cfattach wd_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach pvbus_ca;
extern struct cfattach pvclock_ca;
extern struct cfattach vmt_ca;
extern struct cfattach vio_ca;
extern struct cfattach vioblk_ca;
extern struct cfattach viomb_ca;
extern struct cfattach viornd_ca;
extern struct cfattach vioscsi_ca;
extern struct cfattach vmmci_ca;
extern struct cfattach pci_ca;
extern struct cfattach vga_pci_ca;
extern struct cfattach ahc_pci_ca;
extern struct cfattach ahd_pci_ca;
extern struct cfattach adv_pci_ca;
extern struct cfattach adw_pci_ca;
extern struct cfattach twe_pci_ca;
extern struct cfattach arc_ca;
extern struct cfattach jmb_ca;
extern struct cfattach ahci_pci_ca;
extern struct cfattach ahci_jmb_ca;
extern struct cfattach nvme_pci_ca;
extern struct cfattach ami_pci_ca;
extern struct cfattach mfi_pci_ca;
extern struct cfattach mfii_ca;
extern struct cfattach ips_ca;
extern struct cfattach eap_ca;
extern struct cfattach eso_ca;
extern struct cfattach auacer_ca;
extern struct cfattach auich_ca;
extern struct cfattach azalia_ca;
extern struct cfattach envy_ca;
extern struct cfattach emu_ca;
extern struct cfattach autri_ca;
extern struct cfattach auixp_ca;
extern struct cfattach clcs_ca;
extern struct cfattach clct_ca;
extern struct cfattach maestro_ca;
extern struct cfattach esa_ca;
extern struct cfattach yds_ca;
extern struct cfattach fms_ca;
extern struct cfattach auvia_ca;
extern struct cfattach gdt_pci_ca;
extern struct cfattach cac_pci_ca;
extern struct cfattach ciss_pci_ca;
extern struct cfattach qlw_pci_ca;
extern struct cfattach qla_pci_ca;
extern struct cfattach qle_ca;
extern struct cfattach mpi_pci_ca;
extern struct cfattach mpii_ca;
extern struct cfattach sili_pci_ca;
extern struct cfattach de_ca;
extern struct cfattach ep_pci_ca;
extern struct cfattach pcn_ca;
extern struct cfattach siop_pci_ca;
extern struct cfattach neo_ca;
extern struct cfattach pciide_pci_ca;
extern struct cfattach pciide_jmb_ca;
extern struct cfattach ppb_ca;
extern struct cfattach cy_pci_ca;
extern struct cfattach mtd_pci_ca;
extern struct cfattach rl_pci_ca;
extern struct cfattach re_pci_ca;
extern struct cfattach vr_ca;
extern struct cfattach vte_ca;
extern struct cfattach tl_ca;
extern struct cfattach txp_ca;
extern struct cfattach sv_ca;
extern struct cfattach bktr_ca;
extern struct cfattach xl_pci_ca;
extern struct cfattach fxp_pci_ca;
extern struct cfattach em_ca;
extern struct cfattach ixgb_ca;
extern struct cfattach ix_ca;
extern struct cfattach xge_ca;
extern struct cfattach thtc_ca;
extern struct cfattach tht_ca;
extern struct cfattach myx_ca;
extern struct cfattach oce_ca;
extern struct cfattach dc_pci_ca;
extern struct cfattach epic_pci_ca;
extern struct cfattach ti_pci_ca;
extern struct cfattach ne_pci_ca;
extern struct cfattach gem_pci_ca;
extern struct cfattach cas_ca;
extern struct cfattach hifn_ca;
extern struct cfattach ubsec_ca;
extern struct cfattach safe_ca;
extern struct cfattach wb_ca;
extern struct cfattach sf_pci_ca;
extern struct cfattach sis_ca;
extern struct cfattach se_ca;
extern struct cfattach ste_ca;
extern struct cfattach wdt_ca;
extern struct cfattach berkwdt_ca;
extern struct cfattach pwdog_ca;
extern struct cfattach mbg_ca;
extern struct cfattach uhci_pci_ca;
extern struct cfattach ohci_pci_ca;
extern struct cfattach ehci_pci_ca;
extern struct cfattach xhci_pci_ca;
extern struct cfattach cbb_pci_ca;
extern struct cfattach skc_ca;
extern struct cfattach sk_ca;
extern struct cfattach mskc_ca;
extern struct cfattach msk_ca;
extern struct cfattach com_puc_ca;
extern struct cfattach lpt_puc_ca;
extern struct cfattach puc_pci_ca;
extern struct cfattach wi_pci_ca;
extern struct cfattach an_pci_ca;
extern struct cfattach ipw_ca;
extern struct cfattach iwi_ca;
extern struct cfattach wpi_ca;
extern struct cfattach iwn_ca;
extern struct cfattach iwm_ca;
extern struct cfattach cmpci_ca;
extern struct cfattach iha_pci_ca;
extern struct cfattach trm_pci_ca;
extern struct cfattach pcscp_ca;
extern struct cfattach nge_ca;
extern struct cfattach lge_ca;
extern struct cfattach bge_ca;
extern struct cfattach bnx_ca;
extern struct cfattach vge_ca;
extern struct cfattach stge_ca;
extern struct cfattach nfe_ca;
extern struct cfattach et_ca;
extern struct cfattach jme_ca;
extern struct cfattach age_ca;
extern struct cfattach alc_ca;
extern struct cfattach ale_ca;
extern struct cfattach amdpm_ca;
extern struct cfattach bce_ca;
extern struct cfattach ath_pci_ca;
extern struct cfattach athn_pci_ca;
extern struct cfattach atw_pci_ca;
extern struct cfattach rtw_pci_ca;
extern struct cfattach rtwn_pci_ca;
extern struct cfattach ral_pci_ca;
extern struct cfattach acx_pci_ca;
extern struct cfattach pgt_pci_ca;
extern struct cfattach malo_pci_ca;
extern struct cfattach bwi_pci_ca;
extern struct cfattach piixpm_ca;
extern struct cfattach vic_ca;
extern struct cfattach vmx_ca;
extern struct cfattach vmwpvs_ca;
extern struct cfattach lii_ca;
extern struct cfattach ichiic_ca;
extern struct cfattach alipm_ca;
extern struct cfattach viapm_ca;
extern struct cfattach amdiic_ca;
extern struct cfattach nviic_ca;
extern struct cfattach sdhc_pci_ca;
extern struct cfattach kate_ca;
extern struct cfattach km_ca;
extern struct cfattach gcu_ca;
extern struct cfattach auglx_ca;
extern struct cfattach itherm_ca;
extern struct cfattach pchtemp_ca;
extern struct cfattach glxpcib_ca;
extern struct cfattach rtsx_pci_ca;
extern struct cfattach virtio_pci_ca;
extern struct cfattach bwfm_pci_ca;
extern struct cfattach rge_ca;
extern struct cfattach agp_ca;
extern struct cfattach aliagp_ca;
extern struct cfattach amdagp_ca;
extern struct cfattach intelagp_ca;
extern struct cfattach intagp_ca;
extern struct cfattach sisagp_ca;
extern struct cfattach viaagp_ca;
extern struct cfattach inteldrm_ca;
extern struct cfattach radeondrm_ca;
extern struct cfattach pchb_ca;
extern struct cfattach amas_ca;
extern struct cfattach elansc_ca;
extern struct cfattach geodesc_ca;
extern struct cfattach glxsb_ca;
extern struct cfattach amdmsr_ca;
extern struct cfattach pcib_ca;
extern struct cfattach ichpcib_ca;
extern struct cfattach gscpcib_ca;
extern struct cfattach amdpcib_ca;
extern struct cfattach tcpcib_ca;
extern struct cfattach hme_pci_ca;
extern struct cfattach isa_ca;
extern struct cfattach isadma_ca;
extern struct cfattach fdc_ca;
extern struct cfattach fd_ca;
extern struct cfattach ast_ca;
extern struct cfattach com_isa_ca;
extern struct cfattach com_commulti_ca;
extern struct cfattach cy_isa_ca;
extern struct cfattach pckbc_isa_ca;
extern struct cfattach vga_isa_ca;
extern struct cfattach pcdisplay_ca;
extern struct cfattach aic_isa_ca;
extern struct cfattach uha_isa_ca;
extern struct cfattach wds_ca;
extern struct cfattach wdc_isa_ca;
extern struct cfattach lc_isa_ca;
extern struct cfattach ne_isa_ca;
extern struct cfattach we_isa_ca;
extern struct cfattach ec_ca;
extern struct cfattach eg_ca;
extern struct cfattach el_ca;
extern struct cfattach ep_isa_ca;
extern struct cfattach ie_isa_ca;
extern struct cfattach ex_ca;
extern struct cfattach le_isa_ca;
extern struct cfattach sm_isa_ca;
extern struct cfattach mpu_isa_ca;
extern struct cfattach sb_isa_ca;
extern struct cfattach pas_ca;
extern struct cfattach gus_isa_ca;
extern struct cfattach pcppi_ca;
extern struct cfattach spkr_ca;
extern struct cfattach lpt_isa_ca;
extern struct cfattach wbsio_ca;
extern struct cfattach schsio_ca;
extern struct cfattach lm_isa_ca;
extern struct cfattach lm_wbsio_ca;
extern struct cfattach fins_ca;
extern struct cfattach nsclpcsio_isa_ca;
extern struct cfattach gscsio_ca;
extern struct cfattach it_ca;
extern struct cfattach viasio_ca;
extern struct cfattach uguru_ca;
extern struct cfattach aps_ca;
extern struct cfattach npx_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach wsmouse_ca;
extern struct cfattach pckbd_ca;
extern struct cfattach pms_ca;
extern struct cfattach skgpio_ca;
extern struct cfattach eisa_ca;
extern struct cfattach ahc_eisa_ca;
extern struct cfattach uha_eisa_ca;
extern struct cfattach cac_eisa_ca;
extern struct cfattach ep_eisa_ca;
extern struct cfattach isapnp_ca;
extern struct cfattach mpu_isapnp_ca;
extern struct cfattach com_isapnp_ca;
extern struct cfattach wdc_isapnp_ca;
extern struct cfattach aic_isapnp_ca;
extern struct cfattach sb_isapnp_ca;
extern struct cfattach ess_isapnp_ca;
extern struct cfattach gus_isapnp_ca;
extern struct cfattach an_isapnp_ca;
extern struct cfattach le_isapnp_ca;
extern struct cfattach ep_isapnp_ca;
extern struct cfattach ef_isapnp_ca;
extern struct cfattach ne_isapnp_ca;
extern struct cfattach we_isapnp_ca;
extern struct cfattach joy_isapnp_ca;
extern struct cfattach bios_ca;
extern struct cfattach apm_ca;
extern struct cfattach mpbios_ca;
extern struct cfattach cpu_ca;
extern struct cfattach ioapic_ca;
extern struct cfattach pcibios_ca;
extern struct cfattach cardslot_ca;
extern struct cfattach cardbus_ca;
extern struct cfattach com_cardbus_ca;
extern struct cfattach xl_cardbus_ca;
extern struct cfattach dc_cardbus_ca;
extern struct cfattach fxp_cardbus_ca;
extern struct cfattach rl_cardbus_ca;
extern struct cfattach re_cardbus_ca;
extern struct cfattach ath_cardbus_ca;
extern struct cfattach athn_cardbus_ca;
extern struct cfattach atw_cardbus_ca;
extern struct cfattach rtw_cardbus_ca;
extern struct cfattach ral_cardbus_ca;
extern struct cfattach acx_cardbus_ca;
extern struct cfattach pgt_cardbus_ca;
extern struct cfattach ehci_cardbus_ca;
extern struct cfattach ohci_cardbus_ca;
extern struct cfattach uhci_cardbus_ca;
extern struct cfattach malo_cardbus_ca;
extern struct cfattach bwi_cardbus_ca;
extern struct cfattach pcic_isa_ca;
extern struct cfattach pcic_pci_ca;
extern struct cfattach pcic_isapnp_ca;
extern struct cfattach tcic_isa_ca;
extern struct cfattach pcmcia_ca;
extern struct cfattach ep_pcmcia_ca;
extern struct cfattach ne_pcmcia_ca;
extern struct cfattach aic_pcmcia_ca;
extern struct cfattach com_pcmcia_ca;
extern struct cfattach wdc_pcmcia_ca;
extern struct cfattach sm_pcmcia_ca;
extern struct cfattach xe_pcmcia_ca;
extern struct cfattach wi_pcmcia_ca;
extern struct cfattach malo_pcmcia_ca;
extern struct cfattach an_pcmcia_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach uaudio_ca;
extern struct cfattach uvideo_ca;
extern struct cfattach utvfu_ca;
extern struct cfattach udl_ca;
extern struct cfattach umidi_ca;
extern struct cfattach ucom_ca;
extern struct cfattach ugen_ca;
extern struct cfattach uhidev_ca;
extern struct cfattach uhid_ca;
extern struct cfattach fido_ca;
extern struct cfattach ujoy_ca;
extern struct cfattach ukbd_ca;
extern struct cfattach ums_ca;
extern struct cfattach uts_ca;
extern struct cfattach ubcmtp_ca;
extern struct cfattach ucycom_ca;
extern struct cfattach uslhcom_ca;
extern struct cfattach ulpt_ca;
extern struct cfattach umass_ca;
extern struct cfattach uthum_ca;
extern struct cfattach ugold_ca;
extern struct cfattach utrh_ca;
extern struct cfattach uoakrh_ca;
extern struct cfattach uoaklux_ca;
extern struct cfattach uoakv_ca;
extern struct cfattach uonerng_ca;
extern struct cfattach urng_ca;
extern struct cfattach udcf_ca;
extern struct cfattach umbg_ca;
extern struct cfattach uvisor_ca;
extern struct cfattach udsbr_ca;
extern struct cfattach utwitch_ca;
extern struct cfattach aue_ca;
extern struct cfattach axe_ca;
extern struct cfattach axen_ca;
extern struct cfattach smsc_ca;
extern struct cfattach cue_ca;
extern struct cfattach kue_ca;
extern struct cfattach cdce_ca;
extern struct cfattach urndis_ca;
extern struct cfattach mos_ca;
extern struct cfattach mue_ca;
extern struct cfattach udav_ca;
extern struct cfattach upl_ca;
extern struct cfattach ugl_ca;
extern struct cfattach url_ca;
extern struct cfattach ure_ca;
extern struct cfattach umodem_ca;
extern struct cfattach uftdi_ca;
extern struct cfattach uplcom_ca;
extern struct cfattach umct_ca;
extern struct cfattach uvscom_ca;
extern struct cfattach ubsa_ca;
extern struct cfattach uslcom_ca;
extern struct cfattach uark_ca;
extern struct cfattach moscom_ca;
extern struct cfattach umcs_ca;
extern struct cfattach uscom_ca;
extern struct cfattach ucrcom_ca;
extern struct cfattach uipaq_ca;
extern struct cfattach umsm_ca;
extern struct cfattach uchcom_ca;
extern struct cfattach uticom_ca;
extern struct cfattach wi_usb_ca;
extern struct cfattach atu_ca;
extern struct cfattach ural_ca;
extern struct cfattach rum_ca;
extern struct cfattach run_ca;
extern struct cfattach zyd_ca;
extern struct cfattach upgt_ca;
extern struct cfattach urtw_ca;
extern struct cfattach urtwn_ca;
extern struct cfattach rsu_ca;
extern struct cfattach otus_ca;
extern struct cfattach umb_ca;
extern struct cfattach uath_ca;
extern struct cfattach athn_usb_ca;
extern struct cfattach uow_ca;
extern struct cfattach uberry_ca;
extern struct cfattach upd_ca;
extern struct cfattach uwacom_ca;
extern struct cfattach bwfm_usb_ca;
extern struct cfattach uhidpp_ca;
extern struct cfattach ucc_ca;
extern struct cfattach iic_ca;
extern struct cfattach lmtemp_ca;
extern struct cfattach lmn_ca;
extern struct cfattach lmenv_ca;
extern struct cfattach maxtmp_ca;
extern struct cfattach adc_ca;
extern struct cfattach admtemp_ca;
extern struct cfattach admlc_ca;
extern struct cfattach admtm_ca;
extern struct cfattach admtmp_ca;
extern struct cfattach admtt_ca;
extern struct cfattach maxds_ca;
extern struct cfattach adt_ca;
extern struct cfattach lm_i2c_ca;
extern struct cfattach admcts_ca;
extern struct cfattach asbtm_ca;
extern struct cfattach wbenv_ca;
extern struct cfattach wbng_ca;
extern struct cfattach nvt_ca;
extern struct cfattach glenv_ca;
extern struct cfattach adl_ca;
extern struct cfattach andl_ca;
extern struct cfattach thmc_ca;
extern struct cfattach spdmem_iic_ca;
extern struct cfattach sdtemp_ca;
extern struct cfattach lisa_ca;
extern struct cfattach gpio_ca;
extern struct cfattach gpioiic_ca;
extern struct cfattach gpioow_ca;
extern struct cfattach onewire_ca;
extern struct cfattach owid_ca;
extern struct cfattach owsbm_ca;
extern struct cfattach owtemp_ca;
extern struct cfattach owctr_ca;
extern struct cfattach sdmmc_ca;
extern struct cfattach bwfm_sdio_ca;
extern struct cfattach acpitimer_ca;
extern struct cfattach acpiac_ca;
extern struct cfattach acpibat_ca;
extern struct cfattach acpibtn_ca;
extern struct cfattach acpicmos_ca;
extern struct cfattach acpicpu_ca;
extern struct cfattach acpihpet_ca;
extern struct cfattach acpiec_ca;
extern struct cfattach acpitz_ca;
extern struct cfattach acpimadt_ca;
extern struct cfattach acpimcfg_ca;
extern struct cfattach acpiprt_ca;
extern struct cfattach acpidock_ca;
extern struct cfattach asmc_ca;
extern struct cfattach acpiasus_ca;
extern struct cfattach acpithinkpad_ca;
extern struct cfattach acpitoshiba_ca;
extern struct cfattach acpisony_ca;
extern struct cfattach acpivideo_ca;
extern struct cfattach acpivout_ca;
extern struct cfattach acpipwrres_ca;
extern struct cfattach aibs_ca;
extern struct cfattach acpisbs_ca;
extern struct cfattach acpi_ca;
extern struct cfattach ipmi_ca;
extern struct cfattach esm_ca;


/* locators */
static long loc[360] = {
	0x3e0, 0, 0xd0000, 0x10000, -1, -1, -1, 0x4e,
	0, -1, 0, -1, -1, -1, 0x2e, 0,
	-1, 0, -1, -1, -1, 0x290, 0, -1,
	0, -1, -1, -1, 0x164e, 0, -1, 0,
	-1, -1, -1, 0x162e, 0, -1, 0, -1,
	-1, -1, 0x3bc, 0, -1, 0, -1, -1,
	-1, 0x278, 0, -1, 0, -1, -1, -1,
	0x378, 0, -1, 0, 7, -1, -1, -1,
	0, -1, 0, -1, -1, -1, 0x220, 0,
	-1, 0, 7, 1, 6, 0x220, 0, -1,
	0, 7, 1, -1, 0x220, 0, -1, 0,
	5, 1, -1, 0x300, 0, -1, 0, -1,
	-1, -1, 0x300, 0, -1, 0, 0xa, -1,
	-1, 0x360, 0, -1, 0, 0xf, 6, -1,
	0x320, 0, -1, 0, 5, -1, -1, 0x360,
	0, 0xd0000, 0, 7, -1, -1, 0x300, 0,
	-1, 0, 9, -1, -1, 0x310, 0, -1,
	0, 5, -1, -1, 0x250, 0, 0xd8000, 0,
	9, -1, -1, 0x300, 0, 0xcc000, 0, 0xa,
	-1, -1, 0x280, 0, 0xd0000, 0, 9, -1,
	-1, 0x280, 0, -1, 0, 9, -1, -1,
	0x240, 0, -1, 0, 9, -1, -1, 0x280,
	0, -1, 0, -1, -1, -1, 0x200, 0,
	-1, 0, -1, -1, -1, 0x170, 0, -1,
	0, 0xf, -1, -1, 0x1f0, 0, -1, 0,
	0xe, -1, -1, 0x350, 0, -1, 0, 0xf,
	6, -1, 0x334, 0, -1, 0, -1, -1,
	-1, 0x330, 0, -1, 0, -1, -1, -1,
	0x340, 0, -1, 0, 0xb, -1, -1, -1,
	0, 0xd4000, 0, 0xc, -1, -1, 0x2e8, 0,
	-1, 0, 9, -1, -1, 0x3e8, 0, -1,
	0, 5, -1, -1, 0x2f8, 0, -1, 0,
	3, -1, -1, 0x3f8, 0, -1, 0, 4,
	-1, -1, 0x1a0, 0, -1, 0, 5, -1,
	-1, 0x3f0, 0, -1, 0, 6, 2, -1,
	0x15c, 0, -1, 0, -1, -1, -1, 0x680,
	0, -1, 0, -1, -1, -1, 0x240, 0,
	0xd0000, 0x10000, -1, -1, -1, 0x3e4, 0, 0xe0000,
	0x4000, -1, -1, -1, 0xf0, 0, -1, 0,
	0xd, -1, -1, 0x1600, 0, -1, 0, -1,
	-1, -1, 0x3e2, 0, 0xe0000, 0x4000, -1, -1,
	-1, 0xe0, 0, -1, 0, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, 0,
	-1, 0, -1, -1, -1, 0, 0, -1,
	-1, 1, 1, -1, 1, -1, 1, 1,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"primary",
	"phy",
	"target",
	"lun",
	"channel",
	"apid",
	"bus",
	"dev",
	"function",
	"slot",
	"irq",
	"port",
	"size",
	"iomem",
	"iosiz",
	"drq",
	"drq2",
	"drive",
	"slave",
	"addr",
	"console",
	"mux",
	"controller",
	"socket",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"portno",
	"reportid",
	"offset",
	"mask",
	"flag",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, -1, 0, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 2, 3, -1, 4, -1, 4, -1,
	4, -1, 4, -1, 4, -1, 4, -1,
	5, -1, 6, -1, 6, -1, 7, 8,
	-1, 9, -1, 8, 10, -1, 7, 8,
	-1, 11, -1, 11, 12, 13, 14, 10,
	15, -1, 11, 12, 13, 14, 10, 15,
	16, -1, 17, -1, 18, -1, 19, 12,
	-1, 20, 0, 21, -1, 20, 0, 21,
	-1, 20, 0, 21, -1, 20, 0, 21,
	-1, 20, 21, -1, 20, 21, -1, 20,
	21, -1, 21, -1, 21, -1, 21, -1,
	21, -1, 21, -1, 9, -1, 9, -1,
	9, -1, 22, 23, -1, 22, 23, -1,
	22, 23, -1, 22, 23, -1, 22, 23,
	-1, 22, 23, -1, 11, 24, 25, 26,
	27, 28, -1, 11, 24, 25, 26, 27,
	28, -1, 29, -1, 29, -1, 29, -1,
	29, -1, 29, -1, 29, -1, 29, -1,
	29, -1, 29, -1, 29, -1, 29, -1,
	29, -1, 29, -1, 29, -1, 29, -1,
	29, -1, 29, -1, 29, -1, 29, -1,
	30, -1, 31, 32, 33, -1,
};

/* size of parent vectors */
int pv_size = 329;

/* parent vectors */
short pv[329] = {
	431, 430, 427, 426, 425, 420, 419, 418, 417, 381, 358, 267, 229, 204, 202, 201,
	200, 199, 198, 197, 196, 195, 194, 193, 192, 191, 172, 170, 156, 155, 154, 152,
	148, 125, 124, 123, 111, 153, 144, 147, 292, 293, 294, 359, 145, 146, 143, 306,
	380, 141, 142, 121, 122, 119, 120, 118, 131, 132, 129, 130, 300, 357, 301, 302,
	109, 110, -1, 8, 6, 503, 403, 287, 228, 190, 106, 104, 81, 80, 73, 64,
	61, 54, 7, 189, 188, 112, 285, 286, 348, 107, 105, 77, 76, 75, 103, 102,
	79, 78, 101, 99, 100, 72, 98, 71, 70, 284, 378, 352, 69, 347, 68, -1,
	385, 387, 308, 353, 310, 355, 309, 354, 82, 87, 83, 127, 113, 187, 91, 92,
	84, 239, 85, 90, 89, 97, 86, 96, 93, 94, 95, 88, -1, 432, 414, 436,
	437, 433, 434, 435, 438, 442, 443, 439, 440, 441, 444, 445, 446, 400, 401, 447,
	-1, 242, 328, 329, 225, 231, 230, 232, 233, 234, 203, 496, -1, 389, 308, 353,
	350, 307, 89, 82, 87, -1, 371, 372, 373, 375, 374, 376, 368, -1, 167, 165,
	166, 161, 162, 163, 164, -1, 56, 262, 265, 266, 263, 264, 242, -1, 264, 242,
	258, 326, 327, 345, -1, 288, 289, 379, 351, 115, 114, -1, 248, 249, 250, 252,
	253, 251, -1, 399, 397, 398, 465, 344, -1, 56, 116, 256, -1, 282, 67, 283,
	-1, 396, 469, 343, -1, 415, 128, 96, -1, 383, 384, -1, 386, 387, -1, 254,
	255, -1, 235, 243, -1, 462, 497, -1, 316, 317, -1, 66, -1, 369, -1, 377,
	-1, 470, -1, 42, -1, 56, -1, 346, -1, 176, -1, 270, -1, 272, -1, 281,
	-1, 168, -1, 392, -1, 362, -1, 311, -1, 527, -1, 503, -1, 254, -1, 171,
	-1, 169, -1, 137, -1, 74, -1, 244, -1, 57, -1, 268, -1, 349, -1, 495,
	-1, 388, -1, 382, -1, 498, -1, 522, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: video* at uvideo*|utvfu* */
    {&video_ca,		&video_cd,	 0, STAR,     loc,    0, pv+252, 0,    0},
/*  1: audio* at uaudio*|utvfu*|sb0|sb*|gus0|gus*|pas0|ess*|eap*|envy*|eso*|sv*|neo*|cmpci*|clcs*|clct*|auacer*|auglx*|auich*|auixp*|autri*|auvia*|azalia*|fms*|maestro*|esa*|yds*|emu* */
    {&audio_ca,		&audio_cd,	 0, STAR,     loc,    0, pv+112, 0,    0},
/*  2: midi* at umidi*|sb0|sb*|mpu*|mpu*|autri*|eap*|envy* */
    {&midi_ca,		&midi_cd,	 0, STAR,     loc,    0, pv+173, 0,    0},
/*  3: drm0 at inteldrm*|radeondrm* primary 1 */
    {&drm_ca,		&drm_cd,	 0, NORM, loc+359,    0, pv+255, 1,    0},
/*  4: drm* at inteldrm*|radeondrm* primary -1 */
    {&drm_ca,		&drm_cd,	 1, STAR, loc+347,    0, pv+255, 1,    1},
/*  5: radio* at udsbr*|bktr0|fms* */
    {&radio_ca,		&radio_cd,	 0, STAR,     loc,    0, pv+245, 4,    0},
/*  6: vscsi0 at root */
    {&vscsi_ca,		&vscsi_cd,	 0, NORM,     loc,    0, pv+66, 0,    0},
/*  7: mpath0 at root */
    {&mpath_ca,		&mpath_cd,	 0, NORM,     loc,    0, pv+66, 0,    0},
/*  8: softraid0 at root */
    {&softraid_ca,	&softraid_cd,	 0, NORM,     loc,    0, pv+66, 0,    0},
/*  9: tlphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&tlphy_ca,		&tlphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 10: nsphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&nsphy_ca,		&nsphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 11: nsphyter* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&nsphyter_ca,	&nsphyter_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 12: gentbi* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&gentbi_ca,	&gentbi_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 13: qsphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&qsphy_ca,		&qsphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 14: inphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&inphy_ca,		&inphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 15: iophy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&iophy_ca,		&iophy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 16: eephy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&eephy_ca,		&eephy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 17: exphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&exphy_ca,		&exphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 18: rlphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&rlphy_ca,		&rlphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 19: lxtphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&lxtphy_ca,	&lxtphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 20: luphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&luphy_ca,		&luphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 21: mtdphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&mtdphy_ca,	&mtdphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 22: icsphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&icsphy_ca,	&icsphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 23: sqphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&sqphy_ca,		&sqphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 24: tqphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&tqphy_ca,		&tqphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 25: ukphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 26: dcphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&dcphy_ca,		&dcphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 27: bmtphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&bmtphy_ca,	&bmtphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 28: brgphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&brgphy_ca,	&brgphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 29: xmphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&xmphy_ca,		&xmphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 30: amphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&amphy_ca,		&amphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 31: acphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&acphy_ca,		&acphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 32: nsgphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&nsgphy_ca,	&nsgphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 33: urlphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&urlphy_ca,	&urlphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 34: rgephy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&rgephy_ca,	&rgephy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 35: ciphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&ciphy_ca,		&ciphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 36: ipgphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&ipgphy_ca,	&ipgphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 37: etphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&etphy_ca,		&etphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 38: jmphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&jmphy_ca,		&jmphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 39: atphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&atphy_ca,		&atphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 40: rdcphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&rdcphy_ca,	&rdcphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 41: mlphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|xe*|ef*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|cas*|tl*|vte*|vr*|pcn*|sf*|ti*|gem*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm0|sm*|dc*|dc*|re*|re*|rl*|rl*|mtd*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&mlphy_ca,		&mlphy_cd,	 0, STAR, loc+347,    0, pv+ 0, 5,    0},
/* 42: scsibus* at softraid0|vscsi0|sdmmc*|umass*|wds0|vmwpvs*|pcscp*|mpii*|qle*|ips*|mfii*|arc*|vioscsi*|vioblk*|atapiscsi*|mpath0|trm*|iha*|siop*|uha0|uha1|uha*|sili*|mpi*|nvme*|ahci*|ahci*|qla*|qlw*|mfi*|ami*|ciss*|cac*|cac*|twe*|gdt*|adw*|adv*|aic0|aic*|aic*|ahd*|ahc*|ahc* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+67, 136,    0},
/* 43: cd* at scsibus* target -1 lun -1 */
    {&cd_ca,		&cd_cd,		 0, STAR, loc+346,    0, pv+275, 137,    0},
/* 44: ch* at scsibus* target -1 lun -1 */
    {&ch_ca,		&ch_cd,		 0, STAR, loc+346,    0, pv+275, 137,    0},
/* 45: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+346,    0, pv+275, 137,    0},
/* 46: st* at scsibus* target -1 lun -1 */
    {&st_ca,		&st_cd,		 0, STAR, loc+346,    0, pv+275, 137,    0},
/* 47: uk* at scsibus* target -1 lun -1 */
    {&uk_ca,		&uk_cd,		 0, STAR, loc+346,    0, pv+275, 137,    0},
/* 48: safte* at scsibus* target -1 lun -1 */
    {&safte_ca,		&safte_cd,	 0, STAR, loc+346,    0, pv+275, 137,    0},
/* 49: ses* at scsibus* target -1 lun -1 */
    {&ses_ca,		&ses_cd,	 0, STAR, loc+346,    0, pv+275, 137,    0},
/* 50: sym* at scsibus* target -1 lun -1 */
    {&sym_ca,		&sym_cd,	 0, STAR, loc+346,    0, pv+275, 137,    0},
/* 51: rdac* at scsibus* target -1 lun -1 */
    {&rdac_ca,		&rdac_cd,	 0, STAR, loc+346,    0, pv+275, 137,    0},
/* 52: emc* at scsibus* target -1 lun -1 */
    {&emc_ca,		&emc_cd,	 0, STAR, loc+346,    0, pv+275, 137,    0},
/* 53: hds* at scsibus* target -1 lun -1 */
    {&hds_ca,		&hds_cd,	 0, STAR, loc+346,    0, pv+275, 137,    0},
/* 54: atapiscsi* at wdc0|wdc1|wdc*|wdc*|pciide*|pciide* channel -1 */
    {&atapiscsi_ca,	&atapiscsi_cd,	 0, STAR, loc+347,    0, pv+213, 140,    0},
/* 55: wd* at wdc0|wdc1|wdc*|wdc*|pciide*|pciide* channel -1 drive -1 */
    {&wd_ca,		&wd_cd,		 0, STAR, loc+346,    0, pv+213, 140,    0},
/* 56: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+66, 0,    0},
/* 57: pvbus0 at mainbus0 apid -1 */
    {&pvbus_ca,		&pvbus_cd,	 0, NORM, loc+347,    0, pv+277, 152,    0},
/* 58: pvclock0 at pvbus0 */
    {&pvclock_ca,	&pvclock_cd,	 0, NORM,     loc,    0, pv+313, 153,    0},
/* 59: vmt0 at pvbus0 */
    {&vmt_ca,		&vmt_cd,	 0, NORM,     loc,    0, pv+313, 153,    0},
/* 60: vio* at virtio* */
    {&vio_ca,		&vio_cd,	 0, STAR,     loc,    0, pv+311, 153,    0},
/* 61: vioblk* at virtio* */
    {&vioblk_ca,	&vioblk_cd,	 0, STAR,     loc,    0, pv+311, 153,    0},
/* 62: viomb* at virtio* */
    {&viomb_ca,		&viomb_cd,	 0, STAR,     loc,    0, pv+311, 153,    0},
/* 63: viornd* at virtio* */
    {&viornd_ca,	&viornd_cd,	 0, STAR,     loc,    0, pv+311, 153,    0},
/* 64: vioscsi* at virtio* */
    {&vioscsi_ca,	&vioscsi_cd,	 0, STAR,     loc,    0, pv+311, 153,    0},
/* 65: vmmci* at virtio* */
    {&vmmci_ca,		&vmmci_cd,	 0, STAR,     loc,    0, pv+311, 153,    0},
/* 66: pci* at mainbus0|ppb*|pchb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+347,    0, pv+233, 152,    0},
/* 67: vga* at pci* dev -1 function -1 */
    {&vga_pci_ca,	&vga_cd,	 1, STAR, loc+346,    0, pv+267, 158,    1},
/* 68: ahc* at pci* dev -1 function -1 */
    {&ahc_pci_ca,	&ahc_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 69: ahd* at pci* dev -1 function -1 */
    {&ahd_pci_ca,	&ahd_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 70: adv* at pci* dev -1 function -1 */
    {&adv_pci_ca,	&adv_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 71: adw* at pci* dev -1 function -1 */
    {&adw_pci_ca,	&adw_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 72: twe* at pci* dev -1 function -1 */
    {&twe_pci_ca,	&twe_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 73: arc* at pci* dev -1 function -1 */
    {&arc_ca,		&arc_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 74: jmb* at pci* dev -1 function -1 */
    {&jmb_ca,		&jmb_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 75: ahci* at pci* dev -1 function -1 */
    {&ahci_pci_ca,	&ahci_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 76: ahci* at jmb* */
    {&ahci_jmb_ca,	&ahci_cd,	 0, STAR,     loc,    0, pv+309, 160,    0},
/* 77: nvme* at pci* dev -1 function -1 */
    {&nvme_pci_ca,	&nvme_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 78: ami* at pci* dev -1 function -1 */
    {&ami_pci_ca,	&ami_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 79: mfi* at pci* dev -1 function -1 */
    {&mfi_pci_ca,	&mfi_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 80: mfii* at pci* dev -1 function -1 */
    {&mfii_ca,		&mfii_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 81: ips* at pci* dev -1 function -1 */
    {&ips_ca,		&ips_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 82: eap* at pci* dev -1 function -1 */
    {&eap_ca,		&eap_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 83: eso* at pci* dev -1 function -1 */
    {&eso_ca,		&eso_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 84: auacer* at pci* dev -1 function -1 */
    {&auacer_ca,	&auacer_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 85: auich* at pci* dev -1 function -1 */
    {&auich_ca,		&auich_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 86: azalia* at pci* dev -1 function -1 */
    {&azalia_ca,	&azalia_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 87: envy* at pci* dev -1 function -1 */
    {&envy_ca,		&envy_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 88: emu* at pci* dev -1 function -1 */
    {&emu_ca,		&emu_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 89: autri* at pci* dev -1 function -1 */
    {&autri_ca,		&autri_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 90: auixp* at pci* dev -1 function -1 */
    {&auixp_ca,		&auixp_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 91: clcs* at pci* dev -1 function -1 */
    {&clcs_ca,		&clcs_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 92: clct* at pci* dev -1 function -1 */
    {&clct_ca,		&clct_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 93: maestro* at pci* dev -1 function -1 */
    {&maestro_ca,	&maestro_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 94: esa* at pci* dev -1 function -1 */
    {&esa_ca,		&esa_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 95: yds* at pci* dev -1 function -1 */
    {&yds_ca,		&yds_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 96: fms* at pci* dev -1 function -1 */
    {&fms_ca,		&fms_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 97: auvia* at pci* dev -1 function -1 */
    {&auvia_ca,		&auvia_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 98: gdt* at pci* dev -1 function -1 */
    {&gdt_pci_ca,	&gdt_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/* 99: cac* at pci* dev -1 function -1 */
    {&cac_pci_ca,	&cac_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*100: cac* at eisa0 slot -1 */
    {&cac_eisa_ca,	&cac_cd,	 0, STAR, loc+347,    0, pv+279, 161,    0},
/*101: ciss* at pci* dev -1 function -1 */
    {&ciss_pci_ca,	&ciss_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*102: qlw* at pci* dev -1 function -1 */
    {&qlw_pci_ca,	&qlw_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*103: qla* at pci* dev -1 function -1 */
    {&qla_pci_ca,	&qla_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*104: qle* at pci* dev -1 function -1 */
    {&qle_ca,		&qle_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*105: mpi* at pci* dev -1 function -1 */
    {&mpi_pci_ca,	&mpi_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*106: mpii* at pci* dev -1 function -1 */
    {&mpii_ca,		&mpii_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*107: sili* at pci* dev -1 function -1 */
    {&sili_pci_ca,	&sili_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*108: de* at pci* dev -1 function -1 */
    {&de_ca,		&de_cd,		 0, STAR, loc+346,    0, pv+267, 158,    0},
/*109: ep* at pci* dev -1 function -1 */
    {&ep_pci_ca,	&ep_cd,		 1, STAR, loc+346,    0, pv+267, 158,    1},
/*110: ep* at pcmcia* function -1 irq -1 */
    {&ep_pcmcia_ca,	&ep_cd,		 1, STAR, loc+346,    0, pv+271, 163,    1},
/*111: pcn* at pci* dev -1 function -1 */
    {&pcn_ca,		&pcn_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*112: siop* at pci* dev -1 function -1 */
    {&siop_pci_ca,	&siop_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*113: neo* at pci* dev -1 function -1 */
    {&neo_ca,		&neo_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*114: pciide* at pci* dev -1 function -1 */
    {&pciide_pci_ca,	&pciide_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*115: pciide* at jmb* */
    {&pciide_jmb_ca,	&pciide_cd,	 0, STAR,     loc,    0, pv+309, 160,    0},
/*116: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*117: cy* at pci* dev -1 function -1 */
    {&cy_pci_ca,	&cy_cd,		 1, STAR, loc+346,    0, pv+267, 158,    1},
/*118: mtd* at pci* dev -1 function -1 */
    {&mtd_pci_ca,	&mtd_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*119: rl* at pci* dev -1 function -1 */
    {&rl_pci_ca,	&rl_cd,		 0, STAR, loc+346,    0, pv+267, 158,    0},
/*120: rl* at cardbus* dev -1 function -1 */
    {&rl_cardbus_ca,	&rl_cd,		 0, STAR, loc+346,    0, pv+269, 166,    0},
/*121: re* at pci* dev -1 function -1 */
    {&re_pci_ca,	&re_cd,		 0, STAR, loc+346,    0, pv+267, 158,    0},
/*122: re* at cardbus* dev -1 function -1 */
    {&re_cardbus_ca,	&re_cd,		 0, STAR, loc+346,    0, pv+269, 166,    0},
/*123: vr* at pci* dev -1 function -1 */
    {&vr_ca,		&vr_cd,		 0, STAR, loc+346,    0, pv+267, 158,    0},
/*124: vte* at pci* dev -1 function -1 */
    {&vte_ca,		&vte_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*125: tl* at pci* dev -1 function -1 */
    {&tl_ca,		&tl_cd,		 0, STAR, loc+346,    0, pv+267, 158,    0},
/*126: txp* at pci* dev -1 function -1 */
    {&txp_ca,		&txp_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*127: sv* at pci* dev -1 function -1 */
    {&sv_ca,		&sv_cd,		 0, STAR, loc+346,    0, pv+267, 158,    0},
/*128: bktr0 at pci* dev -1 function -1 */
    {&bktr_ca,		&bktr_cd,	 0, NORM, loc+346,    0, pv+267, 158,    0},
/*129: xl* at pci* dev -1 function -1 */
    {&xl_pci_ca,	&xl_cd,		 0, STAR, loc+346,    0, pv+267, 158,    0},
/*130: xl* at cardbus* dev -1 function -1 */
    {&xl_cardbus_ca,	&xl_cd,		 0, STAR, loc+346,    0, pv+269, 166,    0},
/*131: fxp* at pci* dev -1 function -1 */
    {&fxp_pci_ca,	&fxp_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*132: fxp* at cardbus* dev -1 function -1 */
    {&fxp_cardbus_ca,	&fxp_cd,	 0, STAR, loc+346,    0, pv+269, 166,    0},
/*133: em* at pci* dev -1 function -1 */
    {&em_ca,		&em_cd,		 0, STAR, loc+346,    0, pv+267, 158,    0},
/*134: ixgb* at pci* dev -1 function -1 */
    {&ixgb_ca,		&ixgb_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*135: ix* at pci* dev -1 function -1 */
    {&ix_ca,		&ix_cd,		 0, STAR, loc+346,    0, pv+267, 158,    0},
/*136: xge* at pci* dev -1 function -1 */
    {&xge_ca,		&xge_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*137: thtc* at pci* dev -1 function -1 */
    {&thtc_ca,		&thtc_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*138: tht* at thtc* */
    {&tht_ca,		&tht_cd,	 0, STAR,     loc,    0, pv+307, 168,    0},
/*139: myx* at pci* dev -1 function -1 */
    {&myx_ca,		&myx_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*140: oce* at pci* dev -1 function -1 */
    {&oce_ca,		&oce_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*141: dc* at pci* dev -1 function -1 */
    {&dc_pci_ca,	&dc_cd,		 0, STAR, loc+346,    0, pv+267, 158,    0},
/*142: dc* at cardbus* dev -1 function -1 */
    {&dc_cardbus_ca,	&dc_cd,		 0, STAR, loc+346,    0, pv+269, 166,    0},
/*143: epic* at pci* dev -1 function -1 */
    {&epic_pci_ca,	&epic_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*144: ti* at pci* dev -1 function -1 */
    {&ti_pci_ca,	&ti_cd,		 0, STAR, loc+346,    0, pv+267, 158,    0},
/*145: ne* at pci* dev -1 function -1 */
    {&ne_pci_ca,	&ne_cd,		 3, STAR, loc+346,    0, pv+267, 158,    3},
/*146: ne* at pcmcia* function -1 irq -1 */
    {&ne_pcmcia_ca,	&ne_cd,		 3, STAR, loc+346,    0, pv+271, 163,    3},
/*147: gem* at pci* dev -1 function -1 */
    {&gem_pci_ca,	&gem_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*148: cas* at pci* dev -1 function -1 */
    {&cas_ca,		&cas_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*149: hifn* at pci* dev -1 function -1 */
    {&hifn_ca,		&hifn_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*150: ubsec* at pci* dev -1 function -1 */
    {&ubsec_ca,		&ubsec_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*151: safe* at pci* dev -1 function -1 */
    {&safe_ca,		&safe_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*152: wb* at pci* dev -1 function -1 */
    {&wb_ca,		&wb_cd,		 0, STAR, loc+346,    0, pv+267, 158,    0},
/*153: sf* at pci* dev -1 function -1 */
    {&sf_pci_ca,	&sf_cd,		 0, STAR, loc+346,    0, pv+267, 158,    0},
/*154: sis* at pci* dev -1 function -1 */
    {&sis_ca,		&sis_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*155: se* at pci* dev -1 function -1 */
    {&se_ca,		&se_cd,		 0, STAR, loc+346,    0, pv+267, 158,    0},
/*156: ste* at pci* dev -1 function -1 */
    {&ste_ca,		&ste_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*157: wdt0 at pci* dev -1 function -1 */
    {&wdt_ca,		&wdt_cd,	 0, NORM, loc+346,    0, pv+267, 158,    0},
/*158: berkwdt0 at pci* dev -1 function -1 */
    {&berkwdt_ca,	&berkwdt_cd,	 0, NORM, loc+346,    0, pv+267, 158,    0},
/*159: pwdog0 at pci* dev -1 function -1 */
    {&pwdog_ca,		&pwdog_cd,	 0, NORM, loc+346,    0, pv+267, 158,    0},
/*160: mbg* at pci* dev -1 function -1 */
    {&mbg_ca,		&mbg_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*161: uhci* at pci* dev -1 function -1 */
    {&uhci_pci_ca,	&uhci_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*162: uhci* at cardbus* dev -1 function -1 */
    {&uhci_cardbus_ca,	&uhci_cd,	 0, STAR, loc+346,    0, pv+269, 166,    0},
/*163: ohci* at pci* dev -1 function -1 */
    {&ohci_pci_ca,	&ohci_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*164: ohci* at cardbus* dev -1 function -1 */
    {&ohci_cardbus_ca,	&ohci_cd,	 0, STAR, loc+346,    0, pv+269, 166,    0},
/*165: ehci* at pci* dev -1 function -1 */
    {&ehci_pci_ca,	&ehci_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*166: ehci* at cardbus* dev -1 function -1 */
    {&ehci_cardbus_ca,	&ehci_cd,	 0, STAR, loc+346,    0, pv+269, 166,    0},
/*167: xhci* at pci* dev -1 function -1 */
    {&xhci_pci_ca,	&xhci_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*168: cbb* at pci* dev -1 function -1 */
    {&cbb_pci_ca,	&cbb_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*169: skc* at pci* dev -1 function -1 */
    {&skc_ca,		&skc_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*170: sk* at skc* */
    {&sk_ca,		&sk_cd,		 0, STAR,     loc,    0, pv+305, 168,    0},
/*171: mskc* at pci* dev -1 function -1 */
    {&mskc_ca,		&mskc_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*172: msk* at mskc* */
    {&msk_ca,		&msk_cd,	 0, STAR,     loc,    0, pv+303, 168,    0},
/*173: com* at puc* port -1 */
    {&com_puc_ca,	&com_cd,	 4, STAR, loc+347,    0, pv+281, 169,    4},
/*174: com* at cardbus* dev -1 function -1 */
    {&com_cardbus_ca,	&com_cd,	 4, STAR, loc+346,    0, pv+269, 166,    4},
/*175: lpt* at puc* port -1 */
    {&lpt_puc_ca,	&lpt_cd,	 3, STAR, loc+347,    0, pv+281, 169,    3},
/*176: puc* at pci* dev -1 function -1 */
    {&puc_pci_ca,	&puc_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*177: wi* at pci* dev -1 function -1 */
    {&wi_pci_ca,	&wi_cd,		 0, STAR, loc+346,    0, pv+267, 158,    0},
/*178: wi* at pcmcia* function -1 irq -1 */
    {&wi_pcmcia_ca,	&wi_cd,		 0, STAR, loc+346,    0, pv+271, 163,    0},
/*179: an* at pci* dev -1 function -1 */
    {&an_pci_ca,	&an_cd,		 0, STAR, loc+346,    0, pv+267, 158,    0},
/*180: an* at isapnp0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 */
    {&an_isapnp_ca,	&an_cd,		 0, STAR, loc+342,    0, pv+317, 171,    0},
/*181: an* at pcmcia* function -1 irq -1 */
    {&an_pcmcia_ca,	&an_cd,		 0, STAR, loc+346,    0, pv+271, 163,    0},
/*182: ipw* at pci* dev -1 function -1 */
    {&ipw_ca,		&ipw_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*183: iwi* at pci* dev -1 function -1 */
    {&iwi_ca,		&iwi_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*184: wpi* at pci* dev -1 function -1 */
    {&wpi_ca,		&wpi_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*185: iwn* at pci* dev -1 function -1 */
    {&iwn_ca,		&iwn_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*186: iwm* at pci* dev -1 function -1 */
    {&iwm_ca,		&iwm_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*187: cmpci* at pci* dev -1 function -1 */
    {&cmpci_ca,		&cmpci_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*188: iha* at pci* dev -1 function -1 */
    {&iha_pci_ca,	&iha_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*189: trm* at pci* dev -1 function -1 */
    {&trm_pci_ca,	&trm_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*190: pcscp* at pci* dev -1 function -1 */
    {&pcscp_ca,		&pcscp_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*191: nge* at pci* dev -1 function -1 */
    {&nge_ca,		&nge_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*192: lge* at pci* dev -1 function -1 */
    {&lge_ca,		&lge_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*193: bge* at pci* dev -1 function -1 */
    {&bge_ca,		&bge_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*194: bnx* at pci* dev -1 function -1 */
    {&bnx_ca,		&bnx_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*195: vge* at pci* dev -1 function -1 */
    {&vge_ca,		&vge_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*196: stge* at pci* dev -1 function -1 */
    {&stge_ca,		&stge_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*197: nfe* at pci* dev -1 function -1 */
    {&nfe_ca,		&nfe_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*198: et* at pci* dev -1 function -1 */
    {&et_ca,		&et_cd,		 0, STAR, loc+346,    0, pv+267, 158,    0},
/*199: jme* at pci* dev -1 function -1 */
    {&jme_ca,		&jme_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*200: age* at pci* dev -1 function -1 */
    {&age_ca,		&age_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*201: alc* at pci* dev -1 function -1 */
    {&alc_ca,		&alc_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*202: ale* at pci* dev -1 function -1 */
    {&ale_ca,		&ale_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*203: amdpm* at pci* dev -1 function -1 */
    {&amdpm_ca,		&amdpm_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*204: bce* at pci* dev -1 function -1 */
    {&bce_ca,		&bce_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*205: ath* at pci* dev -1 function -1 */
    {&ath_pci_ca,	&ath_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*206: ath* at cardbus* dev -1 function -1 */
    {&ath_cardbus_ca,	&ath_cd,	 0, STAR, loc+346,    0, pv+269, 166,    0},
/*207: athn* at pci* dev -1 function -1 */
    {&athn_pci_ca,	&athn_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*208: athn* at cardbus* dev -1 function -1 */
    {&athn_cardbus_ca,	&athn_cd,	 0, STAR, loc+346,    0, pv+269, 166,    0},
/*209: atw* at pci* dev -1 function -1 */
    {&atw_pci_ca,	&atw_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*210: atw* at cardbus* dev -1 function -1 */
    {&atw_cardbus_ca,	&atw_cd,	 0, STAR, loc+346,    0, pv+269, 166,    0},
/*211: rtw* at pci* dev -1 function -1 */
    {&rtw_pci_ca,	&rtw_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*212: rtw* at cardbus* dev -1 function -1 */
    {&rtw_cardbus_ca,	&rtw_cd,	 0, STAR, loc+346,    0, pv+269, 166,    0},
/*213: rtwn* at pci* dev -1 function -1 */
    {&rtwn_pci_ca,	&rtwn_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*214: ral* at pci* dev -1 function -1 */
    {&ral_pci_ca,	&ral_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*215: ral* at cardbus* dev -1 function -1 */
    {&ral_cardbus_ca,	&ral_cd,	 0, STAR, loc+346,    0, pv+269, 166,    0},
/*216: acx* at pci* dev -1 function -1 */
    {&acx_pci_ca,	&acx_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*217: acx* at cardbus* dev -1 function -1 */
    {&acx_cardbus_ca,	&acx_cd,	 0, STAR, loc+346,    0, pv+269, 166,    0},
/*218: pgt* at pci* dev -1 function -1 */
    {&pgt_pci_ca,	&pgt_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*219: pgt* at cardbus* dev -1 function -1 */
    {&pgt_cardbus_ca,	&pgt_cd,	 0, STAR, loc+346,    0, pv+269, 166,    0},
/*220: malo* at pci* dev -1 function -1 */
    {&malo_pci_ca,	&malo_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*221: malo* at cardbus* dev -1 function -1 */
    {&malo_cardbus_ca,	&malo_cd,	 0, STAR, loc+346,    0, pv+269, 166,    0},
/*222: malo* at pcmcia* function -1 irq -1 */
    {&malo_pcmcia_ca,	&malo_cd,	 0, STAR, loc+346,    0, pv+271, 163,    0},
/*223: bwi* at pci* dev -1 function -1 */
    {&bwi_pci_ca,	&bwi_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*224: bwi* at cardbus* dev -1 function -1 */
    {&bwi_cardbus_ca,	&bwi_cd,	 0, STAR, loc+346,    0, pv+269, 166,    0},
/*225: piixpm* at pci* dev -1 function -1 */
    {&piixpm_ca,	&piixpm_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*226: vic* at pci* dev -1 function -1 */
    {&vic_ca,		&vic_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*227: vmx* at pci* dev -1 function -1 */
    {&vmx_ca,		&vmx_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*228: vmwpvs* at pci* dev -1 function -1 */
    {&vmwpvs_ca,	&vmwpvs_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*229: lii* at pci* dev -1 function -1 */
    {&lii_ca,		&lii_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*230: ichiic* at pci* dev -1 function -1 */
    {&ichiic_ca,	&ichiic_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*231: alipm* at pci* dev -1 function -1 */
    {&alipm_ca,		&alipm_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*232: viapm* at pci* dev -1 function -1 */
    {&viapm_ca,		&viapm_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*233: amdiic* at pci* dev -1 function -1 */
    {&amdiic_ca,	&amdiic_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*234: nviic* at pci* dev -1 function -1 */
    {&nviic_ca,		&nviic_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*235: sdhc* at pci* dev -1 function -1 */
    {&sdhc_pci_ca,	&sdhc_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*236: kate* at pci* dev -1 function -1 */
    {&kate_ca,		&kate_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*237: km* at pci* dev -1 function -1 */
    {&km_ca,		&km_cd,		 0, STAR, loc+346,    0, pv+267, 158,    0},
/*238: gcu* at pci* dev -1 function -1 */
    {&gcu_ca,		&gcu_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*239: auglx* at pci* dev -1 function -1 */
    {&auglx_ca,		&auglx_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*240: itherm* at pci* dev -1 function -1 */
    {&itherm_ca,	&itherm_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*241: pchtemp* at pci* dev -1 function -1 */
    {&pchtemp_ca,	&pchtemp_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*242: glxpcib* at pci* dev -1 function -1 */
    {&glxpcib_ca,	&glxpcib_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*243: rtsx* at pci* dev -1 function -1 */
    {&rtsx_pci_ca,	&rtsx_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*244: virtio* at pci* dev -1 function -1 */
    {&virtio_pci_ca,	&virtio_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*245: bwfm* at pci* dev -1 function -1 */
    {&bwfm_pci_ca,	&bwfm_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*246: rge* at pci* dev -1 function -1 */
    {&rge_ca,		&rge_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*247: agp* at aliagp*|amdagp*|intelagp*|sisagp*|viaagp*|intagp* */
    {&agp_ca,		&agp_cd,	 0, STAR,     loc,    0, pv+220, 177,    0},
/*248: aliagp* at pchb* */
    {&aliagp_ca,	&aliagp_cd,	 0, STAR,     loc,    0, pv+235, 156,    0},
/*249: amdagp* at pchb* */
    {&amdagp_ca,	&amdagp_cd,	 0, STAR,     loc,    0, pv+235, 156,    0},
/*250: intelagp* at pchb* */
    {&intelagp_ca,	&intelagp_cd,	 0, STAR,     loc,    0, pv+235, 156,    0},
/*251: intagp* at inteldrm* */
    {&intagp_ca,	&intagp_cd,	 0, STAR,     loc,    0, pv+301, 1,    0},
/*252: sisagp* at pchb* */
    {&sisagp_ca,	&sisagp_cd,	 0, STAR,     loc,    0, pv+235, 156,    0},
/*253: viaagp* at pchb* */
    {&viaagp_ca,	&viaagp_cd,	 0, STAR,     loc,    0, pv+235, 156,    0},
/*254: inteldrm* at pci* dev -1 function -1 */
    {&inteldrm_ca,	&inteldrm_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*255: radeondrm* at pci* dev -1 function -1 */
    {&radeondrm_ca,	&radeondrm_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*256: pchb* at pci* dev -1 function -1 */
    {&pchb_ca,		&pchb_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*257: amas* at pci* dev -1 function -1 */
    {&amas_ca,		&amas_cd,	 0, DSTR, loc+346,    0, pv+267, 158,    0},
/*258: elansc* at pci* dev -1 function -1 */
    {&elansc_ca,	&elansc_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*259: geodesc* at pci* dev -1 function -1 */
    {&geodesc_ca,	&geodesc_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*260: glxsb* at pci* dev -1 function -1 */
    {&glxsb_ca,		&glxsb_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*261: amdmsr0 at mainbus0 apid -1 */
    {&amdmsr_ca,	&amdmsr_cd,	 0, NORM, loc+347,    0, pv+277, 152,    0},
/*262: pcib* at pci* dev -1 function -1 */
    {&pcib_ca,		&pcib_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*263: ichpcib* at pci* dev -1 function -1 */
    {&ichpcib_ca,	&ichpcib_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*264: gscpcib* at pci* dev -1 function -1 */
    {&gscpcib_ca,	&gscpcib_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*265: amdpcib* at pci* dev -1 function -1 */
    {&amdpcib_ca,	&amdpcib_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*266: tcpcib* at pci* dev -1 function -1 */
    {&tcpcib_ca,	&tcpcib_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*267: hme* at pci* dev -1 function -1 */
    {&hme_pci_ca,	&hme_cd,	 0, STAR, loc+346,    0, pv+267, 158,    0},
/*268: isa0 at mainbus0|pcib*|amdpcib*|tcpcib*|ichpcib*|gscpcib*|glxpcib* */
    {&isa_ca,		&isa_cd,	 0, NORM,     loc,    0, pv+198, 152,    0},
/*269: isadma0 at isa0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&isadma_ca,	&isadma_cd,	 0, NORM, loc+ 63,    0, pv+315, 178,    0},
/*270: fdc0 at isa0 port 0x3f0 size 0 iomem -1 iosiz 0 irq 6 drq 2 drq2 -1 */
    {&fdc_ca,		&fdc_cd,	 0, NORM, loc+273,    0, pv+315, 178,    0},
/*271: fd* at fdc0 drive -1 */
    {&fd_ca,		&fd_cd,		 0, STAR, loc+347,    0, pv+283, 186,    0},
/*272: ast0 at isa0 port 0x1a0 size 0 iomem -1 iosiz 0 irq 5 drq -1 drq2 -1 */
    {&ast_ca,		&ast_cd,	 0, NORM, loc+266,    0, pv+315, 178,    0},
/*273: com0 at isa0 port 0x3f8 size 0 iomem -1 iosiz 0 irq 4 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 0, NORM, loc+259,    0, pv+315, 178,    0},
/*274: com1 at isa0 port 0x2f8 size 0 iomem -1 iosiz 0 irq 3 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 1, NORM, loc+252,    0, pv+315, 178,    1},
/*275: com2 at isa0 port 0x3e8 size 0 iomem -1 iosiz 0 irq 5 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 2, NORM, loc+245,    0, pv+315, 178,    2},
/*276: com3 at isa0 port 0x2e8 size 0 iomem -1 iosiz 0 irq 9 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 3, DNRM, loc+238,    0, pv+315, 178,    3},
/*277: com* at ast0 slave -1 */
    {&com_commulti_ca,	&com_cd,	 4, STAR, loc+347,    0, pv+285, 188,    4},
/*278: com* at pcmcia* function -1 irq -1 */
    {&com_pcmcia_ca,	&com_cd,	 4, STAR, loc+346,    0, pv+271, 163,    4},
/*279: com* at isapnp0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 */
    {&com_isapnp_ca,	&com_cd,	 4, STAR, loc+342,    0, pv+317, 171,    4},
/*280: cy0 at isa0 port -1 size 0 iomem 0xd4000 iosiz 0 irq 0xc drq -1 drq2 -1 */
    {&cy_isa_ca,	&cy_cd,		 0, NORM, loc+231,    0, pv+315, 178,    0},
/*281: pckbc0 at isa0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&pckbc_isa_ca,	&pckbc_cd,	 0, NORM, loc+ 63,    0, pv+315, 178,    0},
/*282: vga0 at isa0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&vga_isa_ca,	&vga_cd,	 0, NORM, loc+ 63,    0, pv+315, 178,    0},
/*283: pcdisplay0 at isa0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&pcdisplay_ca,	&pcdisplay_cd,	 0, NORM, loc+ 63,    0, pv+315, 178,    0},
/*284: aic0 at isa0 port 0x340 size 0 iomem -1 iosiz 0 irq 0xb drq -1 drq2 -1 */
    {&aic_isa_ca,	&aic_cd,	 0, NORM, loc+224,    0, pv+315, 178,    0},
/*285: uha0 at isa0 port 0x330 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&uha_isa_ca,	&uha_cd,	 0, NORM, loc+217,    0, pv+315, 178,    0},
/*286: uha1 at isa0 port 0x334 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&uha_isa_ca,	&uha_cd,	 1, DNRM, loc+210,    0, pv+315, 178,    1},
/*287: wds0 at isa0 port 0x350 size 0 iomem -1 iosiz 0 irq 0xf drq 6 drq2 -1 */
    {&wds_ca,		&wds_cd,	 0, DNRM, loc+203,    0, pv+315, 178,    0},
/*288: wdc0 at isa0 port 0x1f0 size 0 iomem -1 iosiz 0 irq 0xe drq -1 drq2 -1 */
    {&wdc_isa_ca,	&wdc_cd,	 0, NORM, loc+196,    0, pv+315, 178,    0},
/*289: wdc1 at isa0 port 0x170 size 0 iomem -1 iosiz 0 irq 0xf drq -1 drq2 -1 */
    {&wdc_isa_ca,	&wdc_cd,	 1, NORM, loc+189,    0, pv+315, 178,    1},
/*290: lc0 at isa0 port 0x200 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&lc_isa_ca,	&lc_cd,		 0, NORM, loc+182,    0, pv+315, 178,    0},
/*291: lc1 at isa0 port 0x280 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&lc_isa_ca,	&lc_cd,		 1, NORM, loc+175,    0, pv+315, 178,    1},
/*292: ne0 at isa0 port 0x240 size 0 iomem -1 iosiz 0 irq 9 drq -1 drq2 -1 */
    {&ne_isa_ca,	&ne_cd,		 0, NORM, loc+168,    0, pv+315, 178,    0},
/*293: ne1 at isa0 port 0x300 size 0 iomem -1 iosiz 0 irq 0xa drq -1 drq2 -1 */
    {&ne_isa_ca,	&ne_cd,		 1, NORM, loc+ 98,    0, pv+315, 178,    1},
/*294: ne2 at isa0 port 0x280 size 0 iomem -1 iosiz 0 irq 9 drq -1 drq2 -1 */
    {&ne_isa_ca,	&ne_cd,		 2, NORM, loc+161,    0, pv+315, 178,    2},
/*295: we0 at isa0 port 0x280 size 0 iomem 0xd0000 iosiz 0 irq 9 drq -1 drq2 -1 */
    {&we_isa_ca,	&we_cd,		 0, NORM, loc+154,    0, pv+315, 178,    0},
/*296: we1 at isa0 port 0x300 size 0 iomem 0xcc000 iosiz 0 irq 0xa drq -1 drq2 -1 */
    {&we_isa_ca,	&we_cd,		 1, NORM, loc+147,    0, pv+315, 178,    1},
/*297: ec0 at isa0 port 0x250 size 0 iomem 0xd8000 iosiz 0 irq 9 drq -1 drq2 -1 */
    {&ec_ca,		&ec_cd,		 0, NORM, loc+140,    0, pv+315, 178,    0},
/*298: eg0 at isa0 port 0x310 size 0 iomem -1 iosiz 0 irq 5 drq -1 drq2 -1 */
    {&eg_ca,		&eg_cd,		 0, DNRM, loc+133,    0, pv+315, 178,    0},
/*299: el0 at isa0 port 0x300 size 0 iomem -1 iosiz 0 irq 9 drq -1 drq2 -1 */
    {&el_ca,		&el_cd,		 0, DNRM, loc+126,    0, pv+315, 178,    0},
/*300: ep0 at isa0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&ep_isa_ca,	&ep_cd,		 0, NORM, loc+ 63,    0, pv+315, 178,    0},
/*301: ep* at isa0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&ep_isa_ca,	&ep_cd,		 1, STAR, loc+ 63,    0, pv+315, 178,    1},
/*302: ep* at eisa0 slot -1 */
    {&ep_eisa_ca,	&ep_cd,		 1, STAR, loc+347,    0, pv+279, 161,    1},
/*303: ie0 at isa0 port 0x360 size 0 iomem 0xd0000 iosiz 0 irq 7 drq -1 drq2 -1 */
    {&ie_isa_ca,	&ie_cd,		 0, NORM, loc+119,    0, pv+315, 178,    0},
/*304: ex0 at isa0 port 0x320 size 0 iomem -1 iosiz 0 irq 5 drq -1 drq2 -1 */
    {&ex_ca,		&ex_cd,		 0, NORM, loc+112,    0, pv+315, 178,    0},
/*305: le0 at isa0 port 0x360 size 0 iomem -1 iosiz 0 irq 0xf drq 6 drq2 -1 */
    {&le_isa_ca,	&le_cd,		 0, NORM, loc+105,    0, pv+315, 178,    0},
/*306: sm0 at isa0 port 0x300 size 0 iomem -1 iosiz 0 irq 0xa drq -1 drq2 -1 */
    {&sm_isa_ca,	&sm_cd,		 0, NORM, loc+ 98,    0, pv+315, 178,    0},
/*307: mpu* at isa0 port 0x300 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&mpu_isa_ca,	&mpu_cd,	 0, STAR, loc+ 91,    0, pv+315, 178,    0},
/*308: sb0 at isa0 port 0x220 size 0 iomem -1 iosiz 0 irq 5 drq 1 drq2 -1 */
    {&sb_isa_ca,	&sb_cd,		 0, NORM, loc+ 84,    0, pv+315, 178,    0},
/*309: pas0 at isa0 port 0x220 size 0 iomem -1 iosiz 0 irq 7 drq 1 drq2 -1 */
    {&pas_ca,		&pas_cd,	 0, NORM, loc+ 77,    0, pv+315, 178,    0},
/*310: gus0 at isa0 port 0x220 size 0 iomem -1 iosiz 0 irq 7 drq 1 drq2 6 */
    {&gus_isa_ca,	&gus_cd,	 0, NORM, loc+ 70,    0, pv+315, 178,    0},
/*311: pcppi0 at isa0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&pcppi_ca,		&pcppi_cd,	 0, NORM, loc+ 63,    0, pv+315, 178,    0},
/*312: spkr0 at pcppi0 */
    {&spkr_ca,		&spkr_cd,	 0, NORM,     loc,    0, pv+295, 189,    0},
/*313: lpt0 at isa0 port 0x378 size 0 iomem -1 iosiz 0 irq 7 drq -1 drq2 -1 */
    {&lpt_isa_ca,	&lpt_cd,	 0, NORM, loc+ 56,    0, pv+315, 178,    0},
/*314: lpt1 at isa0 port 0x278 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&lpt_isa_ca,	&lpt_cd,	 1, NORM, loc+ 49,    0, pv+315, 178,    1},
/*315: lpt2 at isa0 port 0x3bc size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&lpt_isa_ca,	&lpt_cd,	 2, NORM, loc+ 42,    0, pv+315, 178,    2},
/*316: wbsio* at isa0 port 0x2e size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&wbsio_ca,		&wbsio_cd,	 0, STAR, loc+ 14,    0, pv+315, 178,    0},
/*317: wbsio* at isa0 port 0x4e size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&wbsio_ca,		&wbsio_cd,	 0, STAR, loc+  7,    0, pv+315, 178,    0},
/*318: schsio* at isa0 port 0x2e size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&schsio_ca,	&schsio_cd,	 0, STAR, loc+ 14,    0, pv+315, 178,    0},
/*319: schsio* at isa0 port 0x4e size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&schsio_ca,	&schsio_cd,	 0, STAR, loc+  7,    0, pv+315, 178,    0},
/*320: schsio* at isa0 port 0x162e size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&schsio_ca,	&schsio_cd,	 0, STAR, loc+ 35,    0, pv+315, 178,    0},
/*321: schsio* at isa0 port 0x164e size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&schsio_ca,	&schsio_cd,	 0, STAR, loc+ 28,    0, pv+315, 178,    0},
/*322: lm0 at isa0 port 0x290 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&lm_isa_ca,	&lm_cd,		 0, NORM, loc+ 21,    0, pv+315, 178,    0},
/*323: lm* at wbsio*|wbsio* */
    {&lm_wbsio_ca,	&lm_cd,		 1, STAR,     loc,    0, pv+264, 189,    1},
/*324: lm* at iic* addr -1 size -1 */
    {&lm_i2c_ca,	&lm_cd,		 1, STAR, loc+346,    0, pv+273, 190,    1},
/*325: fins0 at isa0 port 0x4e size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&fins_ca,		&fins_cd,	 0, NORM, loc+  7,    0, pv+315, 178,    0},
/*326: nsclpcsio* at isa0 port 0x2e size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&nsclpcsio_isa_ca,	&nsclpcsio_cd,	 0, STAR, loc+ 14,    0, pv+315, 178,    0},
/*327: nsclpcsio* at isa0 port 0x4e size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&nsclpcsio_isa_ca,	&nsclpcsio_cd,	 0, STAR, loc+  7,    0, pv+315, 178,    0},
/*328: gscsio* at isa0 port 0x2e size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&gscsio_ca,	&gscsio_cd,	 0, STAR, loc+ 14,    0, pv+315, 178,    0},
/*329: gscsio* at isa0 port 0x15c size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&gscsio_ca,	&gscsio_cd,	 0, STAR, loc+280,    0, pv+315, 178,    0},
/*330: it* at isa0 port 0x2e size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&it_ca,		&it_cd,		 0, STAR, loc+ 14,    0, pv+315, 178,    0},
/*331: it* at isa0 port 0x4e size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&it_ca,		&it_cd,		 0, STAR, loc+  7,    0, pv+315, 178,    0},
/*332: viasio* at isa0 port 0x2e size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&viasio_ca,	&viasio_cd,	 0, STAR, loc+ 14,    0, pv+315, 178,    0},
/*333: viasio* at isa0 port 0x4e size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&viasio_ca,	&viasio_cd,	 0, STAR, loc+  7,    0, pv+315, 178,    0},
/*334: uguru0 at isa0 port 0xe0 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&uguru_ca,		&uguru_cd,	 0, DNRM, loc+329,    0, pv+315, 178,    0},
/*335: aps0 at isa0 port 0x1600 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&aps_ca,		&aps_cd,	 0, NORM, loc+315,    0, pv+315, 178,    0},
/*336: npx0 at isa0 port 0xf0 size 0 iomem -1 iosiz 0 irq 0xd drq -1 drq2 -1 */
    {&npx_ca,		&npx_cd,	 0, NORM, loc+308,    0, pv+315, 178,    0},
/*337: wsdisplay* at udl* console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 1, STAR, loc+351,    0, pv+321, 193,    1},
/*338: wsdisplay* at inteldrm*|radeondrm* console -1 primary -1 mux -1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 1, STAR, loc+339,    0, pv+255, 1,    1},
/*339: wsdisplay0 at vga0|vga*|pcdisplay0 console 1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, NORM, loc+354,    0, pv+237, 197,    0},
/*340: wsdisplay0 at inteldrm*|radeondrm* console -1 primary 1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, NORM, loc+357,    0, pv+255, 1,    0},
/*341: wskbd* at ukbd*|ucc*|pckbd* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+355,    0, pv+241, 209,    0},
/*342: wsmouse* at ubcmtp*|ums*|uts*|uwacom*|pms* mux 0 */
    {&wsmouse_ca,	&wsmouse_cd,	 0, STAR, loc+350,    0, pv+227, 218,    0},
/*343: pckbd* at pckbc0 slot -1 */
    {&pckbd_ca,		&pckbd_cd,	 0, STAR, loc+347,    0, pv+287, 228,    0},
/*344: pms* at pckbc0 slot -1 */
    {&pms_ca,		&pms_cd,	 0, STAR, loc+347,    0, pv+287, 228,    0},
/*345: skgpio0 at isa0 port 0x680 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&skgpio_ca,	&skgpio_cd,	 0, NORM, loc+287,    0, pv+315, 178,    0},
/*346: eisa0 at mainbus0 */
    {&eisa_ca,		&eisa_cd,	 0, NORM,     loc,    0, pv+277, 152,    0},
/*347: ahc* at eisa0 slot -1 */
    {&ahc_eisa_ca,	&ahc_cd,	 0, STAR, loc+347,    0, pv+279, 161,    0},
/*348: uha* at eisa0 slot -1 */
    {&uha_eisa_ca,	&uha_cd,	 2, STAR, loc+347,    0, pv+279, 161,    2},
/*349: isapnp0 at isa0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&isapnp_ca,	&isapnp_cd,	 0, NORM, loc+ 63,    0, pv+315, 178,    0},
/*350: mpu* at isapnp0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 */
    {&mpu_isapnp_ca,	&mpu_cd,	 0, STAR, loc+342,    0, pv+317, 171,    0},
/*351: wdc* at isapnp0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 */
    {&wdc_isapnp_ca,	&wdc_cd,	 2, STAR, loc+342,    0, pv+317, 171,    2},
/*352: aic* at isapnp0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 */
    {&aic_isapnp_ca,	&aic_cd,	 1, STAR, loc+342,    0, pv+317, 171,    1},
/*353: sb* at isapnp0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 */
    {&sb_isapnp_ca,	&sb_cd,		 1, STAR, loc+342,    0, pv+317, 171,    1},
/*354: ess* at isapnp0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 */
    {&ess_isapnp_ca,	&ess_cd,	 0, STAR, loc+342,    0, pv+317, 171,    0},
/*355: gus* at isapnp0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 */
    {&gus_isapnp_ca,	&gus_cd,	 1, STAR, loc+342,    0, pv+317, 171,    1},
/*356: le* at isapnp0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 */
    {&le_isapnp_ca,	&le_cd,		 1, STAR, loc+342,    0, pv+317, 171,    1},
/*357: ep* at isapnp0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 */
    {&ep_isapnp_ca,	&ep_cd,		 1, STAR, loc+342,    0, pv+317, 171,    1},
/*358: ef* at isapnp0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 */
    {&ef_isapnp_ca,	&ef_cd,		 0, STAR, loc+342,    0, pv+317, 171,    0},
/*359: ne* at isapnp0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 */
    {&ne_isapnp_ca,	&ne_cd,		 3, STAR, loc+342,    0, pv+317, 171,    3},
/*360: we* at isapnp0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 */
    {&we_isapnp_ca,	&we_cd,		 2, STAR, loc+342,    0, pv+317, 171,    2},
/*361: joy* at isapnp0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 */
    {&joy_isapnp_ca,	&joy_cd,	 0, STAR, loc+342,    0, pv+317, 171,    0},
/*362: bios0 at mainbus0 apid -1 */
    {&bios_ca,		&bios_cd,	 0, NORM, loc+347,    0, pv+277, 152,    0},
/*363: apm0 at bios0 */
    {&apm_ca,		&apm_cd,	 0, NORM,     loc,    0, pv+293, 229,    0},
/*364: mpbios0 at bios0 */
    {&mpbios_ca,	&mpbios_cd,	 0, NORM,     loc,    0, pv+293, 229,    0},
/*365: cpu0 at mainbus0 apid -1 */
    {&cpu_ca,		&cpu_cd,	 0, NORM, loc+347,    0, pv+277, 152,    0},
/*366: ioapic* at mainbus0 apid -1 */
    {&ioapic_ca,	&ioapic_cd,	 0, STAR, loc+347,    0, pv+277, 152,    0},
/*367: pcibios0 at bios0 */
    {&pcibios_ca,	&pcibios_cd,	 0, NORM,     loc,    0, pv+293, 229,    0},
/*368: cardslot* at cbb* slot -1 */
    {&cardslot_ca,	&cardslot_cd,	 0, STAR, loc+347,    0, pv+289, 230,    0},
/*369: cardbus* at cardslot* slot -1 */
    {&cardbus_ca,	&cardbus_cd,	 0, STAR, loc+347,    0, pv+188, 232,    0},
/*370: com* at cardbus* dev -1 function -1 */
    {&com_cardbus_ca,	&com_cd,	 4, STAR, loc+346,    0, pv+269, 166,    4},
/*371: pcic0 at isa0 port 0x3e0 size 0 iomem 0xd0000 iosiz 0x10000 irq -1 drq -1 drq2 -1 */
    {&pcic_isa_ca,	&pcic_cd,	 0, NORM, loc+  0,    0, pv+315, 178,    0},
/*372: pcic1 at isa0 port 0x3e2 size 0 iomem 0xe0000 iosiz 0x4000 irq -1 drq -1 drq2 -1 */
    {&pcic_isa_ca,	&pcic_cd,	 1, NORM, loc+322,    0, pv+315, 178,    1},
/*373: pcic2 at isa0 port 0x3e4 size 0 iomem 0xe0000 iosiz 0x4000 irq -1 drq -1 drq2 -1 */
    {&pcic_isa_ca,	&pcic_cd,	 2, NORM, loc+301,    0, pv+315, 178,    2},
/*374: pcic* at pci* dev -1 function -1 */
    {&pcic_pci_ca,	&pcic_cd,	 3, STAR, loc+346,    0, pv+267, 158,    3},
/*375: pcic* at isapnp0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 */
    {&pcic_isapnp_ca,	&pcic_cd,	 3, STAR, loc+342,    0, pv+317, 171,    3},
/*376: tcic0 at isa0 port 0x240 size 0 iomem 0xd0000 iosiz 0x10000 irq -1 drq -1 drq2 -1 */
    {&tcic_isa_ca,	&tcic_cd,	 0, DNRM, loc+294,    0, pv+315, 178,    0},
/*377: pcmcia* at pcic0|pcic1|pcic2|pcic*|pcic*|tcic0|cardslot* controller -1 socket -1 */
    {&pcmcia_ca,	&pcmcia_cd,	 0, STAR, loc+346,    0, pv+182, 234,    0},
/*378: aic* at pcmcia* function -1 irq -1 */
    {&aic_pcmcia_ca,	&aic_cd,	 1, STAR, loc+346,    0, pv+271, 163,    1},
/*379: wdc* at pcmcia* function -1 irq -1 */
    {&wdc_pcmcia_ca,	&wdc_cd,	 2, STAR, loc+346,    0, pv+271, 163,    2},
/*380: sm* at pcmcia* function -1 irq -1 */
    {&sm_pcmcia_ca,	&sm_cd,		 1, STAR, loc+346,    0, pv+271, 163,    1},
/*381: xe* at pcmcia* function -1 irq -1 */
    {&xe_pcmcia_ca,	&xe_cd,		 0, STAR, loc+346,    0, pv+271, 163,    0},
/*382: usb* at xhci*|ehci*|ehci*|uhci*|uhci*|ohci*|ohci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+190, 251,    0},
/*383: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+323, 251,    0},
/*384: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*385: uaudio* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uaudio_ca,	&uaudio_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*386: uvideo* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvideo_ca,	&uvideo_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*387: utvfu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&utvfu_ca,		&utvfu_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*388: udl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udl_ca,		&udl_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*389: umidi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umidi_ca,		&umidi_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*390: ucom* at umodem*|uvisor*|uvscom*|ubsa*|uftdi*|uplcom*|umct*|uslcom*|uscom*|ucrcom*|uark*|moscom*|umcs*|uipaq*|umsm*|uchcom*|ucycom*|uslhcom*|uticom* portno -1 */
    {&ucom_ca,		&ucom_cd,	 0, STAR, loc+347,    0, pv+141, 266,    0},
/*391: ugen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugen_ca,		&ugen_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*392: uhidev* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhidev_ca,	&uhidev_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*393: uhid* at uhidev* reportid -1 */
    {&uhid_ca,		&uhid_cd,	 0, STAR, loc+347,    0, pv+291, 304,    0},
/*394: fido* at uhidev* reportid -1 */
    {&fido_ca,		&fido_cd,	 0, STAR, loc+347,    0, pv+291, 304,    0},
/*395: ujoy* at uhidev* reportid -1 */
    {&ujoy_ca,		&ujoy_cd,	 0, STAR, loc+347,    0, pv+291, 304,    0},
/*396: ukbd* at uhidev* reportid -1 */
    {&ukbd_ca,		&ukbd_cd,	 0, STAR, loc+347,    0, pv+291, 304,    0},
/*397: ums* at uhidev* reportid -1 */
    {&ums_ca,		&ums_cd,	 0, STAR, loc+347,    0, pv+291, 304,    0},
/*398: uts* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uts_ca,		&uts_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*399: ubcmtp* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ubcmtp_ca,	&ubcmtp_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*400: ucycom* at uhidev* reportid -1 */
    {&ucycom_ca,	&ucycom_cd,	 0, STAR, loc+347,    0, pv+291, 304,    0},
/*401: uslhcom* at uhidev* reportid -1 */
    {&uslhcom_ca,	&uslhcom_cd,	 0, STAR, loc+347,    0, pv+291, 304,    0},
/*402: ulpt* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ulpt_ca,		&ulpt_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*403: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*404: uthum* at uhidev* reportid -1 */
    {&uthum_ca,		&uthum_cd,	 0, STAR, loc+347,    0, pv+291, 304,    0},
/*405: ugold* at uhidev* reportid -1 */
    {&ugold_ca,		&ugold_cd,	 0, STAR, loc+347,    0, pv+291, 304,    0},
/*406: utrh* at uhidev* reportid -1 */
    {&utrh_ca,		&utrh_cd,	 0, STAR, loc+347,    0, pv+291, 304,    0},
/*407: uoakrh* at uhidev* reportid -1 */
    {&uoakrh_ca,	&uoakrh_cd,	 0, STAR, loc+347,    0, pv+291, 304,    0},
/*408: uoaklux* at uhidev* reportid -1 */
    {&uoaklux_ca,	&uoaklux_cd,	 0, STAR, loc+347,    0, pv+291, 304,    0},
/*409: uoakv* at uhidev* reportid -1 */
    {&uoakv_ca,		&uoakv_cd,	 0, STAR, loc+347,    0, pv+291, 304,    0},
/*410: uonerng* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uonerng_ca,	&uonerng_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*411: urng* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urng_ca,		&urng_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*412: udcf* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udcf_ca,		&udcf_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*413: umbg* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umbg_ca,		&umbg_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*414: uvisor* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvisor_ca,	&uvisor_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*415: udsbr* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udsbr_ca,		&udsbr_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*416: utwitch* at uhidev* reportid -1 */
    {&utwitch_ca,	&utwitch_cd,	 0, STAR, loc+347,    0, pv+291, 304,    0},
/*417: aue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&aue_ca,		&aue_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*418: axe* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axe_ca,		&axe_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*419: axen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axen_ca,		&axen_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*420: smsc* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&smsc_ca,		&smsc_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*421: cue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cue_ca,		&cue_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*422: kue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&kue_ca,		&kue_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*423: cdce* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cdce_ca,		&cdce_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*424: urndis* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urndis_ca,	&urndis_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*425: mos* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mos_ca,		&mos_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*426: mue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mue_ca,		&mue_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*427: udav* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udav_ca,		&udav_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*428: upl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upl_ca,		&upl_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*429: ugl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugl_ca,		&ugl_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*430: url* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&url_ca,		&url_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*431: ure* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ure_ca,		&ure_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*432: umodem* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umodem_ca,	&umodem_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*433: uftdi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uftdi_ca,		&uftdi_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*434: uplcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uplcom_ca,	&uplcom_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*435: umct* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umct_ca,		&umct_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*436: uvscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvscom_ca,	&uvscom_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*437: ubsa* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ubsa_ca,		&ubsa_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*438: uslcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uslcom_ca,	&uslcom_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*439: uark* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uark_ca,		&uark_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*440: moscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&moscom_ca,	&moscom_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*441: umcs* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umcs_ca,		&umcs_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*442: uscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uscom_ca,		&uscom_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*443: ucrcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ucrcom_ca,	&ucrcom_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*444: uipaq* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uipaq_ca,		&uipaq_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*445: umsm* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umsm_ca,		&umsm_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*446: uchcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uchcom_ca,	&uchcom_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*447: uticom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uticom_ca,	&uticom_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*448: wi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&wi_usb_ca,	&wi_cd,		 0, STAR, loc+336,    0, pv+249, 252,    0},
/*449: atu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&atu_ca,		&atu_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*450: ural* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ural_ca,		&ural_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*451: rum* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rum_ca,		&rum_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*452: run* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&run_ca,		&run_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*453: zyd* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&zyd_ca,		&zyd_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*454: upgt* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upgt_ca,		&upgt_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*455: urtw* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtw_ca,		&urtw_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*456: urtwn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtwn_ca,		&urtwn_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*457: rsu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rsu_ca,		&rsu_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*458: otus* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&otus_ca,		&otus_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*459: umb* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umb_ca,		&umb_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*460: uath* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uath_ca,		&uath_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*461: athn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&athn_usb_ca,	&athn_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*462: uow* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uow_ca,		&uow_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*463: uberry* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uberry_ca,	&uberry_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*464: upd* at uhidev* reportid -1 */
    {&upd_ca,		&upd_cd,	 0, STAR, loc+347,    0, pv+291, 304,    0},
/*465: uwacom* at uhidev* reportid -1 */
    {&uwacom_ca,	&uwacom_cd,	 0, STAR, loc+347,    0, pv+291, 304,    0},
/*466: bwfm* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&bwfm_usb_ca,	&bwfm_cd,	 0, STAR, loc+336,    0, pv+249, 252,    0},
/*467: bwfm* at sdmmc* */
    {&bwfm_sdio_ca,	&bwfm_cd,	 0, STAR,     loc,    0, pv+299, 136,    0},
/*468: uhidpp* at uhidev* reportid -1 */
    {&uhidpp_ca,	&uhidpp_cd,	 0, STAR, loc+347,    0, pv+291, 304,    0},
/*469: ucc* at uhidev* reportid -1 */
    {&ucc_ca,		&ucc_cd,	 0, STAR, loc+347,    0, pv+291, 304,    0},
/*470: iic* at glxpcib*|gscsio*|gscsio*|piixpm*|alipm*|ichiic*|viapm*|amdiic*|nviic*|amdpm*|gpioiic* */
    {&iic_ca,		&iic_cd,	 0, STAR,     loc,    0, pv+161, 177,    0},
/*471: lmtemp* at iic* addr -1 size -1 */
    {&lmtemp_ca,	&lmtemp_cd,	 0, STAR, loc+346,    0, pv+273, 190,    0},
/*472: lmn* at iic* addr -1 size -1 */
    {&lmn_ca,		&lmn_cd,	 0, STAR, loc+346,    0, pv+273, 190,    0},
/*473: lmenv* at iic* addr -1 size -1 */
    {&lmenv_ca,		&lmenv_cd,	 0, STAR, loc+346,    0, pv+273, 190,    0},
/*474: maxtmp* at iic* addr -1 size -1 */
    {&maxtmp_ca,	&maxtmp_cd,	 0, STAR, loc+346,    0, pv+273, 190,    0},
/*475: adc* at iic* addr -1 size -1 */
    {&adc_ca,		&adc_cd,	 0, STAR, loc+346,    0, pv+273, 190,    0},
/*476: admtemp* at iic* addr -1 size -1 */
    {&admtemp_ca,	&admtemp_cd,	 0, STAR, loc+346,    0, pv+273, 190,    0},
/*477: admlc* at iic* addr -1 size -1 */
    {&admlc_ca,		&admlc_cd,	 0, STAR, loc+346,    0, pv+273, 190,    0},
/*478: admtm* at iic* addr -1 size -1 */
    {&admtm_ca,		&admtm_cd,	 0, STAR, loc+346,    0, pv+273, 190,    0},
/*479: admtmp* at iic* addr -1 size -1 */
    {&admtmp_ca,	&admtmp_cd,	 0, STAR, loc+346,    0, pv+273, 190,    0},
/*480: admtt* at iic* addr -1 size -1 */
    {&admtt_ca,		&admtt_cd,	 0, STAR, loc+346,    0, pv+273, 190,    0},
/*481: maxds* at iic* addr -1 size -1 */
    {&maxds_ca,		&maxds_cd,	 0, STAR, loc+346,    0, pv+273, 190,    0},
/*482: adt* at iic* addr -1 size -1 */
    {&adt_ca,		&adt_cd,	 0, STAR, loc+346,    0, pv+273, 190,    0},
/*483: admcts* at iic* addr -1 size -1 */
    {&admcts_ca,	&admcts_cd,	 0, STAR, loc+346,    0, pv+273, 190,    0},
/*484: asbtm* at iic* addr -1 size -1 */
    {&asbtm_ca,		&asbtm_cd,	 0, STAR, loc+346,    0, pv+273, 190,    0},
/*485: wbenv* at iic* addr -1 size -1 */
    {&wbenv_ca,		&wbenv_cd,	 0, STAR, loc+346,    0, pv+273, 190,    0},
/*486: wbng* at iic* addr -1 size -1 */
    {&wbng_ca,		&wbng_cd,	 0, STAR, loc+346,    0, pv+273, 190,    0},
/*487: nvt* at iic* addr -1 size -1 */
    {&nvt_ca,		&nvt_cd,	 0, STAR, loc+346,    0, pv+273, 190,    0},
/*488: glenv* at iic* addr -1 size -1 */
    {&glenv_ca,		&glenv_cd,	 0, STAR, loc+346,    0, pv+273, 190,    0},
/*489: adl* at iic* addr -1 size -1 */
    {&adl_ca,		&adl_cd,	 0, STAR, loc+346,    0, pv+273, 190,    0},
/*490: andl* at iic* addr -1 size -1 */
    {&andl_ca,		&andl_cd,	 0, STAR, loc+346,    0, pv+273, 190,    0},
/*491: thmc* at iic* addr -1 size -1 */
    {&thmc_ca,		&thmc_cd,	 0, STAR, loc+346,    0, pv+273, 190,    0},
/*492: spdmem* at iic* addr -1 size -1 */
    {&spdmem_iic_ca,	&spdmem_cd,	 0, STAR, loc+346,    0, pv+273, 190,    0},
/*493: sdtemp* at iic* addr -1 size -1 */
    {&sdtemp_ca,	&sdtemp_cd,	 0, STAR, loc+346,    0, pv+273, 190,    0},
/*494: lisa* at iic* addr -1 size -1 */
    {&lisa_ca,		&lisa_cd,	 0, STAR, loc+346,    0, pv+273, 190,    0},
/*495: gpio* at gscpcib*|glxpcib*|elansc*|nsclpcsio*|nsclpcsio*|skgpio0 */
    {&gpio_ca,		&gpio_cd,	 0, STAR,     loc,    0, pv+206, 177,    0},
/*496: gpioiic* at gpio* offset -1 mask 0 flag 0 */
    {&gpioiic_ca,	&gpioiic_cd,	 0, STAR, loc+348,    0, pv+319, 306,    0},
/*497: gpioow* at gpio* offset -1 mask 0 flag 0 */
    {&gpioow_ca,	&gpioow_cd,	 0, STAR, loc+348,    0, pv+319, 306,    0},
/*498: onewire* at uow*|gpioow* */
    {&onewire_ca,	&onewire_cd,	 0, STAR,     loc,    0, pv+261, 309,    0},
/*499: owid* at onewire* */
    {&owid_ca,		&owid_cd,	 0, STAR,     loc,    0, pv+325, 309,    0},
/*500: owsbm* at onewire* */
    {&owsbm_ca,		&owsbm_cd,	 0, STAR,     loc,    0, pv+325, 309,    0},
/*501: owtemp* at onewire* */
    {&owtemp_ca,	&owtemp_cd,	 0, STAR,     loc,    0, pv+325, 309,    0},
/*502: owctr* at onewire* */
    {&owctr_ca,		&owctr_cd,	 0, STAR,     loc,    0, pv+325, 309,    0},
/*503: sdmmc* at sdhc*|rtsx* */
    {&sdmmc_ca,		&sdmmc_cd,	 0, STAR,     loc,    0, pv+258, 309,    0},
/*504: acpitimer* at acpi0 */
    {&acpitimer_ca,	&acpitimer_cd,	 0, STAR,     loc,    0, pv+297, 309,    0},
/*505: acpiac* at acpi0 */
    {&acpiac_ca,	&acpiac_cd,	 0, STAR,     loc,    0, pv+297, 309,    0},
/*506: acpibat* at acpi0 */
    {&acpibat_ca,	&acpibat_cd,	 0, STAR,     loc,    0, pv+297, 309,    0},
/*507: acpibtn* at acpi0 */
    {&acpibtn_ca,	&acpibtn_cd,	 0, STAR,     loc,    0, pv+297, 309,    0},
/*508: acpicmos* at acpi0 */
    {&acpicmos_ca,	&acpicmos_cd,	 0, STAR,     loc,    0, pv+297, 309,    0},
/*509: acpicpu* at acpi0 */
    {&acpicpu_ca,	&acpicpu_cd,	 0, STAR,     loc,    0, pv+297, 309,    0},
/*510: acpihpet* at acpi0 */
    {&acpihpet_ca,	&acpihpet_cd,	 0, STAR,     loc,    0, pv+297, 309,    0},
/*511: acpiec* at acpi0 */
    {&acpiec_ca,	&acpiec_cd,	 0, STAR,     loc,    0, pv+297, 309,    0},
/*512: acpitz* at acpi0 */
    {&acpitz_ca,	&acpitz_cd,	 0, STAR,     loc,    0, pv+297, 309,    0},
/*513: acpimadt0 at acpi0 */
    {&acpimadt_ca,	&acpimadt_cd,	 0, NORM,     loc,    0, pv+297, 309,    0},
/*514: acpimcfg* at acpi0 */
    {&acpimcfg_ca,	&acpimcfg_cd,	 0, STAR,     loc,    0, pv+297, 309,    0},
/*515: acpiprt* at acpi0 */
    {&acpiprt_ca,	&acpiprt_cd,	 0, STAR,     loc,    0, pv+297, 309,    0},
/*516: acpidock* at acpi0 */
    {&acpidock_ca,	&acpidock_cd,	 0, STAR,     loc,    0, pv+297, 309,    0},
/*517: asmc* at acpi0 */
    {&asmc_ca,		&asmc_cd,	 0, STAR,     loc,    0, pv+297, 309,    0},
/*518: acpiasus* at acpi0 */
    {&acpiasus_ca,	&acpiasus_cd,	 0, STAR,     loc,    0, pv+297, 309,    0},
/*519: acpithinkpad* at acpi0 */
    {&acpithinkpad_ca,	&acpithinkpad_cd,	 0, STAR,     loc,    0, pv+297, 309,    0},
/*520: acpitoshiba* at acpi0 */
    {&acpitoshiba_ca,	&acpitoshiba_cd,	 0, STAR,     loc,    0, pv+297, 309,    0},
/*521: acpisony* at acpi0 */
    {&acpisony_ca,	&acpisony_cd,	 0, STAR,     loc,    0, pv+297, 309,    0},
/*522: acpivideo* at acpi0 */
    {&acpivideo_ca,	&acpivideo_cd,	 0, STAR,     loc,    0, pv+297, 309,    0},
/*523: acpivout* at acpivideo* */
    {&acpivout_ca,	&acpivout_cd,	 0, STAR,     loc,    0, pv+327, 309,    0},
/*524: acpipwrres* at acpi0 */
    {&acpipwrres_ca,	&acpipwrres_cd,	 0, STAR,     loc,    0, pv+297, 309,    0},
/*525: aibs* at acpi0 */
    {&aibs_ca,		&aibs_cd,	 0, STAR,     loc,    0, pv+297, 309,    0},
/*526: acpisbs* at acpi0 */
    {&acpisbs_ca,	&acpisbs_cd,	 0, STAR,     loc,    0, pv+297, 309,    0},
/*527: acpi0 at bios0 */
    {&acpi_ca,		&acpi_cd,	 0, NORM,     loc,    0, pv+293, 229,    0},
/*528: ipmi0 at mainbus0 apid -1 */
    {&ipmi_ca,		&ipmi_cd,	 0, DNRM, loc+347,    0, pv+277, 152,    0},
/*529: esm0 at mainbus0 apid -1 */
    {&esm_ca,		&esm_cd,	 0, NORM, loc+347,    0, pv+277, 152,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 6 /* vscsi0 */,
	 7 /* mpath0 */,
	 8 /* softraid0 */,
	56 /* mainbus0 */,
	-1
};

int cfroots_size = 5;

/* pseudo-devices */
extern void pfattach(int);
extern void pflogattach(int);
extern void pfsyncattach(int);
extern void pflowattach(int);
extern void encattach(int);
extern void ptyattach(int);
extern void nmeaattach(int);
extern void mstsattach(int);
extern void endrunattach(int);
extern void vndattach(int);
extern void ksymsattach(int);
extern void bpfilterattach(int);
extern void bridgeattach(int);
extern void vebattach(int);
extern void carpattach(int);
extern void etheripattach(int);
extern void gifattach(int);
extern void greattach(int);
extern void loopattach(int);
extern void mpeattach(int);
extern void mpwattach(int);
extern void mpipattach(int);
extern void bpeattach(int);
extern void pairattach(int);
extern void pppattach(int);
extern void pppoeattach(int);
extern void pppxattach(int);
extern void spppattach(int);
extern void trunkattach(int);
extern void aggrattach(int);
extern void tpmrattach(int);
extern void tunattach(int);
extern void vetherattach(int);
extern void vxlanattach(int);
extern void vlanattach(int);
extern void switchattach(int);
extern void wgattach(int);
extern void bioattach(int);
extern void fuseattach(int);
extern void pctrattach(int);
extern void nvramattach(int);
extern void hotplugattach(int);
extern void dtattach(int);
extern void wsmuxattach(int);

char *pdevnames[] = {
	"pf",
	"pflog",
	"pfsync",
	"pflow",
	"enc",
	"pty",
	"nmea",
	"msts",
	"endrun",
	"vnd",
	"ksyms",
	"bpfilter",
	"bridge",
	"veb",
	"carp",
	"etherip",
	"gif",
	"gre",
	"loop",
	"mpe",
	"mpw",
	"mpip",
	"bpe",
	"pair",
	"ppp",
	"pppoe",
	"pppx",
	"sppp",
	"trunk",
	"aggr",
	"tpmr",
	"tun",
	"vether",
	"vxlan",
	"vlan",
	"switch",
	"wg",
	"bio",
	"fuse",
	"pctr",
	"nvram",
	"hotplug",
	"dt",
	"wsmux",
};

int pdevnames_size = 44;

struct pdevinit pdevinit[] = {
	{ pfattach, 1 },
	{ pflogattach, 1 },
	{ pfsyncattach, 1 },
	{ pflowattach, 1 },
	{ encattach, 1 },
	{ ptyattach, 16 },
	{ nmeaattach, 1 },
	{ mstsattach, 1 },
	{ endrunattach, 1 },
	{ vndattach, 4 },
	{ ksymsattach, 1 },
	{ bpfilterattach, 1 },
	{ bridgeattach, 1 },
	{ vebattach, 1 },
	{ carpattach, 1 },
	{ etheripattach, 1 },
	{ gifattach, 1 },
	{ greattach, 1 },
	{ loopattach, 1 },
	{ mpeattach, 1 },
	{ mpwattach, 1 },
	{ mpipattach, 1 },
	{ bpeattach, 1 },
	{ pairattach, 1 },
	{ pppattach, 1 },
	{ pppoeattach, 1 },
	{ pppxattach, 1 },
	{ spppattach, 1 },
	{ trunkattach, 1 },
	{ aggrattach, 1 },
	{ tpmrattach, 1 },
	{ tunattach, 1 },
	{ vetherattach, 1 },
	{ vxlanattach, 1 },
	{ vlanattach, 1 },
	{ switchattach, 1 },
	{ wgattach, 1 },
	{ bioattach, 1 },
	{ fuseattach, 1 },
	{ pctrattach, 1 },
	{ nvramattach, 1 },
	{ hotplugattach, 1 },
	{ dtattach, 1 },
	{ wsmuxattach, 2 },
	{ NULL, 0 }
};
