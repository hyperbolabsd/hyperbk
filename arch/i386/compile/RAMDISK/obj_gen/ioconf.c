/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/i386/conf/RAMDISK"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver vga_cd;
extern struct cfdriver wdc_cd;
extern struct cfdriver mpi_cd;
extern struct cfdriver rl_cd;
extern struct cfdriver re_cd;
extern struct cfdriver ne_cd;
extern struct cfdriver com_cd;
extern struct cfdriver pckbc_cd;
extern struct cfdriver uhci_cd;
extern struct cfdriver ohci_cd;
extern struct cfdriver ehci_cd;
extern struct cfdriver nsphy_cd;
extern struct cfdriver nsphyter_cd;
extern struct cfdriver inphy_cd;
extern struct cfdriver iophy_cd;
extern struct cfdriver rlphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver bmtphy_cd;
extern struct cfdriver rgephy_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver wd_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver pciide_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver em_cd;
extern struct cfdriver pchb_cd;
extern struct cfdriver geodesc_cd;
extern struct cfdriver pcib_cd;
extern struct cfdriver fd_cd;
extern struct cfdriver isa_cd;
extern struct cfdriver isadma_cd;
extern struct cfdriver fdc_cd;
extern struct cfdriver pcdisplay_cd;
extern struct cfdriver npx_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver pckbd_cd;
extern struct cfdriver bios_cd;
extern struct cfdriver mpbios_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver ioapic_cd;
extern struct cfdriver pcibios_cd;
extern struct cfdriver pcic_cd;
extern struct cfdriver pcmcia_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver uhidev_cd;
extern struct cfdriver ukbd_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver acpi_cd;
extern struct cfdriver acpicmos_cd;
extern struct cfdriver acpihpet_cd;
extern struct cfdriver acpiec_cd;
extern struct cfdriver acpimadt_cd;
extern struct cfdriver acpiprt_cd;

extern struct cfattach nsphy_ca;
extern struct cfattach nsphyter_ca;
extern struct cfattach inphy_ca;
extern struct cfattach iophy_ca;
extern struct cfattach rlphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach bmtphy_ca;
extern struct cfattach rgephy_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach sd_ca;
extern struct cfattach wd_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach pci_ca;
extern struct cfattach vga_pci_ca;
extern struct cfattach mpi_pci_ca;
extern struct cfattach pciide_pci_ca;
extern struct cfattach ppb_ca;
extern struct cfattach rl_pci_ca;
extern struct cfattach re_pci_ca;
extern struct cfattach em_ca;
extern struct cfattach ne_pci_ca;
extern struct cfattach uhci_pci_ca;
extern struct cfattach ohci_pci_ca;
extern struct cfattach ehci_pci_ca;
extern struct cfattach pchb_ca;
extern struct cfattach geodesc_ca;
extern struct cfattach pcib_ca;
extern struct cfattach isa_ca;
extern struct cfattach isadma_ca;
extern struct cfattach fdc_ca;
extern struct cfattach fd_ca;
extern struct cfattach com_isa_ca;
extern struct cfattach pckbc_isa_ca;
extern struct cfattach vga_isa_ca;
extern struct cfattach pcdisplay_ca;
extern struct cfattach wdc_isa_ca;
extern struct cfattach npx_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach pckbd_ca;
extern struct cfattach bios_ca;
extern struct cfattach mpbios_ca;
extern struct cfattach cpu_ca;
extern struct cfattach ioapic_ca;
extern struct cfattach pcibios_ca;
extern struct cfattach pcic_isa_ca;
extern struct cfattach pcic_pci_ca;
extern struct cfattach pcmcia_ca;
extern struct cfattach ne_pcmcia_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhidev_ca;
extern struct cfattach ukbd_ca;
extern struct cfattach umass_ca;
extern struct cfattach acpicmos_ca;
extern struct cfattach acpihpet_ca;
extern struct cfattach acpiec_ca;
extern struct cfattach acpimadt_ca;
extern struct cfattach acpiprt_ca;
extern struct cfattach acpi_ca;


/* locators */
static long loc[86] = {
	-1, 0, -1, 0, -1, -1, -1, 0x1f0,
	0, -1, 0, 0xe, -1, -1, 0x170, 0,
	-1, 0, 0xf, -1, -1, 0xf0, 0, -1,
	0, 0xd, -1, -1, 0x3e4, 0, 0xd4000, 0x4000,
	-1, -1, -1, 0x3e0, 0, 0xd0000, 0x4000, -1,
	-1, -1, 0x3e2, 0, 0xd4000, 0x4000, -1, -1,
	-1, 0x3f0, 0, -1, 0, 6, 2, -1,
	0x3f8, 0, -1, 0, 4, -1, -1, 0x2f8,
	0, -1, 0, 3, -1, -1, 0x3e8, 0,
	-1, 0, 5, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, 1,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"phy",
	"target",
	"lun",
	"channel",
	"drive",
	"bus",
	"dev",
	"function",
	"irq",
	"port",
	"size",
	"iomem",
	"iosiz",
	"drq",
	"drq2",
	"console",
	"primary",
	"mux",
	"slot",
	"controller",
	"socket",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"reportid",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 1, 2, -1, 3, 4, -1, 3,
	4, -1, 3, 4, -1, 5, -1, 5,
	-1, 5, -1, 6, 7, -1, 7, 8,
	-1, 9, 10, 11, 12, 8, 13, 14,
	-1, 4, -1, 15, 16, 17, -1, 15,
	16, 17, -1, 15, 16, 17, -1, 15,
	17, -1, 15, 17, -1, 18, -1, 19,
	20, -1, 19, 20, -1, 19, 20, -1,
	19, 20, -1, 9, 21, 22, 23, 24,
	25, -1, 26, -1,
};

/* size of parent vectors */
int pv_size = 59;

/* parent vectors */
short pv[59] = {
	20, 21, 18, 17, -1, 49, 50, 51, 52, -1, 38, 39, 15, -1, 11, 16,
	25, -1, 36, 13, 37, -1, 24, 22, 23, -1, 57, 43, -1, 58, 14, -1,
	11, 27, -1, 12, -1, 53, -1, 8, -1, 30, -1, 35, -1, 28, -1, 11,
	-1, 56, -1, 44, -1, 54, -1, 55, -1, 64, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: nsphy* at ne*|ne*|re*|rl* phy -1 */
    {&nsphy_ca,		&nsphy_cd,	 0, STAR, loc+ 82,    0, pv+ 0, 1,    0},
/*  1: nsphyter* at ne*|ne*|re*|rl* phy -1 */
    {&nsphyter_ca,	&nsphyter_cd,	 0, STAR, loc+ 82,    0, pv+ 0, 1,    0},
/*  2: inphy* at ne*|ne*|re*|rl* phy -1 */
    {&inphy_ca,		&inphy_cd,	 0, STAR, loc+ 82,    0, pv+ 0, 1,    0},
/*  3: iophy* at ne*|ne*|re*|rl* phy -1 */
    {&iophy_ca,		&iophy_cd,	 0, STAR, loc+ 82,    0, pv+ 0, 1,    0},
/*  4: rlphy* at ne*|ne*|re*|rl* phy -1 */
    {&rlphy_ca,		&rlphy_cd,	 0, STAR, loc+ 82,    0, pv+ 0, 1,    0},
/*  5: ukphy* at ne*|ne*|re*|rl* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+ 82,    0, pv+ 0, 1,    0},
/*  6: bmtphy* at ne*|ne*|re*|rl* phy -1 */
    {&bmtphy_ca,	&bmtphy_cd,	 0, STAR, loc+ 82,    0, pv+ 0, 1,    0},
/*  7: rgephy* at ne*|ne*|re*|rl* phy -1 */
    {&rgephy_ca,	&rgephy_cd,	 0, STAR, loc+ 82,    0, pv+ 0, 1,    0},
/*  8: scsibus* at umass*|mpi* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+29, 8,    0},
/*  9: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+ 81,    0, pv+39, 9,    0},
/* 10: wd* at wdc0|wdc1|pciide* channel -1 drive -1 */
    {&wd_ca,		&wd_cd,		 0, STAR, loc+ 81,    0, pv+10, 12,    0},
/* 11: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+ 4, 0,    0},
/* 12: pci* at mainbus0|ppb*|pchb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+ 82,    0, pv+14, 21,    0},
/* 13: vga* at pci* dev -1 function -1 */
    {&vga_pci_ca,	&vga_cd,	 1, STAR, loc+ 81,    0, pv+35, 27,    1},
/* 14: mpi* at pci* dev -1 function -1 */
    {&mpi_pci_ca,	&mpi_cd,	 0, STAR, loc+ 81,    0, pv+35, 27,    0},
/* 15: pciide* at pci* dev -1 function -1 */
    {&pciide_pci_ca,	&pciide_cd,	 0, STAR, loc+ 81,    0, pv+35, 27,    0},
/* 16: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+ 81,    0, pv+35, 27,    0},
/* 17: rl* at pci* dev -1 function -1 */
    {&rl_pci_ca,	&rl_cd,		 0, STAR, loc+ 81,    0, pv+35, 27,    0},
/* 18: re* at pci* dev -1 function -1 */
    {&re_pci_ca,	&re_cd,		 0, STAR, loc+ 81,    0, pv+35, 27,    0},
/* 19: em* at pci* dev -1 function -1 */
    {&em_ca,		&em_cd,		 0, STAR, loc+ 81,    0, pv+35, 27,    0},
/* 20: ne* at pci* dev -1 function -1 */
    {&ne_pci_ca,	&ne_cd,		 0, STAR, loc+ 81,    0, pv+35, 27,    0},
/* 21: ne* at pcmcia* function -1 irq -1 */
    {&ne_pcmcia_ca,	&ne_cd,		 0, STAR, loc+ 81,    0, pv+37, 30,    0},
/* 22: uhci* at pci* dev -1 function -1 */
    {&uhci_pci_ca,	&uhci_cd,	 0, STAR, loc+ 81,    0, pv+35, 27,    0},
/* 23: ohci* at pci* dev -1 function -1 */
    {&ohci_pci_ca,	&ohci_cd,	 0, STAR, loc+ 81,    0, pv+35, 27,    0},
/* 24: ehci* at pci* dev -1 function -1 */
    {&ehci_pci_ca,	&ehci_cd,	 0, STAR, loc+ 81,    0, pv+35, 27,    0},
/* 25: pchb* at pci* dev -1 function -1 */
    {&pchb_ca,		&pchb_cd,	 0, STAR, loc+ 81,    0, pv+35, 27,    0},
/* 26: geodesc* at pci* dev -1 function -1 */
    {&geodesc_ca,	&geodesc_cd,	 0, STAR, loc+ 81,    0, pv+35, 27,    0},
/* 27: pcib* at pci* dev -1 function -1 */
    {&pcib_ca,		&pcib_cd,	 0, STAR, loc+ 81,    0, pv+35, 27,    0},
/* 28: isa0 at mainbus0|pcib* */
    {&isa_ca,		&isa_cd,	 0, NORM,     loc,    0, pv+32, 21,    0},
/* 29: isadma0 at isa0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&isadma_ca,	&isadma_cd,	 0, NORM, loc+  0,    0, pv+45, 33,    0},
/* 30: fdc0 at isa0 port 0x3f0 size 0 iomem -1 iosiz 0 irq 6 drq 2 drq2 -1 */
    {&fdc_ca,		&fdc_cd,	 0, NORM, loc+ 49,    0, pv+45, 33,    0},
/* 31: fd* at fdc0 drive -1 */
    {&fd_ca,		&fd_cd,		 0, STAR, loc+ 82,    0, pv+41, 41,    0},
/* 32: com0 at isa0 port 0x3f8 size 0 iomem -1 iosiz 0 irq 4 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 0, NORM, loc+ 56,    0, pv+45, 33,    0},
/* 33: com1 at isa0 port 0x2f8 size 0 iomem -1 iosiz 0 irq 3 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 1, NORM, loc+ 63,    0, pv+45, 33,    1},
/* 34: com2 at isa0 port 0x3e8 size 0 iomem -1 iosiz 0 irq 5 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 2, NORM, loc+ 70,    0, pv+45, 33,    2},
/* 35: pckbc0 at isa0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&pckbc_isa_ca,	&pckbc_cd,	 0, NORM, loc+  0,    0, pv+45, 33,    0},
/* 36: vga0 at isa0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&vga_isa_ca,	&vga_cd,	 0, NORM, loc+  0,    0, pv+45, 33,    0},
/* 37: pcdisplay0 at isa0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&pcdisplay_ca,	&pcdisplay_cd,	 0, NORM, loc+  0,    0, pv+45, 33,    0},
/* 38: wdc0 at isa0 port 0x1f0 size 0 iomem -1 iosiz 0 irq 0xe drq -1 drq2 -1 */
    {&wdc_isa_ca,	&wdc_cd,	 0, NORM, loc+  7,    0, pv+45, 33,    0},
/* 39: wdc1 at isa0 port 0x170 size 0 iomem -1 iosiz 0 irq 0xf drq -1 drq2 -1 */
    {&wdc_isa_ca,	&wdc_cd,	 1, NORM, loc+ 14,    0, pv+45, 33,    1},
/* 40: npx0 at isa0 port 0xf0 size 0 iomem -1 iosiz 0 irq 0xd drq -1 drq2 -1 */
    {&npx_ca,		&npx_cd,	 0, NORM, loc+ 21,    0, pv+45, 33,    0},
/* 41: wsdisplay* at vga0|vga*|pcdisplay0 console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+ 83,    0, pv+18, 43,    0},
/* 42: wskbd* at ukbd*|pckbd* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+ 84,    0, pv+26, 55,    0},
/* 43: pckbd* at pckbc0 slot -1 */
    {&pckbd_ca,		&pckbd_cd,	 0, STAR, loc+ 82,    0, pv+43, 61,    0},
/* 44: bios0 at mainbus0 apid -1 */
    {&bios_ca,		&bios_cd,	 0, NORM, loc+ 82,    0, pv+47, 21,    0},
/* 45: mpbios0 at bios0 */
    {&mpbios_ca,	&mpbios_cd,	 0, NORM,     loc,    0, pv+51, 62,    0},
/* 46: cpu0 at mainbus0 apid -1 */
    {&cpu_ca,		&cpu_cd,	 0, NORM, loc+ 82,    0, pv+47, 21,    0},
/* 47: ioapic* at mainbus0 apid -1 */
    {&ioapic_ca,	&ioapic_cd,	 0, STAR, loc+ 82,    0, pv+47, 21,    0},
/* 48: pcibios0 at bios0 */
    {&pcibios_ca,	&pcibios_cd,	 0, NORM,     loc,    0, pv+51, 62,    0},
/* 49: pcic0 at isa0 port 0x3e0 size 0 iomem 0xd0000 iosiz 0x4000 irq -1 drq -1 drq2 -1 */
    {&pcic_isa_ca,	&pcic_cd,	 0, NORM, loc+ 35,    0, pv+45, 33,    0},
/* 50: pcic1 at isa0 port 0x3e2 size 0 iomem 0xd4000 iosiz 0x4000 irq -1 drq -1 drq2 -1 */
    {&pcic_isa_ca,	&pcic_cd,	 1, NORM, loc+ 42,    0, pv+45, 33,    1},
/* 51: pcic2 at isa0 port 0x3e4 size 0 iomem 0xd4000 iosiz 0x4000 irq -1 drq -1 drq2 -1 */
    {&pcic_isa_ca,	&pcic_cd,	 2, NORM, loc+ 28,    0, pv+45, 33,    2},
/* 52: pcic* at pci* dev -1 function -1 */
    {&pcic_pci_ca,	&pcic_cd,	 3, STAR, loc+ 81,    0, pv+35, 27,    3},
/* 53: pcmcia* at pcic0|pcic1|pcic2|pcic* controller -1 socket -1 */
    {&pcmcia_ca,	&pcmcia_cd,	 0, STAR, loc+ 81,    0, pv+ 5, 63,    0},
/* 54: usb* at ehci*|uhci*|ohci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+22, 74,    0},
/* 55: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+53, 74,    0},
/* 56: uhidev* at uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhidev_ca,	&uhidev_cd,	 0, STAR, loc+ 77,    0, pv+55, 75,    0},
/* 57: ukbd* at uhidev* reportid -1 */
    {&ukbd_ca,		&ukbd_cd,	 0, STAR, loc+ 82,    0, pv+49, 82,    0},
/* 58: umass* at uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+ 77,    0, pv+55, 75,    0},
/* 59: acpicmos* at acpi0 */
    {&acpicmos_ca,	&acpicmos_cd,	 0, STAR,     loc,    0, pv+57, 83,    0},
/* 60: acpihpet* at acpi0 */
    {&acpihpet_ca,	&acpihpet_cd,	 0, STAR,     loc,    0, pv+57, 83,    0},
/* 61: acpiec* at acpi0 */
    {&acpiec_ca,	&acpiec_cd,	 0, STAR,     loc,    0, pv+57, 83,    0},
/* 62: acpimadt0 at acpi0 */
    {&acpimadt_ca,	&acpimadt_cd,	 0, NORM,     loc,    0, pv+57, 83,    0},
/* 63: acpiprt* at acpi0 */
    {&acpiprt_ca,	&acpiprt_cd,	 0, STAR,     loc,    0, pv+57, 83,    0},
/* 64: acpi0 at bios0 */
    {&acpi_ca,		&acpi_cd,	 0, NORM,     loc,    0, pv+51, 62,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	11 /* mainbus0 */,
	-1
};

int cfroots_size = 2;

/* pseudo-devices */
extern void loopattach(int);
extern void bpfilterattach(int);
extern void rdattach(int);
extern void wsmuxattach(int);

char *pdevnames[] = {
	"loop",
	"bpfilter",
	"rd",
	"wsmux",
};

int pdevnames_size = 4;

struct pdevinit pdevinit[] = {
	{ loopattach, 1 },
	{ bpfilterattach, 1 },
	{ rdattach, 1 },
	{ wsmuxattach, 2 },
	{ NULL, 0 }
};
