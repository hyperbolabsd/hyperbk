/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/i386/conf/RAMDISK_CD"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver vga_cd;
extern struct cfdriver wdc_cd;
extern struct cfdriver ahc_cd;
extern struct cfdriver ahd_cd;
extern struct cfdriver aic_cd;
extern struct cfdriver adv_cd;
extern struct cfdriver adw_cd;
extern struct cfdriver gdt_cd;
extern struct cfdriver twe_cd;
extern struct cfdriver cac_cd;
extern struct cfdriver ciss_cd;
extern struct cfdriver ami_cd;
extern struct cfdriver mfi_cd;
extern struct cfdriver qlw_cd;
extern struct cfdriver qla_cd;
extern struct cfdriver ahci_cd;
extern struct cfdriver nvme_cd;
extern struct cfdriver mpi_cd;
extern struct cfdriver sili_cd;
extern struct cfdriver uha_cd;
extern struct cfdriver siop_cd;
extern struct cfdriver ep_cd;
extern struct cfdriver wi_cd;
extern struct cfdriver an_cd;
extern struct cfdriver le_cd;
extern struct cfdriver xl_cd;
extern struct cfdriver fxp_cd;
extern struct cfdriver rl_cd;
extern struct cfdriver re_cd;
extern struct cfdriver dc_cd;
extern struct cfdriver sm_cd;
extern struct cfdriver epic_cd;
extern struct cfdriver ne_cd;
extern struct cfdriver ie_cd;
extern struct cfdriver ti_cd;
extern struct cfdriver com_cd;
extern struct cfdriver pckbc_cd;
extern struct cfdriver ath_cd;
extern struct cfdriver athn_cd;
extern struct cfdriver atw_cd;
extern struct cfdriver rtw_cd;
extern struct cfdriver rtwn_cd;
extern struct cfdriver ral_cd;
extern struct cfdriver acx_cd;
extern struct cfdriver pgt_cd;
extern struct cfdriver sf_cd;
extern struct cfdriver malo_cd;
extern struct cfdriver bwi_cd;
extern struct cfdriver virtio_cd;
extern struct cfdriver uhci_cd;
extern struct cfdriver ohci_cd;
extern struct cfdriver ehci_cd;
extern struct cfdriver xhci_cd;
extern struct cfdriver sdhc_cd;
extern struct cfdriver rtsx_cd;
extern struct cfdriver softraid_cd;
extern struct cfdriver tlphy_cd;
extern struct cfdriver nsphy_cd;
extern struct cfdriver nsphyter_cd;
extern struct cfdriver qsphy_cd;
extern struct cfdriver inphy_cd;
extern struct cfdriver iophy_cd;
extern struct cfdriver eephy_cd;
extern struct cfdriver exphy_cd;
extern struct cfdriver rlphy_cd;
extern struct cfdriver lxtphy_cd;
extern struct cfdriver luphy_cd;
extern struct cfdriver icsphy_cd;
extern struct cfdriver sqphy_cd;
extern struct cfdriver tqphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver dcphy_cd;
extern struct cfdriver bmtphy_cd;
extern struct cfdriver brgphy_cd;
extern struct cfdriver xmphy_cd;
extern struct cfdriver amphy_cd;
extern struct cfdriver acphy_cd;
extern struct cfdriver urlphy_cd;
extern struct cfdriver rgephy_cd;
extern struct cfdriver ciphy_cd;
extern struct cfdriver ipgphy_cd;
extern struct cfdriver etphy_cd;
extern struct cfdriver jmphy_cd;
extern struct cfdriver atphy_cd;
extern struct cfdriver rdcphy_cd;
extern struct cfdriver mlphy_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver cd_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver st_cd;
extern struct cfdriver atapiscsi_cd;
extern struct cfdriver wd_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver pvbus_cd;
extern struct cfdriver vio_cd;
extern struct cfdriver vioblk_cd;
extern struct cfdriver viornd_cd;
extern struct cfdriver vioscsi_cd;
extern struct cfdriver vmmci_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver arc_cd;
extern struct cfdriver jmb_cd;
extern struct cfdriver mfii_cd;
extern struct cfdriver ips_cd;
extern struct cfdriver qle_cd;
extern struct cfdriver mpii_cd;
extern struct cfdriver de_cd;
extern struct cfdriver pcn_cd;
extern struct cfdriver pciide_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver vr_cd;
extern struct cfdriver vte_cd;
extern struct cfdriver tl_cd;
extern struct cfdriver txp_cd;
extern struct cfdriver em_cd;
extern struct cfdriver ixgb_cd;
extern struct cfdriver ix_cd;
extern struct cfdriver xge_cd;
extern struct cfdriver oce_cd;
extern struct cfdriver wb_cd;
extern struct cfdriver sis_cd;
extern struct cfdriver se_cd;
extern struct cfdriver ste_cd;
extern struct cfdriver cbb_cd;
extern struct cfdriver skc_cd;
extern struct cfdriver sk_cd;
extern struct cfdriver mskc_cd;
extern struct cfdriver msk_cd;
extern struct cfdriver ipw_cd;
extern struct cfdriver iwi_cd;
extern struct cfdriver wpi_cd;
extern struct cfdriver iwn_cd;
extern struct cfdriver iwm_cd;
extern struct cfdriver pcscp_cd;
extern struct cfdriver nge_cd;
extern struct cfdriver lge_cd;
extern struct cfdriver bge_cd;
extern struct cfdriver bnx_cd;
extern struct cfdriver vge_cd;
extern struct cfdriver stge_cd;
extern struct cfdriver nfe_cd;
extern struct cfdriver et_cd;
extern struct cfdriver jme_cd;
extern struct cfdriver age_cd;
extern struct cfdriver alc_cd;
extern struct cfdriver ale_cd;
extern struct cfdriver bce_cd;
extern struct cfdriver vic_cd;
extern struct cfdriver vmx_cd;
extern struct cfdriver vmwpvs_cd;
extern struct cfdriver lii_cd;
extern struct cfdriver gcu_cd;
extern struct cfdriver rge_cd;
extern struct cfdriver pchb_cd;
extern struct cfdriver geodesc_cd;
extern struct cfdriver pcib_cd;
extern struct cfdriver hme_cd;
extern struct cfdriver fd_cd;
extern struct cfdriver isa_cd;
extern struct cfdriver isadma_cd;
extern struct cfdriver fdc_cd;
extern struct cfdriver pcdisplay_cd;
extern struct cfdriver we_cd;
extern struct cfdriver ec_cd;
extern struct cfdriver ex_cd;
extern struct cfdriver npx_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver pckbd_cd;
extern struct cfdriver eisa_cd;
extern struct cfdriver isapnp_cd;
extern struct cfdriver bios_cd;
extern struct cfdriver mpbios_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver ioapic_cd;
extern struct cfdriver pcibios_cd;
extern struct cfdriver cardslot_cd;
extern struct cfdriver cardbus_cd;
extern struct cfdriver pcic_cd;
extern struct cfdriver pcmcia_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver uhidev_cd;
extern struct cfdriver ukbd_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver aue_cd;
extern struct cfdriver axe_cd;
extern struct cfdriver axen_cd;
extern struct cfdriver smsc_cd;
extern struct cfdriver cue_cd;
extern struct cfdriver kue_cd;
extern struct cfdriver cdce_cd;
extern struct cfdriver mos_cd;
extern struct cfdriver mue_cd;
extern struct cfdriver udav_cd;
extern struct cfdriver url_cd;
extern struct cfdriver ure_cd;
extern struct cfdriver ural_cd;
extern struct cfdriver rum_cd;
extern struct cfdriver run_cd;
extern struct cfdriver zyd_cd;
extern struct cfdriver upgt_cd;
extern struct cfdriver urtw_cd;
extern struct cfdriver urtwn_cd;
extern struct cfdriver sdmmc_cd;
extern struct cfdriver acpi_cd;
extern struct cfdriver acpicmos_cd;
extern struct cfdriver acpihpet_cd;
extern struct cfdriver acpiec_cd;
extern struct cfdriver acpimadt_cd;
extern struct cfdriver acpiprt_cd;

extern struct cfattach softraid_ca;
extern struct cfattach tlphy_ca;
extern struct cfattach nsphy_ca;
extern struct cfattach nsphyter_ca;
extern struct cfattach qsphy_ca;
extern struct cfattach inphy_ca;
extern struct cfattach iophy_ca;
extern struct cfattach eephy_ca;
extern struct cfattach exphy_ca;
extern struct cfattach rlphy_ca;
extern struct cfattach lxtphy_ca;
extern struct cfattach luphy_ca;
extern struct cfattach icsphy_ca;
extern struct cfattach sqphy_ca;
extern struct cfattach tqphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach dcphy_ca;
extern struct cfattach bmtphy_ca;
extern struct cfattach brgphy_ca;
extern struct cfattach xmphy_ca;
extern struct cfattach amphy_ca;
extern struct cfattach acphy_ca;
extern struct cfattach urlphy_ca;
extern struct cfattach rgephy_ca;
extern struct cfattach ciphy_ca;
extern struct cfattach ipgphy_ca;
extern struct cfattach etphy_ca;
extern struct cfattach jmphy_ca;
extern struct cfattach atphy_ca;
extern struct cfattach rdcphy_ca;
extern struct cfattach mlphy_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach cd_ca;
extern struct cfattach sd_ca;
extern struct cfattach st_ca;
extern struct cfattach atapiscsi_ca;
extern struct cfattach wd_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach pvbus_ca;
extern struct cfattach vio_ca;
extern struct cfattach vioblk_ca;
extern struct cfattach viornd_ca;
extern struct cfattach vioscsi_ca;
extern struct cfattach vmmci_ca;
extern struct cfattach pci_ca;
extern struct cfattach vga_pci_ca;
extern struct cfattach ahc_pci_ca;
extern struct cfattach ahd_pci_ca;
extern struct cfattach adv_pci_ca;
extern struct cfattach adw_pci_ca;
extern struct cfattach twe_pci_ca;
extern struct cfattach arc_ca;
extern struct cfattach jmb_ca;
extern struct cfattach ahci_pci_ca;
extern struct cfattach ahci_jmb_ca;
extern struct cfattach nvme_pci_ca;
extern struct cfattach ami_pci_ca;
extern struct cfattach mfi_pci_ca;
extern struct cfattach mfii_ca;
extern struct cfattach ips_ca;
extern struct cfattach gdt_pci_ca;
extern struct cfattach cac_pci_ca;
extern struct cfattach ciss_pci_ca;
extern struct cfattach qlw_pci_ca;
extern struct cfattach qla_pci_ca;
extern struct cfattach qle_ca;
extern struct cfattach mpi_pci_ca;
extern struct cfattach mpii_ca;
extern struct cfattach sili_pci_ca;
extern struct cfattach de_ca;
extern struct cfattach ep_pci_ca;
extern struct cfattach pcn_ca;
extern struct cfattach siop_pci_ca;
extern struct cfattach pciide_pci_ca;
extern struct cfattach pciide_jmb_ca;
extern struct cfattach ppb_ca;
extern struct cfattach rl_pci_ca;
extern struct cfattach re_pci_ca;
extern struct cfattach vr_ca;
extern struct cfattach vte_ca;
extern struct cfattach tl_ca;
extern struct cfattach txp_ca;
extern struct cfattach xl_pci_ca;
extern struct cfattach fxp_pci_ca;
extern struct cfattach em_ca;
extern struct cfattach ixgb_ca;
extern struct cfattach ix_ca;
extern struct cfattach xge_ca;
extern struct cfattach oce_ca;
extern struct cfattach dc_pci_ca;
extern struct cfattach epic_pci_ca;
extern struct cfattach ti_pci_ca;
extern struct cfattach ne_pci_ca;
extern struct cfattach wb_ca;
extern struct cfattach sf_pci_ca;
extern struct cfattach sis_ca;
extern struct cfattach se_ca;
extern struct cfattach ste_ca;
extern struct cfattach uhci_pci_ca;
extern struct cfattach ohci_pci_ca;
extern struct cfattach ehci_pci_ca;
extern struct cfattach xhci_pci_ca;
extern struct cfattach cbb_pci_ca;
extern struct cfattach skc_ca;
extern struct cfattach sk_ca;
extern struct cfattach mskc_ca;
extern struct cfattach msk_ca;
extern struct cfattach wi_pci_ca;
extern struct cfattach ipw_ca;
extern struct cfattach iwi_ca;
extern struct cfattach wpi_ca;
extern struct cfattach iwn_ca;
extern struct cfattach iwm_ca;
extern struct cfattach pcscp_ca;
extern struct cfattach nge_ca;
extern struct cfattach lge_ca;
extern struct cfattach bge_ca;
extern struct cfattach bnx_ca;
extern struct cfattach vge_ca;
extern struct cfattach stge_ca;
extern struct cfattach nfe_ca;
extern struct cfattach et_ca;
extern struct cfattach jme_ca;
extern struct cfattach age_ca;
extern struct cfattach alc_ca;
extern struct cfattach ale_ca;
extern struct cfattach bce_ca;
extern struct cfattach ath_pci_ca;
extern struct cfattach athn_pci_ca;
extern struct cfattach atw_pci_ca;
extern struct cfattach rtw_pci_ca;
extern struct cfattach rtwn_pci_ca;
extern struct cfattach ral_pci_ca;
extern struct cfattach acx_pci_ca;
extern struct cfattach pgt_pci_ca;
extern struct cfattach bwi_pci_ca;
extern struct cfattach vic_ca;
extern struct cfattach vmx_ca;
extern struct cfattach vmwpvs_ca;
extern struct cfattach lii_ca;
extern struct cfattach sdhc_pci_ca;
extern struct cfattach gcu_ca;
extern struct cfattach rtsx_pci_ca;
extern struct cfattach virtio_pci_ca;
extern struct cfattach rge_ca;
extern struct cfattach pchb_ca;
extern struct cfattach geodesc_ca;
extern struct cfattach pcib_ca;
extern struct cfattach hme_pci_ca;
extern struct cfattach isa_ca;
extern struct cfattach isadma_ca;
extern struct cfattach fdc_ca;
extern struct cfattach fd_ca;
extern struct cfattach com_isa_ca;
extern struct cfattach pckbc_isa_ca;
extern struct cfattach vga_isa_ca;
extern struct cfattach pcdisplay_ca;
extern struct cfattach aic_isa_ca;
extern struct cfattach uha_isa_ca;
extern struct cfattach wdc_isa_ca;
extern struct cfattach ne_isa_ca;
extern struct cfattach we_isa_ca;
extern struct cfattach ec_ca;
extern struct cfattach ep_isa_ca;
extern struct cfattach ie_isa_ca;
extern struct cfattach ex_ca;
extern struct cfattach le_isa_ca;
extern struct cfattach npx_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach pckbd_ca;
extern struct cfattach eisa_ca;
extern struct cfattach ahc_eisa_ca;
extern struct cfattach uha_eisa_ca;
extern struct cfattach cac_eisa_ca;
extern struct cfattach ep_eisa_ca;
extern struct cfattach isapnp_ca;
extern struct cfattach com_isapnp_ca;
extern struct cfattach wdc_isapnp_ca;
extern struct cfattach aic_isapnp_ca;
extern struct cfattach le_isapnp_ca;
extern struct cfattach ep_isapnp_ca;
extern struct cfattach ne_isapnp_ca;
extern struct cfattach we_isapnp_ca;
extern struct cfattach bios_ca;
extern struct cfattach mpbios_ca;
extern struct cfattach cpu_ca;
extern struct cfattach ioapic_ca;
extern struct cfattach pcibios_ca;
extern struct cfattach cardslot_ca;
extern struct cfattach cardbus_ca;
extern struct cfattach xl_cardbus_ca;
extern struct cfattach dc_cardbus_ca;
extern struct cfattach fxp_cardbus_ca;
extern struct cfattach rl_cardbus_ca;
extern struct cfattach re_cardbus_ca;
extern struct cfattach ath_cardbus_ca;
extern struct cfattach athn_cardbus_ca;
extern struct cfattach atw_cardbus_ca;
extern struct cfattach rtw_cardbus_ca;
extern struct cfattach ral_cardbus_ca;
extern struct cfattach acx_cardbus_ca;
extern struct cfattach pgt_cardbus_ca;
extern struct cfattach ehci_cardbus_ca;
extern struct cfattach ohci_cardbus_ca;
extern struct cfattach malo_cardbus_ca;
extern struct cfattach bwi_cardbus_ca;
extern struct cfattach pcic_isa_ca;
extern struct cfattach pcic_pci_ca;
extern struct cfattach pcmcia_ca;
extern struct cfattach ep_pcmcia_ca;
extern struct cfattach ne_pcmcia_ca;
extern struct cfattach aic_pcmcia_ca;
extern struct cfattach com_pcmcia_ca;
extern struct cfattach wdc_pcmcia_ca;
extern struct cfattach sm_pcmcia_ca;
extern struct cfattach wi_pcmcia_ca;
extern struct cfattach malo_pcmcia_ca;
extern struct cfattach an_pcmcia_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach uhidev_ca;
extern struct cfattach ukbd_ca;
extern struct cfattach umass_ca;
extern struct cfattach aue_ca;
extern struct cfattach axe_ca;
extern struct cfattach axen_ca;
extern struct cfattach smsc_ca;
extern struct cfattach cue_ca;
extern struct cfattach kue_ca;
extern struct cfattach cdce_ca;
extern struct cfattach mos_ca;
extern struct cfattach mue_ca;
extern struct cfattach udav_ca;
extern struct cfattach url_ca;
extern struct cfattach ure_ca;
extern struct cfattach wi_usb_ca;
extern struct cfattach ural_ca;
extern struct cfattach rum_ca;
extern struct cfattach run_ca;
extern struct cfattach zyd_ca;
extern struct cfattach upgt_ca;
extern struct cfattach urtw_ca;
extern struct cfattach urtwn_ca;
extern struct cfattach sdmmc_ca;
extern struct cfattach acpicmos_ca;
extern struct cfattach acpihpet_ca;
extern struct cfattach acpiec_ca;
extern struct cfattach acpimadt_ca;
extern struct cfattach acpiprt_ca;
extern struct cfattach acpi_ca;


/* locators */
static long loc[169] = {
	0x1f0, 0, -1, 0, 0xe, -1, -1, -1,
	0, -1, 0, -1, -1, -1, 0xf0, 0,
	-1, 0, 0xd, -1, -1, 0x360, 0, -1,
	0, 0xf, 6, -1, 0x320, 0, -1, 0,
	5, -1, -1, 0x360, 0, 0xd0000, 0, 7,
	-1, -1, 0x250, 0, 0xd8000, 0, 9, -1,
	-1, 0x300, 0, 0xcc000, 0, 0xa, -1, -1,
	0x3e4, 0, 0xd4000, 0x4000, -1, -1, -1, 0x3e2,
	0, 0xd4000, 0x4000, -1, -1, -1, 0x3e0, 0,
	0xd0000, 0x4000, -1, -1, -1, 0x280, 0, -1,
	0, 9, -1, -1, 0x300, 0, -1, 0,
	0xa, -1, -1, 0x240, 0, -1, 0, 9,
	-1, -1, 0x3f0, 0, -1, 0, 6, 2,
	-1, 0x170, 0, -1, 0, 0xf, -1, -1,
	0x3f8, 0, -1, 0, 4, -1, -1, 0x2f8,
	0, -1, 0, 3, -1, -1, 0x3e8, 0,
	-1, 0, 5, -1, -1, 0x340, 0, -1,
	0, 0xb, -1, -1, 0x330, 0, -1, 0,
	-1, -1, -1, 0x280, 0, 0xd0000, 0, 9,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, 0, -1, 0, -1, -1, -1, -1,
	1,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"phy",
	"target",
	"lun",
	"channel",
	"apid",
	"bus",
	"dev",
	"function",
	"slot",
	"irq",
	"port",
	"size",
	"iomem",
	"iosiz",
	"drq",
	"drq2",
	"drive",
	"console",
	"primary",
	"mux",
	"controller",
	"socket",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"reportid",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 1, 2, -1, 3, -1, 3, -1,
	3, -1, 3, -1, 3, -1, 3, -1,
	4, -1, 5, -1, 5, -1, 6, 7,
	-1, 8, -1, 7, 9, -1, 6, 7,
	-1, 10, 11, 12, 13, 9, 14, 15,
	-1, 16, -1, 10, 11, 12, 13, 9,
	14, -1, 17, 18, 19, -1, 17, 18,
	19, -1, 17, 18, 19, -1, 17, 19,
	-1, 17, 19, -1, 8, -1, 8, -1,
	8, -1, 20, 21, -1, 20, 21, -1,
	20, 21, -1, 20, 21, -1, 10, 22,
	23, 24, 25, 26, -1, 10, 22, 23,
	24, 25, 26, -1, 27, -1,
};

/* size of parent vectors */
int pv_size = 180;

/* parent vectors */
short pv[180] = {
	245, 244, 243, 242, 241, 237, 236, 235, 234, 167, 158, 137, 136, 135, 134, 133,
	132, 131, 130, 129, 128, 127, 126, 125, 116, 114, 105, 104, 103, 101, 84, 83,
	82, 73, 102, 98, 182, 183, 184, 208, 99, 100, 97, 226, 95, 96, 80, 81,
	78, 79, 88, 89, 86, 87, 188, 189, 190, 191, 71, 72, -1, 254, 233, 157,
	124, 68, 66, 59, 58, 51, 42, 40, 35, 0, 74, 179, 201, 69, 67, 55,
	54, 53, 65, 64, 57, 56, 63, 61, 62, 50, 60, 49, 48, 178, 205, 206,
	47, 200, 46, -1, 180, 181, 225, 204, 76, 75, -1, 111, 109, 110, 106, 107,
	108, -1, 215, 219, 220, 221, 222, -1, 37, 77, 164, -1, 176, 45, 177, -1,
	229, 230, -1, 232, 198, -1, 37, 166, -1, 159, 161, -1, 44, -1, 216, -1,
	31, -1, 223, -1, 168, -1, 170, -1, 37, -1, 199, -1, 112, -1, 215, -1,
	175, -1, 231, -1, 162, -1, 210, -1, 260, -1, 228, -1, 202, -1, 52, -1,
	115, -1, 113, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: softraid0 at root */
    {&softraid_ca,	&softraid_cd,	 0, NORM,     loc,    0, pv+60, 0,    0},
/*  1: tlphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&tlphy_ca,		&tlphy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/*  2: nsphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&nsphy_ca,		&nsphy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/*  3: nsphyter* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&nsphyter_ca,	&nsphyter_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/*  4: qsphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&qsphy_ca,		&qsphy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/*  5: inphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&inphy_ca,		&inphy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/*  6: iophy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&iophy_ca,		&iophy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/*  7: eephy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&eephy_ca,		&eephy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/*  8: exphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&exphy_ca,		&exphy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/*  9: rlphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&rlphy_ca,		&rlphy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/* 10: lxtphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&lxtphy_ca,	&lxtphy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/* 11: luphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&luphy_ca,		&luphy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/* 12: icsphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&icsphy_ca,	&icsphy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/* 13: sqphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&sqphy_ca,		&sqphy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/* 14: tqphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&tqphy_ca,		&tqphy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/* 15: ukphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/* 16: dcphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&dcphy_ca,		&dcphy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/* 17: bmtphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&bmtphy_ca,	&bmtphy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/* 18: brgphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&brgphy_ca,	&brgphy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/* 19: xmphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&xmphy_ca,		&xmphy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/* 20: amphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&amphy_ca,		&amphy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/* 21: acphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&acphy_ca,		&acphy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/* 22: urlphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&urlphy_ca,	&urlphy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/* 23: rgephy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&rgephy_ca,	&rgephy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/* 24: ciphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&ciphy_ca,		&ciphy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/* 25: ipgphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&ipgphy_ca,	&ipgphy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/* 26: etphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&etphy_ca,		&etphy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/* 27: jmphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&jmphy_ca,		&jmphy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/* 28: atphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&atphy_ca,		&atphy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/* 29: rdcphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&rdcphy_ca,	&rdcphy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/* 30: mlphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|hme*|lii*|bce*|ale*|alc*|age*|jme*|et*|nfe*|stge*|vge*|bnx*|bge*|lge*|nge*|msk*|sk*|ste*|se*|sis*|wb*|tl*|vte*|vr*|pcn*|sf*|ti*|ne0|ne1|ne2|ne*|ne*|ne*|epic*|sm*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep0|ep*|ep*|ep*|ep*|ep* phy -1 */
    {&mlphy_ca,		&mlphy_cd,	 0, STAR, loc+165,    0, pv+ 0, 1,    0},
/* 31: scsibus* at sdmmc*|umass*|vmwpvs*|pcscp*|mpii*|qle*|ips*|mfii*|arc*|vioscsi*|vioblk*|atapiscsi*|softraid0|siop*|uha0|uha*|sili*|mpi*|nvme*|ahci*|ahci*|qla*|qlw*|mfi*|ami*|ciss*|cac*|cac*|twe*|gdt*|adw*|adv*|aic0|aic*|aic*|ahd*|ahc*|ahc* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+61, 120,    0},
/* 32: cd* at scsibus* target -1 lun -1 */
    {&cd_ca,		&cd_cd,		 0, STAR, loc+164,    0, pv+144, 121,    0},
/* 33: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+164,    0, pv+144, 121,    0},
/* 34: st* at scsibus* target -1 lun -1 */
    {&st_ca,		&st_cd,		 0, STAR, loc+164,    0, pv+144, 121,    0},
/* 35: atapiscsi* at wdc0|wdc1|wdc*|wdc*|pciide*|pciide* channel -1 */
    {&atapiscsi_ca,	&atapiscsi_cd,	 0, STAR, loc+165,    0, pv+100, 124,    0},
/* 36: wd* at wdc0|wdc1|wdc*|wdc*|pciide*|pciide* channel -1 drive -1 */
    {&wd_ca,		&wd_cd,		 0, STAR, loc+164,    0, pv+100, 124,    0},
/* 37: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+60, 0,    0},
/* 38: pvbus0 at mainbus0 apid -1 */
    {&pvbus_ca,		&pvbus_cd,	 0, NORM, loc+165,    0, pv+152, 136,    0},
/* 39: vio* at virtio* */
    {&vio_ca,		&vio_cd,	 0, STAR,     loc,    0, pv+164, 137,    0},
/* 40: vioblk* at virtio* */
    {&vioblk_ca,	&vioblk_cd,	 0, STAR,     loc,    0, pv+164, 137,    0},
/* 41: viornd* at virtio* */
    {&viornd_ca,	&viornd_cd,	 0, STAR,     loc,    0, pv+164, 137,    0},
/* 42: vioscsi* at virtio* */
    {&vioscsi_ca,	&vioscsi_cd,	 0, STAR,     loc,    0, pv+164, 137,    0},
/* 43: vmmci* at virtio* */
    {&vmmci_ca,		&vmmci_cd,	 0, STAR,     loc,    0, pv+164, 137,    0},
/* 44: pci* at mainbus0|ppb*|pchb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+165,    0, pv+120, 136,    0},
/* 45: vga* at pci* dev -1 function -1 */
    {&vga_pci_ca,	&vga_cd,	 1, STAR, loc+164,    0, pv+140, 142,    1},
/* 46: ahc* at pci* dev -1 function -1 */
    {&ahc_pci_ca,	&ahc_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 47: ahd* at pci* dev -1 function -1 */
    {&ahd_pci_ca,	&ahd_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 48: adv* at pci* dev -1 function -1 */
    {&adv_pci_ca,	&adv_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 49: adw* at pci* dev -1 function -1 */
    {&adw_pci_ca,	&adw_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 50: twe* at pci* dev -1 function -1 */
    {&twe_pci_ca,	&twe_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 51: arc* at pci* dev -1 function -1 */
    {&arc_ca,		&arc_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 52: jmb* at pci* dev -1 function -1 */
    {&jmb_ca,		&jmb_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 53: ahci* at pci* dev -1 function -1 */
    {&ahci_pci_ca,	&ahci_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 54: ahci* at jmb* */
    {&ahci_jmb_ca,	&ahci_cd,	 0, STAR,     loc,    0, pv+174, 144,    0},
/* 55: nvme* at pci* dev -1 function -1 */
    {&nvme_pci_ca,	&nvme_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 56: ami* at pci* dev -1 function -1 */
    {&ami_pci_ca,	&ami_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 57: mfi* at pci* dev -1 function -1 */
    {&mfi_pci_ca,	&mfi_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 58: mfii* at pci* dev -1 function -1 */
    {&mfii_ca,		&mfii_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 59: ips* at pci* dev -1 function -1 */
    {&ips_ca,		&ips_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 60: gdt* at pci* dev -1 function -1 */
    {&gdt_pci_ca,	&gdt_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 61: cac* at pci* dev -1 function -1 */
    {&cac_pci_ca,	&cac_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 62: cac* at eisa0 slot -1 */
    {&cac_eisa_ca,	&cac_cd,	 0, STAR, loc+165,    0, pv+154, 145,    0},
/* 63: ciss* at pci* dev -1 function -1 */
    {&ciss_pci_ca,	&ciss_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 64: qlw* at pci* dev -1 function -1 */
    {&qlw_pci_ca,	&qlw_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 65: qla* at pci* dev -1 function -1 */
    {&qla_pci_ca,	&qla_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 66: qle* at pci* dev -1 function -1 */
    {&qle_ca,		&qle_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 67: mpi* at pci* dev -1 function -1 */
    {&mpi_pci_ca,	&mpi_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 68: mpii* at pci* dev -1 function -1 */
    {&mpii_ca,		&mpii_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 69: sili* at pci* dev -1 function -1 */
    {&sili_pci_ca,	&sili_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 70: de* at pci* dev -1 function -1 */
    {&de_ca,		&de_cd,		 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 71: ep* at pci* dev -1 function -1 */
    {&ep_pci_ca,	&ep_cd,		 1, STAR, loc+164,    0, pv+140, 142,    1},
/* 72: ep* at pcmcia* function -1 irq -1 */
    {&ep_pcmcia_ca,	&ep_cd,		 1, STAR, loc+164,    0, pv+146, 147,    1},
/* 73: pcn* at pci* dev -1 function -1 */
    {&pcn_ca,		&pcn_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 74: siop* at pci* dev -1 function -1 */
    {&siop_pci_ca,	&siop_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 75: pciide* at pci* dev -1 function -1 */
    {&pciide_pci_ca,	&pciide_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 76: pciide* at jmb* */
    {&pciide_jmb_ca,	&pciide_cd,	 0, STAR,     loc,    0, pv+174, 144,    0},
/* 77: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 78: rl* at pci* dev -1 function -1 */
    {&rl_pci_ca,	&rl_cd,		 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 79: rl* at cardbus* dev -1 function -1 */
    {&rl_cardbus_ca,	&rl_cd,		 0, STAR, loc+164,    0, pv+142, 150,    0},
/* 80: re* at pci* dev -1 function -1 */
    {&re_pci_ca,	&re_cd,		 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 81: re* at cardbus* dev -1 function -1 */
    {&re_cardbus_ca,	&re_cd,		 0, STAR, loc+164,    0, pv+142, 150,    0},
/* 82: vr* at pci* dev -1 function -1 */
    {&vr_ca,		&vr_cd,		 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 83: vte* at pci* dev -1 function -1 */
    {&vte_ca,		&vte_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 84: tl* at pci* dev -1 function -1 */
    {&tl_ca,		&tl_cd,		 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 85: txp* at pci* dev -1 function -1 */
    {&txp_ca,		&txp_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 86: xl* at pci* dev -1 function -1 */
    {&xl_pci_ca,	&xl_cd,		 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 87: xl* at cardbus* dev -1 function -1 */
    {&xl_cardbus_ca,	&xl_cd,		 0, STAR, loc+164,    0, pv+142, 150,    0},
/* 88: fxp* at pci* dev -1 function -1 */
    {&fxp_pci_ca,	&fxp_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 89: fxp* at cardbus* dev -1 function -1 */
    {&fxp_cardbus_ca,	&fxp_cd,	 0, STAR, loc+164,    0, pv+142, 150,    0},
/* 90: em* at pci* dev -1 function -1 */
    {&em_ca,		&em_cd,		 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 91: ixgb* at pci* dev -1 function -1 */
    {&ixgb_ca,		&ixgb_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 92: ix* at pci* dev -1 function -1 */
    {&ix_ca,		&ix_cd,		 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 93: xge* at pci* dev -1 function -1 */
    {&xge_ca,		&xge_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 94: oce* at pci* dev -1 function -1 */
    {&oce_ca,		&oce_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 95: dc* at pci* dev -1 function -1 */
    {&dc_pci_ca,	&dc_cd,		 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 96: dc* at cardbus* dev -1 function -1 */
    {&dc_cardbus_ca,	&dc_cd,		 0, STAR, loc+164,    0, pv+142, 150,    0},
/* 97: epic* at pci* dev -1 function -1 */
    {&epic_pci_ca,	&epic_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 98: ti* at pci* dev -1 function -1 */
    {&ti_pci_ca,	&ti_cd,		 0, STAR, loc+164,    0, pv+140, 142,    0},
/* 99: ne* at pci* dev -1 function -1 */
    {&ne_pci_ca,	&ne_cd,		 3, STAR, loc+164,    0, pv+140, 142,    3},
/*100: ne* at pcmcia* function -1 irq -1 */
    {&ne_pcmcia_ca,	&ne_cd,		 3, STAR, loc+164,    0, pv+146, 147,    3},
/*101: wb* at pci* dev -1 function -1 */
    {&wb_ca,		&wb_cd,		 0, STAR, loc+164,    0, pv+140, 142,    0},
/*102: sf* at pci* dev -1 function -1 */
    {&sf_pci_ca,	&sf_cd,		 0, STAR, loc+164,    0, pv+140, 142,    0},
/*103: sis* at pci* dev -1 function -1 */
    {&sis_ca,		&sis_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*104: se* at pci* dev -1 function -1 */
    {&se_ca,		&se_cd,		 0, STAR, loc+164,    0, pv+140, 142,    0},
/*105: ste* at pci* dev -1 function -1 */
    {&ste_ca,		&ste_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*106: uhci* at pci* dev -1 function -1 */
    {&uhci_pci_ca,	&uhci_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*107: ohci* at pci* dev -1 function -1 */
    {&ohci_pci_ca,	&ohci_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*108: ohci* at cardbus* dev -1 function -1 */
    {&ohci_cardbus_ca,	&ohci_cd,	 0, STAR, loc+164,    0, pv+142, 150,    0},
/*109: ehci* at pci* dev -1 function -1 */
    {&ehci_pci_ca,	&ehci_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*110: ehci* at cardbus* dev -1 function -1 */
    {&ehci_cardbus_ca,	&ehci_cd,	 0, STAR, loc+164,    0, pv+142, 150,    0},
/*111: xhci* at pci* dev -1 function -1 */
    {&xhci_pci_ca,	&xhci_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*112: cbb* at pci* dev -1 function -1 */
    {&cbb_pci_ca,	&cbb_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*113: skc* at pci* dev -1 function -1 */
    {&skc_ca,		&skc_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*114: sk* at skc* */
    {&sk_ca,		&sk_cd,		 0, STAR,     loc,    0, pv+178, 152,    0},
/*115: mskc* at pci* dev -1 function -1 */
    {&mskc_ca,		&mskc_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*116: msk* at mskc* */
    {&msk_ca,		&msk_cd,	 0, STAR,     loc,    0, pv+176, 152,    0},
/*117: wi* at pci* dev -1 function -1 */
    {&wi_pci_ca,	&wi_cd,		 0, STAR, loc+164,    0, pv+140, 142,    0},
/*118: wi* at pcmcia* function -1 irq -1 */
    {&wi_pcmcia_ca,	&wi_cd,		 0, STAR, loc+164,    0, pv+146, 147,    0},
/*119: ipw* at pci* dev -1 function -1 */
    {&ipw_ca,		&ipw_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*120: iwi* at pci* dev -1 function -1 */
    {&iwi_ca,		&iwi_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*121: wpi* at pci* dev -1 function -1 */
    {&wpi_ca,		&wpi_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*122: iwn* at pci* dev -1 function -1 */
    {&iwn_ca,		&iwn_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*123: iwm* at pci* dev -1 function -1 */
    {&iwm_ca,		&iwm_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*124: pcscp* at pci* dev -1 function -1 */
    {&pcscp_ca,		&pcscp_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*125: nge* at pci* dev -1 function -1 */
    {&nge_ca,		&nge_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*126: lge* at pci* dev -1 function -1 */
    {&lge_ca,		&lge_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*127: bge* at pci* dev -1 function -1 */
    {&bge_ca,		&bge_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*128: bnx* at pci* dev -1 function -1 */
    {&bnx_ca,		&bnx_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*129: vge* at pci* dev -1 function -1 */
    {&vge_ca,		&vge_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*130: stge* at pci* dev -1 function -1 */
    {&stge_ca,		&stge_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*131: nfe* at pci* dev -1 function -1 */
    {&nfe_ca,		&nfe_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*132: et* at pci* dev -1 function -1 */
    {&et_ca,		&et_cd,		 0, STAR, loc+164,    0, pv+140, 142,    0},
/*133: jme* at pci* dev -1 function -1 */
    {&jme_ca,		&jme_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*134: age* at pci* dev -1 function -1 */
    {&age_ca,		&age_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*135: alc* at pci* dev -1 function -1 */
    {&alc_ca,		&alc_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*136: ale* at pci* dev -1 function -1 */
    {&ale_ca,		&ale_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*137: bce* at pci* dev -1 function -1 */
    {&bce_ca,		&bce_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*138: ath* at pci* dev -1 function -1 */
    {&ath_pci_ca,	&ath_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*139: ath* at cardbus* dev -1 function -1 */
    {&ath_cardbus_ca,	&ath_cd,	 0, STAR, loc+164,    0, pv+142, 150,    0},
/*140: athn* at pci* dev -1 function -1 */
    {&athn_pci_ca,	&athn_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*141: athn* at cardbus* dev -1 function -1 */
    {&athn_cardbus_ca,	&athn_cd,	 0, STAR, loc+164,    0, pv+142, 150,    0},
/*142: atw* at pci* dev -1 function -1 */
    {&atw_pci_ca,	&atw_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*143: atw* at cardbus* dev -1 function -1 */
    {&atw_cardbus_ca,	&atw_cd,	 0, STAR, loc+164,    0, pv+142, 150,    0},
/*144: rtw* at pci* dev -1 function -1 */
    {&rtw_pci_ca,	&rtw_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*145: rtw* at cardbus* dev -1 function -1 */
    {&rtw_cardbus_ca,	&rtw_cd,	 0, STAR, loc+164,    0, pv+142, 150,    0},
/*146: rtwn* at pci* dev -1 function -1 */
    {&rtwn_pci_ca,	&rtwn_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*147: ral* at pci* dev -1 function -1 */
    {&ral_pci_ca,	&ral_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*148: ral* at cardbus* dev -1 function -1 */
    {&ral_cardbus_ca,	&ral_cd,	 0, STAR, loc+164,    0, pv+142, 150,    0},
/*149: acx* at pci* dev -1 function -1 */
    {&acx_pci_ca,	&acx_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*150: acx* at cardbus* dev -1 function -1 */
    {&acx_cardbus_ca,	&acx_cd,	 0, STAR, loc+164,    0, pv+142, 150,    0},
/*151: pgt* at pci* dev -1 function -1 */
    {&pgt_pci_ca,	&pgt_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*152: pgt* at cardbus* dev -1 function -1 */
    {&pgt_cardbus_ca,	&pgt_cd,	 0, STAR, loc+164,    0, pv+142, 150,    0},
/*153: bwi* at pci* dev -1 function -1 */
    {&bwi_pci_ca,	&bwi_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*154: bwi* at cardbus* dev -1 function -1 */
    {&bwi_cardbus_ca,	&bwi_cd,	 0, STAR, loc+164,    0, pv+142, 150,    0},
/*155: vic* at pci* dev -1 function -1 */
    {&vic_ca,		&vic_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*156: vmx* at pci* dev -1 function -1 */
    {&vmx_ca,		&vmx_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*157: vmwpvs* at pci* dev -1 function -1 */
    {&vmwpvs_ca,	&vmwpvs_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*158: lii* at pci* dev -1 function -1 */
    {&lii_ca,		&lii_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*159: sdhc* at pci* dev -1 function -1 */
    {&sdhc_pci_ca,	&sdhc_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*160: gcu* at pci* dev -1 function -1 */
    {&gcu_ca,		&gcu_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*161: rtsx* at pci* dev -1 function -1 */
    {&rtsx_pci_ca,	&rtsx_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*162: virtio* at pci* dev -1 function -1 */
    {&virtio_pci_ca,	&virtio_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*163: rge* at pci* dev -1 function -1 */
    {&rge_ca,		&rge_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*164: pchb* at pci* dev -1 function -1 */
    {&pchb_ca,		&pchb_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*165: geodesc* at pci* dev -1 function -1 */
    {&geodesc_ca,	&geodesc_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*166: pcib* at pci* dev -1 function -1 */
    {&pcib_ca,		&pcib_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*167: hme* at pci* dev -1 function -1 */
    {&hme_pci_ca,	&hme_cd,	 0, STAR, loc+164,    0, pv+140, 142,    0},
/*168: isa0 at mainbus0|pcib* */
    {&isa_ca,		&isa_cd,	 0, NORM,     loc,    0, pv+134, 136,    0},
/*169: isadma0 at isa0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&isadma_ca,	&isadma_cd,	 0, NORM, loc+  7,    0, pv+148, 153,    0},
/*170: fdc0 at isa0 port 0x3f0 size 0 iomem -1 iosiz 0 irq 6 drq 2 drq2 -1 */
    {&fdc_ca,		&fdc_cd,	 0, NORM, loc+ 98,    0, pv+148, 153,    0},
/*171: fd* at fdc0 drive -1 */
    {&fd_ca,		&fd_cd,		 0, STAR, loc+165,    0, pv+150, 161,    0},
/*172: com0 at isa0 port 0x3f8 size 0 iomem -1 iosiz 0 irq 4 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 0, NORM, loc+112,    0, pv+148, 153,    0},
/*173: com1 at isa0 port 0x2f8 size 0 iomem -1 iosiz 0 irq 3 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 1, NORM, loc+119,    0, pv+148, 153,    1},
/*174: com2 at isa0 port 0x3e8 size 0 iomem -1 iosiz 0 irq 5 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 2, NORM, loc+126,    0, pv+148, 153,    2},
/*175: pckbc0 at isa0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&pckbc_isa_ca,	&pckbc_cd,	 0, NORM, loc+  7,    0, pv+148, 153,    0},
/*176: vga0 at isa0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&vga_isa_ca,	&vga_cd,	 0, NORM, loc+  7,    0, pv+148, 153,    0},
/*177: pcdisplay0 at isa0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&pcdisplay_ca,	&pcdisplay_cd,	 0, NORM, loc+  7,    0, pv+148, 153,    0},
/*178: aic0 at isa0 port 0x340 size 0 iomem -1 iosiz 0 irq 0xb drq -1 drq2 -1 */
    {&aic_isa_ca,	&aic_cd,	 0, NORM, loc+133,    0, pv+148, 153,    0},
/*179: uha0 at isa0 port 0x330 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&uha_isa_ca,	&uha_cd,	 0, NORM, loc+140,    0, pv+148, 153,    0},
/*180: wdc0 at isa0 port 0x1f0 size 0 iomem -1 iosiz 0 irq 0xe drq -1 drq2 -1 */
    {&wdc_isa_ca,	&wdc_cd,	 0, NORM, loc+  0,    0, pv+148, 153,    0},
/*181: wdc1 at isa0 port 0x170 size 0 iomem -1 iosiz 0 irq 0xf drq -1 drq2 -1 */
    {&wdc_isa_ca,	&wdc_cd,	 1, NORM, loc+105,    0, pv+148, 153,    1},
/*182: ne0 at isa0 port 0x240 size 0 iomem -1 iosiz 0 irq 9 drq -1 drq2 -1 */
    {&ne_isa_ca,	&ne_cd,		 0, NORM, loc+ 91,    0, pv+148, 153,    0},
/*183: ne1 at isa0 port 0x300 size 0 iomem -1 iosiz 0 irq 0xa drq -1 drq2 -1 */
    {&ne_isa_ca,	&ne_cd,		 1, NORM, loc+ 84,    0, pv+148, 153,    1},
/*184: ne2 at isa0 port 0x280 size 0 iomem -1 iosiz 0 irq 9 drq -1 drq2 -1 */
    {&ne_isa_ca,	&ne_cd,		 2, NORM, loc+ 77,    0, pv+148, 153,    2},
/*185: we0 at isa0 port 0x280 size 0 iomem 0xd0000 iosiz 0 irq 9 drq -1 drq2 -1 */
    {&we_isa_ca,	&we_cd,		 0, NORM, loc+147,    0, pv+148, 153,    0},
/*186: we1 at isa0 port 0x300 size 0 iomem 0xcc000 iosiz 0 irq 0xa drq -1 drq2 -1 */
    {&we_isa_ca,	&we_cd,		 1, NORM, loc+ 49,    0, pv+148, 153,    1},
/*187: ec0 at isa0 port 0x250 size 0 iomem 0xd8000 iosiz 0 irq 9 drq -1 drq2 -1 */
    {&ec_ca,		&ec_cd,		 0, NORM, loc+ 42,    0, pv+148, 153,    0},
/*188: ep0 at isa0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&ep_isa_ca,	&ep_cd,		 0, NORM, loc+  7,    0, pv+148, 153,    0},
/*189: ep* at isa0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&ep_isa_ca,	&ep_cd,		 1, STAR, loc+  7,    0, pv+148, 153,    1},
/*190: ep* at isapnp0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 */
    {&ep_isapnp_ca,	&ep_cd,		 1, STAR, loc+160,    0, pv+172, 163,    1},
/*191: ep* at eisa0 slot -1 */
    {&ep_eisa_ca,	&ep_cd,		 1, STAR, loc+165,    0, pv+154, 145,    1},
/*192: ie0 at isa0 port 0x360 size 0 iomem 0xd0000 iosiz 0 irq 7 drq -1 drq2 -1 */
    {&ie_isa_ca,	&ie_cd,		 0, NORM, loc+ 35,    0, pv+148, 153,    0},
/*193: ex0 at isa0 port 0x320 size 0 iomem -1 iosiz 0 irq 5 drq -1 drq2 -1 */
    {&ex_ca,		&ex_cd,		 0, NORM, loc+ 28,    0, pv+148, 153,    0},
/*194: le0 at isa0 port 0x360 size 0 iomem -1 iosiz 0 irq 0xf drq 6 drq2 -1 */
    {&le_isa_ca,	&le_cd,		 0, NORM, loc+ 21,    0, pv+148, 153,    0},
/*195: npx0 at isa0 port 0xf0 size 0 iomem -1 iosiz 0 irq 0xd drq -1 drq2 -1 */
    {&npx_ca,		&npx_cd,	 0, NORM, loc+ 14,    0, pv+148, 153,    0},
/*196: wsdisplay* at vga0|vga*|pcdisplay0 console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+166,    0, pv+124, 170,    0},
/*197: wskbd* at ukbd*|pckbd* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+167,    0, pv+131, 182,    0},
/*198: pckbd* at pckbc0 slot -1 */
    {&pckbd_ca,		&pckbd_cd,	 0, STAR, loc+165,    0, pv+160, 188,    0},
/*199: eisa0 at mainbus0 */
    {&eisa_ca,		&eisa_cd,	 0, NORM,     loc,    0, pv+152, 136,    0},
/*200: ahc* at eisa0 slot -1 */
    {&ahc_eisa_ca,	&ahc_cd,	 0, STAR, loc+165,    0, pv+154, 145,    0},
/*201: uha* at eisa0 slot -1 */
    {&uha_eisa_ca,	&uha_cd,	 1, STAR, loc+165,    0, pv+154, 145,    1},
/*202: isapnp0 at isa0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&isapnp_ca,	&isapnp_cd,	 0, NORM, loc+  7,    0, pv+148, 153,    0},
/*203: com* at isapnp0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 */
    {&com_isapnp_ca,	&com_cd,	 3, STAR, loc+160,    0, pv+172, 163,    3},
/*204: wdc* at isapnp0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 */
    {&wdc_isapnp_ca,	&wdc_cd,	 2, STAR, loc+160,    0, pv+172, 163,    2},
/*205: aic* at isapnp0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 */
    {&aic_isapnp_ca,	&aic_cd,	 1, STAR, loc+160,    0, pv+172, 163,    1},
/*206: aic* at pcmcia* function -1 irq -1 */
    {&aic_pcmcia_ca,	&aic_cd,	 1, STAR, loc+164,    0, pv+146, 147,    1},
/*207: le* at isapnp0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 */
    {&le_isapnp_ca,	&le_cd,		 1, STAR, loc+160,    0, pv+172, 163,    1},
/*208: ne* at isapnp0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 */
    {&ne_isapnp_ca,	&ne_cd,		 3, STAR, loc+160,    0, pv+172, 163,    3},
/*209: we* at isapnp0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 */
    {&we_isapnp_ca,	&we_cd,		 2, STAR, loc+160,    0, pv+172, 163,    2},
/*210: bios0 at mainbus0 apid -1 */
    {&bios_ca,		&bios_cd,	 0, NORM, loc+165,    0, pv+152, 136,    0},
/*211: mpbios0 at bios0 */
    {&mpbios_ca,	&mpbios_cd,	 0, NORM,     loc,    0, pv+166, 189,    0},
/*212: cpu0 at mainbus0 apid -1 */
    {&cpu_ca,		&cpu_cd,	 0, NORM, loc+165,    0, pv+152, 136,    0},
/*213: ioapic* at mainbus0 apid -1 */
    {&ioapic_ca,	&ioapic_cd,	 0, STAR, loc+165,    0, pv+152, 136,    0},
/*214: pcibios0 at bios0 */
    {&pcibios_ca,	&pcibios_cd,	 0, NORM,     loc,    0, pv+166, 189,    0},
/*215: cardslot* at cbb* slot -1 */
    {&cardslot_ca,	&cardslot_cd,	 0, STAR, loc+165,    0, pv+156, 190,    0},
/*216: cardbus* at cardslot* slot -1 */
    {&cardbus_ca,	&cardbus_cd,	 0, STAR, loc+165,    0, pv+158, 192,    0},
/*217: malo* at cardbus* dev -1 function -1 */
    {&malo_cardbus_ca,	&malo_cd,	 0, STAR, loc+164,    0, pv+142, 150,    0},
/*218: malo* at pcmcia* function -1 irq -1 */
    {&malo_pcmcia_ca,	&malo_cd,	 0, STAR, loc+164,    0, pv+146, 147,    0},
/*219: pcic0 at isa0 port 0x3e0 size 0 iomem 0xd0000 iosiz 0x4000 irq -1 drq -1 drq2 -1 */
    {&pcic_isa_ca,	&pcic_cd,	 0, NORM, loc+ 70,    0, pv+148, 153,    0},
/*220: pcic1 at isa0 port 0x3e2 size 0 iomem 0xd4000 iosiz 0x4000 irq -1 drq -1 drq2 -1 */
    {&pcic_isa_ca,	&pcic_cd,	 1, NORM, loc+ 63,    0, pv+148, 153,    1},
/*221: pcic2 at isa0 port 0x3e4 size 0 iomem 0xd4000 iosiz 0x4000 irq -1 drq -1 drq2 -1 */
    {&pcic_isa_ca,	&pcic_cd,	 2, NORM, loc+ 56,    0, pv+148, 153,    2},
/*222: pcic* at pci* dev -1 function -1 */
    {&pcic_pci_ca,	&pcic_cd,	 3, STAR, loc+164,    0, pv+140, 142,    3},
/*223: pcmcia* at cardslot*|pcic0|pcic1|pcic2|pcic* controller -1 socket -1 */
    {&pcmcia_ca,	&pcmcia_cd,	 0, STAR, loc+164,    0, pv+114, 192,    0},
/*224: com* at pcmcia* function -1 irq -1 */
    {&com_pcmcia_ca,	&com_cd,	 3, STAR, loc+164,    0, pv+146, 147,    3},
/*225: wdc* at pcmcia* function -1 irq -1 */
    {&wdc_pcmcia_ca,	&wdc_cd,	 2, STAR, loc+164,    0, pv+146, 147,    2},
/*226: sm* at pcmcia* function -1 irq -1 */
    {&sm_pcmcia_ca,	&sm_cd,		 0, STAR, loc+164,    0, pv+146, 147,    0},
/*227: an* at pcmcia* function -1 irq -1 */
    {&an_pcmcia_ca,	&an_cd,		 0, STAR, loc+164,    0, pv+146, 147,    0},
/*228: usb* at xhci*|ehci*|ehci*|uhci*|ohci*|ohci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+107, 205,    0},
/*229: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+170, 205,    0},
/*230: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+154,    0, pv+128, 206,    0},
/*231: uhidev* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhidev_ca,	&uhidev_cd,	 0, STAR, loc+154,    0, pv+128, 206,    0},
/*232: ukbd* at uhidev* reportid -1 */
    {&ukbd_ca,		&ukbd_cd,	 0, STAR, loc+165,    0, pv+162, 220,    0},
/*233: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+154,    0, pv+128, 206,    0},
/*234: aue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&aue_ca,		&aue_cd,	 0, STAR, loc+154,    0, pv+128, 206,    0},
/*235: axe* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axe_ca,		&axe_cd,	 0, STAR, loc+154,    0, pv+128, 206,    0},
/*236: axen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axen_ca,		&axen_cd,	 0, STAR, loc+154,    0, pv+128, 206,    0},
/*237: smsc* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&smsc_ca,		&smsc_cd,	 0, STAR, loc+154,    0, pv+128, 206,    0},
/*238: cue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cue_ca,		&cue_cd,	 0, STAR, loc+154,    0, pv+128, 206,    0},
/*239: kue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&kue_ca,		&kue_cd,	 0, STAR, loc+154,    0, pv+128, 206,    0},
/*240: cdce* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cdce_ca,		&cdce_cd,	 0, STAR, loc+154,    0, pv+128, 206,    0},
/*241: mos* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mos_ca,		&mos_cd,	 0, STAR, loc+154,    0, pv+128, 206,    0},
/*242: mue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mue_ca,		&mue_cd,	 0, STAR, loc+154,    0, pv+128, 206,    0},
/*243: udav* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udav_ca,		&udav_cd,	 0, STAR, loc+154,    0, pv+128, 206,    0},
/*244: url* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&url_ca,		&url_cd,	 0, STAR, loc+154,    0, pv+128, 206,    0},
/*245: ure* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ure_ca,		&ure_cd,	 0, STAR, loc+154,    0, pv+128, 206,    0},
/*246: wi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&wi_usb_ca,	&wi_cd,		 0, STAR, loc+154,    0, pv+128, 206,    0},
/*247: ural* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ural_ca,		&ural_cd,	 0, STAR, loc+154,    0, pv+128, 206,    0},
/*248: rum* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rum_ca,		&rum_cd,	 0, STAR, loc+154,    0, pv+128, 206,    0},
/*249: run* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&run_ca,		&run_cd,	 0, STAR, loc+154,    0, pv+128, 206,    0},
/*250: zyd* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&zyd_ca,		&zyd_cd,	 0, STAR, loc+154,    0, pv+128, 206,    0},
/*251: upgt* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upgt_ca,		&upgt_cd,	 0, STAR, loc+154,    0, pv+128, 206,    0},
/*252: urtw* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtw_ca,		&urtw_cd,	 0, STAR, loc+154,    0, pv+128, 206,    0},
/*253: urtwn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtwn_ca,		&urtwn_cd,	 0, STAR, loc+154,    0, pv+128, 206,    0},
/*254: sdmmc* at sdhc*|rtsx* */
    {&sdmmc_ca,		&sdmmc_cd,	 0, STAR,     loc,    0, pv+137, 221,    0},
/*255: acpicmos* at acpi0 */
    {&acpicmos_ca,	&acpicmos_cd,	 0, STAR,     loc,    0, pv+168, 221,    0},
/*256: acpihpet* at acpi0 */
    {&acpihpet_ca,	&acpihpet_cd,	 0, STAR,     loc,    0, pv+168, 221,    0},
/*257: acpiec* at acpi0 */
    {&acpiec_ca,	&acpiec_cd,	 0, STAR,     loc,    0, pv+168, 221,    0},
/*258: acpimadt0 at acpi0 */
    {&acpimadt_ca,	&acpimadt_cd,	 0, NORM,     loc,    0, pv+168, 221,    0},
/*259: acpiprt* at acpi0 */
    {&acpiprt_ca,	&acpiprt_cd,	 0, STAR,     loc,    0, pv+168, 221,    0},
/*260: acpi0 at bios0 */
    {&acpi_ca,		&acpi_cd,	 0, NORM,     loc,    0, pv+166, 189,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 0 /* softraid0 */,
	37 /* mainbus0 */,
	-1
};

int cfroots_size = 3;

/* pseudo-devices */
extern void loopattach(int);
extern void vlanattach(int);
extern void trunkattach(int);
extern void bpfilterattach(int);
extern void rdattach(int);
extern void wsmuxattach(int);
extern void bioattach(int);

char *pdevnames[] = {
	"loop",
	"vlan",
	"trunk",
	"bpfilter",
	"rd",
	"wsmux",
	"bio",
};

int pdevnames_size = 7;

struct pdevinit pdevinit[] = {
	{ loopattach, 1 },
	{ vlanattach, 1 },
	{ trunkattach, 1 },
	{ bpfilterattach, 1 },
	{ rdattach, 1 },
	{ wsmuxattach, 2 },
	{ bioattach, 1 },
	{ NULL, 0 }
};
