/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/landisk/conf/GENERIC"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver audio_cd;
extern struct cfdriver wdc_cd;
extern struct cfdriver wi_cd;
extern struct cfdriver rl_cd;
extern struct cfdriver re_cd;
extern struct cfdriver athn_cd;
extern struct cfdriver ohci_cd;
extern struct cfdriver ehci_cd;
extern struct cfdriver radio_cd;
extern struct cfdriver vscsi_cd;
extern struct cfdriver softraid_cd;
extern struct cfdriver wd_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver cd_cd;
extern struct cfdriver atapiscsi_cd;
extern struct cfdriver eephy_cd;
extern struct cfdriver rlphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver amphy_cd;
extern struct cfdriver acphy_cd;
extern struct cfdriver urlphy_cd;
extern struct cfdriver rgephy_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver pciide_cd;
extern struct cfdriver shpcic_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver wsmouse_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver uaudio_cd;
extern struct cfdriver udl_cd;
extern struct cfdriver ucom_cd;
extern struct cfdriver ugen_cd;
extern struct cfdriver uhidev_cd;
extern struct cfdriver uhid_cd;
extern struct cfdriver fido_cd;
extern struct cfdriver ujoy_cd;
extern struct cfdriver ukbd_cd;
extern struct cfdriver ums_cd;
extern struct cfdriver ucycom_cd;
extern struct cfdriver uslhcom_cd;
extern struct cfdriver ulpt_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver uthum_cd;
extern struct cfdriver ugold_cd;
extern struct cfdriver uonerng_cd;
extern struct cfdriver urng_cd;
extern struct cfdriver udcf_cd;
extern struct cfdriver uvisor_cd;
extern struct cfdriver udsbr_cd;
extern struct cfdriver utwitch_cd;
extern struct cfdriver aue_cd;
extern struct cfdriver axe_cd;
extern struct cfdriver axen_cd;
extern struct cfdriver smsc_cd;
extern struct cfdriver cue_cd;
extern struct cfdriver kue_cd;
extern struct cfdriver cdce_cd;
extern struct cfdriver urndis_cd;
extern struct cfdriver udav_cd;
extern struct cfdriver upl_cd;
extern struct cfdriver ugl_cd;
extern struct cfdriver url_cd;
extern struct cfdriver ure_cd;
extern struct cfdriver umodem_cd;
extern struct cfdriver uftdi_cd;
extern struct cfdriver uplcom_cd;
extern struct cfdriver umct_cd;
extern struct cfdriver uvscom_cd;
extern struct cfdriver ubsa_cd;
extern struct cfdriver uslcom_cd;
extern struct cfdriver uark_cd;
extern struct cfdriver moscom_cd;
extern struct cfdriver umcs_cd;
extern struct cfdriver uscom_cd;
extern struct cfdriver ucrcom_cd;
extern struct cfdriver uipaq_cd;
extern struct cfdriver umsm_cd;
extern struct cfdriver uchcom_cd;
extern struct cfdriver atu_cd;
extern struct cfdriver ural_cd;
extern struct cfdriver rum_cd;
extern struct cfdriver run_cd;
extern struct cfdriver zyd_cd;
extern struct cfdriver urtwn_cd;
extern struct cfdriver rsu_cd;
extern struct cfdriver uath_cd;
extern struct cfdriver upd_cd;
extern struct cfdriver uhidpp_cd;
extern struct cfdriver shb_cd;
extern struct cfdriver scif_cd;
extern struct cfdriver rsclock_cd;
extern struct cfdriver obio_cd;
extern struct cfdriver power_cd;

extern struct cfattach audio_ca;
extern struct cfattach radio_ca;
extern struct cfattach vscsi_ca;
extern struct cfattach softraid_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach cpu_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach cd_ca;
extern struct cfattach sd_ca;
extern struct cfattach wd_ca;
extern struct cfattach atapiscsi_ca;
extern struct cfattach eephy_ca;
extern struct cfattach rlphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach amphy_ca;
extern struct cfattach acphy_ca;
extern struct cfattach urlphy_ca;
extern struct cfattach rgephy_ca;
extern struct cfattach pci_ca;
extern struct cfattach pciide_pci_ca;
extern struct cfattach rl_pci_ca;
extern struct cfattach re_pci_ca;
extern struct cfattach ohci_pci_ca;
extern struct cfattach ehci_pci_ca;
extern struct cfattach shpcic_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach wsmouse_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach uaudio_ca;
extern struct cfattach udl_ca;
extern struct cfattach ucom_ca;
extern struct cfattach ugen_ca;
extern struct cfattach uhidev_ca;
extern struct cfattach uhid_ca;
extern struct cfattach fido_ca;
extern struct cfattach ujoy_ca;
extern struct cfattach ukbd_ca;
extern struct cfattach ums_ca;
extern struct cfattach ucycom_ca;
extern struct cfattach uslhcom_ca;
extern struct cfattach ulpt_ca;
extern struct cfattach umass_ca;
extern struct cfattach uthum_ca;
extern struct cfattach ugold_ca;
extern struct cfattach uonerng_ca;
extern struct cfattach urng_ca;
extern struct cfattach udcf_ca;
extern struct cfattach uvisor_ca;
extern struct cfattach udsbr_ca;
extern struct cfattach utwitch_ca;
extern struct cfattach aue_ca;
extern struct cfattach axe_ca;
extern struct cfattach axen_ca;
extern struct cfattach smsc_ca;
extern struct cfattach cue_ca;
extern struct cfattach kue_ca;
extern struct cfattach cdce_ca;
extern struct cfattach urndis_ca;
extern struct cfattach udav_ca;
extern struct cfattach upl_ca;
extern struct cfattach ugl_ca;
extern struct cfattach url_ca;
extern struct cfattach ure_ca;
extern struct cfattach umodem_ca;
extern struct cfattach uftdi_ca;
extern struct cfattach uplcom_ca;
extern struct cfattach umct_ca;
extern struct cfattach uvscom_ca;
extern struct cfattach ubsa_ca;
extern struct cfattach uslcom_ca;
extern struct cfattach uark_ca;
extern struct cfattach moscom_ca;
extern struct cfattach umcs_ca;
extern struct cfattach uscom_ca;
extern struct cfattach ucrcom_ca;
extern struct cfattach uipaq_ca;
extern struct cfattach umsm_ca;
extern struct cfattach uchcom_ca;
extern struct cfattach wi_usb_ca;
extern struct cfattach atu_ca;
extern struct cfattach ural_ca;
extern struct cfattach rum_ca;
extern struct cfattach run_ca;
extern struct cfattach zyd_ca;
extern struct cfattach urtwn_ca;
extern struct cfattach rsu_ca;
extern struct cfattach uath_ca;
extern struct cfattach athn_usb_ca;
extern struct cfattach upd_ca;
extern struct cfattach uhidpp_ca;
extern struct cfattach shb_ca;
extern struct cfattach scif_ca;
extern struct cfattach rsclock_ca;
extern struct cfattach obio_ca;
extern struct cfattach wdc_obio_ca;
extern struct cfattach power_ca;


/* locators */
static long loc[20] = {
	-1, -1, -1, -1, -1, -1, -1, 0,
	-1, 0, -1, 0x14000000, 0, -1, 0, 0xa,
	-1, -1, 1, 0,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"target",
	"lun",
	"channel",
	"drive",
	"phy",
	"bus",
	"dev",
	"function",
	"console",
	"primary",
	"mux",
	"port",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"portno",
	"reportid",
	"size",
	"iomem",
	"iosiz",
	"irq",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, 1, -1, 2, 3, -1, 2,
	3, -1, 4, -1, 4, -1, 4, -1,
	4, -1, 4, -1, 4, -1, 4, -1,
	4, -1, 4, -1, 5, -1, 6, 7,
	-1, 8, 9, 10, -1, 8, 10, -1,
	10, -1, 11, 12, 13, 14, 15, 16,
	-1, 11, 12, 13, 14, 15, 16, -1,
	17, -1, 17, -1, 17, -1, 17, -1,
	17, -1, 17, -1, 17, -1, 17, -1,
	17, -1, 17, -1, 17, -1, 17, -1,
	17, -1, 17, -1, 17, -1, 17, -1,
	17, -1, 17, -1, 18, -1, 11, 19,
	20, 21, 22, -1,
};

/* size of parent vectors */
int pv_size = 69;

/* parent vectors */
short pv[69] = {
	66, 50, 70, 71, 67, 68, 69, 72, 76, 77, 73, 74, 75, 78, 79, 80,
	41, 42, -1, 65, 64, 61, 56, 55, 54, 53, 21, 20, -1, 3, 2, 44,
	10, -1, 29, 30, -1, 97, 19, -1, 23, 22, -1, 18, -1, 35, -1, 39,
	-1, 6, -1, 32, -1, 24, -1, 40, -1, 96, -1, 4, -1, 51, -1, 28,
	-1, 93, -1, 31, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: audio* at uaudio* */
    {&audio_ca,		&audio_cd,	 0, STAR,     loc,    0, pv+67, 0,    0},
/*  1: radio* at udsbr* */
    {&radio_ca,		&radio_cd,	 0, STAR,     loc,    0, pv+61, 0,    0},
/*  2: vscsi0 at root */
    {&vscsi_ca,		&vscsi_cd,	 0, NORM,     loc,    0, pv+18, 0,    0},
/*  3: softraid0 at root */
    {&softraid_ca,	&softraid_cd,	 0, NORM,     loc,    0, pv+18, 0,    0},
/*  4: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+18, 0,    0},
/*  5: cpu0 at mainbus0 */
    {&cpu_ca,		&cpu_cd,	 0, NORM,     loc,    0, pv+59, 32,    0},
/*  6: scsibus* at softraid0|vscsi0|umass*|atapiscsi* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+29, 0,    0},
/*  7: cd* at scsibus* target -1 lun -1 */
    {&cd_ca,		&cd_cd,		 0, STAR, loc+  4,    0, pv+49, 1,    0},
/*  8: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+  4,    0, pv+49, 1,    0},
/*  9: wd* at wdc0|pciide* channel -1 drive -1 */
    {&wd_ca,		&wd_cd,		 0, STAR, loc+  4,    0, pv+37, 4,    0},
/* 10: atapiscsi* at pciide* channel -1 */
    {&atapiscsi_ca,	&atapiscsi_cd,	 0, STAR, loc+ 10,    0, pv+38, 7,    0},
/* 11: eephy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&eephy_ca,		&eephy_cd,	 0, STAR, loc+ 10,    0, pv+19, 10,    0},
/* 12: rlphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&rlphy_ca,		&rlphy_cd,	 0, STAR, loc+ 10,    0, pv+19, 10,    0},
/* 13: ukphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+ 10,    0, pv+19, 10,    0},
/* 14: amphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&amphy_ca,		&amphy_cd,	 0, STAR, loc+ 10,    0, pv+19, 10,    0},
/* 15: acphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&acphy_ca,		&acphy_cd,	 0, STAR, loc+ 10,    0, pv+19, 10,    0},
/* 16: urlphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&urlphy_ca,	&urlphy_cd,	 0, STAR, loc+ 10,    0, pv+19, 10,    0},
/* 17: rgephy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&rgephy_ca,	&rgephy_cd,	 0, STAR, loc+ 10,    0, pv+19, 10,    0},
/* 18: pci0 at shpcic* bus -1 */
    {&pci_ca,		&pci_cd,	 0, NORM, loc+ 10,    0, pv+53, 28,    0},
/* 19: pciide* at pci0 dev -1 function -1 */
    {&pciide_pci_ca,	&pciide_cd,	 0, STAR, loc+  4,    0, pv+43, 30,    0},
/* 20: rl* at pci0 dev -1 function -1 */
    {&rl_pci_ca,	&rl_cd,		 0, STAR, loc+  4,    0, pv+43, 30,    0},
/* 21: re* at pci0 dev -1 function -1 */
    {&re_pci_ca,	&re_cd,		 0, STAR, loc+  4,    0, pv+43, 30,    0},
/* 22: ohci* at pci0 dev -1 function -1 */
    {&ohci_pci_ca,	&ohci_cd,	 0, STAR, loc+  4,    0, pv+43, 30,    0},
/* 23: ehci* at pci0 dev -1 function -1 */
    {&ehci_pci_ca,	&ehci_cd,	 0, STAR, loc+  4,    0, pv+43, 30,    0},
/* 24: shpcic* at mainbus0 */
    {&shpcic_ca,	&shpcic_cd,	 0, STAR,     loc,    0, pv+59, 32,    0},
/* 25: wsdisplay* at udl* console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+ 16,    0, pv+51, 33,    0},
/* 26: wskbd* at ukbd* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+ 17,    0, pv+47, 37,    0},
/* 27: wsmouse* at ums* mux 0 */
    {&wsmouse_ca,	&wsmouse_cd,	 0, STAR, loc+ 19,    0, pv+55, 40,    0},
/* 28: usb* at ehci*|ohci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+40, 41,    0},
/* 29: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+63, 41,    0},
/* 30: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 31: uaudio* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uaudio_ca,	&uaudio_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 32: udl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udl_ca,		&udl_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 33: ucom* at umodem*|uvisor*|uvscom*|ubsa*|uftdi*|uplcom*|umct*|uslcom*|uscom*|ucrcom*|uark*|moscom*|umcs*|uipaq*|umsm*|uchcom*|ucycom*|uslhcom* portno -1 */
    {&ucom_ca,		&ucom_cd,	 0, STAR, loc+ 10,    0, pv+ 0, 56,    0},
/* 34: ugen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugen_ca,		&ugen_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 35: uhidev* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhidev_ca,	&uhidev_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 36: uhid* at uhidev* reportid -1 */
    {&uhid_ca,		&uhid_cd,	 0, STAR, loc+ 10,    0, pv+45, 92,    0},
/* 37: fido* at uhidev* reportid -1 */
    {&fido_ca,		&fido_cd,	 0, STAR, loc+ 10,    0, pv+45, 92,    0},
/* 38: ujoy* at uhidev* reportid -1 */
    {&ujoy_ca,		&ujoy_cd,	 0, STAR, loc+ 10,    0, pv+45, 92,    0},
/* 39: ukbd* at uhidev* reportid -1 */
    {&ukbd_ca,		&ukbd_cd,	 0, STAR, loc+ 10,    0, pv+45, 92,    0},
/* 40: ums* at uhidev* reportid -1 */
    {&ums_ca,		&ums_cd,	 0, STAR, loc+ 10,    0, pv+45, 92,    0},
/* 41: ucycom* at uhidev* reportid -1 */
    {&ucycom_ca,	&ucycom_cd,	 0, STAR, loc+ 10,    0, pv+45, 92,    0},
/* 42: uslhcom* at uhidev* reportid -1 */
    {&uslhcom_ca,	&uslhcom_cd,	 0, STAR, loc+ 10,    0, pv+45, 92,    0},
/* 43: ulpt* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ulpt_ca,		&ulpt_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 44: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 45: uthum* at uhidev* reportid -1 */
    {&uthum_ca,		&uthum_cd,	 0, STAR, loc+ 10,    0, pv+45, 92,    0},
/* 46: ugold* at uhidev* reportid -1 */
    {&ugold_ca,		&ugold_cd,	 0, STAR, loc+ 10,    0, pv+45, 92,    0},
/* 47: uonerng* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uonerng_ca,	&uonerng_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 48: urng* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urng_ca,		&urng_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 49: udcf* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udcf_ca,		&udcf_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 50: uvisor* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvisor_ca,	&uvisor_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 51: udsbr* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udsbr_ca,		&udsbr_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 52: utwitch* at uhidev* reportid -1 */
    {&utwitch_ca,	&utwitch_cd,	 0, STAR, loc+ 10,    0, pv+45, 92,    0},
/* 53: aue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&aue_ca,		&aue_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 54: axe* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axe_ca,		&axe_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 55: axen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axen_ca,		&axen_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 56: smsc* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&smsc_ca,		&smsc_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 57: cue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cue_ca,		&cue_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 58: kue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&kue_ca,		&kue_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 59: cdce* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cdce_ca,		&cdce_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 60: urndis* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urndis_ca,	&urndis_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 61: udav* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udav_ca,		&udav_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 62: upl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upl_ca,		&upl_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 63: ugl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugl_ca,		&ugl_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 64: url* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&url_ca,		&url_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 65: ure* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ure_ca,		&ure_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 66: umodem* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umodem_ca,	&umodem_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 67: uftdi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uftdi_ca,		&uftdi_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 68: uplcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uplcom_ca,	&uplcom_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 69: umct* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umct_ca,		&umct_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 70: uvscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvscom_ca,	&uvscom_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 71: ubsa* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ubsa_ca,		&ubsa_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 72: uslcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uslcom_ca,	&uslcom_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 73: uark* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uark_ca,		&uark_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 74: moscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&moscom_ca,	&moscom_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 75: umcs* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umcs_ca,		&umcs_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 76: uscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uscom_ca,		&uscom_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 77: ucrcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ucrcom_ca,	&ucrcom_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 78: uipaq* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uipaq_ca,		&uipaq_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 79: umsm* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umsm_ca,		&umsm_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 80: uchcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uchcom_ca,	&uchcom_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 81: wi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&wi_usb_ca,	&wi_cd,		 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 82: atu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&atu_ca,		&atu_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 83: ural* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ural_ca,		&ural_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 84: rum* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rum_ca,		&rum_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 85: run* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&run_ca,		&run_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 86: zyd* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&zyd_ca,		&zyd_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 87: urtwn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtwn_ca,		&urtwn_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 88: rsu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rsu_ca,		&rsu_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 89: uath* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uath_ca,		&uath_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 90: athn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&athn_usb_ca,	&athn_cd,	 0, STAR, loc+  0,    0, pv+34, 42,    0},
/* 91: upd* at uhidev* reportid -1 */
    {&upd_ca,		&upd_cd,	 0, STAR, loc+ 10,    0, pv+45, 92,    0},
/* 92: uhidpp* at uhidev* reportid -1 */
    {&uhidpp_ca,	&uhidpp_cd,	 0, STAR, loc+ 10,    0, pv+45, 92,    0},
/* 93: shb* at mainbus0 */
    {&shb_ca,		&shb_cd,	 0, STAR,     loc,    0, pv+59, 32,    0},
/* 94: scif0 at shb* */
    {&scif_ca,		&scif_cd,	 0, NORM,     loc,    0, pv+65, 93,    0},
/* 95: rsclock0 at shb* */
    {&rsclock_ca,	&rsclock_cd,	 0, NORM,     loc,    0, pv+65, 93,    0},
/* 96: obio0 at mainbus0 */
    {&obio_ca,		&obio_cd,	 0, NORM,     loc,    0, pv+59, 32,    0},
/* 97: wdc0 at obio0 port 0x14000000 size 0 iomem -1 iosiz 0 irq 0xa */
    {&wdc_obio_ca,	&wdc_cd,	 0, NORM, loc+ 11,    0, pv+57, 94,    0},
/* 98: power0 at obio0 port -1 size 0 iomem -1 iosiz 0 irq -1 */
    {&power_ca,		&power_cd,	 0, NORM, loc+  6,    0, pv+57, 94,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 2 /* vscsi0 */,
	 3 /* softraid0 */,
	 4 /* mainbus0 */,
	-1
};

int cfroots_size = 4;

/* pseudo-devices */
extern void pfattach(int);
extern void pflogattach(int);
extern void pfsyncattach(int);
extern void pflowattach(int);
extern void encattach(int);
extern void ptyattach(int);
extern void nmeaattach(int);
extern void mstsattach(int);
extern void endrunattach(int);
extern void vndattach(int);
extern void ksymsattach(int);
extern void bpfilterattach(int);
extern void bridgeattach(int);
extern void vebattach(int);
extern void carpattach(int);
extern void etheripattach(int);
extern void gifattach(int);
extern void greattach(int);
extern void loopattach(int);
extern void mpeattach(int);
extern void mpwattach(int);
extern void mpipattach(int);
extern void bpeattach(int);
extern void pairattach(int);
extern void pppattach(int);
extern void pppoeattach(int);
extern void pppxattach(int);
extern void spppattach(int);
extern void trunkattach(int);
extern void aggrattach(int);
extern void tpmrattach(int);
extern void tunattach(int);
extern void vetherattach(int);
extern void vxlanattach(int);
extern void vlanattach(int);
extern void switchattach(int);
extern void wgattach(int);
extern void bioattach(int);
extern void fuseattach(int);
extern void hotplugattach(int);
extern void wsmuxattach(int);

char *pdevnames[] = {
	"pf",
	"pflog",
	"pfsync",
	"pflow",
	"enc",
	"pty",
	"nmea",
	"msts",
	"endrun",
	"vnd",
	"ksyms",
	"bpfilter",
	"bridge",
	"veb",
	"carp",
	"etherip",
	"gif",
	"gre",
	"loop",
	"mpe",
	"mpw",
	"mpip",
	"bpe",
	"pair",
	"ppp",
	"pppoe",
	"pppx",
	"sppp",
	"trunk",
	"aggr",
	"tpmr",
	"tun",
	"vether",
	"vxlan",
	"vlan",
	"switch",
	"wg",
	"bio",
	"fuse",
	"hotplug",
	"wsmux",
};

int pdevnames_size = 41;

struct pdevinit pdevinit[] = {
	{ pfattach, 1 },
	{ pflogattach, 1 },
	{ pfsyncattach, 1 },
	{ pflowattach, 1 },
	{ encattach, 1 },
	{ ptyattach, 16 },
	{ nmeaattach, 1 },
	{ mstsattach, 1 },
	{ endrunattach, 1 },
	{ vndattach, 4 },
	{ ksymsattach, 1 },
	{ bpfilterattach, 1 },
	{ bridgeattach, 1 },
	{ vebattach, 1 },
	{ carpattach, 1 },
	{ etheripattach, 1 },
	{ gifattach, 1 },
	{ greattach, 1 },
	{ loopattach, 1 },
	{ mpeattach, 1 },
	{ mpwattach, 1 },
	{ mpipattach, 1 },
	{ bpeattach, 1 },
	{ pairattach, 1 },
	{ pppattach, 1 },
	{ pppoeattach, 1 },
	{ pppxattach, 1 },
	{ spppattach, 1 },
	{ trunkattach, 1 },
	{ aggrattach, 1 },
	{ tpmrattach, 1 },
	{ tunattach, 1 },
	{ vetherattach, 1 },
	{ vxlanattach, 1 },
	{ vlanattach, 1 },
	{ switchattach, 1 },
	{ wgattach, 1 },
	{ bioattach, 1 },
	{ fuseattach, 1 },
	{ hotplugattach, 1 },
	{ wsmuxattach, 2 },
	{ NULL, 0 }
};
