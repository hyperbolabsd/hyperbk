/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/landisk/conf/RAMDISK"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver wdc_cd;
extern struct cfdriver wi_cd;
extern struct cfdriver rl_cd;
extern struct cfdriver re_cd;
extern struct cfdriver ohci_cd;
extern struct cfdriver ehci_cd;
extern struct cfdriver wd_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver cd_cd;
extern struct cfdriver atapiscsi_cd;
extern struct cfdriver eephy_cd;
extern struct cfdriver rlphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver amphy_cd;
extern struct cfdriver acphy_cd;
extern struct cfdriver urlphy_cd;
extern struct cfdriver rgephy_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver pciide_cd;
extern struct cfdriver shpcic_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver ugen_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver aue_cd;
extern struct cfdriver axe_cd;
extern struct cfdriver axen_cd;
extern struct cfdriver smsc_cd;
extern struct cfdriver cue_cd;
extern struct cfdriver kue_cd;
extern struct cfdriver cdce_cd;
extern struct cfdriver udav_cd;
extern struct cfdriver upl_cd;
extern struct cfdriver ugl_cd;
extern struct cfdriver url_cd;
extern struct cfdriver ure_cd;
extern struct cfdriver atu_cd;
extern struct cfdriver ural_cd;
extern struct cfdriver rum_cd;
extern struct cfdriver zyd_cd;
extern struct cfdriver shb_cd;
extern struct cfdriver scif_cd;
extern struct cfdriver rsclock_cd;
extern struct cfdriver obio_cd;

extern struct cfattach mainbus_ca;
extern struct cfattach cpu_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach cd_ca;
extern struct cfattach sd_ca;
extern struct cfattach wd_ca;
extern struct cfattach atapiscsi_ca;
extern struct cfattach eephy_ca;
extern struct cfattach rlphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach amphy_ca;
extern struct cfattach acphy_ca;
extern struct cfattach urlphy_ca;
extern struct cfattach rgephy_ca;
extern struct cfattach pci_ca;
extern struct cfattach pciide_pci_ca;
extern struct cfattach rl_pci_ca;
extern struct cfattach re_pci_ca;
extern struct cfattach ohci_pci_ca;
extern struct cfattach ehci_pci_ca;
extern struct cfattach shpcic_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach ugen_ca;
extern struct cfattach umass_ca;
extern struct cfattach aue_ca;
extern struct cfattach axe_ca;
extern struct cfattach axen_ca;
extern struct cfattach smsc_ca;
extern struct cfattach cue_ca;
extern struct cfattach kue_ca;
extern struct cfattach cdce_ca;
extern struct cfattach udav_ca;
extern struct cfattach upl_ca;
extern struct cfattach ugl_ca;
extern struct cfattach url_ca;
extern struct cfattach ure_ca;
extern struct cfattach wi_usb_ca;
extern struct cfattach atu_ca;
extern struct cfattach ural_ca;
extern struct cfattach rum_ca;
extern struct cfattach zyd_ca;
extern struct cfattach shb_ca;
extern struct cfattach scif_ca;
extern struct cfattach rsclock_ca;
extern struct cfattach obio_ca;
extern struct cfattach wdc_obio_ca;


/* locators */
static long loc[11] = {
	-1, -1, -1, -1, -1, -1, 0x14000000, 0,
	-1, 0, 0xa,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"target",
	"lun",
	"channel",
	"drive",
	"phy",
	"bus",
	"dev",
	"function",
	"port",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"size",
	"iomem",
	"iosiz",
	"irq",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, 1, -1, 2, 3, -1, 2,
	3, -1, 4, -1, 4, -1, 4, -1,
	4, -1, 4, -1, 4, -1, 4, -1,
	4, -1, 4, -1, 5, -1, 6, 7,
	-1, 8, 9, 10, 11, 12, 13, -1,
	8, 9, 10, 11, 12, 13, -1, 8,
	14, 15, 16, 17, -1,
};

/* size of parent vectors */
int pv_size = 36;

/* parent vectors */
short pv[36] = {
	37, 36, 33, 29, 28, 27, 26, 17, 16, -1, 22, 23, -1, 47, 15, -1,
	19, 18, -1, 25, 6, -1, 14, -1, 46, -1, 20, -1, 21, -1, 0, -1,
	43, -1, 2, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+ 9, 0,    0},
/*  1: cpu0 at mainbus0 */
    {&cpu_ca,		&cpu_cd,	 0, NORM,     loc,    0, pv+30, 32,    0},
/*  2: scsibus* at umass*|atapiscsi* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+19, 0,    0},
/*  3: cd* at scsibus* target -1 lun -1 */
    {&cd_ca,		&cd_cd,		 0, STAR, loc+  4,    0, pv+34, 1,    0},
/*  4: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+  4,    0, pv+34, 1,    0},
/*  5: wd* at wdc0|pciide* channel -1 drive -1 */
    {&wd_ca,		&wd_cd,		 0, STAR, loc+  4,    0, pv+13, 4,    0},
/*  6: atapiscsi* at pciide* channel -1 */
    {&atapiscsi_ca,	&atapiscsi_cd,	 0, STAR, loc+  5,    0, pv+14, 7,    0},
/*  7: eephy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&eephy_ca,		&eephy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 10,    0},
/*  8: rlphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&rlphy_ca,		&rlphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 10,    0},
/*  9: ukphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 10,    0},
/* 10: amphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&amphy_ca,		&amphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 10,    0},
/* 11: acphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&acphy_ca,		&acphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 10,    0},
/* 12: urlphy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&urlphy_ca,	&urlphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 10,    0},
/* 13: rgephy* at ure*|url*|udav*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&rgephy_ca,	&rgephy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 10,    0},
/* 14: pci0 at shpcic* bus -1 */
    {&pci_ca,		&pci_cd,	 0, NORM, loc+  5,    0, pv+26, 28,    0},
/* 15: pciide* at pci0 dev -1 function -1 */
    {&pciide_pci_ca,	&pciide_cd,	 0, STAR, loc+  4,    0, pv+22, 30,    0},
/* 16: rl* at pci0 dev -1 function -1 */
    {&rl_pci_ca,	&rl_cd,		 0, STAR, loc+  4,    0, pv+22, 30,    0},
/* 17: re* at pci0 dev -1 function -1 */
    {&re_pci_ca,	&re_cd,		 0, STAR, loc+  4,    0, pv+22, 30,    0},
/* 18: ohci* at pci0 dev -1 function -1 */
    {&ohci_pci_ca,	&ohci_cd,	 0, STAR, loc+  4,    0, pv+22, 30,    0},
/* 19: ehci* at pci0 dev -1 function -1 */
    {&ehci_pci_ca,	&ehci_cd,	 0, STAR, loc+  4,    0, pv+22, 30,    0},
/* 20: shpcic* at mainbus0 */
    {&shpcic_ca,	&shpcic_cd,	 0, STAR,     loc,    0, pv+30, 32,    0},
/* 21: usb* at ehci*|ohci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+16, 32,    0},
/* 22: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+28, 32,    0},
/* 23: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+  0,    0, pv+10, 33,    0},
/* 24: ugen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugen_ca,		&ugen_cd,	 0, STAR, loc+  0,    0, pv+10, 33,    0},
/* 25: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+  0,    0, pv+10, 33,    0},
/* 26: aue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&aue_ca,		&aue_cd,	 0, STAR, loc+  0,    0, pv+10, 33,    0},
/* 27: axe* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axe_ca,		&axe_cd,	 0, STAR, loc+  0,    0, pv+10, 33,    0},
/* 28: axen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axen_ca,		&axen_cd,	 0, STAR, loc+  0,    0, pv+10, 33,    0},
/* 29: smsc* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&smsc_ca,		&smsc_cd,	 0, STAR, loc+  0,    0, pv+10, 33,    0},
/* 30: cue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cue_ca,		&cue_cd,	 0, STAR, loc+  0,    0, pv+10, 33,    0},
/* 31: kue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&kue_ca,		&kue_cd,	 0, STAR, loc+  0,    0, pv+10, 33,    0},
/* 32: cdce* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cdce_ca,		&cdce_cd,	 0, STAR, loc+  0,    0, pv+10, 33,    0},
/* 33: udav* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udav_ca,		&udav_cd,	 0, STAR, loc+  0,    0, pv+10, 33,    0},
/* 34: upl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upl_ca,		&upl_cd,	 0, STAR, loc+  0,    0, pv+10, 33,    0},
/* 35: ugl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugl_ca,		&ugl_cd,	 0, STAR, loc+  0,    0, pv+10, 33,    0},
/* 36: url* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&url_ca,		&url_cd,	 0, STAR, loc+  0,    0, pv+10, 33,    0},
/* 37: ure* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ure_ca,		&ure_cd,	 0, STAR, loc+  0,    0, pv+10, 33,    0},
/* 38: wi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&wi_usb_ca,	&wi_cd,		 0, STAR, loc+  0,    0, pv+10, 33,    0},
/* 39: atu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&atu_ca,		&atu_cd,	 0, STAR, loc+  0,    0, pv+10, 33,    0},
/* 40: ural* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ural_ca,		&ural_cd,	 0, STAR, loc+  0,    0, pv+10, 33,    0},
/* 41: rum* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rum_ca,		&rum_cd,	 0, STAR, loc+  0,    0, pv+10, 33,    0},
/* 42: zyd* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&zyd_ca,		&zyd_cd,	 0, STAR, loc+  0,    0, pv+10, 33,    0},
/* 43: shb* at mainbus0 */
    {&shb_ca,		&shb_cd,	 0, STAR,     loc,    0, pv+30, 32,    0},
/* 44: scif0 at shb* */
    {&scif_ca,		&scif_cd,	 0, NORM,     loc,    0, pv+32, 46,    0},
/* 45: rsclock0 at shb* */
    {&rsclock_ca,	&rsclock_cd,	 0, NORM,     loc,    0, pv+32, 46,    0},
/* 46: obio0 at mainbus0 */
    {&obio_ca,		&obio_cd,	 0, NORM,     loc,    0, pv+30, 32,    0},
/* 47: wdc0 at obio0 port 0x14000000 size 0 iomem -1 iosiz 0 irq 0xa */
    {&wdc_obio_ca,	&wdc_cd,	 0, NORM, loc+  6,    0, pv+24, 47,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 0 /* mainbus0 */,
	-1
};

int cfroots_size = 2;

/* pseudo-devices */
extern void wsmuxattach(int);
extern void loopattach(int);
extern void bpfilterattach(int);
extern void rdattach(int);

char *pdevnames[] = {
	"wsmux",
	"loop",
	"bpfilter",
	"rd",
};

int pdevnames_size = 4;

struct pdevinit pdevinit[] = {
	{ wsmuxattach, 2 },
	{ loopattach, 1 },
	{ bpfilterattach, 1 },
	{ rdattach, 1 },
	{ NULL, 0 }
};
