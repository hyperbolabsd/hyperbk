/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/loongson/conf/GENERIC"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver video_cd;
extern struct cfdriver audio_cd;
extern struct cfdriver midi_cd;
extern struct cfdriver drm_cd;
extern struct cfdriver vga_cd;
extern struct cfdriver ahci_cd;
extern struct cfdriver wi_cd;
extern struct cfdriver rl_cd;
extern struct cfdriver re_cd;
extern struct cfdriver com_cd;
extern struct cfdriver pckbc_cd;
extern struct cfdriver athn_cd;
extern struct cfdriver bwfm_cd;
extern struct cfdriver ral_cd;
extern struct cfdriver uhci_cd;
extern struct cfdriver ohci_cd;
extern struct cfdriver ehci_cd;
extern struct cfdriver vscsi_cd;
extern struct cfdriver softraid_cd;
extern struct cfdriver spdmem_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver cd_cd;
extern struct cfdriver wd_cd;
extern struct cfdriver iic_cd;
extern struct cfdriver lmtemp_cd;
extern struct cfdriver mfokrtc_cd;
extern struct cfdriver gpio_cd;
extern struct cfdriver isa_cd;
extern struct cfdriver eephy_cd;
extern struct cfdriver rlphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver bmtphy_cd;
extern struct cfdriver amphy_cd;
extern struct cfdriver acphy_cd;
extern struct cfdriver urlphy_cd;
extern struct cfdriver rgephy_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver azalia_cd;
extern struct cfdriver auvia_cd;
extern struct cfdriver pciide_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver em_cd;
extern struct cfdriver piixpm_cd;
extern struct cfdriver viapm_cd;
extern struct cfdriver auglx_cd;
extern struct cfdriver glxpcib_cd;
extern struct cfdriver radeondrm_cd;
extern struct cfdriver pckbd_cd;
extern struct cfdriver pms_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver uaudio_cd;
extern struct cfdriver uvideo_cd;
extern struct cfdriver utvfu_cd;
extern struct cfdriver udl_cd;
extern struct cfdriver umidi_cd;
extern struct cfdriver ucom_cd;
extern struct cfdriver ugen_cd;
extern struct cfdriver uhidev_cd;
extern struct cfdriver uhid_cd;
extern struct cfdriver fido_cd;
extern struct cfdriver ujoy_cd;
extern struct cfdriver ukbd_cd;
extern struct cfdriver ums_cd;
extern struct cfdriver uslhcom_cd;
extern struct cfdriver ulpt_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver uthum_cd;
extern struct cfdriver ugold_cd;
extern struct cfdriver uonerng_cd;
extern struct cfdriver urng_cd;
extern struct cfdriver udcf_cd;
extern struct cfdriver uvisor_cd;
extern struct cfdriver utwitch_cd;
extern struct cfdriver aue_cd;
extern struct cfdriver axe_cd;
extern struct cfdriver axen_cd;
extern struct cfdriver smsc_cd;
extern struct cfdriver cue_cd;
extern struct cfdriver kue_cd;
extern struct cfdriver cdce_cd;
extern struct cfdriver urndis_cd;
extern struct cfdriver mos_cd;
extern struct cfdriver udav_cd;
extern struct cfdriver upl_cd;
extern struct cfdriver ugl_cd;
extern struct cfdriver url_cd;
extern struct cfdriver ure_cd;
extern struct cfdriver umodem_cd;
extern struct cfdriver uftdi_cd;
extern struct cfdriver uplcom_cd;
extern struct cfdriver umct_cd;
extern struct cfdriver uvscom_cd;
extern struct cfdriver ubsa_cd;
extern struct cfdriver uslcom_cd;
extern struct cfdriver uark_cd;
extern struct cfdriver moscom_cd;
extern struct cfdriver umcs_cd;
extern struct cfdriver uscom_cd;
extern struct cfdriver ucrcom_cd;
extern struct cfdriver uipaq_cd;
extern struct cfdriver umsm_cd;
extern struct cfdriver uchcom_cd;
extern struct cfdriver atu_cd;
extern struct cfdriver ural_cd;
extern struct cfdriver rum_cd;
extern struct cfdriver run_cd;
extern struct cfdriver zyd_cd;
extern struct cfdriver upgt_cd;
extern struct cfdriver urtw_cd;
extern struct cfdriver urtwn_cd;
extern struct cfdriver rsu_cd;
extern struct cfdriver otus_cd;
extern struct cfdriver uath_cd;
extern struct cfdriver uberry_cd;
extern struct cfdriver upd_cd;
extern struct cfdriver uhidpp_cd;
extern struct cfdriver ucc_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver wsmouse_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver ch_cd;
extern struct cfdriver st_cd;
extern struct cfdriver uk_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver clock_cd;
extern struct cfdriver bonito_cd;
extern struct cfdriver htb_cd;
extern struct cfdriver pcib_cd;
extern struct cfdriver mcclock_cd;
extern struct cfdriver ykbec_cd;
extern struct cfdriver voyager_cd;
extern struct cfdriver gdiumiic_cd;
extern struct cfdriver stsec_cd;
extern struct cfdriver smfb_cd;
extern struct cfdriver sisfb_cd;
extern struct cfdriver radeonfb_cd;
extern struct cfdriver apm_cd;
extern struct cfdriver glxclk_cd;
extern struct cfdriver leioc_cd;

extern struct cfattach video_ca;
extern struct cfattach audio_ca;
extern struct cfattach midi_ca;
extern struct cfattach drm_ca;
extern struct cfattach vscsi_ca;
extern struct cfattach softraid_ca;
extern struct cfattach wd_ca;
extern struct cfattach iic_ca;
extern struct cfattach lmtemp_ca;
extern struct cfattach spdmem_iic_ca;
extern struct cfattach mfokrtc_ca;
extern struct cfattach gpio_ca;
extern struct cfattach isa_ca;
extern struct cfattach com_isa_ca;
extern struct cfattach pckbc_isa_ca;
extern struct cfattach eephy_ca;
extern struct cfattach rlphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach bmtphy_ca;
extern struct cfattach amphy_ca;
extern struct cfattach acphy_ca;
extern struct cfattach urlphy_ca;
extern struct cfattach rgephy_ca;
extern struct cfattach pci_ca;
extern struct cfattach vga_pci_ca;
extern struct cfattach ahci_pci_ca;
extern struct cfattach azalia_ca;
extern struct cfattach auvia_ca;
extern struct cfattach pciide_pci_ca;
extern struct cfattach ppb_ca;
extern struct cfattach rl_pci_ca;
extern struct cfattach re_pci_ca;
extern struct cfattach em_ca;
extern struct cfattach uhci_pci_ca;
extern struct cfattach ohci_pci_ca;
extern struct cfattach ehci_pci_ca;
extern struct cfattach ral_pci_ca;
extern struct cfattach piixpm_ca;
extern struct cfattach viapm_ca;
extern struct cfattach auglx_ca;
extern struct cfattach glxpcib_ca;
extern struct cfattach radeondrm_ca;
extern struct cfattach pckbd_ca;
extern struct cfattach pms_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach uaudio_ca;
extern struct cfattach uvideo_ca;
extern struct cfattach utvfu_ca;
extern struct cfattach udl_ca;
extern struct cfattach umidi_ca;
extern struct cfattach ucom_ca;
extern struct cfattach ugen_ca;
extern struct cfattach uhidev_ca;
extern struct cfattach uhid_ca;
extern struct cfattach fido_ca;
extern struct cfattach ujoy_ca;
extern struct cfattach ukbd_ca;
extern struct cfattach ums_ca;
extern struct cfattach uslhcom_ca;
extern struct cfattach ulpt_ca;
extern struct cfattach umass_ca;
extern struct cfattach uthum_ca;
extern struct cfattach ugold_ca;
extern struct cfattach uonerng_ca;
extern struct cfattach urng_ca;
extern struct cfattach udcf_ca;
extern struct cfattach uvisor_ca;
extern struct cfattach utwitch_ca;
extern struct cfattach aue_ca;
extern struct cfattach axe_ca;
extern struct cfattach axen_ca;
extern struct cfattach smsc_ca;
extern struct cfattach cue_ca;
extern struct cfattach kue_ca;
extern struct cfattach cdce_ca;
extern struct cfattach urndis_ca;
extern struct cfattach mos_ca;
extern struct cfattach udav_ca;
extern struct cfattach upl_ca;
extern struct cfattach ugl_ca;
extern struct cfattach url_ca;
extern struct cfattach ure_ca;
extern struct cfattach umodem_ca;
extern struct cfattach uftdi_ca;
extern struct cfattach uplcom_ca;
extern struct cfattach umct_ca;
extern struct cfattach uvscom_ca;
extern struct cfattach ubsa_ca;
extern struct cfattach uslcom_ca;
extern struct cfattach uark_ca;
extern struct cfattach moscom_ca;
extern struct cfattach umcs_ca;
extern struct cfattach uscom_ca;
extern struct cfattach ucrcom_ca;
extern struct cfattach uipaq_ca;
extern struct cfattach umsm_ca;
extern struct cfattach uchcom_ca;
extern struct cfattach wi_usb_ca;
extern struct cfattach atu_ca;
extern struct cfattach ural_ca;
extern struct cfattach rum_ca;
extern struct cfattach run_ca;
extern struct cfattach zyd_ca;
extern struct cfattach upgt_ca;
extern struct cfattach urtw_ca;
extern struct cfattach urtwn_ca;
extern struct cfattach rsu_ca;
extern struct cfattach otus_ca;
extern struct cfattach uath_ca;
extern struct cfattach athn_usb_ca;
extern struct cfattach uberry_ca;
extern struct cfattach upd_ca;
extern struct cfattach bwfm_usb_ca;
extern struct cfattach uhidpp_ca;
extern struct cfattach ucc_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach wsmouse_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach cd_ca;
extern struct cfattach ch_ca;
extern struct cfattach sd_ca;
extern struct cfattach st_ca;
extern struct cfattach uk_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach cpu_ca;
extern struct cfattach clock_ca;
extern struct cfattach bonito_ca;
extern struct cfattach htb_ca;
extern struct cfattach pcib_ca;
extern struct cfattach mcclock_isa_ca;
extern struct cfattach ykbec_ca;
extern struct cfattach voyager_ca;
extern struct cfattach gdiumiic_ca;
extern struct cfattach stsec_ca;
extern struct cfattach ohci_voyager_ca;
extern struct cfattach smfb_pci_ca;
extern struct cfattach smfb_voyager_ca;
extern struct cfattach sisfb_ca;
extern struct cfattach radeonfb_ca;
extern struct cfattach apm_ca;
extern struct cfattach glxclk_ca;
extern struct cfattach leioc_ca;
extern struct cfattach com_leioc_ca;


/* locators */
static long loc[50] = {
	-1, 0, -1, 0, -1, -1, -1, 0x3f8,
	0, -1, 0, 4, -1, -1, 0x2f8, 0,
	-1, 0, 3, -1, -1, 0x70, 0, -1,
	0, -1, -1, -1, 0x381, 0, -1, 0,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, 0x2e, 3, 0, -1, -1, 1, 6,
	0x81, 0,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"primary",
	"channel",
	"drive",
	"addr",
	"size",
	"port",
	"iomem",
	"iosiz",
	"irq",
	"drq",
	"drq2",
	"phy",
	"bus",
	"dev",
	"function",
	"slot",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"portno",
	"reportid",
	"console",
	"mux",
	"target",
	"lun",
	"offset",
	"mask",
	"flag",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, -1, 1, 2, -1, 3, 4,
	-1, 3, 4, -1, 5, 4, 6, 7,
	8, 9, 10, -1, 11, -1, 11, -1,
	11, -1, 11, -1, 11, -1, 11, -1,
	11, -1, 11, -1, 11, -1, 11, -1,
	12, -1, 12, -1, 12, -1, 13, 14,
	-1, 15, -1, 5, 16, 17, 18, 19,
	20, -1, 5, 16, 17, 18, 19, 20,
	-1, 21, -1, 21, -1, 21, -1, 21,
	-1, 21, -1, 21, -1, 21, -1, 21,
	-1, 21, -1, 21, -1, 21, -1, 21,
	-1, 21, -1, 21, -1, 21, -1, 21,
	-1, 21, -1, 22, -1, 23, 0, 24,
	-1, 23, 0, 24, -1, 23, 0, 24,
	-1, 23, 0, 24, -1, 23, 0, 24,
	-1, 23, 0, 24, -1, 23, 24, -1,
	23, 24, -1, 23, 24, -1, 24, -1,
	24, -1, 25, 26, -1, 27, 28, 29,
	-1,
};

/* size of parent vectors */
int pv_size = 111;

/* parent vectors */
short pv[111] = {
	87, 71, 91, 92, 88, 89, 90, 93, 97, 98, 94, 95, 96, 99, 100, 101,
	63, -1, 86, 85, 82, 81, 76, 75, 74, 73, 34, 33, -1, 144, 142, 143,
	44, 145, 27, 53, -1, 42, 29, 30, 50, 52, -1, 5, 4, 65, 28, -1,
	141, 37, 36, 38, -1, 45, 61, 119, -1, 132, 133, 32, -1, 48, 49, -1,
	46, 62, -1, 51, 52, -1, 43, 134, -1, 138, 139, -1, 7, 8, -1, 129,
	-1, 14, -1, 13, -1, 123, -1, 7, -1, 26, -1, 31, -1, 44, -1, 17,
	-1, 57, -1, 148, -1, 47, -1, 137, -1, 43, -1, 40, -1, 54, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: video* at uvideo*|utvfu* */
    {&video_ca,		&video_cd,	 0, STAR,     loc,    0, pv+67, 0,    0},
/*  1: audio* at auglx*|azalia*|auvia*|uaudio*|utvfu* */
    {&audio_ca,		&audio_cd,	 0, STAR,     loc,    0, pv+37, 0,    0},
/*  2: midi* at umidi* */
    {&midi_ca,		&midi_cd,	 0, STAR,     loc,    0, pv+109, 0,    0},
/*  3: drm* at radeondrm* primary -1 */
    {&drm_ca,		&drm_cd,	 0, STAR, loc+ 40,    0, pv+93, 1,    0},
/*  4: vscsi0 at root */
    {&vscsi_ca,		&vscsi_cd,	 0, NORM,     loc,    0, pv+17, 0,    0},
/*  5: softraid0 at root */
    {&softraid_ca,	&softraid_cd,	 0, NORM,     loc,    0, pv+17, 0,    0},
/*  6: wd* at pciide* channel -1 drive -1 */
    {&wd_ca,		&wd_cd,		 0, STAR, loc+ 39,    0, pv+91, 3,    0},
/*  7: iic0 at gdiumiic0|gdiumiic0 */
    {&iic_ca,		&iic_cd,	 0, NORM,     loc,    0, pv+73, 5,    0},
/*  8: iic* at piixpm* */
    {&iic_ca,		&iic_cd,	 1, STAR,     loc,    0, pv+107, 5,    1},
/*  9: lmtemp0 at iic0 addr -1 size -1 */
    {&lmtemp_ca,	&lmtemp_cd,	 0, NORM, loc+ 39,    0, pv+87, 6,    0},
/* 10: spdmem* at iic0|iic* addr -1 size -1 */
    {&spdmem_iic_ca,	&spdmem_cd,	 0, STAR, loc+ 39,    0, pv+76, 6,    0},
/* 11: mfokrtc0 at iic0 addr -1 size -1 */
    {&mfokrtc_ca,	&mfokrtc_cd,	 0, NORM, loc+ 39,    0, pv+87, 6,    0},
/* 12: gpio* at glxpcib* */
    {&gpio_ca,		&gpio_cd,	 1, STAR,     loc,    0, pv+105, 11,    1},
/* 13: gpio0 at voyager* */
    {&gpio_ca,		&gpio_cd,	 0, NORM,     loc,    0, pv+103, 11,    0},
/* 14: isa0 at glxpcib*|pcib* */
    {&isa_ca,		&isa_cd,	 0, NORM,     loc,    0, pv+70, 11,    0},
/* 15: com0 at isa0 port 0x2f8 size 0 iomem -1 iosiz 0 irq 3 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 0, NORM, loc+ 14,    0, pv+81, 12,    0},
/* 16: com1 at isa0 port 0x3f8 size 0 iomem -1 iosiz 0 irq 4 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 1, NORM, loc+  7,    0, pv+81, 12,    1},
/* 17: pckbc0 at isa0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&pckbc_isa_ca,	&pckbc_cd,	 0, NORM, loc+  0,    0, pv+81, 12,    0},
/* 18: eephy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&eephy_ca,		&eephy_cd,	 0, STAR, loc+ 40,    0, pv+18, 20,    0},
/* 19: rlphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&rlphy_ca,		&rlphy_cd,	 0, STAR, loc+ 40,    0, pv+18, 20,    0},
/* 20: ukphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+ 40,    0, pv+18, 20,    0},
/* 21: bmtphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&bmtphy_ca,	&bmtphy_cd,	 0, STAR, loc+ 40,    0, pv+18, 20,    0},
/* 22: amphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&amphy_ca,		&amphy_cd,	 0, STAR, loc+ 40,    0, pv+18, 20,    0},
/* 23: acphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&acphy_ca,		&acphy_cd,	 0, STAR, loc+ 40,    0, pv+18, 20,    0},
/* 24: urlphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&urlphy_ca,	&urlphy_cd,	 0, STAR, loc+ 40,    0, pv+18, 20,    0},
/* 25: rgephy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&rgephy_ca,	&rgephy_cd,	 0, STAR, loc+ 40,    0, pv+18, 20,    0},
/* 26: pci* at bonito*|htb*|ppb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+ 40,    0, pv+57, 40,    0},
/* 27: vga* at pci* dev -1 function -1 */
    {&vga_pci_ca,	&vga_cd,	 0, STAR, loc+ 39,    0, pv+89, 46,    0},
/* 28: ahci* at pci* dev -1 function -1 */
    {&ahci_pci_ca,	&ahci_cd,	 0, STAR, loc+ 39,    0, pv+89, 46,    0},
/* 29: azalia* at pci* dev -1 function -1 */
    {&azalia_ca,	&azalia_cd,	 0, STAR, loc+ 39,    0, pv+89, 46,    0},
/* 30: auvia* at pci* dev -1 function -1 */
    {&auvia_ca,		&auvia_cd,	 0, STAR, loc+ 39,    0, pv+89, 46,    0},
/* 31: pciide* at pci* dev -1 function -1 */
    {&pciide_pci_ca,	&pciide_cd,	 0, STAR, loc+ 39,    0, pv+89, 46,    0},
/* 32: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+ 39,    0, pv+89, 46,    0},
/* 33: rl* at pci* dev -1 function -1 */
    {&rl_pci_ca,	&rl_cd,		 0, STAR, loc+ 39,    0, pv+89, 46,    0},
/* 34: re* at pci* dev -1 function -1 */
    {&re_pci_ca,	&re_cd,		 0, STAR, loc+ 39,    0, pv+89, 46,    0},
/* 35: em* at pci* dev -1 function -1 */
    {&em_ca,		&em_cd,		 0, STAR, loc+ 39,    0, pv+89, 46,    0},
/* 36: uhci* at pci* dev -1 function -1 */
    {&uhci_pci_ca,	&uhci_cd,	 0, STAR, loc+ 39,    0, pv+89, 46,    0},
/* 37: ohci* at pci* dev -1 function -1 */
    {&ohci_pci_ca,	&ohci_cd,	 0, STAR, loc+ 39,    0, pv+89, 46,    0},
/* 38: ehci* at pci* dev -1 function -1 */
    {&ehci_pci_ca,	&ehci_cd,	 0, STAR, loc+ 39,    0, pv+89, 46,    0},
/* 39: ral* at pci* dev -1 function -1 */
    {&ral_pci_ca,	&ral_cd,	 0, STAR, loc+ 39,    0, pv+89, 46,    0},
/* 40: piixpm* at pci* dev -1 function -1 */
    {&piixpm_ca,	&piixpm_cd,	 0, STAR, loc+ 39,    0, pv+89, 46,    0},
/* 41: viapm* at pci* dev -1 function -1 */
    {&viapm_ca,		&viapm_cd,	 0, STAR, loc+ 39,    0, pv+89, 46,    0},
/* 42: auglx* at pci* dev -1 function -1 */
    {&auglx_ca,		&auglx_cd,	 0, STAR, loc+ 39,    0, pv+89, 46,    0},
/* 43: glxpcib* at pci* dev -1 function -1 */
    {&glxpcib_ca,	&glxpcib_cd,	 0, STAR, loc+ 39,    0, pv+89, 46,    0},
/* 44: radeondrm* at pci* dev -1 function -1 */
    {&radeondrm_ca,	&radeondrm_cd,	 0, STAR, loc+ 39,    0, pv+89, 46,    0},
/* 45: pckbd* at pckbc0 slot -1 */
    {&pckbd_ca,		&pckbd_cd,	 0, STAR, loc+ 40,    0, pv+95, 49,    0},
/* 46: pms* at pckbc0 slot -1 */
    {&pms_ca,		&pms_cd,	 0, STAR, loc+ 40,    0, pv+95, 49,    0},
/* 47: usb* at ohci*|ohci*|uhci*|ehci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+48, 50,    0},
/* 48: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+101, 50,    0},
/* 49: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 50: uaudio* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uaudio_ca,	&uaudio_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 51: uvideo* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvideo_ca,	&uvideo_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 52: utvfu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&utvfu_ca,		&utvfu_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 53: udl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udl_ca,		&udl_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 54: umidi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umidi_ca,		&umidi_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 55: ucom* at umodem*|uvisor*|uvscom*|ubsa*|uftdi*|uplcom*|umct*|uslcom*|uscom*|ucrcom*|uark*|moscom*|umcs*|uipaq*|umsm*|uchcom*|uslhcom* portno -1 */
    {&ucom_ca,		&ucom_cd,	 0, STAR, loc+ 40,    0, pv+ 0, 65,    0},
/* 56: ugen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugen_ca,		&ugen_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 57: uhidev* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhidev_ca,	&uhidev_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 58: uhid* at uhidev* reportid -1 */
    {&uhid_ca,		&uhid_cd,	 0, STAR, loc+ 40,    0, pv+97, 99,    0},
/* 59: fido* at uhidev* reportid -1 */
    {&fido_ca,		&fido_cd,	 0, STAR, loc+ 40,    0, pv+97, 99,    0},
/* 60: ujoy* at uhidev* reportid -1 */
    {&ujoy_ca,		&ujoy_cd,	 0, STAR, loc+ 40,    0, pv+97, 99,    0},
/* 61: ukbd* at uhidev* reportid -1 */
    {&ukbd_ca,		&ukbd_cd,	 0, STAR, loc+ 40,    0, pv+97, 99,    0},
/* 62: ums* at uhidev* reportid -1 */
    {&ums_ca,		&ums_cd,	 0, STAR, loc+ 40,    0, pv+97, 99,    0},
/* 63: uslhcom* at uhidev* reportid -1 */
    {&uslhcom_ca,	&uslhcom_cd,	 0, STAR, loc+ 40,    0, pv+97, 99,    0},
/* 64: ulpt* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ulpt_ca,		&ulpt_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 65: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 66: uthum* at uhidev* reportid -1 */
    {&uthum_ca,		&uthum_cd,	 0, STAR, loc+ 40,    0, pv+97, 99,    0},
/* 67: ugold* at uhidev* reportid -1 */
    {&ugold_ca,		&ugold_cd,	 0, STAR, loc+ 40,    0, pv+97, 99,    0},
/* 68: uonerng* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uonerng_ca,	&uonerng_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 69: urng* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urng_ca,		&urng_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 70: udcf* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udcf_ca,		&udcf_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 71: uvisor* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvisor_ca,	&uvisor_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 72: utwitch* at uhidev* reportid -1 */
    {&utwitch_ca,	&utwitch_cd,	 0, STAR, loc+ 40,    0, pv+97, 99,    0},
/* 73: aue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&aue_ca,		&aue_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 74: axe* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axe_ca,		&axe_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 75: axen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axen_ca,		&axen_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 76: smsc* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&smsc_ca,		&smsc_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 77: cue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cue_ca,		&cue_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 78: kue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&kue_ca,		&kue_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 79: cdce* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cdce_ca,		&cdce_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 80: urndis* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urndis_ca,	&urndis_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 81: mos* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mos_ca,		&mos_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 82: udav* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udav_ca,		&udav_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 83: upl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upl_ca,		&upl_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 84: ugl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugl_ca,		&ugl_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 85: url* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&url_ca,		&url_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 86: ure* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ure_ca,		&ure_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 87: umodem* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umodem_ca,	&umodem_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 88: uftdi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uftdi_ca,		&uftdi_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 89: uplcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uplcom_ca,	&uplcom_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 90: umct* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umct_ca,		&umct_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 91: uvscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvscom_ca,	&uvscom_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 92: ubsa* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ubsa_ca,		&ubsa_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 93: uslcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uslcom_ca,	&uslcom_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 94: uark* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uark_ca,		&uark_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 95: moscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&moscom_ca,	&moscom_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 96: umcs* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umcs_ca,		&umcs_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 97: uscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uscom_ca,		&uscom_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 98: ucrcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ucrcom_ca,	&ucrcom_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/* 99: uipaq* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uipaq_ca,		&uipaq_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/*100: umsm* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umsm_ca,		&umsm_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/*101: uchcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uchcom_ca,	&uchcom_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/*102: wi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&wi_usb_ca,	&wi_cd,		 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/*103: atu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&atu_ca,		&atu_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/*104: ural* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ural_ca,		&ural_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/*105: rum* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rum_ca,		&rum_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/*106: run* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&run_ca,		&run_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/*107: zyd* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&zyd_ca,		&zyd_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/*108: upgt* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upgt_ca,		&upgt_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/*109: urtw* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtw_ca,		&urtw_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/*110: urtwn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtwn_ca,		&urtwn_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/*111: rsu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rsu_ca,		&rsu_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/*112: otus* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&otus_ca,		&otus_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/*113: uath* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uath_ca,		&uath_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/*114: athn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&athn_usb_ca,	&athn_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/*115: uberry* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uberry_ca,	&uberry_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/*116: upd* at uhidev* reportid -1 */
    {&upd_ca,		&upd_cd,	 0, STAR, loc+ 40,    0, pv+97, 99,    0},
/*117: bwfm* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&bwfm_usb_ca,	&bwfm_cd,	 0, STAR, loc+ 35,    0, pv+61, 51,    0},
/*118: uhidpp* at uhidev* reportid -1 */
    {&uhidpp_ca,	&uhidpp_cd,	 0, STAR, loc+ 40,    0, pv+97, 99,    0},
/*119: ucc* at uhidev* reportid -1 */
    {&ucc_ca,		&ucc_cd,	 0, STAR, loc+ 40,    0, pv+97, 99,    0},
/*120: wsdisplay* at sisfb*|smfb*|smfb*|radeondrm*|radeonfb*|vga*|udl* console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+ 44,    0, pv+29, 101,    0},
/*121: wskbd* at pckbd*|ukbd*|ucc* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+ 45,    0, pv+53, 125,    0},
/*122: wsmouse* at pms*|ums* mux 0 */
    {&wsmouse_ca,	&wsmouse_cd,	 0, STAR, loc+ 49,    0, pv+64, 134,    0},
/*123: scsibus* at softraid0|vscsi0|umass*|ahci* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+43, 137,    0},
/*124: cd* at scsibus* target -1 lun -1 */
    {&cd_ca,		&cd_cd,		 0, STAR, loc+ 39,    0, pv+85, 138,    0},
/*125: ch* at scsibus* target -1 lun -1 */
    {&ch_ca,		&ch_cd,		 0, STAR, loc+ 39,    0, pv+85, 138,    0},
/*126: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+ 39,    0, pv+85, 138,    0},
/*127: st* at scsibus* target -1 lun -1 */
    {&st_ca,		&st_cd,		 0, STAR, loc+ 39,    0, pv+85, 138,    0},
/*128: uk* at scsibus* target -1 lun -1 */
    {&uk_ca,		&uk_cd,		 0, STAR, loc+ 39,    0, pv+85, 138,    0},
/*129: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+17, 0,    0},
/*130: cpu0 at mainbus0 */
    {&cpu_ca,		&cpu_cd,	 0, NORM,     loc,    0, pv+79, 140,    0},
/*131: clock0 at mainbus0 */
    {&clock_ca,		&clock_cd,	 0, NORM,     loc,    0, pv+79, 140,    0},
/*132: bonito* at mainbus0 */
    {&bonito_ca,	&bonito_cd,	 0, STAR,     loc,    0, pv+79, 140,    0},
/*133: htb* at mainbus0 */
    {&htb_ca,		&htb_cd,	 0, STAR,     loc,    0, pv+79, 140,    0},
/*134: pcib* at pci* dev -1 function -1 */
    {&pcib_ca,		&pcib_cd,	 0, STAR, loc+ 39,    0, pv+89, 46,    0},
/*135: mcclock0 at isa0 port 0x70 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&mcclock_isa_ca,	&mcclock_cd,	 0, NORM, loc+ 21,    0, pv+81, 12,    0},
/*136: ykbec0 at isa0 port 0x381 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&ykbec_ca,		&ykbec_cd,	 0, NORM, loc+ 28,    0, pv+81, 12,    0},
/*137: voyager* at pci* dev -1 function -1 */
    {&voyager_ca,	&voyager_cd,	 0, STAR, loc+ 39,    0, pv+89, 46,    0},
/*138: gdiumiic0 at gpio0 offset 6 mask 0x81 flag 0 */
    {&gdiumiic_ca,	&gdiumiic_cd,	 0, NORM, loc+ 47,    0, pv+83, 141,    0},
/*139: gdiumiic0 at gpio0 offset 0x2e mask 3 flag 0 */
    {&gdiumiic_ca,	&gdiumiic_cd,	 0, NORM, loc+ 41,    0, pv+83, 141,    0},
/*140: stsec0 at iic0 addr -1 size -1 */
    {&stsec_ca,		&stsec_cd,	 0, NORM, loc+ 39,    0, pv+87, 6,    0},
/*141: ohci* at voyager* */
    {&ohci_voyager_ca,	&ohci_cd,	 0, STAR,     loc,    0, pv+103, 11,    0},
/*142: smfb* at pci* dev -1 function -1 */
    {&smfb_pci_ca,	&smfb_cd,	 0, STAR, loc+ 39,    0, pv+89, 46,    0},
/*143: smfb* at voyager* */
    {&smfb_voyager_ca,	&smfb_cd,	 0, STAR,     loc,    0, pv+103, 11,    0},
/*144: sisfb* at pci* dev -1 function -1 */
    {&sisfb_ca,		&sisfb_cd,	 0, STAR, loc+ 39,    0, pv+89, 46,    0},
/*145: radeonfb* at pci* dev -1 function -1 */
    {&radeonfb_ca,	&radeonfb_cd,	 0, STAR, loc+ 39,    0, pv+89, 46,    0},
/*146: apm0 at mainbus0 */
    {&apm_ca,		&apm_cd,	 0, NORM,     loc,    0, pv+79, 140,    0},
/*147: glxclk* at glxpcib* */
    {&glxclk_ca,	&glxclk_cd,	 0, STAR,     loc,    0, pv+105, 11,    0},
/*148: leioc0 at mainbus0 */
    {&leioc_ca,		&leioc_cd,	 0, NORM,     loc,    0, pv+79, 140,    0},
/*149: com* at leioc0 */
    {&com_leioc_ca,	&com_cd,	 2, STAR,     loc,    0, pv+99, 144,    2},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 4 /* vscsi0 */,
	 5 /* softraid0 */,
	129 /* mainbus0 */,
	-1
};

int cfroots_size = 4;

/* pseudo-devices */
extern void pfattach(int);
extern void pflogattach(int);
extern void pfsyncattach(int);
extern void pflowattach(int);
extern void encattach(int);
extern void ptyattach(int);
extern void nmeaattach(int);
extern void mstsattach(int);
extern void endrunattach(int);
extern void vndattach(int);
extern void ksymsattach(int);
extern void bpfilterattach(int);
extern void bridgeattach(int);
extern void vebattach(int);
extern void carpattach(int);
extern void etheripattach(int);
extern void gifattach(int);
extern void greattach(int);
extern void loopattach(int);
extern void mpeattach(int);
extern void mpwattach(int);
extern void mpipattach(int);
extern void bpeattach(int);
extern void pairattach(int);
extern void pppattach(int);
extern void pppoeattach(int);
extern void pppxattach(int);
extern void spppattach(int);
extern void trunkattach(int);
extern void aggrattach(int);
extern void tpmrattach(int);
extern void tunattach(int);
extern void vetherattach(int);
extern void vxlanattach(int);
extern void vlanattach(int);
extern void switchattach(int);
extern void wgattach(int);
extern void bioattach(int);
extern void fuseattach(int);
extern void hotplugattach(int);
extern void wsmuxattach(int);

char *pdevnames[] = {
	"pf",
	"pflog",
	"pfsync",
	"pflow",
	"enc",
	"pty",
	"nmea",
	"msts",
	"endrun",
	"vnd",
	"ksyms",
	"bpfilter",
	"bridge",
	"veb",
	"carp",
	"etherip",
	"gif",
	"gre",
	"loop",
	"mpe",
	"mpw",
	"mpip",
	"bpe",
	"pair",
	"ppp",
	"pppoe",
	"pppx",
	"sppp",
	"trunk",
	"aggr",
	"tpmr",
	"tun",
	"vether",
	"vxlan",
	"vlan",
	"switch",
	"wg",
	"bio",
	"fuse",
	"hotplug",
	"wsmux",
};

int pdevnames_size = 41;

struct pdevinit pdevinit[] = {
	{ pfattach, 1 },
	{ pflogattach, 1 },
	{ pfsyncattach, 1 },
	{ pflowattach, 1 },
	{ encattach, 1 },
	{ ptyattach, 16 },
	{ nmeaattach, 1 },
	{ mstsattach, 1 },
	{ endrunattach, 1 },
	{ vndattach, 4 },
	{ ksymsattach, 1 },
	{ bpfilterattach, 1 },
	{ bridgeattach, 1 },
	{ vebattach, 1 },
	{ carpattach, 1 },
	{ etheripattach, 1 },
	{ gifattach, 1 },
	{ greattach, 1 },
	{ loopattach, 1 },
	{ mpeattach, 1 },
	{ mpwattach, 1 },
	{ mpipattach, 1 },
	{ bpeattach, 1 },
	{ pairattach, 1 },
	{ pppattach, 1 },
	{ pppoeattach, 1 },
	{ pppxattach, 1 },
	{ spppattach, 1 },
	{ trunkattach, 1 },
	{ aggrattach, 1 },
	{ tpmrattach, 1 },
	{ tunattach, 1 },
	{ vetherattach, 1 },
	{ vxlanattach, 1 },
	{ vlanattach, 1 },
	{ switchattach, 1 },
	{ wgattach, 1 },
	{ bioattach, 1 },
	{ fuseattach, 1 },
	{ hotplugattach, 1 },
	{ wsmuxattach, 2 },
	{ NULL, 0 }
};
