/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/loongson/conf/RAMDISK"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver vga_cd;
extern struct cfdriver ahci_cd;
extern struct cfdriver wi_cd;
extern struct cfdriver rl_cd;
extern struct cfdriver re_cd;
extern struct cfdriver com_cd;
extern struct cfdriver pckbc_cd;
extern struct cfdriver bwfm_cd;
extern struct cfdriver ral_cd;
extern struct cfdriver uhci_cd;
extern struct cfdriver ohci_cd;
extern struct cfdriver ehci_cd;
extern struct cfdriver spdmem_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver cd_cd;
extern struct cfdriver wd_cd;
extern struct cfdriver iic_cd;
extern struct cfdriver mfokrtc_cd;
extern struct cfdriver gpio_cd;
extern struct cfdriver isa_cd;
extern struct cfdriver eephy_cd;
extern struct cfdriver rlphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver bmtphy_cd;
extern struct cfdriver amphy_cd;
extern struct cfdriver acphy_cd;
extern struct cfdriver urlphy_cd;
extern struct cfdriver rgephy_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver pciide_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver em_cd;
extern struct cfdriver piixpm_cd;
extern struct cfdriver glxpcib_cd;
extern struct cfdriver pckbd_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver ucom_cd;
extern struct cfdriver ugen_cd;
extern struct cfdriver uhidev_cd;
extern struct cfdriver uhid_cd;
extern struct cfdriver ukbd_cd;
extern struct cfdriver uslhcom_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver aue_cd;
extern struct cfdriver axe_cd;
extern struct cfdriver axen_cd;
extern struct cfdriver smsc_cd;
extern struct cfdriver cue_cd;
extern struct cfdriver kue_cd;
extern struct cfdriver cdce_cd;
extern struct cfdriver mos_cd;
extern struct cfdriver udav_cd;
extern struct cfdriver upl_cd;
extern struct cfdriver ugl_cd;
extern struct cfdriver url_cd;
extern struct cfdriver ure_cd;
extern struct cfdriver umodem_cd;
extern struct cfdriver uplcom_cd;
extern struct cfdriver uvscom_cd;
extern struct cfdriver ubsa_cd;
extern struct cfdriver atu_cd;
extern struct cfdriver ural_cd;
extern struct cfdriver rum_cd;
extern struct cfdriver run_cd;
extern struct cfdriver zyd_cd;
extern struct cfdriver upgt_cd;
extern struct cfdriver urtw_cd;
extern struct cfdriver otus_cd;
extern struct cfdriver uath_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver clock_cd;
extern struct cfdriver bonito_cd;
extern struct cfdriver htb_cd;
extern struct cfdriver pcib_cd;
extern struct cfdriver mcclock_cd;
extern struct cfdriver voyager_cd;
extern struct cfdriver gdiumiic_cd;
extern struct cfdriver smfb_cd;
extern struct cfdriver sisfb_cd;
extern struct cfdriver radeonfb_cd;
extern struct cfdriver leioc_cd;

extern struct cfattach wd_ca;
extern struct cfattach iic_ca;
extern struct cfattach spdmem_iic_ca;
extern struct cfattach mfokrtc_ca;
extern struct cfattach gpio_ca;
extern struct cfattach isa_ca;
extern struct cfattach com_isa_ca;
extern struct cfattach pckbc_isa_ca;
extern struct cfattach eephy_ca;
extern struct cfattach rlphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach bmtphy_ca;
extern struct cfattach amphy_ca;
extern struct cfattach acphy_ca;
extern struct cfattach urlphy_ca;
extern struct cfattach rgephy_ca;
extern struct cfattach pci_ca;
extern struct cfattach vga_pci_ca;
extern struct cfattach ahci_pci_ca;
extern struct cfattach pciide_pci_ca;
extern struct cfattach ppb_ca;
extern struct cfattach rl_pci_ca;
extern struct cfattach re_pci_ca;
extern struct cfattach em_ca;
extern struct cfattach uhci_pci_ca;
extern struct cfattach ohci_pci_ca;
extern struct cfattach ehci_pci_ca;
extern struct cfattach ral_pci_ca;
extern struct cfattach piixpm_ca;
extern struct cfattach glxpcib_ca;
extern struct cfattach pckbd_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach ucom_ca;
extern struct cfattach ugen_ca;
extern struct cfattach uhidev_ca;
extern struct cfattach uhid_ca;
extern struct cfattach ukbd_ca;
extern struct cfattach uslhcom_ca;
extern struct cfattach umass_ca;
extern struct cfattach aue_ca;
extern struct cfattach axe_ca;
extern struct cfattach axen_ca;
extern struct cfattach smsc_ca;
extern struct cfattach cue_ca;
extern struct cfattach kue_ca;
extern struct cfattach cdce_ca;
extern struct cfattach mos_ca;
extern struct cfattach udav_ca;
extern struct cfattach upl_ca;
extern struct cfattach ugl_ca;
extern struct cfattach url_ca;
extern struct cfattach ure_ca;
extern struct cfattach umodem_ca;
extern struct cfattach uplcom_ca;
extern struct cfattach uvscom_ca;
extern struct cfattach ubsa_ca;
extern struct cfattach wi_usb_ca;
extern struct cfattach atu_ca;
extern struct cfattach ural_ca;
extern struct cfattach rum_ca;
extern struct cfattach run_ca;
extern struct cfattach zyd_ca;
extern struct cfattach upgt_ca;
extern struct cfattach urtw_ca;
extern struct cfattach otus_ca;
extern struct cfattach uath_ca;
extern struct cfattach bwfm_usb_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach cd_ca;
extern struct cfattach sd_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach cpu_ca;
extern struct cfattach clock_ca;
extern struct cfattach bonito_ca;
extern struct cfattach htb_ca;
extern struct cfattach pcib_ca;
extern struct cfattach mcclock_isa_ca;
extern struct cfattach voyager_ca;
extern struct cfattach gdiumiic_ca;
extern struct cfattach ohci_voyager_ca;
extern struct cfattach smfb_pci_ca;
extern struct cfattach smfb_voyager_ca;
extern struct cfattach sisfb_ca;
extern struct cfattach radeonfb_ca;
extern struct cfattach leioc_ca;
extern struct cfattach com_leioc_ca;


/* locators */
static long loc[43] = {
	-1, 0, -1, 0, -1, -1, -1, 0x3f8,
	0, -1, 0, 3, -1, -1, 0x70, 0,
	-1, 0, -1, -1, -1, 0x2f8, 0, -1,
	0, 3, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, 1, 6, 0x81, 0,
	0x2e, 3, 0,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"channel",
	"drive",
	"addr",
	"size",
	"port",
	"iomem",
	"iosiz",
	"irq",
	"drq",
	"drq2",
	"phy",
	"bus",
	"dev",
	"function",
	"slot",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"portno",
	"reportid",
	"console",
	"primary",
	"mux",
	"target",
	"lun",
	"offset",
	"mask",
	"flag",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, 1, -1, 2, 3, -1, 2,
	3, -1, 4, 3, 5, 6, 7, 8,
	9, -1, 10, -1, 10, -1, 10, -1,
	10, -1, 10, -1, 10, -1, 10, -1,
	10, -1, 10, -1, 10, -1, 11, -1,
	11, -1, 11, -1, 12, 13, -1, 14,
	-1, 4, 15, 16, 17, 18, 19, -1,
	4, 15, 16, 17, 18, 19, -1, 20,
	-1, 20, -1, 20, -1, 20, -1, 20,
	-1, 21, -1, 22, 23, 24, -1, 22,
	23, 24, -1, 22, 23, 24, -1, 22,
	23, 24, -1, 22, 23, 24, -1, 22,
	24, -1, 22, 24, -1, 25, 26, -1,
	27, 28, 29, -1,
};

/* size of parent vectors */
int pv_size = 76;

/* parent vectors */
short pv[76] = {
	55, 54, 51, 50, 46, 45, 44, 43, 24, 23, -1, 56, 58, 59, 57, 41,
	-1, 89, 87, 88, 90, 19, -1, 86, 27, 26, 28, -1, 79, 80, 22, -1,
	34, 35, -1, 42, 20, -1, 31, 81, -1, 84, 85, -1, 1, 2, -1, 32,
	40, -1, 76, -1, 6, -1, 5, -1, 18, -1, 1, -1, 73, -1, 21, -1,
	38, -1, 9, -1, 83, -1, 30, -1, 33, -1, 91, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: wd* at pciide* channel -1 drive -1 */
    {&wd_ca,		&wd_cd,		 0, STAR, loc+ 32,    0, pv+62, 1,    0},
/*  1: iic0 at gdiumiic0|gdiumiic0 */
    {&iic_ca,		&iic_cd,	 0, NORM,     loc,    0, pv+41, 3,    0},
/*  2: iic* at piixpm* */
    {&iic_ca,		&iic_cd,	 1, STAR,     loc,    0, pv+70, 3,    1},
/*  3: spdmem* at iic0|iic* addr -1 size -1 */
    {&spdmem_iic_ca,	&spdmem_cd,	 0, STAR, loc+ 32,    0, pv+44, 4,    0},
/*  4: mfokrtc0 at iic0 addr -1 size -1 */
    {&mfokrtc_ca,	&mfokrtc_cd,	 0, NORM, loc+ 32,    0, pv+58, 4,    0},
/*  5: gpio0 at voyager* */
    {&gpio_ca,		&gpio_cd,	 0, NORM,     loc,    0, pv+68, 9,    0},
/*  6: isa0 at glxpcib*|pcib* */
    {&isa_ca,		&isa_cd,	 0, NORM,     loc,    0, pv+38, 9,    0},
/*  7: com0 at isa0 port 0x2f8 size 0 iomem -1 iosiz 0 irq 3 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 0, NORM, loc+ 21,    0, pv+52, 10,    0},
/*  8: com1 at isa0 port 0x3f8 size 0 iomem -1 iosiz 0 irq 3 drq -1 drq2 -1 */
    {&com_isa_ca,	&com_cd,	 1, NORM, loc+  7,    0, pv+52, 10,    1},
/*  9: pckbc0 at isa0 port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&pckbc_isa_ca,	&pckbc_cd,	 0, NORM, loc+  0,    0, pv+52, 10,    0},
/* 10: eephy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&eephy_ca,		&eephy_cd,	 0, STAR, loc+ 33,    0, pv+ 0, 18,    0},
/* 11: rlphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&rlphy_ca,		&rlphy_cd,	 0, STAR, loc+ 33,    0, pv+ 0, 18,    0},
/* 12: ukphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+ 33,    0, pv+ 0, 18,    0},
/* 13: bmtphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&bmtphy_ca,	&bmtphy_cd,	 0, STAR, loc+ 33,    0, pv+ 0, 18,    0},
/* 14: amphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&amphy_ca,		&amphy_cd,	 0, STAR, loc+ 33,    0, pv+ 0, 18,    0},
/* 15: acphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&acphy_ca,		&acphy_cd,	 0, STAR, loc+ 33,    0, pv+ 0, 18,    0},
/* 16: urlphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&urlphy_ca,	&urlphy_cd,	 0, STAR, loc+ 33,    0, pv+ 0, 18,    0},
/* 17: rgephy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|re*|rl* phy -1 */
    {&rgephy_ca,	&rgephy_cd,	 0, STAR, loc+ 33,    0, pv+ 0, 18,    0},
/* 18: pci* at bonito*|htb*|ppb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+ 33,    0, pv+28, 38,    0},
/* 19: vga* at pci* dev -1 function -1 */
    {&vga_pci_ca,	&vga_cd,	 0, STAR, loc+ 32,    0, pv+56, 44,    0},
/* 20: ahci* at pci* dev -1 function -1 */
    {&ahci_pci_ca,	&ahci_cd,	 0, STAR, loc+ 32,    0, pv+56, 44,    0},
/* 21: pciide* at pci* dev -1 function -1 */
    {&pciide_pci_ca,	&pciide_cd,	 0, STAR, loc+ 32,    0, pv+56, 44,    0},
/* 22: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+ 32,    0, pv+56, 44,    0},
/* 23: rl* at pci* dev -1 function -1 */
    {&rl_pci_ca,	&rl_cd,		 0, STAR, loc+ 32,    0, pv+56, 44,    0},
/* 24: re* at pci* dev -1 function -1 */
    {&re_pci_ca,	&re_cd,		 0, STAR, loc+ 32,    0, pv+56, 44,    0},
/* 25: em* at pci* dev -1 function -1 */
    {&em_ca,		&em_cd,		 0, STAR, loc+ 32,    0, pv+56, 44,    0},
/* 26: uhci* at pci* dev -1 function -1 */
    {&uhci_pci_ca,	&uhci_cd,	 0, STAR, loc+ 32,    0, pv+56, 44,    0},
/* 27: ohci* at pci* dev -1 function -1 */
    {&ohci_pci_ca,	&ohci_cd,	 0, STAR, loc+ 32,    0, pv+56, 44,    0},
/* 28: ehci* at pci* dev -1 function -1 */
    {&ehci_pci_ca,	&ehci_cd,	 0, STAR, loc+ 32,    0, pv+56, 44,    0},
/* 29: ral* at pci* dev -1 function -1 */
    {&ral_pci_ca,	&ral_cd,	 0, STAR, loc+ 32,    0, pv+56, 44,    0},
/* 30: piixpm* at pci* dev -1 function -1 */
    {&piixpm_ca,	&piixpm_cd,	 0, STAR, loc+ 32,    0, pv+56, 44,    0},
/* 31: glxpcib* at pci* dev -1 function -1 */
    {&glxpcib_ca,	&glxpcib_cd,	 0, STAR, loc+ 32,    0, pv+56, 44,    0},
/* 32: pckbd* at pckbc0 slot -1 */
    {&pckbd_ca,		&pckbd_cd,	 0, STAR, loc+ 33,    0, pv+66, 47,    0},
/* 33: usb* at ohci*|ohci*|uhci*|ehci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+23, 48,    0},
/* 34: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+72, 48,    0},
/* 35: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 36: ucom* at umodem*|uvscom*|ubsa*|uplcom*|uslhcom* portno -1 */
    {&ucom_ca,		&ucom_cd,	 0, STAR, loc+ 33,    0, pv+11, 63,    0},
/* 37: ugen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugen_ca,		&ugen_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 38: uhidev* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhidev_ca,	&uhidev_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 39: uhid* at uhidev* reportid -1 */
    {&uhid_ca,		&uhid_cd,	 0, STAR, loc+ 33,    0, pv+64, 73,    0},
/* 40: ukbd* at uhidev* reportid -1 */
    {&ukbd_ca,		&ukbd_cd,	 0, STAR, loc+ 33,    0, pv+64, 73,    0},
/* 41: uslhcom* at uhidev* reportid -1 */
    {&uslhcom_ca,	&uslhcom_cd,	 0, STAR, loc+ 33,    0, pv+64, 73,    0},
/* 42: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 43: aue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&aue_ca,		&aue_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 44: axe* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axe_ca,		&axe_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 45: axen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axen_ca,		&axen_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 46: smsc* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&smsc_ca,		&smsc_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 47: cue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cue_ca,		&cue_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 48: kue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&kue_ca,		&kue_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 49: cdce* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cdce_ca,		&cdce_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 50: mos* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mos_ca,		&mos_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 51: udav* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udav_ca,		&udav_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 52: upl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upl_ca,		&upl_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 53: ugl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugl_ca,		&ugl_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 54: url* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&url_ca,		&url_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 55: ure* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ure_ca,		&ure_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 56: umodem* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umodem_ca,	&umodem_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 57: uplcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uplcom_ca,	&uplcom_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 58: uvscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvscom_ca,	&uvscom_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 59: ubsa* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ubsa_ca,		&ubsa_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 60: wi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&wi_usb_ca,	&wi_cd,		 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 61: atu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&atu_ca,		&atu_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 62: ural* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ural_ca,		&ural_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 63: rum* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rum_ca,		&rum_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 64: run* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&run_ca,		&run_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 65: zyd* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&zyd_ca,		&zyd_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 66: upgt* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upgt_ca,		&upgt_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 67: urtw* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtw_ca,		&urtw_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 68: otus* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&otus_ca,		&otus_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 69: uath* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uath_ca,		&uath_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 70: bwfm* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&bwfm_usb_ca,	&bwfm_cd,	 0, STAR, loc+ 28,    0, pv+32, 49,    0},
/* 71: wsdisplay* at sisfb*|smfb*|smfb*|radeonfb*|vga* console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+ 34,    0, pv+17, 75,    0},
/* 72: wskbd* at pckbd*|ukbd* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+ 35,    0, pv+47, 95,    0},
/* 73: scsibus* at umass*|ahci* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+35, 100,    0},
/* 74: cd* at scsibus* target -1 lun -1 */
    {&cd_ca,		&cd_cd,		 0, STAR, loc+ 32,    0, pv+60, 101,    0},
/* 75: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+ 32,    0, pv+60, 101,    0},
/* 76: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+10, 0,    0},
/* 77: cpu0 at mainbus0 */
    {&cpu_ca,		&cpu_cd,	 0, NORM,     loc,    0, pv+50, 103,    0},
/* 78: clock0 at mainbus0 */
    {&clock_ca,		&clock_cd,	 0, NORM,     loc,    0, pv+50, 103,    0},
/* 79: bonito* at mainbus0 */
    {&bonito_ca,	&bonito_cd,	 0, STAR,     loc,    0, pv+50, 103,    0},
/* 80: htb* at mainbus0 */
    {&htb_ca,		&htb_cd,	 0, STAR,     loc,    0, pv+50, 103,    0},
/* 81: pcib* at pci* dev -1 function -1 */
    {&pcib_ca,		&pcib_cd,	 0, STAR, loc+ 32,    0, pv+56, 44,    0},
/* 82: mcclock0 at isa0 port 0x70 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    {&mcclock_isa_ca,	&mcclock_cd,	 0, NORM, loc+ 14,    0, pv+52, 10,    0},
/* 83: voyager* at pci* dev -1 function -1 */
    {&voyager_ca,	&voyager_cd,	 0, STAR, loc+ 32,    0, pv+56, 44,    0},
/* 84: gdiumiic0 at gpio0 offset 6 mask 0x81 flag 0 */
    {&gdiumiic_ca,	&gdiumiic_cd,	 0, NORM, loc+ 37,    0, pv+54, 104,    0},
/* 85: gdiumiic0 at gpio0 offset 0x2e mask 3 flag 0 */
    {&gdiumiic_ca,	&gdiumiic_cd,	 0, NORM, loc+ 40,    0, pv+54, 104,    0},
/* 86: ohci* at voyager* */
    {&ohci_voyager_ca,	&ohci_cd,	 0, STAR,     loc,    0, pv+68, 9,    0},
/* 87: smfb* at pci* dev -1 function -1 */
    {&smfb_pci_ca,	&smfb_cd,	 0, STAR, loc+ 32,    0, pv+56, 44,    0},
/* 88: smfb* at voyager* */
    {&smfb_voyager_ca,	&smfb_cd,	 0, STAR,     loc,    0, pv+68, 9,    0},
/* 89: sisfb* at pci* dev -1 function -1 */
    {&sisfb_ca,		&sisfb_cd,	 0, STAR, loc+ 32,    0, pv+56, 44,    0},
/* 90: radeonfb* at pci* dev -1 function -1 */
    {&radeonfb_ca,	&radeonfb_cd,	 0, STAR, loc+ 32,    0, pv+56, 44,    0},
/* 91: leioc0 at mainbus0 */
    {&leioc_ca,		&leioc_cd,	 0, NORM,     loc,    0, pv+50, 103,    0},
/* 92: com* at leioc0 */
    {&com_leioc_ca,	&com_cd,	 2, STAR,     loc,    0, pv+74, 107,    2},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	76 /* mainbus0 */,
	-1
};

int cfroots_size = 2;

/* pseudo-devices */
extern void loopattach(int);
extern void bpfilterattach(int);
extern void rdattach(int);
extern void wsmuxattach(int);

char *pdevnames[] = {
	"loop",
	"bpfilter",
	"rd",
	"wsmux",
};

int pdevnames_size = 4;

struct pdevinit pdevinit[] = {
	{ loopattach, 1 },
	{ bpfilterattach, 1 },
	{ rdattach, 1 },
	{ wsmuxattach, 2 },
	{ NULL, 0 }
};
