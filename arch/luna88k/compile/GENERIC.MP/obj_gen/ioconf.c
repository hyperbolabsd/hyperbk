/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/luna88k/conf/GENERIC.MP"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver le_cd;
extern struct cfdriver vscsi_cd;
extern struct cfdriver softraid_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver clock_cd;
extern struct cfdriver lcd_cd;
extern struct cfdriver sio_cd;
extern struct cfdriver siotty_cd;
extern struct cfdriver xp_cd;
extern struct cfdriver ws_cd;
extern struct cfdriver fb_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver wsmouse_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver cd_cd;
extern struct cfdriver ch_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver st_cd;
extern struct cfdriver uk_cd;
extern struct cfdriver safte_cd;
extern struct cfdriver ses_cd;
extern struct cfdriver spc_cd;
extern struct cfdriver cbus_cd;
extern struct cfdriver pcex_cd;

extern struct cfattach vscsi_ca;
extern struct cfattach softraid_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach clock_ca;
extern struct cfattach lcd_ca;
extern struct cfattach le_ca;
extern struct cfattach sio_ca;
extern struct cfattach siotty_ca;
extern struct cfattach xp_ca;
extern struct cfattach ws_ca;
extern struct cfattach fb_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach wsmouse_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach cd_ca;
extern struct cfattach ch_ca;
extern struct cfattach sd_ca;
extern struct cfattach st_ca;
extern struct cfattach uk_ca;
extern struct cfattach safte_ca;
extern struct cfattach ses_ca;
extern struct cfattach spc_ca;
extern struct cfattach cbus_ca;
extern struct cfattach pcex_ca;


/* locators */
static long loc[11] = {
	-1, 0, -1, 0, -1, -1, -1, 1,
	-1, -1, 0,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"channel",
	"console",
	"primary",
	"mux",
	"target",
	"lun",
	"port",
	"size",
	"iomem",
	"iosiz",
	"int",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, -1, 1, 2, 3, -1, 1,
	3, -1, 4, 5, -1, 6, 7, 8,
	9, 10, -1,
};

/* size of parent vectors */
int pv_size = 17;

/* parent vectors */
short pv[17] = {
	1, 0, 22, 23, -1, 2, -1, 24, -1, 14, -1, 9, -1, 6, -1, 10,
	-1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: vscsi0 at root */
    {&vscsi_ca,		&vscsi_cd,	 0, NORM,     loc,    0, pv+ 4, 0,    0},
/*  1: softraid0 at root */
    {&softraid_ca,	&softraid_cd,	 0, NORM,     loc,    0, pv+ 4, 0,    0},
/*  2: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+ 4, 0,    0},
/*  3: clock0 at mainbus0 */
    {&clock_ca,		&clock_cd,	 0, NORM,     loc,    0, pv+ 5, 2,    0},
/*  4: lcd0 at mainbus0 */
    {&lcd_ca,		&lcd_cd,	 0, NORM,     loc,    0, pv+ 5, 2,    0},
/*  5: le0 at mainbus0 */
    {&le_ca,		&le_cd,		 0, NORM,     loc,    0, pv+ 5, 2,    0},
/*  6: sio0 at mainbus0 */
    {&sio_ca,		&sio_cd,	 0, NORM,     loc,    0, pv+ 5, 2,    0},
/*  7: siotty0 at sio0 channel -1 */
    {&siotty_ca,	&siotty_cd,	 0, NORM, loc+  9,    0, pv+13, 1,    0},
/*  8: xp0 at mainbus0 */
    {&xp_ca,		&xp_cd,		 0, NORM,     loc,    0, pv+ 5, 2,    0},
/*  9: ws0 at sio0 channel -1 */
    {&ws_ca,		&ws_cd,		 0, NORM, loc+  9,    0, pv+13, 1,    0},
/* 10: fb0 at mainbus0 */
    {&fb_ca,		&fb_cd,		 0, NORM,     loc,    0, pv+ 5, 2,    0},
/* 11: wsdisplay* at fb0 console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+  5,    0, pv+15, 3,    0},
/* 12: wskbd* at ws0 console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+  6,    0, pv+11, 7,    0},
/* 13: wsmouse* at ws0 mux 0 */
    {&wsmouse_ca,	&wsmouse_cd,	 0, STAR, loc+ 10,    0, pv+11, 7,    0},
/* 14: scsibus* at softraid0|vscsi0|spc0|spc1 */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+ 0, 9,    0},
/* 15: cd* at scsibus* target -1 lun -1 */
    {&cd_ca,		&cd_cd,		 0, STAR, loc+  8,    0, pv+ 9, 10,    0},
/* 16: ch* at scsibus* target -1 lun -1 */
    {&ch_ca,		&ch_cd,		 0, STAR, loc+  8,    0, pv+ 9, 10,    0},
/* 17: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+  8,    0, pv+ 9, 10,    0},
/* 18: st* at scsibus* target -1 lun -1 */
    {&st_ca,		&st_cd,		 0, STAR, loc+  8,    0, pv+ 9, 10,    0},
/* 19: uk* at scsibus* target -1 lun -1 */
    {&uk_ca,		&uk_cd,		 0, STAR, loc+  8,    0, pv+ 9, 10,    0},
/* 20: safte* at scsibus* target -1 lun -1 */
    {&safte_ca,		&safte_cd,	 0, STAR, loc+  8,    0, pv+ 9, 10,    0},
/* 21: ses* at scsibus* target -1 lun -1 */
    {&ses_ca,		&ses_cd,	 0, STAR, loc+  8,    0, pv+ 9, 10,    0},
/* 22: spc0 at mainbus0 */
    {&spc_ca,		&spc_cd,	 0, NORM,     loc,    0, pv+ 5, 2,    0},
/* 23: spc1 at mainbus0 */
    {&spc_ca,		&spc_cd,	 1, NORM,     loc,    0, pv+ 5, 2,    1},
/* 24: cbus0 at mainbus0 */
    {&cbus_ca,		&cbus_cd,	 0, NORM,     loc,    0, pv+ 5, 2,    0},
/* 25: pcex0 at cbus0 port -1 size 0 iomem -1 iosiz 0 int -1 */
    {&pcex_ca,		&pcex_cd,	 0, NORM, loc+  0,    0, pv+ 7, 13,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 0 /* vscsi0 */,
	 1 /* softraid0 */,
	 2 /* mainbus0 */,
	-1
};

int cfroots_size = 4;

/* pseudo-devices */
extern void pfattach(int);
extern void pflogattach(int);
extern void pfsyncattach(int);
extern void pflowattach(int);
extern void encattach(int);
extern void ptyattach(int);
extern void nmeaattach(int);
extern void mstsattach(int);
extern void endrunattach(int);
extern void vndattach(int);
extern void ksymsattach(int);
extern void bpfilterattach(int);
extern void bridgeattach(int);
extern void vebattach(int);
extern void carpattach(int);
extern void etheripattach(int);
extern void gifattach(int);
extern void greattach(int);
extern void loopattach(int);
extern void mpeattach(int);
extern void mpwattach(int);
extern void mpipattach(int);
extern void bpeattach(int);
extern void pairattach(int);
extern void pppattach(int);
extern void pppoeattach(int);
extern void pppxattach(int);
extern void spppattach(int);
extern void trunkattach(int);
extern void aggrattach(int);
extern void tpmrattach(int);
extern void tunattach(int);
extern void vetherattach(int);
extern void vxlanattach(int);
extern void vlanattach(int);
extern void switchattach(int);
extern void wgattach(int);
extern void bioattach(int);
extern void fuseattach(int);
extern void wsmuxattach(int);

char *pdevnames[] = {
	"pf",
	"pflog",
	"pfsync",
	"pflow",
	"enc",
	"pty",
	"nmea",
	"msts",
	"endrun",
	"vnd",
	"ksyms",
	"bpfilter",
	"bridge",
	"veb",
	"carp",
	"etherip",
	"gif",
	"gre",
	"loop",
	"mpe",
	"mpw",
	"mpip",
	"bpe",
	"pair",
	"ppp",
	"pppoe",
	"pppx",
	"sppp",
	"trunk",
	"aggr",
	"tpmr",
	"tun",
	"vether",
	"vxlan",
	"vlan",
	"switch",
	"wg",
	"bio",
	"fuse",
	"wsmux",
};

int pdevnames_size = 40;

struct pdevinit pdevinit[] = {
	{ pfattach, 1 },
	{ pflogattach, 1 },
	{ pfsyncattach, 1 },
	{ pflowattach, 1 },
	{ encattach, 1 },
	{ ptyattach, 16 },
	{ nmeaattach, 1 },
	{ mstsattach, 1 },
	{ endrunattach, 1 },
	{ vndattach, 4 },
	{ ksymsattach, 1 },
	{ bpfilterattach, 1 },
	{ bridgeattach, 1 },
	{ vebattach, 1 },
	{ carpattach, 1 },
	{ etheripattach, 1 },
	{ gifattach, 1 },
	{ greattach, 1 },
	{ loopattach, 1 },
	{ mpeattach, 1 },
	{ mpwattach, 1 },
	{ mpipattach, 1 },
	{ bpeattach, 1 },
	{ pairattach, 1 },
	{ pppattach, 1 },
	{ pppoeattach, 1 },
	{ pppxattach, 1 },
	{ spppattach, 1 },
	{ trunkattach, 1 },
	{ aggrattach, 1 },
	{ tpmrattach, 1 },
	{ tunattach, 1 },
	{ vetherattach, 1 },
	{ vxlanattach, 1 },
	{ vlanattach, 1 },
	{ switchattach, 1 },
	{ wgattach, 1 },
	{ bioattach, 1 },
	{ fuseattach, 1 },
	{ wsmuxattach, 2 },
	{ NULL, 0 }
};
