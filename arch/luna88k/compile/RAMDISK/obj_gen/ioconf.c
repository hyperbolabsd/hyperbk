/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/luna88k/conf/RAMDISK"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver le_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver clock_cd;
extern struct cfdriver sio_cd;
extern struct cfdriver siotty_cd;
extern struct cfdriver ws_cd;
extern struct cfdriver fb_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver cd_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver st_cd;
extern struct cfdriver spc_cd;

extern struct cfattach mainbus_ca;
extern struct cfattach clock_ca;
extern struct cfattach le_ca;
extern struct cfattach sio_ca;
extern struct cfattach siotty_ca;
extern struct cfattach ws_ca;
extern struct cfattach fb_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach cd_ca;
extern struct cfattach sd_ca;
extern struct cfattach st_ca;
extern struct cfattach spc_ca;


/* locators */
static long loc[5] = {
	-1, -1, 1, -1, -1,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"channel",
	"console",
	"primary",
	"mux",
	"target",
	"lun",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, -1, 1, 2, 3, -1, 1,
	3, -1, 4, 5, -1,
};

/* size of parent vectors */
int pv_size = 13;

/* parent vectors */
short pv[13] = {
	13, 14, -1, 9, -1, 5, -1, 3, -1, 0, -1, 6, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+ 2, 0,    0},
/*  1: clock0 at mainbus0 */
    {&clock_ca,		&clock_cd,	 0, NORM,     loc,    0, pv+ 9, 2,    0},
/*  2: le0 at mainbus0 */
    {&le_ca,		&le_cd,		 0, NORM,     loc,    0, pv+ 9, 2,    0},
/*  3: sio0 at mainbus0 */
    {&sio_ca,		&sio_cd,	 0, NORM,     loc,    0, pv+ 9, 2,    0},
/*  4: siotty0 at sio0 channel -1 */
    {&siotty_ca,	&siotty_cd,	 0, NORM, loc+  4,    0, pv+ 7, 1,    0},
/*  5: ws0 at sio0 channel -1 */
    {&ws_ca,		&ws_cd,		 0, NORM, loc+  4,    0, pv+ 7, 1,    0},
/*  6: fb0 at mainbus0 */
    {&fb_ca,		&fb_cd,		 0, NORM,     loc,    0, pv+ 9, 2,    0},
/*  7: wsdisplay* at fb0 console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+  0,    0, pv+11, 3,    0},
/*  8: wskbd* at ws0 console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+  1,    0, pv+ 5, 7,    0},
/*  9: scsibus* at spc0|spc1 */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+ 0, 9,    0},
/* 10: cd* at scsibus* target -1 lun -1 */
    {&cd_ca,		&cd_cd,		 0, STAR, loc+  3,    0, pv+ 3, 10,    0},
/* 11: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+  3,    0, pv+ 3, 10,    0},
/* 12: st* at scsibus* target -1 lun -1 */
    {&st_ca,		&st_cd,		 0, STAR, loc+  3,    0, pv+ 3, 10,    0},
/* 13: spc0 at mainbus0 */
    {&spc_ca,		&spc_cd,	 0, NORM,     loc,    0, pv+ 9, 2,    0},
/* 14: spc1 at mainbus0 */
    {&spc_ca,		&spc_cd,	 1, NORM,     loc,    0, pv+ 9, 2,    1},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 0 /* mainbus0 */,
	-1
};

int cfroots_size = 2;

/* pseudo-devices */
extern void loopattach(int);
extern void bpfilterattach(int);
extern void rdattach(int);

char *pdevnames[] = {
	"loop",
	"bpfilter",
	"rd",
};

int pdevnames_size = 3;

struct pdevinit pdevinit[] = {
	{ loopattach, 1 },
	{ bpfilterattach, 1 },
	{ rdattach, 1 },
	{ NULL, 0 }
};
