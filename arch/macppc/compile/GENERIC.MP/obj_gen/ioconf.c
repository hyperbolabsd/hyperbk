/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/macppc/conf/GENERIC.MP"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver video_cd;
extern struct cfdriver audio_cd;
extern struct cfdriver midi_cd;
extern struct cfdriver drm_cd;
extern struct cfdriver wdc_cd;
extern struct cfdriver ahc_cd;
extern struct cfdriver qlw_cd;
extern struct cfdriver mpi_cd;
extern struct cfdriver siop_cd;
extern struct cfdriver ep_cd;
extern struct cfdriver wi_cd;
extern struct cfdriver an_cd;
extern struct cfdriver xl_cd;
extern struct cfdriver fxp_cd;
extern struct cfdriver rl_cd;
extern struct cfdriver re_cd;
extern struct cfdriver dc_cd;
extern struct cfdriver epic_cd;
extern struct cfdriver ne_cd;
extern struct cfdriver gem_cd;
extern struct cfdriver ti_cd;
extern struct cfdriver com_cd;
extern struct cfdriver ath_cd;
extern struct cfdriver athn_cd;
extern struct cfdriver bwfm_cd;
extern struct cfdriver atw_cd;
extern struct cfdriver rtw_cd;
extern struct cfdriver ral_cd;
extern struct cfdriver acx_cd;
extern struct cfdriver malo_cd;
extern struct cfdriver bwi_cd;
extern struct cfdriver uhci_cd;
extern struct cfdriver ohci_cd;
extern struct cfdriver ehci_cd;
extern struct cfdriver xhci_cd;
extern struct cfdriver radio_cd;
extern struct cfdriver vscsi_cd;
extern struct cfdriver mpath_cd;
extern struct cfdriver softraid_cd;
extern struct cfdriver spdmem_cd;
extern struct cfdriver nsphy_cd;
extern struct cfdriver qsphy_cd;
extern struct cfdriver inphy_cd;
extern struct cfdriver iophy_cd;
extern struct cfdriver eephy_cd;
extern struct cfdriver exphy_cd;
extern struct cfdriver rlphy_cd;
extern struct cfdriver lxtphy_cd;
extern struct cfdriver luphy_cd;
extern struct cfdriver mtdphy_cd;
extern struct cfdriver icsphy_cd;
extern struct cfdriver sqphy_cd;
extern struct cfdriver tqphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver dcphy_cd;
extern struct cfdriver bmtphy_cd;
extern struct cfdriver brgphy_cd;
extern struct cfdriver xmphy_cd;
extern struct cfdriver amphy_cd;
extern struct cfdriver acphy_cd;
extern struct cfdriver urlphy_cd;
extern struct cfdriver rgephy_cd;
extern struct cfdriver ciphy_cd;
extern struct cfdriver ipgphy_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver memc_cd;
extern struct cfdriver wd_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver cd_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver ch_cd;
extern struct cfdriver st_cd;
extern struct cfdriver uk_cd;
extern struct cfdriver safte_cd;
extern struct cfdriver ses_cd;
extern struct cfdriver sym_cd;
extern struct cfdriver rdac_cd;
extern struct cfdriver emc_cd;
extern struct cfdriver hds_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver arc_cd;
extern struct cfdriver eap_cd;
extern struct cfdriver eso_cd;
extern struct cfdriver emu_cd;
extern struct cfdriver qle_cd;
extern struct cfdriver de_cd;
extern struct cfdriver pcn_cd;
extern struct cfdriver pciide_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver vr_cd;
extern struct cfdriver bktr_cd;
extern struct cfdriver em_cd;
extern struct cfdriver xge_cd;
extern struct cfdriver hifn_cd;
extern struct cfdriver ubsec_cd;
extern struct cfdriver safe_cd;
extern struct cfdriver pwdog_cd;
extern struct cfdriver mbg_cd;
extern struct cfdriver cbb_cd;
extern struct cfdriver skc_cd;
extern struct cfdriver sk_cd;
extern struct cfdriver mskc_cd;
extern struct cfdriver msk_cd;
extern struct cfdriver puc_cd;
extern struct cfdriver cmpci_cd;
extern struct cfdriver pcscp_cd;
extern struct cfdriver bge_cd;
extern struct cfdriver vge_cd;
extern struct cfdriver stge_cd;
extern struct cfdriver agp_cd;
extern struct cfdriver appleagp_cd;
extern struct cfdriver radeondrm_cd;
extern struct cfdriver mpcpcibr_cd;
extern struct cfdriver ht_cd;
extern struct cfdriver smu_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver wsmouse_cd;
extern struct cfdriver hpb_cd;
extern struct cfdriver pchb_cd;
extern struct cfdriver atapiscsi_cd;
extern struct cfdriver macobio_cd;
extern struct cfdriver kauaiata_cd;
extern struct cfdriver mc_cd;
extern struct cfdriver bm_cd;
extern struct cfdriver macintr_cd;
extern struct cfdriver openpic_cd;
extern struct cfdriver zs_cd;
extern struct cfdriver zstty_cd;
extern struct cfdriver adb_cd;
extern struct cfdriver akbd_cd;
extern struct cfdriver ams_cd;
extern struct cfdriver apm_cd;
extern struct cfdriver abtn_cd;
extern struct cfdriver awacs_cd;
extern struct cfdriver mediabay_cd;
extern struct cfdriver kiic_cd;
extern struct cfdriver piic_cd;
extern struct cfdriver xlights_cd;
extern struct cfdriver snapper_cd;
extern struct cfdriver tumbler_cd;
extern struct cfdriver onyx_cd;
extern struct cfdriver aoa_cd;
extern struct cfdriver daca_cd;
extern struct cfdriver macgpio_cd;
extern struct cfdriver sysbutton_cd;
extern struct cfdriver pgs_cd;
extern struct cfdriver dfs_cd;
extern struct cfdriver vgafb_cd;
extern struct cfdriver cardslot_cd;
extern struct cfdriver cardbus_cd;
extern struct cfdriver pcmcia_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver uaudio_cd;
extern struct cfdriver uvideo_cd;
extern struct cfdriver utvfu_cd;
extern struct cfdriver udl_cd;
extern struct cfdriver umidi_cd;
extern struct cfdriver ucom_cd;
extern struct cfdriver ugen_cd;
extern struct cfdriver uhidev_cd;
extern struct cfdriver uhid_cd;
extern struct cfdriver fido_cd;
extern struct cfdriver ujoy_cd;
extern struct cfdriver ukbd_cd;
extern struct cfdriver ums_cd;
extern struct cfdriver uts_cd;
extern struct cfdriver utpms_cd;
extern struct cfdriver ucycom_cd;
extern struct cfdriver uslhcom_cd;
extern struct cfdriver ulpt_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver uthum_cd;
extern struct cfdriver ugold_cd;
extern struct cfdriver utrh_cd;
extern struct cfdriver uonerng_cd;
extern struct cfdriver urng_cd;
extern struct cfdriver udcf_cd;
extern struct cfdriver umbg_cd;
extern struct cfdriver uvisor_cd;
extern struct cfdriver udsbr_cd;
extern struct cfdriver utwitch_cd;
extern struct cfdriver aue_cd;
extern struct cfdriver axe_cd;
extern struct cfdriver axen_cd;
extern struct cfdriver smsc_cd;
extern struct cfdriver cue_cd;
extern struct cfdriver kue_cd;
extern struct cfdriver cdce_cd;
extern struct cfdriver urndis_cd;
extern struct cfdriver mos_cd;
extern struct cfdriver udav_cd;
extern struct cfdriver upl_cd;
extern struct cfdriver ugl_cd;
extern struct cfdriver url_cd;
extern struct cfdriver ure_cd;
extern struct cfdriver umodem_cd;
extern struct cfdriver uftdi_cd;
extern struct cfdriver uplcom_cd;
extern struct cfdriver umct_cd;
extern struct cfdriver uvscom_cd;
extern struct cfdriver ubsa_cd;
extern struct cfdriver uslcom_cd;
extern struct cfdriver uark_cd;
extern struct cfdriver moscom_cd;
extern struct cfdriver umcs_cd;
extern struct cfdriver uscom_cd;
extern struct cfdriver ucrcom_cd;
extern struct cfdriver uipaq_cd;
extern struct cfdriver umsm_cd;
extern struct cfdriver uchcom_cd;
extern struct cfdriver atu_cd;
extern struct cfdriver ural_cd;
extern struct cfdriver rum_cd;
extern struct cfdriver run_cd;
extern struct cfdriver zyd_cd;
extern struct cfdriver upgt_cd;
extern struct cfdriver urtw_cd;
extern struct cfdriver urtwn_cd;
extern struct cfdriver rsu_cd;
extern struct cfdriver otus_cd;
extern struct cfdriver uath_cd;
extern struct cfdriver uow_cd;
extern struct cfdriver uberry_cd;
extern struct cfdriver upd_cd;
extern struct cfdriver uwacom_cd;
extern struct cfdriver uhidpp_cd;
extern struct cfdriver ucc_cd;
extern struct cfdriver iic_cd;
extern struct cfdriver lmtemp_cd;
extern struct cfdriver lmenv_cd;
extern struct cfdriver maxtmp_cd;
extern struct cfdriver adc_cd;
extern struct cfdriver tsl_cd;
extern struct cfdriver admtmp_cd;
extern struct cfdriver maxds_cd;
extern struct cfdriver fcu_cd;
extern struct cfdriver adt_cd;
extern struct cfdriver mem_cd;
extern struct cfdriver onewire_cd;
extern struct cfdriver owid_cd;
extern struct cfdriver owsbm_cd;
extern struct cfdriver owtemp_cd;
extern struct cfdriver owctr_cd;
extern struct cfdriver hme_cd;
extern struct cfdriver asms_cd;

extern struct cfattach video_ca;
extern struct cfattach audio_ca;
extern struct cfattach midi_ca;
extern struct cfattach drm_ca;
extern struct cfattach radio_ca;
extern struct cfattach vscsi_ca;
extern struct cfattach mpath_ca;
extern struct cfattach softraid_ca;
extern struct cfattach nsphy_ca;
extern struct cfattach qsphy_ca;
extern struct cfattach inphy_ca;
extern struct cfattach iophy_ca;
extern struct cfattach eephy_ca;
extern struct cfattach exphy_ca;
extern struct cfattach rlphy_ca;
extern struct cfattach lxtphy_ca;
extern struct cfattach luphy_ca;
extern struct cfattach mtdphy_ca;
extern struct cfattach icsphy_ca;
extern struct cfattach sqphy_ca;
extern struct cfattach tqphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach dcphy_ca;
extern struct cfattach bmtphy_ca;
extern struct cfattach brgphy_ca;
extern struct cfattach xmphy_ca;
extern struct cfattach amphy_ca;
extern struct cfattach acphy_ca;
extern struct cfattach urlphy_ca;
extern struct cfattach rgephy_ca;
extern struct cfattach ciphy_ca;
extern struct cfattach ipgphy_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach cpu_ca;
extern struct cfattach memc_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach cd_ca;
extern struct cfattach ch_ca;
extern struct cfattach sd_ca;
extern struct cfattach st_ca;
extern struct cfattach uk_ca;
extern struct cfattach safte_ca;
extern struct cfattach ses_ca;
extern struct cfattach sym_ca;
extern struct cfattach rdac_ca;
extern struct cfattach emc_ca;
extern struct cfattach hds_ca;
extern struct cfattach pci_ca;
extern struct cfattach ahc_pci_ca;
extern struct cfattach arc_ca;
extern struct cfattach eap_ca;
extern struct cfattach eso_ca;
extern struct cfattach emu_ca;
extern struct cfattach qlw_pci_ca;
extern struct cfattach qle_ca;
extern struct cfattach mpi_pci_ca;
extern struct cfattach de_ca;
extern struct cfattach pcn_ca;
extern struct cfattach siop_pci_ca;
extern struct cfattach pciide_pci_ca;
extern struct cfattach ppb_ca;
extern struct cfattach rl_pci_ca;
extern struct cfattach re_pci_ca;
extern struct cfattach vr_ca;
extern struct cfattach bktr_ca;
extern struct cfattach xl_pci_ca;
extern struct cfattach fxp_pci_ca;
extern struct cfattach em_ca;
extern struct cfattach xge_ca;
extern struct cfattach dc_pci_ca;
extern struct cfattach epic_pci_ca;
extern struct cfattach ti_pci_ca;
extern struct cfattach gem_pci_ca;
extern struct cfattach hifn_ca;
extern struct cfattach ubsec_ca;
extern struct cfattach safe_ca;
extern struct cfattach pwdog_ca;
extern struct cfattach mbg_ca;
extern struct cfattach uhci_pci_ca;
extern struct cfattach ohci_pci_ca;
extern struct cfattach ehci_pci_ca;
extern struct cfattach xhci_pci_ca;
extern struct cfattach cbb_pci_ca;
extern struct cfattach skc_ca;
extern struct cfattach sk_ca;
extern struct cfattach mskc_ca;
extern struct cfattach msk_ca;
extern struct cfattach com_puc_ca;
extern struct cfattach puc_pci_ca;
extern struct cfattach wi_pci_ca;
extern struct cfattach an_pci_ca;
extern struct cfattach cmpci_ca;
extern struct cfattach pcscp_ca;
extern struct cfattach bge_ca;
extern struct cfattach vge_ca;
extern struct cfattach stge_ca;
extern struct cfattach ath_pci_ca;
extern struct cfattach atw_pci_ca;
extern struct cfattach rtw_pci_ca;
extern struct cfattach ral_pci_ca;
extern struct cfattach acx_pci_ca;
extern struct cfattach malo_pci_ca;
extern struct cfattach bwi_pci_ca;
extern struct cfattach agp_ca;
extern struct cfattach appleagp_ca;
extern struct cfattach radeondrm_ca;
extern struct cfattach mpcpcibr_ca;
extern struct cfattach ht_ca;
extern struct cfattach smu_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach wsmouse_ca;
extern struct cfattach hpb_ca;
extern struct cfattach pchb_ca;
extern struct cfattach atapiscsi_ca;
extern struct cfattach wd_ca;
extern struct cfattach macobio_ca;
extern struct cfattach kauaiata_ca;
extern struct cfattach mc_ca;
extern struct cfattach bm_ca;
extern struct cfattach macintr_ca;
extern struct cfattach openpic_ca;
extern struct cfattach zs_ca;
extern struct cfattach zstty_ca;
extern struct cfattach adb_ca;
extern struct cfattach akbd_ca;
extern struct cfattach ams_ca;
extern struct cfattach apm_ca;
extern struct cfattach abtn_ca;
extern struct cfattach awacs_ca;
extern struct cfattach mediabay_ca;
extern struct cfattach kiic_ca;
extern struct cfattach kiic_memc_ca;
extern struct cfattach piic_ca;
extern struct cfattach xlights_ca;
extern struct cfattach snapper_ca;
extern struct cfattach tumbler_ca;
extern struct cfattach onyx_ca;
extern struct cfattach aoa_ca;
extern struct cfattach daca_ca;
extern struct cfattach macgpio_ca;
extern struct cfattach macgpio_gpio_ca;
extern struct cfattach sysbutton_ca;
extern struct cfattach pgs_ca;
extern struct cfattach dfs_ca;
extern struct cfattach wdc_obio_ca;
extern struct cfattach wi_obio_ca;
extern struct cfattach vgafb_ca;
extern struct cfattach cardslot_ca;
extern struct cfattach cardbus_ca;
extern struct cfattach com_cardbus_ca;
extern struct cfattach xl_cardbus_ca;
extern struct cfattach dc_cardbus_ca;
extern struct cfattach fxp_cardbus_ca;
extern struct cfattach rl_cardbus_ca;
extern struct cfattach re_cardbus_ca;
extern struct cfattach ath_cardbus_ca;
extern struct cfattach rtw_cardbus_ca;
extern struct cfattach ral_cardbus_ca;
extern struct cfattach acx_cardbus_ca;
extern struct cfattach ehci_cardbus_ca;
extern struct cfattach ohci_cardbus_ca;
extern struct cfattach uhci_cardbus_ca;
extern struct cfattach malo_cardbus_ca;
extern struct cfattach bwi_cardbus_ca;
extern struct cfattach pcmcia_ca;
extern struct cfattach ep_pcmcia_ca;
extern struct cfattach ne_pcmcia_ca;
extern struct cfattach com_pcmcia_ca;
extern struct cfattach wdc_pcmcia_ca;
extern struct cfattach wi_pcmcia_ca;
extern struct cfattach malo_pcmcia_ca;
extern struct cfattach an_pcmcia_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach uaudio_ca;
extern struct cfattach uvideo_ca;
extern struct cfattach utvfu_ca;
extern struct cfattach udl_ca;
extern struct cfattach umidi_ca;
extern struct cfattach ucom_ca;
extern struct cfattach ugen_ca;
extern struct cfattach uhidev_ca;
extern struct cfattach uhid_ca;
extern struct cfattach fido_ca;
extern struct cfattach ujoy_ca;
extern struct cfattach ukbd_ca;
extern struct cfattach ums_ca;
extern struct cfattach uts_ca;
extern struct cfattach utpms_ca;
extern struct cfattach ucycom_ca;
extern struct cfattach uslhcom_ca;
extern struct cfattach ulpt_ca;
extern struct cfattach umass_ca;
extern struct cfattach uthum_ca;
extern struct cfattach ugold_ca;
extern struct cfattach utrh_ca;
extern struct cfattach uonerng_ca;
extern struct cfattach urng_ca;
extern struct cfattach udcf_ca;
extern struct cfattach umbg_ca;
extern struct cfattach uvisor_ca;
extern struct cfattach udsbr_ca;
extern struct cfattach utwitch_ca;
extern struct cfattach aue_ca;
extern struct cfattach axe_ca;
extern struct cfattach axen_ca;
extern struct cfattach smsc_ca;
extern struct cfattach cue_ca;
extern struct cfattach kue_ca;
extern struct cfattach cdce_ca;
extern struct cfattach urndis_ca;
extern struct cfattach mos_ca;
extern struct cfattach udav_ca;
extern struct cfattach upl_ca;
extern struct cfattach ugl_ca;
extern struct cfattach url_ca;
extern struct cfattach ure_ca;
extern struct cfattach umodem_ca;
extern struct cfattach uftdi_ca;
extern struct cfattach uplcom_ca;
extern struct cfattach umct_ca;
extern struct cfattach uvscom_ca;
extern struct cfattach ubsa_ca;
extern struct cfattach uslcom_ca;
extern struct cfattach uark_ca;
extern struct cfattach moscom_ca;
extern struct cfattach umcs_ca;
extern struct cfattach uscom_ca;
extern struct cfattach ucrcom_ca;
extern struct cfattach uipaq_ca;
extern struct cfattach umsm_ca;
extern struct cfattach uchcom_ca;
extern struct cfattach wi_usb_ca;
extern struct cfattach atu_ca;
extern struct cfattach ural_ca;
extern struct cfattach rum_ca;
extern struct cfattach run_ca;
extern struct cfattach zyd_ca;
extern struct cfattach upgt_ca;
extern struct cfattach urtw_ca;
extern struct cfattach urtwn_ca;
extern struct cfattach rsu_ca;
extern struct cfattach otus_ca;
extern struct cfattach uath_ca;
extern struct cfattach athn_usb_ca;
extern struct cfattach uow_ca;
extern struct cfattach uberry_ca;
extern struct cfattach upd_ca;
extern struct cfattach uwacom_ca;
extern struct cfattach bwfm_usb_ca;
extern struct cfattach uhidpp_ca;
extern struct cfattach ucc_ca;
extern struct cfattach iic_ca;
extern struct cfattach lmtemp_ca;
extern struct cfattach lmenv_ca;
extern struct cfattach maxtmp_ca;
extern struct cfattach adc_ca;
extern struct cfattach tsl_ca;
extern struct cfattach admtmp_ca;
extern struct cfattach maxds_ca;
extern struct cfattach fcu_ca;
extern struct cfattach adt_ca;
extern struct cfattach spdmem_iic_ca;
extern struct cfattach mem_ca;
extern struct cfattach onewire_ca;
extern struct cfattach owid_ca;
extern struct cfattach owsbm_ca;
extern struct cfattach owtemp_ca;
extern struct cfattach owctr_ca;
extern struct cfattach hme_pci_ca;
extern struct cfattach asms_ca;


/* locators */
static long loc[10] = {
	-1, -1, -1, -1, -1, -1, -1, -1,
	1, 0,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"primary",
	"phy",
	"target",
	"lun",
	"bus",
	"dev",
	"function",
	"port",
	"irq",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"console",
	"mux",
	"channel",
	"slot",
	"portno",
	"reportid",
	"addr",
	"size",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 2, 3, -1, 4, -1,
	4, -1, 4, -1, 4, -1, 5, 6,
	-1, 5, 6, -1, 7, -1, 6, 8,
	-1, 7, 9, 10, 11, 12, 13, -1,
	7, 9, 10, 11, 12, 13, -1, 14,
	0, 15, -1, 14, 0, 15, -1, 14,
	15, -1, 14, 15, -1, 14, 15, -1,
	15, -1, 15, -1, 15, -1, 15, -1,
	15, -1, 16, -1, 16, -1, 16, -1,
	16, -1, 16, -1, 16, -1, 17, -1,
	17, -1, 18, -1, 18, -1, 18, -1,
	18, -1, 18, -1, 18, -1, 18, -1,
	18, -1, 18, -1, 18, -1, 18, -1,
	18, -1, 18, -1, 18, -1, 18, -1,
	18, -1, 18, -1, 18, -1, 19, -1,
	20, 21, -1, 20, 21, -1,
};

/* size of parent vectors */
int pv_size = 179;

/* parent vectors */
short pv[179] = {
	274, 222, 221, 218, 217, 212, 211, 210, 209, 140, 109, 108, 107, 94, 92, 65,
	57, 76, 77, 176, 75, 73, 74, 63, 64, 61, 62, 69, 70, 67, 68, 175,
	-1, 223, 206, 227, 228, 224, 225, 226, 229, 233, 234, 230, 231, 232, 235, 236,
	237, 195, 196, -1, 180, 182, 151, 157, 158, 159, 105, 50, 52, 51, 160, 161,
	-1, 7, 5, 198, 135, 106, 54, 49, 6, 58, 55, 53, 48, -1, 89, 85,
	86, 83, 84, 87, 88, -1, 167, 168, 169, 170, 59, -1, 148, 194, 192, 193,
	253, -1, 127, 128, 60, 133, -1, 154, 153, 155, 129, -1, 183, 171, 126, -1,
	147, 191, 256, -1, 162, 163, -1, 178, 179, -1, 181, 182, -1, 184, 50, -1,
	66, 207, -1, 257, -1, 172, -1, 187, -1, 90, -1, 98, -1, 144, -1, 137,
	-1, 138, -1, 152, -1, 269, -1, 32, -1, 250, -1, 177, -1, 146, -1, 34,
	-1, 134, -1, 125, -1, 93, -1, 91, -1, 47, -1, 173, -1, 174, -1, 35,
	-1, 268, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: video* at uvideo*|utvfu* */
    {&video_ca,		&video_cd,	 0, STAR,     loc,    0, pv+122, 0,    0},
/*  1: audio* at uaudio*|utvfu*|awacs*|snapper*|tumbler*|onyx*|cmpci*|eap*|emu*|eso*|aoa*|daca* */
    {&audio_ca,		&audio_cd,	 0, STAR,     loc,    0, pv+52, 0,    0},
/*  2: midi* at umidi*|eap* */
    {&midi_ca,		&midi_cd,	 0, STAR,     loc,    0, pv+125, 0,    0},
/*  3: drm* at radeondrm* primary -1 */
    {&drm_ca,		&drm_cd,	 0, STAR, loc+  5,    0, pv+110, 1,    0},
/*  4: radio* at bktr0|udsbr* */
    {&radio_ca,		&radio_cd,	 0, STAR,     loc,    0, pv+128, 2,    0},
/*  5: vscsi0 at root */
    {&vscsi_ca,		&vscsi_cd,	 0, NORM,     loc,    0, pv+32, 0,    0},
/*  6: mpath0 at root */
    {&mpath_ca,		&mpath_cd,	 0, NORM,     loc,    0, pv+32, 0,    0},
/*  7: softraid0 at root */
    {&softraid_ca,	&softraid_cd,	 0, NORM,     loc,    0, pv+32, 0,    0},
/*  8: nsphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep* phy -1 */
    {&nsphy_ca,		&nsphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 3,    0},
/*  9: qsphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep* phy -1 */
    {&qsphy_ca,		&qsphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 3,    0},
/* 10: inphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep* phy -1 */
    {&inphy_ca,		&inphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 3,    0},
/* 11: iophy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep* phy -1 */
    {&iophy_ca,		&iophy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 3,    0},
/* 12: eephy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep* phy -1 */
    {&eephy_ca,		&eephy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 3,    0},
/* 13: exphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep* phy -1 */
    {&exphy_ca,		&exphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 3,    0},
/* 14: rlphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep* phy -1 */
    {&rlphy_ca,		&rlphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 3,    0},
/* 15: lxtphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep* phy -1 */
    {&lxtphy_ca,	&lxtphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 3,    0},
/* 16: luphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep* phy -1 */
    {&luphy_ca,		&luphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 3,    0},
/* 17: mtdphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep* phy -1 */
    {&mtdphy_ca,	&mtdphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 3,    0},
/* 18: icsphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep* phy -1 */
    {&icsphy_ca,	&icsphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 3,    0},
/* 19: sqphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep* phy -1 */
    {&sqphy_ca,		&sqphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 3,    0},
/* 20: tqphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep* phy -1 */
    {&tqphy_ca,		&tqphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 3,    0},
/* 21: ukphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 3,    0},
/* 22: dcphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep* phy -1 */
    {&dcphy_ca,		&dcphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 3,    0},
/* 23: bmtphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep* phy -1 */
    {&bmtphy_ca,	&bmtphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 3,    0},
/* 24: brgphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep* phy -1 */
    {&brgphy_ca,	&brgphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 3,    0},
/* 25: xmphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep* phy -1 */
    {&xmphy_ca,		&xmphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 3,    0},
/* 26: amphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep* phy -1 */
    {&amphy_ca,		&amphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 3,    0},
/* 27: acphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep* phy -1 */
    {&acphy_ca,		&acphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 3,    0},
/* 28: urlphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep* phy -1 */
    {&urlphy_ca,	&urlphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 3,    0},
/* 29: rgephy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep* phy -1 */
    {&rgephy_ca,	&rgephy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 3,    0},
/* 30: ciphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep* phy -1 */
    {&ciphy_ca,		&ciphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 3,    0},
/* 31: ipgphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl*|ep* phy -1 */
    {&ipgphy_ca,	&ipgphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 3,    0},
/* 32: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+32, 0,    0},
/* 33: cpu* at mainbus0 */
    {&cpu_ca,		&cpu_cd,	 0, STAR,     loc,    0, pv+151, 66,    0},
/* 34: memc* at mainbus0 */
    {&memc_ca,		&memc_cd,	 0, STAR,     loc,    0, pv+151, 66,    0},
/* 35: scsibus* at softraid0|vscsi0|umass*|atapiscsi*|pcscp*|qle*|arc*|mpath0|siop*|mpi*|qlw*|ahc* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+65, 66,    0},
/* 36: cd* at scsibus* target -1 lun -1 */
    {&cd_ca,		&cd_cd,		 0, STAR, loc+  4,    0, pv+175, 67,    0},
/* 37: ch* at scsibus* target -1 lun -1 */
    {&ch_ca,		&ch_cd,		 0, STAR, loc+  4,    0, pv+175, 67,    0},
/* 38: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+  4,    0, pv+175, 67,    0},
/* 39: st* at scsibus* target -1 lun -1 */
    {&st_ca,		&st_cd,		 0, STAR, loc+  4,    0, pv+175, 67,    0},
/* 40: uk* at scsibus* target -1 lun -1 */
    {&uk_ca,		&uk_cd,		 0, STAR, loc+  4,    0, pv+175, 67,    0},
/* 41: safte* at scsibus* target -1 lun -1 */
    {&safte_ca,		&safte_cd,	 0, STAR, loc+  4,    0, pv+175, 67,    0},
/* 42: ses* at scsibus* target -1 lun -1 */
    {&ses_ca,		&ses_cd,	 0, STAR, loc+  4,    0, pv+175, 67,    0},
/* 43: sym* at scsibus* target -1 lun -1 */
    {&sym_ca,		&sym_cd,	 0, STAR, loc+  4,    0, pv+175, 67,    0},
/* 44: rdac* at scsibus* target -1 lun -1 */
    {&rdac_ca,		&rdac_cd,	 0, STAR, loc+  4,    0, pv+175, 67,    0},
/* 45: emc* at scsibus* target -1 lun -1 */
    {&emc_ca,		&emc_cd,	 0, STAR, loc+  4,    0, pv+175, 67,    0},
/* 46: hds* at scsibus* target -1 lun -1 */
    {&hds_ca,		&hds_cd,	 0, STAR, loc+  4,    0, pv+175, 67,    0},
/* 47: pci* at mpcpcibr*|ht*|ppb*|hpb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+  5,    0, pv+98, 70,    0},
/* 48: ahc* at pci* dev -1 function -1 */
    {&ahc_pci_ca,	&ahc_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 49: arc* at pci* dev -1 function -1 */
    {&arc_ca,		&arc_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 50: eap* at pci* dev -1 function -1 */
    {&eap_ca,		&eap_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 51: eso* at pci* dev -1 function -1 */
    {&eso_ca,		&eso_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 52: emu* at pci* dev -1 function -1 */
    {&emu_ca,		&emu_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 53: qlw* at pci* dev -1 function -1 */
    {&qlw_pci_ca,	&qlw_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 54: qle* at pci* dev -1 function -1 */
    {&qle_ca,		&qle_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 55: mpi* at pci* dev -1 function -1 */
    {&mpi_pci_ca,	&mpi_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 56: de* at pci* dev -1 function -1 */
    {&de_ca,		&de_cd,		 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 57: pcn* at pci* dev -1 function -1 */
    {&pcn_ca,		&pcn_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 58: siop* at pci* dev -1 function -1 */
    {&siop_pci_ca,	&siop_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 59: pciide* at pci* dev -1 function -1 */
    {&pciide_pci_ca,	&pciide_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 60: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 61: rl* at pci* dev -1 function -1 */
    {&rl_pci_ca,	&rl_cd,		 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 62: rl* at cardbus* dev -1 function -1 */
    {&rl_cardbus_ca,	&rl_cd,		 0, STAR, loc+  4,    0, pv+171, 81,    0},
/* 63: re* at pci* dev -1 function -1 */
    {&re_pci_ca,	&re_cd,		 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 64: re* at cardbus* dev -1 function -1 */
    {&re_cardbus_ca,	&re_cd,		 0, STAR, loc+  4,    0, pv+171, 81,    0},
/* 65: vr* at pci* dev -1 function -1 */
    {&vr_ca,		&vr_cd,		 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 66: bktr0 at pci* dev -1 function -1 */
    {&bktr_ca,		&bktr_cd,	 0, NORM, loc+  4,    0, pv+169, 78,    0},
/* 67: xl* at pci* dev -1 function -1 */
    {&xl_pci_ca,	&xl_cd,		 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 68: xl* at cardbus* dev -1 function -1 */
    {&xl_cardbus_ca,	&xl_cd,		 0, STAR, loc+  4,    0, pv+171, 81,    0},
/* 69: fxp* at pci* dev -1 function -1 */
    {&fxp_pci_ca,	&fxp_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 70: fxp* at cardbus* dev -1 function -1 */
    {&fxp_cardbus_ca,	&fxp_cd,	 0, STAR, loc+  4,    0, pv+171, 81,    0},
/* 71: em* at pci* dev -1 function -1 */
    {&em_ca,		&em_cd,		 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 72: xge* at pci* dev -1 function -1 */
    {&xge_ca,		&xge_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 73: dc* at pci* dev -1 function -1 */
    {&dc_pci_ca,	&dc_cd,		 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 74: dc* at cardbus* dev -1 function -1 */
    {&dc_cardbus_ca,	&dc_cd,		 0, STAR, loc+  4,    0, pv+171, 81,    0},
/* 75: epic* at pci* dev -1 function -1 */
    {&epic_pci_ca,	&epic_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 76: ti* at pci* dev -1 function -1 */
    {&ti_pci_ca,	&ti_cd,		 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 77: gem* at pci* dev -1 function -1 */
    {&gem_pci_ca,	&gem_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 78: hifn* at pci* dev -1 function -1 */
    {&hifn_ca,		&hifn_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 79: ubsec* at pci* dev -1 function -1 */
    {&ubsec_ca,		&ubsec_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 80: safe* at pci* dev -1 function -1 */
    {&safe_ca,		&safe_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 81: pwdog0 at pci* dev -1 function -1 */
    {&pwdog_ca,		&pwdog_cd,	 0, NORM, loc+  4,    0, pv+169, 78,    0},
/* 82: mbg* at pci* dev -1 function -1 */
    {&mbg_ca,		&mbg_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 83: uhci* at pci* dev -1 function -1 */
    {&uhci_pci_ca,	&uhci_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 84: uhci* at cardbus* dev -1 function -1 */
    {&uhci_cardbus_ca,	&uhci_cd,	 0, STAR, loc+  4,    0, pv+171, 81,    0},
/* 85: ohci* at pci* dev -1 function -1 */
    {&ohci_pci_ca,	&ohci_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 86: ohci* at cardbus* dev -1 function -1 */
    {&ohci_cardbus_ca,	&ohci_cd,	 0, STAR, loc+  4,    0, pv+171, 81,    0},
/* 87: ehci* at pci* dev -1 function -1 */
    {&ehci_pci_ca,	&ehci_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 88: ehci* at cardbus* dev -1 function -1 */
    {&ehci_cardbus_ca,	&ehci_cd,	 0, STAR, loc+  4,    0, pv+171, 81,    0},
/* 89: xhci* at pci* dev -1 function -1 */
    {&xhci_pci_ca,	&xhci_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 90: cbb* at pci* dev -1 function -1 */
    {&cbb_pci_ca,	&cbb_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 91: skc* at pci* dev -1 function -1 */
    {&skc_ca,		&skc_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 92: sk* at skc* */
    {&sk_ca,		&sk_cd,		 0, STAR,     loc,    0, pv+167, 83,    0},
/* 93: mskc* at pci* dev -1 function -1 */
    {&mskc_ca,		&mskc_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 94: msk* at mskc* */
    {&msk_ca,		&msk_cd,	 0, STAR,     loc,    0, pv+165, 83,    0},
/* 95: com* at puc* port -1 */
    {&com_puc_ca,	&com_cd,	 0, STAR, loc+  5,    0, pv+139, 84,    0},
/* 96: com* at pcmcia* function -1 irq -1 */
    {&com_pcmcia_ca,	&com_cd,	 0, STAR, loc+  4,    0, pv+173, 86,    0},
/* 97: com* at cardbus* dev -1 function -1 */
    {&com_cardbus_ca,	&com_cd,	 0, STAR, loc+  4,    0, pv+171, 81,    0},
/* 98: puc* at pci* dev -1 function -1 */
    {&puc_pci_ca,	&puc_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/* 99: wi* at pci* dev -1 function -1 */
    {&wi_pci_ca,	&wi_cd,		 0, STAR, loc+  4,    0, pv+169, 78,    0},
/*100: wi* at macobio0 */
    {&wi_obio_ca,	&wi_cd,		 0, STAR,     loc,    0, pv+143, 88,    0},
/*101: wi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&wi_usb_ca,	&wi_cd,		 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*102: wi* at pcmcia* function -1 irq -1 */
    {&wi_pcmcia_ca,	&wi_cd,		 0, STAR, loc+  4,    0, pv+173, 86,    0},
/*103: an* at pci* dev -1 function -1 */
    {&an_pci_ca,	&an_cd,		 0, STAR, loc+  4,    0, pv+169, 78,    0},
/*104: an* at pcmcia* function -1 irq -1 */
    {&an_pcmcia_ca,	&an_cd,		 0, STAR, loc+  4,    0, pv+173, 86,    0},
/*105: cmpci* at pci* dev -1 function -1 */
    {&cmpci_ca,		&cmpci_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/*106: pcscp* at pci* dev -1 function -1 */
    {&pcscp_ca,		&pcscp_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/*107: bge* at pci* dev -1 function -1 */
    {&bge_ca,		&bge_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/*108: vge* at pci* dev -1 function -1 */
    {&vge_ca,		&vge_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/*109: stge* at pci* dev -1 function -1 */
    {&stge_ca,		&stge_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/*110: ath* at pci* dev -1 function -1 */
    {&ath_pci_ca,	&ath_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/*111: ath* at cardbus* dev -1 function -1 */
    {&ath_cardbus_ca,	&ath_cd,	 0, STAR, loc+  4,    0, pv+171, 81,    0},
/*112: atw* at pci* dev -1 function -1 */
    {&atw_pci_ca,	&atw_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/*113: rtw* at pci* dev -1 function -1 */
    {&rtw_pci_ca,	&rtw_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/*114: rtw* at cardbus* dev -1 function -1 */
    {&rtw_cardbus_ca,	&rtw_cd,	 0, STAR, loc+  4,    0, pv+171, 81,    0},
/*115: ral* at pci* dev -1 function -1 */
    {&ral_pci_ca,	&ral_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/*116: ral* at cardbus* dev -1 function -1 */
    {&ral_cardbus_ca,	&ral_cd,	 0, STAR, loc+  4,    0, pv+171, 81,    0},
/*117: acx* at pci* dev -1 function -1 */
    {&acx_pci_ca,	&acx_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/*118: acx* at cardbus* dev -1 function -1 */
    {&acx_cardbus_ca,	&acx_cd,	 0, STAR, loc+  4,    0, pv+171, 81,    0},
/*119: malo* at pci* dev -1 function -1 */
    {&malo_pci_ca,	&malo_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/*120: malo* at pcmcia* function -1 irq -1 */
    {&malo_pcmcia_ca,	&malo_cd,	 0, STAR, loc+  4,    0, pv+173, 86,    0},
/*121: malo* at cardbus* dev -1 function -1 */
    {&malo_cardbus_ca,	&malo_cd,	 0, STAR, loc+  4,    0, pv+171, 81,    0},
/*122: bwi* at pci* dev -1 function -1 */
    {&bwi_pci_ca,	&bwi_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/*123: bwi* at cardbus* dev -1 function -1 */
    {&bwi_cardbus_ca,	&bwi_cd,	 0, STAR, loc+  4,    0, pv+171, 81,    0},
/*124: agp* at appleagp* */
    {&agp_ca,		&agp_cd,	 0, STAR,     loc,    0, pv+163, 102,    0},
/*125: appleagp* at pchb* */
    {&appleagp_ca,	&appleagp_cd,	 0, STAR,     loc,    0, pv+161, 102,    0},
/*126: radeondrm* at pci* dev -1 function -1 */
    {&radeondrm_ca,	&radeondrm_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/*127: mpcpcibr* at mainbus0 */
    {&mpcpcibr_ca,	&mpcpcibr_cd,	 0, STAR,     loc,    0, pv+151, 66,    0},
/*128: ht* at mainbus0 */
    {&ht_ca,		&ht_cd,		 0, STAR,     loc,    0, pv+151, 66,    0},
/*129: smu* at mainbus0 */
    {&smu_ca,		&smu_cd,	 0, STAR,     loc,    0, pv+151, 66,    0},
/*130: wsdisplay* at udl*|vgafb0|radeondrm* console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+  6,    0, pv+108, 103,    0},
/*131: wskbd* at akbd*|ukbd*|ucc* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+  7,    0, pv+112, 111,    0},
/*132: wsmouse* at ams*|utpms*|ums*|uts*|uwacom* mux 0 */
    {&wsmouse_ca,	&wsmouse_cd,	 0, STAR, loc+  9,    0, pv+92, 120,    0},
/*133: hpb* at pci* dev -1 function -1 */
    {&hpb_ca,		&hpb_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/*134: pchb* at pci* dev -1 function -1 */
    {&pchb_ca,		&pchb_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/*135: atapiscsi* at wdc*|wdc*|wdc*|wdc*|pciide* channel -1 */
    {&atapiscsi_ca,	&atapiscsi_cd,	 0, STAR, loc+  5,    0, pv+86, 130,    0},
/*136: wd* at wdc*|wdc*|wdc*|wdc*|pciide* channel -1 drive -1 */
    {&wd_ca,		&wd_cd,		 0, STAR, loc+  4,    0, pv+86, 130,    0},
/*137: macobio0 at pci* dev -1 function -1 */
    {&macobio_ca,	&macobio_cd,	 0, NORM, loc+  4,    0, pv+169, 78,    0},
/*138: kauaiata* at pci* dev -1 function -1 */
    {&kauaiata_ca,	&kauaiata_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/*139: mc* at macobio0 */
    {&mc_ca,		&mc_cd,		 0, STAR,     loc,    0, pv+143, 88,    0},
/*140: bm* at macobio0 */
    {&bm_ca,		&bm_cd,		 0, STAR,     loc,    0, pv+143, 88,    0},
/*141: macintr0 at macobio0 */
    {&macintr_ca,	&macintr_cd,	 0, NORM,     loc,    0, pv+143, 88,    0},
/*142: openpic* at memc* */
    {&openpic_ca,	&openpic_cd,	 0, STAR,     loc,    0, pv+159, 139,    0},
/*143: openpic* at macobio0 */
    {&openpic_ca,	&openpic_cd,	 0, STAR,     loc,    0, pv+143, 88,    0},
/*144: zs* at macobio0 */
    {&zs_ca,		&zs_cd,		 0, STAR,     loc,    0, pv+143, 88,    0},
/*145: zstty* at zs* channel -1 */
    {&zstty_ca,		&zstty_cd,	 0, STAR, loc+  5,    0, pv+141, 140,    0},
/*146: adb* at macobio0 */
    {&adb_ca,		&adb_cd,	 0, STAR,     loc,    0, pv+143, 88,    0},
/*147: akbd* at adb* */
    {&akbd_ca,		&akbd_cd,	 0, STAR,     loc,    0, pv+157, 141,    0},
/*148: ams* at adb* */
    {&ams_ca,		&ams_cd,	 0, STAR,     loc,    0, pv+157, 141,    0},
/*149: apm0 at adb* */
    {&apm_ca,		&apm_cd,	 0, NORM,     loc,    0, pv+157, 141,    0},
/*150: abtn* at adb* */
    {&abtn_ca,		&abtn_cd,	 0, STAR,     loc,    0, pv+157, 141,    0},
/*151: awacs* at macobio0 */
    {&awacs_ca,		&awacs_cd,	 0, STAR,     loc,    0, pv+143, 88,    0},
/*152: mediabay* at macobio0 */
    {&mediabay_ca,	&mediabay_cd,	 0, STAR,     loc,    0, pv+143, 88,    0},
/*153: kiic* at macobio0 */
    {&kiic_ca,		&kiic_cd,	 0, STAR,     loc,    0, pv+143, 88,    0},
/*154: kiic* at memc* */
    {&kiic_memc_ca,	&kiic_cd,	 0, STAR,     loc,    0, pv+159, 139,    0},
/*155: piic0 at adb* */
    {&piic_ca,		&piic_cd,	 0, NORM,     loc,    0, pv+157, 141,    0},
/*156: xlights* at macobio0 */
    {&xlights_ca,	&xlights_cd,	 0, STAR,     loc,    0, pv+143, 88,    0},
/*157: snapper* at macobio0 */
    {&snapper_ca,	&snapper_cd,	 0, STAR,     loc,    0, pv+143, 88,    0},
/*158: tumbler* at macobio0 */
    {&tumbler_ca,	&tumbler_cd,	 0, STAR,     loc,    0, pv+143, 88,    0},
/*159: onyx* at macobio0 */
    {&onyx_ca,		&onyx_cd,	 0, STAR,     loc,    0, pv+143, 88,    0},
/*160: aoa* at macobio0 */
    {&aoa_ca,		&aoa_cd,	 0, STAR,     loc,    0, pv+143, 88,    0},
/*161: daca* at macobio0 */
    {&daca_ca,		&daca_cd,	 0, STAR,     loc,    0, pv+143, 88,    0},
/*162: macgpio* at macobio0 */
    {&macgpio_ca,	&macgpio_cd,	 0, STAR,     loc,    0, pv+143, 88,    0},
/*163: macgpio* at macgpio*|macgpio* */
    {&macgpio_gpio_ca,	&macgpio_cd,	 0, STAR,     loc,    0, pv+116, 141,    0},
/*164: sysbutton* at macgpio*|macgpio* */
    {&sysbutton_ca,	&sysbutton_cd,	 0, STAR,     loc,    0, pv+116, 141,    0},
/*165: pgs* at macgpio*|macgpio* */
    {&pgs_ca,		&pgs_cd,	 0, STAR,     loc,    0, pv+116, 141,    0},
/*166: dfs* at macgpio*|macgpio* */
    {&dfs_ca,		&dfs_cd,	 0, STAR,     loc,    0, pv+116, 141,    0},
/*167: wdc* at kauaiata* */
    {&wdc_obio_ca,	&wdc_cd,	 0, STAR,     loc,    0, pv+145, 141,    0},
/*168: wdc* at mediabay* */
    {&wdc_obio_ca,	&wdc_cd,	 0, STAR,     loc,    0, pv+147, 141,    0},
/*169: wdc* at macobio0 */
    {&wdc_obio_ca,	&wdc_cd,	 0, STAR,     loc,    0, pv+143, 88,    0},
/*170: wdc* at pcmcia* function -1 irq -1 */
    {&wdc_pcmcia_ca,	&wdc_cd,	 0, STAR, loc+  4,    0, pv+173, 86,    0},
/*171: vgafb0 at pci* dev -1 function -1 */
    {&vgafb_ca,		&vgafb_cd,	 0, NORM, loc+  4,    0, pv+169, 78,    0},
/*172: cardslot* at cbb* slot -1 */
    {&cardslot_ca,	&cardslot_cd,	 0, STAR, loc+  5,    0, pv+137, 142,    0},
/*173: cardbus* at cardslot* slot -1 */
    {&cardbus_ca,	&cardbus_cd,	 0, STAR, loc+  5,    0, pv+133, 144,    0},
/*174: pcmcia* at cardslot* controller -1 socket -1 */
    {&pcmcia_ca,	&pcmcia_cd,	 0, STAR, loc+  4,    0, pv+133, 144,    0},
/*175: ep* at pcmcia* function -1 irq -1 */
    {&ep_pcmcia_ca,	&ep_cd,		 0, STAR, loc+  4,    0, pv+173, 86,    0},
/*176: ne* at pcmcia* function -1 irq -1 */
    {&ne_pcmcia_ca,	&ne_cd,		 0, STAR, loc+  4,    0, pv+173, 86,    0},
/*177: usb* at xhci*|ohci*|ohci*|uhci*|uhci*|ehci*|ehci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+78, 145,    0},
/*178: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+155, 145,    0},
/*179: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*180: uaudio* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uaudio_ca,	&uaudio_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*181: uvideo* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvideo_ca,	&uvideo_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*182: utvfu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&utvfu_ca,		&utvfu_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*183: udl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udl_ca,		&udl_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*184: umidi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umidi_ca,		&umidi_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*185: ucom* at umodem*|uvisor*|uvscom*|ubsa*|uftdi*|uplcom*|umct*|uslcom*|uscom*|ucrcom*|uark*|moscom*|umcs*|uipaq*|umsm*|uchcom*|ucycom*|uslhcom* portno -1 */
    {&ucom_ca,		&ucom_cd,	 0, STAR, loc+  5,    0, pv+33, 146,    0},
/*186: ugen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugen_ca,		&ugen_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*187: uhidev* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhidev_ca,	&uhidev_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*188: uhid* at uhidev* reportid -1 */
    {&uhid_ca,		&uhid_cd,	 0, STAR, loc+  5,    0, pv+135, 182,    0},
/*189: fido* at uhidev* reportid -1 */
    {&fido_ca,		&fido_cd,	 0, STAR, loc+  5,    0, pv+135, 182,    0},
/*190: ujoy* at uhidev* reportid -1 */
    {&ujoy_ca,		&ujoy_cd,	 0, STAR, loc+  5,    0, pv+135, 182,    0},
/*191: ukbd* at uhidev* reportid -1 */
    {&ukbd_ca,		&ukbd_cd,	 0, STAR, loc+  5,    0, pv+135, 182,    0},
/*192: ums* at uhidev* reportid -1 */
    {&ums_ca,		&ums_cd,	 0, STAR, loc+  5,    0, pv+135, 182,    0},
/*193: uts* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uts_ca,		&uts_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*194: utpms* at uhidev* reportid -1 */
    {&utpms_ca,		&utpms_cd,	 0, STAR, loc+  5,    0, pv+135, 182,    0},
/*195: ucycom* at uhidev* reportid -1 */
    {&ucycom_ca,	&ucycom_cd,	 0, STAR, loc+  5,    0, pv+135, 182,    0},
/*196: uslhcom* at uhidev* reportid -1 */
    {&uslhcom_ca,	&uslhcom_cd,	 0, STAR, loc+  5,    0, pv+135, 182,    0},
/*197: ulpt* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ulpt_ca,		&ulpt_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*198: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*199: uthum* at uhidev* reportid -1 */
    {&uthum_ca,		&uthum_cd,	 0, STAR, loc+  5,    0, pv+135, 182,    0},
/*200: ugold* at uhidev* reportid -1 */
    {&ugold_ca,		&ugold_cd,	 0, STAR, loc+  5,    0, pv+135, 182,    0},
/*201: utrh* at uhidev* reportid -1 */
    {&utrh_ca,		&utrh_cd,	 0, STAR, loc+  5,    0, pv+135, 182,    0},
/*202: uonerng* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uonerng_ca,	&uonerng_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*203: urng* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urng_ca,		&urng_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*204: udcf* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udcf_ca,		&udcf_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*205: umbg* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umbg_ca,		&umbg_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*206: uvisor* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvisor_ca,	&uvisor_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*207: udsbr* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udsbr_ca,		&udsbr_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*208: utwitch* at uhidev* reportid -1 */
    {&utwitch_ca,	&utwitch_cd,	 0, STAR, loc+  5,    0, pv+135, 182,    0},
/*209: aue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&aue_ca,		&aue_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*210: axe* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axe_ca,		&axe_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*211: axen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axen_ca,		&axen_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*212: smsc* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&smsc_ca,		&smsc_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*213: cue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cue_ca,		&cue_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*214: kue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&kue_ca,		&kue_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*215: cdce* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cdce_ca,		&cdce_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*216: urndis* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urndis_ca,	&urndis_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*217: mos* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mos_ca,		&mos_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*218: udav* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udav_ca,		&udav_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*219: upl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upl_ca,		&upl_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*220: ugl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugl_ca,		&ugl_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*221: url* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&url_ca,		&url_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*222: ure* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ure_ca,		&ure_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*223: umodem* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umodem_ca,	&umodem_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*224: uftdi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uftdi_ca,		&uftdi_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*225: uplcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uplcom_ca,	&uplcom_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*226: umct* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umct_ca,		&umct_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*227: uvscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvscom_ca,	&uvscom_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*228: ubsa* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ubsa_ca,		&ubsa_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*229: uslcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uslcom_ca,	&uslcom_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*230: uark* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uark_ca,		&uark_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*231: moscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&moscom_ca,	&moscom_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*232: umcs* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umcs_ca,		&umcs_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*233: uscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uscom_ca,		&uscom_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*234: ucrcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ucrcom_ca,	&ucrcom_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*235: uipaq* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uipaq_ca,		&uipaq_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*236: umsm* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umsm_ca,		&umsm_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*237: uchcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uchcom_ca,	&uchcom_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*238: atu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&atu_ca,		&atu_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*239: ural* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ural_ca,		&ural_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*240: rum* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rum_ca,		&rum_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*241: run* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&run_ca,		&run_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*242: zyd* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&zyd_ca,		&zyd_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*243: upgt* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upgt_ca,		&upgt_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*244: urtw* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtw_ca,		&urtw_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*245: urtwn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtwn_ca,		&urtwn_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*246: rsu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rsu_ca,		&rsu_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*247: otus* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&otus_ca,		&otus_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*248: uath* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uath_ca,		&uath_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*249: athn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&athn_usb_ca,	&athn_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*250: uow* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uow_ca,		&uow_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*251: uberry* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uberry_ca,	&uberry_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*252: upd* at uhidev* reportid -1 */
    {&upd_ca,		&upd_cd,	 0, STAR, loc+  5,    0, pv+135, 182,    0},
/*253: uwacom* at uhidev* reportid -1 */
    {&uwacom_ca,	&uwacom_cd,	 0, STAR, loc+  5,    0, pv+135, 182,    0},
/*254: bwfm* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&bwfm_usb_ca,	&bwfm_cd,	 0, STAR, loc+  0,    0, pv+119, 89,    0},
/*255: uhidpp* at uhidev* reportid -1 */
    {&uhidpp_ca,	&uhidpp_cd,	 0, STAR, loc+  5,    0, pv+135, 182,    0},
/*256: ucc* at uhidev* reportid -1 */
    {&ucc_ca,		&ucc_cd,	 0, STAR, loc+  5,    0, pv+135, 182,    0},
/*257: iic* at kiic*|kiic*|piic0|smu* */
    {&iic_ca,		&iic_cd,	 0, STAR,     loc,    0, pv+103, 183,    0},
/*258: lmtemp* at iic* addr -1 size -1 */
    {&lmtemp_ca,	&lmtemp_cd,	 0, STAR, loc+  4,    0, pv+131, 184,    0},
/*259: lmenv* at iic* addr -1 size -1 */
    {&lmenv_ca,		&lmenv_cd,	 0, STAR, loc+  4,    0, pv+131, 184,    0},
/*260: maxtmp* at iic* addr -1 size -1 */
    {&maxtmp_ca,	&maxtmp_cd,	 0, STAR, loc+  4,    0, pv+131, 184,    0},
/*261: adc* at iic* addr -1 size -1 */
    {&adc_ca,		&adc_cd,	 0, STAR, loc+  4,    0, pv+131, 184,    0},
/*262: tsl* at iic* addr -1 size -1 */
    {&tsl_ca,		&tsl_cd,	 0, STAR, loc+  4,    0, pv+131, 184,    0},
/*263: admtmp* at iic* addr -1 size -1 */
    {&admtmp_ca,	&admtmp_cd,	 0, STAR, loc+  4,    0, pv+131, 184,    0},
/*264: maxds* at iic* addr -1 size -1 */
    {&maxds_ca,		&maxds_cd,	 0, STAR, loc+  4,    0, pv+131, 184,    0},
/*265: fcu* at iic* addr -1 size -1 */
    {&fcu_ca,		&fcu_cd,	 0, STAR, loc+  4,    0, pv+131, 184,    0},
/*266: adt* at iic* addr -1 size -1 */
    {&adt_ca,		&adt_cd,	 0, STAR, loc+  4,    0, pv+131, 184,    0},
/*267: spdmem* at mem* addr -1 size -1 */
    {&spdmem_iic_ca,	&spdmem_cd,	 0, STAR, loc+  4,    0, pv+177, 187,    0},
/*268: mem* at mainbus0 */
    {&mem_ca,		&mem_cd,	 0, STAR,     loc,    0, pv+151, 66,    0},
/*269: onewire* at uow* */
    {&onewire_ca,	&onewire_cd,	 0, STAR,     loc,    0, pv+153, 189,    0},
/*270: owid* at onewire* */
    {&owid_ca,		&owid_cd,	 0, STAR,     loc,    0, pv+149, 189,    0},
/*271: owsbm* at onewire* */
    {&owsbm_ca,		&owsbm_cd,	 0, STAR,     loc,    0, pv+149, 189,    0},
/*272: owtemp* at onewire* */
    {&owtemp_ca,	&owtemp_cd,	 0, STAR,     loc,    0, pv+149, 189,    0},
/*273: owctr* at onewire* */
    {&owctr_ca,		&owctr_cd,	 0, STAR,     loc,    0, pv+149, 189,    0},
/*274: hme* at pci* dev -1 function -1 */
    {&hme_pci_ca,	&hme_cd,	 0, STAR, loc+  4,    0, pv+169, 78,    0},
/*275: asms* at iic* addr -1 size -1 */
    {&asms_ca,		&asms_cd,	 0, STAR, loc+  4,    0, pv+131, 184,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 5 /* vscsi0 */,
	 6 /* mpath0 */,
	 7 /* softraid0 */,
	32 /* mainbus0 */,
	-1
};

int cfroots_size = 5;

/* pseudo-devices */
extern void pfattach(int);
extern void pflogattach(int);
extern void pfsyncattach(int);
extern void pflowattach(int);
extern void encattach(int);
extern void ptyattach(int);
extern void nmeaattach(int);
extern void mstsattach(int);
extern void endrunattach(int);
extern void vndattach(int);
extern void ksymsattach(int);
extern void bpfilterattach(int);
extern void bridgeattach(int);
extern void vebattach(int);
extern void carpattach(int);
extern void etheripattach(int);
extern void gifattach(int);
extern void greattach(int);
extern void loopattach(int);
extern void mpeattach(int);
extern void mpwattach(int);
extern void mpipattach(int);
extern void bpeattach(int);
extern void pairattach(int);
extern void pppattach(int);
extern void pppoeattach(int);
extern void pppxattach(int);
extern void spppattach(int);
extern void trunkattach(int);
extern void aggrattach(int);
extern void tpmrattach(int);
extern void tunattach(int);
extern void vetherattach(int);
extern void vxlanattach(int);
extern void vlanattach(int);
extern void switchattach(int);
extern void wgattach(int);
extern void bioattach(int);
extern void fuseattach(int);
extern void hotplugattach(int);
extern void wsmuxattach(int);

char *pdevnames[] = {
	"pf",
	"pflog",
	"pfsync",
	"pflow",
	"enc",
	"pty",
	"nmea",
	"msts",
	"endrun",
	"vnd",
	"ksyms",
	"bpfilter",
	"bridge",
	"veb",
	"carp",
	"etherip",
	"gif",
	"gre",
	"loop",
	"mpe",
	"mpw",
	"mpip",
	"bpe",
	"pair",
	"ppp",
	"pppoe",
	"pppx",
	"sppp",
	"trunk",
	"aggr",
	"tpmr",
	"tun",
	"vether",
	"vxlan",
	"vlan",
	"switch",
	"wg",
	"bio",
	"fuse",
	"hotplug",
	"wsmux",
};

int pdevnames_size = 41;

struct pdevinit pdevinit[] = {
	{ pfattach, 1 },
	{ pflogattach, 1 },
	{ pfsyncattach, 1 },
	{ pflowattach, 1 },
	{ encattach, 1 },
	{ ptyattach, 16 },
	{ nmeaattach, 1 },
	{ mstsattach, 1 },
	{ endrunattach, 1 },
	{ vndattach, 4 },
	{ ksymsattach, 1 },
	{ bpfilterattach, 1 },
	{ bridgeattach, 1 },
	{ vebattach, 1 },
	{ carpattach, 1 },
	{ etheripattach, 1 },
	{ gifattach, 1 },
	{ greattach, 1 },
	{ loopattach, 1 },
	{ mpeattach, 1 },
	{ mpwattach, 1 },
	{ mpipattach, 1 },
	{ bpeattach, 1 },
	{ pairattach, 1 },
	{ pppattach, 1 },
	{ pppoeattach, 1 },
	{ pppxattach, 1 },
	{ spppattach, 1 },
	{ trunkattach, 1 },
	{ aggrattach, 1 },
	{ tpmrattach, 1 },
	{ tunattach, 1 },
	{ vetherattach, 1 },
	{ vxlanattach, 1 },
	{ vlanattach, 1 },
	{ switchattach, 1 },
	{ wgattach, 1 },
	{ bioattach, 1 },
	{ fuseattach, 1 },
	{ hotplugattach, 1 },
	{ wsmuxattach, 2 },
	{ NULL, 0 }
};
