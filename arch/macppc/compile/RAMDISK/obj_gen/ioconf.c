/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/macppc/conf/RAMDISK"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver wdc_cd;
extern struct cfdriver ahc_cd;
extern struct cfdriver qlw_cd;
extern struct cfdriver qla_cd;
extern struct cfdriver mpi_cd;
extern struct cfdriver siop_cd;
extern struct cfdriver wi_cd;
extern struct cfdriver an_cd;
extern struct cfdriver xl_cd;
extern struct cfdriver fxp_cd;
extern struct cfdriver rl_cd;
extern struct cfdriver re_cd;
extern struct cfdriver dc_cd;
extern struct cfdriver epic_cd;
extern struct cfdriver ne_cd;
extern struct cfdriver gem_cd;
extern struct cfdriver ti_cd;
extern struct cfdriver ath_cd;
extern struct cfdriver bwfm_cd;
extern struct cfdriver ral_cd;
extern struct cfdriver uhci_cd;
extern struct cfdriver ohci_cd;
extern struct cfdriver ehci_cd;
extern struct cfdriver xhci_cd;
extern struct cfdriver softraid_cd;
extern struct cfdriver nsphy_cd;
extern struct cfdriver qsphy_cd;
extern struct cfdriver inphy_cd;
extern struct cfdriver iophy_cd;
extern struct cfdriver eephy_cd;
extern struct cfdriver exphy_cd;
extern struct cfdriver rlphy_cd;
extern struct cfdriver lxtphy_cd;
extern struct cfdriver luphy_cd;
extern struct cfdriver mtdphy_cd;
extern struct cfdriver icsphy_cd;
extern struct cfdriver sqphy_cd;
extern struct cfdriver tqphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver dcphy_cd;
extern struct cfdriver bmtphy_cd;
extern struct cfdriver brgphy_cd;
extern struct cfdriver xmphy_cd;
extern struct cfdriver amphy_cd;
extern struct cfdriver acphy_cd;
extern struct cfdriver urlphy_cd;
extern struct cfdriver rgephy_cd;
extern struct cfdriver ciphy_cd;
extern struct cfdriver ipgphy_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver memc_cd;
extern struct cfdriver wd_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver cd_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver st_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver arc_cd;
extern struct cfdriver qle_cd;
extern struct cfdriver de_cd;
extern struct cfdriver pcn_cd;
extern struct cfdriver pciide_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver vr_cd;
extern struct cfdriver em_cd;
extern struct cfdriver xge_cd;
extern struct cfdriver cbb_cd;
extern struct cfdriver skc_cd;
extern struct cfdriver sk_cd;
extern struct cfdriver mskc_cd;
extern struct cfdriver msk_cd;
extern struct cfdriver pcscp_cd;
extern struct cfdriver bge_cd;
extern struct cfdriver vge_cd;
extern struct cfdriver stge_cd;
extern struct cfdriver mpcpcibr_cd;
extern struct cfdriver ht_cd;
extern struct cfdriver smu_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver hpb_cd;
extern struct cfdriver pchb_cd;
extern struct cfdriver atapiscsi_cd;
extern struct cfdriver macobio_cd;
extern struct cfdriver kauaiata_cd;
extern struct cfdriver mc_cd;
extern struct cfdriver bm_cd;
extern struct cfdriver macintr_cd;
extern struct cfdriver openpic_cd;
extern struct cfdriver zs_cd;
extern struct cfdriver zstty_cd;
extern struct cfdriver adb_cd;
extern struct cfdriver akbd_cd;
extern struct cfdriver mediabay_cd;
extern struct cfdriver kiic_cd;
extern struct cfdriver macgpio_cd;
extern struct cfdriver vgafb_cd;
extern struct cfdriver cardslot_cd;
extern struct cfdriver cardbus_cd;
extern struct cfdriver pcmcia_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver uhidev_cd;
extern struct cfdriver uhid_cd;
extern struct cfdriver ukbd_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver aue_cd;
extern struct cfdriver axe_cd;
extern struct cfdriver axen_cd;
extern struct cfdriver smsc_cd;
extern struct cfdriver cue_cd;
extern struct cfdriver kue_cd;
extern struct cfdriver cdce_cd;
extern struct cfdriver mos_cd;
extern struct cfdriver udav_cd;
extern struct cfdriver upl_cd;
extern struct cfdriver ugl_cd;
extern struct cfdriver url_cd;
extern struct cfdriver ure_cd;
extern struct cfdriver ural_cd;
extern struct cfdriver rum_cd;
extern struct cfdriver zyd_cd;
extern struct cfdriver urtwn_cd;
extern struct cfdriver iic_cd;
extern struct cfdriver fcu_cd;
extern struct cfdriver hme_cd;

extern struct cfattach softraid_ca;
extern struct cfattach nsphy_ca;
extern struct cfattach qsphy_ca;
extern struct cfattach inphy_ca;
extern struct cfattach iophy_ca;
extern struct cfattach eephy_ca;
extern struct cfattach exphy_ca;
extern struct cfattach rlphy_ca;
extern struct cfattach lxtphy_ca;
extern struct cfattach luphy_ca;
extern struct cfattach mtdphy_ca;
extern struct cfattach icsphy_ca;
extern struct cfattach sqphy_ca;
extern struct cfattach tqphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach dcphy_ca;
extern struct cfattach bmtphy_ca;
extern struct cfattach brgphy_ca;
extern struct cfattach xmphy_ca;
extern struct cfattach amphy_ca;
extern struct cfattach acphy_ca;
extern struct cfattach urlphy_ca;
extern struct cfattach rgephy_ca;
extern struct cfattach ciphy_ca;
extern struct cfattach ipgphy_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach cpu_ca;
extern struct cfattach memc_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach cd_ca;
extern struct cfattach sd_ca;
extern struct cfattach st_ca;
extern struct cfattach pci_ca;
extern struct cfattach ahc_pci_ca;
extern struct cfattach arc_ca;
extern struct cfattach qlw_pci_ca;
extern struct cfattach qla_pci_ca;
extern struct cfattach qle_ca;
extern struct cfattach mpi_pci_ca;
extern struct cfattach de_ca;
extern struct cfattach pcn_ca;
extern struct cfattach siop_pci_ca;
extern struct cfattach pciide_pci_ca;
extern struct cfattach ppb_ca;
extern struct cfattach rl_pci_ca;
extern struct cfattach re_pci_ca;
extern struct cfattach vr_ca;
extern struct cfattach xl_pci_ca;
extern struct cfattach fxp_pci_ca;
extern struct cfattach em_ca;
extern struct cfattach xge_ca;
extern struct cfattach dc_pci_ca;
extern struct cfattach epic_pci_ca;
extern struct cfattach ti_pci_ca;
extern struct cfattach gem_pci_ca;
extern struct cfattach uhci_pci_ca;
extern struct cfattach ohci_pci_ca;
extern struct cfattach ehci_pci_ca;
extern struct cfattach xhci_pci_ca;
extern struct cfattach cbb_pci_ca;
extern struct cfattach skc_ca;
extern struct cfattach sk_ca;
extern struct cfattach mskc_ca;
extern struct cfattach msk_ca;
extern struct cfattach wi_pci_ca;
extern struct cfattach an_pci_ca;
extern struct cfattach pcscp_ca;
extern struct cfattach bge_ca;
extern struct cfattach vge_ca;
extern struct cfattach stge_ca;
extern struct cfattach ath_pci_ca;
extern struct cfattach ral_pci_ca;
extern struct cfattach mpcpcibr_ca;
extern struct cfattach ht_ca;
extern struct cfattach smu_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach hpb_ca;
extern struct cfattach pchb_ca;
extern struct cfattach atapiscsi_ca;
extern struct cfattach wd_ca;
extern struct cfattach macobio_ca;
extern struct cfattach kauaiata_ca;
extern struct cfattach mc_ca;
extern struct cfattach bm_ca;
extern struct cfattach macintr_ca;
extern struct cfattach openpic_ca;
extern struct cfattach zs_ca;
extern struct cfattach zstty_ca;
extern struct cfattach adb_ca;
extern struct cfattach akbd_ca;
extern struct cfattach mediabay_ca;
extern struct cfattach kiic_memc_ca;
extern struct cfattach macgpio_ca;
extern struct cfattach macgpio_gpio_ca;
extern struct cfattach wdc_obio_ca;
extern struct cfattach wi_obio_ca;
extern struct cfattach vgafb_ca;
extern struct cfattach cardslot_ca;
extern struct cfattach cardbus_ca;
extern struct cfattach xl_cardbus_ca;
extern struct cfattach dc_cardbus_ca;
extern struct cfattach fxp_cardbus_ca;
extern struct cfattach rl_cardbus_ca;
extern struct cfattach re_cardbus_ca;
extern struct cfattach ath_cardbus_ca;
extern struct cfattach ral_cardbus_ca;
extern struct cfattach ehci_cardbus_ca;
extern struct cfattach ohci_cardbus_ca;
extern struct cfattach pcmcia_ca;
extern struct cfattach ne_pcmcia_ca;
extern struct cfattach wdc_pcmcia_ca;
extern struct cfattach wi_pcmcia_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach uhidev_ca;
extern struct cfattach uhid_ca;
extern struct cfattach ukbd_ca;
extern struct cfattach umass_ca;
extern struct cfattach aue_ca;
extern struct cfattach axe_ca;
extern struct cfattach axen_ca;
extern struct cfattach smsc_ca;
extern struct cfattach cue_ca;
extern struct cfattach kue_ca;
extern struct cfattach cdce_ca;
extern struct cfattach mos_ca;
extern struct cfattach udav_ca;
extern struct cfattach upl_ca;
extern struct cfattach ugl_ca;
extern struct cfattach url_ca;
extern struct cfattach ure_ca;
extern struct cfattach wi_usb_ca;
extern struct cfattach ural_ca;
extern struct cfattach rum_ca;
extern struct cfattach zyd_ca;
extern struct cfattach urtwn_ca;
extern struct cfattach bwfm_usb_ca;
extern struct cfattach iic_ca;
extern struct cfattach fcu_ca;
extern struct cfattach hme_pci_ca;


/* locators */
static long loc[9] = {
	-1, -1, -1, -1, -1, -1, -1, -1,
	1,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"phy",
	"target",
	"lun",
	"bus",
	"dev",
	"function",
	"port",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"irq",
	"console",
	"primary",
	"mux",
	"channel",
	"slot",
	"reportid",
	"addr",
	"size",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 0,
	-1, 0, -1, 0, -1, 0, -1, 1,
	2, -1, 3, -1, 3, -1, 3, -1,
	3, -1, 4, 5, -1, 4, 5, -1,
	6, 7, 8, 9, 10, 11, -1, 6,
	7, 8, 9, 10, 11, -1, 5, 12,
	-1, 13, 14, 15, -1, 13, 15, -1,
	13, 15, -1, 16, -1, 16, -1, 16,
	-1, 16, -1, 16, -1, 16, -1, 17,
	-1, 17, -1, 18, -1, 19, 20, -1,
};

/* size of parent vectors */
int pv_size = 111;

/* parent vectors */
short pv[111] = {
	144, 136, 135, 132, 131, 127, 126, 125, 124, 96, 79, 78, 77, 70, 68, 48,
	40, 58, 59, 116, 57, 55, 56, 46, 47, 44, 45, 51, 52, 49, 50, -1,
	123, 91, 76, 37, 34, 0, 41, 38, 36, 35, 33, -1, 65, 61, 62, 60,
	63, 64, -1, 108, 109, 110, 111, 42, -1, 84, 85, 43, 89, -1, 118, 119,
	-1, 106, 107, -1, 103, 122, -1, 32, -1, 115, -1, 113, -1, 142, -1, 28,
	-1, 100, -1, 66, -1, 120, -1, 25, -1, 94, -1, 104, -1, 93, -1, 67,
	-1, 69, -1, 105, -1, 27, -1, 117, -1, 112, -1, 114, -1, 102, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: softraid0 at root */
    {&softraid_ca,	&softraid_cd,	 0, NORM,     loc,    0, pv+31, 0,    0},
/*  1: nsphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl* phy -1 */
    {&nsphy_ca,		&nsphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 1,    0},
/*  2: qsphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl* phy -1 */
    {&qsphy_ca,		&qsphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 1,    0},
/*  3: inphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl* phy -1 */
    {&inphy_ca,		&inphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 1,    0},
/*  4: iophy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl* phy -1 */
    {&iophy_ca,		&iophy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 1,    0},
/*  5: eephy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl* phy -1 */
    {&eephy_ca,		&eephy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 1,    0},
/*  6: exphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl* phy -1 */
    {&exphy_ca,		&exphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 1,    0},
/*  7: rlphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl* phy -1 */
    {&rlphy_ca,		&rlphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 1,    0},
/*  8: lxtphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl* phy -1 */
    {&lxtphy_ca,	&lxtphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 1,    0},
/*  9: luphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl* phy -1 */
    {&luphy_ca,		&luphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 1,    0},
/* 10: mtdphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl* phy -1 */
    {&mtdphy_ca,	&mtdphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 1,    0},
/* 11: icsphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl* phy -1 */
    {&icsphy_ca,	&icsphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 1,    0},
/* 12: sqphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl* phy -1 */
    {&sqphy_ca,		&sqphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 1,    0},
/* 13: tqphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl* phy -1 */
    {&tqphy_ca,		&tqphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 1,    0},
/* 14: ukphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 1,    0},
/* 15: dcphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl* phy -1 */
    {&dcphy_ca,		&dcphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 1,    0},
/* 16: bmtphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl* phy -1 */
    {&bmtphy_ca,	&bmtphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 1,    0},
/* 17: brgphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl* phy -1 */
    {&brgphy_ca,	&brgphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 1,    0},
/* 18: xmphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl* phy -1 */
    {&xmphy_ca,		&xmphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 1,    0},
/* 19: amphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl* phy -1 */
    {&amphy_ca,		&amphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 1,    0},
/* 20: acphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl* phy -1 */
    {&acphy_ca,		&acphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 1,    0},
/* 21: urlphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl* phy -1 */
    {&urlphy_ca,	&urlphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 1,    0},
/* 22: rgephy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl* phy -1 */
    {&rgephy_ca,	&rgephy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 1,    0},
/* 23: ciphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl* phy -1 */
    {&ciphy_ca,		&ciphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 1,    0},
/* 24: ipgphy* at hme*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|bm*|stge*|vge*|bge*|msk*|sk*|vr*|pcn*|ti*|gem*|ne*|epic*|dc*|dc*|re*|re*|rl*|rl*|fxp*|fxp*|xl*|xl* phy -1 */
    {&ipgphy_ca,	&ipgphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 1,    0},
/* 25: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+31, 0,    0},
/* 26: cpu0 at mainbus0 */
    {&cpu_ca,		&cpu_cd,	 0, NORM,     loc,    0, pv+87, 62,    0},
/* 27: memc* at mainbus0 */
    {&memc_ca,		&memc_cd,	 0, STAR,     loc,    0, pv+87, 62,    0},
/* 28: scsibus* at umass*|atapiscsi*|pcscp*|qle*|arc*|softraid0|siop*|mpi*|qla*|qlw*|ahc* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+32, 62,    0},
/* 29: cd* at scsibus* target -1 lun -1 */
    {&cd_ca,		&cd_cd,		 0, STAR, loc+  4,    0, pv+79, 63,    0},
/* 30: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+  4,    0, pv+79, 63,    0},
/* 31: st* at scsibus* target -1 lun -1 */
    {&st_ca,		&st_cd,		 0, STAR, loc+  4,    0, pv+79, 63,    0},
/* 32: pci* at mpcpcibr*|ht*|ppb*|hpb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+  5,    0, pv+57, 66,    0},
/* 33: ahc* at pci* dev -1 function -1 */
    {&ahc_pci_ca,	&ahc_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 34: arc* at pci* dev -1 function -1 */
    {&arc_ca,		&arc_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 35: qlw* at pci* dev -1 function -1 */
    {&qlw_pci_ca,	&qlw_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 36: qla* at pci* dev -1 function -1 */
    {&qla_pci_ca,	&qla_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 37: qle* at pci* dev -1 function -1 */
    {&qle_ca,		&qle_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 38: mpi* at pci* dev -1 function -1 */
    {&mpi_pci_ca,	&mpi_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 39: de* at pci* dev -1 function -1 */
    {&de_ca,		&de_cd,		 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 40: pcn* at pci* dev -1 function -1 */
    {&pcn_ca,		&pcn_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 41: siop* at pci* dev -1 function -1 */
    {&siop_pci_ca,	&siop_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 42: pciide* at pci* dev -1 function -1 */
    {&pciide_pci_ca,	&pciide_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 43: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 44: rl* at pci* dev -1 function -1 */
    {&rl_pci_ca,	&rl_cd,		 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 45: rl* at cardbus* dev -1 function -1 */
    {&rl_cardbus_ca,	&rl_cd,		 0, STAR, loc+  4,    0, pv+107, 77,    0},
/* 46: re* at pci* dev -1 function -1 */
    {&re_pci_ca,	&re_cd,		 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 47: re* at cardbus* dev -1 function -1 */
    {&re_cardbus_ca,	&re_cd,		 0, STAR, loc+  4,    0, pv+107, 77,    0},
/* 48: vr* at pci* dev -1 function -1 */
    {&vr_ca,		&vr_cd,		 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 49: xl* at pci* dev -1 function -1 */
    {&xl_pci_ca,	&xl_cd,		 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 50: xl* at cardbus* dev -1 function -1 */
    {&xl_cardbus_ca,	&xl_cd,		 0, STAR, loc+  4,    0, pv+107, 77,    0},
/* 51: fxp* at pci* dev -1 function -1 */
    {&fxp_pci_ca,	&fxp_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 52: fxp* at cardbus* dev -1 function -1 */
    {&fxp_cardbus_ca,	&fxp_cd,	 0, STAR, loc+  4,    0, pv+107, 77,    0},
/* 53: em* at pci* dev -1 function -1 */
    {&em_ca,		&em_cd,		 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 54: xge* at pci* dev -1 function -1 */
    {&xge_ca,		&xge_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 55: dc* at pci* dev -1 function -1 */
    {&dc_pci_ca,	&dc_cd,		 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 56: dc* at cardbus* dev -1 function -1 */
    {&dc_cardbus_ca,	&dc_cd,		 0, STAR, loc+  4,    0, pv+107, 77,    0},
/* 57: epic* at pci* dev -1 function -1 */
    {&epic_pci_ca,	&epic_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 58: ti* at pci* dev -1 function -1 */
    {&ti_pci_ca,	&ti_cd,		 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 59: gem* at pci* dev -1 function -1 */
    {&gem_pci_ca,	&gem_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 60: uhci* at pci* dev -1 function -1 */
    {&uhci_pci_ca,	&uhci_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 61: ohci* at pci* dev -1 function -1 */
    {&ohci_pci_ca,	&ohci_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 62: ohci* at cardbus* dev -1 function -1 */
    {&ohci_cardbus_ca,	&ohci_cd,	 0, STAR, loc+  4,    0, pv+107, 77,    0},
/* 63: ehci* at pci* dev -1 function -1 */
    {&ehci_pci_ca,	&ehci_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 64: ehci* at cardbus* dev -1 function -1 */
    {&ehci_cardbus_ca,	&ehci_cd,	 0, STAR, loc+  4,    0, pv+107, 77,    0},
/* 65: xhci* at pci* dev -1 function -1 */
    {&xhci_pci_ca,	&xhci_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 66: cbb* at pci* dev -1 function -1 */
    {&cbb_pci_ca,	&cbb_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 67: skc* at pci* dev -1 function -1 */
    {&skc_ca,		&skc_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 68: sk* at skc* */
    {&sk_ca,		&sk_cd,		 0, STAR,     loc,    0, pv+95, 79,    0},
/* 69: mskc* at pci* dev -1 function -1 */
    {&mskc_ca,		&mskc_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 70: msk* at mskc* */
    {&msk_ca,		&msk_cd,	 0, STAR,     loc,    0, pv+97, 79,    0},
/* 71: wi* at pci* dev -1 function -1 */
    {&wi_pci_ca,	&wi_cd,		 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 72: wi* at macobio0 */
    {&wi_obio_ca,	&wi_cd,		 0, STAR,     loc,    0, pv+93, 79,    0},
/* 73: wi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&wi_usb_ca,	&wi_cd,		 0, STAR, loc+  0,    0, pv+62, 80,    0},
/* 74: wi* at pcmcia* function -1 irq -1 */
    {&wi_pcmcia_ca,	&wi_cd,		 0, STAR, loc+  4,    0, pv+73, 94,    0},
/* 75: an* at pci* dev -1 function -1 */
    {&an_pci_ca,	&an_cd,		 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 76: pcscp* at pci* dev -1 function -1 */
    {&pcscp_ca,		&pcscp_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 77: bge* at pci* dev -1 function -1 */
    {&bge_ca,		&bge_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 78: vge* at pci* dev -1 function -1 */
    {&vge_ca,		&vge_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 79: stge* at pci* dev -1 function -1 */
    {&stge_ca,		&stge_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 80: ath* at pci* dev -1 function -1 */
    {&ath_pci_ca,	&ath_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 81: ath* at cardbus* dev -1 function -1 */
    {&ath_cardbus_ca,	&ath_cd,	 0, STAR, loc+  4,    0, pv+107, 77,    0},
/* 82: ral* at pci* dev -1 function -1 */
    {&ral_pci_ca,	&ral_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 83: ral* at cardbus* dev -1 function -1 */
    {&ral_cardbus_ca,	&ral_cd,	 0, STAR, loc+  4,    0, pv+107, 77,    0},
/* 84: mpcpcibr* at mainbus0 */
    {&mpcpcibr_ca,	&mpcpcibr_cd,	 0, STAR,     loc,    0, pv+87, 62,    0},
/* 85: ht* at mainbus0 */
    {&ht_ca,		&ht_cd,		 0, STAR,     loc,    0, pv+87, 62,    0},
/* 86: smu* at mainbus0 */
    {&smu_ca,		&smu_cd,	 0, STAR,     loc,    0, pv+87, 62,    0},
/* 87: wsdisplay* at vgafb0 console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+  6,    0, pv+105, 97,    0},
/* 88: wskbd* at akbd*|ukbd* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+  7,    0, pv+68, 101,    0},
/* 89: hpb* at pci* dev -1 function -1 */
    {&hpb_ca,		&hpb_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 90: pchb* at pci* dev -1 function -1 */
    {&pchb_ca,		&pchb_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 91: atapiscsi* at wdc*|wdc*|wdc*|wdc*|pciide* channel -1 */
    {&atapiscsi_ca,	&atapiscsi_cd,	 0, STAR, loc+  5,    0, pv+51, 107,    0},
/* 92: wd* at wdc*|wdc*|wdc*|wdc*|pciide* channel -1 drive -1 */
    {&wd_ca,		&wd_cd,		 0, STAR, loc+  4,    0, pv+51, 107,    0},
/* 93: macobio0 at pci* dev -1 function -1 */
    {&macobio_ca,	&macobio_cd,	 0, NORM, loc+  4,    0, pv+71, 74,    0},
/* 94: kauaiata* at pci* dev -1 function -1 */
    {&kauaiata_ca,	&kauaiata_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
/* 95: mc* at macobio0 */
    {&mc_ca,		&mc_cd,		 0, STAR,     loc,    0, pv+93, 79,    0},
/* 96: bm* at macobio0 */
    {&bm_ca,		&bm_cd,		 0, STAR,     loc,    0, pv+93, 79,    0},
/* 97: macintr0 at macobio0 */
    {&macintr_ca,	&macintr_cd,	 0, NORM,     loc,    0, pv+93, 79,    0},
/* 98: openpic* at memc* */
    {&openpic_ca,	&openpic_cd,	 0, STAR,     loc,    0, pv+101, 116,    0},
/* 99: openpic* at macobio0 */
    {&openpic_ca,	&openpic_cd,	 0, STAR,     loc,    0, pv+93, 79,    0},
/*100: zs* at macobio0 */
    {&zs_ca,		&zs_cd,		 0, STAR,     loc,    0, pv+93, 79,    0},
/*101: zstty* at zs* channel -1 */
    {&zstty_ca,		&zstty_cd,	 0, STAR, loc+  5,    0, pv+81, 117,    0},
/*102: adb* at macobio0 */
    {&adb_ca,		&adb_cd,	 0, STAR,     loc,    0, pv+93, 79,    0},
/*103: akbd* at adb* */
    {&akbd_ca,		&akbd_cd,	 0, STAR,     loc,    0, pv+109, 118,    0},
/*104: mediabay* at macobio0 */
    {&mediabay_ca,	&mediabay_cd,	 0, STAR,     loc,    0, pv+93, 79,    0},
/*105: kiic* at memc* */
    {&kiic_memc_ca,	&kiic_cd,	 0, STAR,     loc,    0, pv+101, 116,    0},
/*106: macgpio* at macobio0 */
    {&macgpio_ca,	&macgpio_cd,	 0, STAR,     loc,    0, pv+93, 79,    0},
/*107: macgpio* at macgpio*|macgpio* */
    {&macgpio_gpio_ca,	&macgpio_cd,	 0, STAR,     loc,    0, pv+65, 118,    0},
/*108: wdc* at kauaiata* */
    {&wdc_obio_ca,	&wdc_cd,	 0, STAR,     loc,    0, pv+89, 118,    0},
/*109: wdc* at mediabay* */
    {&wdc_obio_ca,	&wdc_cd,	 0, STAR,     loc,    0, pv+91, 118,    0},
/*110: wdc* at macobio0 */
    {&wdc_obio_ca,	&wdc_cd,	 0, STAR,     loc,    0, pv+93, 79,    0},
/*111: wdc* at pcmcia* function -1 irq -1 */
    {&wdc_pcmcia_ca,	&wdc_cd,	 0, STAR, loc+  4,    0, pv+73, 94,    0},
/*112: vgafb0 at pci* dev -1 function -1 */
    {&vgafb_ca,		&vgafb_cd,	 0, NORM, loc+  4,    0, pv+71, 74,    0},
/*113: cardslot* at cbb* slot -1 */
    {&cardslot_ca,	&cardslot_cd,	 0, STAR, loc+  5,    0, pv+83, 119,    0},
/*114: cardbus* at cardslot* slot -1 */
    {&cardbus_ca,	&cardbus_cd,	 0, STAR, loc+  5,    0, pv+75, 121,    0},
/*115: pcmcia* at cardslot* controller -1 socket -1 */
    {&pcmcia_ca,	&pcmcia_cd,	 0, STAR, loc+  4,    0, pv+75, 121,    0},
/*116: ne* at pcmcia* function -1 irq -1 */
    {&ne_pcmcia_ca,	&ne_cd,		 0, STAR, loc+  4,    0, pv+73, 94,    0},
/*117: usb* at xhci*|ohci*|ohci*|uhci*|ehci*|ehci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+44, 122,    0},
/*118: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+103, 122,    0},
/*119: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+  0,    0, pv+62, 80,    0},
/*120: uhidev* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhidev_ca,	&uhidev_cd,	 0, STAR, loc+  0,    0, pv+62, 80,    0},
/*121: uhid* at uhidev* reportid -1 */
    {&uhid_ca,		&uhid_cd,	 0, STAR, loc+  5,    0, pv+85, 123,    0},
/*122: ukbd* at uhidev* reportid -1 */
    {&ukbd_ca,		&ukbd_cd,	 0, STAR, loc+  5,    0, pv+85, 123,    0},
/*123: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+  0,    0, pv+62, 80,    0},
/*124: aue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&aue_ca,		&aue_cd,	 0, STAR, loc+  0,    0, pv+62, 80,    0},
/*125: axe* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axe_ca,		&axe_cd,	 0, STAR, loc+  0,    0, pv+62, 80,    0},
/*126: axen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axen_ca,		&axen_cd,	 0, STAR, loc+  0,    0, pv+62, 80,    0},
/*127: smsc* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&smsc_ca,		&smsc_cd,	 0, STAR, loc+  0,    0, pv+62, 80,    0},
/*128: cue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cue_ca,		&cue_cd,	 0, STAR, loc+  0,    0, pv+62, 80,    0},
/*129: kue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&kue_ca,		&kue_cd,	 0, STAR, loc+  0,    0, pv+62, 80,    0},
/*130: cdce* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cdce_ca,		&cdce_cd,	 0, STAR, loc+  0,    0, pv+62, 80,    0},
/*131: mos* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mos_ca,		&mos_cd,	 0, STAR, loc+  0,    0, pv+62, 80,    0},
/*132: udav* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udav_ca,		&udav_cd,	 0, STAR, loc+  0,    0, pv+62, 80,    0},
/*133: upl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upl_ca,		&upl_cd,	 0, STAR, loc+  0,    0, pv+62, 80,    0},
/*134: ugl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugl_ca,		&ugl_cd,	 0, STAR, loc+  0,    0, pv+62, 80,    0},
/*135: url* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&url_ca,		&url_cd,	 0, STAR, loc+  0,    0, pv+62, 80,    0},
/*136: ure* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ure_ca,		&ure_cd,	 0, STAR, loc+  0,    0, pv+62, 80,    0},
/*137: ural* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ural_ca,		&ural_cd,	 0, STAR, loc+  0,    0, pv+62, 80,    0},
/*138: rum* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rum_ca,		&rum_cd,	 0, STAR, loc+  0,    0, pv+62, 80,    0},
/*139: zyd* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&zyd_ca,		&zyd_cd,	 0, STAR, loc+  0,    0, pv+62, 80,    0},
/*140: urtwn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtwn_ca,		&urtwn_cd,	 0, STAR, loc+  0,    0, pv+62, 80,    0},
/*141: bwfm* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&bwfm_usb_ca,	&bwfm_cd,	 0, STAR, loc+  0,    0, pv+62, 80,    0},
/*142: iic* at kiic* */
    {&iic_ca,		&iic_cd,	 0, STAR,     loc,    0, pv+99, 124,    0},
/*143: fcu* at iic* addr -1 size -1 */
    {&fcu_ca,		&fcu_cd,	 0, STAR, loc+  4,    0, pv+77, 125,    0},
/*144: hme* at pci* dev -1 function -1 */
    {&hme_pci_ca,	&hme_cd,	 0, STAR, loc+  4,    0, pv+71, 74,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 0 /* softraid0 */,
	25 /* mainbus0 */,
	-1
};

int cfroots_size = 3;

/* pseudo-devices */
extern void loopattach(int);
extern void bpfilterattach(int);
extern void vlanattach(int);
extern void rdattach(int);
extern void wsmuxattach(int);
extern void bioattach(int);

char *pdevnames[] = {
	"loop",
	"bpfilter",
	"vlan",
	"rd",
	"wsmux",
	"bio",
};

int pdevnames_size = 6;

struct pdevinit pdevinit[] = {
	{ loopattach, 1 },
	{ bpfilterattach, 1 },
	{ vlanattach, 1 },
	{ rdattach, 1 },
	{ wsmuxattach, 2 },
	{ bioattach, 1 },
	{ NULL, 0 }
};
