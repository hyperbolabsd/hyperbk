/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/octeon/conf/BOOT"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver ahci_cd;
extern struct cfdriver nvme_cd;
extern struct cfdriver com_cd;
extern struct cfdriver ohci_cd;
extern struct cfdriver ehci_cd;
extern struct cfdriver xhci_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver octcf_cd;
extern struct cfdriver amdcf_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver sdmmc_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver clock_cd;
extern struct cfdriver iobus_cd;
extern struct cfdriver simplebus_cd;
extern struct cfdriver dwctwo_cd;
extern struct cfdriver octuctl_cd;
extern struct cfdriver octpcie_cd;
extern struct cfdriver octcib_cd;
extern struct cfdriver octcit_cd;
extern struct cfdriver octciu_cd;
extern struct cfdriver octmmc_cd;
extern struct cfdriver octsctl_cd;
extern struct cfdriver octxctl_cd;

extern struct cfattach pci_ca;
extern struct cfattach nvme_pci_ca;
extern struct cfattach ppb_ca;
extern struct cfattach sdmmc_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach umass_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach sd_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach cpu_ca;
extern struct cfattach clock_ca;
extern struct cfattach iobus_ca;
extern struct cfattach simplebus_ca;
extern struct cfattach ahci_fdt_ca;
extern struct cfattach xhci_fdt_ca;
extern struct cfattach octdwctwo_ca;
extern struct cfattach octuctl_ca;
extern struct cfattach octehci_ca;
extern struct cfattach octohci_ca;
extern struct cfattach octcf_ca;
extern struct cfattach amdcf_ca;
extern struct cfattach octuart_ca;
extern struct cfattach octpcie_ca;
extern struct cfattach octcib_ca;
extern struct cfattach octcit_ca;
extern struct cfattach octciu_ca;
extern struct cfattach octmmc_ca;
extern struct cfattach octsctl_ca;
extern struct cfattach octxctl_ca;


/* locators */
static long loc[10] = {
	-1, -1, -1, -1, -1, -1, -1, 0,
	-1, 0x38,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"bus",
	"dev",
	"function",
	"port",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"target",
	"lun",
	"early",
	"base",
	"irq",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, -1, 0, -1, 1, 2, -1,
	3, 4, 5, 6, 7, 8, -1, 3,
	4, 5, 6, 7, 8, -1, 9, 10,
	-1, 11, -1, 11, -1, 11, -1, 11,
	-1, 12, 13, -1,
};

/* size of parent vectors */
int pv_size = 39;

/* parent vectors */
short pv[39] = {
	31, 30, 14, 15, -1, 7, 3, 1, 16, -1, 18, 20, 21, 17, -1, 5,
	6, -1, 25, 2, -1, 30, -1, 0, -1, 8, -1, 13, -1, 10, -1, 19,
	-1, 29, -1, 4, -1, 31, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: pci* at octpcie*|ppb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+  5,    0, pv+18, 1,    0},
/*  1: nvme* at pci* dev -1 function -1 */
    {&nvme_pci_ca,	&nvme_cd,	 0, STAR, loc+  4,    0, pv+23, 5,    0},
/*  2: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+  4,    0, pv+23, 5,    0},
/*  3: sdmmc* at octmmc* */
    {&sdmmc_ca,		&sdmmc_cd,	 0, STAR,     loc,    0, pv+33, 7,    0},
/*  4: usb* at dwctwo0|ehci0|ohci0|xhci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+10, 7,    0},
/*  5: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+35, 7,    0},
/*  6: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+  0,    0, pv+15, 8,    0},
/*  7: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+  0,    0, pv+15, 8,    0},
/*  8: scsibus* at umass*|sdmmc*|nvme*|ahci* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+ 5, 21,    0},
/*  9: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+  4,    0, pv+25, 22,    0},
/* 10: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+ 4, 0,    0},
/* 11: cpu0 at mainbus0 */
    {&cpu_ca,		&cpu_cd,	 0, NORM,     loc,    0, pv+29, 24,    0},
/* 12: clock0 at mainbus0 */
    {&clock_ca,		&clock_cd,	 0, NORM,     loc,    0, pv+29, 24,    0},
/* 13: iobus0 at mainbus0 */
    {&iobus_ca,		&iobus_cd,	 0, NORM,     loc,    0, pv+29, 24,    0},
/* 14: simplebus* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&simplebus_ca,	&simplebus_cd,	 0, STAR, loc+  7,    0, pv+ 0, 25,    0},
/* 15: simplebus* at iobus0 base -1 irq 0 */
    {&simplebus_ca,	&simplebus_cd,	 0, STAR, loc+  6,    0, pv+27, 33,    0},
/* 16: ahci* at octsctl* early 0 */
    {&ahci_fdt_ca,	&ahci_cd,	 0, STAR, loc+  7,    0, pv+21, 27,    0},
/* 17: xhci* at octxctl* early 0 */
    {&xhci_fdt_ca,	&xhci_cd,	 0, STAR, loc+  7,    0, pv+37, 25,    0},
/* 18: dwctwo0 at iobus0 base -1 irq 0x38 */
    {&octdwctwo_ca,	&dwctwo_cd,	 0, NORM, loc+  8,    0, pv+27, 33,    0},
/* 19: octuctl* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octuctl_ca,	&octuctl_cd,	 0, STAR, loc+  7,    0, pv+ 0, 25,    0},
/* 20: ehci0 at octuctl* */
    {&octehci_ca,	&ehci_cd,	 0, NORM,     loc,    0, pv+31, 35,    0},
/* 21: ohci0 at octuctl* */
    {&octohci_ca,	&ohci_cd,	 0, NORM,     loc,    0, pv+31, 35,    0},
/* 22: octcf0 at iobus0 base -1 irq 0 */
    {&octcf_ca,		&octcf_cd,	 0, NORM, loc+  6,    0, pv+27, 33,    0},
/* 23: amdcf0 at iobus0 base -1 irq 0 */
    {&amdcf_ca,		&amdcf_cd,	 0, NORM, loc+  6,    0, pv+27, 33,    0},
/* 24: com* at simplebus*|simplebus* early 0 */
    {&octuart_ca,	&com_cd,	 0, STAR, loc+  7,    0, pv+ 2, 29,    0},
/* 25: octpcie* at iobus0 base -1 irq 0 */
    {&octpcie_ca,	&octpcie_cd,	 0, STAR, loc+  6,    0, pv+27, 33,    0},
/* 26: octcib* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octcib_ca,	&octcib_cd,	 0, STAR, loc+  7,    0, pv+ 0, 25,    0},
/* 27: octcit* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octcit_ca,	&octcit_cd,	 0, STAR, loc+  7,    0, pv+ 0, 25,    0},
/* 28: octciu* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octciu_ca,	&octciu_cd,	 0, STAR, loc+  7,    0, pv+ 0, 25,    0},
/* 29: octmmc* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octmmc_ca,	&octmmc_cd,	 0, STAR, loc+  7,    0, pv+ 0, 25,    0},
/* 30: octsctl* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octsctl_ca,	&octsctl_cd,	 0, STAR, loc+  7,    0, pv+ 0, 25,    0},
/* 31: octxctl* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octxctl_ca,	&octxctl_cd,	 0, STAR, loc+  7,    0, pv+ 0, 25,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	10 /* mainbus0 */,
	-1
};

int cfroots_size = 2;

/* pseudo-devices */
extern void etheripattach(int);
extern void octbootattach(int);
extern void rdattach(int);
extern void wsmuxattach(int);

char *pdevnames[] = {
	"etherip",
	"octboot",
	"rd",
	"wsmux",
};

int pdevnames_size = 4;

struct pdevinit pdevinit[] = {
	{ etheripattach, 1 },
	{ octbootattach, 1 },
	{ rdattach, 1 },
	{ wsmuxattach, 2 },
	{ NULL, 0 }
};
