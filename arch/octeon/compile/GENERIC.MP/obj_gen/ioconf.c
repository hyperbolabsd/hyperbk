/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/octeon/conf/GENERIC.MP"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver video_cd;
extern struct cfdriver audio_cd;
extern struct cfdriver ahci_cd;
extern struct cfdriver nvme_cd;
extern struct cfdriver wi_cd;
extern struct cfdriver com_cd;
extern struct cfdriver athn_cd;
extern struct cfdriver ohci_cd;
extern struct cfdriver ehci_cd;
extern struct cfdriver xhci_cd;
extern struct cfdriver radio_cd;
extern struct cfdriver vscsi_cd;
extern struct cfdriver softraid_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver wd_cd;
extern struct cfdriver octcf_cd;
extern struct cfdriver amdcf_cd;
extern struct cfdriver eephy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver brgphy_cd;
extern struct cfdriver rgephy_cd;
extern struct cfdriver atphy_cd;
extern struct cfdriver brswphy_cd;
extern struct cfdriver onewire_cd;
extern struct cfdriver owid_cd;
extern struct cfdriver owsbm_cd;
extern struct cfdriver owtemp_cd;
extern struct cfdriver owctr_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver pciide_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver sdmmc_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver uaudio_cd;
extern struct cfdriver uvideo_cd;
extern struct cfdriver utvfu_cd;
extern struct cfdriver ucom_cd;
extern struct cfdriver ugen_cd;
extern struct cfdriver uhidev_cd;
extern struct cfdriver uhid_cd;
extern struct cfdriver fido_cd;
extern struct cfdriver ujoy_cd;
extern struct cfdriver ucycom_cd;
extern struct cfdriver uslhcom_cd;
extern struct cfdriver ulpt_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver uthum_cd;
extern struct cfdriver ugold_cd;
extern struct cfdriver utrh_cd;
extern struct cfdriver uoakrh_cd;
extern struct cfdriver uoaklux_cd;
extern struct cfdriver uoakv_cd;
extern struct cfdriver uonerng_cd;
extern struct cfdriver urng_cd;
extern struct cfdriver udcf_cd;
extern struct cfdriver umbg_cd;
extern struct cfdriver uvisor_cd;
extern struct cfdriver udsbr_cd;
extern struct cfdriver utwitch_cd;
extern struct cfdriver aue_cd;
extern struct cfdriver axe_cd;
extern struct cfdriver axen_cd;
extern struct cfdriver smsc_cd;
extern struct cfdriver cue_cd;
extern struct cfdriver kue_cd;
extern struct cfdriver cdce_cd;
extern struct cfdriver urndis_cd;
extern struct cfdriver mos_cd;
extern struct cfdriver udav_cd;
extern struct cfdriver upl_cd;
extern struct cfdriver ugl_cd;
extern struct cfdriver url_cd;
extern struct cfdriver ure_cd;
extern struct cfdriver umodem_cd;
extern struct cfdriver uftdi_cd;
extern struct cfdriver uplcom_cd;
extern struct cfdriver umct_cd;
extern struct cfdriver uvscom_cd;
extern struct cfdriver ubsa_cd;
extern struct cfdriver uslcom_cd;
extern struct cfdriver uark_cd;
extern struct cfdriver moscom_cd;
extern struct cfdriver umcs_cd;
extern struct cfdriver uscom_cd;
extern struct cfdriver ucrcom_cd;
extern struct cfdriver uipaq_cd;
extern struct cfdriver umsm_cd;
extern struct cfdriver uchcom_cd;
extern struct cfdriver uticom_cd;
extern struct cfdriver atu_cd;
extern struct cfdriver ural_cd;
extern struct cfdriver rum_cd;
extern struct cfdriver run_cd;
extern struct cfdriver zyd_cd;
extern struct cfdriver upgt_cd;
extern struct cfdriver urtw_cd;
extern struct cfdriver urtwn_cd;
extern struct cfdriver rsu_cd;
extern struct cfdriver otus_cd;
extern struct cfdriver uath_cd;
extern struct cfdriver uow_cd;
extern struct cfdriver uberry_cd;
extern struct cfdriver upd_cd;
extern struct cfdriver uhidpp_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver clock_cd;
extern struct cfdriver octcrypto_cd;
extern struct cfdriver octrtc_cd;
extern struct cfdriver iobus_cd;
extern struct cfdriver simplebus_cd;
extern struct cfdriver dwctwo_cd;
extern struct cfdriver octuctl_cd;
extern struct cfdriver octrng_cd;
extern struct cfdriver octsmi_cd;
extern struct cfdriver octgmx_cd;
extern struct cfdriver cnmac_cd;
extern struct cfdriver pcibus_cd;
extern struct cfdriver octpcie_cd;
extern struct cfdriver octcib_cd;
extern struct cfdriver octcit_cd;
extern struct cfdriver octciu_cd;
extern struct cfdriver octgpio_cd;
extern struct cfdriver octmmc_cd;
extern struct cfdriver octpip_cd;
extern struct cfdriver octsctl_cd;
extern struct cfdriver octxctl_cd;
extern struct cfdriver ogxnexus_cd;
extern struct cfdriver ogx_cd;

extern struct cfattach video_ca;
extern struct cfattach audio_ca;
extern struct cfattach radio_ca;
extern struct cfattach vscsi_ca;
extern struct cfattach softraid_ca;
extern struct cfattach wd_ca;
extern struct cfattach eephy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach brgphy_ca;
extern struct cfattach rgephy_ca;
extern struct cfattach atphy_ca;
extern struct cfattach brswphy_ca;
extern struct cfattach onewire_ca;
extern struct cfattach owid_ca;
extern struct cfattach owsbm_ca;
extern struct cfattach owtemp_ca;
extern struct cfattach owctr_ca;
extern struct cfattach pci_ca;
extern struct cfattach nvme_pci_ca;
extern struct cfattach pciide_pci_ca;
extern struct cfattach ppb_ca;
extern struct cfattach sdmmc_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach uaudio_ca;
extern struct cfattach uvideo_ca;
extern struct cfattach utvfu_ca;
extern struct cfattach ucom_ca;
extern struct cfattach ugen_ca;
extern struct cfattach uhidev_ca;
extern struct cfattach uhid_ca;
extern struct cfattach fido_ca;
extern struct cfattach ujoy_ca;
extern struct cfattach ucycom_ca;
extern struct cfattach uslhcom_ca;
extern struct cfattach ulpt_ca;
extern struct cfattach umass_ca;
extern struct cfattach uthum_ca;
extern struct cfattach ugold_ca;
extern struct cfattach utrh_ca;
extern struct cfattach uoakrh_ca;
extern struct cfattach uoaklux_ca;
extern struct cfattach uoakv_ca;
extern struct cfattach uonerng_ca;
extern struct cfattach urng_ca;
extern struct cfattach udcf_ca;
extern struct cfattach umbg_ca;
extern struct cfattach uvisor_ca;
extern struct cfattach udsbr_ca;
extern struct cfattach utwitch_ca;
extern struct cfattach aue_ca;
extern struct cfattach axe_ca;
extern struct cfattach axen_ca;
extern struct cfattach smsc_ca;
extern struct cfattach cue_ca;
extern struct cfattach kue_ca;
extern struct cfattach cdce_ca;
extern struct cfattach urndis_ca;
extern struct cfattach mos_ca;
extern struct cfattach udav_ca;
extern struct cfattach upl_ca;
extern struct cfattach ugl_ca;
extern struct cfattach url_ca;
extern struct cfattach ure_ca;
extern struct cfattach umodem_ca;
extern struct cfattach uftdi_ca;
extern struct cfattach uplcom_ca;
extern struct cfattach umct_ca;
extern struct cfattach uvscom_ca;
extern struct cfattach ubsa_ca;
extern struct cfattach uslcom_ca;
extern struct cfattach uark_ca;
extern struct cfattach moscom_ca;
extern struct cfattach umcs_ca;
extern struct cfattach uscom_ca;
extern struct cfattach ucrcom_ca;
extern struct cfattach uipaq_ca;
extern struct cfattach umsm_ca;
extern struct cfattach uchcom_ca;
extern struct cfattach uticom_ca;
extern struct cfattach wi_usb_ca;
extern struct cfattach atu_ca;
extern struct cfattach ural_ca;
extern struct cfattach rum_ca;
extern struct cfattach run_ca;
extern struct cfattach zyd_ca;
extern struct cfattach upgt_ca;
extern struct cfattach urtw_ca;
extern struct cfattach urtwn_ca;
extern struct cfattach rsu_ca;
extern struct cfattach otus_ca;
extern struct cfattach uath_ca;
extern struct cfattach athn_usb_ca;
extern struct cfattach uow_ca;
extern struct cfattach uberry_ca;
extern struct cfattach upd_ca;
extern struct cfattach uhidpp_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach sd_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach cpu_ca;
extern struct cfattach clock_ca;
extern struct cfattach octcrypto_ca;
extern struct cfattach octrtc_ca;
extern struct cfattach iobus_ca;
extern struct cfattach simplebus_ca;
extern struct cfattach ahci_fdt_ca;
extern struct cfattach xhci_fdt_ca;
extern struct cfattach octdwctwo_ca;
extern struct cfattach octuctl_ca;
extern struct cfattach octehci_ca;
extern struct cfattach octohci_ca;
extern struct cfattach octcf_ca;
extern struct cfattach amdcf_ca;
extern struct cfattach octrng_ca;
extern struct cfattach octsmi_ca;
extern struct cfattach octgmx_ca;
extern struct cfattach cnmac_ca;
extern struct cfattach octuart_ca;
extern struct cfattach pcibus_ca;
extern struct cfattach octpcie_ca;
extern struct cfattach octcib_ca;
extern struct cfattach octcit_ca;
extern struct cfattach octciu_ca;
extern struct cfattach octgpio_ca;
extern struct cfattach octmmc_ca;
extern struct cfattach octpip_ca;
extern struct cfattach octsctl_ca;
extern struct cfattach octxctl_ca;
extern struct cfattach ogxnexus_ca;
extern struct cfattach ogx_ca;


/* locators */
static long loc[10] = {
	-1, -1, -1, -1, -1, -1, -1, 0,
	-1, 0x38,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"channel",
	"drive",
	"phy",
	"bus",
	"dev",
	"function",
	"port",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"portno",
	"reportid",
	"target",
	"lun",
	"early",
	"base",
	"irq",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, 1, -1, 2, -1, 2, -1,
	2, -1, 2, -1, 2, -1, 2, -1,
	2, -1, 2, -1, 2, -1, 2, -1,
	3, -1, 3, -1, 3, -1, 4, 5,
	-1, 6, 7, 8, 9, 10, 11, -1,
	6, 7, 8, 9, 10, 11, -1, 12,
	-1, 12, -1, 12, -1, 12, -1, 12,
	-1, 12, -1, 12, -1, 12, -1, 12,
	-1, 12, -1, 12, -1, 12, -1, 12,
	-1, 12, -1, 12, -1, 12, -1, 12,
	-1, 12, -1, 12, -1, 13, -1, 14,
	15, -1, 16, -1, 16, -1, 16, -1,
	16, -1, 17, 18, -1,
};

/* size of parent vectors */
int pv_size = 95;

/* parent vectors */
short pv[95] = {
	65, 48, 69, 70, 66, 67, 68, 71, 75, 76, 72, 73, 74, 77, 78, 79,
	80, 34, 35, -1, 133, 120, 64, 63, 60, 59, 54, 53, 52, 51, -1, 4,
	3, 37, 21, 18, 109, -1, 131, 130, 107, 108, -1, 111, 113, 114, 110, -1,
	123, 122, 20, -1, 23, 24, -1, 25, 27, -1, 26, 27, -1, 30, -1, 17,
	-1, 130, -1, 131, -1, 98, -1, 19, -1, 106, -1, 129, -1, 132, -1, 119,
	-1, 49, -1, 94, -1, 12, -1, 128, -1, 22, -1, 100, -1, 112, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: video* at uvideo*|utvfu* */
    {&video_ca,		&video_cd,	 0, STAR,     loc,    0, pv+58, 0,    0},
/*  1: audio* at uaudio*|utvfu* */
    {&audio_ca,		&audio_cd,	 0, STAR,     loc,    0, pv+55, 0,    0},
/*  2: radio* at udsbr* */
    {&radio_ca,		&radio_cd,	 0, STAR,     loc,    0, pv+81, 0,    0},
/*  3: vscsi0 at root */
    {&vscsi_ca,		&vscsi_cd,	 0, NORM,     loc,    0, pv+19, 0,    0},
/*  4: softraid0 at root */
    {&softraid_ca,	&softraid_cd,	 0, NORM,     loc,    0, pv+19, 0,    0},
/*  5: wd* at pciide* channel -1 drive -1 */
    {&wd_ca,		&wd_cd,		 0, STAR, loc+  4,    0, pv+71, 1,    0},
/*  6: eephy* at ogx*|cnmac*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue* phy -1 */
    {&eephy_ca,		&eephy_cd,	 0, STAR, loc+  5,    0, pv+20, 4,    0},
/*  7: ukphy* at ogx*|cnmac*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+  5,    0, pv+20, 4,    0},
/*  8: brgphy* at ogx*|cnmac*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue* phy -1 */
    {&brgphy_ca,	&brgphy_cd,	 0, STAR, loc+  5,    0, pv+20, 4,    0},
/*  9: rgephy* at ogx*|cnmac*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue* phy -1 */
    {&rgephy_ca,	&rgephy_cd,	 0, STAR, loc+  5,    0, pv+20, 4,    0},
/* 10: atphy* at ogx*|cnmac*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue* phy -1 */
    {&atphy_ca,		&atphy_cd,	 0, STAR, loc+  5,    0, pv+20, 4,    0},
/* 11: brswphy* at ogx*|cnmac*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue* phy -1 */
    {&brswphy_ca,	&brswphy_cd,	 0, STAR, loc+  5,    0, pv+20, 4,    0},
/* 12: onewire* at uow* */
    {&onewire_ca,	&onewire_cd,	 0, STAR,     loc,    0, pv+83, 23,    0},
/* 13: owid* at onewire* */
    {&owid_ca,		&owid_cd,	 0, STAR,     loc,    0, pv+85, 23,    0},
/* 14: owsbm* at onewire* */
    {&owsbm_ca,		&owsbm_cd,	 0, STAR,     loc,    0, pv+85, 23,    0},
/* 15: owtemp* at onewire* */
    {&owtemp_ca,	&owtemp_cd,	 0, STAR,     loc,    0, pv+85, 23,    0},
/* 16: owctr* at onewire* */
    {&owctr_ca,		&owctr_cd,	 0, STAR,     loc,    0, pv+85, 23,    0},
/* 17: pci* at octpcie*|pcibus*|ppb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+  5,    0, pv+48, 24,    0},
/* 18: nvme* at pci* dev -1 function -1 */
    {&nvme_pci_ca,	&nvme_cd,	 0, STAR, loc+  4,    0, pv+63, 30,    0},
/* 19: pciide* at pci* dev -1 function -1 */
    {&pciide_pci_ca,	&pciide_cd,	 0, STAR, loc+  4,    0, pv+63, 30,    0},
/* 20: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+  4,    0, pv+63, 30,    0},
/* 21: sdmmc* at octmmc* */
    {&sdmmc_ca,		&sdmmc_cd,	 0, STAR,     loc,    0, pv+87, 32,    0},
/* 22: usb* at dwctwo0|ehci0|ohci0|xhci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+43, 32,    0},
/* 23: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+89, 32,    0},
/* 24: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 25: uaudio* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uaudio_ca,	&uaudio_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 26: uvideo* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvideo_ca,	&uvideo_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 27: utvfu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&utvfu_ca,		&utvfu_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 28: ucom* at umodem*|uvisor*|uvscom*|ubsa*|uftdi*|uplcom*|umct*|uslcom*|uscom*|ucrcom*|uark*|moscom*|umcs*|uipaq*|umsm*|uchcom*|uticom*|ucycom*|uslhcom* portno -1 */
    {&ucom_ca,		&ucom_cd,	 0, STAR, loc+  5,    0, pv+ 0, 47,    0},
/* 29: ugen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugen_ca,		&ugen_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 30: uhidev* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhidev_ca,	&uhidev_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 31: uhid* at uhidev* reportid -1 */
    {&uhid_ca,		&uhid_cd,	 0, STAR, loc+  5,    0, pv+61, 85,    0},
/* 32: fido* at uhidev* reportid -1 */
    {&fido_ca,		&fido_cd,	 0, STAR, loc+  5,    0, pv+61, 85,    0},
/* 33: ujoy* at uhidev* reportid -1 */
    {&ujoy_ca,		&ujoy_cd,	 0, STAR, loc+  5,    0, pv+61, 85,    0},
/* 34: ucycom* at uhidev* reportid -1 */
    {&ucycom_ca,	&ucycom_cd,	 0, STAR, loc+  5,    0, pv+61, 85,    0},
/* 35: uslhcom* at uhidev* reportid -1 */
    {&uslhcom_ca,	&uslhcom_cd,	 0, STAR, loc+  5,    0, pv+61, 85,    0},
/* 36: ulpt* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ulpt_ca,		&ulpt_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 37: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 38: uthum* at uhidev* reportid -1 */
    {&uthum_ca,		&uthum_cd,	 0, STAR, loc+  5,    0, pv+61, 85,    0},
/* 39: ugold* at uhidev* reportid -1 */
    {&ugold_ca,		&ugold_cd,	 0, STAR, loc+  5,    0, pv+61, 85,    0},
/* 40: utrh* at uhidev* reportid -1 */
    {&utrh_ca,		&utrh_cd,	 0, STAR, loc+  5,    0, pv+61, 85,    0},
/* 41: uoakrh* at uhidev* reportid -1 */
    {&uoakrh_ca,	&uoakrh_cd,	 0, STAR, loc+  5,    0, pv+61, 85,    0},
/* 42: uoaklux* at uhidev* reportid -1 */
    {&uoaklux_ca,	&uoaklux_cd,	 0, STAR, loc+  5,    0, pv+61, 85,    0},
/* 43: uoakv* at uhidev* reportid -1 */
    {&uoakv_ca,		&uoakv_cd,	 0, STAR, loc+  5,    0, pv+61, 85,    0},
/* 44: uonerng* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uonerng_ca,	&uonerng_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 45: urng* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urng_ca,		&urng_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 46: udcf* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udcf_ca,		&udcf_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 47: umbg* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umbg_ca,		&umbg_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 48: uvisor* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvisor_ca,	&uvisor_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 49: udsbr* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udsbr_ca,		&udsbr_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 50: utwitch* at uhidev* reportid -1 */
    {&utwitch_ca,	&utwitch_cd,	 0, STAR, loc+  5,    0, pv+61, 85,    0},
/* 51: aue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&aue_ca,		&aue_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 52: axe* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axe_ca,		&axe_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 53: axen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axen_ca,		&axen_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 54: smsc* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&smsc_ca,		&smsc_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 55: cue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cue_ca,		&cue_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 56: kue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&kue_ca,		&kue_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 57: cdce* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cdce_ca,		&cdce_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 58: urndis* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urndis_ca,	&urndis_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 59: mos* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mos_ca,		&mos_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 60: udav* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udav_ca,		&udav_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 61: upl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upl_ca,		&upl_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 62: ugl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugl_ca,		&ugl_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 63: url* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&url_ca,		&url_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 64: ure* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ure_ca,		&ure_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 65: umodem* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umodem_ca,	&umodem_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 66: uftdi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uftdi_ca,		&uftdi_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 67: uplcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uplcom_ca,	&uplcom_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 68: umct* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umct_ca,		&umct_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 69: uvscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvscom_ca,	&uvscom_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 70: ubsa* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ubsa_ca,		&ubsa_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 71: uslcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uslcom_ca,	&uslcom_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 72: uark* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uark_ca,		&uark_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 73: moscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&moscom_ca,	&moscom_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 74: umcs* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umcs_ca,		&umcs_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 75: uscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uscom_ca,		&uscom_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 76: ucrcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ucrcom_ca,	&ucrcom_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 77: uipaq* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uipaq_ca,		&uipaq_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 78: umsm* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umsm_ca,		&umsm_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 79: uchcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uchcom_ca,	&uchcom_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 80: uticom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uticom_ca,	&uticom_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 81: wi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&wi_usb_ca,	&wi_cd,		 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 82: atu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&atu_ca,		&atu_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 83: ural* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ural_ca,		&ural_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 84: rum* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rum_ca,		&rum_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 85: run* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&run_ca,		&run_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 86: zyd* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&zyd_ca,		&zyd_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 87: upgt* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upgt_ca,		&upgt_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 88: urtw* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtw_ca,		&urtw_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 89: urtwn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtwn_ca,		&urtwn_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 90: rsu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rsu_ca,		&rsu_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 91: otus* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&otus_ca,		&otus_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 92: uath* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uath_ca,		&uath_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 93: athn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&athn_usb_ca,	&athn_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 94: uow* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uow_ca,		&uow_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 95: uberry* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uberry_ca,	&uberry_cd,	 0, STAR, loc+  0,    0, pv+52, 33,    0},
/* 96: upd* at uhidev* reportid -1 */
    {&upd_ca,		&upd_cd,	 0, STAR, loc+  5,    0, pv+61, 85,    0},
/* 97: uhidpp* at uhidev* reportid -1 */
    {&uhidpp_ca,	&uhidpp_cd,	 0, STAR, loc+  5,    0, pv+61, 85,    0},
/* 98: scsibus* at softraid0|vscsi0|umass*|sdmmc*|nvme*|ahci* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+31, 86,    0},
/* 99: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+  4,    0, pv+69, 87,    0},
/*100: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+19, 0,    0},
/*101: cpu0 at mainbus0 */
    {&cpu_ca,		&cpu_cd,	 0, NORM,     loc,    0, pv+91, 89,    0},
/*102: cpu* at mainbus0 */
    {&cpu_ca,		&cpu_cd,	 1, STAR,     loc,    0, pv+91, 89,    1},
/*103: clock0 at mainbus0 */
    {&clock_ca,		&clock_cd,	 0, NORM,     loc,    0, pv+91, 89,    0},
/*104: octcrypto0 at mainbus0 */
    {&octcrypto_ca,	&octcrypto_cd,	 0, NORM,     loc,    0, pv+91, 89,    0},
/*105: octrtc0 at mainbus0 */
    {&octrtc_ca,	&octrtc_cd,	 0, NORM,     loc,    0, pv+91, 89,    0},
/*106: iobus0 at mainbus0 */
    {&iobus_ca,		&iobus_cd,	 0, NORM,     loc,    0, pv+91, 89,    0},
/*107: simplebus* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&simplebus_ca,	&simplebus_cd,	 0, STAR, loc+  7,    0, pv+38, 90,    0},
/*108: simplebus* at iobus0 base -1 irq 0 */
    {&simplebus_ca,	&simplebus_cd,	 0, STAR, loc+  6,    0, pv+73, 98,    0},
/*109: ahci* at octsctl* early 0 */
    {&ahci_fdt_ca,	&ahci_cd,	 0, STAR, loc+  7,    0, pv+65, 92,    0},
/*110: xhci* at octxctl* early 0 */
    {&xhci_fdt_ca,	&xhci_cd,	 0, STAR, loc+  7,    0, pv+67, 90,    0},
/*111: dwctwo0 at iobus0 base -1 irq 0x38 */
    {&octdwctwo_ca,	&dwctwo_cd,	 0, NORM, loc+  8,    0, pv+73, 98,    0},
/*112: octuctl* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octuctl_ca,	&octuctl_cd,	 0, STAR, loc+  7,    0, pv+38, 90,    0},
/*113: ehci0 at octuctl* */
    {&octehci_ca,	&ehci_cd,	 0, NORM,     loc,    0, pv+93, 100,    0},
/*114: ohci0 at octuctl* */
    {&octohci_ca,	&ohci_cd,	 0, NORM,     loc,    0, pv+93, 100,    0},
/*115: octcf0 at iobus0 base -1 irq 0 */
    {&octcf_ca,		&octcf_cd,	 0, NORM, loc+  6,    0, pv+73, 98,    0},
/*116: amdcf0 at iobus0 base -1 irq 0 */
    {&amdcf_ca,		&amdcf_cd,	 0, NORM, loc+  6,    0, pv+73, 98,    0},
/*117: octrng0 at iobus0 base -1 irq 0 */
    {&octrng_ca,	&octrng_cd,	 0, NORM, loc+  6,    0, pv+73, 98,    0},
/*118: octsmi* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octsmi_ca,	&octsmi_cd,	 0, STAR, loc+  7,    0, pv+38, 90,    0},
/*119: octgmx* at octpip* */
    {&octgmx_ca,	&octgmx_cd,	 0, STAR,     loc,    0, pv+75, 100,    0},
/*120: cnmac* at octgmx* */
    {&cnmac_ca,		&cnmac_cd,	 0, STAR,     loc,    0, pv+79, 100,    0},
/*121: com* at simplebus*|simplebus* early 0 */
    {&octuart_ca,	&com_cd,	 0, STAR, loc+  7,    0, pv+40, 94,    0},
/*122: pcibus* at iobus0 base -1 irq 0 */
    {&pcibus_ca,	&pcibus_cd,	 0, STAR, loc+  6,    0, pv+73, 98,    0},
/*123: octpcie* at iobus0 base -1 irq 0 */
    {&octpcie_ca,	&octpcie_cd,	 0, STAR, loc+  6,    0, pv+73, 98,    0},
/*124: octcib* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octcib_ca,	&octcib_cd,	 0, STAR, loc+  7,    0, pv+38, 90,    0},
/*125: octcit* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octcit_ca,	&octcit_cd,	 0, STAR, loc+  7,    0, pv+38, 90,    0},
/*126: octciu* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octciu_ca,	&octciu_cd,	 0, STAR, loc+  7,    0, pv+38, 90,    0},
/*127: octgpio* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octgpio_ca,	&octgpio_cd,	 0, STAR, loc+  7,    0, pv+38, 90,    0},
/*128: octmmc* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octmmc_ca,	&octmmc_cd,	 0, STAR, loc+  7,    0, pv+38, 90,    0},
/*129: octpip* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octpip_ca,	&octpip_cd,	 0, STAR, loc+  7,    0, pv+38, 90,    0},
/*130: octsctl* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octsctl_ca,	&octsctl_cd,	 0, STAR, loc+  7,    0, pv+38, 90,    0},
/*131: octxctl* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octxctl_ca,	&octxctl_cd,	 0, STAR, loc+  7,    0, pv+38, 90,    0},
/*132: ogxnexus* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&ogxnexus_ca,	&ogxnexus_cd,	 0, STAR, loc+  7,    0, pv+38, 90,    0},
/*133: ogx* at ogxnexus* */
    {&ogx_ca,		&ogx_cd,	 0, STAR,     loc,    0, pv+77, 100,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 3 /* vscsi0 */,
	 4 /* softraid0 */,
	100 /* mainbus0 */,
	-1
};

int cfroots_size = 4;

/* pseudo-devices */
extern void pfattach(int);
extern void pflogattach(int);
extern void pfsyncattach(int);
extern void pflowattach(int);
extern void encattach(int);
extern void ptyattach(int);
extern void nmeaattach(int);
extern void mstsattach(int);
extern void endrunattach(int);
extern void vndattach(int);
extern void ksymsattach(int);
extern void bpfilterattach(int);
extern void bridgeattach(int);
extern void vebattach(int);
extern void carpattach(int);
extern void etheripattach(int);
extern void gifattach(int);
extern void greattach(int);
extern void loopattach(int);
extern void mpeattach(int);
extern void mpwattach(int);
extern void mpipattach(int);
extern void bpeattach(int);
extern void pairattach(int);
extern void pppattach(int);
extern void pppoeattach(int);
extern void pppxattach(int);
extern void spppattach(int);
extern void trunkattach(int);
extern void aggrattach(int);
extern void tpmrattach(int);
extern void tunattach(int);
extern void vetherattach(int);
extern void vxlanattach(int);
extern void vlanattach(int);
extern void switchattach(int);
extern void wgattach(int);
extern void bioattach(int);
extern void fuseattach(int);
extern void openpromattach(int);

char *pdevnames[] = {
	"pf",
	"pflog",
	"pfsync",
	"pflow",
	"enc",
	"pty",
	"nmea",
	"msts",
	"endrun",
	"vnd",
	"ksyms",
	"bpfilter",
	"bridge",
	"veb",
	"carp",
	"etherip",
	"gif",
	"gre",
	"loop",
	"mpe",
	"mpw",
	"mpip",
	"bpe",
	"pair",
	"ppp",
	"pppoe",
	"pppx",
	"sppp",
	"trunk",
	"aggr",
	"tpmr",
	"tun",
	"vether",
	"vxlan",
	"vlan",
	"switch",
	"wg",
	"bio",
	"fuse",
	"openprom",
};

int pdevnames_size = 40;

struct pdevinit pdevinit[] = {
	{ pfattach, 1 },
	{ pflogattach, 1 },
	{ pfsyncattach, 1 },
	{ pflowattach, 1 },
	{ encattach, 1 },
	{ ptyattach, 16 },
	{ nmeaattach, 1 },
	{ mstsattach, 1 },
	{ endrunattach, 1 },
	{ vndattach, 4 },
	{ ksymsattach, 1 },
	{ bpfilterattach, 1 },
	{ bridgeattach, 1 },
	{ vebattach, 1 },
	{ carpattach, 1 },
	{ etheripattach, 1 },
	{ gifattach, 1 },
	{ greattach, 1 },
	{ loopattach, 1 },
	{ mpeattach, 1 },
	{ mpwattach, 1 },
	{ mpipattach, 1 },
	{ bpeattach, 1 },
	{ pairattach, 1 },
	{ pppattach, 1 },
	{ pppoeattach, 1 },
	{ pppxattach, 1 },
	{ spppattach, 1 },
	{ trunkattach, 1 },
	{ aggrattach, 1 },
	{ tpmrattach, 1 },
	{ tunattach, 1 },
	{ vetherattach, 1 },
	{ vxlanattach, 1 },
	{ vlanattach, 1 },
	{ switchattach, 1 },
	{ wgattach, 1 },
	{ bioattach, 1 },
	{ fuseattach, 1 },
	{ openpromattach, 1 },
	{ NULL, 0 }
};
