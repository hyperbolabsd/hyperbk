/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/octeon/conf/RAMDISK"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver ahci_cd;
extern struct cfdriver nvme_cd;
extern struct cfdriver wi_cd;
extern struct cfdriver com_cd;
extern struct cfdriver athn_cd;
extern struct cfdriver ohci_cd;
extern struct cfdriver ehci_cd;
extern struct cfdriver xhci_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver wd_cd;
extern struct cfdriver octcf_cd;
extern struct cfdriver amdcf_cd;
extern struct cfdriver eephy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver brgphy_cd;
extern struct cfdriver rgephy_cd;
extern struct cfdriver atphy_cd;
extern struct cfdriver brswphy_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver pciide_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver sdmmc_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver aue_cd;
extern struct cfdriver axe_cd;
extern struct cfdriver axen_cd;
extern struct cfdriver smsc_cd;
extern struct cfdriver cue_cd;
extern struct cfdriver kue_cd;
extern struct cfdriver cdce_cd;
extern struct cfdriver mos_cd;
extern struct cfdriver udav_cd;
extern struct cfdriver upl_cd;
extern struct cfdriver ugl_cd;
extern struct cfdriver url_cd;
extern struct cfdriver ure_cd;
extern struct cfdriver atu_cd;
extern struct cfdriver ural_cd;
extern struct cfdriver rum_cd;
extern struct cfdriver run_cd;
extern struct cfdriver zyd_cd;
extern struct cfdriver upgt_cd;
extern struct cfdriver urtw_cd;
extern struct cfdriver urtwn_cd;
extern struct cfdriver rsu_cd;
extern struct cfdriver otus_cd;
extern struct cfdriver uath_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver clock_cd;
extern struct cfdriver octrtc_cd;
extern struct cfdriver iobus_cd;
extern struct cfdriver simplebus_cd;
extern struct cfdriver dwctwo_cd;
extern struct cfdriver octuctl_cd;
extern struct cfdriver octsmi_cd;
extern struct cfdriver octgmx_cd;
extern struct cfdriver cnmac_cd;
extern struct cfdriver pcibus_cd;
extern struct cfdriver octpcie_cd;
extern struct cfdriver octcib_cd;
extern struct cfdriver octcit_cd;
extern struct cfdriver octciu_cd;
extern struct cfdriver octgpio_cd;
extern struct cfdriver octmmc_cd;
extern struct cfdriver octpip_cd;
extern struct cfdriver octsctl_cd;
extern struct cfdriver octxctl_cd;
extern struct cfdriver ogxnexus_cd;
extern struct cfdriver ogx_cd;

extern struct cfattach wd_ca;
extern struct cfattach eephy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach brgphy_ca;
extern struct cfattach rgephy_ca;
extern struct cfattach atphy_ca;
extern struct cfattach brswphy_ca;
extern struct cfattach pci_ca;
extern struct cfattach nvme_pci_ca;
extern struct cfattach pciide_pci_ca;
extern struct cfattach ppb_ca;
extern struct cfattach sdmmc_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach umass_ca;
extern struct cfattach aue_ca;
extern struct cfattach axe_ca;
extern struct cfattach axen_ca;
extern struct cfattach smsc_ca;
extern struct cfattach cue_ca;
extern struct cfattach kue_ca;
extern struct cfattach cdce_ca;
extern struct cfattach mos_ca;
extern struct cfattach udav_ca;
extern struct cfattach upl_ca;
extern struct cfattach ugl_ca;
extern struct cfattach url_ca;
extern struct cfattach ure_ca;
extern struct cfattach wi_usb_ca;
extern struct cfattach atu_ca;
extern struct cfattach ural_ca;
extern struct cfattach rum_ca;
extern struct cfattach run_ca;
extern struct cfattach zyd_ca;
extern struct cfattach upgt_ca;
extern struct cfattach urtw_ca;
extern struct cfattach urtwn_ca;
extern struct cfattach rsu_ca;
extern struct cfattach otus_ca;
extern struct cfattach uath_ca;
extern struct cfattach athn_usb_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach sd_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach cpu_ca;
extern struct cfattach clock_ca;
extern struct cfattach octrtc_ca;
extern struct cfattach iobus_ca;
extern struct cfattach simplebus_ca;
extern struct cfattach ahci_fdt_ca;
extern struct cfattach xhci_fdt_ca;
extern struct cfattach octdwctwo_ca;
extern struct cfattach octuctl_ca;
extern struct cfattach octehci_ca;
extern struct cfattach octohci_ca;
extern struct cfattach octcf_ca;
extern struct cfattach amdcf_ca;
extern struct cfattach octsmi_ca;
extern struct cfattach octgmx_ca;
extern struct cfattach cnmac_ca;
extern struct cfattach octuart_ca;
extern struct cfattach pcibus_ca;
extern struct cfattach octpcie_ca;
extern struct cfattach octcib_ca;
extern struct cfattach octcit_ca;
extern struct cfattach octciu_ca;
extern struct cfattach octgpio_ca;
extern struct cfattach octmmc_ca;
extern struct cfattach octpip_ca;
extern struct cfattach octsctl_ca;
extern struct cfattach octxctl_ca;
extern struct cfattach ogxnexus_ca;
extern struct cfattach ogx_ca;


/* locators */
static long loc[10] = {
	-1, -1, -1, -1, -1, -1, -1, 0,
	-1, 0x38,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"channel",
	"drive",
	"phy",
	"bus",
	"dev",
	"function",
	"port",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"target",
	"lun",
	"early",
	"base",
	"irq",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, 1, -1, 2, -1, 2, -1,
	2, -1, 2, -1, 2, -1, 2, -1,
	2, -1, 2, -1, 2, -1, 2, -1,
	3, -1, 3, -1, 3, -1, 4, 5,
	-1, 6, 7, 8, 9, 10, 11, -1,
	6, 7, 8, 9, 10, 11, -1, 12,
	13, -1, 14, -1, 14, -1, 14, -1,
	14, -1, 15, 16, -1,
};

/* size of parent vectors */
int pv_size = 59;

/* parent vectors */
short pv[59] = {
	74, 61, 28, 27, 24, 23, 19, 18, 17, 16, -1, 72, 71, 49, 50, -1,
	53, 55, 56, 52, -1, 15, 11, 8, 51, -1, 64, 63, 10, -1, 13, 14,
	-1, 7, -1, 48, -1, 42, -1, 9, -1, 71, -1, 72, -1, 54, -1, 44,
	-1, 69, -1, 73, -1, 12, -1, 70, -1, 60, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: wd* at pciide* channel -1 drive -1 */
    {&wd_ca,		&wd_cd,		 0, STAR, loc+  4,    0, pv+39, 1,    0},
/*  1: eephy* at ogx*|cnmac*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue* phy -1 */
    {&eephy_ca,		&eephy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 4,    0},
/*  2: ukphy* at ogx*|cnmac*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 4,    0},
/*  3: brgphy* at ogx*|cnmac*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue* phy -1 */
    {&brgphy_ca,	&brgphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 4,    0},
/*  4: rgephy* at ogx*|cnmac*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue* phy -1 */
    {&rgephy_ca,	&rgephy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 4,    0},
/*  5: atphy* at ogx*|cnmac*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue* phy -1 */
    {&atphy_ca,		&atphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 4,    0},
/*  6: brswphy* at ogx*|cnmac*|ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue* phy -1 */
    {&brswphy_ca,	&brswphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 4,    0},
/*  7: pci* at octpcie*|pcibus*|ppb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+  5,    0, pv+26, 24,    0},
/*  8: nvme* at pci* dev -1 function -1 */
    {&nvme_pci_ca,	&nvme_cd,	 0, STAR, loc+  4,    0, pv+33, 30,    0},
/*  9: pciide* at pci* dev -1 function -1 */
    {&pciide_pci_ca,	&pciide_cd,	 0, STAR, loc+  4,    0, pv+33, 30,    0},
/* 10: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+  4,    0, pv+33, 30,    0},
/* 11: sdmmc* at octmmc* */
    {&sdmmc_ca,		&sdmmc_cd,	 0, STAR,     loc,    0, pv+49, 32,    0},
/* 12: usb* at dwctwo0|ehci0|ohci0|xhci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+16, 32,    0},
/* 13: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+53, 32,    0},
/* 14: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 15: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 16: aue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&aue_ca,		&aue_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 17: axe* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axe_ca,		&axe_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 18: axen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axen_ca,		&axen_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 19: smsc* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&smsc_ca,		&smsc_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 20: cue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cue_ca,		&cue_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 21: kue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&kue_ca,		&kue_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 22: cdce* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cdce_ca,		&cdce_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 23: mos* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mos_ca,		&mos_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 24: udav* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udav_ca,		&udav_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 25: upl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upl_ca,		&upl_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 26: ugl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugl_ca,		&ugl_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 27: url* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&url_ca,		&url_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 28: ure* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ure_ca,		&ure_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 29: wi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&wi_usb_ca,	&wi_cd,		 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 30: atu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&atu_ca,		&atu_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 31: ural* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ural_ca,		&ural_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 32: rum* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rum_ca,		&rum_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 33: run* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&run_ca,		&run_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 34: zyd* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&zyd_ca,		&zyd_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 35: upgt* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upgt_ca,		&upgt_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 36: urtw* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtw_ca,		&urtw_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 37: urtwn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtwn_ca,		&urtwn_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 38: rsu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rsu_ca,		&rsu_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 39: otus* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&otus_ca,		&otus_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 40: uath* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uath_ca,		&uath_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 41: athn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&athn_usb_ca,	&athn_cd,	 0, STAR, loc+  0,    0, pv+30, 33,    0},
/* 42: scsibus* at umass*|sdmmc*|nvme*|ahci* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+21, 46,    0},
/* 43: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+  4,    0, pv+37, 47,    0},
/* 44: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+10, 0,    0},
/* 45: cpu0 at mainbus0 */
    {&cpu_ca,		&cpu_cd,	 0, NORM,     loc,    0, pv+47, 49,    0},
/* 46: clock0 at mainbus0 */
    {&clock_ca,		&clock_cd,	 0, NORM,     loc,    0, pv+47, 49,    0},
/* 47: octrtc0 at mainbus0 */
    {&octrtc_ca,	&octrtc_cd,	 0, NORM,     loc,    0, pv+47, 49,    0},
/* 48: iobus0 at mainbus0 */
    {&iobus_ca,		&iobus_cd,	 0, NORM,     loc,    0, pv+47, 49,    0},
/* 49: simplebus* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&simplebus_ca,	&simplebus_cd,	 0, STAR, loc+  7,    0, pv+11, 50,    0},
/* 50: simplebus* at iobus0 base -1 irq 0 */
    {&simplebus_ca,	&simplebus_cd,	 0, STAR, loc+  6,    0, pv+35, 58,    0},
/* 51: ahci* at octsctl* early 0 */
    {&ahci_fdt_ca,	&ahci_cd,	 0, STAR, loc+  7,    0, pv+41, 52,    0},
/* 52: xhci* at octxctl* early 0 */
    {&xhci_fdt_ca,	&xhci_cd,	 0, STAR, loc+  7,    0, pv+43, 50,    0},
/* 53: dwctwo0 at iobus0 base -1 irq 0x38 */
    {&octdwctwo_ca,	&dwctwo_cd,	 0, NORM, loc+  8,    0, pv+35, 58,    0},
/* 54: octuctl* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octuctl_ca,	&octuctl_cd,	 0, STAR, loc+  7,    0, pv+11, 50,    0},
/* 55: ehci0 at octuctl* */
    {&octehci_ca,	&ehci_cd,	 0, NORM,     loc,    0, pv+45, 60,    0},
/* 56: ohci0 at octuctl* */
    {&octohci_ca,	&ohci_cd,	 0, NORM,     loc,    0, pv+45, 60,    0},
/* 57: octcf0 at iobus0 base -1 irq 0 */
    {&octcf_ca,		&octcf_cd,	 0, NORM, loc+  6,    0, pv+35, 58,    0},
/* 58: amdcf0 at iobus0 base -1 irq 0 */
    {&amdcf_ca,		&amdcf_cd,	 0, NORM, loc+  6,    0, pv+35, 58,    0},
/* 59: octsmi* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octsmi_ca,	&octsmi_cd,	 0, STAR, loc+  7,    0, pv+11, 50,    0},
/* 60: octgmx* at octpip* */
    {&octgmx_ca,	&octgmx_cd,	 0, STAR,     loc,    0, pv+55, 60,    0},
/* 61: cnmac* at octgmx* */
    {&cnmac_ca,		&cnmac_cd,	 0, STAR,     loc,    0, pv+57, 60,    0},
/* 62: com* at simplebus*|simplebus* early 0 */
    {&octuart_ca,	&com_cd,	 0, STAR, loc+  7,    0, pv+13, 54,    0},
/* 63: pcibus* at iobus0 base -1 irq 0 */
    {&pcibus_ca,	&pcibus_cd,	 0, STAR, loc+  6,    0, pv+35, 58,    0},
/* 64: octpcie* at iobus0 base -1 irq 0 */
    {&octpcie_ca,	&octpcie_cd,	 0, STAR, loc+  6,    0, pv+35, 58,    0},
/* 65: octcib* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octcib_ca,	&octcib_cd,	 0, STAR, loc+  7,    0, pv+11, 50,    0},
/* 66: octcit* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octcit_ca,	&octcit_cd,	 0, STAR, loc+  7,    0, pv+11, 50,    0},
/* 67: octciu* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octciu_ca,	&octciu_cd,	 0, STAR, loc+  7,    0, pv+11, 50,    0},
/* 68: octgpio* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octgpio_ca,	&octgpio_cd,	 0, STAR, loc+  7,    0, pv+11, 50,    0},
/* 69: octmmc* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octmmc_ca,	&octmmc_cd,	 0, STAR, loc+  7,    0, pv+11, 50,    0},
/* 70: octpip* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octpip_ca,	&octpip_cd,	 0, STAR, loc+  7,    0, pv+11, 50,    0},
/* 71: octsctl* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octsctl_ca,	&octsctl_cd,	 0, STAR, loc+  7,    0, pv+11, 50,    0},
/* 72: octxctl* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&octxctl_ca,	&octxctl_cd,	 0, STAR, loc+  7,    0, pv+11, 50,    0},
/* 73: ogxnexus* at octxctl*|octsctl*|simplebus*|simplebus* early 0 */
    {&ogxnexus_ca,	&ogxnexus_cd,	 0, STAR, loc+  7,    0, pv+11, 50,    0},
/* 74: ogx* at ogxnexus* */
    {&ogx_ca,		&ogx_cd,	 0, STAR,     loc,    0, pv+51, 60,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	44 /* mainbus0 */,
	-1
};

int cfroots_size = 2;

/* pseudo-devices */
extern void openpromattach(int);
extern void loopattach(int);
extern void bpfilterattach(int);
extern void rdattach(int);
extern void wsmuxattach(int);

char *pdevnames[] = {
	"openprom",
	"loop",
	"bpfilter",
	"rd",
	"wsmux",
};

int pdevnames_size = 5;

struct pdevinit pdevinit[] = {
	{ openpromattach, 1 },
	{ loopattach, 1 },
	{ bpfilterattach, 1 },
	{ rdattach, 1 },
	{ wsmuxattach, 2 },
	{ NULL, 0 }
};
