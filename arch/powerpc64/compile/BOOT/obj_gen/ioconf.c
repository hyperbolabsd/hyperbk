/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/powerpc64/conf/BOOT"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver ahci_cd;
extern struct cfdriver nvme_cd;
extern struct cfdriver xhci_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver cd_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver ch_cd;
extern struct cfdriver st_cd;
extern struct cfdriver uk_cd;
extern struct cfdriver safte_cd;
extern struct cfdriver ses_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver wsmouse_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver mpii_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver opal_cd;
extern struct cfdriver opalcons_cd;
extern struct cfdriver phb_cd;
extern struct cfdriver xics_cd;
extern struct cfdriver xicp_cd;
extern struct cfdriver xive_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver uhidev_cd;
extern struct cfdriver uhid_cd;
extern struct cfdriver ukbd_cd;
extern struct cfdriver ums_cd;
extern struct cfdriver umass_cd;

extern struct cfattach mainbus_ca;
extern struct cfattach cpu_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach cd_ca;
extern struct cfattach ch_ca;
extern struct cfattach sd_ca;
extern struct cfattach st_ca;
extern struct cfattach uk_ca;
extern struct cfattach safte_ca;
extern struct cfattach ses_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach wsmouse_ca;
extern struct cfattach pci_ca;
extern struct cfattach ahci_pci_ca;
extern struct cfattach nvme_pci_ca;
extern struct cfattach mpii_ca;
extern struct cfattach ppb_ca;
extern struct cfattach xhci_pci_ca;
extern struct cfattach opal_ca;
extern struct cfattach opalcons_ca;
extern struct cfattach phb_ca;
extern struct cfattach xics_ca;
extern struct cfattach xicp_ca;
extern struct cfattach xive_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach uhidev_ca;
extern struct cfattach uhid_ca;
extern struct cfattach ukbd_ca;
extern struct cfattach ums_ca;
extern struct cfattach umass_ca;


/* locators */
static long loc[9] = {
	-1, -1, -1, -1, -1, -1, -1, 1,
	0,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"target",
	"lun",
	"console",
	"mux",
	"bus",
	"dev",
	"function",
	"early",
	"port",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"reportid",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, 1, -1, 2, 3, -1, 3,
	-1, 4, -1, 4, -1, 5, 6, -1,
	7, -1, 7, -1, 8, 9, 10, 11,
	12, 13, -1, 8, 9, 10, 11, 12,
	13, -1, 14, -1,
};

/* size of parent vectors */
int pv_size = 28;

/* parent vectors */
short pv[28] = {
	31, 15, 14, 13, -1, 25, 26, -1, 20, 16, -1, 18, 0, -1, 2, -1,
	12, -1, 30, -1, 24, -1, 27, -1, 17, -1, 29, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+ 4, 0,    0},
/*  1: cpu0 at mainbus0 */
    {&cpu_ca,		&cpu_cd,	 0, NORM,     loc,    0, pv+12, 18,    0},
/*  2: scsibus* at umass*|mpii*|nvme*|ahci* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+ 0, 0,    0},
/*  3: cd* at scsibus* target -1 lun -1 */
    {&cd_ca,		&cd_cd,		 0, STAR, loc+  4,    0, pv+14, 1,    0},
/*  4: ch* at scsibus* target -1 lun -1 */
    {&ch_ca,		&ch_cd,		 0, STAR, loc+  4,    0, pv+14, 1,    0},
/*  5: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+  4,    0, pv+14, 1,    0},
/*  6: st* at scsibus* target -1 lun -1 */
    {&st_ca,		&st_cd,		 0, STAR, loc+  4,    0, pv+14, 1,    0},
/*  7: uk* at scsibus* target -1 lun -1 */
    {&uk_ca,		&uk_cd,		 0, STAR, loc+  4,    0, pv+14, 1,    0},
/*  8: safte* at scsibus* target -1 lun -1 */
    {&safte_ca,		&safte_cd,	 0, STAR, loc+  4,    0, pv+14, 1,    0},
/*  9: ses* at scsibus* target -1 lun -1 */
    {&ses_ca,		&ses_cd,	 0, STAR, loc+  4,    0, pv+14, 1,    0},
/* 10: wskbd* at ukbd* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+  6,    0, pv+26, 4,    0},
/* 11: wsmouse* at ums* mux 0 */
    {&wsmouse_ca,	&wsmouse_cd,	 0, STAR, loc+  8,    0, pv+18, 7,    0},
/* 12: pci* at phb*|ppb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+  5,    0, pv+ 8, 9,    0},
/* 13: ahci* at pci* dev -1 function -1 */
    {&ahci_pci_ca,	&ahci_cd,	 0, STAR, loc+  4,    0, pv+16, 13,    0},
/* 14: nvme* at pci* dev -1 function -1 */
    {&nvme_pci_ca,	&nvme_cd,	 0, STAR, loc+  4,    0, pv+16, 13,    0},
/* 15: mpii* at pci* dev -1 function -1 */
    {&mpii_ca,		&mpii_cd,	 0, STAR, loc+  4,    0, pv+16, 13,    0},
/* 16: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+  4,    0, pv+16, 13,    0},
/* 17: xhci* at pci* dev -1 function -1 */
    {&xhci_pci_ca,	&xhci_cd,	 0, STAR, loc+  4,    0, pv+16, 13,    0},
/* 18: opal0 at opal0|mainbus0 early 0 */
    {&opal_ca,		&opal_cd,	 0, NORM, loc+  8,    0, pv+11, 16,    0},
/* 19: opalcons* at opal0|mainbus0 early 0 */
    {&opalcons_ca,	&opalcons_cd,	 0, STAR, loc+  8,    0, pv+11, 16,    0},
/* 20: phb* at opal0|mainbus0 early 0 */
    {&phb_ca,		&phb_cd,	 0, STAR, loc+  8,    0, pv+11, 16,    0},
/* 21: xics* at opal0|mainbus0 early 0 */
    {&xics_ca,		&xics_cd,	 0, STAR, loc+  8,    0, pv+11, 16,    0},
/* 22: xicp* at opal0|mainbus0 early 0 */
    {&xicp_ca,		&xicp_cd,	 0, STAR, loc+  8,    0, pv+11, 16,    0},
/* 23: xive* at opal0|mainbus0 early 0 */
    {&xive_ca,		&xive_cd,	 0, STAR, loc+  8,    0, pv+11, 16,    0},
/* 24: usb* at xhci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+24, 19,    0},
/* 25: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+20, 19,    0},
/* 26: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+  0,    0, pv+ 5, 20,    0},
/* 27: uhidev* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhidev_ca,	&uhidev_cd,	 0, STAR, loc+  0,    0, pv+ 5, 20,    0},
/* 28: uhid* at uhidev* reportid -1 */
    {&uhid_ca,		&uhid_cd,	 0, STAR, loc+  5,    0, pv+22, 34,    0},
/* 29: ukbd* at uhidev* reportid -1 */
    {&ukbd_ca,		&ukbd_cd,	 0, STAR, loc+  5,    0, pv+22, 34,    0},
/* 30: ums* at uhidev* reportid -1 */
    {&ums_ca,		&ums_cd,	 0, STAR, loc+  5,    0, pv+22, 34,    0},
/* 31: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+  0,    0, pv+ 5, 20,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 0 /* mainbus0 */,
	-1
};

int cfroots_size = 2;

/* pseudo-devices */
extern void loopattach(int);
extern void vlanattach(int);
extern void trunkattach(int);
extern void bpfilterattach(int);
extern void rdattach(int);
extern void bioattach(int);
extern void kexecattach(int);
extern void wsmuxattach(int);

char *pdevnames[] = {
	"loop",
	"vlan",
	"trunk",
	"bpfilter",
	"rd",
	"bio",
	"kexec",
	"wsmux",
};

int pdevnames_size = 8;

struct pdevinit pdevinit[] = {
	{ loopattach, 1 },
	{ vlanattach, 1 },
	{ trunkattach, 1 },
	{ bpfilterattach, 1 },
	{ rdattach, 1 },
	{ bioattach, 1 },
	{ kexecattach, 1 },
	{ wsmuxattach, 2 },
	{ NULL, 0 }
};
