/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/powerpc64/conf/GENERIC"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver video_cd;
extern struct cfdriver audio_cd;
extern struct cfdriver midi_cd;
extern struct cfdriver drm_cd;
extern struct cfdriver ahci_cd;
extern struct cfdriver nvme_cd;
extern struct cfdriver wi_cd;
extern struct cfdriver athn_cd;
extern struct cfdriver bwfm_cd;
extern struct cfdriver xhci_cd;
extern struct cfdriver radio_cd;
extern struct cfdriver ipmi_cd;
extern struct cfdriver vscsi_cd;
extern struct cfdriver softraid_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver cd_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver ch_cd;
extern struct cfdriver st_cd;
extern struct cfdriver uk_cd;
extern struct cfdriver safte_cd;
extern struct cfdriver ses_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver wsmouse_cd;
extern struct cfdriver brgphy_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver mpii_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver bge_cd;
extern struct cfdriver mcx_cd;
extern struct cfdriver radeondrm_cd;
extern struct cfdriver opal_cd;
extern struct cfdriver opalcons_cd;
extern struct cfdriver opalsens_cd;
extern struct cfdriver phb_cd;
extern struct cfdriver xics_cd;
extern struct cfdriver xicp_cd;
extern struct cfdriver xive_cd;
extern struct cfdriver astfb_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver uaudio_cd;
extern struct cfdriver uvideo_cd;
extern struct cfdriver utvfu_cd;
extern struct cfdriver udl_cd;
extern struct cfdriver umidi_cd;
extern struct cfdriver ucom_cd;
extern struct cfdriver ugen_cd;
extern struct cfdriver uhidev_cd;
extern struct cfdriver uhid_cd;
extern struct cfdriver fido_cd;
extern struct cfdriver ujoy_cd;
extern struct cfdriver ukbd_cd;
extern struct cfdriver ums_cd;
extern struct cfdriver umt_cd;
extern struct cfdriver uts_cd;
extern struct cfdriver ubcmtp_cd;
extern struct cfdriver ucycom_cd;
extern struct cfdriver uslhcom_cd;
extern struct cfdriver ulpt_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver uthum_cd;
extern struct cfdriver ugold_cd;
extern struct cfdriver utrh_cd;
extern struct cfdriver uoakrh_cd;
extern struct cfdriver uoaklux_cd;
extern struct cfdriver uoakv_cd;
extern struct cfdriver uonerng_cd;
extern struct cfdriver urng_cd;
extern struct cfdriver udcf_cd;
extern struct cfdriver umbg_cd;
extern struct cfdriver uvisor_cd;
extern struct cfdriver udsbr_cd;
extern struct cfdriver utwitch_cd;
extern struct cfdriver aue_cd;
extern struct cfdriver axe_cd;
extern struct cfdriver axen_cd;
extern struct cfdriver smsc_cd;
extern struct cfdriver cue_cd;
extern struct cfdriver kue_cd;
extern struct cfdriver cdce_cd;
extern struct cfdriver urndis_cd;
extern struct cfdriver mos_cd;
extern struct cfdriver mue_cd;
extern struct cfdriver udav_cd;
extern struct cfdriver upl_cd;
extern struct cfdriver ugl_cd;
extern struct cfdriver url_cd;
extern struct cfdriver ure_cd;
extern struct cfdriver umodem_cd;
extern struct cfdriver uftdi_cd;
extern struct cfdriver uplcom_cd;
extern struct cfdriver umct_cd;
extern struct cfdriver uvscom_cd;
extern struct cfdriver ubsa_cd;
extern struct cfdriver ukspan_cd;
extern struct cfdriver uslcom_cd;
extern struct cfdriver uark_cd;
extern struct cfdriver moscom_cd;
extern struct cfdriver umcs_cd;
extern struct cfdriver uscom_cd;
extern struct cfdriver ucrcom_cd;
extern struct cfdriver uxrcom_cd;
extern struct cfdriver uipaq_cd;
extern struct cfdriver umsm_cd;
extern struct cfdriver uchcom_cd;
extern struct cfdriver uticom_cd;
extern struct cfdriver atu_cd;
extern struct cfdriver ural_cd;
extern struct cfdriver rum_cd;
extern struct cfdriver run_cd;
extern struct cfdriver zyd_cd;
extern struct cfdriver upgt_cd;
extern struct cfdriver urtw_cd;
extern struct cfdriver urtwn_cd;
extern struct cfdriver rsu_cd;
extern struct cfdriver otus_cd;
extern struct cfdriver umb_cd;
extern struct cfdriver uath_cd;
extern struct cfdriver uberry_cd;
extern struct cfdriver upd_cd;
extern struct cfdriver uwacom_cd;
extern struct cfdriver umstc_cd;
extern struct cfdriver uhidpp_cd;
extern struct cfdriver ucc_cd;

extern struct cfattach video_ca;
extern struct cfattach audio_ca;
extern struct cfattach midi_ca;
extern struct cfattach drm_ca;
extern struct cfattach radio_ca;
extern struct cfattach vscsi_ca;
extern struct cfattach softraid_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach cpu_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach cd_ca;
extern struct cfattach ch_ca;
extern struct cfattach sd_ca;
extern struct cfattach st_ca;
extern struct cfattach uk_ca;
extern struct cfattach safte_ca;
extern struct cfattach ses_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach wsmouse_ca;
extern struct cfattach brgphy_ca;
extern struct cfattach pci_ca;
extern struct cfattach ahci_pci_ca;
extern struct cfattach nvme_pci_ca;
extern struct cfattach mpii_ca;
extern struct cfattach ppb_ca;
extern struct cfattach xhci_pci_ca;
extern struct cfattach bge_ca;
extern struct cfattach mcx_ca;
extern struct cfattach radeondrm_ca;
extern struct cfattach opal_ca;
extern struct cfattach opalcons_ca;
extern struct cfattach opalsens_ca;
extern struct cfattach ipmi_opal_ca;
extern struct cfattach phb_ca;
extern struct cfattach xics_ca;
extern struct cfattach xicp_ca;
extern struct cfattach xive_ca;
extern struct cfattach astfb_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach uaudio_ca;
extern struct cfattach uvideo_ca;
extern struct cfattach utvfu_ca;
extern struct cfattach udl_ca;
extern struct cfattach umidi_ca;
extern struct cfattach ucom_ca;
extern struct cfattach ugen_ca;
extern struct cfattach uhidev_ca;
extern struct cfattach uhid_ca;
extern struct cfattach fido_ca;
extern struct cfattach ujoy_ca;
extern struct cfattach ukbd_ca;
extern struct cfattach ums_ca;
extern struct cfattach umt_ca;
extern struct cfattach uts_ca;
extern struct cfattach ubcmtp_ca;
extern struct cfattach ucycom_ca;
extern struct cfattach uslhcom_ca;
extern struct cfattach ulpt_ca;
extern struct cfattach umass_ca;
extern struct cfattach uthum_ca;
extern struct cfattach ugold_ca;
extern struct cfattach utrh_ca;
extern struct cfattach uoakrh_ca;
extern struct cfattach uoaklux_ca;
extern struct cfattach uoakv_ca;
extern struct cfattach uonerng_ca;
extern struct cfattach urng_ca;
extern struct cfattach udcf_ca;
extern struct cfattach umbg_ca;
extern struct cfattach uvisor_ca;
extern struct cfattach udsbr_ca;
extern struct cfattach utwitch_ca;
extern struct cfattach aue_ca;
extern struct cfattach axe_ca;
extern struct cfattach axen_ca;
extern struct cfattach smsc_ca;
extern struct cfattach cue_ca;
extern struct cfattach kue_ca;
extern struct cfattach cdce_ca;
extern struct cfattach urndis_ca;
extern struct cfattach mos_ca;
extern struct cfattach mue_ca;
extern struct cfattach udav_ca;
extern struct cfattach upl_ca;
extern struct cfattach ugl_ca;
extern struct cfattach url_ca;
extern struct cfattach ure_ca;
extern struct cfattach umodem_ca;
extern struct cfattach uftdi_ca;
extern struct cfattach uplcom_ca;
extern struct cfattach umct_ca;
extern struct cfattach uvscom_ca;
extern struct cfattach ubsa_ca;
extern struct cfattach ukspan_ca;
extern struct cfattach uslcom_ca;
extern struct cfattach uark_ca;
extern struct cfattach moscom_ca;
extern struct cfattach umcs_ca;
extern struct cfattach uscom_ca;
extern struct cfattach ucrcom_ca;
extern struct cfattach uxrcom_ca;
extern struct cfattach uipaq_ca;
extern struct cfattach umsm_ca;
extern struct cfattach uchcom_ca;
extern struct cfattach uticom_ca;
extern struct cfattach wi_usb_ca;
extern struct cfattach atu_ca;
extern struct cfattach ural_ca;
extern struct cfattach rum_ca;
extern struct cfattach run_ca;
extern struct cfattach zyd_ca;
extern struct cfattach upgt_ca;
extern struct cfattach urtw_ca;
extern struct cfattach urtwn_ca;
extern struct cfattach rsu_ca;
extern struct cfattach otus_ca;
extern struct cfattach umb_ca;
extern struct cfattach uath_ca;
extern struct cfattach athn_usb_ca;
extern struct cfattach uberry_ca;
extern struct cfattach upd_ca;
extern struct cfattach uwacom_ca;
extern struct cfattach bwfm_usb_ca;
extern struct cfattach umstc_ca;
extern struct cfattach uhidpp_ca;
extern struct cfattach ucc_ca;


/* locators */
static long loc[10] = {
	-1, -1, -1, -1, -1, -1, -1, -1,
	1, 0,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"primary",
	"target",
	"lun",
	"console",
	"mux",
	"phy",
	"bus",
	"dev",
	"function",
	"early",
	"port",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"portno",
	"reportid",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, -1, 1, 2, -1, 3, 0,
	4, -1, 3, 0, 4, -1, 3, 4,
	-1, 3, 4, -1, 4, -1, 4, -1,
	4, -1, 4, -1, 4, -1, 5, -1,
	5, -1, 5, -1, 5, -1, 5, -1,
	5, -1, 5, -1, 5, -1, 5, -1,
	5, -1, 6, -1, 6, -1, 7, 8,
	-1, 9, -1, 10, 11, 12, 13, 14,
	15, -1, 10, 11, 12, 13, 14, 15,
	-1, 16, -1, 16, -1, 16, -1, 16,
	-1, 16, -1, 16, -1, 16, -1, 16,
	-1, 16, -1, 16, -1, 16, -1, 16,
	-1, 16, -1, 16, -1, 16, -1, 16,
	-1, 16, -1, 16, -1, 16, -1, 16,
	-1, 16, -1, 17, -1,
};

/* size of parent vectors */
int pv_size = 84;

/* parent vectors */
short pv[84] = {
	90, 72, 94, 95, 96, 91, 92, 93, 97, 101, 102, 98, 99, 100, 104, 105,
	106, 107, 103, 58, 59, -1, 89, 88, 85, 84, 83, 78, 77, 76, 75, 27,
	-1, 6, 5, 61, 24, 23, 22, -1, 57, 54, 55, 56, 124, -1, 38, 29,
	45, -1, 40, 41, -1, 53, 128, -1, 34, 25, -1, 30, 7, -1, 42, 44,
	-1, 43, 44, -1, 21, -1, 49, -1, 29, -1, 26, -1, 9, -1, 73, -1,
	39, -1, 46, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: video* at uvideo*|utvfu* */
    {&video_ca,		&video_cd,	 0, STAR,     loc,    0, pv+65, 0,    0},
/*  1: audio* at uaudio*|utvfu* */
    {&audio_ca,		&audio_cd,	 0, STAR,     loc,    0, pv+62, 0,    0},
/*  2: midi* at umidi* */
    {&midi_ca,		&midi_cd,	 0, STAR,     loc,    0, pv+82, 0,    0},
/*  3: drm* at radeondrm* primary -1 */
    {&drm_ca,		&drm_cd,	 0, STAR, loc+  5,    0, pv+72, 1,    0},
/*  4: radio* at udsbr* */
    {&radio_ca,		&radio_cd,	 0, STAR,     loc,    0, pv+78, 2,    0},
/*  5: vscsi0 at root */
    {&vscsi_ca,		&vscsi_cd,	 0, NORM,     loc,    0, pv+21, 0,    0},
/*  6: softraid0 at root */
    {&softraid_ca,	&softraid_cd,	 0, NORM,     loc,    0, pv+21, 0,    0},
/*  7: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+21, 0,    0},
/*  8: cpu0 at mainbus0 */
    {&cpu_ca,		&cpu_cd,	 0, NORM,     loc,    0, pv+60, 2,    0},
/*  9: scsibus* at softraid0|vscsi0|umass*|mpii*|nvme*|ahci* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+33, 2,    0},
/* 10: cd* at scsibus* target -1 lun -1 */
    {&cd_ca,		&cd_cd,		 0, STAR, loc+  4,    0, pv+76, 3,    0},
/* 11: ch* at scsibus* target -1 lun -1 */
    {&ch_ca,		&ch_cd,		 0, STAR, loc+  4,    0, pv+76, 3,    0},
/* 12: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+  4,    0, pv+76, 3,    0},
/* 13: st* at scsibus* target -1 lun -1 */
    {&st_ca,		&st_cd,		 0, STAR, loc+  4,    0, pv+76, 3,    0},
/* 14: uk* at scsibus* target -1 lun -1 */
    {&uk_ca,		&uk_cd,		 0, STAR, loc+  4,    0, pv+76, 3,    0},
/* 15: safte* at scsibus* target -1 lun -1 */
    {&safte_ca,		&safte_cd,	 0, STAR, loc+  4,    0, pv+76, 3,    0},
/* 16: ses* at scsibus* target -1 lun -1 */
    {&ses_ca,		&ses_cd,	 0, STAR, loc+  4,    0, pv+76, 3,    0},
/* 17: wsdisplay* at astfb*|radeondrm*|udl* console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+  6,    0, pv+46, 6,    0},
/* 18: wskbd* at ukbd*|ucc* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+  7,    0, pv+53, 14,    0},
/* 19: wsmouse* at ubcmtp*|ums*|umt*|uts*|uwacom* mux 0 */
    {&wsmouse_ca,	&wsmouse_cd,	 0, STAR, loc+  9,    0, pv+40, 20,    0},
/* 20: brgphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|bge* phy -1 */
    {&brgphy_ca,	&brgphy_cd,	 0, STAR, loc+  5,    0, pv+22, 30,    0},
/* 21: pci* at phb*|ppb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+  5,    0, pv+56, 50,    0},
/* 22: ahci* at pci* dev -1 function -1 */
    {&ahci_pci_ca,	&ahci_cd,	 0, STAR, loc+  4,    0, pv+68, 54,    0},
/* 23: nvme* at pci* dev -1 function -1 */
    {&nvme_pci_ca,	&nvme_cd,	 0, STAR, loc+  4,    0, pv+68, 54,    0},
/* 24: mpii* at pci* dev -1 function -1 */
    {&mpii_ca,		&mpii_cd,	 0, STAR, loc+  4,    0, pv+68, 54,    0},
/* 25: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+  4,    0, pv+68, 54,    0},
/* 26: xhci* at pci* dev -1 function -1 */
    {&xhci_pci_ca,	&xhci_cd,	 0, STAR, loc+  4,    0, pv+68, 54,    0},
/* 27: bge* at pci* dev -1 function -1 */
    {&bge_ca,		&bge_cd,	 0, STAR, loc+  4,    0, pv+68, 54,    0},
/* 28: mcx* at pci* dev -1 function -1 */
    {&mcx_ca,		&mcx_cd,	 0, STAR, loc+  4,    0, pv+68, 54,    0},
/* 29: radeondrm* at pci* dev -1 function -1 */
    {&radeondrm_ca,	&radeondrm_cd,	 0, STAR, loc+  4,    0, pv+68, 54,    0},
/* 30: opal0 at opal0|mainbus0 early 0 */
    {&opal_ca,		&opal_cd,	 0, NORM, loc+  9,    0, pv+59, 57,    0},
/* 31: opalcons* at opal0|mainbus0 early 0 */
    {&opalcons_ca,	&opalcons_cd,	 0, STAR, loc+  9,    0, pv+59, 57,    0},
/* 32: opalsens* at opal0|mainbus0 early 0 */
    {&opalsens_ca,	&opalsens_cd,	 0, STAR, loc+  9,    0, pv+59, 57,    0},
/* 33: ipmi0 at opal0|mainbus0 early 0 */
    {&ipmi_opal_ca,	&ipmi_cd,	 0, NORM, loc+  9,    0, pv+59, 57,    0},
/* 34: phb* at opal0|mainbus0 early 0 */
    {&phb_ca,		&phb_cd,	 0, STAR, loc+  9,    0, pv+59, 57,    0},
/* 35: xics* at opal0|mainbus0 early 0 */
    {&xics_ca,		&xics_cd,	 0, STAR, loc+  9,    0, pv+59, 57,    0},
/* 36: xicp* at opal0|mainbus0 early 0 */
    {&xicp_ca,		&xicp_cd,	 0, STAR, loc+  9,    0, pv+59, 57,    0},
/* 37: xive* at opal0|mainbus0 early 0 */
    {&xive_ca,		&xive_cd,	 0, STAR, loc+  9,    0, pv+59, 57,    0},
/* 38: astfb* at pci* dev -1 function -1 */
    {&astfb_ca,		&astfb_cd,	 0, STAR, loc+  4,    0, pv+68, 54,    0},
/* 39: usb* at xhci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+74, 58,    0},
/* 40: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+80, 58,    0},
/* 41: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 42: uaudio* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uaudio_ca,	&uaudio_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 43: uvideo* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvideo_ca,	&uvideo_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 44: utvfu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&utvfu_ca,		&utvfu_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 45: udl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udl_ca,		&udl_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 46: umidi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umidi_ca,		&umidi_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 47: ucom* at umodem*|uvisor*|uvscom*|ubsa*|ukspan*|uftdi*|uplcom*|umct*|uslcom*|uscom*|ucrcom*|uark*|moscom*|umcs*|uipaq*|umsm*|uchcom*|uticom*|uxrcom*|ucycom*|uslhcom* portno -1 */
    {&ucom_ca,		&ucom_cd,	 0, STAR, loc+  5,    0, pv+ 0, 73,    0},
/* 48: ugen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugen_ca,		&ugen_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 49: uhidev* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhidev_ca,	&uhidev_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 50: uhid* at uhidev* reportid -1 */
    {&uhid_ca,		&uhid_cd,	 0, STAR, loc+  5,    0, pv+70, 115,    0},
/* 51: fido* at uhidev* reportid -1 */
    {&fido_ca,		&fido_cd,	 0, STAR, loc+  5,    0, pv+70, 115,    0},
/* 52: ujoy* at uhidev* reportid -1 */
    {&ujoy_ca,		&ujoy_cd,	 0, STAR, loc+  5,    0, pv+70, 115,    0},
/* 53: ukbd* at uhidev* reportid -1 */
    {&ukbd_ca,		&ukbd_cd,	 0, STAR, loc+  5,    0, pv+70, 115,    0},
/* 54: ums* at uhidev* reportid -1 */
    {&ums_ca,		&ums_cd,	 0, STAR, loc+  5,    0, pv+70, 115,    0},
/* 55: umt* at uhidev* reportid -1 */
    {&umt_ca,		&umt_cd,	 0, STAR, loc+  5,    0, pv+70, 115,    0},
/* 56: uts* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uts_ca,		&uts_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 57: ubcmtp* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ubcmtp_ca,	&ubcmtp_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 58: ucycom* at uhidev* reportid -1 */
    {&ucycom_ca,	&ucycom_cd,	 0, STAR, loc+  5,    0, pv+70, 115,    0},
/* 59: uslhcom* at uhidev* reportid -1 */
    {&uslhcom_ca,	&uslhcom_cd,	 0, STAR, loc+  5,    0, pv+70, 115,    0},
/* 60: ulpt* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ulpt_ca,		&ulpt_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 61: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 62: uthum* at uhidev* reportid -1 */
    {&uthum_ca,		&uthum_cd,	 0, STAR, loc+  5,    0, pv+70, 115,    0},
/* 63: ugold* at uhidev* reportid -1 */
    {&ugold_ca,		&ugold_cd,	 0, STAR, loc+  5,    0, pv+70, 115,    0},
/* 64: utrh* at uhidev* reportid -1 */
    {&utrh_ca,		&utrh_cd,	 0, STAR, loc+  5,    0, pv+70, 115,    0},
/* 65: uoakrh* at uhidev* reportid -1 */
    {&uoakrh_ca,	&uoakrh_cd,	 0, STAR, loc+  5,    0, pv+70, 115,    0},
/* 66: uoaklux* at uhidev* reportid -1 */
    {&uoaklux_ca,	&uoaklux_cd,	 0, STAR, loc+  5,    0, pv+70, 115,    0},
/* 67: uoakv* at uhidev* reportid -1 */
    {&uoakv_ca,		&uoakv_cd,	 0, STAR, loc+  5,    0, pv+70, 115,    0},
/* 68: uonerng* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uonerng_ca,	&uonerng_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 69: urng* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urng_ca,		&urng_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 70: udcf* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udcf_ca,		&udcf_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 71: umbg* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umbg_ca,		&umbg_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 72: uvisor* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvisor_ca,	&uvisor_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 73: udsbr* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udsbr_ca,		&udsbr_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 74: utwitch* at uhidev* reportid -1 */
    {&utwitch_ca,	&utwitch_cd,	 0, STAR, loc+  5,    0, pv+70, 115,    0},
/* 75: aue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&aue_ca,		&aue_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 76: axe* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axe_ca,		&axe_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 77: axen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axen_ca,		&axen_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 78: smsc* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&smsc_ca,		&smsc_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 79: cue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cue_ca,		&cue_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 80: kue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&kue_ca,		&kue_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 81: cdce* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cdce_ca,		&cdce_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 82: urndis* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urndis_ca,	&urndis_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 83: mos* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mos_ca,		&mos_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 84: mue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mue_ca,		&mue_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 85: udav* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udav_ca,		&udav_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 86: upl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upl_ca,		&upl_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 87: ugl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugl_ca,		&ugl_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 88: url* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&url_ca,		&url_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 89: ure* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ure_ca,		&ure_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 90: umodem* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umodem_ca,	&umodem_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 91: uftdi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uftdi_ca,		&uftdi_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 92: uplcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uplcom_ca,	&uplcom_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 93: umct* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umct_ca,		&umct_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 94: uvscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvscom_ca,	&uvscom_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 95: ubsa* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ubsa_ca,		&ubsa_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 96: ukspan* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ukspan_ca,	&ukspan_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 97: uslcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uslcom_ca,	&uslcom_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 98: uark* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uark_ca,		&uark_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/* 99: moscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&moscom_ca,	&moscom_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/*100: umcs* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umcs_ca,		&umcs_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/*101: uscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uscom_ca,		&uscom_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/*102: ucrcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ucrcom_ca,	&ucrcom_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/*103: uxrcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uxrcom_ca,	&uxrcom_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/*104: uipaq* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uipaq_ca,		&uipaq_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/*105: umsm* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umsm_ca,		&umsm_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/*106: uchcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uchcom_ca,	&uchcom_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/*107: uticom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uticom_ca,	&uticom_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/*108: wi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&wi_usb_ca,	&wi_cd,		 0, STAR, loc+  0,    0, pv+50, 59,    0},
/*109: atu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&atu_ca,		&atu_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/*110: ural* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ural_ca,		&ural_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/*111: rum* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rum_ca,		&rum_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/*112: run* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&run_ca,		&run_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/*113: zyd* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&zyd_ca,		&zyd_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/*114: upgt* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upgt_ca,		&upgt_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/*115: urtw* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtw_ca,		&urtw_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/*116: urtwn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtwn_ca,		&urtwn_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/*117: rsu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rsu_ca,		&rsu_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/*118: otus* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&otus_ca,		&otus_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/*119: umb* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umb_ca,		&umb_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/*120: uath* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uath_ca,		&uath_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/*121: athn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&athn_usb_ca,	&athn_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/*122: uberry* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uberry_ca,	&uberry_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/*123: upd* at uhidev* reportid -1 */
    {&upd_ca,		&upd_cd,	 0, STAR, loc+  5,    0, pv+70, 115,    0},
/*124: uwacom* at uhidev* reportid -1 */
    {&uwacom_ca,	&uwacom_cd,	 0, STAR, loc+  5,    0, pv+70, 115,    0},
/*125: bwfm* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&bwfm_usb_ca,	&bwfm_cd,	 0, STAR, loc+  0,    0, pv+50, 59,    0},
/*126: umstc* at uhidev* reportid -1 */
    {&umstc_ca,		&umstc_cd,	 0, STAR, loc+  5,    0, pv+70, 115,    0},
/*127: uhidpp* at uhidev* reportid -1 */
    {&uhidpp_ca,	&uhidpp_cd,	 0, STAR, loc+  5,    0, pv+70, 115,    0},
/*128: ucc* at uhidev* reportid -1 */
    {&ucc_ca,		&ucc_cd,	 0, STAR, loc+  5,    0, pv+70, 115,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 5 /* vscsi0 */,
	 6 /* softraid0 */,
	 7 /* mainbus0 */,
	-1
};

int cfroots_size = 4;

/* pseudo-devices */
extern void pfattach(int);
extern void pflogattach(int);
extern void pfsyncattach(int);
extern void pflowattach(int);
extern void encattach(int);
extern void ptyattach(int);
extern void nmeaattach(int);
extern void mstsattach(int);
extern void endrunattach(int);
extern void vndattach(int);
extern void ksymsattach(int);
extern void bpfilterattach(int);
extern void bridgeattach(int);
extern void vebattach(int);
extern void carpattach(int);
extern void etheripattach(int);
extern void gifattach(int);
extern void greattach(int);
extern void loopattach(int);
extern void mpeattach(int);
extern void mpwattach(int);
extern void mpipattach(int);
extern void bpeattach(int);
extern void pairattach(int);
extern void pppattach(int);
extern void pppoeattach(int);
extern void pppxattach(int);
extern void spppattach(int);
extern void trunkattach(int);
extern void aggrattach(int);
extern void tpmrattach(int);
extern void tunattach(int);
extern void vetherattach(int);
extern void vxlanattach(int);
extern void vlanattach(int);
extern void switchattach(int);
extern void wgattach(int);
extern void bioattach(int);
extern void fuseattach(int);
extern void openpromattach(int);
extern void dtattach(int);
extern void wsmuxattach(int);

char *pdevnames[] = {
	"pf",
	"pflog",
	"pfsync",
	"pflow",
	"enc",
	"pty",
	"nmea",
	"msts",
	"endrun",
	"vnd",
	"ksyms",
	"bpfilter",
	"bridge",
	"veb",
	"carp",
	"etherip",
	"gif",
	"gre",
	"loop",
	"mpe",
	"mpw",
	"mpip",
	"bpe",
	"pair",
	"ppp",
	"pppoe",
	"pppx",
	"sppp",
	"trunk",
	"aggr",
	"tpmr",
	"tun",
	"vether",
	"vxlan",
	"vlan",
	"switch",
	"wg",
	"bio",
	"fuse",
	"openprom",
	"dt",
	"wsmux",
};

int pdevnames_size = 42;

struct pdevinit pdevinit[] = {
	{ pfattach, 1 },
	{ pflogattach, 1 },
	{ pfsyncattach, 1 },
	{ pflowattach, 1 },
	{ encattach, 1 },
	{ ptyattach, 16 },
	{ nmeaattach, 1 },
	{ mstsattach, 1 },
	{ endrunattach, 1 },
	{ vndattach, 4 },
	{ ksymsattach, 1 },
	{ bpfilterattach, 1 },
	{ bridgeattach, 1 },
	{ vebattach, 1 },
	{ carpattach, 1 },
	{ etheripattach, 1 },
	{ gifattach, 1 },
	{ greattach, 1 },
	{ loopattach, 1 },
	{ mpeattach, 1 },
	{ mpwattach, 1 },
	{ mpipattach, 1 },
	{ bpeattach, 1 },
	{ pairattach, 1 },
	{ pppattach, 1 },
	{ pppoeattach, 1 },
	{ pppxattach, 1 },
	{ spppattach, 1 },
	{ trunkattach, 1 },
	{ aggrattach, 1 },
	{ tpmrattach, 1 },
	{ tunattach, 1 },
	{ vetherattach, 1 },
	{ vxlanattach, 1 },
	{ vlanattach, 1 },
	{ switchattach, 1 },
	{ wgattach, 1 },
	{ bioattach, 1 },
	{ fuseattach, 1 },
	{ openpromattach, 1 },
	{ dtattach, 1 },
	{ wsmuxattach, 2 },
	{ NULL, 0 }
};
