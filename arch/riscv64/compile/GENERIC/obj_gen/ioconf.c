/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/riscv64/conf/GENERIC"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver audio_cd;
extern struct cfdriver midi_cd;
extern struct cfdriver drm_cd;
extern struct cfdriver ahci_cd;
extern struct cfdriver nvme_cd;
extern struct cfdriver wi_cd;
extern struct cfdriver com_cd;
extern struct cfdriver athn_cd;
extern struct cfdriver bwfm_cd;
extern struct cfdriver virtio_cd;
extern struct cfdriver xhci_cd;
extern struct cfdriver vscsi_cd;
extern struct cfdriver softraid_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver intc_cd;
extern struct cfdriver simplebus_cd;
extern struct cfdriver plic_cd;
extern struct cfdriver sfcc_cd;
extern struct cfdriver sfclock_cd;
extern struct cfdriver sfuart_cd;
extern struct cfdriver vio_cd;
extern struct cfdriver vioblk_cd;
extern struct cfdriver viornd_cd;
extern struct cfdriver vioscsi_cd;
extern struct cfdriver iic_cd;
extern struct cfdriver titmp_cd;
extern struct cfdriver eephy_cd;
extern struct cfdriver rlphy_cd;
extern struct cfdriver sqphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver bmtphy_cd;
extern struct cfdriver brgphy_cd;
extern struct cfdriver amphy_cd;
extern struct cfdriver acphy_cd;
extern struct cfdriver urlphy_cd;
extern struct cfdriver rgephy_cd;
extern struct cfdriver atphy_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver aq_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver em_cd;
extern struct cfdriver ix_cd;
extern struct cfdriver oce_cd;
extern struct cfdriver iwm_cd;
extern struct cfdriver bge_cd;
extern struct cfdriver radeondrm_cd;
extern struct cfdriver sdmmc_cd;
extern struct cfdriver gfrtc_cd;
extern struct cfdriver ociic_cd;
extern struct cfdriver dwge_cd;
extern struct cfdriver syscon_cd;
extern struct cfdriver cad_cd;
extern struct cfdriver dwmmc_cd;
extern struct cfdriver dwpcie_cd;
extern struct cfdriver dapmic_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver cd_cd;
extern struct cfdriver ch_cd;
extern struct cfdriver uk_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver wsmouse_cd;
extern struct cfdriver pciecam_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver uaudio_cd;
extern struct cfdriver udl_cd;
extern struct cfdriver umidi_cd;
extern struct cfdriver uhidev_cd;
extern struct cfdriver ukbd_cd;
extern struct cfdriver ums_cd;
extern struct cfdriver umt_cd;
extern struct cfdriver uts_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver aue_cd;
extern struct cfdriver axe_cd;
extern struct cfdriver axen_cd;
extern struct cfdriver smsc_cd;
extern struct cfdriver cue_cd;
extern struct cfdriver kue_cd;
extern struct cfdriver cdce_cd;
extern struct cfdriver mos_cd;
extern struct cfdriver mue_cd;
extern struct cfdriver udav_cd;
extern struct cfdriver upl_cd;
extern struct cfdriver ugl_cd;
extern struct cfdriver url_cd;
extern struct cfdriver ure_cd;
extern struct cfdriver atu_cd;
extern struct cfdriver ural_cd;
extern struct cfdriver rum_cd;
extern struct cfdriver run_cd;
extern struct cfdriver zyd_cd;
extern struct cfdriver upgt_cd;
extern struct cfdriver urtw_cd;
extern struct cfdriver urtwn_cd;
extern struct cfdriver rsu_cd;
extern struct cfdriver otus_cd;
extern struct cfdriver uath_cd;
extern struct cfdriver uwacom_cd;

extern struct cfattach audio_ca;
extern struct cfattach midi_ca;
extern struct cfattach drm_ca;
extern struct cfattach vscsi_ca;
extern struct cfattach softraid_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach cpu_ca;
extern struct cfattach intc_ca;
extern struct cfattach simplebus_ca;
extern struct cfattach plic_ca;
extern struct cfattach sfcc_ca;
extern struct cfattach sfclock_ca;
extern struct cfattach sfuart_ca;
extern struct cfattach vio_ca;
extern struct cfattach vioblk_ca;
extern struct cfattach viornd_ca;
extern struct cfattach vioscsi_ca;
extern struct cfattach iic_ca;
extern struct cfattach titmp_ca;
extern struct cfattach eephy_ca;
extern struct cfattach rlphy_ca;
extern struct cfattach sqphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach bmtphy_ca;
extern struct cfattach brgphy_ca;
extern struct cfattach amphy_ca;
extern struct cfattach acphy_ca;
extern struct cfattach urlphy_ca;
extern struct cfattach rgephy_ca;
extern struct cfattach atphy_ca;
extern struct cfattach pci_ca;
extern struct cfattach ahci_pci_ca;
extern struct cfattach nvme_pci_ca;
extern struct cfattach aq_ca;
extern struct cfattach ppb_ca;
extern struct cfattach em_ca;
extern struct cfattach ix_ca;
extern struct cfattach oce_ca;
extern struct cfattach xhci_pci_ca;
extern struct cfattach iwm_ca;
extern struct cfattach bge_ca;
extern struct cfattach virtio_pci_ca;
extern struct cfattach radeondrm_ca;
extern struct cfattach sdmmc_ca;
extern struct cfattach gfrtc_ca;
extern struct cfattach ociic_ca;
extern struct cfattach virtio_mmio_ca;
extern struct cfattach dwge_ca;
extern struct cfattach syscon_ca;
extern struct cfattach cad_ca;
extern struct cfattach dwmmc_ca;
extern struct cfattach dwpcie_ca;
extern struct cfattach com_fdt_ca;
extern struct cfattach dapmic_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach cd_ca;
extern struct cfattach ch_ca;
extern struct cfattach sd_ca;
extern struct cfattach uk_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach wsmouse_ca;
extern struct cfattach pciecam_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach uaudio_ca;
extern struct cfattach udl_ca;
extern struct cfattach umidi_ca;
extern struct cfattach uhidev_ca;
extern struct cfattach ukbd_ca;
extern struct cfattach ums_ca;
extern struct cfattach umt_ca;
extern struct cfattach uts_ca;
extern struct cfattach umass_ca;
extern struct cfattach aue_ca;
extern struct cfattach axe_ca;
extern struct cfattach axen_ca;
extern struct cfattach smsc_ca;
extern struct cfattach cue_ca;
extern struct cfattach kue_ca;
extern struct cfattach cdce_ca;
extern struct cfattach mos_ca;
extern struct cfattach mue_ca;
extern struct cfattach udav_ca;
extern struct cfattach upl_ca;
extern struct cfattach ugl_ca;
extern struct cfattach url_ca;
extern struct cfattach ure_ca;
extern struct cfattach wi_usb_ca;
extern struct cfattach atu_ca;
extern struct cfattach ural_ca;
extern struct cfattach rum_ca;
extern struct cfattach run_ca;
extern struct cfattach zyd_ca;
extern struct cfattach upgt_ca;
extern struct cfattach urtw_ca;
extern struct cfattach urtwn_ca;
extern struct cfattach rsu_ca;
extern struct cfattach otus_ca;
extern struct cfattach uath_ca;
extern struct cfattach athn_usb_ca;
extern struct cfattach uwacom_ca;
extern struct cfattach bwfm_usb_ca;


/* locators */
static long loc[10] = {
	-1, -1, -1, -1, -1, -1, -1, -1,
	1, 0,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"primary",
	"early",
	"addr",
	"size",
	"phy",
	"bus",
	"dev",
	"function",
	"target",
	"lun",
	"console",
	"mux",
	"port",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"reportid",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, -1, 1, -1, 1, -1, 2,
	3, -1, 4, -1, 4, -1, 4, -1,
	4, -1, 4, -1, 4, -1, 4, -1,
	4, -1, 4, -1, 4, -1, 4, -1,
	4, -1, 4, -1, 5, -1, 5, -1,
	5, -1, 6, 7, -1, 8, 9, -1,
	10, 0, 11, -1, 10, 11, -1, 11,
	-1, 11, -1, 11, -1, 11, -1, 12,
	13, 14, 15, 16, 17, -1, 12, 13,
	14, 15, 16, 17, -1, 18, -1,
};

/* size of parent vectors */
int pv_size = 71;

/* parent vectors */
short pv[71] = {
	88, 87, 84, 83, 82, 78, 77, 76, 75, 49, 47, 40, 33, -1, 4, 3,
	74, 43, 16, 14, 32, 31, -1, 71, 72, 73, 102, -1, 48, 8, 5, -1,
	51, 62, 34, -1, 64, 65, -1, 46, 41, -1, 42, 67, -1, 66, -1, 30,
	-1, 17, -1, 54, -1, 70, -1, 42, -1, 69, -1, 6, -1, 68, -1, 38,
	-1, 63, -1, 45, -1, 50, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: audio* at uaudio* */
    {&audio_ca,		&audio_cd,	 0, STAR,     loc,    0, pv+45, 0,    0},
/*  1: midi* at umidi* */
    {&midi_ca,		&midi_cd,	 0, STAR,     loc,    0, pv+61, 0,    0},
/*  2: drm* at radeondrm* primary -1 */
    {&drm_ca,		&drm_cd,	 0, STAR, loc+  5,    0, pv+55, 1,    0},
/*  3: vscsi0 at root */
    {&vscsi_ca,		&vscsi_cd,	 0, NORM,     loc,    0, pv+13, 0,    0},
/*  4: softraid0 at root */
    {&softraid_ca,	&softraid_cd,	 0, NORM,     loc,    0, pv+13, 0,    0},
/*  5: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+13, 0,    0},
/*  6: cpu0 at mainbus0 */
    {&cpu_ca,		&cpu_cd,	 0, NORM,     loc,    0, pv+30, 2,    0},
/*  7: intc0 at cpu0 */
    {&intc_ca,		&intc_cd,	 0, NORM,     loc,    0, pv+59, 2,    0},
/*  8: simplebus* at syscon*|simplebus*|mainbus0 early 0 */
    {&simplebus_ca,	&simplebus_cd,	 0, STAR, loc+  9,    0, pv+28, 3,    0},
/*  9: plic* at syscon*|simplebus*|mainbus0 early 1 */
    {&plic_ca,		&plic_cd,	 0, STAR, loc+  8,    0, pv+28, 3,    0},
/* 10: sfcc* at syscon*|simplebus*|mainbus0 early 1 */
    {&sfcc_ca,		&sfcc_cd,	 0, STAR, loc+  8,    0, pv+28, 3,    0},
/* 11: sfclock* at syscon*|simplebus*|mainbus0 early 1 */
    {&sfclock_ca,	&sfclock_cd,	 0, STAR, loc+  8,    0, pv+28, 3,    0},
/* 12: sfuart* at syscon*|simplebus*|mainbus0 early 0 */
    {&sfuart_ca,	&sfuart_cd,	 0, STAR, loc+  9,    0, pv+28, 3,    0},
/* 13: vio* at virtio*|virtio* */
    {&vio_ca,		&vio_cd,	 0, STAR,     loc,    0, pv+39, 6,    0},
/* 14: vioblk* at virtio*|virtio* */
    {&vioblk_ca,	&vioblk_cd,	 0, STAR,     loc,    0, pv+39, 6,    0},
/* 15: viornd* at virtio*|virtio* */
    {&viornd_ca,	&viornd_cd,	 0, STAR,     loc,    0, pv+39, 6,    0},
/* 16: vioscsi* at virtio*|virtio* */
    {&vioscsi_ca,	&vioscsi_cd,	 0, STAR,     loc,    0, pv+39, 6,    0},
/* 17: iic* at ociic* */
    {&iic_ca,		&iic_cd,	 0, STAR,     loc,    0, pv+67, 6,    0},
/* 18: titmp* at iic* addr -1 size -1 */
    {&titmp_ca,		&titmp_cd,	 0, STAR, loc+  4,    0, pv+49, 7,    0},
/* 19: eephy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|cad*|dwge*|bge*|aq* phy -1 */
    {&eephy_ca,		&eephy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 10,    0},
/* 20: rlphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|cad*|dwge*|bge*|aq* phy -1 */
    {&rlphy_ca,		&rlphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 10,    0},
/* 21: sqphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|cad*|dwge*|bge*|aq* phy -1 */
    {&sqphy_ca,		&sqphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 10,    0},
/* 22: ukphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|cad*|dwge*|bge*|aq* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 10,    0},
/* 23: bmtphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|cad*|dwge*|bge*|aq* phy -1 */
    {&bmtphy_ca,	&bmtphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 10,    0},
/* 24: brgphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|cad*|dwge*|bge*|aq* phy -1 */
    {&brgphy_ca,	&brgphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 10,    0},
/* 25: amphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|cad*|dwge*|bge*|aq* phy -1 */
    {&amphy_ca,		&amphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 10,    0},
/* 26: acphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|cad*|dwge*|bge*|aq* phy -1 */
    {&acphy_ca,		&acphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 10,    0},
/* 27: urlphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|cad*|dwge*|bge*|aq* phy -1 */
    {&urlphy_ca,	&urlphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 10,    0},
/* 28: rgephy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|cad*|dwge*|bge*|aq* phy -1 */
    {&rgephy_ca,	&rgephy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 10,    0},
/* 29: atphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|cad*|dwge*|bge*|aq* phy -1 */
    {&atphy_ca,		&atphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 10,    0},
/* 30: pci* at dwpcie*|pciecam*|ppb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+  5,    0, pv+32, 36,    0},
/* 31: ahci* at pci* dev -1 function -1 */
    {&ahci_pci_ca,	&ahci_cd,	 0, STAR, loc+  4,    0, pv+47, 42,    0},
/* 32: nvme* at pci* dev -1 function -1 */
    {&nvme_pci_ca,	&nvme_cd,	 0, STAR, loc+  4,    0, pv+47, 42,    0},
/* 33: aq* at pci* dev -1 function -1 */
    {&aq_ca,		&aq_cd,		 0, STAR, loc+  4,    0, pv+47, 42,    0},
/* 34: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+  4,    0, pv+47, 42,    0},
/* 35: em* at pci* dev -1 function -1 */
    {&em_ca,		&em_cd,		 0, STAR, loc+  4,    0, pv+47, 42,    0},
/* 36: ix* at pci* dev -1 function -1 */
    {&ix_ca,		&ix_cd,		 0, STAR, loc+  4,    0, pv+47, 42,    0},
/* 37: oce* at pci* dev -1 function -1 */
    {&oce_ca,		&oce_cd,	 0, STAR, loc+  4,    0, pv+47, 42,    0},
/* 38: xhci* at pci* dev -1 function -1 */
    {&xhci_pci_ca,	&xhci_cd,	 0, STAR, loc+  4,    0, pv+47, 42,    0},
/* 39: iwm* at pci* dev -1 function -1 */
    {&iwm_ca,		&iwm_cd,	 0, STAR, loc+  4,    0, pv+47, 42,    0},
/* 40: bge* at pci* dev -1 function -1 */
    {&bge_ca,		&bge_cd,	 0, STAR, loc+  4,    0, pv+47, 42,    0},
/* 41: virtio* at pci* dev -1 function -1 */
    {&virtio_pci_ca,	&virtio_cd,	 0, STAR, loc+  4,    0, pv+47, 42,    0},
/* 42: radeondrm* at pci* dev -1 function -1 */
    {&radeondrm_ca,	&radeondrm_cd,	 0, STAR, loc+  4,    0, pv+47, 42,    0},
/* 43: sdmmc* at dwmmc* */
    {&sdmmc_ca,		&sdmmc_cd,	 0, STAR,     loc,    0, pv+69, 44,    0},
/* 44: gfrtc* at syscon*|simplebus*|mainbus0 early 0 */
    {&gfrtc_ca,		&gfrtc_cd,	 0, STAR, loc+  9,    0, pv+28, 3,    0},
/* 45: ociic* at syscon*|simplebus*|mainbus0 early 0 */
    {&ociic_ca,		&ociic_cd,	 0, STAR, loc+  9,    0, pv+28, 3,    0},
/* 46: virtio* at syscon*|simplebus*|mainbus0 early 0 */
    {&virtio_mmio_ca,	&virtio_cd,	 0, STAR, loc+  9,    0, pv+28, 3,    0},
/* 47: dwge* at syscon*|simplebus*|mainbus0 early 0 */
    {&dwge_ca,		&dwge_cd,	 0, STAR, loc+  9,    0, pv+28, 3,    0},
/* 48: syscon* at syscon*|simplebus*|mainbus0 early 1 */
    {&syscon_ca,	&syscon_cd,	 0, STAR, loc+  8,    0, pv+28, 3,    0},
/* 49: cad* at syscon*|simplebus*|mainbus0 early 0 */
    {&cad_ca,		&cad_cd,	 0, STAR, loc+  9,    0, pv+28, 3,    0},
/* 50: dwmmc* at syscon*|simplebus*|mainbus0 early 0 */
    {&dwmmc_ca,		&dwmmc_cd,	 0, STAR, loc+  9,    0, pv+28, 3,    0},
/* 51: dwpcie* at syscon*|simplebus*|mainbus0 early 0 */
    {&dwpcie_ca,	&dwpcie_cd,	 0, STAR, loc+  9,    0, pv+28, 3,    0},
/* 52: com* at syscon*|simplebus*|mainbus0 early 0 */
    {&com_fdt_ca,	&com_cd,	 0, STAR, loc+  9,    0, pv+28, 3,    0},
/* 53: dapmic* at iic* addr -1 size -1 */
    {&dapmic_ca,	&dapmic_cd,	 0, STAR, loc+  4,    0, pv+49, 7,    0},
/* 54: scsibus* at softraid0|vscsi0|umass*|sdmmc*|vioscsi*|vioblk*|nvme*|ahci* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+14, 44,    0},
/* 55: cd* at scsibus* target -1 lun -1 */
    {&cd_ca,		&cd_cd,		 0, STAR, loc+  4,    0, pv+51, 45,    0},
/* 56: ch* at scsibus* target -1 lun -1 */
    {&ch_ca,		&ch_cd,		 0, STAR, loc+  4,    0, pv+51, 45,    0},
/* 57: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+  4,    0, pv+51, 45,    0},
/* 58: uk* at scsibus* target -1 lun -1 */
    {&uk_ca,		&uk_cd,		 0, STAR, loc+  4,    0, pv+51, 45,    0},
/* 59: wsdisplay* at radeondrm*|udl* console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+  6,    0, pv+42, 1,    0},
/* 60: wskbd* at ukbd* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+  7,    0, pv+53, 52,    0},
/* 61: wsmouse* at ums*|umt*|uts*|uwacom* mux 0 */
    {&wsmouse_ca,	&wsmouse_cd,	 0, STAR, loc+  9,    0, pv+23, 55,    0},
/* 62: pciecam* at syscon*|simplebus*|mainbus0 early 0 */
    {&pciecam_ca,	&pciecam_cd,	 0, STAR, loc+  9,    0, pv+28, 3,    0},
/* 63: usb* at xhci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+63, 62,    0},
/* 64: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+65, 62,    0},
/* 65: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 66: uaudio* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uaudio_ca,	&uaudio_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 67: udl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udl_ca,		&udl_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 68: umidi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umidi_ca,		&umidi_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 69: uhidev* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhidev_ca,	&uhidev_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 70: ukbd* at uhidev* reportid -1 */
    {&ukbd_ca,		&ukbd_cd,	 0, STAR, loc+  5,    0, pv+57, 77,    0},
/* 71: ums* at uhidev* reportid -1 */
    {&ums_ca,		&ums_cd,	 0, STAR, loc+  5,    0, pv+57, 77,    0},
/* 72: umt* at uhidev* reportid -1 */
    {&umt_ca,		&umt_cd,	 0, STAR, loc+  5,    0, pv+57, 77,    0},
/* 73: uts* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uts_ca,		&uts_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 74: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 75: aue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&aue_ca,		&aue_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 76: axe* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axe_ca,		&axe_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 77: axen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axen_ca,		&axen_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 78: smsc* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&smsc_ca,		&smsc_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 79: cue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cue_ca,		&cue_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 80: kue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&kue_ca,		&kue_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 81: cdce* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cdce_ca,		&cdce_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 82: mos* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mos_ca,		&mos_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 83: mue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mue_ca,		&mue_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 84: udav* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udav_ca,		&udav_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 85: upl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upl_ca,		&upl_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 86: ugl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugl_ca,		&ugl_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 87: url* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&url_ca,		&url_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 88: ure* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ure_ca,		&ure_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 89: wi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&wi_usb_ca,	&wi_cd,		 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 90: atu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&atu_ca,		&atu_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 91: ural* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ural_ca,		&ural_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 92: rum* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rum_ca,		&rum_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 93: run* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&run_ca,		&run_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 94: zyd* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&zyd_ca,		&zyd_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 95: upgt* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upgt_ca,		&upgt_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 96: urtw* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtw_ca,		&urtw_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 97: urtwn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtwn_ca,		&urtwn_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 98: rsu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rsu_ca,		&rsu_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/* 99: otus* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&otus_ca,		&otus_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/*100: uath* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uath_ca,		&uath_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/*101: athn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&athn_usb_ca,	&athn_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
/*102: uwacom* at uhidev* reportid -1 */
    {&uwacom_ca,	&uwacom_cd,	 0, STAR, loc+  5,    0, pv+57, 77,    0},
/*103: bwfm* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&bwfm_usb_ca,	&bwfm_cd,	 0, STAR, loc+  0,    0, pv+36, 63,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 3 /* vscsi0 */,
	 4 /* softraid0 */,
	 5 /* mainbus0 */,
	-1
};

int cfroots_size = 4;

/* pseudo-devices */
extern void pfattach(int);
extern void pflogattach(int);
extern void pfsyncattach(int);
extern void pflowattach(int);
extern void encattach(int);
extern void ptyattach(int);
extern void nmeaattach(int);
extern void mstsattach(int);
extern void endrunattach(int);
extern void vndattach(int);
extern void ksymsattach(int);
extern void bpfilterattach(int);
extern void bridgeattach(int);
extern void vebattach(int);
extern void carpattach(int);
extern void etheripattach(int);
extern void gifattach(int);
extern void greattach(int);
extern void loopattach(int);
extern void mpeattach(int);
extern void mpwattach(int);
extern void mpipattach(int);
extern void bpeattach(int);
extern void pairattach(int);
extern void pppattach(int);
extern void pppoeattach(int);
extern void pppxattach(int);
extern void spppattach(int);
extern void trunkattach(int);
extern void aggrattach(int);
extern void tpmrattach(int);
extern void tunattach(int);
extern void vetherattach(int);
extern void vxlanattach(int);
extern void vlanattach(int);
extern void switchattach(int);
extern void wgattach(int);
extern void bioattach(int);
extern void fuseattach(int);
extern void openpromattach(int);
extern void hotplugattach(int);
extern void wsmuxattach(int);

char *pdevnames[] = {
	"pf",
	"pflog",
	"pfsync",
	"pflow",
	"enc",
	"pty",
	"nmea",
	"msts",
	"endrun",
	"vnd",
	"ksyms",
	"bpfilter",
	"bridge",
	"veb",
	"carp",
	"etherip",
	"gif",
	"gre",
	"loop",
	"mpe",
	"mpw",
	"mpip",
	"bpe",
	"pair",
	"ppp",
	"pppoe",
	"pppx",
	"sppp",
	"trunk",
	"aggr",
	"tpmr",
	"tun",
	"vether",
	"vxlan",
	"vlan",
	"switch",
	"wg",
	"bio",
	"fuse",
	"openprom",
	"hotplug",
	"wsmux",
};

int pdevnames_size = 42;

struct pdevinit pdevinit[] = {
	{ pfattach, 1 },
	{ pflogattach, 1 },
	{ pfsyncattach, 1 },
	{ pflowattach, 1 },
	{ encattach, 1 },
	{ ptyattach, 16 },
	{ nmeaattach, 1 },
	{ mstsattach, 1 },
	{ endrunattach, 1 },
	{ vndattach, 4 },
	{ ksymsattach, 1 },
	{ bpfilterattach, 1 },
	{ bridgeattach, 1 },
	{ vebattach, 1 },
	{ carpattach, 1 },
	{ etheripattach, 1 },
	{ gifattach, 1 },
	{ greattach, 1 },
	{ loopattach, 1 },
	{ mpeattach, 1 },
	{ mpwattach, 1 },
	{ mpipattach, 1 },
	{ bpeattach, 1 },
	{ pairattach, 1 },
	{ pppattach, 1 },
	{ pppoeattach, 1 },
	{ pppxattach, 1 },
	{ spppattach, 1 },
	{ trunkattach, 1 },
	{ aggrattach, 1 },
	{ tpmrattach, 1 },
	{ tunattach, 1 },
	{ vetherattach, 1 },
	{ vxlanattach, 1 },
	{ vlanattach, 1 },
	{ switchattach, 1 },
	{ wgattach, 1 },
	{ bioattach, 1 },
	{ fuseattach, 1 },
	{ openpromattach, 1 },
	{ hotplugattach, 1 },
	{ wsmuxattach, 2 },
	{ NULL, 0 }
};
