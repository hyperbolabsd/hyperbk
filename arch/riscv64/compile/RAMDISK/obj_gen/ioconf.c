/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/riscv64/conf/RAMDISK"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver ahci_cd;
extern struct cfdriver nvme_cd;
extern struct cfdriver wi_cd;
extern struct cfdriver com_cd;
extern struct cfdriver athn_cd;
extern struct cfdriver bwfm_cd;
extern struct cfdriver virtio_cd;
extern struct cfdriver xhci_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver intc_cd;
extern struct cfdriver simplebus_cd;
extern struct cfdriver plic_cd;
extern struct cfdriver sfcc_cd;
extern struct cfdriver sfclock_cd;
extern struct cfdriver sfuart_cd;
extern struct cfdriver vio_cd;
extern struct cfdriver vioblk_cd;
extern struct cfdriver viornd_cd;
extern struct cfdriver vioscsi_cd;
extern struct cfdriver iic_cd;
extern struct cfdriver eephy_cd;
extern struct cfdriver rlphy_cd;
extern struct cfdriver sqphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver bmtphy_cd;
extern struct cfdriver brgphy_cd;
extern struct cfdriver amphy_cd;
extern struct cfdriver acphy_cd;
extern struct cfdriver urlphy_cd;
extern struct cfdriver rgephy_cd;
extern struct cfdriver atphy_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver aq_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver em_cd;
extern struct cfdriver ix_cd;
extern struct cfdriver oce_cd;
extern struct cfdriver iwm_cd;
extern struct cfdriver bge_cd;
extern struct cfdriver sdmmc_cd;
extern struct cfdriver gfrtc_cd;
extern struct cfdriver ociic_cd;
extern struct cfdriver dwge_cd;
extern struct cfdriver syscon_cd;
extern struct cfdriver cad_cd;
extern struct cfdriver dwmmc_cd;
extern struct cfdriver dwpcie_cd;
extern struct cfdriver dapmic_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver cd_cd;
extern struct cfdriver ch_cd;
extern struct cfdriver uk_cd;
extern struct cfdriver pciecam_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver aue_cd;
extern struct cfdriver axe_cd;
extern struct cfdriver axen_cd;
extern struct cfdriver smsc_cd;
extern struct cfdriver cue_cd;
extern struct cfdriver kue_cd;
extern struct cfdriver cdce_cd;
extern struct cfdriver mos_cd;
extern struct cfdriver mue_cd;
extern struct cfdriver udav_cd;
extern struct cfdriver upl_cd;
extern struct cfdriver ugl_cd;
extern struct cfdriver url_cd;
extern struct cfdriver ure_cd;
extern struct cfdriver atu_cd;
extern struct cfdriver ural_cd;
extern struct cfdriver rum_cd;
extern struct cfdriver run_cd;
extern struct cfdriver zyd_cd;
extern struct cfdriver upgt_cd;
extern struct cfdriver urtw_cd;
extern struct cfdriver urtwn_cd;
extern struct cfdriver rsu_cd;
extern struct cfdriver otus_cd;
extern struct cfdriver uath_cd;

extern struct cfattach mainbus_ca;
extern struct cfattach cpu_ca;
extern struct cfattach intc_ca;
extern struct cfattach simplebus_ca;
extern struct cfattach plic_ca;
extern struct cfattach sfcc_ca;
extern struct cfattach sfclock_ca;
extern struct cfattach sfuart_ca;
extern struct cfattach vio_ca;
extern struct cfattach vioblk_ca;
extern struct cfattach viornd_ca;
extern struct cfattach vioscsi_ca;
extern struct cfattach iic_ca;
extern struct cfattach eephy_ca;
extern struct cfattach rlphy_ca;
extern struct cfattach sqphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach bmtphy_ca;
extern struct cfattach brgphy_ca;
extern struct cfattach amphy_ca;
extern struct cfattach acphy_ca;
extern struct cfattach urlphy_ca;
extern struct cfattach rgephy_ca;
extern struct cfattach atphy_ca;
extern struct cfattach pci_ca;
extern struct cfattach ahci_pci_ca;
extern struct cfattach nvme_pci_ca;
extern struct cfattach aq_ca;
extern struct cfattach ppb_ca;
extern struct cfattach em_ca;
extern struct cfattach ix_ca;
extern struct cfattach oce_ca;
extern struct cfattach xhci_pci_ca;
extern struct cfattach iwm_ca;
extern struct cfattach bge_ca;
extern struct cfattach virtio_pci_ca;
extern struct cfattach sdmmc_ca;
extern struct cfattach gfrtc_ca;
extern struct cfattach ociic_ca;
extern struct cfattach virtio_mmio_ca;
extern struct cfattach dwge_ca;
extern struct cfattach syscon_ca;
extern struct cfattach cad_ca;
extern struct cfattach dwmmc_ca;
extern struct cfattach dwpcie_ca;
extern struct cfattach com_fdt_ca;
extern struct cfattach dapmic_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach cd_ca;
extern struct cfattach ch_ca;
extern struct cfattach sd_ca;
extern struct cfattach uk_ca;
extern struct cfattach pciecam_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach umass_ca;
extern struct cfattach aue_ca;
extern struct cfattach axe_ca;
extern struct cfattach axen_ca;
extern struct cfattach smsc_ca;
extern struct cfattach cue_ca;
extern struct cfattach kue_ca;
extern struct cfattach cdce_ca;
extern struct cfattach mos_ca;
extern struct cfattach mue_ca;
extern struct cfattach udav_ca;
extern struct cfattach upl_ca;
extern struct cfattach ugl_ca;
extern struct cfattach url_ca;
extern struct cfattach ure_ca;
extern struct cfattach wi_usb_ca;
extern struct cfattach atu_ca;
extern struct cfattach ural_ca;
extern struct cfattach rum_ca;
extern struct cfattach run_ca;
extern struct cfattach zyd_ca;
extern struct cfattach upgt_ca;
extern struct cfattach urtw_ca;
extern struct cfattach urtwn_ca;
extern struct cfattach rsu_ca;
extern struct cfattach otus_ca;
extern struct cfattach uath_ca;
extern struct cfattach athn_usb_ca;
extern struct cfattach bwfm_usb_ca;


/* locators */
static long loc[8] = {
	-1, -1, -1, -1, -1, -1, 0, 1,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"early",
	"phy",
	"bus",
	"dev",
	"function",
	"addr",
	"size",
	"target",
	"lun",
	"port",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, -1, 0, -1, 0, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 2, -1, 2, -1, 2, -1, 3,
	4, -1, 5, 6, -1, 7, 8, -1,
	9, 10, 11, 12, 13, 14, -1, 9,
	10, 11, 12, 13, 14, -1,
};

/* size of parent vectors */
int pv_size = 51;

/* parent vectors */
short pv[51] = {
	70, 69, 66, 65, 64, 60, 59, 58, 57, 42, 40, 34, 27, -1, 56, 36,
	11, 9, 26, 25, -1, 44, 52, 28, -1, 41, 3, 0, -1, 54, 55, -1,
	39, 35, -1, 53, -1, 24, -1, 47, -1, 12, -1, 32, -1, 43, -1, 38,
	-1, 1, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+13, 0,    0},
/*  1: cpu0 at mainbus0 */
    {&cpu_ca,		&cpu_cd,	 0, NORM,     loc,    0, pv+27, 5,    0},
/*  2: intc0 at cpu0 */
    {&intc_ca,		&intc_cd,	 0, NORM,     loc,    0, pv+49, 0,    0},
/*  3: simplebus* at syscon*|simplebus*|mainbus0 early 0 */
    {&simplebus_ca,	&simplebus_cd,	 0, STAR, loc+  6,    0, pv+25, 1,    0},
/*  4: plic* at syscon*|simplebus*|mainbus0 early 1 */
    {&plic_ca,		&plic_cd,	 0, STAR, loc+  7,    0, pv+25, 1,    0},
/*  5: sfcc* at syscon*|simplebus*|mainbus0 early 1 */
    {&sfcc_ca,		&sfcc_cd,	 0, STAR, loc+  7,    0, pv+25, 1,    0},
/*  6: sfclock* at syscon*|simplebus*|mainbus0 early 1 */
    {&sfclock_ca,	&sfclock_cd,	 0, STAR, loc+  7,    0, pv+25, 1,    0},
/*  7: sfuart* at syscon*|simplebus*|mainbus0 early 0 */
    {&sfuart_ca,	&sfuart_cd,	 0, STAR, loc+  6,    0, pv+25, 1,    0},
/*  8: vio* at virtio*|virtio* */
    {&vio_ca,		&vio_cd,	 0, STAR,     loc,    0, pv+32, 6,    0},
/*  9: vioblk* at virtio*|virtio* */
    {&vioblk_ca,	&vioblk_cd,	 0, STAR,     loc,    0, pv+32, 6,    0},
/* 10: viornd* at virtio*|virtio* */
    {&viornd_ca,	&viornd_cd,	 0, STAR,     loc,    0, pv+32, 6,    0},
/* 11: vioscsi* at virtio*|virtio* */
    {&vioscsi_ca,	&vioscsi_cd,	 0, STAR,     loc,    0, pv+32, 6,    0},
/* 12: iic* at ociic* */
    {&iic_ca,		&iic_cd,	 0, STAR,     loc,    0, pv+47, 6,    0},
/* 13: eephy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|cad*|dwge*|bge*|aq* phy -1 */
    {&eephy_ca,		&eephy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 7,    0},
/* 14: rlphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|cad*|dwge*|bge*|aq* phy -1 */
    {&rlphy_ca,		&rlphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 7,    0},
/* 15: sqphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|cad*|dwge*|bge*|aq* phy -1 */
    {&sqphy_ca,		&sqphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 7,    0},
/* 16: ukphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|cad*|dwge*|bge*|aq* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 7,    0},
/* 17: bmtphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|cad*|dwge*|bge*|aq* phy -1 */
    {&bmtphy_ca,	&bmtphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 7,    0},
/* 18: brgphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|cad*|dwge*|bge*|aq* phy -1 */
    {&brgphy_ca,	&brgphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 7,    0},
/* 19: amphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|cad*|dwge*|bge*|aq* phy -1 */
    {&amphy_ca,		&amphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 7,    0},
/* 20: acphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|cad*|dwge*|bge*|aq* phy -1 */
    {&acphy_ca,		&acphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 7,    0},
/* 21: urlphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|cad*|dwge*|bge*|aq* phy -1 */
    {&urlphy_ca,	&urlphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 7,    0},
/* 22: rgephy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|cad*|dwge*|bge*|aq* phy -1 */
    {&rgephy_ca,	&rgephy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 7,    0},
/* 23: atphy* at ure*|url*|udav*|mue*|mos*|smsc*|axen*|axe*|aue*|cad*|dwge*|bge*|aq* phy -1 */
    {&atphy_ca,		&atphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 7,    0},
/* 24: pci* at dwpcie*|pciecam*|ppb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+  5,    0, pv+21, 33,    0},
/* 25: ahci* at pci* dev -1 function -1 */
    {&ahci_pci_ca,	&ahci_cd,	 0, STAR, loc+  4,    0, pv+37, 39,    0},
/* 26: nvme* at pci* dev -1 function -1 */
    {&nvme_pci_ca,	&nvme_cd,	 0, STAR, loc+  4,    0, pv+37, 39,    0},
/* 27: aq* at pci* dev -1 function -1 */
    {&aq_ca,		&aq_cd,		 0, STAR, loc+  4,    0, pv+37, 39,    0},
/* 28: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+  4,    0, pv+37, 39,    0},
/* 29: em* at pci* dev -1 function -1 */
    {&em_ca,		&em_cd,		 0, STAR, loc+  4,    0, pv+37, 39,    0},
/* 30: ix* at pci* dev -1 function -1 */
    {&ix_ca,		&ix_cd,		 0, STAR, loc+  4,    0, pv+37, 39,    0},
/* 31: oce* at pci* dev -1 function -1 */
    {&oce_ca,		&oce_cd,	 0, STAR, loc+  4,    0, pv+37, 39,    0},
/* 32: xhci* at pci* dev -1 function -1 */
    {&xhci_pci_ca,	&xhci_cd,	 0, STAR, loc+  4,    0, pv+37, 39,    0},
/* 33: iwm* at pci* dev -1 function -1 */
    {&iwm_ca,		&iwm_cd,	 0, STAR, loc+  4,    0, pv+37, 39,    0},
/* 34: bge* at pci* dev -1 function -1 */
    {&bge_ca,		&bge_cd,	 0, STAR, loc+  4,    0, pv+37, 39,    0},
/* 35: virtio* at pci* dev -1 function -1 */
    {&virtio_pci_ca,	&virtio_cd,	 0, STAR, loc+  4,    0, pv+37, 39,    0},
/* 36: sdmmc* at dwmmc* */
    {&sdmmc_ca,		&sdmmc_cd,	 0, STAR,     loc,    0, pv+45, 41,    0},
/* 37: gfrtc* at syscon*|simplebus*|mainbus0 early 0 */
    {&gfrtc_ca,		&gfrtc_cd,	 0, STAR, loc+  6,    0, pv+25, 1,    0},
/* 38: ociic* at syscon*|simplebus*|mainbus0 early 0 */
    {&ociic_ca,		&ociic_cd,	 0, STAR, loc+  6,    0, pv+25, 1,    0},
/* 39: virtio* at syscon*|simplebus*|mainbus0 early 0 */
    {&virtio_mmio_ca,	&virtio_cd,	 0, STAR, loc+  6,    0, pv+25, 1,    0},
/* 40: dwge* at syscon*|simplebus*|mainbus0 early 0 */
    {&dwge_ca,		&dwge_cd,	 0, STAR, loc+  6,    0, pv+25, 1,    0},
/* 41: syscon* at syscon*|simplebus*|mainbus0 early 1 */
    {&syscon_ca,	&syscon_cd,	 0, STAR, loc+  7,    0, pv+25, 1,    0},
/* 42: cad* at syscon*|simplebus*|mainbus0 early 0 */
    {&cad_ca,		&cad_cd,	 0, STAR, loc+  6,    0, pv+25, 1,    0},
/* 43: dwmmc* at syscon*|simplebus*|mainbus0 early 0 */
    {&dwmmc_ca,		&dwmmc_cd,	 0, STAR, loc+  6,    0, pv+25, 1,    0},
/* 44: dwpcie* at syscon*|simplebus*|mainbus0 early 0 */
    {&dwpcie_ca,	&dwpcie_cd,	 0, STAR, loc+  6,    0, pv+25, 1,    0},
/* 45: com* at syscon*|simplebus*|mainbus0 early 0 */
    {&com_fdt_ca,	&com_cd,	 0, STAR, loc+  6,    0, pv+25, 1,    0},
/* 46: dapmic* at iic* addr -1 size -1 */
    {&dapmic_ca,	&dapmic_cd,	 0, STAR, loc+  4,    0, pv+41, 42,    0},
/* 47: scsibus* at umass*|sdmmc*|vioscsi*|vioblk*|nvme*|ahci* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+14, 44,    0},
/* 48: cd* at scsibus* target -1 lun -1 */
    {&cd_ca,		&cd_cd,		 0, STAR, loc+  4,    0, pv+39, 45,    0},
/* 49: ch* at scsibus* target -1 lun -1 */
    {&ch_ca,		&ch_cd,		 0, STAR, loc+  4,    0, pv+39, 45,    0},
/* 50: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+  4,    0, pv+39, 45,    0},
/* 51: uk* at scsibus* target -1 lun -1 */
    {&uk_ca,		&uk_cd,		 0, STAR, loc+  4,    0, pv+39, 45,    0},
/* 52: pciecam* at syscon*|simplebus*|mainbus0 early 0 */
    {&pciecam_ca,	&pciecam_cd,	 0, STAR, loc+  6,    0, pv+25, 1,    0},
/* 53: usb* at xhci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+43, 47,    0},
/* 54: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+35, 47,    0},
/* 55: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 56: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 57: aue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&aue_ca,		&aue_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 58: axe* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axe_ca,		&axe_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 59: axen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axen_ca,		&axen_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 60: smsc* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&smsc_ca,		&smsc_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 61: cue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cue_ca,		&cue_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 62: kue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&kue_ca,		&kue_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 63: cdce* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cdce_ca,		&cdce_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 64: mos* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mos_ca,		&mos_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 65: mue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mue_ca,		&mue_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 66: udav* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udav_ca,		&udav_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 67: upl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upl_ca,		&upl_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 68: ugl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugl_ca,		&ugl_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 69: url* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&url_ca,		&url_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 70: ure* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ure_ca,		&ure_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 71: wi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&wi_usb_ca,	&wi_cd,		 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 72: atu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&atu_ca,		&atu_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 73: ural* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ural_ca,		&ural_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 74: rum* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rum_ca,		&rum_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 75: run* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&run_ca,		&run_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 76: zyd* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&zyd_ca,		&zyd_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 77: upgt* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upgt_ca,		&upgt_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 78: urtw* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtw_ca,		&urtw_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 79: urtwn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtwn_ca,		&urtwn_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 80: rsu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rsu_ca,		&rsu_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 81: otus* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&otus_ca,		&otus_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 82: uath* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uath_ca,		&uath_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 83: athn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&athn_usb_ca,	&athn_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
/* 84: bwfm* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&bwfm_usb_ca,	&bwfm_cd,	 0, STAR, loc+  0,    0, pv+29, 48,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 0 /* mainbus0 */,
	-1
};

int cfroots_size = 2;

/* pseudo-devices */
extern void loopattach(int);
extern void vlanattach(int);
extern void trunkattach(int);
extern void bpfilterattach(int);
extern void rdattach(int);
extern void bioattach(int);

char *pdevnames[] = {
	"loop",
	"vlan",
	"trunk",
	"bpfilter",
	"rd",
	"bio",
};

int pdevnames_size = 6;

struct pdevinit pdevinit[] = {
	{ loopattach, 1 },
	{ vlanattach, 1 },
	{ trunkattach, 1 },
	{ bpfilterattach, 1 },
	{ rdattach, 1 },
	{ bioattach, 1 },
	{ NULL, 0 }
};
