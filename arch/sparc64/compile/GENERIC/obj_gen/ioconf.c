/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/sparc64/conf/GENERIC"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver video_cd;
extern struct cfdriver audio_cd;
extern struct cfdriver midi_cd;
extern struct cfdriver drm_cd;
extern struct cfdriver wdc_cd;
extern struct cfdriver qlw_cd;
extern struct cfdriver qla_cd;
extern struct cfdriver ahci_cd;
extern struct cfdriver nvme_cd;
extern struct cfdriver mpi_cd;
extern struct cfdriver sili_cd;
extern struct cfdriver esp_cd;
extern struct cfdriver siop_cd;
extern struct cfdriver ep_cd;
extern struct cfdriver wi_cd;
extern struct cfdriver le_cd;
extern struct cfdriver xl_cd;
extern struct cfdriver fxp_cd;
extern struct cfdriver rl_cd;
extern struct cfdriver re_cd;
extern struct cfdriver dc_cd;
extern struct cfdriver epic_cd;
extern struct cfdriver ne_cd;
extern struct cfdriver gem_cd;
extern struct cfdriver ti_cd;
extern struct cfdriver com_cd;
extern struct cfdriver pckbc_cd;
extern struct cfdriver pcfiic_cd;
extern struct cfdriver lpt_cd;
extern struct cfdriver lm_cd;
extern struct cfdriver ath_cd;
extern struct cfdriver athn_cd;
extern struct cfdriver ral_cd;
extern struct cfdriver malo_cd;
extern struct cfdriver uhci_cd;
extern struct cfdriver ohci_cd;
extern struct cfdriver ehci_cd;
extern struct cfdriver xhci_cd;
extern struct cfdriver wbsd_cd;
extern struct cfdriver radio_cd;
extern struct cfdriver vscsi_cd;
extern struct cfdriver mpath_cd;
extern struct cfdriver softraid_cd;
extern struct cfdriver spdmem_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver ssm_cd;
extern struct cfdriver upa_cd;
extern struct cfdriver central_cd;
extern struct cfdriver fhc_cd;
extern struct cfdriver clkbrd_cd;
extern struct cfdriver vbus_cd;
extern struct cfdriver cbus_cd;
extern struct cfdriver hme_cd;
extern struct cfdriver uperf_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver wsmouse_cd;
extern struct cfdriver pcons_cd;
extern struct cfdriver prtc_cd;
extern struct cfdriver sbus_cd;
extern struct cfdriver dma_cd;
extern struct cfdriver ledma_cd;
extern struct cfdriver lebuffer_cd;
extern struct cfdriver qec_cd;
extern struct cfdriver be_cd;
extern struct cfdriver qe_cd;
extern struct cfdriver audiocs_cd;
extern struct cfdriver agten_cd;
extern struct cfdriver bwtwo_cd;
extern struct cfdriver cgsix_cd;
extern struct cfdriver cgthree_cd;
extern struct cfdriver cgtwelve_cd;
extern struct cfdriver mgx_cd;
extern struct cfdriver rfx_cd;
extern struct cfdriver tvtwo_cd;
extern struct cfdriver vigra_cd;
extern struct cfdriver zx_cd;
extern struct cfdriver magma_cd;
extern struct cfdriver mtty_cd;
extern struct cfdriver mbpp_cd;
extern struct cfdriver spif_cd;
extern struct cfdriver stty_cd;
extern struct cfdriver sbpp_cd;
extern struct cfdriver asio_cd;
extern struct cfdriver apio_cd;
extern struct cfdriver stp_cd;
extern struct cfdriver xbox_cd;
extern struct cfdriver bpp_cd;
extern struct cfdriver nsphy_cd;
extern struct cfdriver nsphyter_cd;
extern struct cfdriver gentbi_cd;
extern struct cfdriver qsphy_cd;
extern struct cfdriver inphy_cd;
extern struct cfdriver iophy_cd;
extern struct cfdriver eephy_cd;
extern struct cfdriver exphy_cd;
extern struct cfdriver rlphy_cd;
extern struct cfdriver lxtphy_cd;
extern struct cfdriver luphy_cd;
extern struct cfdriver icsphy_cd;
extern struct cfdriver sqphy_cd;
extern struct cfdriver tqphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver dcphy_cd;
extern struct cfdriver bmtphy_cd;
extern struct cfdriver brgphy_cd;
extern struct cfdriver xmphy_cd;
extern struct cfdriver amphy_cd;
extern struct cfdriver acphy_cd;
extern struct cfdriver nsgphy_cd;
extern struct cfdriver urlphy_cd;
extern struct cfdriver rgephy_cd;
extern struct cfdriver ipgphy_cd;
extern struct cfdriver jmphy_cd;
extern struct cfdriver atapiscsi_cd;
extern struct cfdriver wd_cd;
extern struct cfdriver pckbd_cd;
extern struct cfdriver pms_cd;
extern struct cfdriver pcmcia_cd;
extern struct cfdriver zs_cd;
extern struct cfdriver zsms_cd;
extern struct cfdriver creator_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver arc_cd;
extern struct cfdriver jmb_cd;
extern struct cfdriver eap_cd;
extern struct cfdriver eso_cd;
extern struct cfdriver auacer_cd;
extern struct cfdriver emu_cd;
extern struct cfdriver autri_cd;
extern struct cfdriver qle_cd;
extern struct cfdriver mpii_cd;
extern struct cfdriver de_cd;
extern struct cfdriver pcn_cd;
extern struct cfdriver pciide_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver vr_cd;
extern struct cfdriver em_cd;
extern struct cfdriver ix_cd;
extern struct cfdriver ixl_cd;
extern struct cfdriver thtc_cd;
extern struct cfdriver tht_cd;
extern struct cfdriver myx_cd;
extern struct cfdriver oce_cd;
extern struct cfdriver cas_cd;
extern struct cfdriver nep_cd;
extern struct cfdriver hifn_cd;
extern struct cfdriver ubsec_cd;
extern struct cfdriver mbg_cd;
extern struct cfdriver cbb_cd;
extern struct cfdriver skc_cd;
extern struct cfdriver sk_cd;
extern struct cfdriver mskc_cd;
extern struct cfdriver msk_cd;
extern struct cfdriver puc_cd;
extern struct cfdriver cmpci_cd;
extern struct cfdriver pcscp_cd;
extern struct cfdriver bge_cd;
extern struct cfdriver bnx_cd;
extern struct cfdriver stge_cd;
extern struct cfdriver jme_cd;
extern struct cfdriver alipm_cd;
extern struct cfdriver mcx_cd;
extern struct cfdriver radeondrm_cd;
extern struct cfdriver psycho_cd;
extern struct cfdriver schizo_cd;
extern struct cfdriver pyro_cd;
extern struct cfdriver vpci_cd;
extern struct cfdriver vgafb_cd;
extern struct cfdriver machfb_cd;
extern struct cfdriver radeonfb_cd;
extern struct cfdriver ifb_cd;
extern struct cfdriver raptor_cd;
extern struct cfdriver gfxp_cd;
extern struct cfdriver sbbc_cd;
extern struct cfdriver ebus_cd;
extern struct cfdriver clock_cd;
extern struct cfdriver timer_cd;
extern struct cfdriver cmp_cd;
extern struct cfdriver core_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver auxio_cd;
extern struct cfdriver bbc_cd;
extern struct cfdriver beeper_cd;
extern struct cfdriver beep_cd;
extern struct cfdriver led_cd;
extern struct cfdriver lom_cd;
extern struct cfdriver pmc_cd;
extern struct cfdriver ppm_cd;
extern struct cfdriver power_cd;
extern struct cfdriver rtc_cd;
extern struct cfdriver sab_cd;
extern struct cfdriver sabtty_cd;
extern struct cfdriver audioce_cd;
extern struct cfdriver comkbd_cd;
extern struct cfdriver comms_cd;
extern struct cfdriver zstty_cd;
extern struct cfdriver zskbd_cd;
extern struct cfdriver fdc_cd;
extern struct cfdriver fd_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver cd_cd;
extern struct cfdriver ch_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver st_cd;
extern struct cfdriver uk_cd;
extern struct cfdriver safte_cd;
extern struct cfdriver ses_cd;
extern struct cfdriver sym_cd;
extern struct cfdriver rdac_cd;
extern struct cfdriver emc_cd;
extern struct cfdriver hds_cd;
extern struct cfdriver iic_cd;
extern struct cfdriver lmtemp_cd;
extern struct cfdriver lmenv_cd;
extern struct cfdriver maxtmp_cd;
extern struct cfdriver adc_cd;
extern struct cfdriver admtemp_cd;
extern struct cfdriver admlc_cd;
extern struct cfdriver admtm_cd;
extern struct cfdriver admtmp_cd;
extern struct cfdriver admtt_cd;
extern struct cfdriver maxds_cd;
extern struct cfdriver adt_cd;
extern struct cfdriver sdmmc_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver uaudio_cd;
extern struct cfdriver uvideo_cd;
extern struct cfdriver utvfu_cd;
extern struct cfdriver umidi_cd;
extern struct cfdriver ucom_cd;
extern struct cfdriver ugen_cd;
extern struct cfdriver uhidev_cd;
extern struct cfdriver uhid_cd;
extern struct cfdriver fido_cd;
extern struct cfdriver ujoy_cd;
extern struct cfdriver ukbd_cd;
extern struct cfdriver ums_cd;
extern struct cfdriver ucycom_cd;
extern struct cfdriver uslhcom_cd;
extern struct cfdriver ulpt_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver uthum_cd;
extern struct cfdriver ugold_cd;
extern struct cfdriver uonerng_cd;
extern struct cfdriver urng_cd;
extern struct cfdriver udcf_cd;
extern struct cfdriver umbg_cd;
extern struct cfdriver uvisor_cd;
extern struct cfdriver udsbr_cd;
extern struct cfdriver utwitch_cd;
extern struct cfdriver aue_cd;
extern struct cfdriver axe_cd;
extern struct cfdriver axen_cd;
extern struct cfdriver smsc_cd;
extern struct cfdriver cue_cd;
extern struct cfdriver kue_cd;
extern struct cfdriver cdce_cd;
extern struct cfdriver urndis_cd;
extern struct cfdriver mos_cd;
extern struct cfdriver udav_cd;
extern struct cfdriver upl_cd;
extern struct cfdriver ugl_cd;
extern struct cfdriver url_cd;
extern struct cfdriver ure_cd;
extern struct cfdriver umodem_cd;
extern struct cfdriver uftdi_cd;
extern struct cfdriver uplcom_cd;
extern struct cfdriver umct_cd;
extern struct cfdriver uvscom_cd;
extern struct cfdriver ubsa_cd;
extern struct cfdriver uslcom_cd;
extern struct cfdriver uark_cd;
extern struct cfdriver uscom_cd;
extern struct cfdriver ucrcom_cd;
extern struct cfdriver uipaq_cd;
extern struct cfdriver umsm_cd;
extern struct cfdriver uchcom_cd;
extern struct cfdriver atu_cd;
extern struct cfdriver ural_cd;
extern struct cfdriver rum_cd;
extern struct cfdriver run_cd;
extern struct cfdriver zyd_cd;
extern struct cfdriver urtwn_cd;
extern struct cfdriver rsu_cd;
extern struct cfdriver uath_cd;
extern struct cfdriver uow_cd;
extern struct cfdriver upd_cd;
extern struct cfdriver uhidpp_cd;
extern struct cfdriver ucc_cd;
extern struct cfdriver cardslot_cd;
extern struct cfdriver cardbus_cd;
extern struct cfdriver pcfadc_cd;
extern struct cfdriver ecadc_cd;
extern struct cfdriver tda_cd;
extern struct cfdriver onewire_cd;
extern struct cfdriver owid_cd;
extern struct cfdriver owsbm_cd;
extern struct cfdriver owtemp_cd;
extern struct cfdriver owctr_cd;
extern struct cfdriver vcons_cd;
extern struct cfdriver vrng_cd;
extern struct cfdriver vrtc_cd;
extern struct cfdriver vds_cd;
extern struct cfdriver vdsp_cd;
extern struct cfdriver vdsk_cd;
extern struct cfdriver vsw_cd;
extern struct cfdriver vnet_cd;
extern struct cfdriver vcc_cd;
extern struct cfdriver vcctty_cd;
extern struct cfdriver vldc_cd;
extern struct cfdriver vldcp_cd;

extern struct cfattach video_ca;
extern struct cfattach audio_ca;
extern struct cfattach midi_ca;
extern struct cfattach drm_ca;
extern struct cfattach radio_ca;
extern struct cfattach vscsi_ca;
extern struct cfattach mpath_ca;
extern struct cfattach softraid_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach ssm_ca;
extern struct cfattach upa_ca;
extern struct cfattach central_ca;
extern struct cfattach fhc_central_ca;
extern struct cfattach fhc_mainbus_ca;
extern struct cfattach clkbrd_ca;
extern struct cfattach vbus_ca;
extern struct cfattach cbus_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach wsmouse_ca;
extern struct cfattach pcons_ca;
extern struct cfattach prtc_ca;
extern struct cfattach dma_sbus_ca;
extern struct cfattach ledma_ca;
extern struct cfattach lebuffer_ca;
extern struct cfattach le_sbus_ca;
extern struct cfattach le_lebuffer_ca;
extern struct cfattach le_ledma_ca;
extern struct cfattach qec_ca;
extern struct cfattach be_ca;
extern struct cfattach qe_ca;
extern struct cfattach esp_sbus_ca;
extern struct cfattach esp_dma_ca;
extern struct cfattach hme_sbus_ca;
extern struct cfattach ti_sbus_ca;
extern struct cfattach gem_sbus_ca;
extern struct cfattach audiocs_ca;
extern struct cfattach qlw_sbus_ca;
extern struct cfattach qla_sbus_ca;
extern struct cfattach agten_ca;
extern struct cfattach bwtwo_ca;
extern struct cfattach cgsix_ca;
extern struct cfattach cgthree_ca;
extern struct cfattach cgtwelve_ca;
extern struct cfattach mgx_ca;
extern struct cfattach rfx_ca;
extern struct cfattach tvtwo_ca;
extern struct cfattach vigra_ca;
extern struct cfattach zx_ca;
extern struct cfattach magma_ca;
extern struct cfattach mtty_ca;
extern struct cfattach mbpp_ca;
extern struct cfattach spif_ca;
extern struct cfattach stty_ca;
extern struct cfattach sbpp_ca;
extern struct cfattach uperf_sbus_ca;
extern struct cfattach asio_ca;
extern struct cfattach com_asio_ca;
extern struct cfattach apio_ca;
extern struct cfattach lpt_apio_ca;
extern struct cfattach stp_sbus_ca;
extern struct cfattach xbox_ca;
extern struct cfattach bpp_ca;
extern struct cfattach nsphy_ca;
extern struct cfattach nsphyter_ca;
extern struct cfattach gentbi_ca;
extern struct cfattach qsphy_ca;
extern struct cfattach inphy_ca;
extern struct cfattach iophy_ca;
extern struct cfattach eephy_ca;
extern struct cfattach exphy_ca;
extern struct cfattach rlphy_ca;
extern struct cfattach lxtphy_ca;
extern struct cfattach luphy_ca;
extern struct cfattach icsphy_ca;
extern struct cfattach sqphy_ca;
extern struct cfattach tqphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach dcphy_ca;
extern struct cfattach bmtphy_ca;
extern struct cfattach brgphy_ca;
extern struct cfattach xmphy_ca;
extern struct cfattach amphy_ca;
extern struct cfattach acphy_ca;
extern struct cfattach nsgphy_ca;
extern struct cfattach urlphy_ca;
extern struct cfattach rgephy_ca;
extern struct cfattach ipgphy_ca;
extern struct cfattach jmphy_ca;
extern struct cfattach atapiscsi_ca;
extern struct cfattach wd_ca;
extern struct cfattach pckbd_ca;
extern struct cfattach pms_ca;
extern struct cfattach pcmcia_ca;
extern struct cfattach ep_pcmcia_ca;
extern struct cfattach ne_pcmcia_ca;
extern struct cfattach com_pcmcia_ca;
extern struct cfattach wdc_pcmcia_ca;
extern struct cfattach wi_pcmcia_ca;
extern struct cfattach malo_pcmcia_ca;
extern struct cfattach zsms_ca;
extern struct cfattach sbus_mb_ca;
extern struct cfattach sbus_xbox_ca;
extern struct cfattach creator_ca;
extern struct cfattach pci_ca;
extern struct cfattach arc_ca;
extern struct cfattach jmb_ca;
extern struct cfattach ahci_pci_ca;
extern struct cfattach ahci_jmb_ca;
extern struct cfattach nvme_pci_ca;
extern struct cfattach eap_ca;
extern struct cfattach eso_ca;
extern struct cfattach auacer_ca;
extern struct cfattach emu_ca;
extern struct cfattach autri_ca;
extern struct cfattach qlw_pci_ca;
extern struct cfattach qla_pci_ca;
extern struct cfattach qle_ca;
extern struct cfattach mpi_pci_ca;
extern struct cfattach mpii_ca;
extern struct cfattach sili_pci_ca;
extern struct cfattach de_ca;
extern struct cfattach pcn_ca;
extern struct cfattach siop_pci_ca;
extern struct cfattach pciide_pci_ca;
extern struct cfattach pciide_jmb_ca;
extern struct cfattach ppb_ca;
extern struct cfattach rl_pci_ca;
extern struct cfattach re_pci_ca;
extern struct cfattach vr_ca;
extern struct cfattach xl_pci_ca;
extern struct cfattach fxp_pci_ca;
extern struct cfattach em_ca;
extern struct cfattach ix_ca;
extern struct cfattach ixl_ca;
extern struct cfattach thtc_ca;
extern struct cfattach tht_ca;
extern struct cfattach myx_ca;
extern struct cfattach oce_ca;
extern struct cfattach dc_pci_ca;
extern struct cfattach epic_pci_ca;
extern struct cfattach ti_pci_ca;
extern struct cfattach ne_pci_ca;
extern struct cfattach gem_pci_ca;
extern struct cfattach cas_ca;
extern struct cfattach nep_ca;
extern struct cfattach hifn_ca;
extern struct cfattach ubsec_ca;
extern struct cfattach mbg_ca;
extern struct cfattach uhci_pci_ca;
extern struct cfattach ohci_pci_ca;
extern struct cfattach ehci_pci_ca;
extern struct cfattach xhci_pci_ca;
extern struct cfattach cbb_pci_ca;
extern struct cfattach skc_ca;
extern struct cfattach sk_ca;
extern struct cfattach mskc_ca;
extern struct cfattach msk_ca;
extern struct cfattach com_puc_ca;
extern struct cfattach puc_pci_ca;
extern struct cfattach wi_pci_ca;
extern struct cfattach cmpci_ca;
extern struct cfattach pcscp_ca;
extern struct cfattach bge_ca;
extern struct cfattach bnx_ca;
extern struct cfattach stge_ca;
extern struct cfattach jme_ca;
extern struct cfattach ath_pci_ca;
extern struct cfattach athn_pci_ca;
extern struct cfattach ral_pci_ca;
extern struct cfattach malo_pci_ca;
extern struct cfattach alipm_ca;
extern struct cfattach mcx_ca;
extern struct cfattach radeondrm_ca;
extern struct cfattach psycho_ca;
extern struct cfattach schizo_ca;
extern struct cfattach pyro_ca;
extern struct cfattach vpci_ca;
extern struct cfattach hme_pci_ca;
extern struct cfattach vgafb_ca;
extern struct cfattach machfb_ca;
extern struct cfattach radeonfb_ca;
extern struct cfattach ifb_ca;
extern struct cfattach raptor_ca;
extern struct cfattach gfxp_ca;
extern struct cfattach sbbc_ca;
extern struct cfattach ebus_ca;
extern struct cfattach ebus_mainbus_ca;
extern struct cfattach uperf_ebus_ca;
extern struct cfattach clock_sbus_ca;
extern struct cfattach clock_ebus_ca;
extern struct cfattach clock_fhc_ca;
extern struct cfattach timer_ca;
extern struct cfattach cmp_ca;
extern struct cfattach core_ca;
extern struct cfattach cpu_ca;
extern struct cfattach auxio_ebus_ca;
extern struct cfattach auxio_sbus_ca;
extern struct cfattach bbc_ca;
extern struct cfattach beeper_ca;
extern struct cfattach beep_ca;
extern struct cfattach led_ca;
extern struct cfattach lom_ca;
extern struct cfattach pmc_ca;
extern struct cfattach ppm_ca;
extern struct cfattach power_ca;
extern struct cfattach rtc_ca;
extern struct cfattach sab_ca;
extern struct cfattach sabtty_ca;
extern struct cfattach audioce_ca;
extern struct cfattach wbsd_ebus_ca;
extern struct cfattach com_ebus_ca;
extern struct cfattach pckbc_ebus_ca;
extern struct cfattach comkbd_ca;
extern struct cfattach comms_ca;
extern struct cfattach lpt_ebus_ca;
extern struct cfattach zs_sbus_ca;
extern struct cfattach zs_fhc_ca;
extern struct cfattach zstty_ca;
extern struct cfattach zskbd_ca;
extern struct cfattach fdc_sbus_ca;
extern struct cfattach fd_ca;
extern struct cfattach pcfiic_ebus_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach cd_ca;
extern struct cfattach ch_ca;
extern struct cfattach sd_ca;
extern struct cfattach st_ca;
extern struct cfattach uk_ca;
extern struct cfattach safte_ca;
extern struct cfattach ses_ca;
extern struct cfattach sym_ca;
extern struct cfattach rdac_ca;
extern struct cfattach emc_ca;
extern struct cfattach hds_ca;
extern struct cfattach iic_ca;
extern struct cfattach lmtemp_ca;
extern struct cfattach lmenv_ca;
extern struct cfattach maxtmp_ca;
extern struct cfattach adc_ca;
extern struct cfattach admtemp_ca;
extern struct cfattach admlc_ca;
extern struct cfattach admtm_ca;
extern struct cfattach admtmp_ca;
extern struct cfattach admtt_ca;
extern struct cfattach maxds_ca;
extern struct cfattach adt_ca;
extern struct cfattach lm_i2c_ca;
extern struct cfattach spdmem_iic_ca;
extern struct cfattach sdmmc_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach uaudio_ca;
extern struct cfattach uvideo_ca;
extern struct cfattach utvfu_ca;
extern struct cfattach umidi_ca;
extern struct cfattach ucom_ca;
extern struct cfattach ugen_ca;
extern struct cfattach uhidev_ca;
extern struct cfattach uhid_ca;
extern struct cfattach fido_ca;
extern struct cfattach ujoy_ca;
extern struct cfattach ukbd_ca;
extern struct cfattach ums_ca;
extern struct cfattach ucycom_ca;
extern struct cfattach uslhcom_ca;
extern struct cfattach ulpt_ca;
extern struct cfattach umass_ca;
extern struct cfattach uthum_ca;
extern struct cfattach ugold_ca;
extern struct cfattach uonerng_ca;
extern struct cfattach urng_ca;
extern struct cfattach udcf_ca;
extern struct cfattach umbg_ca;
extern struct cfattach uvisor_ca;
extern struct cfattach udsbr_ca;
extern struct cfattach utwitch_ca;
extern struct cfattach aue_ca;
extern struct cfattach axe_ca;
extern struct cfattach axen_ca;
extern struct cfattach smsc_ca;
extern struct cfattach cue_ca;
extern struct cfattach kue_ca;
extern struct cfattach cdce_ca;
extern struct cfattach urndis_ca;
extern struct cfattach mos_ca;
extern struct cfattach udav_ca;
extern struct cfattach upl_ca;
extern struct cfattach ugl_ca;
extern struct cfattach url_ca;
extern struct cfattach ure_ca;
extern struct cfattach umodem_ca;
extern struct cfattach uftdi_ca;
extern struct cfattach uplcom_ca;
extern struct cfattach umct_ca;
extern struct cfattach uvscom_ca;
extern struct cfattach ubsa_ca;
extern struct cfattach uslcom_ca;
extern struct cfattach uark_ca;
extern struct cfattach uscom_ca;
extern struct cfattach ucrcom_ca;
extern struct cfattach uipaq_ca;
extern struct cfattach umsm_ca;
extern struct cfattach uchcom_ca;
extern struct cfattach wi_usb_ca;
extern struct cfattach atu_ca;
extern struct cfattach ural_ca;
extern struct cfattach rum_ca;
extern struct cfattach run_ca;
extern struct cfattach zyd_ca;
extern struct cfattach urtwn_ca;
extern struct cfattach rsu_ca;
extern struct cfattach uath_ca;
extern struct cfattach athn_usb_ca;
extern struct cfattach uow_ca;
extern struct cfattach upd_ca;
extern struct cfattach uhidpp_ca;
extern struct cfattach ucc_ca;
extern struct cfattach cardslot_ca;
extern struct cfattach cardbus_ca;
extern struct cfattach xl_cardbus_ca;
extern struct cfattach dc_cardbus_ca;
extern struct cfattach re_cardbus_ca;
extern struct cfattach ath_cardbus_ca;
extern struct cfattach athn_cardbus_ca;
extern struct cfattach ral_cardbus_ca;
extern struct cfattach ehci_cardbus_ca;
extern struct cfattach ohci_cardbus_ca;
extern struct cfattach malo_cardbus_ca;
extern struct cfattach pcfadc_ca;
extern struct cfattach ecadc_ca;
extern struct cfattach tda_ca;
extern struct cfattach onewire_ca;
extern struct cfattach owid_ca;
extern struct cfattach owsbm_ca;
extern struct cfattach owtemp_ca;
extern struct cfattach owctr_ca;
extern struct cfattach vcons_ca;
extern struct cfattach vrng_ca;
extern struct cfattach vrtc_ca;
extern struct cfattach vds_ca;
extern struct cfattach vdsp_ca;
extern struct cfattach vdsk_ca;
extern struct cfattach vsw_ca;
extern struct cfattach vnet_ca;
extern struct cfattach vcc_ca;
extern struct cfattach vcctty_ca;
extern struct cfattach vldc_ca;
extern struct cfattach vldcp_ca;


/* locators */
static long loc[10] = {
	-1, -1, -1, -1, -1, -1, -1, -1,
	1, 0,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"primary",
	"console",
	"mux",
	"slot",
	"offset",
	"function",
	"irq",
	"phy",
	"channel",
	"drive",
	"controller",
	"socket",
	"bus",
	"dev",
	"port",
	"addr",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"target",
	"lun",
	"size",
	"portno",
	"reportid",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, -1, 1, 0, 2, -1, 1,
	0, 2, -1, 1, 0, 2, -1, 1,
	0, 2, -1, 1, 0, 2, -1, 1,
	0, 2, -1, 1, 0, 2, -1, 1,
	0, 2, -1, 1, 0, 2, -1, 1,
	0, 2, -1, 1, 0, 2, -1, 1,
	0, 2, -1, 1, 0, 2, -1, 1,
	0, 2, -1, 1, 0, 2, -1, 1,
	0, 2, -1, 1, 0, 2, -1, 1,
	0, 2, -1, 1, 0, 2, -1, 1,
	2, -1, 1, 2, -1, 1, 2, -1,
	1, 2, -1, 1, 2, -1, 2, -1,
	2, -1, 2, -1, 2, -1, 3, 4,
	-1, 3, 4, -1, 5, 6, -1, 7,
	-1, 7, -1, 7, -1, 7, -1, 7,
	-1, 7, -1, 7, -1, 7, -1, 7,
	-1, 7, -1, 7, -1, 7, -1, 7,
	-1, 7, -1, 7, -1, 7, -1, 7,
	-1, 7, -1, 7, -1, 7, -1, 7,
	-1, 7, -1, 7, -1, 7, -1, 7,
	-1, 7, -1, 7, -1, 7, -1, 7,
	-1, 7, -1, 7, -1, 7, -1, 7,
	-1, 7, -1, 7, -1, 7, -1, 7,
	-1, 8, -1, 8, -1, 8, 9, -1,
	3, -1, 10, 11, -1, 10, 11, -1,
	8, -1, 8, -1, 12, -1, 12, -1,
	12, -1, 12, -1, 12, -1, 13, 5,
	-1, 13, 5, -1, 14, -1, 15, -1,
	15, -1, 14, 16, 17, 18, 19, 20,
	-1, 14, 16, 17, 18, 19, 20, -1,
	21, 22, -1, 15, 23, -1, 24, -1,
	24, -1, 24, -1, 24, -1, 24, -1,
	24, -1, 24, -1, 24, -1, 24, -1,
	24, -1, 24, -1, 24, -1, 24, -1,
	24, -1, 24, -1, 24, -1, 25, -1,
	3, -1,
};

/* size of parent vectors */
int pv_size = 253;

/* parent vectors */
short pv[253] = {
	305, 304, 301, 300, 295, 294, 293, 292, 174, 173, 172, 171, 164, 162, 150, 149,
	132, 124, 29, 191, 33, 146, 34, 148, 35, 147, 97, 145, 143, 144, 130, 131,
	129, 135, 133, 134, 96, -1, 7, 5, 347, 282, 263, 170, 121, 119, 107, 90,
	6, 125, 32, 31, 122, 120, 111, 110, 109, 118, 38, 117, 37, -1, 20, 39,
	40, 41, 42, 43, 104, 105, 197, 195, 193, 44, 194, 196, 45, 46, 192, 47,
	48, 186, -1, 306, 289, 310, 311, 307, 308, 309, 312, 314, 315, 313, 316, 318,
	279, 280, 317, -1, 114, 224, 36, 116, 169, 112, 115, 113, 267, 269, -1, 155,
	156, 154, 157, 158, 159, -1, 277, 331, 227, 233, 93, -1, 187, 188, 189, 190,
	128, -1, 278, 228, 101, 94, -1, 116, 112, 270, -1, 265, 266, -1, 102, 103,
	-1, 127, 126, -1, 332, 61, -1, 200, 199, -1, 230, 231, -1, 268, 269, -1,
	8, 9, -1, 12, 13, -1, 236, 184, -1, 333, -1, 106, -1, 225, -1, 11,
	-1, 8, -1, 237, -1, 249, -1, 226, -1, 161, -1, 163, -1, 62, -1, 10,
	-1, 351, -1, 16, -1, 353, -1, 108, -1, 328, -1, 337, -1, 264, -1, 15,
	-1, 273, -1, 160, -1, 332, -1, 167, -1, 95, -1, 345, -1, 206, -1, 207,
	-1, 290, -1, 59, -1, 56, -1, 52, -1, 49, -1, 139, -1, 22, -1, 28,
	-1, 222, -1, 24, -1, 23, -1, 348, -1, 98, -1, 234, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: video* at uvideo*|utvfu* */
    {&video_ca,		&video_cd,	 0, STAR,     loc,    0, pv+157, 0,    0},
/*  1: audio* at auacer*|audioce*|audiocs*|autri*|cmpci*|eap*|emu*|eso*|uaudio*|utvfu* */
    {&audio_ca,		&audio_cd,	 0, STAR,     loc,    0, pv+100, 0,    0},
/*  2: midi* at autri*|eap*|umidi* */
    {&midi_ca,		&midi_cd,	 0, STAR,     loc,    0, pv+135, 0,    0},
/*  3: drm* at radeondrm* primary -1 */
    {&drm_ca,		&drm_cd,	 0, STAR, loc+  5,    0, pv+81, 1,    0},
/*  4: radio* at udsbr* */
    {&radio_ca,		&radio_cd,	 0, STAR,     loc,    0, pv+225, 2,    0},
/*  5: vscsi0 at root */
    {&vscsi_ca,		&vscsi_cd,	 0, NORM,     loc,    0, pv+37, 0,    0},
/*  6: mpath0 at root */
    {&mpath_ca,		&mpath_cd,	 0, NORM,     loc,    0, pv+37, 0,    0},
/*  7: softraid0 at root */
    {&softraid_ca,	&softraid_cd,	 0, NORM,     loc,    0, pv+37, 0,    0},
/*  8: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+37, 0,    0},
/*  9: ssm* at mainbus0 */
    {&ssm_ca,		&ssm_cd,	 0, STAR,     loc,    0, pv+177, 2,    0},
/* 10: upa0 at mainbus0 */
    {&upa_ca,		&upa_cd,	 0, NORM,     loc,    0, pv+177, 2,    0},
/* 11: central0 at mainbus0 */
    {&central_ca,	&central_cd,	 0, NORM,     loc,    0, pv+177, 2,    0},
/* 12: fhc0 at central0 */
    {&fhc_central_ca,	&fhc_cd,	 0, NORM,     loc,    0, pv+175, 2,    0},
/* 13: fhc* at mainbus0 */
    {&fhc_mainbus_ca,	&fhc_cd,	 1, STAR,     loc,    0, pv+177, 2,    1},
/* 14: clkbrd* at fhc0|fhc* */
    {&clkbrd_ca,	&clkbrd_cd,	 0, STAR,     loc,    0, pv+163, 2,    0},
/* 15: vbus0 at mainbus0 */
    {&vbus_ca,		&vbus_cd,	 0, NORM,     loc,    0, pv+177, 2,    0},
/* 16: cbus* at vbus0 */
    {&cbus_ca,		&cbus_cd,	 0, STAR,     loc,    0, pv+207, 2,    0},
/* 17: wsdisplay* at pcons0|agten*|bwtwo*|cgsix*|cgthree*|cgtwelve*|creator*|creator*|gfxp*|ifb*|machfb*|mgx*|radeonfb*|raptor*|rfx*|tvtwo*|vgafb*|vigra*|zx*|radeondrm* console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+  6,    0, pv+62, 3,    0},
/* 18: wskbd* at ukbd*|ucc*|comkbd*|zskbd*|pckbd* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+  7,    0, pv+118, 79,    0},
/* 19: wsmouse* at ums*|comms*|zsms*|pms* mux 0 */
    {&wsmouse_ca,	&wsmouse_cd,	 0, STAR, loc+  9,    0, pv+130, 94,    0},
/* 20: pcons0 at mainbus0 */
    {&pcons_ca,		&pcons_cd,	 0, NORM,     loc,    0, pv+177, 2,    0},
/* 21: prtc0 at mainbus0 */
    {&prtc_ca,		&prtc_cd,	 0, NORM,     loc,    0, pv+177, 2,    0},
/* 22: dma* at sbus*|sbus* slot -1 offset -1 */
    {&dma_sbus_ca,	&dma_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 23: ledma* at sbus*|sbus* slot -1 offset -1 */
    {&ledma_ca,		&ledma_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 24: lebuffer* at sbus*|sbus* slot -1 offset -1 */
    {&lebuffer_ca,	&lebuffer_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 25: le* at sbus*|sbus* slot -1 offset -1 */
    {&le_sbus_ca,	&le_cd,		 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 26: le* at ledma* */
    {&le_ledma_ca,	&le_cd,		 0, STAR,     loc,    0, pv+245, 107,    0},
/* 27: le* at lebuffer* */
    {&le_lebuffer_ca,	&le_cd,		 0, STAR,     loc,    0, pv+243, 107,    0},
/* 28: qec* at sbus*|sbus* slot -1 offset -1 */
    {&qec_ca,		&qec_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 29: be* at qec* */
    {&be_ca,		&be_cd,		 0, STAR,     loc,    0, pv+239, 107,    0},
/* 30: qe* at qec* */
    {&qe_ca,		&qe_cd,		 0, STAR,     loc,    0, pv+239, 107,    0},
/* 31: esp* at sbus*|sbus* slot -1 offset -1 */
    {&esp_sbus_ca,	&esp_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 32: esp* at dma* */
    {&esp_dma_ca,	&esp_cd,	 0, STAR,     loc,    0, pv+237, 107,    0},
/* 33: hme* at sbus*|sbus* slot -1 offset -1 */
    {&hme_sbus_ca,	&hme_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 34: ti* at sbus*|sbus* slot -1 offset -1 */
    {&ti_sbus_ca,	&ti_cd,		 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 35: gem* at sbus*|sbus* slot -1 offset -1 */
    {&gem_sbus_ca,	&gem_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 36: audiocs* at sbus*|sbus* slot -1 offset -1 */
    {&audiocs_ca,	&audiocs_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 37: qlw* at sbus*|sbus* slot -1 offset -1 */
    {&qlw_sbus_ca,	&qlw_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 38: qla* at sbus*|sbus* slot -1 offset -1 */
    {&qla_sbus_ca,	&qla_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 39: agten* at sbus*|sbus* slot -1 offset -1 */
    {&agten_ca,		&agten_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 40: bwtwo* at sbus*|sbus* slot -1 offset -1 */
    {&bwtwo_ca,		&bwtwo_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 41: cgsix* at sbus*|sbus* slot -1 offset -1 */
    {&cgsix_ca,		&cgsix_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 42: cgthree* at sbus*|sbus* slot -1 offset -1 */
    {&cgthree_ca,	&cgthree_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 43: cgtwelve* at sbus*|sbus* slot -1 offset -1 */
    {&cgtwelve_ca,	&cgtwelve_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 44: mgx* at sbus*|sbus* slot -1 offset -1 */
    {&mgx_ca,		&mgx_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 45: rfx* at sbus*|sbus* slot -1 offset -1 */
    {&rfx_ca,		&rfx_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 46: tvtwo* at sbus*|sbus* slot -1 offset -1 */
    {&tvtwo_ca,		&tvtwo_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 47: vigra* at sbus*|sbus* slot -1 offset -1 */
    {&vigra_ca,		&vigra_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 48: zx* at sbus*|sbus* slot -1 offset -1 */
    {&zx_ca,		&zx_cd,		 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 49: magma* at sbus*|sbus* slot -1 offset -1 */
    {&magma_ca,		&magma_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 50: mtty* at magma* */
    {&mtty_ca,		&mtty_cd,	 0, STAR,     loc,    0, pv+233, 107,    0},
/* 51: mbpp* at magma* */
    {&mbpp_ca,		&mbpp_cd,	 0, STAR,     loc,    0, pv+233, 107,    0},
/* 52: spif* at sbus*|sbus* slot -1 offset -1 */
    {&spif_ca,		&spif_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 53: stty* at spif* */
    {&stty_ca,		&stty_cd,	 0, STAR,     loc,    0, pv+231, 107,    0},
/* 54: sbpp* at spif* */
    {&sbpp_ca,		&sbpp_cd,	 0, STAR,     loc,    0, pv+231, 107,    0},
/* 55: uperf* at sbus*|sbus* slot -1 offset -1 */
    {&uperf_sbus_ca,	&uperf_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 56: asio* at sbus*|sbus* slot -1 offset -1 */
    {&asio_ca,		&asio_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 57: com* at asio* */
    {&com_asio_ca,	&com_cd,	 0, STAR,     loc,    0, pv+229, 107,    0},
/* 58: com* at pcmcia* function -1 irq -1 */
    {&com_pcmcia_ca,	&com_cd,	 0, STAR, loc+  4,    0, pv+217, 108,    0},
/* 59: apio* at sbus*|sbus* slot -1 offset -1 */
    {&apio_ca,		&apio_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 60: lpt* at apio* */
    {&lpt_apio_ca,	&lpt_cd,	 0, STAR,     loc,    0, pv+227, 110,    0},
/* 61: stp* at sbus*|sbus* slot -1 offset -1 */
    {&stp_sbus_ca,	&stp_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 62: xbox* at sbus*|sbus* slot -1 offset -1 */
    {&xbox_ca,		&xbox_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 63: bpp* at sbus*|sbus* slot -1 offset -1 */
    {&bpp_ca,		&bpp_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/* 64: nsphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&nsphy_ca,		&nsphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 111,    0},
/* 65: nsphyter* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&nsphyter_ca,	&nsphyter_cd,	 0, STAR, loc+  5,    0, pv+ 0, 111,    0},
/* 66: gentbi* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&gentbi_ca,	&gentbi_cd,	 0, STAR, loc+  5,    0, pv+ 0, 111,    0},
/* 67: qsphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&qsphy_ca,		&qsphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 111,    0},
/* 68: inphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&inphy_ca,		&inphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 111,    0},
/* 69: iophy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&iophy_ca,		&iophy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 111,    0},
/* 70: eephy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&eephy_ca,		&eephy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 111,    0},
/* 71: exphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&exphy_ca,		&exphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 111,    0},
/* 72: rlphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&rlphy_ca,		&rlphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 111,    0},
/* 73: lxtphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&lxtphy_ca,	&lxtphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 111,    0},
/* 74: luphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&luphy_ca,		&luphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 111,    0},
/* 75: icsphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&icsphy_ca,	&icsphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 111,    0},
/* 76: sqphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&sqphy_ca,		&sqphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 111,    0},
/* 77: tqphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&tqphy_ca,		&tqphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 111,    0},
/* 78: ukphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 111,    0},
/* 79: dcphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&dcphy_ca,		&dcphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 111,    0},
/* 80: bmtphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&bmtphy_ca,	&bmtphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 111,    0},
/* 81: brgphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&brgphy_ca,	&brgphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 111,    0},
/* 82: xmphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&xmphy_ca,		&xmphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 111,    0},
/* 83: amphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&amphy_ca,		&amphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 111,    0},
/* 84: acphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&acphy_ca,		&acphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 111,    0},
/* 85: nsgphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&nsgphy_ca,	&nsgphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 111,    0},
/* 86: urlphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&urlphy_ca,	&urlphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 111,    0},
/* 87: rgephy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&rgephy_ca,	&rgephy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 111,    0},
/* 88: ipgphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&ipgphy_ca,	&ipgphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 111,    0},
/* 89: jmphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&jmphy_ca,		&jmphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 111,    0},
/* 90: atapiscsi* at pciide*|pciide* channel -1 */
    {&atapiscsi_ca,	&atapiscsi_cd,	 0, STAR, loc+  5,    0, pv+145, 185,    0},
/* 91: wd* at pciide*|pciide* channel -1 drive -1 */
    {&wd_ca,		&wd_cd,		 0, STAR, loc+  4, 0xa00, pv+145, 185,    0},
/* 92: wd* at wdc* channel -1 drive -1 */
    {&wd_ca,		&wd_cd,		 0, STAR, loc+  4,    0, pv+249, 189,    0},
/* 93: pckbd* at pckbc* slot -1 */
    {&pckbd_ca,		&pckbd_cd,	 0, STAR, loc+  5,    0, pv+183, 192,    0},
/* 94: pms* at pckbc* slot -1 */
    {&pms_ca,		&pms_cd,	 0, STAR, loc+  5,    0, pv+183, 192,    0},
/* 95: pcmcia* at cardslot*|stp* controller -1 socket -1 */
    {&pcmcia_ca,	&pcmcia_cd,	 0, STAR, loc+  4,    0, pv+148, 194,    0},
/* 96: ep* at pcmcia* function -1 irq -1 */
    {&ep_pcmcia_ca,	&ep_cd,		 0, STAR, loc+  4,    0, pv+217, 108,    0},
/* 97: ne* at pcmcia* function -1 irq -1 */
    {&ne_pcmcia_ca,	&ne_cd,		 0, STAR, loc+  4,    0, pv+217, 108,    0},
/* 98: wdc* at pcmcia* function -1 irq -1 */
    {&wdc_pcmcia_ca,	&wdc_cd,	 0, STAR, loc+  4,    0, pv+217, 108,    0},
/* 99: wi* at pcmcia* function -1 irq -1 */
    {&wi_pcmcia_ca,	&wi_cd,		 0, STAR, loc+  4,    0, pv+217, 108,    0},
/*100: malo* at pcmcia* function -1 irq -1 */
    {&malo_pcmcia_ca,	&malo_cd,	 0, STAR, loc+  4,    0, pv+217, 108,    0},
/*101: zsms* at zs*|zs* channel -1 */
    {&zsms_ca,		&zsms_cd,	 0, STAR, loc+  5,    0, pv+154, 200,    0},
/*102: sbus* at mainbus0 */
    {&sbus_mb_ca,	&sbus_cd,	 0, STAR,     loc,    0, pv+177, 2,    0},
/*103: sbus* at xbox* */
    {&sbus_xbox_ca,	&sbus_cd,	 0, STAR,     loc,    0, pv+189, 203,    0},
/*104: creator* at mainbus0 */
    {&creator_ca,	&creator_cd,	 0, STAR,     loc,    0, pv+177, 2,    0},
/*105: creator* at upa0 */
    {&creator_ca,	&creator_cd,	 0, STAR,     loc,    0, pv+191, 203,    0},
/*106: pci* at psycho*|schizo*|pyro*|vpci*|ppb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+  5,    0, pv+124, 204,    0},
/*107: arc* at pci* dev -1 function -1 */
    {&arc_ca,		&arc_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*108: jmb* at pci* dev -1 function -1 */
    {&jmb_ca,		&jmb_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*109: ahci* at pci* dev -1 function -1 */
    {&ahci_pci_ca,	&ahci_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*110: ahci* at jmb* */
    {&ahci_jmb_ca,	&ahci_cd,	 0, STAR,     loc,    0, pv+199, 216,    0},
/*111: nvme* at pci* dev -1 function -1 */
    {&nvme_pci_ca,	&nvme_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*112: eap* at pci* dev -1 function -1 */
    {&eap_ca,		&eap_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*113: eso* at pci* dev -1 function -1 */
    {&eso_ca,		&eso_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*114: auacer* at pci* dev -1 function -1 */
    {&auacer_ca,	&auacer_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*115: emu* at pci* dev -1 function -1 */
    {&emu_ca,		&emu_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*116: autri* at pci* dev -1 function -1 */
    {&autri_ca,		&autri_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*117: qlw* at pci* dev -1 function -1 */
    {&qlw_pci_ca,	&qlw_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*118: qla* at pci* dev -1 function -1 */
    {&qla_pci_ca,	&qla_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*119: qle* at pci* dev -1 function -1 */
    {&qle_ca,		&qle_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*120: mpi* at pci* dev -1 function -1 */
    {&mpi_pci_ca,	&mpi_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*121: mpii* at pci* dev -1 function -1 */
    {&mpii_ca,		&mpii_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*122: sili* at pci* dev -1 function -1 */
    {&sili_pci_ca,	&sili_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*123: de* at pci* dev -1 function -1 */
    {&de_ca,		&de_cd,		 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*124: pcn* at pci* dev -1 function -1 */
    {&pcn_ca,		&pcn_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*125: siop* at pci* dev -1 function -1 */
    {&siop_pci_ca,	&siop_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*126: pciide* at pci* dev -1 function -1 */
    {&pciide_pci_ca,	&pciide_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*127: pciide* at jmb* */
    {&pciide_jmb_ca,	&pciide_cd,	 0, STAR,     loc,    0, pv+199, 216,    0},
/*128: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*129: rl* at pci* dev -1 function -1 */
    {&rl_pci_ca,	&rl_cd,		 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*130: re* at pci* dev -1 function -1 */
    {&re_pci_ca,	&re_cd,		 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*131: re* at cardbus* dev -1 function -1 */
    {&re_cardbus_ca,	&re_cd,		 0, STAR, loc+  4,    0, pv+169, 217,    0},
/*132: vr* at pci* dev -1 function -1 */
    {&vr_ca,		&vr_cd,		 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*133: xl* at pci* dev -1 function -1 */
    {&xl_pci_ca,	&xl_cd,		 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*134: xl* at cardbus* dev -1 function -1 */
    {&xl_cardbus_ca,	&xl_cd,		 0, STAR, loc+  4,    0, pv+169, 217,    0},
/*135: fxp* at pci* dev -1 function -1 */
    {&fxp_pci_ca,	&fxp_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*136: em* at pci* dev -1 function -1 */
    {&em_ca,		&em_cd,		 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*137: ix* at pci* dev -1 function -1 */
    {&ix_ca,		&ix_cd,		 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*138: ixl* at pci* dev -1 function -1 */
    {&ixl_ca,		&ixl_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*139: thtc* at pci* dev -1 function -1 */
    {&thtc_ca,		&thtc_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*140: tht* at thtc* */
    {&tht_ca,		&tht_cd,	 0, STAR,     loc,    0, pv+235, 219,    0},
/*141: myx* at pci* dev -1 function -1 */
    {&myx_ca,		&myx_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*142: oce* at pci* dev -1 function -1 */
    {&oce_ca,		&oce_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*143: dc* at pci* dev -1 function -1 */
    {&dc_pci_ca,	&dc_cd,		 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*144: dc* at cardbus* dev -1 function -1 */
    {&dc_cardbus_ca,	&dc_cd,		 0, STAR, loc+  4,    0, pv+169, 217,    0},
/*145: epic* at pci* dev -1 function -1 */
    {&epic_pci_ca,	&epic_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*146: ti* at pci* dev -1 function -1 */
    {&ti_pci_ca,	&ti_cd,		 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*147: ne* at pci* dev -1 function -1 */
    {&ne_pci_ca,	&ne_cd,		 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*148: gem* at pci* dev -1 function -1 */
    {&gem_pci_ca,	&gem_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*149: cas* at pci* dev -1 function -1 */
    {&cas_ca,		&cas_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*150: nep* at pci* dev -1 function -1 */
    {&nep_ca,		&nep_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*151: hifn* at pci* dev -1 function -1 */
    {&hifn_ca,		&hifn_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*152: ubsec* at pci* dev -1 function -1 */
    {&ubsec_ca,		&ubsec_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*153: mbg* at pci* dev -1 function -1 */
    {&mbg_ca,		&mbg_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*154: uhci* at pci* dev -1 function -1 */
    {&uhci_pci_ca,	&uhci_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*155: ohci* at pci* dev -1 function -1 */
    {&ohci_pci_ca,	&ohci_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*156: ohci* at cardbus* dev -1 function -1 */
    {&ohci_cardbus_ca,	&ohci_cd,	 0, STAR, loc+  4,    0, pv+169, 217,    0},
/*157: ehci* at pci* dev -1 function -1 */
    {&ehci_pci_ca,	&ehci_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*158: ehci* at cardbus* dev -1 function -1 */
    {&ehci_cardbus_ca,	&ehci_cd,	 0, STAR, loc+  4,    0, pv+169, 217,    0},
/*159: xhci* at pci* dev -1 function -1 */
    {&xhci_pci_ca,	&xhci_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*160: cbb* at pci* dev -1 function -1 */
    {&cbb_pci_ca,	&cbb_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*161: skc* at pci* dev -1 function -1 */
    {&skc_ca,		&skc_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*162: sk* at skc* */
    {&sk_ca,		&sk_cd,		 0, STAR,     loc,    0, pv+185, 219,    0},
/*163: mskc* at pci* dev -1 function -1 */
    {&mskc_ca,		&mskc_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*164: msk* at mskc* */
    {&msk_ca,		&msk_cd,	 0, STAR,     loc,    0, pv+187, 219,    0},
/*165: com* at puc* port -1 */
    {&com_puc_ca,	&com_cd,	 0, STAR, loc+  5,    0, pv+215, 220,    0},
/*166: com* at ebus*|ebus* addr -1 */
    {&com_ebus_ca,	&com_cd,	 0, STAR, loc+  5,    0, pv+151, 222,    0},
/*167: puc* at pci* dev -1 function -1 */
    {&puc_pci_ca,	&puc_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*168: wi* at pci* dev -1 function -1 */
    {&wi_pci_ca,	&wi_cd,		 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*169: cmpci* at pci* dev -1 function -1 */
    {&cmpci_ca,		&cmpci_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*170: pcscp* at pci* dev -1 function -1 */
    {&pcscp_ca,		&pcscp_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*171: bge* at pci* dev -1 function -1 */
    {&bge_ca,		&bge_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*172: bnx* at pci* dev -1 function -1 */
    {&bnx_ca,		&bnx_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*173: stge* at pci* dev -1 function -1 */
    {&stge_ca,		&stge_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*174: jme* at pci* dev -1 function -1 */
    {&jme_ca,		&jme_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*175: ath* at pci* dev -1 function -1 */
    {&ath_pci_ca,	&ath_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*176: ath* at cardbus* dev -1 function -1 */
    {&ath_cardbus_ca,	&ath_cd,	 0, STAR, loc+  4,    0, pv+169, 217,    0},
/*177: athn* at pci* dev -1 function -1 */
    {&athn_pci_ca,	&athn_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*178: athn* at cardbus* dev -1 function -1 */
    {&athn_cardbus_ca,	&athn_cd,	 0, STAR, loc+  4,    0, pv+169, 217,    0},
/*179: athn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&athn_usb_ca,	&athn_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*180: ral* at pci* dev -1 function -1 */
    {&ral_pci_ca,	&ral_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*181: ral* at cardbus* dev -1 function -1 */
    {&ral_cardbus_ca,	&ral_cd,	 0, STAR, loc+  4,    0, pv+169, 217,    0},
/*182: malo* at pci* dev -1 function -1 */
    {&malo_pci_ca,	&malo_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*183: malo* at cardbus* dev -1 function -1 */
    {&malo_cardbus_ca,	&malo_cd,	 0, STAR, loc+  4,    0, pv+169, 217,    0},
/*184: alipm* at pci* dev -1 function -1 */
    {&alipm_ca,		&alipm_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*185: mcx* at pci* dev -1 function -1 */
    {&mcx_ca,		&mcx_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*186: radeondrm* at pci* dev -1 function -1 */
    {&radeondrm_ca,	&radeondrm_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*187: psycho* at mainbus0 */
    {&psycho_ca,	&psycho_cd,	 0, STAR,     loc,    0, pv+177, 2,    0},
/*188: schizo* at mainbus0|ssm* */
    {&schizo_ca,	&schizo_cd,	 0, STAR,     loc,    0, pv+160, 2,    0},
/*189: pyro* at mainbus0 */
    {&pyro_ca,		&pyro_cd,	 0, STAR,     loc,    0, pv+177, 2,    0},
/*190: vpci* at mainbus0 */
    {&vpci_ca,		&vpci_cd,	 0, STAR,     loc,    0, pv+177, 2,    0},
/*191: hme* at pci* dev -1 function -1 */
    {&hme_pci_ca,	&hme_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*192: vgafb* at pci* dev -1 function -1 */
    {&vgafb_ca,		&vgafb_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*193: machfb* at pci* dev -1 function -1 */
    {&machfb_ca,	&machfb_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*194: radeonfb* at pci* dev -1 function -1 */
    {&radeonfb_ca,	&radeonfb_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*195: ifb* at pci* dev -1 function -1 */
    {&ifb_ca,		&ifb_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*196: raptor* at pci* dev -1 function -1 */
    {&raptor_ca,	&raptor_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*197: gfxp* at pci* dev -1 function -1 */
    {&gfxp_ca,		&gfxp_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*198: sbbc* at pci* dev -1 function -1 */
    {&sbbc_ca,		&sbbc_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*199: ebus* at pci* dev -1 function -1 */
    {&ebus_ca,		&ebus_cd,	 0, STAR, loc+  4,    0, pv+171, 214,    0},
/*200: ebus* at mainbus0 */
    {&ebus_mainbus_ca,	&ebus_cd,	 0, STAR,     loc,    0, pv+177, 2,    0},
/*201: uperf* at ebus*|ebus* addr -1 */
    {&uperf_ebus_ca,	&uperf_cd,	 0, STAR, loc+  5,    0, pv+151, 222,    0},
/*202: clock* at sbus*|sbus* slot -1 offset -1 */
    {&clock_sbus_ca,	&clock_cd,	 1, STAR, loc+  4,    0, pv+142, 102,    1},
/*203: clock* at ebus*|ebus* addr -1 */
    {&clock_ebus_ca,	&clock_cd,	 1, STAR, loc+  5,    0, pv+151, 222,    1},
/*204: clock0 at fhc0|fhc* */
    {&clock_fhc_ca,	&clock_cd,	 0, NORM,     loc,    0, pv+163, 2,    0},
/*205: timer* at mainbus0 */
    {&timer_ca,		&timer_cd,	 0, STAR,     loc,    0, pv+177, 2,    0},
/*206: cmp* at mainbus0|ssm* */
    {&cmp_ca,		&cmp_cd,	 0, STAR,     loc,    0, pv+160, 2,    0},
/*207: core* at cmp* */
    {&core_ca,		&core_cd,	 0, STAR,     loc,    0, pv+221, 239,    0},
/*208: cpu0 at mainbus0|ssm* */
    {&cpu_ca,		&cpu_cd,	 0, NORM,     loc,    0, pv+160, 2,    0},
/*209: cpu0 at cmp* */
    {&cpu_ca,		&cpu_cd,	 0, NORM,     loc,    0, pv+221, 239,    0},
/*210: cpu0 at core* */
    {&cpu_ca,		&cpu_cd,	 0, NORM,     loc,    0, pv+223, 239,    0},
/*211: auxio* at ebus*|ebus* addr -1 */
    {&auxio_ebus_ca,	&auxio_cd,	 0, STAR, loc+  5,    0, pv+151, 222,    0},
/*212: auxio* at sbus*|sbus* slot -1 offset -1 */
    {&auxio_sbus_ca,	&auxio_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/*213: bbc* at ebus*|ebus* addr -1 */
    {&bbc_ca,		&bbc_cd,	 0, STAR, loc+  5,    0, pv+151, 222,    0},
/*214: beeper* at ebus*|ebus* addr -1 */
    {&beeper_ca,	&beeper_cd,	 0, STAR, loc+  5,    0, pv+151, 222,    0},
/*215: beep* at ebus*|ebus* addr -1 */
    {&beep_ca,		&beep_cd,	 0, STAR, loc+  5,    0, pv+151, 222,    0},
/*216: led* at ebus*|ebus* addr -1 */
    {&led_ca,		&led_cd,	 0, STAR, loc+  5,    0, pv+151, 222,    0},
/*217: lom* at ebus*|ebus* addr -1 */
    {&lom_ca,		&lom_cd,	 0, STAR, loc+  5,    0, pv+151, 222,    0},
/*218: pmc* at ebus*|ebus* addr -1 */
    {&pmc_ca,		&pmc_cd,	 0, STAR, loc+  5,    0, pv+151, 222,    0},
/*219: ppm* at ebus*|ebus* addr -1 */
    {&ppm_ca,		&ppm_cd,	 0, STAR, loc+  5,    0, pv+151, 222,    0},
/*220: power* at ebus*|ebus* addr -1 */
    {&power_ca,		&power_cd,	 0, STAR, loc+  5,    0, pv+151, 222,    0},
/*221: rtc* at ebus*|ebus* addr -1 */
    {&rtc_ca,		&rtc_cd,	 0, STAR, loc+  5,    0, pv+151, 222,    0},
/*222: sab* at ebus*|ebus* addr -1 */
    {&sab_ca,		&sab_cd,	 0, STAR, loc+  5,    0, pv+151, 222,    0},
/*223: sabtty* at sab* */
    {&sabtty_ca,	&sabtty_cd,	 0, STAR,     loc,    0, pv+241, 239,    0},
/*224: audioce* at ebus*|ebus* addr -1 */
    {&audioce_ca,	&audioce_cd,	 0, STAR, loc+  5,    0, pv+151, 222,    0},
/*225: wbsd* at ebus*|ebus* addr -1 */
    {&wbsd_ebus_ca,	&wbsd_cd,	 0, STAR, loc+  5,    0, pv+151, 222,    0},
/*226: pckbc* at ebus*|ebus* addr -1 */
    {&pckbc_ebus_ca,	&pckbc_cd,	 0, STAR, loc+  5,    0, pv+151, 222,    0},
/*227: comkbd* at ebus*|ebus* addr -1 */
    {&comkbd_ca,	&comkbd_cd,	 0, STAR, loc+  5,    0, pv+151, 222,    0},
/*228: comms* at ebus*|ebus* addr -1 */
    {&comms_ca,		&comms_cd,	 0, STAR, loc+  5,    0, pv+151, 222,    0},
/*229: lpt* at ebus*|ebus* addr -1 */
    {&lpt_ebus_ca,	&lpt_cd,	 0, STAR, loc+  5,    0, pv+151, 222,    0},
/*230: zs* at sbus*|sbus* slot -1 offset -1 */
    {&zs_sbus_ca,	&zs_cd,		 0, STAR, loc+  4,    0, pv+142, 102,    0},
/*231: zs* at fhc0|fhc* */
    {&zs_fhc_ca,	&zs_cd,		 0, STAR,     loc,    0, pv+163, 2,    0},
/*232: zstty* at zs*|zs* channel -1 */
    {&zstty_ca,		&zstty_cd,	 0, STAR, loc+  5,    0, pv+154, 200,    0},
/*233: zskbd* at zs*|zs* channel -1 */
    {&zskbd_ca,		&zskbd_cd,	 0, STAR, loc+  5,    0, pv+154, 200,    0},
/*234: fdc* at sbus*|sbus* slot -1 offset -1 */
    {&fdc_sbus_ca,	&fdc_cd,	 0, STAR, loc+  4,    0, pv+142, 102,    0},
/*235: fd* at fdc* */
    {&fd_ca,		&fd_cd,		 0, STAR,     loc,    0, pv+251, 239,    0},
/*236: pcfiic* at ebus*|ebus* addr -1 */
    {&pcfiic_ebus_ca,	&pcfiic_cd,	 0, STAR, loc+  5,    0, pv+151, 222,    0},
/*237: scsibus* at softraid0|vscsi0|vdsk*|umass*|sdmmc*|pcscp*|mpii*|qle*|arc*|atapiscsi*|mpath0|siop*|esp*|esp*|sili*|mpi*|nvme*|ahci*|ahci*|qla*|qla*|qlw*|qlw* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+38, 239,    0},
/*238: cd* at scsibus* target -1 lun -1 */
    {&cd_ca,		&cd_cd,		 0, STAR, loc+  4,    0, pv+179, 240,    0},
/*239: ch* at scsibus* target -1 lun -1 */
    {&ch_ca,		&ch_cd,		 0, STAR, loc+  4,    0, pv+179, 240,    0},
/*240: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+  4,    0, pv+179, 240,    0},
/*241: st* at scsibus* target -1 lun -1 */
    {&st_ca,		&st_cd,		 0, STAR, loc+  4,    0, pv+179, 240,    0},
/*242: uk* at scsibus* target -1 lun -1 */
    {&uk_ca,		&uk_cd,		 0, STAR, loc+  4,    0, pv+179, 240,    0},
/*243: safte* at scsibus* target -1 lun -1 */
    {&safte_ca,		&safte_cd,	 0, STAR, loc+  4,    0, pv+179, 240,    0},
/*244: ses* at scsibus* target -1 lun -1 */
    {&ses_ca,		&ses_cd,	 0, STAR, loc+  4,    0, pv+179, 240,    0},
/*245: sym* at scsibus* target -1 lun -1 */
    {&sym_ca,		&sym_cd,	 0, STAR, loc+  4,    0, pv+179, 240,    0},
/*246: rdac* at scsibus* target -1 lun -1 */
    {&rdac_ca,		&rdac_cd,	 0, STAR, loc+  4,    0, pv+179, 240,    0},
/*247: emc* at scsibus* target -1 lun -1 */
    {&emc_ca,		&emc_cd,	 0, STAR, loc+  4,    0, pv+179, 240,    0},
/*248: hds* at scsibus* target -1 lun -1 */
    {&hds_ca,		&hds_cd,	 0, STAR, loc+  4,    0, pv+179, 240,    0},
/*249: iic* at pcfiic*|alipm* */
    {&iic_ca,		&iic_cd,	 0, STAR,     loc,    0, pv+166, 242,    0},
/*250: lmtemp* at iic* addr -1 size -1 */
    {&lmtemp_ca,	&lmtemp_cd,	 0, STAR, loc+  4,    0, pv+181, 243,    0},
/*251: lmenv* at iic* addr -1 size -1 */
    {&lmenv_ca,		&lmenv_cd,	 0, STAR, loc+  4,    0, pv+181, 243,    0},
/*252: maxtmp* at iic* addr -1 size -1 */
    {&maxtmp_ca,	&maxtmp_cd,	 0, STAR, loc+  4,    0, pv+181, 243,    0},
/*253: adc* at iic* addr -1 size -1 */
    {&adc_ca,		&adc_cd,	 0, STAR, loc+  4,    0, pv+181, 243,    0},
/*254: admtemp* at iic* addr -1 size -1 */
    {&admtemp_ca,	&admtemp_cd,	 0, STAR, loc+  4,    0, pv+181, 243,    0},
/*255: admlc* at iic* addr -1 size -1 */
    {&admlc_ca,		&admlc_cd,	 0, STAR, loc+  4,    0, pv+181, 243,    0},
/*256: admtm* at iic* addr -1 size -1 */
    {&admtm_ca,		&admtm_cd,	 0, STAR, loc+  4,    0, pv+181, 243,    0},
/*257: admtmp* at iic* addr -1 size -1 */
    {&admtmp_ca,	&admtmp_cd,	 0, STAR, loc+  4,    0, pv+181, 243,    0},
/*258: admtt* at iic* addr -1 size -1 */
    {&admtt_ca,		&admtt_cd,	 0, STAR, loc+  4,    0, pv+181, 243,    0},
/*259: maxds* at iic* addr -1 size -1 */
    {&maxds_ca,		&maxds_cd,	 0, STAR, loc+  4,    0, pv+181, 243,    0},
/*260: adt* at iic* addr -1 size -1 */
    {&adt_ca,		&adt_cd,	 0, STAR, loc+  4,    0, pv+181, 243,    0},
/*261: lm* at iic* addr -1 size -1 */
    {&lm_i2c_ca,	&lm_cd,		 0, STAR, loc+  4,    0, pv+181, 243,    0},
/*262: spdmem* at iic* addr -1 size -1 */
    {&spdmem_iic_ca,	&spdmem_cd,	 0, STAR, loc+  4,    0, pv+181, 243,    0},
/*263: sdmmc* at wbsd* */
    {&sdmmc_ca,		&sdmmc_cd,	 0, STAR,     loc,    0, pv+173, 245,    0},
/*264: usb* at ohci*|ohci*|uhci*|ehci*|ehci*|xhci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+111, 245,    0},
/*265: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+205, 245,    0},
/*266: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*267: uaudio* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uaudio_ca,	&uaudio_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*268: uvideo* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvideo_ca,	&uvideo_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*269: utvfu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&utvfu_ca,		&utvfu_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*270: umidi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umidi_ca,		&umidi_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*271: ucom* at umodem*|uvisor*|uvscom*|ubsa*|uftdi*|uplcom*|umct*|uslcom*|uscom*|ucrcom*|uark*|uipaq*|uchcom*|ucycom*|uslhcom*|umsm* portno -1 */
    {&ucom_ca,		&ucom_cd,	 0, STAR, loc+  5,    0, pv+83, 246,    0},
/*272: ugen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugen_ca,		&ugen_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*273: uhidev* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhidev_ca,	&uhidev_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*274: uhid* at uhidev* reportid -1 */
    {&uhid_ca,		&uhid_cd,	 0, STAR, loc+  5,    0, pv+209, 278,    0},
/*275: fido* at uhidev* reportid -1 */
    {&fido_ca,		&fido_cd,	 0, STAR, loc+  5,    0, pv+209, 278,    0},
/*276: ujoy* at uhidev* reportid -1 */
    {&ujoy_ca,		&ujoy_cd,	 0, STAR, loc+  5,    0, pv+209, 278,    0},
/*277: ukbd* at uhidev* reportid -1 */
    {&ukbd_ca,		&ukbd_cd,	 0, STAR, loc+  5,    0, pv+209, 278,    0},
/*278: ums* at uhidev* reportid -1 */
    {&ums_ca,		&ums_cd,	 0, STAR, loc+  5,    0, pv+209, 278,    0},
/*279: ucycom* at uhidev* reportid -1 */
    {&ucycom_ca,	&ucycom_cd,	 0, STAR, loc+  5,    0, pv+209, 278,    0},
/*280: uslhcom* at uhidev* reportid -1 */
    {&uslhcom_ca,	&uslhcom_cd,	 0, STAR, loc+  5,    0, pv+209, 278,    0},
/*281: ulpt* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ulpt_ca,		&ulpt_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*282: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*283: uthum* at uhidev* reportid -1 */
    {&uthum_ca,		&uthum_cd,	 0, STAR, loc+  5,    0, pv+209, 278,    0},
/*284: ugold* at uhidev* reportid -1 */
    {&ugold_ca,		&ugold_cd,	 0, STAR, loc+  5,    0, pv+209, 278,    0},
/*285: uonerng* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uonerng_ca,	&uonerng_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*286: urng* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urng_ca,		&urng_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*287: udcf* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udcf_ca,		&udcf_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*288: umbg* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umbg_ca,		&umbg_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*289: uvisor* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvisor_ca,	&uvisor_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*290: udsbr* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udsbr_ca,		&udsbr_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*291: utwitch* at uhidev* reportid -1 */
    {&utwitch_ca,	&utwitch_cd,	 0, STAR, loc+  5,    0, pv+209, 278,    0},
/*292: aue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&aue_ca,		&aue_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*293: axe* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axe_ca,		&axe_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*294: axen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axen_ca,		&axen_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*295: smsc* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&smsc_ca,		&smsc_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*296: cue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cue_ca,		&cue_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*297: kue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&kue_ca,		&kue_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*298: cdce* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cdce_ca,		&cdce_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*299: urndis* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urndis_ca,	&urndis_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*300: mos* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mos_ca,		&mos_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*301: udav* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udav_ca,		&udav_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*302: upl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&upl_ca,		&upl_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*303: ugl* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ugl_ca,		&ugl_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*304: url* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&url_ca,		&url_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*305: ure* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ure_ca,		&ure_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*306: umodem* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umodem_ca,	&umodem_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*307: uftdi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uftdi_ca,		&uftdi_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*308: uplcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uplcom_ca,	&uplcom_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*309: umct* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umct_ca,		&umct_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*310: uvscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uvscom_ca,	&uvscom_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*311: ubsa* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ubsa_ca,		&ubsa_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*312: uslcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uslcom_ca,	&uslcom_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*313: uark* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uark_ca,		&uark_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*314: uscom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uscom_ca,		&uscom_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*315: ucrcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ucrcom_ca,	&ucrcom_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*316: uipaq* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uipaq_ca,		&uipaq_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*317: umsm* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umsm_ca,		&umsm_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*318: uchcom* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uchcom_ca,	&uchcom_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*319: wi* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&wi_usb_ca,	&wi_cd,		 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*320: atu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&atu_ca,		&atu_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*321: ural* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ural_ca,		&ural_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*322: rum* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rum_ca,		&rum_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*323: run* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&run_ca,		&run_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*324: zyd* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&zyd_ca,		&zyd_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*325: urtwn* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&urtwn_ca,		&urtwn_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*326: rsu* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&rsu_ca,		&rsu_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*327: uath* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uath_ca,		&uath_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*328: uow* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uow_ca,		&uow_cd,	 0, STAR, loc+  0,    0, pv+139, 226,    0},
/*329: upd* at uhidev* reportid -1 */
    {&upd_ca,		&upd_cd,	 0, STAR, loc+  5,    0, pv+209, 278,    0},
/*330: uhidpp* at uhidev* reportid -1 */
    {&uhidpp_ca,	&uhidpp_cd,	 0, STAR, loc+  5,    0, pv+209, 278,    0},
/*331: ucc* at uhidev* reportid -1 */
    {&ucc_ca,		&ucc_cd,	 0, STAR, loc+  5,    0, pv+209, 278,    0},
/*332: cardslot* at cbb* slot -1 */
    {&cardslot_ca,	&cardslot_cd,	 0, STAR, loc+  5,    0, pv+211, 280,    0},
/*333: cardbus* at cardslot* slot -1 */
    {&cardbus_ca,	&cardbus_cd,	 0, STAR, loc+  5,    0, pv+213, 194,    0},
/*334: pcfadc* at iic* addr -1 size -1 */
    {&pcfadc_ca,	&pcfadc_cd,	 0, STAR, loc+  4,    0, pv+181, 243,    0},
/*335: ecadc* at iic* addr -1 size -1 */
    {&ecadc_ca,		&ecadc_cd,	 0, STAR, loc+  4,    0, pv+181, 243,    0},
/*336: tda* at iic* addr -1 size -1 */
    {&tda_ca,		&tda_cd,	 0, STAR, loc+  4,    0, pv+181, 243,    0},
/*337: onewire* at uow* */
    {&onewire_ca,	&onewire_cd,	 0, STAR,     loc,    0, pv+201, 281,    0},
/*338: owid* at onewire* */
    {&owid_ca,		&owid_cd,	 0, STAR,     loc,    0, pv+203, 281,    0},
/*339: owsbm* at onewire* */
    {&owsbm_ca,		&owsbm_cd,	 0, STAR,     loc,    0, pv+203, 281,    0},
/*340: owtemp* at onewire* */
    {&owtemp_ca,	&owtemp_cd,	 0, STAR,     loc,    0, pv+203, 281,    0},
/*341: owctr* at onewire* */
    {&owctr_ca,		&owctr_cd,	 0, STAR,     loc,    0, pv+203, 281,    0},
/*342: vcons0 at vbus0 */
    {&vcons_ca,		&vcons_cd,	 0, NORM,     loc,    0, pv+207, 2,    0},
/*343: vrng0 at vbus0 */
    {&vrng_ca,		&vrng_cd,	 0, NORM,     loc,    0, pv+207, 2,    0},
/*344: vrtc0 at vbus0 */
    {&vrtc_ca,		&vrtc_cd,	 0, NORM,     loc,    0, pv+207, 2,    0},
/*345: vds* at cbus* */
    {&vds_ca,		&vds_cd,	 0, STAR,     loc,    0, pv+195, 281,    0},
/*346: vdsp* at vds* */
    {&vdsp_ca,		&vdsp_cd,	 0, STAR,     loc,    0, pv+219, 281,    0},
/*347: vdsk* at cbus* */
    {&vdsk_ca,		&vdsk_cd,	 0, STAR,     loc,    0, pv+195, 281,    0},
/*348: vsw* at cbus* */
    {&vsw_ca,		&vsw_cd,	 0, STAR,     loc,    0, pv+195, 281,    0},
/*349: vnet* at cbus* */
    {&vnet_ca,		&vnet_cd,	 0, STAR,     loc,    0, pv+195, 281,    0},
/*350: vnet* at vsw* */
    {&vnet_ca,		&vnet_cd,	 0, STAR,     loc,    0, pv+247, 281,    0},
/*351: vcc* at cbus* */
    {&vcc_ca,		&vcc_cd,	 0, STAR,     loc,    0, pv+195, 281,    0},
/*352: vcctty* at vcc* */
    {&vcctty_ca,	&vcctty_cd,	 0, STAR,     loc,    0, pv+193, 281,    0},
/*353: vldc* at cbus* */
    {&vldc_ca,		&vldc_cd,	 0, STAR,     loc,    0, pv+195, 281,    0},
/*354: vldcp* at vldc* */
    {&vldcp_ca,		&vldcp_cd,	 0, STAR,     loc,    0, pv+197, 281,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 5 /* vscsi0 */,
	 6 /* mpath0 */,
	 7 /* softraid0 */,
	 8 /* mainbus0 */,
	-1
};

int cfroots_size = 5;

/* pseudo-devices */
extern void pfattach(int);
extern void pflogattach(int);
extern void pfsyncattach(int);
extern void pflowattach(int);
extern void encattach(int);
extern void ptyattach(int);
extern void nmeaattach(int);
extern void mstsattach(int);
extern void endrunattach(int);
extern void vndattach(int);
extern void ksymsattach(int);
extern void bpfilterattach(int);
extern void bridgeattach(int);
extern void vebattach(int);
extern void carpattach(int);
extern void etheripattach(int);
extern void gifattach(int);
extern void greattach(int);
extern void loopattach(int);
extern void mpeattach(int);
extern void mpwattach(int);
extern void mpipattach(int);
extern void bpeattach(int);
extern void pairattach(int);
extern void pppattach(int);
extern void pppoeattach(int);
extern void pppxattach(int);
extern void spppattach(int);
extern void trunkattach(int);
extern void aggrattach(int);
extern void tpmrattach(int);
extern void tunattach(int);
extern void vetherattach(int);
extern void vxlanattach(int);
extern void vlanattach(int);
extern void switchattach(int);
extern void wgattach(int);
extern void bioattach(int);
extern void fuseattach(int);
extern void hotplugattach(int);
extern void dtattach(int);
extern void wsmuxattach(int);

char *pdevnames[] = {
	"pf",
	"pflog",
	"pfsync",
	"pflow",
	"enc",
	"pty",
	"nmea",
	"msts",
	"endrun",
	"vnd",
	"ksyms",
	"bpfilter",
	"bridge",
	"veb",
	"carp",
	"etherip",
	"gif",
	"gre",
	"loop",
	"mpe",
	"mpw",
	"mpip",
	"bpe",
	"pair",
	"ppp",
	"pppoe",
	"pppx",
	"sppp",
	"trunk",
	"aggr",
	"tpmr",
	"tun",
	"vether",
	"vxlan",
	"vlan",
	"switch",
	"wg",
	"bio",
	"fuse",
	"hotplug",
	"dt",
	"wsmux",
};

int pdevnames_size = 42;

struct pdevinit pdevinit[] = {
	{ pfattach, 1 },
	{ pflogattach, 1 },
	{ pfsyncattach, 1 },
	{ pflowattach, 1 },
	{ encattach, 1 },
	{ ptyattach, 16 },
	{ nmeaattach, 1 },
	{ mstsattach, 1 },
	{ endrunattach, 1 },
	{ vndattach, 4 },
	{ ksymsattach, 1 },
	{ bpfilterattach, 1 },
	{ bridgeattach, 1 },
	{ vebattach, 1 },
	{ carpattach, 1 },
	{ etheripattach, 1 },
	{ gifattach, 1 },
	{ greattach, 1 },
	{ loopattach, 1 },
	{ mpeattach, 1 },
	{ mpwattach, 1 },
	{ mpipattach, 1 },
	{ bpeattach, 1 },
	{ pairattach, 1 },
	{ pppattach, 1 },
	{ pppoeattach, 1 },
	{ pppxattach, 1 },
	{ spppattach, 1 },
	{ trunkattach, 1 },
	{ aggrattach, 1 },
	{ tpmrattach, 1 },
	{ tunattach, 1 },
	{ vetherattach, 1 },
	{ vxlanattach, 1 },
	{ vlanattach, 1 },
	{ switchattach, 1 },
	{ wgattach, 1 },
	{ bioattach, 1 },
	{ fuseattach, 1 },
	{ hotplugattach, 1 },
	{ dtattach, 1 },
	{ wsmuxattach, 2 },
	{ NULL, 0 }
};
