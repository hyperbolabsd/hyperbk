/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/sparc64/conf/RAMDISK"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver ahc_cd;
extern struct cfdriver qlw_cd;
extern struct cfdriver qla_cd;
extern struct cfdriver ahci_cd;
extern struct cfdriver nvme_cd;
extern struct cfdriver mpi_cd;
extern struct cfdriver sili_cd;
extern struct cfdriver esp_cd;
extern struct cfdriver siop_cd;
extern struct cfdriver ep_cd;
extern struct cfdriver wi_cd;
extern struct cfdriver le_cd;
extern struct cfdriver xl_cd;
extern struct cfdriver fxp_cd;
extern struct cfdriver rl_cd;
extern struct cfdriver re_cd;
extern struct cfdriver dc_cd;
extern struct cfdriver epic_cd;
extern struct cfdriver ne_cd;
extern struct cfdriver gem_cd;
extern struct cfdriver ti_cd;
extern struct cfdriver com_cd;
extern struct cfdriver pckbc_cd;
extern struct cfdriver ath_cd;
extern struct cfdriver athn_cd;
extern struct cfdriver uhci_cd;
extern struct cfdriver ohci_cd;
extern struct cfdriver ehci_cd;
extern struct cfdriver xhci_cd;
extern struct cfdriver softraid_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver ssm_cd;
extern struct cfdriver upa_cd;
extern struct cfdriver central_cd;
extern struct cfdriver fhc_cd;
extern struct cfdriver vbus_cd;
extern struct cfdriver cbus_cd;
extern struct cfdriver hme_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver pcons_cd;
extern struct cfdriver prtc_cd;
extern struct cfdriver sbus_cd;
extern struct cfdriver dma_cd;
extern struct cfdriver ledma_cd;
extern struct cfdriver lebuffer_cd;
extern struct cfdriver qec_cd;
extern struct cfdriver be_cd;
extern struct cfdriver qe_cd;
extern struct cfdriver agten_cd;
extern struct cfdriver bwtwo_cd;
extern struct cfdriver cgsix_cd;
extern struct cfdriver cgthree_cd;
extern struct cfdriver cgtwelve_cd;
extern struct cfdriver mgx_cd;
extern struct cfdriver rfx_cd;
extern struct cfdriver tvtwo_cd;
extern struct cfdriver vigra_cd;
extern struct cfdriver zx_cd;
extern struct cfdriver xbox_cd;
extern struct cfdriver nsphy_cd;
extern struct cfdriver nsphyter_cd;
extern struct cfdriver gentbi_cd;
extern struct cfdriver qsphy_cd;
extern struct cfdriver inphy_cd;
extern struct cfdriver iophy_cd;
extern struct cfdriver eephy_cd;
extern struct cfdriver exphy_cd;
extern struct cfdriver rlphy_cd;
extern struct cfdriver lxtphy_cd;
extern struct cfdriver luphy_cd;
extern struct cfdriver icsphy_cd;
extern struct cfdriver sqphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver dcphy_cd;
extern struct cfdriver bmtphy_cd;
extern struct cfdriver brgphy_cd;
extern struct cfdriver xmphy_cd;
extern struct cfdriver amphy_cd;
extern struct cfdriver acphy_cd;
extern struct cfdriver nsgphy_cd;
extern struct cfdriver urlphy_cd;
extern struct cfdriver rgephy_cd;
extern struct cfdriver ipgphy_cd;
extern struct cfdriver jmphy_cd;
extern struct cfdriver atapiscsi_cd;
extern struct cfdriver wd_cd;
extern struct cfdriver pckbd_cd;
extern struct cfdriver pcmcia_cd;
extern struct cfdriver zs_cd;
extern struct cfdriver creator_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver arc_cd;
extern struct cfdriver jmb_cd;
extern struct cfdriver qle_cd;
extern struct cfdriver mpii_cd;
extern struct cfdriver de_cd;
extern struct cfdriver pcn_cd;
extern struct cfdriver pciide_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver vr_cd;
extern struct cfdriver em_cd;
extern struct cfdriver ix_cd;
extern struct cfdriver ixl_cd;
extern struct cfdriver cas_cd;
extern struct cfdriver nep_cd;
extern struct cfdriver cbb_cd;
extern struct cfdriver skc_cd;
extern struct cfdriver sk_cd;
extern struct cfdriver mskc_cd;
extern struct cfdriver msk_cd;
extern struct cfdriver bge_cd;
extern struct cfdriver bnx_cd;
extern struct cfdriver stge_cd;
extern struct cfdriver jme_cd;
extern struct cfdriver psycho_cd;
extern struct cfdriver schizo_cd;
extern struct cfdriver pyro_cd;
extern struct cfdriver vpci_cd;
extern struct cfdriver vgafb_cd;
extern struct cfdriver machfb_cd;
extern struct cfdriver radeonfb_cd;
extern struct cfdriver ifb_cd;
extern struct cfdriver raptor_cd;
extern struct cfdriver gfxp_cd;
extern struct cfdriver sbbc_cd;
extern struct cfdriver ebus_cd;
extern struct cfdriver clock_cd;
extern struct cfdriver timer_cd;
extern struct cfdriver cmp_cd;
extern struct cfdriver core_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver auxio_cd;
extern struct cfdriver rtc_cd;
extern struct cfdriver sab_cd;
extern struct cfdriver sabtty_cd;
extern struct cfdriver comkbd_cd;
extern struct cfdriver zstty_cd;
extern struct cfdriver zskbd_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver cd_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver uhidev_cd;
extern struct cfdriver ukbd_cd;
extern struct cfdriver umass_cd;
extern struct cfdriver aue_cd;
extern struct cfdriver axe_cd;
extern struct cfdriver axen_cd;
extern struct cfdriver smsc_cd;
extern struct cfdriver cue_cd;
extern struct cfdriver kue_cd;
extern struct cfdriver cdce_cd;
extern struct cfdriver mos_cd;
extern struct cfdriver udav_cd;
extern struct cfdriver url_cd;
extern struct cfdriver ure_cd;
extern struct cfdriver cardslot_cd;
extern struct cfdriver cardbus_cd;
extern struct cfdriver vcons_cd;
extern struct cfdriver vrng_cd;
extern struct cfdriver vrtc_cd;
extern struct cfdriver vdsk_cd;
extern struct cfdriver vnet_cd;

extern struct cfattach softraid_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach ssm_ca;
extern struct cfattach upa_ca;
extern struct cfattach central_ca;
extern struct cfattach fhc_central_ca;
extern struct cfattach fhc_mainbus_ca;
extern struct cfattach vbus_ca;
extern struct cfattach cbus_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach pcons_ca;
extern struct cfattach prtc_ca;
extern struct cfattach dma_sbus_ca;
extern struct cfattach ledma_ca;
extern struct cfattach lebuffer_ca;
extern struct cfattach le_sbus_ca;
extern struct cfattach le_lebuffer_ca;
extern struct cfattach le_ledma_ca;
extern struct cfattach qec_ca;
extern struct cfattach be_ca;
extern struct cfattach qe_ca;
extern struct cfattach esp_sbus_ca;
extern struct cfattach esp_dma_ca;
extern struct cfattach hme_sbus_ca;
extern struct cfattach ti_sbus_ca;
extern struct cfattach gem_sbus_ca;
extern struct cfattach qlw_sbus_ca;
extern struct cfattach qla_sbus_ca;
extern struct cfattach agten_ca;
extern struct cfattach bwtwo_ca;
extern struct cfattach cgsix_ca;
extern struct cfattach cgthree_ca;
extern struct cfattach cgtwelve_ca;
extern struct cfattach mgx_ca;
extern struct cfattach rfx_ca;
extern struct cfattach tvtwo_ca;
extern struct cfattach vigra_ca;
extern struct cfattach zx_ca;
extern struct cfattach xbox_ca;
extern struct cfattach nsphy_ca;
extern struct cfattach nsphyter_ca;
extern struct cfattach gentbi_ca;
extern struct cfattach qsphy_ca;
extern struct cfattach inphy_ca;
extern struct cfattach iophy_ca;
extern struct cfattach eephy_ca;
extern struct cfattach exphy_ca;
extern struct cfattach rlphy_ca;
extern struct cfattach lxtphy_ca;
extern struct cfattach luphy_ca;
extern struct cfattach icsphy_ca;
extern struct cfattach sqphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach dcphy_ca;
extern struct cfattach bmtphy_ca;
extern struct cfattach brgphy_ca;
extern struct cfattach xmphy_ca;
extern struct cfattach amphy_ca;
extern struct cfattach acphy_ca;
extern struct cfattach nsgphy_ca;
extern struct cfattach urlphy_ca;
extern struct cfattach rgephy_ca;
extern struct cfattach ipgphy_ca;
extern struct cfattach jmphy_ca;
extern struct cfattach atapiscsi_ca;
extern struct cfattach wd_ca;
extern struct cfattach pckbd_ca;
extern struct cfattach pcmcia_ca;
extern struct cfattach ep_pcmcia_ca;
extern struct cfattach ne_pcmcia_ca;
extern struct cfattach com_pcmcia_ca;
extern struct cfattach wi_pcmcia_ca;
extern struct cfattach sbus_mb_ca;
extern struct cfattach sbus_xbox_ca;
extern struct cfattach creator_ca;
extern struct cfattach pci_ca;
extern struct cfattach ahc_pci_ca;
extern struct cfattach arc_ca;
extern struct cfattach jmb_ca;
extern struct cfattach ahci_pci_ca;
extern struct cfattach ahci_jmb_ca;
extern struct cfattach nvme_pci_ca;
extern struct cfattach qlw_pci_ca;
extern struct cfattach qla_pci_ca;
extern struct cfattach qle_ca;
extern struct cfattach mpi_pci_ca;
extern struct cfattach mpii_ca;
extern struct cfattach sili_pci_ca;
extern struct cfattach de_ca;
extern struct cfattach pcn_ca;
extern struct cfattach siop_pci_ca;
extern struct cfattach pciide_pci_ca;
extern struct cfattach pciide_jmb_ca;
extern struct cfattach ppb_ca;
extern struct cfattach rl_pci_ca;
extern struct cfattach re_pci_ca;
extern struct cfattach vr_ca;
extern struct cfattach xl_pci_ca;
extern struct cfattach fxp_pci_ca;
extern struct cfattach em_ca;
extern struct cfattach ix_ca;
extern struct cfattach ixl_ca;
extern struct cfattach dc_pci_ca;
extern struct cfattach epic_pci_ca;
extern struct cfattach ti_pci_ca;
extern struct cfattach ne_pci_ca;
extern struct cfattach gem_pci_ca;
extern struct cfattach cas_ca;
extern struct cfattach nep_ca;
extern struct cfattach uhci_pci_ca;
extern struct cfattach ohci_pci_ca;
extern struct cfattach ehci_pci_ca;
extern struct cfattach xhci_pci_ca;
extern struct cfattach cbb_pci_ca;
extern struct cfattach skc_ca;
extern struct cfattach sk_ca;
extern struct cfattach mskc_ca;
extern struct cfattach msk_ca;
extern struct cfattach wi_pci_ca;
extern struct cfattach bge_ca;
extern struct cfattach bnx_ca;
extern struct cfattach stge_ca;
extern struct cfattach jme_ca;
extern struct cfattach ath_pci_ca;
extern struct cfattach athn_pci_ca;
extern struct cfattach psycho_ca;
extern struct cfattach schizo_ca;
extern struct cfattach pyro_ca;
extern struct cfattach vpci_ca;
extern struct cfattach hme_pci_ca;
extern struct cfattach vgafb_ca;
extern struct cfattach machfb_ca;
extern struct cfattach radeonfb_ca;
extern struct cfattach ifb_ca;
extern struct cfattach raptor_ca;
extern struct cfattach gfxp_ca;
extern struct cfattach sbbc_ca;
extern struct cfattach ebus_ca;
extern struct cfattach ebus_mainbus_ca;
extern struct cfattach clock_sbus_ca;
extern struct cfattach clock_ebus_ca;
extern struct cfattach clock_fhc_ca;
extern struct cfattach timer_ca;
extern struct cfattach cmp_ca;
extern struct cfattach core_ca;
extern struct cfattach cpu_ca;
extern struct cfattach auxio_ebus_ca;
extern struct cfattach auxio_sbus_ca;
extern struct cfattach rtc_ca;
extern struct cfattach sab_ca;
extern struct cfattach sabtty_ca;
extern struct cfattach com_ebus_ca;
extern struct cfattach pckbc_ebus_ca;
extern struct cfattach comkbd_ca;
extern struct cfattach zs_sbus_ca;
extern struct cfattach zs_fhc_ca;
extern struct cfattach zstty_ca;
extern struct cfattach zskbd_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach cd_ca;
extern struct cfattach sd_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach uhidev_ca;
extern struct cfattach ukbd_ca;
extern struct cfattach umass_ca;
extern struct cfattach aue_ca;
extern struct cfattach axe_ca;
extern struct cfattach axen_ca;
extern struct cfattach smsc_ca;
extern struct cfattach cue_ca;
extern struct cfattach kue_ca;
extern struct cfattach cdce_ca;
extern struct cfattach mos_ca;
extern struct cfattach udav_ca;
extern struct cfattach url_ca;
extern struct cfattach ure_ca;
extern struct cfattach cardslot_ca;
extern struct cfattach cardbus_ca;
extern struct cfattach xl_cardbus_ca;
extern struct cfattach dc_cardbus_ca;
extern struct cfattach re_cardbus_ca;
extern struct cfattach ath_cardbus_ca;
extern struct cfattach athn_cardbus_ca;
extern struct cfattach ehci_cardbus_ca;
extern struct cfattach ohci_cardbus_ca;
extern struct cfattach vcons_ca;
extern struct cfattach vrng_ca;
extern struct cfattach vrtc_ca;
extern struct cfattach vdsk_ca;
extern struct cfattach vnet_ca;


/* locators */
static long loc[9] = {
	-1, -1, -1, -1, -1, -1, -1, -1,
	1,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"console",
	"primary",
	"mux",
	"slot",
	"offset",
	"dev",
	"function",
	"phy",
	"channel",
	"controller",
	"socket",
	"irq",
	"addr",
	"bus",
	"target",
	"lun",
	"port",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"reportid",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, 1, 2, -1, 0, 1, 2,
	-1, 0, 1, 2, -1, 0, 1, 2,
	-1, 0, 1, 2, -1, 0, 1, 2,
	-1, 0, 1, 2, -1, 0, 1, 2,
	-1, 0, 1, 2, -1, 0, 1, 2,
	-1, 0, 1, 2, -1, 0, 1, 2,
	-1, 0, 1, 2, -1, 0, 1, 2,
	-1, 0, 1, 2, -1, 0, 1, 2,
	-1, 0, 1, 2, -1, 0, 1, 2,
	-1, 0, 1, 2, -1, 0, 2, -1,
	0, 2, -1, 0, 2, -1, 0, 2,
	-1, 3, 4, -1, 3, 4, -1, 5,
	6, -1, 7, -1, 7, -1, 7, -1,
	7, -1, 7, -1, 7, -1, 7, -1,
	7, -1, 7, -1, 7, -1, 7, -1,
	7, -1, 7, -1, 7, -1, 7, -1,
	7, -1, 7, -1, 7, -1, 7, -1,
	7, -1, 7, -1, 7, -1, 7, -1,
	7, -1, 7, -1, 7, -1, 7, -1,
	7, -1, 7, -1, 7, -1, 7, -1,
	7, -1, 7, -1, 7, -1, 7, -1,
	7, -1, 7, -1, 8, -1, 8, -1,
	3, -1, 9, 10, -1, 6, 11, -1,
	12, -1, 12, -1, 13, -1, 13, -1,
	13, -1, 13, -1, 13, -1, 5, 6,
	-1, 8, -1, 8, -1, 14, 15, -1,
	16, 17, 18, 19, 20, 21, -1, 16,
	17, 18, 19, 20, 21, -1, 22, -1,
	3, -1,
};

/* size of parent vectors */
int pv_size = 168;

/* parent vectors */
short pv[168] = {
	188, 187, 186, 185, 181, 180, 179, 178, 130, 129, 128, 127, 125, 123, 114, 113,
	101, 93, 20, 139, 24, 111, 25, 112, 26, 72, 73, 110, 108, 109, 99, 100,
	98, 104, 102, 103, 71, -1, 194, 177, 90, 88, 83, 67, 0, 94, 23, 22,
	91, 89, 87, 86, 85, 29, 30, 27, 28, 82, -1, 31, 32, 33, 34, 35,
	79, 80, 145, 143, 141, 36, 142, 144, 37, 38, 140, 39, 40, 11, -1, 116,
	117, 115, 118, 119, 120, -1, 135, 136, 137, 138, 97, -1, 176, 164, 168, 69,
	-1, 173, 174, -1, 77, 78, -1, 1, 2, -1, 148, 147, -1, 165, 166, -1,
	95, 96, -1, 5, 6, -1, 7, -1, 1, -1, 8, -1, 122, -1, 172, -1,
	124, -1, 19, -1, 13, -1, 41, -1, 3, -1, 153, -1, 84, -1, 4, -1,
	81, -1, 189, -1, 70, -1, 190, -1, 169, -1, 163, -1, 175, -1, 121, -1,
	154, -1, 15, -1, 161, -1, 14, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: softraid0 at root */
    {&softraid_ca,	&softraid_cd,	 0, NORM,     loc,    0, pv+37, 0,    0},
/*  1: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+37, 0,    0},
/*  2: ssm* at mainbus0 */
    {&ssm_ca,		&ssm_cd,	 0, STAR,     loc,    0, pv+120, 88,    0},
/*  3: upa0 at mainbus0 */
    {&upa_ca,		&upa_cd,	 0, NORM,     loc,    0, pv+120, 88,    0},
/*  4: central0 at mainbus0 */
    {&central_ca,	&central_cd,	 0, NORM,     loc,    0, pv+120, 88,    0},
/*  5: fhc0 at central0 */
    {&fhc_central_ca,	&fhc_cd,	 0, NORM,     loc,    0, pv+142, 0,    0},
/*  6: fhc* at mainbus0 */
    {&fhc_mainbus_ca,	&fhc_cd,	 1, STAR,     loc,    0, pv+120, 88,    1},
/*  7: vbus0 at mainbus0 */
    {&vbus_ca,		&vbus_cd,	 0, NORM,     loc,    0, pv+120, 88,    0},
/*  8: cbus* at vbus0 */
    {&cbus_ca,		&cbus_cd,	 0, STAR,     loc,    0, pv+118, 225,    0},
/*  9: wsdisplay* at agten*|bwtwo*|cgsix*|cgthree*|cgtwelve*|creator*|creator*|gfxp*|ifb*|machfb*|mgx*|radeonfb*|raptor*|rfx*|tvtwo*|vgafb*|vigra*|zx*|pcons0 console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+  6,    0, pv+59, 1,    0},
/* 10: wskbd* at ukbd*|comkbd*|zskbd*|pckbd* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+  7,    0, pv+92, 77,    0},
/* 11: pcons0 at mainbus0 */
    {&pcons_ca,		&pcons_cd,	 0, NORM,     loc,    0, pv+120, 88,    0},
/* 12: prtc0 at mainbus0 */
    {&prtc_ca,		&prtc_cd,	 0, NORM,     loc,    0, pv+120, 88,    0},
/* 13: dma* at sbus*|sbus* slot -1 offset -1 */
    {&dma_sbus_ca,	&dma_cd,	 0, STAR, loc+  4,    0, pv+100, 89,    0},
/* 14: ledma* at sbus*|sbus* slot -1 offset -1 */
    {&ledma_ca,		&ledma_cd,	 0, STAR, loc+  4,    0, pv+100, 89,    0},
/* 15: lebuffer* at sbus*|sbus* slot -1 offset -1 */
    {&lebuffer_ca,	&lebuffer_cd,	 0, STAR, loc+  4,    0, pv+100, 89,    0},
/* 16: le* at sbus*|sbus* slot -1 offset -1 */
    {&le_sbus_ca,	&le_cd,		 0, STAR, loc+  4,    0, pv+100, 89,    0},
/* 17: le* at ledma* */
    {&le_ledma_ca,	&le_cd,		 0, STAR,     loc,    0, pv+166, 94,    0},
/* 18: le* at lebuffer* */
    {&le_lebuffer_ca,	&le_cd,		 0, STAR,     loc,    0, pv+162, 94,    0},
/* 19: qec* at sbus*|sbus* slot -1 offset -1 */
    {&qec_ca,		&qec_cd,	 0, STAR, loc+  4,    0, pv+100, 89,    0},
/* 20: be* at qec* */
    {&be_ca,		&be_cd,		 0, STAR,     loc,    0, pv+130, 94,    0},
/* 21: qe* at qec* */
    {&qe_ca,		&qe_cd,		 0, STAR,     loc,    0, pv+130, 94,    0},
/* 22: esp* at sbus*|sbus* slot -1 offset -1 */
    {&esp_sbus_ca,	&esp_cd,	 0, STAR, loc+  4,    0, pv+100, 89,    0},
/* 23: esp* at dma* */
    {&esp_dma_ca,	&esp_cd,	 0, STAR,     loc,    0, pv+132, 94,    0},
/* 24: hme* at sbus*|sbus* slot -1 offset -1 */
    {&hme_sbus_ca,	&hme_cd,	 0, STAR, loc+  4,    0, pv+100, 89,    0},
/* 25: ti* at sbus*|sbus* slot -1 offset -1 */
    {&ti_sbus_ca,	&ti_cd,		 0, STAR, loc+  4,    0, pv+100, 89,    0},
/* 26: gem* at sbus*|sbus* slot -1 offset -1 */
    {&gem_sbus_ca,	&gem_cd,	 0, STAR, loc+  4,    0, pv+100, 89,    0},
/* 27: qlw* at sbus*|sbus* slot -1 offset -1 */
    {&qlw_sbus_ca,	&qlw_cd,	 0, STAR, loc+  4,    0, pv+100, 89,    0},
/* 28: qlw* at pci* dev -1 function -1 */
    {&qlw_pci_ca,	&qlw_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/* 29: qla* at sbus*|sbus* slot -1 offset -1 */
    {&qla_sbus_ca,	&qla_cd,	 0, STAR, loc+  4,    0, pv+100, 89,    0},
/* 30: qla* at pci* dev -1 function -1 */
    {&qla_pci_ca,	&qla_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/* 31: agten* at sbus*|sbus* slot -1 offset -1 */
    {&agten_ca,		&agten_cd,	 0, STAR, loc+  4,    0, pv+100, 89,    0},
/* 32: bwtwo* at sbus*|sbus* slot -1 offset -1 */
    {&bwtwo_ca,		&bwtwo_cd,	 0, STAR, loc+  4,    0, pv+100, 89,    0},
/* 33: cgsix* at sbus*|sbus* slot -1 offset -1 */
    {&cgsix_ca,		&cgsix_cd,	 0, STAR, loc+  4,    0, pv+100, 89,    0},
/* 34: cgthree* at sbus*|sbus* slot -1 offset -1 */
    {&cgthree_ca,	&cgthree_cd,	 0, STAR, loc+  4,    0, pv+100, 89,    0},
/* 35: cgtwelve* at sbus*|sbus* slot -1 offset -1 */
    {&cgtwelve_ca,	&cgtwelve_cd,	 0, STAR, loc+  4,    0, pv+100, 89,    0},
/* 36: mgx* at sbus*|sbus* slot -1 offset -1 */
    {&mgx_ca,		&mgx_cd,	 0, STAR, loc+  4,    0, pv+100, 89,    0},
/* 37: rfx* at sbus*|sbus* slot -1 offset -1 */
    {&rfx_ca,		&rfx_cd,	 0, STAR, loc+  4,    0, pv+100, 89,    0},
/* 38: tvtwo* at sbus*|sbus* slot -1 offset -1 */
    {&tvtwo_ca,		&tvtwo_cd,	 0, STAR, loc+  4,    0, pv+100, 89,    0},
/* 39: vigra* at sbus*|sbus* slot -1 offset -1 */
    {&vigra_ca,		&vigra_cd,	 0, STAR, loc+  4,    0, pv+100, 89,    0},
/* 40: zx* at sbus*|sbus* slot -1 offset -1 */
    {&zx_ca,		&zx_cd,		 0, STAR, loc+  4,    0, pv+100, 89,    0},
/* 41: xbox* at sbus*|sbus* slot -1 offset -1 */
    {&xbox_ca,		&xbox_cd,	 0, STAR, loc+  4,    0, pv+100, 89,    0},
/* 42: nsphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&nsphy_ca,		&nsphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 98,    0},
/* 43: nsphyter* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&nsphyter_ca,	&nsphyter_cd,	 0, STAR, loc+  5,    0, pv+ 0, 98,    0},
/* 44: gentbi* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&gentbi_ca,	&gentbi_cd,	 0, STAR, loc+  5,    0, pv+ 0, 98,    0},
/* 45: qsphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&qsphy_ca,		&qsphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 98,    0},
/* 46: inphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&inphy_ca,		&inphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 98,    0},
/* 47: iophy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&iophy_ca,		&iophy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 98,    0},
/* 48: eephy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&eephy_ca,		&eephy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 98,    0},
/* 49: exphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&exphy_ca,		&exphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 98,    0},
/* 50: rlphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&rlphy_ca,		&rlphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 98,    0},
/* 51: lxtphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&lxtphy_ca,	&lxtphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 98,    0},
/* 52: luphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&luphy_ca,		&luphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 98,    0},
/* 53: icsphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&icsphy_ca,	&icsphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 98,    0},
/* 54: sqphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&sqphy_ca,		&sqphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 98,    0},
/* 55: ukphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 98,    0},
/* 56: dcphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&dcphy_ca,		&dcphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 98,    0},
/* 57: bmtphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&bmtphy_ca,	&bmtphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 98,    0},
/* 58: brgphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&brgphy_ca,	&brgphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 98,    0},
/* 59: xmphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&xmphy_ca,		&xmphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 98,    0},
/* 60: amphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&amphy_ca,		&amphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 98,    0},
/* 61: acphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&acphy_ca,		&acphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 98,    0},
/* 62: nsgphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&nsgphy_ca,	&nsgphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 98,    0},
/* 63: urlphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&urlphy_ca,	&urlphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 98,    0},
/* 64: rgephy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&rgephy_ca,	&rgephy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 98,    0},
/* 65: ipgphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&ipgphy_ca,	&ipgphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 98,    0},
/* 66: jmphy* at ure*|url*|udav*|mos*|smsc*|axen*|axe*|aue*|jme*|stge*|bnx*|bge*|msk*|sk*|nep*|cas*|vr*|pcn*|be*|hme*|hme*|ti*|ti*|gem*|gem*|ne*|ne*|epic*|dc*|dc*|re*|re*|rl*|fxp*|xl*|xl*|ep* phy -1 */
    {&jmphy_ca,		&jmphy_cd,	 0, STAR, loc+  5,    0, pv+ 0, 98,    0},
/* 67: atapiscsi* at pciide*|pciide* channel -1 */
    {&atapiscsi_ca,	&atapiscsi_cd,	 0, STAR, loc+  5,    0, pv+112, 172,    0},
/* 68: wd* at pciide*|pciide* channel -1 drive -1 */
    {&wd_ca,		&wd_cd,		 0, STAR, loc+  4, 0xa00, pv+112, 172,    0},
/* 69: pckbd* at pckbc* slot -1 */
    {&pckbd_ca,		&pckbd_cd,	 0, STAR, loc+  5,    0, pv+154, 176,    0},
/* 70: pcmcia* at cardslot* controller -1 socket -1 */
    {&pcmcia_ca,	&pcmcia_cd,	 0, STAR, loc+  4,    0, pv+146, 178,    0},
/* 71: ep* at pcmcia* function -1 irq -1 */
    {&ep_pcmcia_ca,	&ep_cd,		 0, STAR, loc+  4,    0, pv+148, 181,    0},
/* 72: ne* at pcmcia* function -1 irq -1 */
    {&ne_pcmcia_ca,	&ne_cd,		 0, STAR, loc+  4,    0, pv+148, 181,    0},
/* 73: ne* at pci* dev -1 function -1 */
    {&ne_pci_ca,	&ne_cd,		 0, STAR, loc+  4,    0, pv+144, 95,    0},
/* 74: com* at pcmcia* function -1 irq -1 */
    {&com_pcmcia_ca,	&com_cd,	 0, STAR, loc+  4,    0, pv+148, 181,    0},
/* 75: com* at ebus*|ebus* addr -1 */
    {&com_ebus_ca,	&com_cd,	 0, STAR, loc+  5,    0, pv+106, 184,    0},
/* 76: wi* at pcmcia* function -1 irq -1 */
    {&wi_pcmcia_ca,	&wi_cd,		 0, STAR, loc+  4,    0, pv+148, 181,    0},
/* 77: sbus* at mainbus0 */
    {&sbus_mb_ca,	&sbus_cd,	 0, STAR,     loc,    0, pv+120, 88,    0},
/* 78: sbus* at xbox* */
    {&sbus_xbox_ca,	&sbus_cd,	 0, STAR,     loc,    0, pv+134, 187,    0},
/* 79: creator* at mainbus0 */
    {&creator_ca,	&creator_cd,	 0, STAR,     loc,    0, pv+120, 88,    0},
/* 80: creator* at upa0 */
    {&creator_ca,	&creator_cd,	 0, STAR,     loc,    0, pv+136, 187,    0},
/* 81: pci* at psycho*|schizo*|pyro*|vpci*|ppb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+  5,    0, pv+86, 188,    0},
/* 82: ahc* at pci* dev -1 function -1 */
    {&ahc_pci_ca,	&ahc_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/* 83: arc* at pci* dev -1 function -1 */
    {&arc_ca,		&arc_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/* 84: jmb* at pci* dev -1 function -1 */
    {&jmb_ca,		&jmb_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/* 85: ahci* at pci* dev -1 function -1 */
    {&ahci_pci_ca,	&ahci_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/* 86: ahci* at jmb* */
    {&ahci_jmb_ca,	&ahci_cd,	 0, STAR,     loc,    0, pv+140, 197,    0},
/* 87: nvme* at pci* dev -1 function -1 */
    {&nvme_pci_ca,	&nvme_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/* 88: qle* at pci* dev -1 function -1 */
    {&qle_ca,		&qle_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/* 89: mpi* at pci* dev -1 function -1 */
    {&mpi_pci_ca,	&mpi_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/* 90: mpii* at pci* dev -1 function -1 */
    {&mpii_ca,		&mpii_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/* 91: sili* at pci* dev -1 function -1 */
    {&sili_pci_ca,	&sili_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/* 92: de* at pci* dev -1 function -1 */
    {&de_ca,		&de_cd,		 0, STAR, loc+  4,    0, pv+144, 95,    0},
/* 93: pcn* at pci* dev -1 function -1 */
    {&pcn_ca,		&pcn_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/* 94: siop* at pci* dev -1 function -1 */
    {&siop_pci_ca,	&siop_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/* 95: pciide* at pci* dev -1 function -1 */
    {&pciide_pci_ca,	&pciide_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/* 96: pciide* at jmb* */
    {&pciide_jmb_ca,	&pciide_cd,	 0, STAR,     loc,    0, pv+140, 197,    0},
/* 97: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/* 98: rl* at pci* dev -1 function -1 */
    {&rl_pci_ca,	&rl_cd,		 0, STAR, loc+  4,    0, pv+144, 95,    0},
/* 99: re* at pci* dev -1 function -1 */
    {&re_pci_ca,	&re_cd,		 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*100: re* at cardbus* dev -1 function -1 */
    {&re_cardbus_ca,	&re_cd,		 0, STAR, loc+  4,    0, pv+150, 198,    0},
/*101: vr* at pci* dev -1 function -1 */
    {&vr_ca,		&vr_cd,		 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*102: xl* at pci* dev -1 function -1 */
    {&xl_pci_ca,	&xl_cd,		 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*103: xl* at cardbus* dev -1 function -1 */
    {&xl_cardbus_ca,	&xl_cd,		 0, STAR, loc+  4,    0, pv+150, 198,    0},
/*104: fxp* at pci* dev -1 function -1 */
    {&fxp_pci_ca,	&fxp_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*105: em* at pci* dev -1 function -1 */
    {&em_ca,		&em_cd,		 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*106: ix* at pci* dev -1 function -1 */
    {&ix_ca,		&ix_cd,		 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*107: ixl* at pci* dev -1 function -1 */
    {&ixl_ca,		&ixl_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*108: dc* at pci* dev -1 function -1 */
    {&dc_pci_ca,	&dc_cd,		 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*109: dc* at cardbus* dev -1 function -1 */
    {&dc_cardbus_ca,	&dc_cd,		 0, STAR, loc+  4,    0, pv+150, 198,    0},
/*110: epic* at pci* dev -1 function -1 */
    {&epic_pci_ca,	&epic_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*111: ti* at pci* dev -1 function -1 */
    {&ti_pci_ca,	&ti_cd,		 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*112: gem* at pci* dev -1 function -1 */
    {&gem_pci_ca,	&gem_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*113: cas* at pci* dev -1 function -1 */
    {&cas_ca,		&cas_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*114: nep* at pci* dev -1 function -1 */
    {&nep_ca,		&nep_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*115: uhci* at pci* dev -1 function -1 */
    {&uhci_pci_ca,	&uhci_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*116: ohci* at pci* dev -1 function -1 */
    {&ohci_pci_ca,	&ohci_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*117: ohci* at cardbus* dev -1 function -1 */
    {&ohci_cardbus_ca,	&ohci_cd,	 0, STAR, loc+  4,    0, pv+150, 198,    0},
/*118: ehci* at pci* dev -1 function -1 */
    {&ehci_pci_ca,	&ehci_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*119: ehci* at cardbus* dev -1 function -1 */
    {&ehci_cardbus_ca,	&ehci_cd,	 0, STAR, loc+  4,    0, pv+150, 198,    0},
/*120: xhci* at pci* dev -1 function -1 */
    {&xhci_pci_ca,	&xhci_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*121: cbb* at pci* dev -1 function -1 */
    {&cbb_pci_ca,	&cbb_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*122: skc* at pci* dev -1 function -1 */
    {&skc_ca,		&skc_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*123: sk* at skc* */
    {&sk_ca,		&sk_cd,		 0, STAR,     loc,    0, pv+124, 200,    0},
/*124: mskc* at pci* dev -1 function -1 */
    {&mskc_ca,		&mskc_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*125: msk* at mskc* */
    {&msk_ca,		&msk_cd,	 0, STAR,     loc,    0, pv+128, 200,    0},
/*126: wi* at pci* dev -1 function -1 */
    {&wi_pci_ca,	&wi_cd,		 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*127: bge* at pci* dev -1 function -1 */
    {&bge_ca,		&bge_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*128: bnx* at pci* dev -1 function -1 */
    {&bnx_ca,		&bnx_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*129: stge* at pci* dev -1 function -1 */
    {&stge_ca,		&stge_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*130: jme* at pci* dev -1 function -1 */
    {&jme_ca,		&jme_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*131: ath* at pci* dev -1 function -1 */
    {&ath_pci_ca,	&ath_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*132: ath* at cardbus* dev -1 function -1 */
    {&ath_cardbus_ca,	&ath_cd,	 0, STAR, loc+  4,    0, pv+150, 198,    0},
/*133: athn* at pci* dev -1 function -1 */
    {&athn_pci_ca,	&athn_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*134: athn* at cardbus* dev -1 function -1 */
    {&athn_cardbus_ca,	&athn_cd,	 0, STAR, loc+  4,    0, pv+150, 198,    0},
/*135: psycho* at mainbus0 */
    {&psycho_ca,	&psycho_cd,	 0, STAR,     loc,    0, pv+120, 88,    0},
/*136: schizo* at mainbus0|ssm* */
    {&schizo_ca,	&schizo_cd,	 0, STAR,     loc,    0, pv+103, 88,    0},
/*137: pyro* at mainbus0 */
    {&pyro_ca,		&pyro_cd,	 0, STAR,     loc,    0, pv+120, 88,    0},
/*138: vpci* at mainbus0 */
    {&vpci_ca,		&vpci_cd,	 0, STAR,     loc,    0, pv+120, 88,    0},
/*139: hme* at pci* dev -1 function -1 */
    {&hme_pci_ca,	&hme_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*140: vgafb* at pci* dev -1 function -1 */
    {&vgafb_ca,		&vgafb_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*141: machfb* at pci* dev -1 function -1 */
    {&machfb_ca,	&machfb_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*142: radeonfb* at pci* dev -1 function -1 */
    {&radeonfb_ca,	&radeonfb_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*143: ifb* at pci* dev -1 function -1 */
    {&ifb_ca,		&ifb_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*144: raptor* at pci* dev -1 function -1 */
    {&raptor_ca,	&raptor_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*145: gfxp* at pci* dev -1 function -1 */
    {&gfxp_ca,		&gfxp_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*146: sbbc* at pci* dev -1 function -1 */
    {&sbbc_ca,		&sbbc_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*147: ebus* at pci* dev -1 function -1 */
    {&ebus_ca,		&ebus_cd,	 0, STAR, loc+  4,    0, pv+144, 95,    0},
/*148: ebus* at mainbus0 */
    {&ebus_mainbus_ca,	&ebus_cd,	 0, STAR,     loc,    0, pv+120, 88,    0},
/*149: clock* at sbus*|sbus* slot -1 offset -1 */
    {&clock_sbus_ca,	&clock_cd,	 1, STAR, loc+  4,    0, pv+100, 89,    1},
/*150: clock* at ebus*|ebus* addr -1 */
    {&clock_ebus_ca,	&clock_cd,	 1, STAR, loc+  5,    0, pv+106, 184,    1},
/*151: clock0 at fhc0|fhc* */
    {&clock_fhc_ca,	&clock_cd,	 0, NORM,     loc,    0, pv+115, 200,    0},
/*152: timer* at mainbus0 */
    {&timer_ca,		&timer_cd,	 0, STAR,     loc,    0, pv+120, 88,    0},
/*153: cmp* at mainbus0 */
    {&cmp_ca,		&cmp_cd,	 0, STAR,     loc,    0, pv+120, 88,    0},
/*154: core* at cmp* */
    {&core_ca,		&core_cd,	 0, STAR,     loc,    0, pv+138, 200,    0},
/*155: cpu0 at mainbus0|ssm* */
    {&cpu_ca,		&cpu_cd,	 0, NORM,     loc,    0, pv+103, 88,    0},
/*156: cpu0 at cmp* */
    {&cpu_ca,		&cpu_cd,	 0, NORM,     loc,    0, pv+138, 200,    0},
/*157: cpu0 at core* */
    {&cpu_ca,		&cpu_cd,	 0, NORM,     loc,    0, pv+160, 200,    0},
/*158: auxio* at ebus*|ebus* addr -1 */
    {&auxio_ebus_ca,	&auxio_cd,	 0, STAR, loc+  5,    0, pv+106, 184,    0},
/*159: auxio* at sbus*|sbus* slot -1 offset -1 */
    {&auxio_sbus_ca,	&auxio_cd,	 0, STAR, loc+  4,    0, pv+100, 89,    0},
/*160: rtc* at ebus*|ebus* addr -1 */
    {&rtc_ca,		&rtc_cd,	 0, STAR, loc+  5,    0, pv+106, 184,    0},
/*161: sab* at ebus*|ebus* addr -1 */
    {&sab_ca,		&sab_cd,	 0, STAR, loc+  5,    0, pv+106, 184,    0},
/*162: sabtty* at sab* */
    {&sabtty_ca,	&sabtty_cd,	 0, STAR,     loc,    0, pv+164, 200,    0},
/*163: pckbc* at ebus*|ebus* addr -1 */
    {&pckbc_ebus_ca,	&pckbc_cd,	 0, STAR, loc+  5,    0, pv+106, 184,    0},
/*164: comkbd* at ebus*|ebus* addr -1 */
    {&comkbd_ca,	&comkbd_cd,	 0, STAR, loc+  5,    0, pv+106, 184,    0},
/*165: zs* at sbus*|sbus* slot -1 offset -1 */
    {&zs_sbus_ca,	&zs_cd,		 0, STAR, loc+  4,    0, pv+100, 89,    0},
/*166: zs* at fhc0|fhc* */
    {&zs_fhc_ca,	&zs_cd,		 0, STAR,     loc,    0, pv+115, 200,    0},
/*167: zstty* at zs*|zs* channel -1 */
    {&zstty_ca,		&zstty_cd,	 0, STAR, loc+  5,    0, pv+109, 201,    0},
/*168: zskbd* at zs*|zs* channel -1 */
    {&zskbd_ca,		&zskbd_cd,	 0, STAR, loc+  5,    0, pv+109, 201,    0},
/*169: scsibus* at vdsk*|umass*|mpii*|qle*|arc*|atapiscsi*|softraid0|siop*|esp*|esp*|sili*|mpi*|nvme*|ahci*|ahci*|qla*|qla*|qlw*|qlw*|ahc* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+38, 204,    0},
/*170: cd* at scsibus* target -1 lun -1 */
    {&cd_ca,		&cd_cd,		 0, STAR, loc+  4,    0, pv+152, 205,    0},
/*171: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+  4,    0, pv+152, 205,    0},
/*172: usb* at ohci*|ohci*|uhci*|ehci*|ehci*|xhci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+79, 207,    0},
/*173: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+126, 207,    0},
/*174: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+  0,    0, pv+97, 208,    0},
/*175: uhidev* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhidev_ca,	&uhidev_cd,	 0, STAR, loc+  0,    0, pv+97, 208,    0},
/*176: ukbd* at uhidev* reportid -1 */
    {&ukbd_ca,		&ukbd_cd,	 0, STAR, loc+  5,    0, pv+156, 222,    0},
/*177: umass* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&umass_ca,		&umass_cd,	 0, STAR, loc+  0,    0, pv+97, 208,    0},
/*178: aue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&aue_ca,		&aue_cd,	 0, STAR, loc+  0,    0, pv+97, 208,    0},
/*179: axe* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axe_ca,		&axe_cd,	 0, STAR, loc+  0,    0, pv+97, 208,    0},
/*180: axen* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&axen_ca,		&axen_cd,	 0, STAR, loc+  0,    0, pv+97, 208,    0},
/*181: smsc* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&smsc_ca,		&smsc_cd,	 0, STAR, loc+  0,    0, pv+97, 208,    0},
/*182: cue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cue_ca,		&cue_cd,	 0, STAR, loc+  0,    0, pv+97, 208,    0},
/*183: kue* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&kue_ca,		&kue_cd,	 0, STAR, loc+  0,    0, pv+97, 208,    0},
/*184: cdce* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&cdce_ca,		&cdce_cd,	 0, STAR, loc+  0,    0, pv+97, 208,    0},
/*185: mos* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&mos_ca,		&mos_cd,	 0, STAR, loc+  0,    0, pv+97, 208,    0},
/*186: udav* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&udav_ca,		&udav_cd,	 0, STAR, loc+  0,    0, pv+97, 208,    0},
/*187: url* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&url_ca,		&url_cd,	 0, STAR, loc+  0,    0, pv+97, 208,    0},
/*188: ure* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&ure_ca,		&ure_cd,	 0, STAR, loc+  0,    0, pv+97, 208,    0},
/*189: cardslot* at cbb* slot -1 */
    {&cardslot_ca,	&cardslot_cd,	 0, STAR, loc+  5,    0, pv+158, 224,    0},
/*190: cardbus* at cardslot* slot -1 */
    {&cardbus_ca,	&cardbus_cd,	 0, STAR, loc+  5,    0, pv+146, 178,    0},
/*191: vcons0 at vbus0 */
    {&vcons_ca,		&vcons_cd,	 0, NORM,     loc,    0, pv+118, 225,    0},
/*192: vrng0 at vbus0 */
    {&vrng_ca,		&vrng_cd,	 0, NORM,     loc,    0, pv+118, 225,    0},
/*193: vrtc0 at vbus0 */
    {&vrtc_ca,		&vrtc_cd,	 0, NORM,     loc,    0, pv+118, 225,    0},
/*194: vdsk* at cbus* */
    {&vdsk_ca,		&vdsk_cd,	 0, STAR,     loc,    0, pv+122, 225,    0},
/*195: vnet* at cbus* */
    {&vnet_ca,		&vnet_cd,	 0, STAR,     loc,    0, pv+122, 225,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 0 /* softraid0 */,
	 1 /* mainbus0 */,
	-1
};

int cfroots_size = 3;

/* pseudo-devices */
extern void rdattach(int);
extern void loopattach(int);
extern void bpfilterattach(int);
extern void vlanattach(int);
extern void trunkattach(int);
extern void bioattach(int);

char *pdevnames[] = {
	"rd",
	"loop",
	"bpfilter",
	"vlan",
	"trunk",
	"bio",
};

int pdevnames_size = 6;

struct pdevinit pdevinit[] = {
	{ rdattach, 1 },
	{ loopattach, 1 },
	{ bpfilterattach, 1 },
	{ vlanattach, 1 },
	{ trunkattach, 1 },
	{ bioattach, 1 },
	{ NULL, 0 }
};
