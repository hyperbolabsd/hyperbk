/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/sparc64/conf/RAMDISKU1"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver esp_cd;
extern struct cfdriver le_cd;
extern struct cfdriver gem_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver hme_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver pcons_cd;
extern struct cfdriver sbus_cd;
extern struct cfdriver dma_cd;
extern struct cfdriver ledma_cd;
extern struct cfdriver lebuffer_cd;
extern struct cfdriver agten_cd;
extern struct cfdriver bwtwo_cd;
extern struct cfdriver cgsix_cd;
extern struct cfdriver cgthree_cd;
extern struct cfdriver cgtwelve_cd;
extern struct cfdriver mgx_cd;
extern struct cfdriver rfx_cd;
extern struct cfdriver tvtwo_cd;
extern struct cfdriver vigra_cd;
extern struct cfdriver zx_cd;
extern struct cfdriver xbox_cd;
extern struct cfdriver nsphy_cd;
extern struct cfdriver qsphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver zs_cd;
extern struct cfdriver creator_cd;
extern struct cfdriver clock_cd;
extern struct cfdriver timer_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver zstty_cd;
extern struct cfdriver zskbd_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver sd_cd;

extern struct cfattach mainbus_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach pcons_ca;
extern struct cfattach dma_sbus_ca;
extern struct cfattach ledma_ca;
extern struct cfattach lebuffer_ca;
extern struct cfattach le_sbus_ca;
extern struct cfattach le_lebuffer_ca;
extern struct cfattach le_ledma_ca;
extern struct cfattach esp_sbus_ca;
extern struct cfattach esp_dma_ca;
extern struct cfattach hme_sbus_ca;
extern struct cfattach gem_sbus_ca;
extern struct cfattach agten_ca;
extern struct cfattach bwtwo_ca;
extern struct cfattach cgsix_ca;
extern struct cfattach cgthree_ca;
extern struct cfattach cgtwelve_ca;
extern struct cfattach mgx_ca;
extern struct cfattach rfx_ca;
extern struct cfattach tvtwo_ca;
extern struct cfattach vigra_ca;
extern struct cfattach zx_ca;
extern struct cfattach xbox_ca;
extern struct cfattach nsphy_ca;
extern struct cfattach qsphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach sbus_mb_ca;
extern struct cfattach sbus_xbox_ca;
extern struct cfattach creator_ca;
extern struct cfattach clock_sbus_ca;
extern struct cfattach timer_ca;
extern struct cfattach cpu_ca;
extern struct cfattach zs_sbus_ca;
extern struct cfattach zstty_ca;
extern struct cfattach zskbd_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach sd_ca;


/* locators */
static long loc[5] = {
	-1, -1, 1, -1, -1,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"console",
	"primary",
	"mux",
	"slot",
	"offset",
	"phy",
	"channel",
	"target",
	"lun",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, 1, 2, -1, 0, 1, 2,
	-1, 0, 1, 2, -1, 0, 1, 2,
	-1, 0, 1, 2, -1, 0, 1, 2,
	-1, 0, 1, 2, -1, 0, 1, 2,
	-1, 0, 1, 2, -1, 0, 1, 2,
	-1, 0, 1, 2, -1, 0, 2, -1,
	3, 4, -1, 3, 4, -1, 5, -1,
	5, -1, 6, -1, 7, 8, -1,
};

/* size of parent vectors */
int pv_size = 37;

/* parent vectors */
short pv[37] = {
	14, 15, 16, 17, 18, 30, 19, 20, 21, 22, 23, -1, 28, 29, -1, 12,
	13, -1, 11, 10, -1, 5, -1, 6, -1, 4, -1, 0, -1, 36, -1, 24,
	-1, 37, -1, 34, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+11, 0,    0},
/*  1: wsdisplay* at agten*|bwtwo*|cgsix*|cgthree*|cgtwelve*|creator*|mgx*|rfx*|tvtwo*|vigra*|zx* console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+  0,    0, pv+ 0, 1,    0},
/*  2: wskbd* at zskbd* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+  1,    0, pv+29, 45,    0},
/*  3: pcons0 at mainbus0 */
    {&pcons_ca,		&pcons_cd,	 0, NORM,     loc,    0, pv+27, 47,    0},
/*  4: dma* at sbus*|sbus* slot -1 offset -1 */
    {&dma_sbus_ca,	&dma_cd,	 0, STAR, loc+  3,    0, pv+12, 48,    0},
/*  5: ledma* at sbus*|sbus* slot -1 offset -1 */
    {&ledma_ca,		&ledma_cd,	 0, STAR, loc+  3,    0, pv+12, 48,    0},
/*  6: lebuffer* at sbus*|sbus* slot -1 offset -1 */
    {&lebuffer_ca,	&lebuffer_cd,	 0, STAR, loc+  3,    0, pv+12, 48,    0},
/*  7: le* at sbus*|sbus* slot -1 offset -1 */
    {&le_sbus_ca,	&le_cd,		 0, STAR, loc+  3,    0, pv+12, 48,    0},
/*  8: le* at ledma* */
    {&le_ledma_ca,	&le_cd,		 0, STAR,     loc,    0, pv+21, 53,    0},
/*  9: le* at lebuffer* */
    {&le_lebuffer_ca,	&le_cd,		 0, STAR,     loc,    0, pv+23, 53,    0},
/* 10: esp* at sbus*|sbus* slot -1 offset -1 */
    {&esp_sbus_ca,	&esp_cd,	 0, STAR, loc+  3,    0, pv+12, 48,    0},
/* 11: esp* at dma* */
    {&esp_dma_ca,	&esp_cd,	 0, STAR,     loc,    0, pv+25, 53,    0},
/* 12: hme* at sbus*|sbus* slot -1 offset -1 */
    {&hme_sbus_ca,	&hme_cd,	 0, STAR, loc+  3,    0, pv+12, 48,    0},
/* 13: gem* at sbus*|sbus* slot -1 offset -1 */
    {&gem_sbus_ca,	&gem_cd,	 0, STAR, loc+  3,    0, pv+12, 48,    0},
/* 14: agten* at sbus*|sbus* slot -1 offset -1 */
    {&agten_ca,		&agten_cd,	 0, STAR, loc+  3,    0, pv+12, 48,    0},
/* 15: bwtwo* at sbus*|sbus* slot -1 offset -1 */
    {&bwtwo_ca,		&bwtwo_cd,	 0, STAR, loc+  3,    0, pv+12, 48,    0},
/* 16: cgsix* at sbus*|sbus* slot -1 offset -1 */
    {&cgsix_ca,		&cgsix_cd,	 0, STAR, loc+  3,    0, pv+12, 48,    0},
/* 17: cgthree* at sbus*|sbus* slot -1 offset -1 */
    {&cgthree_ca,	&cgthree_cd,	 0, STAR, loc+  3,    0, pv+12, 48,    0},
/* 18: cgtwelve* at sbus*|sbus* slot -1 offset -1 */
    {&cgtwelve_ca,	&cgtwelve_cd,	 0, STAR, loc+  3,    0, pv+12, 48,    0},
/* 19: mgx* at sbus*|sbus* slot -1 offset -1 */
    {&mgx_ca,		&mgx_cd,	 0, STAR, loc+  3,    0, pv+12, 48,    0},
/* 20: rfx* at sbus*|sbus* slot -1 offset -1 */
    {&rfx_ca,		&rfx_cd,	 0, STAR, loc+  3,    0, pv+12, 48,    0},
/* 21: tvtwo* at sbus*|sbus* slot -1 offset -1 */
    {&tvtwo_ca,		&tvtwo_cd,	 0, STAR, loc+  3,    0, pv+12, 48,    0},
/* 22: vigra* at sbus*|sbus* slot -1 offset -1 */
    {&vigra_ca,		&vigra_cd,	 0, STAR, loc+  3,    0, pv+12, 48,    0},
/* 23: zx* at sbus*|sbus* slot -1 offset -1 */
    {&zx_ca,		&zx_cd,		 0, STAR, loc+  3,    0, pv+12, 48,    0},
/* 24: xbox* at sbus*|sbus* slot -1 offset -1 */
    {&xbox_ca,		&xbox_cd,	 0, STAR, loc+  3,    0, pv+12, 48,    0},
/* 25: nsphy* at hme*|gem* phy -1 */
    {&nsphy_ca,		&nsphy_cd,	 0, STAR, loc+  4,    0, pv+15, 54,    0},
/* 26: qsphy* at hme*|gem* phy -1 */
    {&qsphy_ca,		&qsphy_cd,	 0, STAR, loc+  4,    0, pv+15, 54,    0},
/* 27: ukphy* at hme*|gem* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+  4,    0, pv+15, 54,    0},
/* 28: sbus* at mainbus0 */
    {&sbus_mb_ca,	&sbus_cd,	 0, STAR,     loc,    0, pv+27, 47,    0},
/* 29: sbus* at xbox* */
    {&sbus_xbox_ca,	&sbus_cd,	 0, STAR,     loc,    0, pv+31, 57,    0},
/* 30: creator* at mainbus0 */
    {&creator_ca,	&creator_cd,	 0, STAR,     loc,    0, pv+27, 47,    0},
/* 31: clock* at sbus*|sbus* slot -1 offset -1 */
    {&clock_sbus_ca,	&clock_cd,	 0, STAR, loc+  3,    0, pv+12, 48,    0},
/* 32: timer* at mainbus0 */
    {&timer_ca,		&timer_cd,	 0, STAR,     loc,    0, pv+27, 47,    0},
/* 33: cpu0 at mainbus0 */
    {&cpu_ca,		&cpu_cd,	 0, NORM,     loc,    0, pv+27, 47,    0},
/* 34: zs* at sbus*|sbus* slot -1 offset -1 */
    {&zs_sbus_ca,	&zs_cd,		 0, STAR, loc+  3,    0, pv+12, 48,    0},
/* 35: zstty* at zs* channel -1 */
    {&zstty_ca,		&zstty_cd,	 0, STAR, loc+  4,    0, pv+35, 58,    0},
/* 36: zskbd* at zs* channel -1 */
    {&zskbd_ca,		&zskbd_cd,	 0, STAR, loc+  4,    0, pv+35, 58,    0},
/* 37: scsibus* at esp*|esp* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+18, 59,    0},
/* 38: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+  3,    0, pv+33, 60,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 0 /* mainbus0 */,
	-1
};

int cfroots_size = 2;

/* pseudo-devices */
extern void rdattach(int);
extern void loopattach(int);

char *pdevnames[] = {
	"rd",
	"loop",
};

int pdevnames_size = 2;

struct pdevinit pdevinit[] = {
	{ rdattach, 1 },
	{ loopattach, 1 },
	{ NULL, 0 }
};
