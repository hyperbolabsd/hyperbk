/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "arch/sparc64/conf/RAMDISKU5"
 */

#include <sys/param.h>
#include <sys/device.h>

extern struct cfdriver siop_cd;
extern struct cfdriver gem_cd;
extern struct cfdriver com_cd;
extern struct cfdriver pckbc_cd;
extern struct cfdriver uhci_cd;
extern struct cfdriver ohci_cd;
extern struct cfdriver mainbus_cd;
extern struct cfdriver upa_cd;
extern struct cfdriver hme_cd;
extern struct cfdriver wsdisplay_cd;
extern struct cfdriver wskbd_cd;
extern struct cfdriver pcons_cd;
extern struct cfdriver nsphy_cd;
extern struct cfdriver qsphy_cd;
extern struct cfdriver ukphy_cd;
extern struct cfdriver atapiscsi_cd;
extern struct cfdriver wd_cd;
extern struct cfdriver pckbd_cd;
extern struct cfdriver creator_cd;
extern struct cfdriver pci_cd;
extern struct cfdriver pciide_cd;
extern struct cfdriver ppb_cd;
extern struct cfdriver psycho_cd;
extern struct cfdriver schizo_cd;
extern struct cfdriver vgafb_cd;
extern struct cfdriver machfb_cd;
extern struct cfdriver raptor_cd;
extern struct cfdriver gfxp_cd;
extern struct cfdriver ebus_cd;
extern struct cfdriver clock_cd;
extern struct cfdriver timer_cd;
extern struct cfdriver cpu_cd;
extern struct cfdriver auxio_cd;
extern struct cfdriver sab_cd;
extern struct cfdriver sabtty_cd;
extern struct cfdriver comkbd_cd;
extern struct cfdriver scsibus_cd;
extern struct cfdriver sd_cd;
extern struct cfdriver usb_cd;
extern struct cfdriver uhub_cd;
extern struct cfdriver uhidev_cd;
extern struct cfdriver ukbd_cd;

extern struct cfattach mainbus_ca;
extern struct cfattach upa_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach pcons_ca;
extern struct cfattach nsphy_ca;
extern struct cfattach qsphy_ca;
extern struct cfattach ukphy_ca;
extern struct cfattach atapiscsi_ca;
extern struct cfattach wd_ca;
extern struct cfattach pckbd_ca;
extern struct cfattach creator_ca;
extern struct cfattach pci_ca;
extern struct cfattach siop_pci_ca;
extern struct cfattach pciide_pci_ca;
extern struct cfattach ppb_ca;
extern struct cfattach gem_pci_ca;
extern struct cfattach uhci_pci_ca;
extern struct cfattach ohci_pci_ca;
extern struct cfattach psycho_ca;
extern struct cfattach schizo_ca;
extern struct cfattach hme_pci_ca;
extern struct cfattach vgafb_ca;
extern struct cfattach machfb_ca;
extern struct cfattach raptor_ca;
extern struct cfattach gfxp_ca;
extern struct cfattach ebus_ca;
extern struct cfattach clock_ebus_ca;
extern struct cfattach timer_ca;
extern struct cfattach cpu_ca;
extern struct cfattach auxio_ebus_ca;
extern struct cfattach sab_ca;
extern struct cfattach sabtty_ca;
extern struct cfattach com_ebus_ca;
extern struct cfattach pckbc_ebus_ca;
extern struct cfattach comkbd_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach sd_ca;
extern struct cfattach usb_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uhub_uhub_ca;
extern struct cfattach uhidev_ca;
extern struct cfattach ukbd_ca;


/* locators */
static long loc[9] = {
	-1, -1, -1, -1, -1, -1, -1, -1,
	1,
};

#ifndef MAXEXTRALOC
#define MAXEXTRALOC 32
#endif
long extraloc[MAXEXTRALOC] = { -1 }; /* extra locator space */
int rextraloc = MAXEXTRALOC; /* remaining extra locators */
const int textraloc = MAXEXTRALOC; /* total extra relocators */

char *locnames[] = {
	"console",
	"primary",
	"mux",
	"phy",
	"channel",
	"slot",
	"bus",
	"dev",
	"function",
	"addr",
	"target",
	"lun",
	"port",
	"configuration",
	"interface",
	"vendor",
	"product",
	"release",
	"reportid",
};

/* each entry is an index into locnames[]; -1 terminates */
short locnamp[] = {
	-1, 0, 1, 2, -1, 0, 1, 2,
	-1, 0, 1, 2, -1, 0, 1, 2,
	-1, 0, 1, 2, -1, 0, 2, -1,
	0, 2, -1, 0, 2, -1, 3, -1,
	3, -1, 4, -1, 5, -1, 6, -1,
	6, -1, 6, -1, 7, 8, -1, 9,
	-1, 10, 11, -1, 12, 13, 14, 15,
	16, 17, -1, 12, 13, 14, 15, 16,
	17, -1, 18, -1,
};

/* size of parent vectors */
int pv_size = 46;

/* parent vectors */
short pv[46] = {
	11, 25, 23, 24, 22, -1, 42, 35, 10, -1, 20, 19, 15, -1, 8, 13,
	-1, 21, 16, -1, 39, 40, -1, 18, 17, -1, 12, -1, 41, -1, 14, -1,
	34, -1, 26, -1, 31, -1, 0, -1, 1, -1, 38, -1, 36, -1,
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm starunit1 */
/*  0: mainbus0 at root */
    {&mainbus_ca,	&mainbus_cd,	 0, NORM,     loc,    0, pv+ 5, 0,    0},
/*  1: upa0 at mainbus0 */
    {&upa_ca,		&upa_cd,	 0, NORM,     loc,    0, pv+38, 29,    0},
/*  2: wsdisplay* at creator*|gfxp*|machfb*|raptor*|vgafb* console -1 primary -1 mux 1 */
    {&wsdisplay_emul_ca,	&wsdisplay_cd,	 0, STAR, loc+  6,    0, pv+ 0, 1,    0},
/*  3: wskbd* at ukbd*|comkbd*|pckbd* console -1 mux 1 */
    {&wskbd_ca,		&wskbd_cd,	 0, STAR, loc+  7,    0, pv+ 6, 21,    0},
/*  4: pcons0 at mainbus0 */
    {&pcons_ca,		&pcons_cd,	 0, NORM,     loc,    0, pv+38, 29,    0},
/*  5: nsphy* at hme*|gem* phy -1 */
    {&nsphy_ca,		&nsphy_cd,	 0, STAR, loc+  5,    0, pv+17, 30,    0},
/*  6: qsphy* at hme*|gem* phy -1 */
    {&qsphy_ca,		&qsphy_cd,	 0, STAR, loc+  5,    0, pv+17, 30,    0},
/*  7: ukphy* at hme*|gem* phy -1 */
    {&ukphy_ca,		&ukphy_cd,	 0, STAR, loc+  5,    0, pv+17, 30,    0},
/*  8: atapiscsi* at pciide* channel -1 */
    {&atapiscsi_ca,	&atapiscsi_cd,	 0, STAR, loc+  5,    0, pv+30, 34,    0},
/*  9: wd* at pciide* channel -1 drive -1 */
    {&wd_ca,		&wd_cd,		 0, STAR, loc+  4, 0xa00, pv+30, 34,    0},
/* 10: pckbd* at pckbc* slot -1 */
    {&pckbd_ca,		&pckbd_cd,	 0, STAR, loc+  5,    0, pv+32, 36,    0},
/* 11: creator* at upa0 */
    {&creator_ca,	&creator_cd,	 0, STAR,     loc,    0, pv+40, 37,    0},
/* 12: pci* at schizo*|psycho*|ppb* bus -1 */
    {&pci_ca,		&pci_cd,	 0, STAR, loc+  5,    0, pv+10, 38,    0},
/* 13: siop* at pci* dev -1 function -1 */
    {&siop_pci_ca,	&siop_cd,	 0, STAR, loc+  4,    0, pv+26, 44,    0},
/* 14: pciide* at pci* dev -1 function -1 */
    {&pciide_pci_ca,	&pciide_cd,	 0, STAR, loc+  4,    0, pv+26, 44,    0},
/* 15: ppb* at pci* dev -1 function -1 */
    {&ppb_ca,		&ppb_cd,	 0, STAR, loc+  4,    0, pv+26, 44,    0},
/* 16: gem* at pci* dev -1 function -1 */
    {&gem_pci_ca,	&gem_cd,	 0, STAR, loc+  4,    0, pv+26, 44,    0},
/* 17: uhci* at pci* dev -1 function -1 */
    {&uhci_pci_ca,	&uhci_cd,	 0, STAR, loc+  4,    0, pv+26, 44,    0},
/* 18: ohci* at pci* dev -1 function -1 */
    {&ohci_pci_ca,	&ohci_cd,	 0, STAR, loc+  4,    0, pv+26, 44,    0},
/* 19: psycho* at mainbus0 */
    {&psycho_ca,	&psycho_cd,	 0, STAR,     loc,    0, pv+38, 29,    0},
/* 20: schizo* at mainbus0 */
    {&schizo_ca,	&schizo_cd,	 0, STAR,     loc,    0, pv+38, 29,    0},
/* 21: hme* at pci* dev -1 function -1 */
    {&hme_pci_ca,	&hme_cd,	 0, STAR, loc+  4,    0, pv+26, 44,    0},
/* 22: vgafb* at pci* dev -1 function -1 */
    {&vgafb_ca,		&vgafb_cd,	 0, STAR, loc+  4,    0, pv+26, 44,    0},
/* 23: machfb* at pci* dev -1 function -1 */
    {&machfb_ca,	&machfb_cd,	 0, STAR, loc+  4,    0, pv+26, 44,    0},
/* 24: raptor* at pci* dev -1 function -1 */
    {&raptor_ca,	&raptor_cd,	 0, STAR, loc+  4,    0, pv+26, 44,    0},
/* 25: gfxp* at pci* dev -1 function -1 */
    {&gfxp_ca,		&gfxp_cd,	 0, STAR, loc+  4,    0, pv+26, 44,    0},
/* 26: ebus* at pci* dev -1 function -1 */
    {&ebus_ca,		&ebus_cd,	 0, STAR, loc+  4,    0, pv+26, 44,    0},
/* 27: clock* at ebus* addr -1 */
    {&clock_ebus_ca,	&clock_cd,	 0, STAR, loc+  5,    0, pv+34, 47,    0},
/* 28: timer* at mainbus0 */
    {&timer_ca,		&timer_cd,	 0, STAR,     loc,    0, pv+38, 29,    0},
/* 29: cpu0 at mainbus0 */
    {&cpu_ca,		&cpu_cd,	 0, NORM,     loc,    0, pv+38, 29,    0},
/* 30: auxio* at ebus* addr -1 */
    {&auxio_ebus_ca,	&auxio_cd,	 0, STAR, loc+  5,    0, pv+34, 47,    0},
/* 31: sab* at ebus* addr -1 */
    {&sab_ca,		&sab_cd,	 0, STAR, loc+  5,    0, pv+34, 47,    0},
/* 32: sabtty* at sab* */
    {&sabtty_ca,	&sabtty_cd,	 0, STAR,     loc,    0, pv+36, 48,    0},
/* 33: com* at ebus* addr -1 */
    {&com_ebus_ca,	&com_cd,	 0, STAR, loc+  5,    0, pv+34, 47,    0},
/* 34: pckbc* at ebus* addr -1 */
    {&pckbc_ebus_ca,	&pckbc_cd,	 0, STAR, loc+  5,    0, pv+34, 47,    0},
/* 35: comkbd* at ebus* addr -1 */
    {&comkbd_ca,	&comkbd_cd,	 0, STAR, loc+  5,    0, pv+34, 47,    0},
/* 36: scsibus* at atapiscsi*|siop* */
    {&scsibus_ca,	&scsibus_cd,	 0, STAR,     loc,    0, pv+14, 48,    0},
/* 37: sd* at scsibus* target -1 lun -1 */
    {&sd_ca,		&sd_cd,		 0, STAR, loc+  4,    0, pv+44, 49,    0},
/* 38: usb* at ohci*|uhci* */
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+23, 51,    0},
/* 39: uhub* at usb* */
    {&uhub_ca,		&uhub_cd,	 0, STAR,     loc,    0, pv+42, 51,    0},
/* 40: uhub* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhub_uhub_ca,	&uhub_cd,	 0, STAR, loc+  0,    0, pv+20, 52,    0},
/* 41: uhidev* at uhub*|uhub* port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    {&uhidev_ca,	&uhidev_cd,	 0, STAR, loc+  0,    0, pv+20, 52,    0},
/* 42: ukbd* at uhidev* reportid -1 */
    {&ukbd_ca,		&ukbd_cd,	 0, STAR, loc+  5,    0, pv+28, 66,    0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {0},
    {(struct cfattach *)-1}
};

short cfroots[] = {
	 0 /* mainbus0 */,
	-1
};

int cfroots_size = 2;

/* pseudo-devices */
extern void rdattach(int);
extern void loopattach(int);

char *pdevnames[] = {
	"rd",
	"loop",
};

int pdevnames_size = 2;

struct pdevinit pdevinit[] = {
	{ rdattach, 1 },
	{ loopattach, 1 },
	{ NULL, 0 }
};
