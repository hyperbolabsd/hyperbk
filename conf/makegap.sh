#!/bin/sh -

random_uniform() {
	local	_upper_bound

	if [[ $1 -gt 0 ]]; then
		_upper_bound=$(($1 - 1))
	else
		_upper_bound=0
	fi

	echo `jot -r 1 0 $_upper_bound 2>/dev/null`
}

umask 007

# Modifications to support HyperbolaBSD:
# Written in 2022 by Hyperbola Project
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along
# with this software. If not, see
# <https://creativecommons.org/publicdomain/zero/1.0/>.
#
# Note: modifications were made in order to solve compilation error
#
# Modifications are:
# * 'if' and 'elif' conditional statements
# * '$()' instead of '``' in 'PAGE_SIZE' variable
# * Tab in 'PAGE_SIZE' variable
# * Declaration of 'PAGE_SIZE' variable for 'elif' conditional statement
#
# Start of modifications made by Hyperbola Project
if [ $(uname -s) = 'HyperbolaBSD' ] || [ $(uname -s) = 'OpenBSD' ]; then
	PAGE_SIZE=$(sysctl -n hw.pagesize)
elif [ $(uname -s) = 'Linux' ]; then
	PAGE_SIZE=$(getconf PAGESIZE)
fi
# End of modifications made by Hyperbola Project
PAD=$1
GAPDUMMY=$2

RANDOM1=`random_uniform $((3 * PAGE_SIZE))`
RANDOM2=`random_uniform $PAGE_SIZE`
RANDOM3=`random_uniform $PAGE_SIZE`
RANDOM4=`random_uniform $PAGE_SIZE`
RANDOM5=`random_uniform $PAGE_SIZE`

cat > gap.link << __EOF__

PHDRS {
	text PT_LOAD FILEHDR PHDRS;
	rodata PT_LOAD;
	data PT_LOAD;
	bss PT_LOAD;
}

SECTIONS {
	.text : ALIGN($PAGE_SIZE) {
		LONG($PAD);
		. += $RANDOM1;
		. = ALIGN($PAGE_SIZE);
		endboot = .;
		PROVIDE (endboot = .);
		. = ALIGN($PAGE_SIZE);
		. += $RANDOM2;
		. = ALIGN(16);
		*(.text .text.*)
	} :text =$PAD

	.rodata : {
		LONG($PAD);
		. += $RANDOM3;
		. = ALIGN(16);
		*(.rodata .rodata.*)
	} :rodata =$PAD

	.data : {
		LONG($PAD);
		. = . + $RANDOM4;	/* fragment of page */
		. = ALIGN(16);
		*(.data .data.*)
	} :data =$PAD

	.bss : {
		. = . + $RANDOM5;	/* fragment of page */
		. = ALIGN(16);
		*(.bss .bss.*)
	} :bss
}
__EOF__

$LD $LDFLAGS -r gap.link $GAPDUMMY -o gap.o
