/* $OpenBSD: rcvbundl.h,v 1.2 2005/04/24 20:41:34 brad Exp $ */

/*
Copyright (c) 1999-2001, Intel Corporation

All rights reserved.

Modifications to support HyperbolaBSD:
Copyright (c) 2023 Hyperbola Project

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

 3. Neither the name of Intel Corporation nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*
rcvbundl.h

Author:  Patrick J Luhmann (PJL)
Date:    05/30/2000
Version: 3.28

This file contains the loadable micro code arrays to implement receive bundling on the
D101 A-step, D101 B-step, D101M (B-step only), D101S, D102 B-step,
D102 B-step with TCO work around, D102 C-step and D102 E-step.

Each controller has its own specific micro code array.  The array for one controller
is totally incompatible with any other controller, and if used will most likely
cause the controller to lock up and stop responding to the driver.  Each micro
code array has its own parameter offsets (described below), and they each have
their own version number (which should not be confused with the version of the
rcvbundl.h file given above).

*/



/*************************************************************************
*  CPUSaver parameters
*
*  All CPUSaver parameters are 16-bit literals that are part of a
*  "move immediate value" instruction.  By changing the value of
*  the literal in the instruction before the code is loaded, the
*  driver can change algorithm.
*
*  CPUSAVER_DWORD - This is the location of the instruction that loads
*    the dead-man timer with its inital value.  By writing a 16-bit
*    value to the low word of this instruction, the driver can change
*    the timer value.  The current default is either x600 or x800;
*    experiments show that the value probably should stay within the
*    range of x200 - x1000.
*
*  CPUSAVER_BUNDLE_MAX_DWORD - This is the location of the instruction
*    that sets the maximum number of frames that will be bundled.  In
*    some situations, such as the TCP windowing algorithm, it may be
*    better to limit the growth of the bundle size than let it go as
*    high as it can, because that could cause too much added latency.
*    The default is six, because this is the number of packets in the
*    default TCP window size.  A value of 1 would make CPUSaver indicate
*    an interrupt for every frame received.  If you do not want to put
*    a limit on the bundle size, set this value to xFFFF.
*
*  CPUSAVER_MIN_SIZE_DWORD - This is the location of the instruction
*    that contains a bit-mask describing the minimum size frame that
*    will be bundled.  The default masks the lower 7 bits, which means
*    that any frame less than 128 bytes in length will not be bundled,
*    but will instead immediately generate an interrupt.  This does
*    not affect the current bundle in any way.  Any frame that is 128
*    bytes or large will be bundled normally.  This feature is meant
*    to provide immediate indication of ACK frames in a TCP environment.
*    Customers were seeing poor performance when a machine with CPUSaver
*    enabled was sending but not receiving.  The delay introduced when
*    the ACKs were received was enough to reduce total throughput, because
*    the sender would sit idle until the ACK was finally seen.
*
*    The current default is 0xFF80, which masks out the lower 7 bits.
*    This means that any frame which is x7F (127) bytes or smaller
*    will cause an immediate interrupt.  Because this value must be a
*    bit mask, there are only a few valid values that can be used.  To
*    turn this feature off, the driver can write the value xFFFF to the
*    lower word of this instruction (in the same way that the other
*    parameters are used).  Likewise, a value of 0xF800 (2047) would
*    cause an interrupt to be generated for every frame, because all
*    standard Ethernet frames are <= 2047 bytes in length.
*************************************************************************/



/********************************************************/
/*  CPUSaver micro code for the D101A and D101B         */
/********************************************************/

/*  Version 2.0  */

/*  This value is the same for both A and B step of 558.  */
#define D101_CPUSAVER_DWORD         72

/********************************************************/
/*  CPUSaver micro code for the D101M (B-step only)     */
/********************************************************/

/*  Version 2.10  */

/*  Parameter values for the D101M B-step  */
#define D101M_CPUSAVER_DWORD                78
#define D101M_CPUSAVER_BUNDLE_MAX_DWORD     65
#define D101M_CPUSAVER_MIN_SIZE_DWORD       126

/********************************************************/
/*  CPUSaver micro code for the D101S                   */
/********************************************************/

/*  Version 1.20  */

/*  Parameter values for the D101S  */
#define D101S_CPUSAVER_DWORD                78
#define D101S_CPUSAVER_BUNDLE_MAX_DWORD     67
#define D101S_CPUSAVER_MIN_SIZE_DWORD       129

/********************************************************/
/*  CPUSaver (and TCO) micro code for the D102 B-step   */
/********************************************************/

/*  Version 2.0  */

/*
    This version of CPUSaver is different from all others in
    a different way.  It combines the CPUSaver algorithm with
    fixes for bugs in the B-step hardware (specifically, bugs
    with Inline Receive).
    Thus, when CPUSaver is disabled, this micro code image will
    still need to be loaded.  Before this happens, the hit addresses
    for the CPUSaver algorithm must be set to 0x1FFFF.  The hit
    addresses for CPUSaver are (starting with 0, and remember that
*/

/*  Parameter values for the D102 B-step  */
#define D102_B_CPUSAVER_DWORD                91
#define D102_B_CPUSAVER_BUNDLE_MAX_DWORD     115
#define D102_B_CPUSAVER_MIN_SIZE_DWORD       70

/********************************************************/
/*  Micro code for the D102 C-step                      */
/********************************************************/

/*  Parameter values for the D102 C-step  */
#define D102_C_CPUSAVER_DWORD                46
#define D102_C_CPUSAVER_BUNDLE_MAX_DWORD     54
#define D102_C_CPUSAVER_MIN_SIZE_DWORD      133 /* not implemented */

/********************************************************/
/*  Micro code for the D102 E-step                      */
/********************************************************/

/*  Parameter values for the D102 E-step  */
#define D102_E_CPUSAVER_DWORD			42
#define D102_E_CPUSAVER_BUNDLE_MAX_DWORD	54
#define D102_E_CPUSAVER_MIN_SIZE_DWORD		46
