/*
 * Copyright © 2008-2018 Intel Corporation
 *
 * Modifications to support HyperbolaBSD:
 * Copyright (c) 2022 Hyperbola Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 */

#ifndef I915_RESET_H
#define I915_RESET_H

#include <linux/compiler.h>
#include <linux/types.h>
#include <linux/srcu.h>

#include "intel_engine_types.h"
#include "intel_reset_types.h"

struct i915_request;
struct intel_engine_cs;
struct intel_gt;
struct intel_guc;

void intel_gt_init_reset(struct intel_gt *gt);
void intel_gt_fini_reset(struct intel_gt *gt);

void intel_gt_handle_error(struct intel_gt *gt,
			   intel_engine_mask_t engine_mask,
			   unsigned long flags, const char *fmt,
			   ...) __FORMAT_PRINTF_A__(4, 5);
#define I915_ERROR_CAPTURE BIT(0)

void intel_gt_reset(struct intel_gt *gt,
		    intel_engine_mask_t stalled_mask,
		    const char *reason);
int intel_engine_reset(struct intel_engine_cs *engine,
		       const char *reason);

void __i915_request_reset(struct i915_request *rq, bool guilty);

int __must_check intel_gt_reset_trylock(struct intel_gt *gt, int *srcu);
void intel_gt_reset_unlock(struct intel_gt *gt, int tag);

void intel_gt_set_wedged(struct intel_gt *gt);
bool intel_gt_unset_wedged(struct intel_gt *gt);
int intel_gt_terminally_wedged(struct intel_gt *gt);

/*
 * There's no unset_wedged_on_init paired with this one.
 * Once we're wedged on init, there's no going back.
 * Same thing for unset_wedged_on_fini.
 */
void intel_gt_set_wedged_on_init(struct intel_gt *gt);
void intel_gt_set_wedged_on_fini(struct intel_gt *gt);

int __intel_gt_reset(struct intel_gt *gt, intel_engine_mask_t engine_mask);

int intel_reset_guc(struct intel_gt *gt);

struct intel_wedge_me {
	struct delayed_work work;
	struct intel_gt *gt;
	const char *name;
};

void __intel_init_wedge(struct intel_wedge_me *w,
			struct intel_gt *gt,
			long timeout,
			const char *name);
void __intel_fini_wedge(struct intel_wedge_me *w);

#define intel_wedge_on_timeout(W, GT, TIMEOUT)				\
	for (__intel_init_wedge((W), (GT), (TIMEOUT), __func__);	\
	     (W)->gt;							\
	     __intel_fini_wedge((W)))

bool intel_has_gpu_reset(const struct intel_gt *gt);
bool intel_has_reset_engine(const struct intel_gt *gt);

#endif /* I915_RESET_H */
