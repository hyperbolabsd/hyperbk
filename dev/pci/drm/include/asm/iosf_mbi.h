/*
 * Public domain.
 *
 * Modifications to support HyperbolaBSD:
 * Written in 2023 by Hyperbola Project
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <https://creativecommons.org/publicdomain/zero/1.0/>.
 */

#ifndef _ASM_IOSF_MBI_H
#define _ASM_IOSF_MBI_H

#define MBI_PMIC_BUS_ACCESS_BEGIN	1
#define MBI_PMIC_BUS_ACCESS_END		2

#define iosf_mbi_assert_punit_acquired()
#define iosf_mbi_punit_acquire()
#define iosf_mbi_punit_release()

static inline char
iosf_mbi_register_pmic_bus_access_notifier(void *x)
{
	return 0;
}

static inline char
iosf_mbi_unregister_pmic_bus_access_notifier_unlocked(void *x)
{
	return 0;
}

#endif
