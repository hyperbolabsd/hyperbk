/*
 * Public domain.
 *
 * Modifications to support HyperbolaBSD:
 * Written in 2023 by Hyperbola Project
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <https://creativecommons.org/publicdomain/zero/1.0/>.
 */

#ifndef _LINUX_BUILD_BUG_H
#define _LINUX_BUILD_BUG_H

#include <linux/compiler.h>

#define BUILD_BUG()
#define BUILD_BUG_ON(x) typedef char __assert[!(x) ? 1 : -1] __MAYBE_UNUSED_A__

static inline char
BUILD_BUG_ON_NOT_POWER_OF_2(int32_t x)
{
	return 0;
}

#define BUILD_BUG_ON_MSG(x, y) do {	\
} while (0)

#define BUILD_BUG_ON_INVALID(x) (void)0
#define BUILD_BUG_ON_ZERO(x) 0

#endif
