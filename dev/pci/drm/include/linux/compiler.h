/*
 * Public domain.
 *
 * Modifications to support HyperbolaBSD:
 * Written in 2022-2023 by Hyperbola Project
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <https://creativecommons.org/publicdomain/zero/1.0/>.
 */

#ifndef __LINUX_COMPILER_H__
#define __LINUX_COMPILER_H__

#include <linux/kconfig.h>
#include <sys/atomic.h> /* for READ_ONCE() WRITE_ONCE() */

#define unlikely(x) __builtin_expect(!!(x), 0)
#define likely(x) __builtin_expect(!!(x), 1)

#define __force
#define __acquires(x)
#define __releases(x)
#define __read_mostly
#define __iomem
#define __must_check __NODISCARD_A__
#define __cold
#define __init __cold
#define __exit /* __USED_A__ __cold __NO_INSTRUMENT_FUNCTION_A__ */
#define __deprecated /* This macro shall to be empty */

#ifndef __user
# define __user
#endif

#define barrier() __asm__ __volatile__("" : : : "memory")

/* The Linux code doesn't meet our usual standards! */
#if defined(__clang__)
# pragma clang diagnostic ignored "-Winitializer-overrides"
# pragma clang diagnostic ignored "-Wtautological-compare"
# pragma clang diagnostic ignored "-Wunneeded-internal-declaration"
# pragma clang diagnostic ignored "-Wunused-function"
# pragma clang diagnostic ignored "-Wunused-variable"
#elif defined(__GNUC__)
# pragma GCC diagnostic ignored "-Wformat-zero-length"
#endif

#endif
