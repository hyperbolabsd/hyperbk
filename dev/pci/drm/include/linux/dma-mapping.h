/*
 * Public domain.
 *
 * Modifications to support HyperbolaBSD:
 * Written in 2023 by Hyperbola Project
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <https://creativecommons.org/publicdomain/zero/1.0/>.
 */

#ifndef _LINUX_DMA_MAPPING_H
#define _LINUX_DMA_MAPPING_H

#include <linux/sizes.h>
#include <linux/scatterlist.h>

#define DMA_BIT_MASK(n) (((n) == 64) ? ~0ULL : (1ULL<<(n)) -1)

#define dma_set_coherent_mask(x, y) 0

static inline char
dma_set_max_seg_size(void *x, uint32_t y)
{
	return 0;
}

#define dma_set_mask(x, y) 0
#define dma_set_mask_and_coherent(x, y) 0
#define dma_addressing_limited(x) false

#endif
