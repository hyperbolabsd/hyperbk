/*
 * Public domain.
 *
 * Modifications to support HyperbolaBSD:
 * Written in 2023 by Hyperbola Project
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <https://creativecommons.org/publicdomain/zero/1.0/>.
 */

#ifndef _LINUX_IOPOLL_H
#define _LINUX_IOPOLL_H

#define readx_poll_timeout(op, addr, val, cond, sleep_us, timeout_us)	\
({									\
	struct timeval __end, __now, __timeout_tv;			\
	int __timed_out = 0;						\
									\
	uint64_t timeout_cond = (timeout_us);				\
	if (timeout_cond) {						\
		microuptime(&__now);					\
		USEC_TO_TIMEVAL((timeout_us), &__timeout_tv);		\
		timeradd(&__now, &__timeout_tv, &__end);		\
	}								\
									\
	for (;;) {							\
		(val) = (op)(addr);					\
		if ((cond))						\
			break;						\
		if (timeout_cond) {					\
			microuptime(&__now);				\
			if (timercmp(&__end, &__now, <=)) {		\
				__timed_out = 1;			\
				break;					\
			}						\
		}							\
		if ((sleep_us))						\
			delay((sleep_us) / 2);				\
	}								\
	(__timed_out) ? -ETIMEDOUT : 0;					\
})

#endif
