/*
 * Public domain.
 *
 * Modifications to support HyperbolaBSD:
 * Written in 2022 by Hyperbola Project
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <https://creativecommons.org/publicdomain/zero/1.0/>.
 */

#ifndef _LINUX_RATELIMIT_H
#define _LINUX_RATELIMIT_H

struct ratelimit_state {
};

#define DEFINE_RATELIMIT_STATE(name, interval, burst) \
	int name __USED_A__ = 1;

#define __ratelimit(x)	(1)

#define ratelimit_state_init(x, y, z)
#define ratelimit_set_flags(x, y)

#endif
