/*
 * Public domain.
 *
 * Modifications to support HyperbolaBSD:
 * Written in 2022 by Hyperbola Project
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <https://creativecommons.org/publicdomain/zero/1.0/>.
 */

#ifndef _LINUX_TRACEPOINT_H
#define _LINUX_TRACEPOINT_H

#include <linux/types.h>

#define TP_PROTO(x...) x

#define DEFINE_EVENT(template, name, proto, args)	\
    static inline void					\
    trace_##name(proto)					\
    {							\
    }							\
							\
    static inline bool					\
    trace_##name##_enabled(void)			\
    {							\
	    return false;				\
    }

#define DEFINE_EVENT_PRINT(template, name, proto, args, print)	\
    static inline void						\
    trace_##name(proto)						\
    {								\
    }

#define TRACE_EVENT(name, proto, args, tstruct, assign, print)	\
    static inline void						\
    trace_##name(proto)						\
    {								\
    }								\
								\
    static inline bool						\
    trace_##name##_enabled(void)				\
    {								\
	    return false;					\
    }

#define TRACE_EVENT_CONDITION(name, proto, args, cond, tstruct,	assign,	\
			      print)					\
    static inline void							\
    trace_##name(proto)							\
    {									\
    }

#define DECLARE_EVENT_CLASS(name, proto, args, tstruct, assign, print)	\
    static inline void							\
    trace_##name(proto)							\
    {									\
    }

#endif
