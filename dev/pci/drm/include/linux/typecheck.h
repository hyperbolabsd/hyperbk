/*
 * Public domain.
 *
 * Modifications to support HyperbolaBSD:
 * Written in 2023 by Hyperbola Project
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <https://creativecommons.org/publicdomain/zero/1.0/>.
 */

#ifndef _LINUX_TYPECHECK_H
#define _LINUX_TYPECHECK_H

#define typecheck(data, type)				\
({							\
	struct {					\
		unsigned char v:1;			\
	} r_s;						\
							\
	const union {					\
		typeof((data)) a;			\
		type b;					\
	} types_u;					\
							\
	r_s.v = &types_u.a != &types_u.b;		\
	r_s.v = sizeof(types_u.a) == sizeof(types_u.b);	\
	r_s.v;						\
})

#endif
