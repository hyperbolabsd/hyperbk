/*
 * Public domain.
 *
 * Modifications to support HyperbolaBSD:
 * Written in 2023 by Hyperbola Project
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <https://creativecommons.org/publicdomain/zero/1.0/>.
 */

#ifndef _LINUX_VGA_SWITCHEROO_H
#define _LINUX_VGA_SWITCHEROO_H

#include <linux/fb.h>

struct pci_dev;

struct vga_switcheroo_client_ops {
	void *reprobe;
};

static inline char
vga_switcheroo_register_client(void *a,
			       const struct vga_switcheroo_client_ops *b,
			       _Bool c)
{
	return 0;
}

#define vga_switcheroo_unregister_client(a)
#define vga_switcheroo_process_delayed_switch()
#define vga_switcheroo_fini_domain_pm_ops(x)
#define vga_switcheroo_handler_flags() 0
#define vga_switcheroo_client_fb_set(a, b)
#define vga_switcheroo_init_domain_pm_ops(a, b)

#define VGA_SWITCHEROO_CAN_SWITCH_DDC 1

static inline int
vga_switcheroo_lock_ddc(struct pci_dev *pdev)
{
	return -ENOSYS;
}

static inline int
vga_switcheroo_unlock_ddc(struct pci_dev *pdev)
{
	return -ENOSYS;
}

#endif
