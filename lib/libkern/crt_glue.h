/*
 * LLVM 8.0.1 int_types.h:
 * https://github.com/llvm/llvm-project/blob/llvmorg-8.0.1/
 *     compiler-rt/lib/builtins/int_types.h
 *
 * For _YUGA_LITTLE_ENDIAN variable macro:
 * https://github.com/llvm/llvm-project/blob/llvmorg-8.0.1/
 *     compiler-rt/lib/builtins/int_endianness.h
 *
 * LLVM 8.0.1 LICENSE.TXT:
 * https://github.com/llvm/llvm-project/blob/llvmorg-8.0.1/
 *     compiler-rt/LICENSE.TXT
 *
 * LLVM 8.0.1 CREDITS.TXT:
 * https://github.com/llvm/llvm-project/blob/llvmorg-8.0.1/
 *     compiler-rt/CREDITS.TXT
 */

/* ===-- int_lib.h - configuration header for compiler-rt  -----------------===
 *
 *                     The LLVM 8.0.1 Compiler Infrastructure
 *
 * This file is dual licensed under the MIT and the University of Illinois Open
 * Source Licenses. See LICENSE.TXT for details.
 *
 * ===----------------------------------------------------------------------===
 *
 * This file is a compat header for compiler-rt source files used in the
 * HyperbolaBSD kernel.
 * This file is not part of the interface of this library.
 *
 * ===----------------------------------------------------------------------===
 */

/*
 * LLVM 8.0.1 CREDITS.TXT:
 *     Craig van Vliet <cvanvliet@auroraux.org>
 *     Edward O'Callaghan <eocallaghan@auroraux.org>
 *     Howard Hinnant <hhinnant@apple.com>
 *     Guan-Hong Liu <koviankevin@hotmail.com>
 *     Joerg Sonnenberger <joerg@NetBSD.org>
 *     Matt Thomas <matt@NetBSD.org>
 */

/*
==============================================================================

Copyright (c) 2009-2015 by the contributors listed in CREDITS.TXT

Modifications to support HyperbolaBSD:
Copyright (c) 2022 Hyperbola Project

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

==============================================================================

University of Illinois/NCSA
Open Source License

Copyright (c) 2009-2019 by the contributors listed in CREDITS.TXT

All rights reserved.

Developed by:

    LLVM Team

    University of Illinois at Urbana-Champaign

    http://llvm.org

Modifications to support HyperbolaBSD:
Copyright (c) 2022 Hyperbola Project

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal with
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimers.

    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimers in the
      documentation and/or other materials provided with the distribution.

    * Neither the names of the LLVM Team, University of Illinois at
      Urbana-Champaign, nor the names of its contributors may be used to
      endorse or promote products derived from this Software without specific
      prior written permission.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE
SOFTWARE.

==============================================================================
*/

#ifndef _CRT_INT_LIB_H_
#define _CRT_INT_LIB_H_

#include <sys/limits.h>
#include <sys/endian.h>

typedef int si_int;
typedef unsigned int su_int;
typedef long long di_int;
typedef unsigned long long du_int;
typedef int ti_int __MODE_A__(TI);
typedef int tu_int __MODE_A__(TI);

#if BYTE_ORDER == LITTLE_ENDIAN
# define _YUGA_LITTLE_ENDIAN 0
#else
# define _YUGA_LITTLE_ENDIAN 1
#endif

typedef union
{
	ti_int all;
	struct
	{
#if _YUGA_LITTLE_ENDIAN
		du_int low;
		di_int high;
#else
		di_int high;
		du_int low;
#endif /* _YUGA_LITTLE_ENDIAN */
	} s;
} twords;

typedef union
{
	tu_int all;
	struct
	{
#if _YUGA_LITTLE_ENDIAN
		du_int low;
		du_int high;
#else
		du_int high;
		du_int low;
#endif /* _YUGA_LITTLE_ENDIAN */
	} s;
} utwords;

#endif /* _CRT_INT_LIB_H_ */
