/* $OpenBSD: cdefs.h,v 1.43 2018/10/29 17:10:40 guenther Exp $ */
/* $NetBSD: cdefs.h,v 1.16 1996/04/03 20:46:39 christos Exp $ */

/*
 * Copyright (c) 1991, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * This code is derived from software contributed to Berkeley by
 * Berkeley Software Design, Inc.
 *
 * Modifications to support HyperbolaBSD:
 * Copyright (c) 2022-2023 Hyperbola Project
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)cdefs.h	8.7 (Berkeley) 1/21/94
 */

#ifndef __SYS_CDEFS_H__
#define __SYS_CDEFS_H__

#include <machine/cdefs.h>

/*
 * Compiler version macros
 */

/*
 * clang version macro
 *
 * Test if we're using a specific version of clang or later.
 */
#if defined(__clang__)
# define __CLANG_PREREQ__(ma, mi) \
    __clang_major__ > (ma) \
    || __clang_major__ == (ma) && __clang_minor__ >= (mi)
#else
# define __CLANG_PREREQ__(ma, mi) 0
#endif

/*
 * GCC version macro
 *
 * Test if we're using a specific version of GCC or later.
 */
#if defined(__GNUC__)
# define __GNUC_PREREQ__(ma, mi) \
    __GNUC__ > (ma) || __GNUC__ == (ma) && __GNUC_MINOR__ >= (mi)
#else
# define __GNUC_PREREQ__(ma, mi) 0
#endif

/*
 * The Attribute macro in GNU dialect
 *
 * Delete the __attribute__ macro if the compiler
 * is not clang, is not GCC version 2.5 (or below)
 * or is C/C++ standard compiler.
 */
#if !defined(__clang__) && !__GNUC_PREREQ__(2, 5) || defined(__STRICT_ANSI__)
# define __attribute__(x)
#endif

/*
 * The obsoleted __extension__ macro
 *
 * Disable __extension__ macro for GCC version 2.95 (and later)
 * or any compiler.
 */
#if !__GNUC_PREREQ__(2, 8)
# define __extension__
#endif

/*
 * __P(), __CONCAT() and __STRING(x) macros
 * with constant, signed and volatile keywords
 *
 * The __CONCAT macro is used to concatenate parts of symbol names, e.g.
 * with "#define OLD(foo) __CONCAT(old,foo)", OLD(foo) produces oldfoo.
 * The __CONCAT macro is a bit tricky -- make sure you don't put spaces
 * in between its arguments.  Do not use __CONCAT on double-quoted strings,
 * such as those from the __STRING macro: to concatenate strings just put
 * them next to each other.
 */
#if defined(__STDC__) || defined(__cplusplus)
# define __P(protos) protos /* full-blown ANSI C */
# define __CONCAT(x,y) x ## y
# define __STRING(x) #x
# define CONST_K const /* define reserved names to standard */
# define SIGNED_K signed
# define VOLATILE_K volatile /* in __cplusplus >= 202002L is deprecated */
#else
# define __P(protos) () /* traditional C preprocessor */
# define __CONCAT(x,y) x/**/y
# define __STRING(x) "x"
# if !defined(__GNUC__)
#  define CONST_K /* delete pseudo-ANSI C keywords */
#  define SIGNED_K
#  define VOLATILE_K
# endif
#endif
#define __const CONST_K
#define __signed SIGNED_K
#define __volatile VOLATILE_K

/*
 * Inline keywords
 *
 * In functions works with C99+, GNU C89+ and C++.
 * And variables works with C++17+.
 */
/* C standard (C99 version and later) */
#if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901
# define INLINE_K extern inline
# define NOREF_INLINE_K inline
# define __inline inline
/* GNU C dilalect (non-standard) */
#elif defined(__STDC_VERSION__) && defined(__GNUC_GNU_INLINE__) \
    && !defined(__STRICT_ANSI__)
# define INLINE_K inline
# define NOREF_INLINE_K extern inline
# define __inline inline
/* C++ standard */
#elif defined(__cplusplus)
# define INLINE_K inline /* inline and extern inline are same */
# define NOREF_INLINE_K /* delete inline keyword without reference */
# define __inline__ inline
# define __inline inline
#else
# define INLINE_K /* delete inline keyword */
# define NOREF_INLINE_K /* delete inline keyword without reference */
# define __inline /* delete inline keyword */
#endif

/*
 * The Inline function keywords (and attributes) in GNU C dialect
 */

/*
 * The Unqualified Inline function keyword
 *
 * UNQUALIFIED_INLINE_K makes the compiler only use this function definition
 * for inlining; references that can't be inlined will be left as
 * external references instead of generating a local copy.  The
 * matching library should include a simple extern definition for
 * the function to handle those references.  c.f. ctype.h
 */
#if (defined(__STDC_VERSION__) && __STDC_VERSION__ > 202300 \
     || defined(__cplusplus) && __cplusplus >= 201103) \
    && (__CLANG_PREREQ__(8, 0) || __GNUC_PREREQ__(4, 8)) \
    && !defined(__STRICT_ANSI__)
# define UNQUALIFIED_INLINE_K [[gnu::gnu_inline]] NOREF_INLINE_K
#elif (__CLANG_PREREQ__(8, 0) || __GNUC_PREREQ__(4, 2)) \
    && !defined(__STRICT_ANSI__)
# define UNQUALIFIED_INLINE_K __attribute__((__gnu_inline__)) NOREF_INLINE_K
#elif (defined(__clang__) || defined(__GNUC__)) && !defined(__STRICT_ANSI__)
# define UNQUALIFIED_INLINE_K NOREF_INLINE_K
#else
# define UNQUALIFIED_INLINE_K static inline
#endif
#define __only_inline UNQUALIFIED_INLINE_K

/*
 * The Always Inline function keyword
 */
#if (defined(__STDC_VERSION__) && __STDC_VERSION__ > 202300 \
     || defined(__cplusplus) && __cplusplus >= 201103) \
    && __CLANG_PREREQ__(3, 3) && !defined(__STRICT_ANSI__)
# define ALWAYS_INLINE_K [[clang::always_inline]] inline
#elif (defined(__STDC_VERSION__) && __STDC_VERSION__ > 202300 \
       || defined(__cplusplus) && __cplusplus >= 201103) \
    && __GNUC_PREREQ__(4, 8) && !defined(__STRICT_ANSI__)
# define ALWAYS_INLINE_K [[gnu::always_inline]] inline
#elif (__CLANG_PREREQ__(2, 0) || __GNUC_PREREQ__(3, 1)) \
    && !defined(__STRICT_ANSI__)
# define ALWAYS_INLINE_K __attribute__((__always_inline__)) inline
#else
# define ALWAYS_INLINE_K inline
#endif
#define __always_inline ALWAYS_INLINE_K

/*
 * The No Inline function attribute
 */
#if (defined(__STDC_VERSION__) && __STDC_VERSION__ > 202300 \
     || defined(__cplusplus) && __cplusplus >= 201103) \
    && __CLANG_PREREQ__(3, 3) && !defined(__STRICT_ANSI__)
# define __NOINLINE_A__ [[clang::noinline]]
#elif (defined(__STDC_VERSION__) && __STDC_VERSION__ > 202300 \
       || defined(__cplusplus) && __cplusplus >= 201103) \
    && __GNUC_PREREQ__(4, 8) && !defined(__STRICT_ANSI__)
# define __NOINLINE_A__ [[gnu::noinline]]
#elif (__CLANG_PREREQ__(2, 0) || __GNUC_PREREQ__(3, 1) /* or below */) \
    && !defined(__STRICT_ANSI__)
# define __NOINLINE_A__ __attribute__((__noinline__))
#else
# define __NOINLINE_A__ /* delete */
#endif
#define noinline __NOINLINE_A__

/*
 * The No Return function attribute
 *
 * GCC1 and some versions of GCC2 declare dead (non-returning) function
 * using "volatile"; unfortunately, this then cause warnings under
 * "-ansi -pedantic". GCC >= 2.5 uses the __attribute__((attrs)) style.
 * This works for GNU C++ (modulo a slight glitch in the C++ grammar
 * in the distribution version of 2.5.5).
 */
#if defined(__STDC_VERSION__) && __STDC_VERSION__ > 202300 \
    || defined(__cplusplus) && __cplusplus >= 201103
# define __NORETURN_A__ [[noreturn]]
/* #elif defined(__STDC_VERSION__) && __STDC_VERSION__ >= 201112
# define __NORETURN_A__ _Noreturn */
#elif (__CLANG_PREREQ__(1, 4) || __GNUC_PREREQ__(2, 5)) \
    && !defined(__STRICT_ANSI__)
# define __NORETURN_A__ __attribute__((__noreturn__))
#elif (defined(__clang__) || defined(__GNUC__)) && !defined(__STRICT_ANSI__)
# define __NORETURN_A__ VOLATILE_K
#else
# define __NORETURN_A__ /* delete */
#endif
#define __dead __NORETURN_A__

/*
 * The Constant function attribute in GNU dialect
 *
 * GCC1 and some versions of GCC2 declare pure (no side effects) function
 * using "const"; unfortunately, this then cause warnings under
 * "-ansi -pedantic". GCC >= 2.5 uses the __attribute__((attrs)) style.
 * This works for GNU C++ (modulo a slight glitch in the C++ grammar
 * in the distribution version of 2.5.5).
 */
#if (defined(__STDC_VERSION__) && __STDC_VERSION__ > 202300 \
     || defined(__cplusplus) && __cplusplus >= 201103) \
    && (__CLANG_PREREQ__(3, 3) || __GNUC_PREREQ__(4, 8)) \
    && !defined(__STRICT_ANSI__)
# define __CONST_A__ [[gnu::const]]
#elif (__CLANG_PREREQ__(2, 2) || __GNUC_PREREQ__(2, 5)) \
    && !defined(__STRICT_ANSI__)
# define __CONST_A__ __attribute__((__const__))
#elif (defined(__clang__) || defined(__GNUC__)) && !defined(__STRICT_ANSI__)
# define __CONST_A__ CONST_K
#else
# define __CONST_A__ /* delete pseudo-ANSI C keyword */
#endif
#define __pure __CONST_A__

/*
 * The Format function attribute in GNU dialect
 */
#if (defined(__STDC_VERSION__) && __STDC_VERSION__ > 202300 \
     || defined(__cplusplus) && __cplusplus >= 201103) \
    && (__CLANG_PREREQ__(3, 3) || __GNUC_PREREQ__(4, 8)) \
    && !defined(__STRICT_ANSI__)
# define __FORMAT_A__(at, si, ftc) [[gnu::format(at, si, ftc)]]
# define __FORMAT_PRINTF_A__(si, ftc) __FORMAT_A__(printf, si, ftc)
# define __FORMAT_SCANF_A__(si, ftc) __FORMAT_A__(scanf, si, ftc)
#elif (defined(__clang__) || __GNUC_PREREQ__(2, 7)) \
    && !defined(__STRICT_ANSI__)
# define __FORMAT_A__(at, si, ftc) __attribute__((__format__(at, si, ftc)))
# define __FORMAT_PRINTF_A__(si, ftc) __FORMAT_A__(__printf__, si, ftc)
# define __FORMAT_SCANF_A__(si, ftc) __FORMAT_A__(__scanf__, si, ftc)
#else
# define __FORMAT_A__(at, si, ftc) /* delete */
# define __FORMAT_PRINTF_A__(si, ftc)
# define __FORMAT_SCANF_A__(si, ftc)
#endif
#define __printf(si, ftc) __FORMAT_PRINTF_A__(si, ftc)

/*
 * The Mode data attribute in GNU dialect
 */
#if (defined(__STDC_VERSION__) && __STDC_VERSION__ > 202300 \
     || defined(__cplusplus) && __cplusplus >= 201103) \
    && (__CLANG_PREREQ__(3, 3) || __GNUC_PREREQ__(4, 8)) \
    && !defined(__STRICT_ANSI__)
# define __MODE_A__(mach_mode) [[gnu::mode(mach_mode)]]
#elif (__CLANG_PREREQ__(2, 0) || __GNUC_PREREQ__(2, 7)) \
    && !defined(__STRICT_ANSI__)
# define __MODE_A__(mach_mode) __attribute__((__mode__(mach_mode)))
#else
# define __MODE_A__(mach_mode) /* delete */
#endif

/*
 * The NonNull function attribute in GNU dialect
 */
#if (defined(__STDC_VERSION__) && __STDC_VERSION__ > 202300 \
     || defined(__cplusplus) && __cplusplus >= 201103) \
    && (__CLANG_PREREQ__(3, 3) || __GNUC_PREREQ__(4, 8)) \
    && !defined(__STRICT_ANSI__)
# define __NONNULL_A__ [[gnu::nonnull]]
# define __NONNULL_ARG_A__(arg_idx) [[gnu::nonnull(arg_idx)]]
#elif (defined(__clang__) || __GNUC_PREREQ__(3, 3)) \
    && !defined(__STRICT_ANSI__)
# define __NONNULL_A__ __attribute__((__nonnull__))
# define __NONNULL_ARG_A__(arg_idx) __attribute__((__nonnull__(arg_idx)))
#else
# define __NUNNULL_A__ /* delete */
# define __NUNNULL_ARG_A__(arg_idx)
#endif

/*
 * The Weak attribute in GNU dialect
 */
#if (defined(__STDC_VERSION__) && __STDC_VERSION__ > 202300 \
     || defined(__cplusplus) && __cplusplus >= 201103) \
    && (__CLANG_PREREQ__(3, 3) || __GNUC_PREREQ__(4, 8)) \
    && !defined(__STRICT_ANSI__)
# define __WEAK_A__ [[gnu::weak]]
#elif (defined(__clang__) || __GNUC_PREREQ__(2, 95) /* or below */) \
    && !defined(__STRICT_ANSI__)
# define __WEAK_A__ __attribute__((__weak__))
#else
# define __WEAK_A__ /* delete */
#endif

/*
 * The Maybe Unused attribute
 */
#if defined(__STDC_VERSION__) && __STDC_VERSION__ > 202300 \
    || defined(__cplusplus) && __cplusplus >= 201703
# define __MAYBE_UNUSED_A__ [[maybe_unused]]
#elif defined(__cplusplus) && __cplusplus >= 201103 \
    && (__CLANG_PREREQ__(3, 3) || __GNUC_PREREQ__(4, 8)) \
    && !defined(__STRICT_ANSI__)
# define __MAYBE_UNUSED_A__ [[gnu::unused]]
#elif (defined(__clang__) || __GNUC_PREREQ__(2, 7)) \
    && !defined(__STRICT_ANSI__)
# define __MAYBE_UNUSED_A__ __attribute__((__unused__))
#else
# define __MAYBE_UNUSED_A__ /* delete */
#endif
#define __unused __MAYBE_UNUSED_A__

/*
 * The Fallthrough statement attribute
 */
#if defined(__STDC_VERSION__) && __STDC_VERSION__ > 202300 \
    || defined(__cplusplus) && __cplusplus >= 201703
# define __FALLTHROUGH_A__ [[fallthrough]]
#elif defined(__cplusplus) && __cplusplus >= 201103 \
    && __CLANG_PREREQ__(3, 3) \
    && !defined(__STRICT_ANSI__)
# define __FALLTHROUGH_A__ [[clang::fallthrough]]
#elif defined(__cplusplus) && __cplusplus >= 201103 \
    && __GNUC_PREREQ__(7, 1) \
    && !defined(__STRICT_ANSI__)
# define __FALLTHROUGH_A__ [[gnu::fallthrough]]
#elif (__CLANG_PREREQ__(10, 0) || __GNUC_PREREQ__(7, 1)) \
    && !defined(__STRICT_ANSI__)
# define __FALLTHROUGH_A__ __attribute__((__fallthrough__))
#else
# define __FALLTHROUGH_A__ do {} while (0)
#endif
#define fallthrough __FALLTHROUGH_A__

/*
 * The Used attribute in GNU dialect
 */
#if (defined(__STDC_VERSION__) && __STDC_VERSION__ > 202300 \
     || defined(__cplusplus) && __cplusplus >= 201103) \
    && (__CLANG_PREREQ__(3, 3) || __GNUC_PREREQ__(4, 8)) \
    && !defined(__STRICT_ANSI__)
# define __USED_A__ [[gnu::used]]
#elif (__CLANG_PREREQ__(1, 7) || __GNUC_PREREQ__(3, 1)) \
    && !defined(__STRICT_ANSI__)
# define __USED_A__ __attribute__((__used__))
#else
# define __USED_A__ __MAYBE_UNUSED_A__ /* suppress -Wunused warnings */
#endif
#define __used __USED_A__

/*
 * The No Discard function attribute
 */
#if defined(__STDC_VERSION__) && __STDC_VERSION__ > 202300 \
    || defined(__cplusplus) && __cplusplus >= 201703
# define __NODISCARD_A__ [[nodiscard]]
#elif defined(__cplusplus) && __cplusplus >= 201103 \
    && __CLANG_PREREQ__(3, 3) && !defined(__STRICT_ANSI__)
# define __NODISCARD_A__ [[clang::warn_unused_result]]
#elif defined(__cplusplus) && __cplusplus >= 201103 \
    && __GNUC_PREREQ__(4, 8) && !defined(__STRICT_ANSI__)
# define __NODISCARD_A__ [[gnu::warn_unused_result]]
#elif (defined(__clang__) || __GNUC_PREREQ__(3, 4)) \
    && !defined(__STRICT_ANSI__)
# define __NODISCARD_A__ __attribute__((__warn_unused_result__))
#else
# define __NODISCARD_A__ /* delete */
#endif
#define __warn_unused_result __NODISCARD_A__

/*
 * The Returns Twice function attribute in GNU dialect
 *
 * __RETURNS_TWICE_A__ makes the compiler not assume the function
 * only returns once.  This affects registerisation of variables:
 * even local variables need to be in memory across such a call.
 * Example: setjmp()
 */
#if (defined(__STDC_VERSION__) && __STDC_VERSION__ > 202300 \
     || defined(__cplusplus) && __cplusplus >= 201103) \
    && (__CLANG_PREREQ__(15, 0) || __GNUC_PREREQ__(4, 8)) \
    && !defined(__STRICT_ANSI__)
# define __RETURNS_TWICE_A__ [[gnu::returns_twice]]
#elif (__CLANG_PREREQ__(15, 0) || __GNUC_PREREQ__(4, 1)) \
    && !defined(__STRICT_ANSI__)
# define __RETURNS_TWICE_A__ __attribute__((__returns_twice__))
#else
# define __RETURNS_TWICE_A__ /* delete */
#endif
#define __returns_twice __RETURNS_TWICE_A__

/*
 * The No Instrument Function attribute in GNU dialect
 */
#if (defined(__STDC_VERSION__) && __STDC_VERSION__ > 202300 \
     || defined(__cplusplus) && __cplusplus >= 201103) \
    && (__CLANG_PREREQ__(3, 3) || __GNUC_PREREQ__(4, 8)) \
    && !defined(__STRICT_ANSI__)
# define __NO_INSTRUMENT_FUNCTION_A__ [[gnu::no_instrument_function]]
#elif (__CLANG_PREREQ__(2, 3) || __GNUC_PREREQ__(2, 95) /* or below */) \
    && !defined(__STRICT_ANSI__)
# define __NO_INSTRUMENT_FUNCTION_A__ __attribute__((__no_instrument_function__))
#else
# define __NO_INSTRUMENT_FUNCTION_A__ /* delete */
#endif
#define __noprof __NO_INSTRUMENT_FUNCTION_A__
#define __notrace __NO_INSTRUMENT_FUNCTION_A__

/*
 * The No Optimize function attribute in GNU dialect
 */
#if (defined(__STDC_VERSION__) && __STDC_VERSION__ > 202300 \
     || defined(__cplusplus) && __cplusplus >= 201103) \
    && __CLANG_PREREQ__(3, 5) && !defined(__STRICT_ANSI__)
# define __OPTIMIZE_NONE_A__ [[clang::optnone(0)]]
#elif (defined(__STDC_VERSION__) && __STDC_VERSION__ > 202300 \
       || defined(__cplusplus) && __cplusplus >= 201103) \
    && __GNUC_PREREQ__(4, 8) && !defined(__STRICT_ANSI__)
# define __OPTIMIZE_NONE_A__ [[gnu::optimize(0)]]
#elif __CLANG_PREREQ__(3, 5) && !defined(__STRICT_ANSI__)
# define __OPTIMIZE_NONE_A__ __attribute__((__optnone__))
#elif __GNUC_PREREQ__(5, 1) && !defined(__STRICT_ANSI__)
# define __OPTIMIZE_NONE_A__ __attribute__((__optimize__(0)))
#else
# define __OPTIMIZE_NONE_A__ /* delete */
#endif

/*
 * The Predict function macros
 *
 * clang version 14.0 and GCC version 2.96 adds explicit branch prediction
 * so that the CPU back-end can hint the processor and also so that
 * code blocks can be reordered such that the predicted path
 * sees a more linear flow, thus improving cache behavior, etc.
 *
 * The following two macros provide us with a way to utilize this
 * compiler feature.  Use PREDICT_TRUE() if you expect the expression
 * to evaluate to true, and PREDICT_FALSE() if you expect the
 * expression to evaluate to false.
 *
 * A few notes about usage:
 *
 *	* Generally, PREDICT_FALSE() error condition checks (unless
 *	  you have some _strong_ reason to do otherwise, in which case
 *	  document it), and/or PREDICT_TRUE() `no-error' condition
 *	  checks, assuming you want to optimize for the no-error case.
 *
 *	* Other than that, if you don't know the likelihood of a test
 *	  succeeding from empirical or other `hard' evidence, don't
 *	  make predictions.
 *
 *	* These are meant to be used in places that are run `a lot'.
 *	  It is wasteful to make predictions in code that is run
 *	  seldomly (e.g. at subsystem initialization time) as the
 *	  basic block reordering that this affects can often generate
 *	  larger code.
 */
#if (__CLANG_PREREQ__(14, 0) || __GNUC_PREREQ__(2, 96)) \
    && !defined(__STRICT_ANSI__)
# define PREDICT_TRUE(exp) __builtin_expect(((exp) != 0), 1)
# define PREDICT_FALSE(exp) __builtin_expect(((exp) != 0), 0)
#else
# define PREDICT_TRUE(exp) ((exp) != 0)
# define PREDICT_FALSE(exp) ((exp) != 0)
#endif
#define __predict_true(exp) PREDICT_TRUE(exp)
#define __predict_false(exp) PREDICT_FALSE(exp)

/*
 * The Packed data attribute in GNU dialect
 *
 * The __PACKED_A__ macro indicates that a variable or structure members
 * should have the smallest possible alignment, despite any host CPU
 * alignment requirements.
 *
 * This macro with __ALIGNAS_A__(x) are useful for describing the layout
 * and alignment of messages exchanged with hardware or other systems.
 */
#if (defined(__STDC_VERSION__) && __STDC_VERSION__ > 202300 \
     || defined(__cplusplus) && __cplusplus >= 201103) \
    && (__CLANG_PREREQ__(3, 3) || __GNUC_PREREQ__(4, 8)) \
    && !defined(__STRICT_ANSI__)
# define __PACKED_A__ [[gnu::packed]]
#elif (__CLANG_PREREQ__(2, 0) || __GNUC_PREREQ__(2, 7)) \
    && !defined(__STRICT_ANSI__)
# define __PACKED_A__ __attribute__((__packed__))
#else
# define __PACKED_A__ /* delete */
#endif
#define __packed __PACKED_A__

/*
 * The Alignas data attribute in GNU dialect
 *
 * The __ALIGNAS_A__(x) macro specifies the minimum alignment of a
 * variable or structure.
 *
 * This macro with __PACKED_A__ are useful for describing the layout
 * and alignment of messages exchanged with hardware or other systems.
 */
#if (defined(__STDC_VERSION__) && __STDC_VERSION__ > 202300 \
     || defined(__cplusplus) && __cplusplus >= 201103) \
    && (__CLANG_PREREQ__(3, 3) || __GNUC_PREREQ__(4, 8)) \
    && !defined(__STRICT_ANSI__)
# define __ALIGNAS_A__(x) [[gnu::aligned(x)]]
#elif (__CLANG_PREREQ__(2, 0) || __GNUC_PREREQ__(2, 7)) \
    && !defined(__STRICT_ANSI__)
# define __ALIGNAS_A__(x) __attribute__((__aligned__(x)))
#else
# define __ALIGNAS_A__(x) /* delete */
#endif

/*
 * The Alignas data keyword
 */
#if defined(__STDC_VERSION__) && __STDC_VERSION__ > 202300 \
    || defined(__cplusplus) && __cplusplus >= 201103
# define ALIGNAS_K(x) alignas(x)
/*#elif defined(__STDC_VERSION__) && __STDC_VERSION__ >= 201112
# define ALIGNAS_K(x) _Alignas(x) */
#else
# define ALIGNAS_K(x) __ALIGNAS_A__(x)
#endif
#define __aligned(x) __ALIGNAS_A__(x)

/*
 * The Malloc function attribute in GNU dialect
 */
#if (defined(__STDC_VERSION__) && __STDC_VERSION__ > 202300 \
     || defined(__cplusplus) && __cplusplus >= 201103) \
    && (__CLANG_PREREQ__(13, 0) || __GNUC_PREREQ__(4, 8)) \
    && !defined(__STRICT_ANSI__)
# define __MALLOC_A__ [[gnu::malloc]]
#elif (__CLANG_PREREQ__(13, 0) || __GNUC_PREREQ__(3, 0)) \
    && !defined(__STRICT_ANSI__)
# define __MALLOC_A__ __attribute__((__malloc__))
#else
# define __MALLOC_A__ /* delete */
#endif
#define __malloc __MALLOC_A__

/*
 * The Section attributes in GNU dialect
 */

/*
 * The Section attribute
 */
#if (defined(__STDC_VERSION__) && __STDC_VERSION__ > 202300 \
     || defined(__cplusplus) && __cplusplus >= 201103) \
    && (__CLANG_PREREQ__(3, 3) || __GNUC_PREREQ__(4, 8)) \
    && !defined(__STRICT_ANSI__)
# define __SECTION_A__(stg) [[gnu::section(stg)]]
#elif (__CLANG_PREREQ__(2, 0) || __GNUC_PREREQ__(2, 95) /* or below */) \
    && !defined(__STRICT_ANSI__)
# define __SECTION_A__(stg) __attribute__((__section__(stg)))
#else
# define __SECTION_A__(stg) /* delete section attributes */
#endif

/*
 * The BootData, CPText, KUData, RandomData and ROData Section macros
 */
#if (__CLANG_PREREQ__(2, 0) || __GNUC_PREREQ__(2, 95) /* or below */) \
    && !defined(__STRICT_ANSI__)
# define __SECTION_BOOTDATA_A__ __SECTION_A__(".bootdata")
# define __SECTION_CPTEXT_A__ __SECTION_A__(".cptext")
# define __SECTION_KUDATA_A__ __SECTION_A__(".kudata")
# define __SECTION_RANDOMDATA_A__ __SECTION_A__(".randomdata")
# define __SECTION_RODATA_A__ __SECTION_A__(".rodata")
#else
# define __SECTION_BOOTDATA_A__
# define __SECTION_CPTEXT_A__
# define __SECTION_KUDATA_A__
# define __SECTION_RANDOMDATA_A__
# define __SECTION_RODATA_A__
#endif

/*
 * Start the C++ compatibilty
 */
#if defined(__cplusplus)
# define __BEGIN_EXTERN_C extern "C" {
# define __END_EXTERN_C }
#else
# define __BEGIN_EXTERN_C
# define __END_EXTERN_C
#endif

/*
 * The Visibility attributes in GNU dialect
 */

/*
 * The Visibility attribute
 */
#if (defined(__STDC_VERSION__) && __STDC_VERSION__ > 202300 \
     || defined(__cplusplus) && __cplusplus >= 201103) \
    && (__CLANG_PREREQ__(3, 3) || __GNUC_PREREQ__(4, 8)) \
    && !defined(__STRICT_ANSI__)
# define __VISIBILITY_A__(stg) [[gnu::visibility(stg)]]
#elif (__CLANG_PREREQ__(2, 0) || __GNUC_PREREQ__(3, 3)) \
    && !defined(__STRICT_ANSI__)
# define __VISIBILITY_A__(stg) __attribute__((__visibility__(stg)))
#else
# define __VISIBILITY_A__(stg)
#endif
#define __visibility(stg) __VISIBILITY_A__(stg)

/*
 * Public/Hidden Visibility macros and pragmas in GNU dialect
 */
#if (__CLANG_PREREQ__(3, 3) || __GNUC_PREREQ__(4, 0)) \
    && !defined(__STRICT_ANSI__)
# define __VISIBILITY_PUBLIC_A__ __VISIBILITY_A__("default")
# define __VISIBILITY_HIDDEN_A__ __VISIBILITY_A__("hidden")
# define __BEGIN_PUBLIC_DECLS \
    _Pragma("GCC visibility push(default)") __BEGIN_EXTERN_C
# define __END_PUBLIC_DECLS __END_EXTERN_C _Pragma("GCC visibility pop")
# define __BEGIN_HIDDEN_DECLS \
    _Pragma("GCC visibility push(hidden)") __BEGIN_EXTERN_C
# define __END_HIDDEN_DECLS __END_EXTERN_C _Pragma("GCC visibility pop")
#else
# define __VISIBILITY_PUBLIC_A__
# define __VISIBILITY_HIDDEN_A__
# define __BEGIN_PUBLIC_DECLS __BEGIN_EXTERN_C
# define __END_PUBLIC_DECLS __END_EXTERN_C
# define __BEGIN_HIDDEN_DECLS __BEGIN_EXTERN_C
# define __END_HIDDEN_DECLS __END_EXTERN_C
#endif
#define __dso_public __VISIBILITY_PUBLIC_A__
#define __dso_hidden __VISIBILITY_HIDDEN_A__

/*
 * End the C++ compatibilty
 */
#define __BEGIN_DECLS __BEGIN_EXTERN_C
#define __END_DECLS __END_EXTERN_C

/*
 * "The nice thing about standards is that there are so many to choose from."
 * There are a number of "feature test macros" specified by (different)
 * standards that determine which interfaces and types the header files
 * should expose.
 *
 * Because of inconsistencies in these macros, we define our own
 * set in the private name space that end in _VISIBLE.  These are
 * always defined and so headers can test their values easily.
 * Things can get tricky when multiple feature macros are defined.
 * We try to take the union of all the features requested.
 *
 * The following macros are guaranteed to have a value after cdefs.h
 * has been included:
 *	__POSIX_VISIBLE
 *	__XPG_VISIBLE
 *	__ISO_C_VISIBLE
 *	__BSD_VISIBLE
 */

/*
 * X/Open Portability Guides and Single Unix Specifications.
 * _XOPEN_SOURCE				XPG3
 * _XOPEN_SOURCE && _XOPEN_VERSION = 4		XPG4
 * _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED = 1	XPG4v2
 * _XOPEN_SOURCE == 500				XPG5
 * _XOPEN_SOURCE == 520				XPG5v2
 * _XOPEN_SOURCE == 600				POSIX 1003.1-2001 with XSI
 * _XOPEN_SOURCE == 700				POSIX 1003.1-2008 with XSI
 *
 * The XPG spec implies a specific value for _POSIX_C_SOURCE.
 */
#ifdef _XOPEN_SOURCE
# if (_XOPEN_SOURCE - 0 >= 700)
#  define __XPG_VISIBLE 700
#  undef _POSIX_C_SOURCE
#  define _POSIX_C_SOURCE 200809L
# elif (_XOPEN_SOURCE - 0 >= 600)
#  define __XPG_VISIBLE 600
#  undef _POSIX_C_SOURCE
#  define _POSIX_C_SOURCE 200112L
# elif (_XOPEN_SOURCE - 0 >= 520)
#  define __XPG_VISIBLE 520
#  undef _POSIX_C_SOURCE
#  define _POSIX_C_SOURCE 199506L
# elif (_XOPEN_SOURCE - 0 >= 500)
#  define __XPG_VISIBLE 500
#  undef _POSIX_C_SOURCE
#  define _POSIX_C_SOURCE 199506L
# elif (_XOPEN_SOURCE_EXTENDED - 0 == 1)
#  define __XPG_VISIBLE 420
# elif (_XOPEN_VERSION - 0 >= 4)
#  define __XPG_VISIBLE 400
# else
#  define __XPG_VISIBLE 300
# endif
#endif

/*
 * POSIX macros, these checks must follow the XOPEN ones above.
 *
 * _POSIX_SOURCE == 1		1003.1-1988 (superseded by _POSIX_C_SOURCE)
 *				Board Approval: 1988-08-22
 *				https://standards.ieee.org/ieee/1003.1/1388/
 * _POSIX_C_SOURCE == 1			1003.1-1990
 *				Board Approval (1003.1): 1990-09-28
 *				https://standards.ieee.org/ieee/1003.1/1385/
 *				ISO/IEC 9945-1:1990
 *				https://standards.ieee.org/ieee/9945-1/2367/
 * _POSIX_C_SOURCE == 2			1003.2-1992
 *				Board Approval: 1992-09-17
 *				https://standards.ieee.org/ieee/1003.2/1408/
 * _POSIX_C_SOURCE == 199309L	1003.1b-1993
 *				Board Approval: 1993-09-15
 *				https://standards.ieee.org/ieee/1003.1b/1392/
 * _POSIX_C_SOURCE == 199506L	1003.1c-1995, 1003.1i-1995,
 *				and the omnibus ISO/IEC 9945-1:1996
 *				Board Approval (1003.1): 1995-06-14
 *				https://standards.ieee.org/ieee/1003.1c/1393/
 *				https://standards.ieee.org/ieee/1003.1i/1399/
 *				https://standards.ieee.org/ieee/9945-1/2366/
 * _POSIX_C_SOURCE == 199909L	1003.1d-1999
 *				Board Approval: 1999-09-16
 *				https://standards.ieee.org/ieee/1003.1d/1394/
 * _POSIX_C_SOURCE == 200001L	1003.1g-2000 and 1003.1j-2000
 *				Board Approval: 2000-01-30
 *				https://standards.ieee.org/ieee/1003.1g/1397/
 *				https://standards.ieee.org/ieee/1003.1j/1400/
 * _POSIX_C_SOURCE == 200009L	1003.1q-2000
 *				Board Approval: 2000-09-21
 *				https://standards.ieee.org/ieee/1003.1q/1405/
 * _POSIX_C_SOURCE == 200112L	1003.1-2001
 *				Board Approval (1003.1): 2001-12-06
 *				https://standards.ieee.org/ieee/1003.1/1389/
 *				ISO/IEC 9945-1:2003
 *				https://standards.ieee.org/ieee/9945-1/3453/
 * _POSIX_C_SOURCE == 200809L	1003.1-2008
 *				Board Approval: 2008-09-26
 *				https://standards.ieee.org/ieee/1003.1/4020/
 * _POSIX_C_SOURCE == 201712L	1003.1-2017
 *				Board Approval: 2017-12-06
 *				https://standards.ieee.org/ieee/1003.1/4020/
 *
 * The POSIX spec implies a specific value for __ISO_C_VISIBLE, though
 * this may be overridden by the _ISOC99_SOURCE macro later.
 */
#ifdef _POSIX_C_SOURCE
# if (_POSIX_C_SOURCE - 0 >= 201712)
#  define __POSIX_VISIBLE 201712
#  define __ISO_C_VISIBLE 1999
# elif (_POSIX_C_SOURCE - 0 >= 200809)
#  define __POSIX_VISIBLE 200809
#  define __ISO_C_VISIBLE 1999
# elif (_POSIX_C_SOURCE - 0 >= 200112)
#  define __POSIX_VISIBLE 200112
#  define __ISO_C_VISIBLE 1999
# elif (_POSIX_C_SOURCE - 0 >= 200009)
#  define __POSIX_VISIBLE 200009
#  define __ISO_C_VISIBLE 1990
# elif (_POSIX_C_SOURCE - 0 >= 200001)
#  define __POSIX_VISIBLE 200001
#  define __ISO_C_VISIBLE 1990
# elif (_POSIX_C_SOURCE - 0 >= 199909)
#  define __POSIX_VISIBLE 199909
#  define __ISO_C_VISIBLE 1990
# elif (_POSIX_C_SOURCE - 0 >= 199506)
#  define __POSIX_VISIBLE 199506
#  define __ISO_C_VISIBLE 1990
# elif (_POSIX_C_SOURCE - 0 >= 199309)
#  define __POSIX_VISIBLE 199309
#  define __ISO_C_VISIBLE 1990
# elif (_POSIX_C_SOURCE - 0 >= 2)
#  define __POSIX_VISIBLE 199209
#  define __ISO_C_VISIBLE 1990
# else
#  define __POSIX_VISIBLE 199009
#  define __ISO_C_VISIBLE 1990
# endif
#elif defined(_POSIX_SOURCE)
# define __POSIX_VISIBLE 198808
#  define __ISO_C_VISIBLE 0
#endif

/*
 * _ANSI_SOURCE means to expose ANSI C89 interfaces only.
 * If the user defines it in addition to one of the POSIX or XOPEN
 * macros, assume the POSIX/XOPEN macro(s) should take precedence.
 */
#if defined(_ANSI_SOURCE) && !defined(__POSIX_VISIBLE) && \
    !defined(__XPG_VISIBLE)
# define __POSIX_VISIBLE 0
# define __XPG_VISIBLE 0
# define __ISO_C_VISIBLE 1990
#endif

/*
 * _ISOC??_SOURCE, __STDC_VERSION__, and __cplusplus
 * override any of the other macros since they are non-exclusive.
 */
/* C17: ISO/IEC 9899:2018 */
#if defined(_ISOC17_SOURCE) || \
    (defined(__STDC_VERSION__) && __STDC_VERSION__ >= 201710) || \
    (defined(__cplusplus) && __cplusplus >= 202002)
# undef __ISO_C_VISIBLE
# define __ISO_C_VISIBLE 2017
/* C11: ISO/IEC 9899:2011 */
#elif defined(_ISOC11_SOURCE) || \
    (defined(__STDC_VERSION__) && __STDC_VERSION__ >= 201112) || \
    (defined(__cplusplus) && (__cplusplus >= 201703 || __cplusplus >= 201402))
# undef __ISO_C_VISIBLE
# define __ISO_C_VISIBLE 2011
/* C99: ISO/IEC 9899:1999 */
#elif defined(_ISOC99_SOURCE) || \
    (defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901) || \
    (defined(__cplusplus) && __cplusplus >= 201103)
# undef __ISO_C_VISIBLE
# define __ISO_C_VISIBLE 1999
/* C90 + C95 (amendment): ISO/IEC 9899:1990/AMD1:1995 */
#elif defined(_ISOC90_SOURCE) || \
    (defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199409) || \
    (defined(__cplusplus) && __cplusplus >= 199711)
# undef __ISO_C_VISIBLE
# define __ISO_C_VISIBLE 1990
#else
/* C89/C90: ANSI X3.159-1989, ISO/IEC 9899:1990 */
# undef __ISO_C_VISIBLE
# define __ISO_C_VISIBLE 0
#endif

/*
 * Finally deal with BSD-specific interfaces that are not covered
 * by any standards.  We expose these when none of the POSIX or XPG
 * macros is defined or if the user explicitly asks for them.
 */
#if !defined(_BSD_SOURCE) && \
    (defined(_ANSI_SOURCE) || defined(__XPG_VISIBLE) || defined(__POSIX_VISIBLE))
# define __BSD_VISIBLE 0
#endif

/*
 * Default values.
 */
#ifndef __XPG_VISIBLE
# define __XPG_VISIBLE 700
#endif
#ifndef __POSIX_VISIBLE
# define __POSIX_VISIBLE 201712
#endif
#ifndef __ISO_C_VISIBLE
# define __ISO_C_VISIBLE 2017
#endif
#ifndef __BSD_VISIBLE
# define __BSD_VISIBLE 1
#endif

#endif /* !__SYS_CDEFS_H__ */
